/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLTMONITORBASE_H
#define HLTMONITORBASE_H 1

// Include files
// Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

// boost
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>

namespace LHCb {
   class HltDecReports;
}

/** @class HltMonitorBase HltMonitorBase.h
 *  Base class for Offline Hlt Monitoring algorithms. It contains a
 *  map of decisions to the regex which they match. This container can
 *  only be filled when the first event is processed.
 *
 *  @author Roel Aaij
 *  @date   2010-08-24
 */
class HltMonitorBase : public GaudiHistoAlg {
public:
   /// Standard constructor
   using GaudiHistoAlg::GaudiHistoAlg;

   StatusCode initialize() override;    ///< Algorithm initialization

protected:

   std::vector<const LHCb::HltDecReports*> hltDecReports() const;
   Gaudi::Property<std::vector< std::string >> m_decReportsLocations 
      {this, "HltDecReportsLocations", {LHCb::HltDecReportsLocation::Default} };

   // Tags for the container.
   struct regexTag { };
   struct decisionTag { };

   typedef std::pair< std::string, std::string > pair_t;

   // Multi index container to hold the items.
   typedef boost::multi_index_container<
      pair_t,
      boost::multi_index::indexed_by<
         boost::multi_index::ordered_non_unique<
            boost::multi_index::tag< regexTag >,
            boost::multi_index::member< pair_t, std::string, &pair_t::first >
            >,
         boost::multi_index::ordered_unique<
            boost::multi_index::tag< decisionTag >,
            boost::multi_index::member< pair_t, std::string, &pair_t::second >
            > >
      > decMap_t;

   typedef decMap_t::index< regexTag >::type decByRegex_t;
   typedef decMap_t::index< decisionTag >::type decByDec_t;

   decMap_t m_decisions;

   bool m_filledDecisions = false;

   void fillDecisions( const LHCb::HltDecReports* decReports );

private:
   Gaudi::Property<std::vector<std::string>> m_regexes {this, "Regexes"};

};
#endif // HLTMONITORBASE_H
