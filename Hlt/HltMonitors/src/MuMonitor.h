/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUHISTOGRAMS_H
#define MUHISTOGRAMS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "LoKi/HLT.h"
#include "LoKi/HLTTypes.h"
#include "AIDA/IProfile1D.h"
#include "AIDA/IHistogram1D.h"
#include "HltMonitorBase.h"
/** @class MuMonitor MuMonitor.h
 *
 *  Fill histogram of mu per bunch crossing on NoBias events
 *
 *  Some code from the HLT_PASS_RE in C++ thread in lhcb-hlt2-development
 *
 *  @author Patrick Koppenburg
 *  @date   2011-04-01
 */
class MuMonitor : public HltMonitorBase {
public:
  /// Standard constructor
  MuMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

private:
  std::string m_pattern ; ///< HLT filter expression
  std::string m_location; ///< Location of HltDecReports
  LoKi::Types::HLT_Cut m_filter = LoKi::Constant<const LHCb::HltDecReports*,bool> ( true ); ///< Loki filter
  unsigned int m_nBX  = 2808;
  AIDA::IProfile1D* m_hMuVelo = nullptr;
  AIDA::IHistogram1D* m_hVelo = nullptr;
  AIDA::IHistogram1D* m_bBx = nullptr;

};
#endif // MUHISTOGRAMS_H
