/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLTRATEMONITOR_H
#define HLTRATEMONITOR_H 1

// Include files
// from Gaudi
#include "HltMonitorBase.h"

struct Condition;

/** @class HltRateMonitor HltRateMonitor.h
 *  Algorithm to monitor hlt rates offline.
 *  The most important property is Regexes. It is a list of regexes and for
 *  every decision which matches a regex, a histogram of its rate is filled.
 *  A histogram is also filled with the combined rate of all decisions which
 *  match a regex.
 *  The default is "Hlt2Express.*Decision", which monitors the Hlt2Express
 *  lines.
 *
 *  @author Roel Aaij
 *  @date   2010-08-24
 */
class HltRateMonitor : public HltMonitorBase {
public:

   /// Standard constructor
   HltRateMonitor( const std::string& name, ISvcLocator* pSvcLocator );

   StatusCode initialize() override;    ///< Algorithm initialization
   StatusCode execute() override;    ///< Algorithm execution

private:

   std::string m_decReportsLocation;

   Gaudi::Property<std::string> m_ODINLocation
      {this, "ODINLocation", LHCb::ODINLocation::Default};

   Gaudi::Property<std::string> m_runParameterLocation
      {this, "RunParameterLocation", "Conditions/Online/LHCb/RunParameters"};

   Condition* m_runParameters = nullptr;
   long long unsigned int m_startOfRun = 0;
   unsigned int m_runNumber = 0;

   bool m_disabled = false;
   Gaudi::Property<bool> m_forceEnable {this, "ForceEnable", false};

   Gaudi::Property<unsigned int> m_secondsPerBin {this, "SecondsPerBin", 10};

   /// Helper method to manage our condtion
   StatusCode i_updateConditions();

};
#endif // HLTRATEMONITOR_H
