/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRFTZONEHANDLER_H
#define PRFTZONEHANDLER_H 1

// Include files
#include "DetDesc/ValidDataObject.h"
#include "Kernel/DetectorSegment.h"
#include "PrKernel/PrHitZone.h"
#include "PrKernel/PrFTInfo.h"

/** @class PrFTZoneHandler PrFTZoneHandler.h
 * Handlers of zones, the object is stored in the detector event store as a condition
 * and each algorithms reads the object from there without calling the HitManagers (tools)
 * @author Renato Quagliani
 * @author Sebastien Ponce
 */

class PrFTZoneHandler final : public ValidDataObject {

public:

  /// Standard constructor
  PrFTZoneHandler( ) = default;

  void MakeZone( unsigned int n , DetectorSegment& seg , float xMin, float xMax, float yMin, float yMax){
    m_zones[n].setZone( n, seg, xMin, xMax, yMin, yMax);
  }
  const PrHitZone& zone( unsigned int n ) const{
    return m_zones[n];
  }

  template<PrHitZone::Side SIDE>
  static int getXZone(int layer) {
    if constexpr (SIDE == PrHitZone::Side::Upper) {
      return PrFTInfo::xZonesUpper[layer];
    } else {
      return PrFTInfo::xZonesLower[layer];
    }
  }

  template<PrHitZone::Side SIDE>
  static int getUVZone(int layer) {
    if constexpr (SIDE == PrHitZone::Side::Upper) {
      return PrFTInfo::uvZonesUpper[layer];
    } else {
      return PrFTInfo::uvZonesLower[layer];
    }
  }

  template<PrHitZone::Side SIDE>
  static int getTriangleZone(int layer) {
    if constexpr (SIDE == PrHitZone::Side::Upper) {
      return PrFTInfo::uvZonesLower[layer];
    } else {
      return PrFTInfo::uvZonesUpper[layer];
    }
  }

 private:

  //plain vector with indexing needed!
  std::array<PrHitZone, PrFTInfo::Numbers::NFTZones> m_zones;
};

#endif // PRFTZONEHANDLER_H
