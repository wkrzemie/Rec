/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LHCB_CONVERTERS_TRACK_V1_FROMV2TRACKV1TRACK_H
#define LHCB_CONVERTERS_TRACK_V1_FROMV2TRACKV1TRACK_H

// Include files
// from Gaudi
#include "Event/Track.h"
#include "Event/Track_v2.h"
#include "GaudiAlg/ScalarTransformer.h"

namespace {
  LHCb::Event::v1::Track ConvertTrack( LHCb::Event::v2::Track const& track )
  {
    auto outTr = LHCb::Event::v1::Track{};
    outTr.setChi2PerDoF( track.chi2PerDoF() );
    outTr.setNDoF( track.nDoF() );
    outTr.setLikelihood( track.likelihood() );
    outTr.setGhostProbability( track.ghostProbability() );
    outTr.setFlags( track.flags() );
    outTr.addToLhcbIDs( track.lhcbIDs() );

    // copy the states
    for ( auto const& state : track.states() ) {
      outTr.addToStates( state );
    }

    // copy the track fit info
    if ( track.fitResult() != nullptr ) {
      outTr.setFitResult( track.fitResult()->clone() );
    }

    outTr.setExtraInfo( track.extraInfo() );

    // Ancestors won't be copied
    return outTr;
  }
}
namespace LHCb
{
  namespace Converters
  {
    namespace Track
    {
      namespace v1
      {

        struct fromV2TrackV1Track
            : public Gaudi::Functional::ScalarTransformer<
                  fromV2TrackV1Track, LHCb::Event::v1::Tracks( std::vector<LHCb::Event::v2::Track> const& )> {

          fromV2TrackV1Track( std::string const& name, ISvcLocator* pSvcLocator )
              : ScalarTransformer( name, pSvcLocator, KeyValue{"InputTracksName", ""},
                                   KeyValue{"OutputTracksName", ""} )
          {
          }

          using ScalarTransformer::operator();

          /// The main function, converts the track
          LHCb::Event::v1::Track* operator()( LHCb::Event::v2::Track const& track ) const
          {
            return new LHCb::Event::v1::Track{ConvertTrack( track )};
          }
        };
        DECLARE_COMPONENT( fromV2TrackV1Track )

        struct fromV2TrackV1TrackVector
            : public Gaudi::Functional::ScalarTransformer<fromV2TrackV1TrackVector,
                                                          std::vector<LHCb::Event::v1::Track>(
                                                              std::vector<LHCb::Event::v2::Track> const& )> {

          fromV2TrackV1TrackVector( std::string const& name, ISvcLocator* pSvcLocator )
              : ScalarTransformer( name, pSvcLocator, KeyValue{"InputTracksName", ""},
                                   KeyValue{"OutputTracksName", ""} )
          {
          }

          using ScalarTransformer::operator();

          /// The main function, converts the track
          LHCb::Event::v1::Track operator()( LHCb::Event::v2::Track const& track ) const
          {
            return ConvertTrack( track );
          }
        };
        DECLARE_COMPONENT( fromV2TrackV1TrackVector )

      } // namespace v1
    }   // namespace Track
  }     // namespace Converters
} // namespace LHCb

#endif // LHCB_CONVERTERS_TRACK_V1_FROMV2TRACKV1TRACK_H
