###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: PrPixel
################################################################################
gaudi_subdir(PrPixel v1r14)

gaudi_depends_on_subdirs(Det/VPDet
                         DAQ/DAQKernel
                         Event/DAQEvent
                         Event/DigiEvent
                         Event/TrackEvent
                         Pr/PrKernel
                         GaudiAlg
                         GaudiKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

#SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address -fno-omit-frame-pointer")
#set (CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -fsanitize=address")

gaudi_add_module(PrPixel
                 src/*.cpp
                 INCLUDE_DIRS AIDA GSL Pr/PrKernel
		 LINK_LIBRARIES VPDetLib DAQEventLib DAQKernelLib TrackEvent GaudiAlgLib GaudiKernel)



