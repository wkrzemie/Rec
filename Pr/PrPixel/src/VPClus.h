/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VPCLUS_H
#define VPCLUS_H 1

#include <array>
#include <tuple>
#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/VPChannelID.h"
#include "Kernel/VPConstants.h"
#include "PrKernel/PrPixelModule.h"
#include "VPDet/DeVP.h"
#include "PrKernel/VeloPixelInfo.h"

//FIXME still need to handle nx and ny for the clusters

// Namespace for locations in TDS
namespace LHCb
{
  namespace VPClusterLocation
  {
    inline const std::string Offsets = "Raw/VP/LightClustersOffsets";
  }
}


class VPClus
  : public Gaudi::Functional::MultiTransformer<std::tuple<std::vector<LHCb::VPLightCluster>,
                                                          std::array<unsigned, VeloInfo::Numbers::NOffsets>>(
          const LHCb::RawEvent& )>
{

public:
  /// Standard constructor
  VPClus( const std::string& name, ISvcLocator* pSvcLocator );

  /// Algorithm initialization
  StatusCode initialize() override;

  /// Algorithm execution
  std::tuple<std::vector<LHCb::VPLightCluster>, std::array<unsigned,VeloInfo::Numbers::NOffsets >>
  operator()( const LHCb::RawEvent& ) const override;

private:

  /// Recompute the geometry in case of change
  StatusCode rebuildGeometry();
  //
  /// Detector element
  DeVP* m_vp{nullptr};

  /// List of pointers to modules (which contain pointers to their hits)
  /// FIXME This is not thread safe on a change on geometry.
  /// FIXME These vectors should be stored as a derived condition
  std::vector<PrPixelModule*> m_modules;
  std::vector<PrPixelModule> m_module_pool;

  /// Indices of first and last module
  unsigned int m_firstModule;
  unsigned int m_lastModule;

  /// Maximum allowed cluster size (no effect when running on lite clusters).
  unsigned int m_maxClusterSize = 999999999;

  float m_ltg[16 * VP::NSensors]; // 16*208 = 16*number of sensors
  LHCb::span<const double> m_local_x;
  LHCb::span<const double> m_x_pitch;
  float m_pixel_size;

  std::bitset<VP::NModules> m_modulesToSkipMask;
  Gaudi::Property<std::vector<unsigned int>> m_modulesToSkip
    { this, "ModulesToSkip",{}, [=](Property&) {
          // if( msgLevel(MSG::DEBUG)){
          //   info() << "Modules to skip size: " << m_modulesToSkip.size() << endmsg;
          // }
          m_modulesToSkipMask.reset();
          for( auto i : m_modulesToSkip) {
              // if( msgLevel(MSG::DEBUG)){
              //   info() << "Skipping module " << moduleNumber << endmsg;
              // }
              m_modulesToSkipMask.set( i);
          }
      }, Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
      "List of modules that should be skipped in decoding"};

  mutable Gaudi::Accumulators::SummingCounter<> m_nbClustersCounter{ this, "Nb of Produced Clusters" };
};

#endif // VPClus_H

