/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LHCb
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Kernel/VPConstants.h"
#include "VPKernel/PixelUtils.h"

// Local
#include "VPClusFull.h"
#include <iomanip>

DECLARE_COMPONENT( VPClusFull )

namespace
{
  struct SPCache {
    std::array<float, 4> fxy;
    unsigned char pattern;
    unsigned char nx1;
    unsigned char nx2;
    unsigned char ny1;
    unsigned char ny2;
  };
  //=========================================================================
  // Cache Super Pixel cluster patterns.
  //=========================================================================
  auto create_SPPatterns()
  {
    std::array<SPCache, 256> SPCaches;
    // create a cache for all super pixel cluster patterns.
    // this is an unoptimized 8-way flood fill on the 8 pixels
    // in the super pixel.
    // no point in optimizing as this is called once in
    // initialize() and only takes about 20 us.

    // define deltas to 8-connectivity neighbours
    const int dx[] = {-1, 0, 1, -1, 0, 1, -1, 1};
    const int dy[] = {-1, -1, -1, 1, 1, 1, 0, 0};

    // clustering buffer for isolated superpixels.
    unsigned char sp_buffer[8];

    // SP index buffer and its size for single SP clustering
    unsigned char sp_idx[8];
    unsigned char sp_idx_size = 0;

    // stack and stack pointer for single SP clustering
    unsigned char sp_stack[8];
    unsigned char sp_stack_ptr = 0;

    // loop over all possible SP patterns
    for ( unsigned int sp = 0; sp < 256; ++sp ) {
      sp_idx_size = 0;
      for ( unsigned int shift = 0; shift < 8; ++shift ) {
        const unsigned char p = sp & ( 1 << shift );
        sp_buffer[shift]      = p;
        if ( p ) {
          sp_idx[sp_idx_size++] = shift;
        }
      }

      // loop over pixels in this SP and use them as
      // cluster seeds.
      // note that there are at most two clusters
      // in a single super pixel!
      unsigned char clu_idx = 0;
      for ( unsigned int ip = 0; ip < sp_idx_size; ++ip ) {
        unsigned char idx = sp_idx[ip];

        if ( 0 == sp_buffer[idx] ) {
          continue;
        } // pixel is used

        sp_stack_ptr             = 0;
        sp_stack[sp_stack_ptr++] = idx;
        sp_buffer[idx]           = 0;
        unsigned char x          = 0;
        unsigned char y          = 0;
        unsigned char n          = 0;

        while ( sp_stack_ptr ) {
          idx                     = sp_stack[--sp_stack_ptr];
          const unsigned char row = idx % 4;
          const unsigned char col = idx / 4;
          x += col;
          y += row;
          ++n;

          for ( unsigned int ni = 0; ni < 8; ++ni ) {
            const char ncol = col + dx[ni];
            if ( ncol < 0 || ncol > 1 ) continue;
            const char nrow = row + dy[ni];
            if ( nrow < 0 || nrow > 3 ) continue;
            const unsigned char nidx = ( ncol << 2 ) | nrow;
            if ( 0 == sp_buffer[nidx] ) continue;
            sp_stack[sp_stack_ptr++] = nidx;
            sp_buffer[nidx]          = 0;
          }
        }

        const uint32_t cx = x / n;
        const uint32_t cy = y / n;
        const float fx    = x / static_cast<float>( n ) - cx;
        const float fy    = y / static_cast<float>( n ) - cy;

        // store the centroid pixel
        SPCaches[sp].pattern |= ( ( cx << 2 ) | cy ) << 4 * clu_idx;

        // set the two cluster flag if this is the second cluster
        SPCaches[sp].pattern |= clu_idx << 3;

        // set the pixel fractions
        SPCaches[sp].fxy[2 * clu_idx]     = fx;
        SPCaches[sp].fxy[2 * clu_idx + 1] = fy;

        // increment cluster count. note that this can only become 0 or 1!
        ++clu_idx;
      }
    }
    return SPCaches;
  }
  // SP pattern buffers for clustering, cached once.
  // There are 256 patterns and there can be at most two
  // distinct clusters in an SP.
  static const std::array<SPCache, 256> s_SPCaches = create_SPPatterns();

  using namespace Pixel;
}
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPClusFull::VPClusFull( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer( name, pSvcLocator,
			KeyValue{"RawEventLocation",
			    Gaudi::Functional::concat_alternatives(LHCb::RawEventLocation::Velo,
								   LHCb::RawEventLocation::Default,
								   LHCb::RawEventLocation::Other)},
                        {KeyValue{"ClusterLocation", LHCb::VPFullClusterLocation::Default},
                         KeyValue{"ClusterOffsets", LHCb::VPFullClusterLocation::Offsets}} )
{ }

//=============================================================================
// Initialization
//=============================================================================
StatusCode VPClusFull::initialize()
{

  StatusCode sc = MultiTransformer::initialize();
  if ( sc.isFailure() ) return sc;

  m_vp = getDet<DeVP>( DeVPLocation::Default );
  // Make sure we are up-to-date on populated VELO stations
  registerCondition( m_vp->sensors().front()->geometry(), &VPClusFull::rebuildGeometry );

  sc = updMgrSvc()->update( this );
  if ( !sc.isSuccess() ) return Error( "Failed to update station structure." );

  return StatusCode::SUCCESS;
}

//============================================================================
// Rebuild the geometry (in case something changes in the Velo during the run)
//============================================================================
StatusCode VPClusFull::rebuildGeometry()
{

  // Delete the existing modules.
  m_modules.clear();
  m_firstModule = 999;
  m_lastModule  = 0;

  int previousLeft   = -1;
  int previousRight  = -1;
  const auto sensors = m_vp->sensors();

  m_local_x = sensors.front()->xLocal();
  m_x_pitch = sensors.front()->xPitch();
  m_pixel_size = sensors.front()->pixelSize(LHCb::VPChannelID(0, 0, 0, 0)).second;


  float ltg_rot_components[9];
  for ( unsigned i = 0; i < 208; ++i ) {
    // TODO:
    // if (!sensor->isReadOut()) continue;
    auto sensor = sensors[i];

    // get the local to global transformation matrix and
    // store it in a flat float array of sixe 12.
    Gaudi::Rotation3D ltg_rot;
    Gaudi::TranslationXYZ ltg_trans;
    sensor->geometry()->toGlobalMatrix().GetDecomposition( ltg_rot, ltg_trans );
    ltg_rot.GetComponents( ltg_rot_components );
    unsigned idx = 16 * sensor->sensorNumber();
    m_ltg[idx++] = ltg_rot_components[0];
    m_ltg[idx++] = ltg_rot_components[1];
    m_ltg[idx++] = ltg_rot_components[2];
    m_ltg[idx++] = ltg_rot_components[3];
    m_ltg[idx++] = ltg_rot_components[4];
    m_ltg[idx++] = ltg_rot_components[5];
    m_ltg[idx++] = ltg_rot_components[6];
    m_ltg[idx++] = ltg_rot_components[7];
    m_ltg[idx++] = ltg_rot_components[8];
    m_ltg[idx++] = ltg_trans.X();
    m_ltg[idx++] = ltg_trans.Y();
    m_ltg[idx++] = ltg_trans.Z();


    // Get the number of the module this sensor is on.
    const unsigned int number = sensor->module();
    if ( number < m_modules.size() ) {
      // Check if this module has already been setup.
      if ( m_modules[number] ) continue;
    } else {
      m_modules.resize( number + 1, 0 );
    }

    // Create a new module and add it to the list.
    m_module_pool.emplace_back( number, sensor->isRight() );
    PixelModule* module = &m_module_pool.back();
    module->setZ( sensor->z() );
    if ( sensor->isRight() ) {
      module->setPrevious( previousRight );
      previousRight = number;
    } else {
      module->setPrevious( previousLeft );
      previousLeft = number;
    }
    m_modules[number] = module;
    if ( m_firstModule > number ) m_firstModule = number;
    if ( m_lastModule < number ) m_lastModule = number;
  }
  // the module pool might have been resized -- make sure
  // all module pointers are valid.
  for ( unsigned int i = 0; i < m_module_pool.size(); ++i ) {
    PixelModule* module       = &m_module_pool[i];
    m_modules[module->number()] = module;
  }
  return StatusCode::SUCCESS;
}


//=============================================================================
// Main execution
//=============================================================================
std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>> VPClusFull::operator()( const LHCb::RawEvent& rawEvent ) const
{

  //WARNING:
  //This clustering algorithm is designed to perform the Offline Clustering without checking any isolation flag
  auto result = std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>{};

  const auto& tBanks = rawEvent.banks( LHCb::RawBank::VP );
  if ( tBanks.empty() ) return result;

  const unsigned int version = tBanks[0]->version();
  if ( version != 2 ) {
    warning() << "Unsupported raw bank version (" << version << ")" << endmsg;
    return result;
  }
  // WARNING:
  // This is a rather long function. Please refrain from breaking this
  // up into smaller functions as this will severely impact the
  // timing performance. And yes, this has been measured. Just don't.

  // Clustering buffers
  std::array<bool, VP::NPixelsPerSensor> buffer{};
  std::vector<uint32_t> pixel_idx;
  std::vector<uint32_t> stack;

  // reserve a minimal stack
  stack.reserve( 64 );

  // Since the pool is local, to first preallocate the pool, then count hits per module,
  // and then preallocate per module and move hits might not be faster than adding
  // directly to the PixelModuleHits (which would require more allocations, but
  // not many if we start with a sensible default)
  auto& [ pool, offsets]  = result;
  const unsigned int startSize = 10000U;
  pool.reserve( startSize );

  //1 module = N Cluster -> N x M channelIDs
  std::vector<std::vector<LHCb::VPChannelID> > channelIDs;
  channelIDs.reserve(400);
  // Loop over VP RawBanks
  for ( auto iterBank = tBanks.begin(); iterBank != tBanks.end(); ++iterBank ) {

    const unsigned int sensor = ( *iterBank )->sourceID();
    const unsigned int module = 1 + ( sensor / VP::NSensorsPerModule );
    if ( m_modulesToSkipMask[module-1] ){
      continue;
    }
    channelIDs.clear();
    // reset and then fill the super pixel buffer for a sensor
    // memset(m_sp_buffer,0,256*256*3*sizeof(unsigned char));
    // the memset is too slow here. the occupancy is so low that
    // resetting a few elements is *much* faster.
    const unsigned int nrc = pixel_idx.size();
    for ( unsigned int irc = 0; irc < nrc; ++irc ) {
      buffer[pixel_idx[irc]] = 0;
    }
    pixel_idx.clear();

    const float *ltg = m_ltg + 16 * sensor;

    const uint32_t* bank = ( *iterBank )->data();
    const uint32_t nsp   = *bank++;

    for ( unsigned int i = 0; i < nsp; ++i ) {
      const uint32_t sp_word = *bank++;
      uint8_t sp             = sp_word & 0xFFU;

      if ( 0 == sp ) continue; // protect against zero super pixels.

      const uint32_t sp_addr          = ( sp_word & 0x007FFF00U ) >> 8;
      const uint32_t sp_row           = sp_addr & 0x3FU;
      const uint32_t sp_col           = ( sp_addr >> 6 );

      // const uint32_t no_sp_neighbours = sp_word & 0x80000000U;
      // Skip the check of no_sp_neighbours here, we want to record all pixels here for MCLinking purposes [offline Mode, full pixels recording]

      //Record all pixels
      for ( uint32_t shift = 0; shift < 8; ++shift ) {
        const uint8_t pixel = sp & 1;
        if ( pixel ) {
          const uint32_t row = sp_row * 4 + shift % 4;
          const uint32_t col = sp_col * 2 + shift / 4;
          const uint32_t idx = ( col << 8 ) | row;
          //std::cout << (int)pixel << std::endl;
          buffer[idx]        = pixel;
          pixel_idx.push_back( idx );
        }
        sp = sp >> 1;
        if ( 0 == sp ) break;
      }
    } // loop over super pixels in raw bank

    // the sensor buffer is filled, perform the clustering on
    // clusters that span several super pixels.
    const unsigned int nidx = pixel_idx.size();
    for ( unsigned int irc = 0; irc < nidx; ++irc ) {

      const uint32_t idx  = pixel_idx[irc];
      const uint8_t pixel = buffer[idx];
      if ( 0 == pixel ) continue; // pixel is used in another cluster

      // 8-way row scan optimized seeded flood fill from here.
      stack.clear();

      // mark seed as used
      buffer[idx] = 0;

      // initialize sums
      unsigned int x = 0;
      unsigned int y = 0;
      unsigned int n = 0;

      // push seed on stack
      stack.push_back( idx );

      // as long as the stack is not exhausted:
      // - pop the stack and add popped pixel to cluster
      // - scan the row to left and right, adding set pixels
      //   to the cluster and push set pixels above and below
      //   on the stack (and delete both from the pixel buffer).
      while ( !stack.empty() ) {

        // pop pixel from stack and add it to cluster
        const uint32_t idx = stack.back();
        stack.pop_back();
        const uint32_t row = idx & 0xFFU;
        const uint32_t col = ( idx >> 8 ) & 0x3FFU;
        x += col;
        y += row;
        ++n;

	const uint32_t chip = col/CHIP_COLUMNS;
	LHCb::VPChannelID cid( sensor, chip, col %CHIP_COLUMNS, row);
	if( n== 1){
	  channelIDs.push_back( {{cid}});
	}else{
	  channelIDs.back().push_back( cid );
	}
        // check up and down
        uint32_t u_idx = idx + 1;
        if ( row < VP::NRows - 1 && buffer[u_idx] ) {
          buffer[u_idx] = 0;
          stack.push_back( u_idx );
        }
        uint32_t d_idx = idx - 1;
        if ( row > 0 && buffer[d_idx] ) {
          buffer[d_idx] = 0;
          stack.push_back( d_idx );
        }

        // scan row to the right
        for ( unsigned int c = col + 1; c < VP::NSensorColumns; ++c ) {
          const uint32_t nidx = ( c << 8 ) | row;
          // check up and down
          u_idx = nidx + 1;
          if ( row < VP::NRows - 1 && buffer[u_idx] ) {
            buffer[u_idx] = 0;
            stack.push_back( u_idx );
          }
          d_idx = nidx - 1;
          if ( row > 0 && buffer[d_idx] ) {
            buffer[d_idx] = 0;
            stack.push_back( d_idx );
          }
          // add set pixel to cluster or stop scanning
          if ( buffer[nidx] ) {
            buffer[nidx] = 0;
            x += c;
            y += row;
            ++n;
	    const uint32_t chip = c/ CHIP_COLUMNS;
	    LHCb::VPChannelID cid(sensor, chip, c %CHIP_COLUMNS, row);
	    channelIDs.back().push_back( cid);
          } else {
            break;
          }
        }

        // scan row to the left
        for ( int c = col - 1; c >= 0; --c ) {
          const uint32_t nidx = ( c << 8 ) | row;
          // check up and down
          u_idx = nidx + 1;
          if ( row < VP::NRows - 1 && buffer[u_idx] ) {
            buffer[u_idx] = 0;
            stack.push_back( u_idx );
          }
          d_idx = nidx - 1;
          if ( row > 0 && buffer[d_idx] ) {
            buffer[d_idx] = 0;
            stack.push_back( d_idx );
          }
          // add set pixel to cluster or stop scanning
          if ( buffer[nidx] ) {
            buffer[nidx] = 0;
            x += c;
            y += row;
            ++n;
	    const uint32_t chip = c/ CHIP_COLUMNS;
	    LHCb::VPChannelID cid(sensor, chip, c% CHIP_COLUMNS , row );
	    channelIDs.back().push_back(cid);
          } else {
            break;
          }
        }
      } // while the stack is not empty

      // we are done with this cluster, calculate
      // centroid pixel coordinate and fractions.
      if ( n <= m_maxClusterSize ) {
        // if the pixel is smaller than the max cluster size, store it for the tracking
        const unsigned int cx = x / n;
        const unsigned int cy = y / n;

        const uint32_t chip = cx / CHIP_COLUMNS;
        const uint32_t ccol = cx % CHIP_COLUMNS;

        // store target (3D point for tracking)
        LHCb::VPChannelID cid( sensor, chip, ccol, cy );

        const float fx = x / static_cast<float>(n) - cx;
        const float fy = y / static_cast<float>(n) - cy;
        const float local_x = m_local_x[cx] + fx * m_x_pitch[cx];
        const float local_y = (cy + 0.5 + fy) * m_pixel_size;

        //gx
        const float gx = (ltg[0] * local_x + ltg[1] * local_y + ltg[9]);
        //gy
        const float gy = (ltg[3] * local_x + ltg[4] * local_y + ltg[10]);
        //gz
        const float gz = (ltg[6] * local_x + ltg[7] * local_y + ltg[11]);
        pool.emplace_back( fx, fy , gx, gy, gz, cid, std::move( channelIDs.back() ));
        ++offsets[module];
      }
    } // loop over all potential seed pixels
  }// loop over all banks

  std::partial_sum( offsets.begin(), offsets.end(), offsets.begin() );
  //Do we actually need to sort the hits ? [ depends, if the offline clustering will be re-run and tracking will use those clusters , yes, otherwise no ]
  for ( size_t moduleID = 0; moduleID < VeloInfo::Numbers::NModules ; ++moduleID ) {
    //In even modules you fall in the branching at -180, 180 degrees, you want to do that continuos
    std::stable_sort( pool.begin() + offsets[moduleID], pool.begin() + offsets[moduleID +1],
		      []( const LHCb::VPFullCluster& a, const LHCb::VPFullCluster& b){
			return a.channelID() < b.channelID();
		      });
  }
  if( msgLevel(MSG::DEBUG)){
    for( auto & cl : pool ){
      info()<< "----"<<endmsg;
      info()<< cl << endmsg;
      info()<<" [fx,fy] "<< cl.xfraction() <<","<<cl.yfraction()<< endmsg;
      info()<<" [x,y,z] "<< cl.x()<<","<<cl.y()<<","<<cl.z() <<endmsg;
      info()<< "pixels"<< endmsg;
      for( auto & pixel : cl.pixels() ){
	info()<<"\t"<< pixel << endmsg;
      }
    }
  }
  if( msgLevel(MSG::DEBUG) ){
    info() << "N VPFullCluster produced :  "<<pool.size()  << endmsg;
  }
  return result;
}

