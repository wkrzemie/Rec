/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRPIXELTRACK_H
#define PRPIXELTRACK_H 1

// LHCb
#include "Event/StateVector.h"
#include "Kernel/STLExtensions.h"
// Local
#include "Event/VPLightCluster.h"
#include <xmmintrin.h>
#include <boost/container/static_vector.hpp>
#include <boost/align/aligned_allocator.hpp>
#include <vector>

namespace LHCb
{
  class State;
}

/** @class PrPixelTrack PrPixelTrack.h
 *  Working tracks, used inside the PrPixelTracking
 *
 *  @author Olivier Callot
 *  @date   2012-01-06
 */

template <typename T>
using aligned_vector = std::vector<T, boost::alignment::aligned_allocator<T, 64>>;

class PrPixelTrack final
{
public:
  explicit PrPixelTrack( size_t size )
      : m_hitsX( size, 0 ), m_hitsY( size, 0 ), m_hitsZ( size, 0 ), m_tx( 0. ), m_ty( 0. ), m_x0( 0. ), m_y0( 0. )
  {
  }


  /// Return the list of hits on this track.
  const aligned_vector<float>& hitsX() const { return m_hitsX; } ;
  const aligned_vector<float>& hitsY() const { return m_hitsY; } ;
  const aligned_vector<float>& hitsZ() const { return m_hitsZ; } ;
  //aligned_vector<LHCb::LHCbID>& hitsID() { return m_hitsID; }

  /// Chi2 / degrees-of-freedom of straight-line fit
  float chi2PerDoF() const
  {
    float ch = 0.;
    int nDoF = -4;
    for ( size_t i = 0; i < m_size; ++i ) {
      ch += chi2( m_hitsX[i], m_hitsY[i], m_hitsZ[i] );
      nDoF += 2;
    }
    return ch / nDoF;
  }
  /// Chi2 constribution from a given hit
  float chi2( float x, float y, float z ) const noexcept
  {
    // the errors are currently the same for all hits, otherwise this would have to be handled differently
    float err = defaultError;
    float dx = err * ( ( m_x0 + m_tx * z ) - x );
    float dy = err * ( ( m_y0 + m_ty * z ) - y );

    return dx * dx + dy * dy;
  }
  /// Position at given z from straight-line fit
  float xAtZ( const float z ) const { return m_x0 + m_tx * z; }
  float yAtZ( const float z ) const { return m_y0 + m_ty * z; }

  /// Create a track state vector: position and the slopes: dx/dz and dy/dz
  LHCb::StateVector state( const float z ) const
  {
    LHCb::StateVector temp;
    temp.setX( m_x0 + z * m_tx );
    temp.setY( m_y0 + z * m_ty );
    temp.setZ( z );
    temp.setTx( m_tx );
    temp.setTy( m_ty );
    temp.setQOverP( 0. ); // Q/P is Charge/Momentum ratio
    return temp;
  }

  // Calculate the z-pos. where the track passes closest to the beam
  float zBeam() const
  {
    if ( m_tx != 0.0 || m_ty != 0.0 ) {
      return -( m_x0 * m_tx + m_y0 * m_ty ) / ( m_tx * m_tx + m_ty * m_ty );
    } else {
      // edge case if the track is parallel to the beam
      // don't use m_hitsZ.end() beacuse m_hitsZ is not resized to m_size
      return std::accumulate( m_hitsZ.begin(), std::next( m_hitsZ.begin(), m_size ), 0.0f ) / m_size;
    }
  }

  Gaudi::TrackSymMatrix covariance( const float z ) const;

  /// Perform a straight-line fit.
  void fit();

  void fill( LHCb::span<const LHCb::VPLightCluster> clusters,
             LHCb::span<unsigned char> clusterIsUsed,
             LHCb::span<const size_t> hitbuffer )
  {
    m_size = hitbuffer.size();
    size_t cnt = 0;
    for ( size_t offset : hitbuffer ) {
      clusterIsUsed[offset] = 1;
      m_hitsX[cnt] = clusters[offset].x();
      m_hitsY[cnt] = clusters[offset].y();
      m_hitsZ[cnt] = clusters[offset].z();
      ++cnt;
    }
  }
  //Special call not flagging hits on track.
  void fill( LHCb::span<const LHCb::VPLightCluster> clusters,
             LHCb::span<const size_t> hitbuffer )
  {
    m_size = hitbuffer.size();
    size_t cnt = 0;
    for ( size_t offset : hitbuffer ) {
      m_hitsX[cnt] = clusters[offset].x();
      m_hitsY[cnt] = clusters[offset].y();
      m_hitsZ[cnt] = clusters[offset].z();
      ++cnt;
    }
  }
  void fill( LHCb::span<const LHCb::VPLightCluster> clusters,
             const size_t h0, const size_t h1, const size_t h2 )
  {
    m_size     = 3;
    m_hitsX[0] = clusters[h0].x();
    m_hitsY[0] = clusters[h0].y();
    m_hitsZ[0] = clusters[h0].z();
    m_hitsX[1] = clusters[h1].x();
    m_hitsY[1] = clusters[h1].y();
    m_hitsZ[1] = clusters[h1].z();
    m_hitsX[2] = clusters[h2].x();
    m_hitsY[2] = clusters[h2].y();
    m_hitsZ[2] = clusters[h2].z();
  }

  /// Fit with a K-filter with scattering. Return the chi2
  float fitKalman( LHCb::State& state, const int direction, const float noise2PerLayer ) const;

  // Number of hits assigned to the track
  unsigned int size( void ) const { return m_size; }

private:
  /// List of hits
  aligned_vector<float> m_hitsX;
  aligned_vector<float> m_hitsY;
  aligned_vector<float> m_hitsZ;
  /// Straight-line fit parameters
  float m_tx;
  float m_ty;
  float m_x0;
  float m_y0;
  size_t m_size;

  /// Sums for the x-slope fit
  __m128 v_sx, v_sxz, v_s0;
  /// Constants
  static const __m128 v_compValue, v_1s, v_2s, v_sign_mask;
  static constexpr float defaultError{62.98366573f}; //std::sqrt( 12.0 ) / ( 0.055 );

};

typedef std::vector<PrPixelTrack> PrPixelTracks; // vector of tracks

#endif // PRPIXELTRACK_H
