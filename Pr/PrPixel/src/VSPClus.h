/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VSPClus_H
#define VSPClus_H 1

#include <array>
#include <tuple>
#include <vector>
#include <bitset>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"
#include "Kernel/VPChannelID.h"
#include "Kernel/VPConstants.h"
#include "VPKernel/PixelModule.h"
#include "VPDet/DeVP.h"
#include "VPKernel/VeloPixelInfo.h"

//FIXME still need to handle nx and ny for the clusters

// Namespace for locations in TDS
namespace LHCb
{
  namespace VPClusterLocation
  {
    inline const std::string Offsets = "Raw/VP/LightClustersOffsets";
  }
}

struct SensorCache {
  // representing the transformed pitch vectors
  float xx, xy, yx, yy;
  // coordinates (xi, yi) of pixel in lower left corner of chip
  std::array<float, VeloInfo::Numbers::NChipCorners> ChipCorners;
  // sensor z position
  float z;
};

struct SPCache {
  std::array<float, 4> fxy;
  unsigned char pattern;
  unsigned char nx1;
  unsigned char nx2;
  unsigned char ny1;
  unsigned char ny2;
};

// 0 1 2 3
// 4 5 6 7

// First Version : check if it exist a pixel link between SP
#define linkH(l, r) (((l) & 0x88) && ((r) & 0x11))
#define linkDF(sw, ne) (((sw) & 0x08) && ((ne) & 0x10))
#define linkDB(nw, se) (((nw) & 0x80) && ((se) & 0x01))
#define linkV(b, t) (_linkNS[b][t])

// Second Version : always connect adjacent SP (faster, with a tiny drop in efficiency)
/*#define linkH(l, r)    (1)
#define linkDF(sw, ne) (1)
#define linkDB(nw, se) (1)
#define linkV(b, t)    (1)*/

class VSPClus
  : public Gaudi::Functional::MultiTransformer<std::tuple<std::vector<LHCb::VPLightCluster>,
                                                          std::array<unsigned, VeloInfo::Numbers::NOffsets>>(
          const LHCb::RawEvent& )>
{

public:
  /// Standard constructor
  VSPClus( const std::string& name, ISvcLocator* pSvcLocator );

  /// Algorithm initialization
  StatusCode initialize() override;

  /// Algorithm execution
  std::tuple<std::vector<LHCb::VPLightCluster>, std::array<unsigned,VeloInfo::Numbers::NOffsets >>
  operator()( const LHCb::RawEvent& ) const override;

private:
  /// Cache Super Pixel patterns for isolated Super Pixel clustering.
  void cacheSPPatterns();

  /// Recompute the geometry in case of change
  StatusCode rebuildGeometry();
  //
  /// Detector element
  DeVP* m_vp{nullptr};

  /// List of pointers to modules (which contain pointers to their hits)
  /// FIXME This is not thread safe on a change on geometry.
  /// FIXME These vectors should be stored as a derived condition
  std::vector<PixelModule*> m_modules;
  std::vector<PixelModule> m_module_pool;

  /// Indices of first and last module
  unsigned int m_firstModule;
  unsigned int m_lastModule;

  /// Maximum allowed cluster size (no effect when running on lite clusters).
  unsigned int m_maxClusterSize = 999999999;

  float m_ltg[16 * VP::NSensors]; // 16*208 = 16*number of sensors
  const double *m_local_x;
  const double *m_x_pitch;
  float m_pixel_size;

  // SP pattern buffers for clustering, cached once.
  // There are 256 patterns and there can be at most two
  // distinct clusters in an SP.
  std::array<SPCache, 256> m_SPCaches{};

  bool _linkNS[256][256];

  Gaudi::Property<std::vector<unsigned int>> m_modulesToSkip{ this, "ModulesToSkip",{}, "List of modules that should be skipped in decoding"};
  std::bitset<VP::NModules> m_modulesToSkipMask;

  mutable Gaudi::Accumulators::SummingCounter<> m_nbClustersCounter{ this, "Nb of Produced Clusters" };
};

#endif // VPClusSP_H

