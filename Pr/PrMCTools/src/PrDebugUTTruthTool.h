/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: PrDebugUTTruthTool.h,v 1.4 2008-12-04 09:05:07 cattanem Exp $
#ifndef PRDEBUGTTTRUTHTOOL_H
#define PRDEBUGTTTRUTHTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleTool.h"
#include "PrKernel/IPrDebugUTTool.h"            // Interface
#include "MCInterfaces/IIdealStateCreator.h"
class DeUTDetector;

/** @class PrDebugUTTruthTool PrDebugUTTruthTool.h
 *
 *  Class to extract MC information for Upgrade Tracking.
 *  Updated to have information for PrLongLivedTracking.
 *  @author Olivier Callot
 *  @date   2007-10-22
 *
 *  @author Adam Davis
 *  @date   2016-04-10
 *
 *  @2017-03-01: Christoph Hasse (adapt to future framework)
 */
class PrDebugUTTruthTool : public GaudiTupleTool, virtual public IPrDebugUTTool {
public:
  /// Standard constructor
  PrDebugUTTruthTool( const std::string& type,
                       const std::string& name,
                       const IInterface* parent);

  virtual ~PrDebugUTTruthTool( ); ///< Destructor

  StatusCode initialize() override; /// initialize

  void debugUTClusterOnTrack( const LHCb::Track* track,
                              const UT::Mut::Hits::const_iterator beginCoord,
                              const UT::Mut::Hits::const_iterator endCoord   ) override;

  void debugUTCluster( MsgStream& msg, const UT::Mut::Hit& hit ) override;

  bool isTrueHit( const LHCb::Track* track, const UT::Mut::Hit& hit) override;

  double fracGoodHits( const LHCb::Track* track, const UT::Mut::Hits& hits) override;

  bool isTrueTrack( const LHCb::Track* track, const UT::Mut::Hits& hits) override;

  void chi2Tuple( const double p, const double chi2, const unsigned int nHits) override;

  //added by AD 2/1/16 for efficiency vs step

  void initializeSteps(std::vector<std::string> steps) override;//initialize all steps in the process

  void recordStepInProcess(std::string step, bool result) override;//record the result of a step in the process

  void resetflags() override;//reset all flags

  void forceMCHits(UT::Mut::Hits& hits, LHCb::Track* track) override;//Special. Force only MC matched hits in the track.

  void tuneFisher( const LHCb::Track* seedTrack) override;

  void tuneDeltaP( const LHCb::Track* seedTrack, const double deltaP, const double momentum) override;

  void tuneFinalMVA( const LHCb::Track* seedTrack, const bool goodTrack, std::vector<double> vals) override;


  void getMagnetError( const LHCb::Track* seedTrack) override;


protected:

private:

  DeUTDetector* m_tracker;
  std::map<std::string,bool> m_flags;
  /// The ideal state creator, which makes a state out of an MCParticle
  IIdealStateCreator* m_idealStateCreator;

};
#endif // PRDEBUGUTTRUTHTOOL_H
