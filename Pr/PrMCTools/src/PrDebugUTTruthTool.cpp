/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/IRegistry.h"

#include "Linker/LinkedTo.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCParticle.h"
#include "Event/UTCluster.h"
#include "Event/Track.h"
#include "TfKernel/RecoFuncs.h"
#include "UTDet/DeUTDetector.h"

// local
#include "PrDebugUTTruthTool.h"


//-----------------------------------------------------------------------------
// Implementation file for class : PrDebugUTTruthTool
//
// 2016-04-10 : Adam Davis
// 2017-03-01: Christoph Hasse (adapt to future framework)
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( PrDebugUTTruthTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrDebugUTTruthTool::PrDebugUTTruthTool( const std::string& type,
                                        const std::string& name,
                                        const IInterface* parent )
  : GaudiTupleTool ( type, name , parent )
{
  declareInterface<IPrDebugUTTool>(this);

}


StatusCode PrDebugUTTruthTool::initialize()
{
  StatusCode sc = GaudiTupleTool::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);

  m_tracker = getDet<DeUTDetector>(DeUTDetLocation::UT);
  m_flags["ReconstructibleAsLong"] = false;//AD 2-1-16
  m_flags["ReconstructibleAsDown"] = false;//AD 2-1-16

  m_idealStateCreator = tool<IIdealStateCreator>("IdealStateCreator", this);

  return sc;
}

//=============================================================================
// Destructor
//=============================================================================
PrDebugUTTruthTool::~PrDebugUTTruthTool() {}
//=========================================================================
//  Print the true TT clusters associated to the specified track
//=========================================================================
void PrDebugUTTruthTool::debugUTClusterOnTrack (  const LHCb::Track* track,
                                                   const UT::Mut::Hits::const_iterator beginCoord,
                                                   const UT::Mut::Hits::const_iterator endCoord   ) {
  //== Get the truth on this track
  std::string containerName = track->parent()->registry()->identifier();
  std::string linkerName = "Link/"+containerName;
  if ( "/Event/" == containerName.substr(0,7) ) {
    linkerName = "Link/" + containerName.substr(7);
  }
  if ( exist<LHCb::LinksByKey>( linkerName ) ) {
    LinkedTo<LHCb::MCParticle, LHCb::Track> trLink( evtSvc(), msgSvc(), containerName );
    LinkedTo<LHCb::MCParticle> itLink( evtSvc(), msgSvc(), LHCb::UTClusterLocation::UTClusters );

    LHCb::MCParticle* part = trLink.first( track->key() );
    while ( 0 != part ) {
      double momentum = part->momentum().R();
      info() << format( " MC Key %4d PID %6d  P %8.3f GeV tx %9.5f ty %9.5f",
                        part->key(), part->particleID().pid(), momentum / Gaudi::Units::GeV,
                        part->momentum().x()/ part->momentum().z(),
                        part->momentum().y()/ part->momentum().z() ) << endmsg;
      for ( UT::Mut::Hits::const_iterator itH = beginCoord; endCoord != itH; ++itH ){
        const UT::Mut::Hit& hit = *itH;
        LHCb::UTChannelID id = hit.HitPtr->lhcbID().utID();
        bool found = false;
        for( auto kk = 0; hit.HitPtr->size() > kk; ++kk ) {
          LHCb::MCParticle* clusPart = 0;
          if ( id != LHCb::UTChannelID(0)) clusPart = itLink.first( id );
          while ( 0 != clusPart ) {
            if ( clusPart->key() == part->key() ) found = true;
            clusPart = itLink.next();
          }
          id = m_tracker->nextRight(id);
        }
        if ( found ) {
          double xCoord = hit.x ;
          info() << "      TT Clus "
                 << format( "(S%1d,L%1d,R%2d,S%2d,S%3d) x%7.1f High %1d",
                            id.station(), id.layer(), id.detRegion(),
                            id.sector(), id.strip(), xCoord,
                            (*itH).HitPtr->highThreshold() ) << endmsg;
        }
      }
      part = trLink.next();
    }
  }
}

//=========================================================================
//  Print the MC keys associated to this cluster
//=========================================================================
void PrDebugUTTruthTool::debugUTCluster( MsgStream& msg, const UT::Mut::Hit& hit ) {

  LinkedTo<LHCb::MCParticle> itLink( evtSvc(), msgSvc(), LHCb::UTClusterLocation::UTClusters );
  std::string value = "";

  LHCb::UTChannelID id = hit.HitPtr->lhcbID().utID();
  int lastKey = -1;
  for( auto kk = 0; hit.HitPtr->size() > kk; ++kk ) {
    LHCb::MCParticle* part = 0;
    if (id != LHCb::UTChannelID(0)) part = itLink.first( id );
    while ( 0 != part ) {
      if ( lastKey != part->key() ) {
        lastKey = part->key();
        msg << " " << lastKey;
      }
      part = itLink.next();
    }
    id = m_tracker->nextRight(id);
  }
}

//=============================================================================
//  Does this hit belong to the MCParticle matched to the track?
//=========================================================================
bool PrDebugUTTruthTool::isTrueHit( const LHCb::Track* track, const UT::Mut::Hit& hit){

  LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink ( evtSvc(), msgSvc(),LHCb::TrackLocation::Seed);
  LinkedTo<LHCb::MCParticle, LHCb::UTCluster> myClusterLink ( evtSvc(), msgSvc(), LHCb::UTClusterLocation::UTClusters );

  const LHCb::MCParticle* mcSeedPart = mySeedLink.first( track->key() );

  if( mcSeedPart == nullptr ) return false;

  bool found = false;
  LHCb::MCParticle* mcPart = myClusterLink.first( hit.HitPtr->lhcbID().utID() );
  while( mcPart != nullptr ){
    if( mcPart == mcSeedPart) found = true;
    mcPart = myClusterLink.next();
  }

  return found;

}
//=============================================================================
//  Fraction of 'good' (ie matched) hits on this track
//=============================================================================
double PrDebugUTTruthTool::fracGoodHits( const LHCb::Track* track, const UT::Mut::Hits& hits){

  LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink ( evtSvc(), msgSvc(),LHCb::TrackLocation::Seed);
  LinkedTo<LHCb::MCParticle, LHCb::UTCluster> myClusterLink ( evtSvc(), msgSvc(), LHCb::UTClusterLocation::UTClusters );

  const LHCb::MCParticle* mcSeedPart = mySeedLink.first( track->key() );

  if( mcSeedPart == nullptr ) return 0.0;

  unsigned int nTrueHits = 0;
  unsigned int nBadHits = 0;
  for( const UT::Mut::Hit& hit : hits ){

    bool found = false;

    LHCb::MCParticle* mcPart = myClusterLink.first( hit.HitPtr->lhcbID().utID() );
    while( mcPart != nullptr){
      if( mcPart == mcSeedPart ) found = true;
      mcPart = myClusterLink.next();
    }

    if(found){
      ++nTrueHits;
    }else{
      ++nBadHits;
    }

  }

  info () << "good Hits: " << nTrueHits << " badHits " << nBadHits << " fraction: "
          <<  (double)nTrueHits / ( (double)nTrueHits + (double)nBadHits ) << endmsg;

  return (double)nTrueHits / ( (double)nTrueHits + (double)nBadHits );

}
//=============================================================================
//  Is this a 'good' track (according to the TrackAssociator) definition?
//=============================================================================
bool PrDebugUTTruthTool::isTrueTrack( const LHCb::Track* track,
                                      const UT::Mut::Hits& hits){

  LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink ( evtSvc(), msgSvc(),LHCb::TrackLocation::Seed);
  LinkedTo<LHCb::MCParticle, LHCb::UTCluster> myClusterLink ( evtSvc(), msgSvc(), LHCb::UTClusterLocation::UTClusters );

  const LHCb::MCParticle* mcSeedPart = mySeedLink.first( track->key() );

  if( mcSeedPart == nullptr ) return false;

  unsigned int nTrueHits = 0;
  unsigned int nBadHits = 0;
  for(auto& hit : hits){

    bool found = isTrueHit( track, hit);

    if(found){
      ++nTrueHits;
    }else{
      ++nBadHits;
    }
  }

  const unsigned int nTotHits = nTrueHits + nBadHits;

  return nTrueHits > nTotHits - 2;

}

//=============================================================================
//  Put chi2, number of hits and momentum in a tuple
//=========================================================================
void PrDebugUTTruthTool::chi2Tuple( const double p, const double chi2, const unsigned int nHits){

  Tuple tuple = nTuple( "chi2Tuple", "chi2Tuple" );
  tuple->column("p", p );
  tuple->column("nHits", nHits );
  tuple->column("chi2", chi2 );
  //add the eff vs step from this tool.
  for(auto flag : m_flags){//AD
    tuple->column(flag.first, flag.second);//AD
  }//AD
  tuple->write();

}

//AD 2-1-16
//=========================================================================
//  Initialize the steps
//=========================================================================
void PrDebugUTTruthTool::initializeSteps(std::vector<std::string> steps){
  for(auto step : steps){//AD, loop over steps to do, and initialize to false. This adds to the map m_flags.
    m_flags[step] = false;
  }
  return;
}
//=========================================================================
//  Record the step
//=========================================================================
void PrDebugUTTruthTool::recordStepInProcess(std::string step,bool result){
  if(!m_flags[step]){
    m_flags[step]|=result;
  }
  //don't change things if we already have the right answer.
  debug()<<"Recorded for step "<<step<<" result"<<result<<endmsg;
}
//=========================================================================
//  Reset the flags
//=========================================================================
void PrDebugUTTruthTool::resetflags(){
  for(auto flag : m_flags){//note, this resets also the Reconstructible flags.
    flag.second=false;//I think this is right
  }
  return;
}
//=========================================================================
//  Force, that only hits matched to the correct MCParticle are used
//=========================================================================
void PrDebugUTTruthTool::forceMCHits(UT::Mut::Hits& hits, LHCb::Track* track){
  //modify the input containter to return only the hits which are MC tached
  UT::Mut::Hits tmp;
  for(auto hit: hits){
    if(!isTrueHit( track, hit))continue;
    tmp.push_back(hit);
  }
  hits.clear();
  for(auto hit: tmp){hits.push_back(hit);}
  return;
}
//=============================================================================
//  Put chi2, momentum, pt and number of hits in a tuple (to tune the Fisher in PrLongLivedTracking)
//=========================================================================
void PrDebugUTTruthTool::tuneFisher( const LHCb::Track* seedTrack){

  const MCTrackInfo trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );
  LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink ( evtSvc(), msgSvc(),LHCb::TrackLocation::Seed);

  bool goodTrack = true;

  const LHCb::MCParticle* mcSeedPart = mySeedLink.first( seedTrack->key() );
  if( mcSeedPart == nullptr ){
    goodTrack = false;
  }else{
    // -- We ask for downstream tracks that are not long reconstructible
    const bool isDown  = trackInfo.hasT( mcSeedPart ) && trackInfo.hasTT( mcSeedPart ) && !trackInfo.hasVelo( mcSeedPart );
    if( !isDown ) goodTrack = false;
    if( std::abs(mcSeedPart->particleID().pid()) == 11 ) goodTrack = false;
  }

  // -- this assumes a X-U-V-X  X-U-V-X  X-U-V-X geometry...
  const int nXLayers = std::count_if( seedTrack->lhcbIDs().begin(),
                                      seedTrack->lhcbIDs().end(),
                                      [](LHCb::LHCbID id){
                                  const int layer = id.ftID().layer();
                                  if( layer == 0 ||
                                      layer == 3 ||
                                      layer == 4 ||
                                      layer == 7 ||
                                      layer == 8 ||
                                      layer == 11 ){
                                    return true;
                                  }else{
                                    return false;
                                  }
                                      });



  const double lhcbIDSizeD = static_cast<double>(seedTrack->lhcbIDs().size());
  const double nXLayersD   = static_cast<double>(nXLayers);
  const double chi2        = seedTrack->chi2PerDoF();
  const double momentum    = seedTrack->p();
  const double pt          = seedTrack->pt();



  Tuple tuple = nTuple( "LLTFisherTuple", "LLTFisherTuple" );
  tuple->column("p",         momentum );
  tuple->column("pt",        pt );
  tuple->column("nHits",     lhcbIDSizeD );
  tuple->column("nXLayers",  nXLayersD );
  tuple->column("chi2",      chi2 );
  tuple->column("goodTrack", goodTrack );


  tuple->write();

}

//=============================================================================
//  Put chi2, momentum, pt and number of hits in a tuple (to tune the Fisher in PrLongLivedTracking)
//=========================================================================
void PrDebugUTTruthTool::tuneDeltaP( const LHCb::Track* seedTrack, const double deltaP, const double momentum){

  const MCTrackInfo trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );
  LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink ( evtSvc(), msgSvc(),LHCb::TrackLocation::Seed);

  bool goodTrack = true;

  const LHCb::MCParticle* mcSeedPart = mySeedLink.first( seedTrack->key() );
  if( mcSeedPart == nullptr ){
    goodTrack = false;
  }else{
    // -- We ask for downstream tracks that are not long reconstructible
    const bool isDown  = trackInfo.hasT( mcSeedPart ) && trackInfo.hasTT( mcSeedPart ) && !trackInfo.hasVelo( mcSeedPart );
    if( !isDown ) goodTrack = false;
    if( std::abs(mcSeedPart->particleID().pid()) == 11 ) goodTrack = false;
  }


  LHCb::State state = seedTrack->closestState( 10000. );
  double momFromState = std::abs(1./state.qOverP());
  double momFromMC = -1000;
  if( mcSeedPart ) momFromMC = mcSeedPart->p();


  Tuple tuple = nTuple( "LLTDeltaPTuple", "LLTDeltaPTuple" );
  tuple->column("p",         momentum );
  tuple->column("pFromState", momFromState );
  tuple->column("pFromMC",    momFromMC );
  tuple->column("deltaP",    deltaP );
  tuple->column("goodTrack", goodTrack );


  tuple->write();

}
//=============================================================================
//  Put chi2, momentum, pt and number of hits in a tuple (to tune the MVA in PrLongLivedTracking)
//=========================================================================
void PrDebugUTTruthTool::tuneFinalMVA( const LHCb::Track* seedTrack, bool goodTrack, std::vector<double> vals){

  const MCTrackInfo trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );
  LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink ( evtSvc(), msgSvc(),LHCb::TrackLocation::Seed);

  bool goodSeedTrack = true;
  bool isLong = false;

  const LHCb::MCParticle* mcSeedPart = mySeedLink.first( seedTrack->key() );
  if( mcSeedPart == nullptr ){
    goodSeedTrack = false;
  }else{
    // -- We ask for downstream tracks that are not long reconstructible
    const bool isDown  = trackInfo.hasT( mcSeedPart ) && trackInfo.hasTT( mcSeedPart );
    isLong = isDown &&  trackInfo.hasVelo( mcSeedPart );
    if( !isDown ) goodSeedTrack = false;
    if( std::abs(mcSeedPart->particleID().pid()) == 11 ) goodSeedTrack = false;
  }

  if(isLong) return;

  // -- this might not be needed
  bool trueTrack = goodTrack && goodSeedTrack;

  /*
  track.chi2(),
  track.track()->chi2PerDoF()
  std::abs(track.momentum()),
  pt,
  deltaP,
  deviation,
  initialChi2,
  static_cast<double>(track.hits().size()),
  static_cast<double>(nHighThres)
  */


  Tuple tuple = nTuple( "LLTMVATuple", "LLTMVATuple" );
  tuple->column("chi2",    vals[0] );
  tuple->column("seedChi2",vals[1] );
  tuple->column("p",vals[2] );
  tuple->column("pt", vals[3] );
  tuple->column("deltaP",    vals[4] );
  tuple->column("deviation", vals[5] );
  tuple->column("initialChi2", vals[6]);
  tuple->column("nHits",    vals[7] );
  tuple->column("highThresHits", vals[8] );

  tuple->column("goodTrack", trueTrack );

  tuple->write();

}
//=============================================================================
// Try to get the uncertainty on the magnet x and y position
//=========================================================================
void PrDebugUTTruthTool::getMagnetError( const LHCb::Track* seedTrack){

  const MCTrackInfo trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );
  LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink ( evtSvc(), msgSvc(),LHCb::TrackLocation::Seed);

  bool goodSeedTrack = true;

  const LHCb::MCParticle* mcSeedPart = mySeedLink.first( seedTrack->key() );
  if( mcSeedPart == nullptr ){
    goodSeedTrack = false;
  }else{
    // -- We ask for downstream tracks that are not long reconstructible
    const bool isDown  = trackInfo.hasT( mcSeedPart ) && trackInfo.hasTT( mcSeedPart ) && !trackInfo.hasVelo( mcSeedPart );
    if( !isDown ) goodSeedTrack = false;
    if( std::abs(mcSeedPart->particleID().pid()) == 11 ) goodSeedTrack = false;
  }

  if( !goodSeedTrack ) return;

  // -- the parameters, from PrKsFitParams
  std::array<double, 7> magnetParams        = { 5379.88, -2143.93, 366.124, 119074, -0.0100333, -0.146055, 1260.96 };
  std::array<double, 2> yParams             = {5.,2000.};

  LHCb::State seedState = seedTrack->closestState( 10000. );
  double zState = seedState.z();
  const double zMagnet =
    magnetParams[0] +
    magnetParams[1] * seedState.ty() * seedState.ty() +
    magnetParams[2] * seedState.tx() * seedState.tx() +
    magnetParams[3] /seedState.p() + ///this is where the old one stopped.
    magnetParams[4] * std::abs( seedState.x() ) +
    magnetParams[5] * std::abs( seedState.y() ) +
    magnetParams[6] * std::abs( seedState.ty() );

  LHCb::State idState;
  m_idealStateCreator->createState( mcSeedPart, zState, idState);

  const double dz       = zMagnet - seedState.z();
  const double xMagnet  = seedState.x() + dz * seedState.tx();
  const double xMagnetIdeal = idState.x() + dz * idState.tx();

  const double slopeX       = xMagnet / zMagnet;
  const double slopeXIdeal  = xMagnetIdeal / zMagnet;
  const double dSlope       = std::abs( slopeX - seedState.tx() );
  const double dSlope2      = dSlope*dSlope;
  const double dSlopeIdeal  = std::abs( slopeXIdeal - idState.tx() );
  const double dSlope2Ideal = dSlopeIdeal*dSlopeIdeal;
  const double by           = seedState.y() / ( seedState.z() +
                                                ( yParams[0] * std::abs(seedState.ty()) * zMagnet + yParams[1] )* dSlope2  );
  const double byIdeal      = idState.y() / ( idState.z()+
                                              (yParams[0] * std::abs(idState.ty())*zMagnet + yParams[1]) * dSlope2Ideal);
  const double yMagnet      = seedState.y() + dz * by - yParams[1] * by * dSlope2;
  const double yMagnetIdeal = idState.y()   + dz * byIdeal - yParams[1] * byIdeal * dSlope2Ideal;


  Tuple tuple = nTuple( "LLTMagErrorTuple", "LLTMagErrorTuple" );
  tuple->column("zMag",   zMagnet );
  tuple->column("xIdeal", xMagnetIdeal );
  tuple->column("yIdeal", yMagnetIdeal );
  tuple->column("xReco",  xMagnet );
  tuple->column("yReco",  yMagnet );
  tuple->column("dSlope", dSlope );

  tuple->write();





}

