/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "Event/MCParticle.h"

// includes from DigiEvent
#include "Event/VeloCluster.h"
#include "Event/VeloLiteCluster.h"
#include "Event/VPLightCluster.h"
#include "Event/VPFullCluster.h"
#include "Event/STCluster.h"
#include "Event/UTCluster.h"
#include "Event/OTTime.h"
#include "Event/FTLiteCluster.h"
#include "PrKernel/UTHitHandler.h"

// Range V3
#include <range/v3/view/transform.hpp>
#include <range/v3/view/slice.hpp>

// boost
#include <boost/numeric/conversion/cast.hpp>

// local
#include "PrLHCbID2MCParticle.h"

namespace {
  static const std::vector<std::string> s_stClusterNames = { LHCb::STClusterLocation::TTClusters,
                                                             LHCb::STClusterLocation::ITClusters };
  static const std::vector<std::string> s_stLiteClusterNames = { LHCb::STLiteClusterLocation::TTClusters,
                                                                 LHCb::STLiteClusterLocation::ITClusters };
  using namespace ranges;
}
//-----------------------------------------------------------------------------
// Implementation file for class : PrLHCbID2MCParticle
//
// 2010-03-22 Victor Coco
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( PrLHCbID2MCParticle )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  PrLHCbID2MCParticle::PrLHCbID2MCParticle( const std::string& name,
                                              ISvcLocator* pSvcLocator)
    : GaudiAlgorithm ( name , pSvcLocator ),
      m_otHitCreator("Tf::OTHitCreator/OTHitCreator")
{
  declareProperty( "TargetName",  m_targetName = "Pr/LHCbID" );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PrLHCbID2MCParticle::execute() {

  if (msgLevel(MSG::DEBUG)) debug() << "==> Execute" << endmsg;

  LinkerWithKey<LHCb::MCParticle> lhcbLink( evtSvc(), msgSvc(), m_targetName );

  //== Velo
  auto makeID = [](auto id) -> unsigned int { return id.channelID(); };

  std::vector<unsigned int> ids;
  ids.reserve(10);

  const auto* v_clusters = getIfExists<LHCb::VeloClusters>(LHCb::VeloClusterLocation::Default);
  if ( v_clusters ) {
    LinkedTo<LHCb::MCParticle> veloLink( evtSvc(), msgSvc(), LHCb::VeloClusterLocation::Default );
    for(auto clus : *v_clusters) {
       auto channels = clus->channels();
       // pseudoSize 3 means 3 or more. Are maximum three links wanted?
       linkAll(veloLink, lhcbLink, clus->channelID(),
               (channels | view::transform(makeID))[{0, clus->pseudoSize()}]);
    }
  } else {
      //FIXME: this does not work in the future branch!!!!
    const auto* vl_clusters = getIfExists<LHCb::VeloLiteCluster::VeloLiteClusters>(LHCb::VeloLiteClusterLocation::Default);
    if ( vl_clusters ) {
      LinkedTo<LHCb::MCParticle> veloLink( evtSvc(), msgSvc(), LHCb::VeloClusterLocation::Default );
      for(const auto& clus : *vl_clusters) {
         linkAll( veloLink, lhcbLink, clus.channelID(), {clus.channelID().channelID()});
      }
    }
  }

  //== VP
  const auto* vp_fullclusters = getIfExists< std::vector<LHCb::VPFullCluster> >(LHCb::VPFullClusterLocation::Default);
  if ( 0 != vp_fullclusters ) {
    LinkedTo<LHCb::MCParticle> vpLink(evtSvc(), msgSvc(), LHCb::VPFullClusterLocation::Default);
    for (const auto& clus : *vp_fullclusters) {
      linkAll(vpLink, lhcbLink, clus.channelID(), {clus.channelID().channelID()});
    }
  }else{
    info()<<"MCLinking VP done with VPLightClusters, must be done with VPFullClusters "<<endmsg;
    const auto* vp_clusters = getIfExists<std::vector<LHCb::VPLightCluster>>(LHCb::VPClusterLocation::Light);
    if ( vp_clusters ) {
      LinkedTo<LHCb::MCParticle> vpLink(evtSvc(), msgSvc(), LHCb::VPClusterLocation::Light);
      for (const auto& clus : *vp_clusters) {
	linkAll(vpLink, lhcbLink, clus.channelID(), {clus.channelID().channelID()});
      }
    }
  }

  //== TT, IT

  for ( unsigned int kk = 0; s_stClusterNames.size() > kk; ++kk ) {
    std::string clusterName = s_stClusterNames[kk];
    std::string liteName    = s_stLiteClusterNames[kk];
    const auto* cont = getIfExists<LHCb::STCluster::Container>( clusterName );
    if ( cont ) {
      LinkedTo<LHCb::MCParticle> ttLink( evtSvc(), msgSvc(), clusterName );
      for(  const auto& clus : *cont ) {
        auto channels = clus->channels();
        linkAll(ttLink, lhcbLink, clus->channelID(),
                (channels | view::transform(makeID))[{0, clus->pseudoSize()}]);
      }
    } else {
      //FIXME: this does not work in the future branch!!!!
      const auto* clusters = getIfExists<LHCb::STLiteCluster::STLiteClusters>( liteName );
      if ( clusters ) {
        LinkedTo<LHCb::MCParticle> ttLink( evtSvc(), msgSvc(), clusterName );
        for(const auto& clus : *clusters) {
          linkAll( ttLink, lhcbLink, clus.channelID(),
                   {boost::numeric_cast<unsigned int>(clus.channelID().channelID())});
        }
      }
    }
  }

  //== UT

  const auto* hitHandler = getIfExists<UT::HitHandler>( UT::Info::HitLocation );
  if ( hitHandler ) {
    LinkedTo<LHCb::MCParticle> ttLink( evtSvc(), msgSvc(), LHCb::UTClusterLocation::UTClusters );
    for (unsigned int station = 1; station < 3; station++) {
      for (unsigned int layer = 1; layer < 3; layer++) {
        for (unsigned int region = 1; region < 4; region++) {
          for (unsigned int sector = 1; sector < 99; sector++) {
            for(const auto& hit : hitHandler->hits(station, layer, region, sector)) {
              linkAll( ttLink, lhcbLink, hit.chanID(),
                       {boost::numeric_cast<unsigned int>(hit.chanID().channelID())});
            }
          }
        }
      }
    }
  } else {
    throw std::string("PrLHCbID2MCParticle : could not UTHitHandler");
  }

  //== OT coordinates
  if ( exist<LHCb::LinksByKey>( "Link/" + LHCb::OTTimeLocation::Default ) ) {
    LinkedTo<LHCb::MCParticle> otLink( evtSvc(), msgSvc(),LHCb::OTTimeLocation::Default );
    if ( !otLink.notFound() ) {
      if ( !m_otReady ) {
        m_otHitCreator.retrieve().ignore();
        m_otReady = true;
      }
      for (const auto& hit: m_otHitCreator->hits() ) {
        linkAll( otLink, lhcbLink, hit->lhcbID(), {hit->lhcbID().otID()} );
      }
    }
  }

  //== FT
  const auto& ft_clusters = m_clusters.get();
  if ( ft_clusters ) {
    LinkedTo<LHCb::MCParticle> ftLink( evtSvc(), msgSvc(),LHCb::FTLiteClusterLocation::Default );
    if ( !ftLink.notFound() ) {
      for(const auto& cluster : ft_clusters->range()) {
        linkAll( ftLink, lhcbLink, cluster.channelID(), {cluster.channelID()});
      }
    }
  }
  return StatusCode::SUCCESS;
}

//=========================================================================
//  link all particles to the specified id
//=========================================================================
void PrLHCbID2MCParticle::linkAll( LinkedTo<LHCb::MCParticle>& ilink, LinkerWithKey<LHCb::MCParticle>& olink,
                                   LHCb::LHCbID id, const std::vector<unsigned int>& ids ) {
  std::vector<const LHCb::MCParticle*> partList; partList.reserve(ids.size());
  for (auto subID : ids) {
    for (const LHCb::MCParticle* part = ilink.first(subID);
         part != nullptr; part = ilink.next()) {
      partList.push_back(part);
    }
  }
  // SORTING: 
  // THe access pattern in the ilink is determined by the sorting of the underlying Clusters
  // For the Velo we do offline Clustering storing vector< VPFullCluster > which can be in a different order w.r.t the tracking clusters
  // The underlying linker of lhcbID 2 MCParticle gets filled in a different order using weights.
  // For equal weights the access patters is first in -> first out (from the linker )
  // We need to sort this parList again to have a stable PrTrackAssociator algorithm using the linker produced in this algorithm
  std::sort( partList.begin(), partList.end(), [](const LHCb::MCParticle * a, const LHCb::MCParticle * b  ){ 
      if( a->key() != b->key() ){
	return a->key() < b->key(); 
      }else{
	return a->pt() > b->pt();
      }
    });
  //remove any possible duplicates
  partList.erase( std::unique(partList.begin(),partList.end()),
                    partList.end() );
  if ( msgLevel(MSG::DEBUG) ) info() << format( "For ID %8x  MC: ", id.lhcbID() );
  for ( const auto& part : partList ){
    if ( msgLevel(MSG::DEBUG) ) info() << part->key() << " ";
    olink.link( id.lhcbID(), part );
  }
  if ( msgLevel(MSG::DEBUG) ) info() << endmsg;
}
