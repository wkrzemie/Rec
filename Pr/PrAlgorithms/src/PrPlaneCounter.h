/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRPLANECOUNTER_H
#define PRPLANECOUNTER_H 1

// Include files
#include "PrKernel/PrHit.h"

/** @class PrPlaneCounter PrPlaneCounter.h
 *  Small class to count how many different planes are in a list and how many planes with a single hit fired
 *
 *  @author Olivier Callot
 *  @date   2012-03-23
 *  @author Michel De Cian
 *  @date   2014-03-12 Added number of planes which only have a single hit
 *  @author Thomas Nikodem
 *  @date   2016-03-04
 *
 */

class PrPlaneCounter final {
public:

  /// Standard constructor
  PrPlaneCounter( )
    : m_nbDifferent( 0 ),
      m_planeList( {{0,0,0,0,0,0,0,0,0,0,0,0}} )
  {
  }

  /** Update values for additional hit
   *  @param hit Hit to be added
   *  @return int number of different planes
   */
  inline void addHit(const ModPrHit& hit) {
    m_nbDifferent += (int)((m_planeList[hit.planeCode] += 1 ) == 1) ;
  }
  inline void addHit(const PrHit& hit) {
    m_nbDifferent += (int)((m_planeList[hit.planeCode()] += 1 ) == 1) ;
  }

  /** Update values for removed hit
   *  @param hit Hit to be removed
   *  @return int number of different planes
   */
  inline void removeHit(const ModPrHit& hit) {
    m_nbDifferent -= ((int)((m_planeList[hit.planeCode] -= 1 ) == 0)) ;
  }
  inline void removeHit(const PrHit& hit) {
    m_nbDifferent -= ((int)((m_planeList[hit.planeCode()] -= 1 ) == 0)) ;
  }

  /** Set values (fired planes, single planes, different planes) for a given range of iterators
   *  @brief Set values for a given range of iterators
   *  @param itBeg First iterator, begin of range
   *  @param itEnd Last iterator, end of range
   */
  void set( PrHits::const_iterator it, PrHits::const_iterator itEnd)  {
    for (; itEnd != it; ++it)
      addHit(**it);
  }
  void set(const PrHits& hits){
    for(const auto& hit : hits)
      addHit(*hit);
  }

  /// returns number of different planes
  inline unsigned int nbDifferent() const { return m_nbDifferent; }

  /// returns number of hits in specified plane
  inline int nbInPlane( const int plane ) const { return m_planeList[plane]; }
  inline int nbInPlane( const ModPrHit& hit ) const { return m_planeList[hit.planeCode]; }

  /// returns number of single planes
  //TODO make this smarter?
  unsigned int nbSingle() const {
    unsigned int nbDouble = 0;
    for(int i=0;i<12;i++)
      nbDouble += (int) (m_planeList[i]>1);

    return m_nbDifferent - nbDouble;
  }

  /// clear list with hits in planes and number of different planes / single-hit planes
  void clear(){
     m_nbDifferent = 0;

     for(int i=0;i<12;i++)
       m_planeList[i]=0; //do NOT alloc new mem
  }


private:

  /// array for: number of different plane (0) and number of planes with single hit (1)
  unsigned int m_nbDifferent;
  std::array<int,12> m_planeList;
};

#endif // PRPLANECOUNTER_H
