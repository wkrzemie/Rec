/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRLONGLIVEDTRACKING_H
#define PRLONGLIVEDTRACKING_H 1

// Include files
#include <string>
#include <vector>
#include "GaudiKernel/Point3DTypes.h"
// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "GaudiAlg/ISequencerTimerTool.h"

#include "TfKernel/UTStationHitManager.h"
#include "PrKernel/UTHit.h"
#include "Event/Track.h"

#include "Event/UTCluster.h"

#include "PrKernel/UTHitHandler.h"
#include "PrKernel/UTHitInfo.h"
#include "PrKernel/UTHit.h"

// forward declarations
class IPrDebugUTTool;
class DeUTDetector;
class PrDownTrack;
class IClassifierReaderForLLT;

/** @class PrLongLivedTracking PrLongLiveTracking.h
 *  Algorithm to reconstruct tracks with seed and UT, for daughters of long lived particles like Kshort, Lambda, ...
 *  Quick-and-dirty port from PatLongLivedTracking, more tuning needed
 *
 * Parameters:
 * - InputLocation: Input location of seed tracks
 * - OutputLocation: Output location of downstream tracks.
 * - ForwardLocation: Location of forward tracks, for hit rejection
 * - MatchLocation: Location of match tracks, for seed rejection
 * - XPredTolConst: x-window for preselection is XPredTolConst/p + XPredTolOffset
 * - XPredTolOffset: x-window for preselection is XPredTolConst/p + XPredTolOffset
 * - TolMatchConst: x-window for matching x hits is TolMatchConst/p + TolMatchOffset
 * - TolMatchOffset: x-window for matching x hits is TolMatchConst/p + TolMatchOffset
 * - MaxWindowSize: maximum window size for matching x hits
 * - TolUConst: window for u hits is TolUConst/p + TolUOffset
 * - TolUOffset: window for u hits is TolUConst/p + TolUOffset
 * - TolVConst: window for v hits is TolVConst/p + TolVOffset
 * - TolVOffset: window for v hits is TolVConst/p + TolVOffset
 * - MaxChi2: Maximum chi2 for tracks with at least 4 hits
 * - MaxChi2ThreeHits: Maximum chi2 for tracks with 3 hits
 * - MinPt: Minimum pT of the track
 * - MinMomentum: Minimum momentum of the track
 * - MinUTx: half-width  of beampipe rectangle
 * - MinUTy: half-height of of beampipe rectangle
 * - ZMagnetParams: Parameters to determine the z-position of the magnet point. Tune with PrKsParams.
 * - MomentumParams: Parameters to determine the momentum. Tune with PrKsParams.
 * - YParams: Parameters to determine the bending in y.  Tune with PrKsParams.
 * - ZUT: z-position of middle of UT.
 * - ZUTa: z-position of first UT station
 * - MaxDeltaPConst: Window for deltaP is: MaxDeltaPConst/p + MaxDeltaPOffset
 * - MaxDeltaPOffset: Window for deltaP is: MaxDeltaPConst/p + MaxDeltaPOffset
 * - XCorrectionConst: Correction for x-position of search window in preselection is XCorrectionConst/p + XCorrestionOffset
 * - XCorrectionOffset: Correction for x-position of search window in preselection is XCorrectionConst/p + XCorrestionOffset
 * - MaxXTracks: Maximum number of x-tracklets to process further
 * - MaxChi2DistXTracks: Maximum chi2 difference to x-tracklet with best chi2
 * - MaxXUTracks:  Maximum number of xu-tracklets to process further
 * - MinChi2XProjection: Maximum chi2 of x-tracklet
 * - OverlapTol: Tolerance for adding overlap hits
 * - YTol: YTolerance for adding / removing hits.
 * - SeedCut: Cut on Fisher-discriminant to reject bad seed tracks.
 * - StateErrorX2: Error^2 on x-position (for making Track)
 * - StateErrorY2: Error^2 on y-position (for making Track)
 * - StateErrorTX2: Error^2 on tx-slope (for making Track)
 * - StateErrorTY2: Error^2 on ty-slope (for making Track)
 * - StateErrorP:  Error^2 on momentum (for making Track)
 * - RemoveUsed: Remove seed tracks and used UT hits (with chi2-cut on long track)?
 * - RemoveAll: Remove seed tracks and used UT hits (withoug chi2-cut on long track)?
 * - LongChi2: Chi2-cut for the removal
 * - SeedKey: Key of MCParticle for debugging
 * - WithDebugTool: Use the debug tool?
 * - DebugTool: Name of the debug tool
 * - PrintTracks: Print the tracks?
 * - TimingMeasurement: Do timing measurements?
 * - ForceMCTrack: Do you want to force the downstream track to only have true hits?
 *
 *  @author Michel De Cian and Adam Davis. Sascha Stahl and Olivier Callot for the original PrDownstream
 *  @date   2016-04-10
 *
 *  @2017-03-01: Christoph Hasse (adapt to future framework)
 *
 *
 */

class PrLongLivedTracking : public Gaudi::Functional::Transformer<std::vector<LHCb::Event::v2::Track>(const std::vector<LHCb::Event::v2::Track>&,
                                                                               const UT::HitHandler&)> {
  using Track = LHCb::Event::v2::Track;
  
public:
  /// Standard constructor
  PrLongLivedTracking( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  std::vector<Track> operator()(const std::vector<Track>&, const UT::HitHandler&) const override;

private:

  // Added during conversion:
  Gaudi::Property<bool>   m_printing         { this                         , "DebugPrinting"           , false };
  // Leave thes commented for now. These could possibly be used for as input for flagging, which is not supported currently
  //DataObjectReadHandle<LHCb::Tracks> m_ForwardTracks  { this, "ForwardTracks" LHCb::TrackLocation::Forward  };
  //DataObjectReadHandle<LHCb::Tracks> m_MatchTracks    { this, "MatchTracks", LHCb::TrackLocation::Match   };
  //

  Gaudi::Property<double> m_xPredTolConst    { this , "XPredTolConst"    , 200. * Gaudi::Units::mm * Gaudi::Units::GeV };
  Gaudi::Property<double> m_xPredTolOffset   { this , "XPredTolOffset"   , 6. * Gaudi::Units::mm                       };
  Gaudi::Property<double> m_tolMatchConst    { this , "TolMatchConst"    , 20000.                                      };
  Gaudi::Property<double> m_tolMatchOffset   { this , "TolMatchOffset"   , 1.5 *  Gaudi::Units::mm                     };
  Gaudi::Property<double> m_tolUConst        { this , "TolUConst"        , 20000.0                                     };
  Gaudi::Property<double> m_tolUOffset       { this , "TolUOffset"       , 2.5                                         };
  Gaudi::Property<double> m_tolVConst        { this , "TolVConst"        , 2000.0                                      };
  Gaudi::Property<double> m_tolVOffset       { this , "TolVOffset"       , 0.5                                         };
  Gaudi::Property<double> m_maxWindow        { this , "MaxWindowSize"    , 10.0 * Gaudi::Units::mm                     };
  Gaudi::Property<double> m_maxChi2          { this , "MaxChi2"          , 20.                                         };
  Gaudi::Property<double> m_maxChi2ThreeHits { this , "MaxChi2ThreeHits" , 10.0                                        };
  Gaudi::Property<double> m_minUTx           { this , "MinUTx"           , 25. *  Gaudi::Units::mm                     };
  Gaudi::Property<double> m_minUTy           { this , "MinUTy"           , 25. *  Gaudi::Units::mm                     };

  // Define parameters for MC09 field, zState = 9410
  Gaudi::Property<std::vector<double>> m_zMagnetParams  { this , "ZMagnetParams"  , { 5379.88 , -2143.93 , 366.124 , 119074 , -0.0100333 , -0.146055 , 1260.96 }};
  Gaudi::Property<std::vector<double>> m_momentumParams { this , "MomentumParams" , { 1217.77 , 454.598  , 3353.39                                             }};
  Gaudi::Property<std::vector<double>> m_yParams        { this , "YParams"        , {5.       , 2000.                                                          }};
  Gaudi::Property<double> m_zUT           { this , "ZUT"           , 2485.* Gaudi::Units::mm };
  Gaudi::Property<double> m_zUTa          { this , "ZUTa"          , 2350.* Gaudi::Units::mm };
  Gaudi::Property<double> m_stateErrorX2  { this , "StateErrorX2"  , 4.0                     };
  Gaudi::Property<double> m_stateErrorY2  { this , "StateErrorY2"  , 400.                    };
  Gaudi::Property<double> m_stateErrorTX2 { this , "StateErrorTX2" , 6.e-5                   };
  Gaudi::Property<double> m_stateErrorTY2 { this , "StateErrorTY2" , 1.e-4                   };
  Gaudi::Property<double> m_stateErrorP   { this , "StateErrorP"   , 0.15                    };
  Gaudi::Property<double> m_minPt         { this , "MinPt"         , 0. * Gaudi::Units::MeV  };
  Gaudi::Property<double> m_minMomentum   { this , "MinMomentum"   , 0. * Gaudi::Units::GeV  };

  // -- Parameter to reject seed track which are likely ghosts
  Gaudi::Property<double> m_seedCut { this, "FisherCut", -1.0 };

  // -- Parameters for the cut on deltaP (momentum estimate from Seeding and Downstream kink)
  Gaudi::Property<double> m_maxDeltaPConst  { this, "MaxDeltaPConst", 0.0 };
  Gaudi::Property<double> m_maxDeltaPOffset { this, "MaxDeltaPOffset", 0.25 };

  // -- Parameters for correcting the predicted position
  Gaudi::Property<double>       m_xCorrectionConst  { this , "XCorrectionConst"   , 23605.0 };
  Gaudi::Property<double>       m_xCorrectionOffset { this , "XCorrestionOffset"  , 0.4     };
  Gaudi::Property<unsigned int> m_maxXTracks        { this , "MaxXTracks"         , 2       };
  Gaudi::Property<double>       m_maxChi2DistXTracks{ this , "MaxChi2DistXTracks" , 0.2     };
  Gaudi::Property<unsigned int> m_maxXUTracks       { this , "MaxXUTracks"        , 2       };
  Gaudi::Property<double>       m_fitXProjChi2Offset{ this , "FitXProjChi2Offset" , 4.5     };
  Gaudi::Property<double>       m_fitXProjChi2Const { this , "FitXProjChi2Const"  , 35000.0 };

  // -- Tolerance for adding overlap hits
  Gaudi::Property<double> m_overlapTol{ this, "OverlapTol", 2.0*Gaudi::Units::mm };
  Gaudi::Property<double> m_yTol      { this, "YTol", 2.0*Gaudi::Units::mm };
  // Change this in order to remove hits and T-tracks used for longtracks.
  // RemoveAll configures that everything is removed.
  // If false only hits and T-tracks from good longtracks are removed.
  // The criterion for this is the Chi2 of the longtracks from the fit.
  Gaudi::Property<bool>   m_removeUsed { this , "RemoveUsed" , false };
  Gaudi::Property<bool>   m_removeAll  { this , "RemoveAll"  , false };
  Gaudi::Property<double> m_longChi2   { this , "LongChi2"   , 1.5   };

  //== debugging options
  Gaudi::Property<int>         m_seedKey       { this , "SeedKey"           , -1                   };
  Gaudi::Property<bool>        m_withDebugTool { this , "WithDebugTool"     , false};
  Gaudi::Property<std::string> m_debugToolName { this , "DebugTool"         , "PrDebugUTTruthTool" };
  Gaudi::Property<bool>        m_printTracks   { this , "PrintTracks"       , false                };
  Gaudi::Property<bool>        m_doTiming      { this , "TimingMeasurement" , false                };
  Gaudi::Property<bool>        m_forceMCTrack  { this , "ForceMCTrack"      , false                };
  Gaudi::Property<bool>        m_tuneFisher    { this , "TuneFisher"        , false                };


  //void ttCoordCleanup() const;  ///< Tag already used coordinates

  //void prepareSeeds(const Tracks& inTracks, std::vector<Track*>& myInTracks)const; ///< Tag already used T-Seeds

  void getPreSelection( PrDownTrack& track, std::array<UT::Mut::Hits,4>& m_preSelHits,
                        UT::Mut::Hits& , const UT::HitHandler& ) const; ///< Get a preselection of hits around a first track estimate

  template<bool onlyFit>
  void fitAndRemove( PrDownTrack& track ) const; ///< Perform a chi2 fit to the track and remove outliers

  void findMatchingHits( const PrDownTrack& track, const int plane , std::array<UT::Mut::Hits,4>& m_preSelHits, UT::Mut::Hits& m_matchingXHits) const; ///< Find x hits matching the first track estimate

  Track makeTrack( PrDownTrack const & track ) const; ///< Store the track as Track

  void addUHits( const PrDownTrack& track, const unsigned int maxNumTracks, std::array<UT::Mut::Hits,4>& m_preSelHits, PrDownTracks& m_goodXUTracks) const; ///< Add hits in u layer

  void addVHits( PrDownTrack& track , std::array<UT::Mut::Hits,4>& m_preSelHits) const; ///< Add hits in v layer. The hit with the best chi2 is taken.

  void tagUsedUT( const Track* tr ) const; ///< Tag hits that were already used elsewhere

  void fitXProjection( PrDownTrack& track, UT::Mut::Hit& firstHit , UT::Mut::Hits& m_matchingXHits, PrDownTracks& m_goodXTracks) const; ///< Fit the different x candidates and take the best ones (according to chi2)

  bool acceptCandidate( PrDownTrack& track, int& maxPoints, bool magnetOff ) const; ///< Is this candidate good enough?

  void addOverlapRegions( PrDownTrack& track, std::array<UT::Mut::Hits,4>& m_preSelHits) const; ///< Add hits in the overlap regions

  //double evaluateFisher( const Track* track ) const;///< Evaluate the Fisher discriminant for a preselection of seed tracks

  void xFit( PrDownTrack& track, const UT::Mut::Hit& hit1, const UT::Mut::Hit& hit2 ) const; ///< Fit tracklet with hits in x layers only.


  // -- UT hits don't need the correction for the differnce in the reference frame, this should be a bit faster
  // -- than the original implementation
  void updateUTHitForTrackFast(UT::Mut::Hit& hit, const double y0, const double dyDz) const {
    const auto y  = ( y0 + dyDz * hit.HitPtr->zAtYEq0() );
    hit.x =  hit.HitPtr->xAt(y);
  }

  /// Does this track point inside the beampipe?
  bool insideBeampipe(const PrDownTrack& track) const{
    return (m_minUTx > fabs( track.xAtZ( m_zUTa ) )) && (m_minUTy > fabs( track.yAtZ( m_zUTa ) ));
  }

  /// Add to nStereo if hit is in a stereo layer
  bool addIsStereo(const UT::Mut::Hit& hit) const{
    return m_addIsStereoHelper[hit.HitPtr->planeCode()];
  }

  /// Helper to evaluate the Fisher discriminant
  /*
  double getFisher(const std::array<double,5> vals){
    double fishVal =  m_fishConst;
    for(int i = 0; i < 5; i++) fishVal += vals[i]*m_fishCoefficients[i];
    return fishVal;
  }
  */

  /// Helper to evaluate the maximum discrepancy between momentum from kink and curvature in T-stations

  double maxDeltaP( const PrDownTrack& track ) const {
    return m_maxDeltaPConst/std::abs( track.momentum() ) + m_maxDeltaPOffset;
  }


  /// Helper to evaluate the correction to the x position in the UT
  double xPosCorrection( const PrDownTrack& track ) const {
    return std::copysign( m_xCorrectionOffset + m_xCorrectionConst/std::abs( track.momentum() ), track.momentum() );
  }

  // -- timing information
  int           m_downTime = 0;
  int           m_preselTime = 0;
  int           m_findMatchingHitsTime = 0;
  int           m_fitXProjectionTime = 0;
  int           m_fitAndRemoveTime = 0;
  int           m_xFitTime = 0;
  int           m_addUHitsTime = 0;
  int           m_addVHitsTime = 0;
  int           m_acceptCandidateTime = 0;
  int           m_storeTrackTime = 0;
  int           m_overlapTime = 0;

  // -- counters
  mutable Gaudi::Accumulators::Counter<> m_downTrackCounter{this, "# Downstream tracks made"};
  mutable Gaudi::Accumulators::Counter<> m_utHitsCounter{this, "#UT hits added"};

  // -- pointers to tools
  ILHCbMagnetSvc*                     m_magFieldSvc = nullptr;
  IPrDebugUTTool*                     m_debugTool = nullptr;
  ISequencerTimerTool*                m_timerTool = nullptr;
  std::unique_ptr<IClassifierReaderForLLT> m_mvaReader;


  // -- internal helper variables
  std::array<double,7>    m_magPars;
  std::array<double,3>    m_momPars;
  double                  m_fishConst;
  std::array<double,5>    m_fishCoefficients;
  std::array<int,4>       m_addIsStereoHelper;

};

//=========================================================================
//  Fit and remove the worst hit, as long as over tolerance
//=========================================================================
template<bool onlyFit>
void PrLongLivedTracking::fitAndRemove ( PrDownTrack& track ) const {

  if ( m_doTiming ) m_timerTool->start( m_fitAndRemoveTime );

  if ( 2 > track.hits().size() ){
    if ( m_doTiming ) m_timerTool->stop( m_fitAndRemoveTime );
    return;  // no fit if single point only !
  }
  bool again = false;
  do {

    again = false;

    //== Fit, using the magnet point as constraint.
    double mat[6], rhs[3];
    mat[0] = 1./( track.errXMag() * track.errXMag() );
    mat[1] = 0.;
    mat[2] = 0.;
    mat[3] = 0.;
    mat[4] = 0.;
    mat[5] = 0.;
    rhs[0] = track.dxMagnet() /( track.errXMag() * track.errXMag() );//( m_magnetSave.x() - m_magnet.x() );
    rhs[1] = 0.;
    rhs[2] = 0.;

    int nbUV = 0;

    std::array<int,4> differentPlanes = {0, 0, 0, 0 };
    unsigned int nDoF = 0;

    double yTrack = track.yAtZ( 0. );
    double tyTr   = track.slopeY();

    for(auto& hit : track.hits()) {

      if( !onlyFit ) updateUTHitForTrackFast( hit, yTrack, tyTr);
      const double dz   = 0.001*(hit.z - track.zMagnet());
      const double dist = track.distance( hit );
      const double w    = hit.HitPtr->weight();
      const double t    = hit.HitPtr->sinT();

      mat[0] += w;
      mat[1] += w * dz;
      mat[2] += w * dz * dz;
      mat[3] += w * t;
      mat[4] += w * dz * t ;
      mat[5] += w * t  * t ;
      rhs[0] += w * dist;
      rhs[1] += w * dist * dz;
      rhs[2] += w * dist * t ;

      // -- check how many different layers have fired
      differentPlanes[hit.HitPtr->planeCode()]++;
      nbUV += addIsStereo( hit );

      if ( UNLIKELY( m_printing )) {
        info() << format( "   Plane %2d x %7.2f dist %6.3f ",
                          hit.HitPtr->planeCode(), hit.x, dist );
        if ( m_debugTool ) m_debugTool->debugUTCluster( info(), hit );
        info() << endmsg;
      }

    }

    nDoF = std::count_if(differentPlanes.begin(), differentPlanes.end(), [](const int a){ return a > 0; });
    track.setFiredLayers( nDoF );

    // -- solve the equation and update the parameters of the track
    CholeskyDecomp<double, 3> decomp(mat);
    if (UNLIKELY(!decomp)) {
      track.setChi2(1e42);
      if ( m_doTiming ) m_timerTool->stop( m_fitAndRemoveTime );
      return;
    } else {
      decomp.Solve(rhs);
    }

    const double dx  = rhs[0];
    const double dsl = 0.001*rhs[1];
    const double dy  = rhs[2];

    if ( UNLIKELY( m_printing )) {
      info() << format( "  dx %7.3f dsl %7.6f dy %7.3f, displY %7.2f",
                        dx, dsl, dy, track.displY() ) << endmsg;
    }

    if ( 4 > nbUV ) track.updateX( dx, dsl );
    track.setDisplY( track.displY() + dy );

    //== Remove worst hit and retry, if too far.
    double chi2 = track.initialChi2();

    double maxDist = -1.;
    UT::Mut::Hits::iterator worst;

    yTrack = track.yAtZ( 0. );
    tyTr   = track.slopeY();

    for (auto itH = track.hits().begin(); itH != track.hits().end(); ++itH){

      UT::Mut::Hit& hit = *itH;
      updateUTHitForTrackFast( hit, yTrack, tyTr);

      if( !onlyFit ){
        const double yTrackAtZ = track.yAtZ( hit.z );
        if( !hit.HitPtr->isYCompatible(yTrackAtZ, m_yTol ) ){
          // --
          if ( UNLIKELY( m_printing )) {
            info() << "   remove Y incompatible hit measure = " << hit.x
                   << " : y " << yTrackAtZ << " min " << hit.HitPtr->yMin()
                   << " max " << hit.HitPtr->yMax() << endmsg;
          }
          // --
          track.hits().erase( itH );
          if ( 2 < track.hits().size() ) again = true;
          break;
        }
      }

      const double dist = track.distance( hit );
      hit.projection = std::abs(dist);
      // --
      if ( UNLIKELY( m_printing )) {
        info() << format( "   Plane %2d x %7.2f dist %6.3f ",
                          hit.HitPtr->planeCode(), hit.x, dist );
        if ( m_debugTool ) m_debugTool->debugUTCluster( info(), hit );
        info() << endmsg;
      }
      // --
      chi2 += dist * dist * hit.HitPtr->weight();
      // -- only flag this hit as removable if it is not alone in a plane or there are 4 planes that fired
      if ( !onlyFit && maxDist < std::abs(dist) &&  (1 < differentPlanes[hit.HitPtr->planeCode()] || nDoF == track.hits().size() ) ) {
        maxDist = std::abs( dist );
        worst = itH;
      }
    }

    if(again) continue;

    if ( 2 < track.hits().size() ) chi2 /= (track.hits().size() - 2 );
    track.setChi2( chi2 );

    if(onlyFit){
      if ( m_doTiming ) m_timerTool->stop( m_fitAndRemoveTime );
      return;
    }

    if ( m_maxChi2 < chi2 && track.hits().size() > 3 && maxDist > 0) {

      track.hits().erase( worst );

      again = true;
      if( UNLIKELY( m_printing )) info() << "   remove worst and retry" << endmsg;
    }

    if ( UNLIKELY( m_printing )) {
      info() << format( "  ---> chi2 %7.2f maxDist %7.3f", chi2, maxDist) << endmsg;
    }
  } while (again);

  if ( m_doTiming ) m_timerTool->stop( m_fitAndRemoveTime );

}
#endif // PRLONGLIVEDTRACKING
