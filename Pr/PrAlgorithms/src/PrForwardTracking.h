/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRFORWARDTRACKING_H
#define PRFORWARDTRACKING_H 1

// Include files
// from Gaudi
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "PrKernel/PrFTHitHandler.h"

#include "PrKernel/IPrDebugTool.h"

#include "Event/Track_v2.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/extends.h"
#include "PrGeometryTool.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/PrFTZoneHandler.h"
#include "PrLineFitter.h"
#include "PrPlaneCounter.h"

#include "IPrAddUTHitsTool.h"

// NN for ghostprob
#include "weights/TMVA_MLP_Forward1stLoop.h"
#include "weights/TMVA_MLP_Forward2ndLoop.h"

// Parameters passed around in PrForwardTool
struct PrParameters {
  PrParameters( unsigned int minXHits_, float maxXWindow_, float maxXWindowSlope_, float maxXGap_,
                unsigned int minStereoHits_ )
      : minXHits{minXHits_}
      , maxXWindow{maxXWindow_}
      , maxXWindowSlope{maxXWindowSlope_}
      , maxXGap{maxXGap_}
      , minStereoHits{minStereoHits_}
  {
  }
  const unsigned int minXHits;
  const float        maxXWindow;
  const float        maxXWindowSlope;
  const float        maxXGap;
  unsigned int       minStereoHits;
};

/** @class PrForwardTracking PrForwardTracking.h
 *
 *  \brief Main algorithm for the Forward tracking of the upgrade
 *  \brief The main work is done in PrForwardTool
 *
 *   Parameters:
 * - InputName: Name of the input container
 * - OutputName: Name of the output container
 * - HitManagerName: Name of the hit manager
 * - ForwardToolName: Name of the tool that does the actual forward tracking
 * - DeltaQuality: Quality criterion to reject duplicates
 * - DebugToolName: Name of the debug tool
 * - WantedKey: Key of the MCParticle one wants to look at
 * - VeloKey: Key of the Velo track one wants to look at
 * - TimingMeasurement: Print table with timing measurements
 *
 *
 *  @author Olivier Callot
 *  @date   2012-03-20
 *  @author Michel De Cian
 *  @date   2014-03-12 Changes with make code more standard and faster
 *  @author Olli Lupton
 *  @date   2018-11-07 Imported PrForwardTool into PrForwardTracking as a step towards making PrForwardTracking accept
 *                     selections
 */

namespace Pr
{
  namespace detail
  {
    namespace PrForwardTracking
    {
      using Track       = LHCb::Event::v2::Track;
      using InputTracks = std::vector<Track> const&;
      using Output      = std::vector<Track>;
      using Transformer = Gaudi::Functional::Transformer<Output( PrFTHitHandler<PrHit> const&, InputTracks )>;
    } // namespace PrForwardTracking
  }   // namespace detail
} // namespace Pr

class PrForwardTracking : public Pr::detail::PrForwardTracking::Transformer
{
public:
  using Track       = Pr::detail::PrForwardTracking::Track;
  using Output      = Pr::detail::PrForwardTracking::Output;
  using InputTracks = Pr::detail::PrForwardTracking::InputTracks;

  /// Standard constructor
  PrForwardTracking( const std::string& name, ISvcLocator* pSvcLocator );

  /// initialization
  StatusCode initialize() override;

  /// main call
  Output operator()( const PrFTHitHandler<PrHit>&, InputTracks ) const override final;

private:
  // Parameters for debugging
  Gaudi::Property<std::string> m_debugToolName{this, "DebugToolName", ""};
  Gaudi::Property<int>         m_wantedKey{this, "WantedKey", -100};
  Gaudi::Property<int>         m_veloKey{this, "VeloKey", -100};
  Gaudi::Property<bool>        m_doTiming{this, "TimingMeasurement", false};

  // Parameters that used to come from PrForwardTool
  Gaudi::Property<float> m_yTolUVSearch{this, "YToleranceUVsearch", 11. * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolY{this, "TolY", 5. * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolYSlope{this, "TolYSlope", 0.002 * Gaudi::Units::mm};
  Gaudi::Property<float> m_maxChi2LinearFit{this, "MaxChi2LinearFit", 100.};
  Gaudi::Property<float> m_maxChi2XProjection{this, "MaxChi2XProjection", 15.};
  Gaudi::Property<float> m_maxChi2PerDoF{this, "MaxChi2PerDoF", 7.};

  Gaudi::Property<float> m_tolYMag{this, "TolYMag", 10. * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolYMagSlope{this, "TolYMagSlope", 0.015};
  Gaudi::Property<float> m_minYGap{this, "MinYGap", 0.4 * Gaudi::Units::mm};

  Gaudi::Property<unsigned int> m_minTotalHits{this, "MinTotalHits", 10};
  Gaudi::Property<float>        m_maxChi2StereoLinear{this, "MaxChi2StereoLinear", 60.};
  Gaudi::Property<float>        m_maxChi2Stereo{this, "MaxChi2Stereo", 4.5};

  // first loop Hough Cluster search
  Gaudi::Property<unsigned int> m_minXHits{this, "MinXHits", 5};
  Gaudi::Property<float>        m_maxXWindow{this, "MaxXWindow", 1.2 * Gaudi::Units::mm};
  Gaudi::Property<float>        m_maxXWindowSlope{this, "MaxXWindowSlope", 0.002 * Gaudi::Units::mm};
  Gaudi::Property<float>        m_maxXGap{this, "MaxXGap", 1.2 * Gaudi::Units::mm};
  Gaudi::Property<unsigned int> m_minSingleHits{this, "MinSingleHits", 2};

  // second loop Hough Cluster search
  Gaudi::Property<bool>         m_secondLoop{this, "SecondLoop", true};
  Gaudi::Property<unsigned int> m_minXHits2nd{this, "MinXHits2nd", 4};
  Gaudi::Property<float>        m_maxXWindow2nd{this, "MaxXWindow2nd", 1.5 * Gaudi::Units::mm};
  Gaudi::Property<float>        m_maxXWindowSlope2nd{this, "MaxXWindowSlope2nd", 0.002 * Gaudi::Units::mm};
  Gaudi::Property<float>        m_maxXGap2nd{this, "MaxXGap2nd", 0.5 * Gaudi::Units::mm};

  // stereo hit matching
  Gaudi::Property<float> m_tolYCollectX{this, "TolYCollectX", 4.1 * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolYSlopeCollectX{this, "TolYSlopeCollectX", 0.0018 * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolYTriangleSearch{this, "TolYTriangleSearch", 20.f};

  // collectX search
  Gaudi::Property<float> m_minPT{this, "MinPT", 50 * Gaudi::Units::MeV};
  // veloUT momentum estimate
  Gaudi::Property<bool>  m_useMomentumEstimate{this, "UseMomentumEstimate", true};
  Gaudi::Property<bool>  m_useWrongSignWindow{this, "UseWrongSignWindow", true};
  Gaudi::Property<float> m_wrongSignPT{this, "WrongSignPT", 2000. * Gaudi::Units::MeV};
  // preselection (if done here??!!)
  Gaudi::Property<bool>  m_preselection{this, "Preselection", false};
  Gaudi::Property<float> m_preselectionPT{this, "PreselectionPT", 400. * Gaudi::Units::MeV};
  // Momentum guided search window switch
  Gaudi::Property<bool> m_useMomSearchWindow{this, "UseMomentumGuidedSearchWindow", false};

  // Track Quality (Neural Net)
  Gaudi::Property<float> m_maxQuality{this, "MaxQuality", 0.9};
  Gaudi::Property<float> m_deltaQuality{this, "DeltaQuality", 0.1};

  /** Check if track can be forwarded
   *  @brief Check if track can be forwarded
   *  @param track Velo track that might be forwarded
   *  @return bool
   */
  bool acceptTrack( const Track& track ) const
  {
    return !( track.checkFlag( Track::Flag::Invalid ) ) && !( track.checkFlag( Track::Flag::Backward ) );
  }

  //== Debugging controls
  IPrDebugTool*        m_debugTool = nullptr;
  ISequencerTimerTool* m_timerTool = nullptr;
  int                  m_timeTotal;
  int                  m_timePrepare;
  int                  m_timeExtend;
  int                  m_timeFinal;

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nbOutputTracksCounter{this, "Nb output tracks"};

  // -- MAIN METHOD
  Output extendTracks( InputTracks velo, const PrFTHitHandler<PrHit>& FTHitHandler ) const;

  // ====================================================================================
  // -- DEBUG HELPERS
  // ====================================================================================
  bool matchKey( const PrHit& hit ) const { return m_debugTool && m_debugTool->matchKey( hit.id(), m_wantedKey ); };

  void printHit( const PrHit& hit, const std::string title = "" ) const
  {
    info() << "  " << title
           << format( "Plane%3d zone%2d z0 %8.2f x0 %8.2f", hit.planeCode(), hit.zone(), hit.z(), hit.x() );
    if ( m_debugTool ) m_debugTool->printKey( info(), hit.id() );
    if ( matchKey( hit ) ) info() << " ***";
    info() << endmsg;
  }

  //=========================================================================
  //  Print the content of a track
  //=========================================================================
  void printTrack( const PrForwardTrack& track ) const
  {
    if ( track.nDoF() > 0 ) {
      info() << format( "   Track nDoF %3d   chi2/nDoF %7.3f quality %6.3f", int( track.getChi2nDof().get( 1 ) ),
                        track.chi2PerDoF(), track.quality() )
             << endmsg;
    } else {
      info() << "   Track" << endmsg;
    }

    for ( const auto& hit : track.hits() ) {
      info() << format( "dist %7.3f dy %7.2f chi2 %7.2f ", track.distance( *hit ), track.distance( *hit ) / hit->dxDy(),
                        track.chi2( *hit ) );
      printHit( *hit );
    }
  }

  //=========================================================================
  //  Print the hits for a given track
  //=========================================================================
  void printHitsForTrack( const PrHits& hits, const PrForwardTrack& track ) const
  {
    info() << "=== Hits to Track ===" << endmsg;
    for ( const auto& hit : hits ) {
      info() << format( "dist %7.3f dy %7.2f chi2 %7.2f ", track.distance( *hit ), track.distance( *hit ) / hit->dxDy(),
                        track.chi2( *hit ) );
      printHit( *hit );
    }
  }

  // make full PrForward Track candidates
  void selectFullCandidates( PrForwardTracks& trackCandidates, const PrFTHitHandler<PrHit>& FTHitHandler,
                             PrParameters& pars, const float magScaleFactor ) const;

  // work wth with X-hits on x projection, side = top or bottom
  template <PrHitZone::Side SIDE>
  void collectAllXHits( std::vector<ModPrHit>& allXHits, const PrForwardTrack& track,
                        const PrFTHitHandler<PrHit>& FTHitHandler, const PrParameters& pars,
                        const float magScaleFactor ) const;

  template <PrHitZone::Side SIDE>
  void selectXCandidates( PrForwardTracks& trackCandidates, const PrForwardTrack& seed, std::vector<ModPrHit>& allXHits,
                          const PrFTHitHandler<PrHit>& FTHitHandler, const PrParameters& pars ) const;

  void fastLinearFit( PrForwardTrack& track, const PrParameters& pars, PrPlaneCounter& pc ) const;

  // also used in the end for "complete" fit
  bool fitXProjection( PrForwardTrack& track, const PrParameters& pars, PrPlaneCounter& pc ) const;

  template <PrHitZone::Side SIDE>
  bool addHitsOnEmptyXLayers( PrForwardTrack& track, bool refit, const PrFTHitHandler<PrHit>& FTHitHandler,
                              const PrParameters& pars, PrPlaneCounter& pc ) const;

  // work with U/V hits on y projection
  std::vector<ModPrHit> collectStereoHits( PrForwardTrack& track, const PrFTHitHandler<PrHit>& FTHitHandler ) const;

  bool selectStereoHits( PrForwardTrack& track, const PrFTHitHandler<PrHit>& FTHitHandler,
                         const std::vector<ModPrHit>& allStereoHits, const PrParameters& pars ) const;

  bool fitYProjection( PrForwardTrack& track, std::vector<const PrHit*>& stereoHits, const PrParameters& pars,
                       PrPlaneCounter& pc ) const;

  bool addHitsOnEmptyStereoLayers( PrForwardTrack& track, std::vector<const PrHit*>& stereoHits,
                                   const PrFTHitHandler<PrHit>& FTHitHandler, const PrParameters& pars,
                                   PrPlaneCounter& pc ) const;

  // save good tracks
  void makeLHCbTracks( PrForwardTracks& trackCandidates, std::vector<Track>& result ) const;

  // Tools
  ToolHandle<IPrAddUTHitsTool>     m_addUTHitsTool{this, "AddUTHitsToolName", "PrAddUTHitsTool"};
  PublicToolHandle<PrGeometryTool> m_geoTool{this, "PrGeometryTool", "PrGeometryTool"};

  // Neural Net The neural net readers are classes generated by TMVA. They are not threadsafe
  // and have been modified by hand. That will have to be done every time the MLP is retuned!
  // For as long as there are the hand tuned networks, no mutex here.
  ReadMLP_Forward1stLoop m_MLPReader_1st;
  ReadMLP_Forward2ndLoop m_MLPReader_2nd;

  /// derived condition caching computed zones
  PrFTZoneHandler* m_zoneHandler = nullptr;

  // helper functions
  void setHitsUsed( std::vector<ModPrHit>::iterator it, const std::vector<ModPrHit>::iterator& itEnd,
                    const PrForwardTrack& track, const PrParameters& pars ) const;

  float zMagnet( const PrForwardTrack& track ) const;
  float calcqOverP( const PrForwardTrack& track, const float magScaleFactor ) const;
  void  merge6Sorted( std::vector<ModPrHit>& allXHits, const std::array<int, 7>& boundaries ) const;
  void  setTrackParameters( PrForwardTrack& track, const float xAtRef ) const;
  void  xAtRef_SamePlaneHits( const PrForwardTrack& track, std::vector<ModPrHit>::iterator itH,
                              const std::vector<ModPrHit>::iterator itEnd ) const;

  // find matching stereo hit if available, also searching in triangle region
  template <PrHitZone::Side SIDE>
  bool matchStereoHitWithTriangle( std::vector<PrHit>::const_iterator&       itUV2,
                                   const std::vector<PrHit>::const_iterator& itUV2end, const float yInZone,
                                   const float xMinUV, const float xMaxUV ) const;
};
#endif // PRFORWARDTRACKING_H
