/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRPLANEHYBRIDCOUNTER_H
#define PRPLANEHYBRIDCOUNTER_H 1

// Include files
#include "Kernel/STLExtensions.h"
#include "PrKernel/PrFTHitHandler.h"
/** @class PrPlaneHybridCounter PrPlaneHybridCounter.h
 *  Class to count how many different planes are in a List and how many planes with a single hit fired.
 *  It also checks if the content of hits is distributed in the SciFi in a proper way 1 hit per station X ( and UV ).
 *
 *  @author renato quagliani
 *  @date   2015-07-13
 */

class PrPlaneHybridCounter final{
public:
  PrPlaneHybridCounter( ):
    m_nbFiredLayers(0),
    m_nbFiredLayers_singleHit(0),
    m_planeList({{0,0,0,0,0,0,0,0,0,0,0,0}}),
    m_nbT1X(0),
    m_nbT1X_singleHit(0),
    m_nbT2X(0),
    m_nbT2X_singleHit(0),
    m_nbT3X(0),
    m_nbT3X_singleHit(0),
    m_nbT1UV(0),
    m_nbT1UV_singleHit(0),
    m_nbT2UV(0),
    m_nbT2UV_singleHit(0),
    m_nbT3UV(0),
    m_nbT3UV_singleHit(0),
    m_nbHits(0)
  { };

  inline size_t nUsed( ModPrHitConstIter itBeg, ModPrHitConstIter itEnd, const PrFTHitHandler<ModPrHit>& hitHandler )const noexcept{
    return std::count_if(itBeg, itEnd ,[&hitHandler](const ModPrHit& h) { return ! hitHandler.hit( h.hitIndex).isValid();
      });
      //!h.isValid(); });
  }

  /** Set values: given a range of iterators it fills the information for the hit content
   *  @brief Set values for a given range of iterators
   *  @param itBeg First iterator, begin of range
   *  @param itEnd Last iterator, end of range
   *  @param fill Should the values be reset?
   */
  void set(ModPrHitConstIter itBeg, ModPrHitConstIter itEnd, const bool fill = true) noexcept{
    if(fill){
      m_nbHits=0;
      m_nbFiredLayers = 0;
      m_nbFiredLayers_singleHit = 0;
      m_planeList = {{0,0,0,0,0,0,0,0,0,0,0,0}};
      //X layer T1
      m_nbT1X=0;
      m_nbT1X_singleHit=0;
      //X layer T2
      m_nbT2X=0;
      m_nbT2X_singleHit=0;
      //X layer T3
      m_nbT3X=0;
      m_nbT3X_singleHit=0;
      //UV Layer T1
      m_nbT1UV=0;
      m_nbT1UV_singleHit=0;
      //UV Layer T2
      m_nbT2UV=0;
      m_nbT2UV_singleHit=0;
      //UV Layer T3
      m_nbT3UV=0;
      m_nbT3UV_singleHit=0;
      //single hits in T1 / T3
      m_nbT1_singleHit = 0;
      m_nbT2_singleHit = 0;
      m_nbT3_singleHit = 0;
    }

    for( auto itH = itBeg; itEnd != itH; ++itH) {
      m_nbHits++;
      //0---3 (T1) , 4--7 (T2), 8--11( T3
      unsigned int plane = itH->planeCode;
      bool isT1 = (plane/4 ==0);
      bool isT2 = (plane/4 ==1);
      bool isT3 = (plane/4 ==2);
      bool isX =  ( (plane%4 ==0) ||  ( plane%4 == 3 ) );
      //if no hits in the plane, increase the counters anyway
      if( 0 == m_planeList[ plane ]++){
        m_nbFiredLayers++;
        m_nbFiredLayers_singleHit++;
        //----- T1 Controls
        m_nbT1+=            isT1;
        m_nbT1_singleHit+=  isT1;
        m_nbT1X_singleHit+= (isT1  &&  isX  );
        m_nbT1X+=           (isT1  &&  isX  );
        m_nbT1UV+=          (isT1  &&  !isX );
        m_nbT1UV_singleHit+=(isT1  &&  !isX );
        //----- T2 controls
        m_nbT2+=            isT2;
        m_nbT2_singleHit+=  isT2;
        m_nbT2X_singleHit+= (isT2  &&  isX  );
        m_nbT2X+=           (isT2  &&  isX  );
        m_nbT2UV+=          (isT2  &&  !isX );
        m_nbT2UV_singleHit+=(isT2  &&  !isX );
        //----- T3 controls
        m_nbT3+=            isT3;
        m_nbT3_singleHit+=  isT3;
        m_nbT3X_singleHit+= (isT3   &&  isX);
        m_nbT3X+=           (isT3  &&  isX  );
        m_nbT3UV+=          (isT3  &&  !isX );
        m_nbT3UV_singleHit+=(isT3  &&  !isX );
      }else if(2 == m_planeList[plane]){ // you have already increased it in the >0 condition, never from ==0 conditio
        m_nbFiredLayers_singleHit --;

        //-----T1 controls
        m_nbT1_singleHit -= isT1;
        m_nbT1X_singleHit -= (isT1 &&  isX);
        m_nbT1UV_singleHit -= (isT1 && !isX);

        //---- T2 controls
        m_nbT2_singleHit -= isT2;
        m_nbT2X_singleHit -= (isT2 &&  isX);
        m_nbT2UV_singleHit -= (isT2 && !isX);

        //------ T3 controls
        m_nbT2_singleHit -= isT3;
        m_nbT2X_singleHit -= (isT3 &&  isX);
        m_nbT2UV_singleHit -= (isT3 && !isX);
      }
    }//end loop hits
  }

  int addHit( const ModPrHit& hit) noexcept{
    //if(hit->isUsed()) return m_nbFiredLayers;
    unsigned int plane = hit.hit->planeCode();
    m_nbHits++;
    bool isT1 = plane/4 ==0;
    bool isT2 = plane/4 ==1;
    bool isT3 = plane/4 ==2;
    bool isX = ( ( plane%4 == 0 ) ||  ( plane%4 == 3 ) ) ;

    if( 0 == m_planeList[plane]++){
       m_nbFiredLayers_singleHit ++;
       m_nbFiredLayers ++;
       m_nbT1+=            isT1;
       m_nbT1X+=           (isT1  &&  isX  );
       m_nbT1UV+=          (isT1  &&  !isX );

       m_nbT1_singleHit+=  isT1;
       m_nbT1X_singleHit+= (isT1  &&  isX  );
       m_nbT1UV_singleHit+=(isT1  &&  !isX );

       m_nbT2+=            isT2;
       m_nbT2X+=           (isT2  &&  isX  );
       m_nbT2UV+=          (isT2  &&  !isX );

       m_nbT2_singleHit+=  isT2;
       m_nbT2X_singleHit+= (isT2  &&  isX  );
       m_nbT2UV_singleHit+=(isT2  &&  !isX );

       m_nbT3+=            isT3;
       m_nbT3X+=           (isT3  &&  isX  );
       m_nbT3UV+=          (isT3  &&  !isX );

       m_nbT3_singleHit+=  isT3;
       m_nbT3X_singleHit+= (isT3  &&  isX);
       m_nbT3UV_singleHit+=(isT3  &&  !isX );

       // optimization with the elseif possible here ( not used anyway)
    }else if( 2 == m_planeList[plane]){ //if you are adding a hit where already present one singlecounting decreased
      m_nbFiredLayers_singleHit --;
      m_nbT1_singleHit   -= isT1;
      m_nbT1X_singleHit  -= (isT1  &&  isX);
      m_nbT1UV_singleHit -= (isT1  &&  !isX );

      m_nbT2_singleHit   -= isT2;
      m_nbT2X_singleHit  -= (isT2  &&  isX);
      m_nbT2UV_singleHit -= (isT2  &&  !isX );

      m_nbT3_singleHit   -= isT3;
      m_nbT3X_singleHit  -= (isT3  &&  isX);
      m_nbT3UV_singleHit -= (isT3  &&  !isX );
    }
    return m_nbFiredLayers;
  }

  int removeHit( const ModPrHit& hit) noexcept{
    m_nbHits--;
    unsigned int plane = hit.planeCode;
    unsigned int NumberInLayerAfterRemove = m_planeList[plane] -1;
    m_planeList[plane]--;
    bool isT1 = plane/4 ==0;
    bool isT2 = plane/4 ==1;
    bool isT3 = plane/4 ==2;
    bool isX = ( ( plane%4 == 0 ) ||  ( plane%4 == 3 ) );
    if( 0 == NumberInLayerAfterRemove){ //you remain with 0 hits in that plane decrease everything
      m_nbFiredLayers_singleHit --;
      m_nbFiredLayers -- ;
      m_nbT1              -= isT1;
      m_nbT1_singleHit    -= isT1;
      m_nbT1X             -= ( isX && isT1) ;
      m_nbT1X_singleHit   -= ( isX && isT1) ;
      m_nbT1UV            -= (!isX && isT1) ;
      m_nbT1UV_singleHit  -= (!isX && isT1) ;

      m_nbT2-= isT2;
      m_nbT2_singleHit-= isT2;
      m_nbT2X             -= ( isX && isT2) ;
      m_nbT2X_singleHit   -= ( isX && isT2) ;
      m_nbT2UV            -= (!isX && isT2) ;
      m_nbT2UV_singleHit  -= (!isX && isT2) ;

      m_nbT3-= isT3;
      m_nbT3_singleHit-= isT3;
      m_nbT3X             -= ( isX && isT3) ;
      m_nbT3X_singleHit   -= ( isX && isT3) ;
      m_nbT3UV            -= (!isX && isT3) ;
      m_nbT3UV_singleHit  -= (!isX && isT3) ;

    }
    if( 1 == NumberInLayerAfterRemove) //you remain with just one hit in that layer after removing it
    {
      m_nbFiredLayers_singleHit++;
      m_nbT1_singleHit  += isT1  ;
      m_nbT1X_singleHit += ( isT1 && isX );
      m_nbT1UV_singleHit+= ( isT1 && !isX);
      m_nbT2_singleHit  += isT2  ;
      m_nbT2X_singleHit += ( isT2 && isX );
      m_nbT2UV_singleHit+= ( isT2 && !isX);
      m_nbT3_singleHit  += isT3  ;
      m_nbT3X_singleHit += ( isT3 && isX );
      m_nbT3UV_singleHit+= ( isT3 && !isX);
    }
    return m_nbFiredLayers;
  }
  unsigned int nbDifferent() const noexcept{ return m_nbFiredLayers;}
  unsigned int nbSingle() const noexcept{ return m_nbFiredLayers_singleHit;}
  unsigned int nbDifferentX() const noexcept { return m_nbT1X + m_nbT2X + m_nbT3X; }
  unsigned int nbSingleX() const noexcept {return (m_nbT1X_singleHit + m_nbT2X_singleHit + m_nbT3X_singleHit);}
  unsigned int nbDifferentUV() const noexcept{ return (m_nbT1UV + m_nbT2UV + m_nbT3UV);}
  unsigned int nbSingleUV() const noexcept {return (m_nbT1UV_singleHit + m_nbT2UV_singleHit + m_nbT3UV_singleHit);}
  unsigned int nbHits() const noexcept {return m_nbHits;}

  bool isOK2() const noexcept{
    return ( isOK() && isOKUV() && (m_nbT1X+m_nbT1UV>=3) && (m_nbT2X+m_nbT2UV>=3) && (m_nbT3X+m_nbT3UV>=3));
  }

  bool isOKUV() const noexcept{
    //at least 1 UV hit in each station & at least one station with 2 UV hits
    return ( (m_nbT1UV>=1 && m_nbT2UV>=1 && m_nbT3UV>=1) &&
             ( m_nbT1UV>1 || m_nbT2UV>1 || m_nbT3UV>1) );
    return false;
  }
  bool isOKX() const noexcept{
    return ( (m_nbT1X>=1 && m_nbT2X>=1 && m_nbT3X>=1) &&
             (m_nbT1X>1 || m_nbT2X>1 || m_nbT3X>1));
    return false;
  }

  template <typename Range>
  static bool hasT1T2T3Track(const Range& hits) noexcept {
    std::bitset<3> T{};
    for( const auto& hit : hits ) {
      int planeBit = hit.planeCode / 4;
      ASSUME(planeBit < 3);
      T[ planeBit ] = true;
    }
    return T.all();
  }

  bool isOK() const noexcept{
    return isOKX() && isOKUV();
  }

  unsigned int nbInPlane( const int plane) const noexcept{return m_planeList[plane];}
  //  unsigned int nbInPlane( int plane) const noexcept{ return m_planeList[plane];}
  void removeHitInPlane( unsigned int plane) noexcept { m_planeList[plane]--; return;}

private:
  unsigned int m_nbFiredLayers;

  unsigned int m_nbFiredLayers_singleHit;
  std::array<unsigned int,12> m_planeList;
  unsigned int m_nbT1X;
  unsigned int m_nbT1X_singleHit;

  unsigned int m_nbT2X;
  unsigned int m_nbT2X_singleHit;

  unsigned int m_nbT3X;
  unsigned int m_nbT3X_singleHit;

  unsigned int m_nbT1UV;
  unsigned int m_nbT1UV_singleHit;

  unsigned int m_nbT2UV;
  unsigned int m_nbT2UV_singleHit;

  unsigned int m_nbT3UV;
  unsigned int m_nbT3UV_singleHit;

  unsigned int m_nbT1;
  unsigned int m_nbT1_singleHit;

  unsigned int m_nbT2;
  unsigned int m_nbT2_singleHit;

  unsigned int m_nbT3;
  unsigned int m_nbT3_singleHit;

  unsigned int m_nbHits;

};
#endif // PRPLANEHYBRIDCOUNTER_H
