/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <algorithm>
#include <array>

// Include files
// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "UTDAQ/UTDAQHelper.h"
#include "PrAddUTHitsTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrAddUTHitsTool
//
// 2016-05-11 : Michel De Cian
//
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( PrAddUTHitsTool )

using ROOT::Math::CholeskyDecomp; // -- Maybe copy locally if not running at CERN?
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrAddUTHitsTool::PrAddUTHitsTool( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ), m_invMajAxProj2( 0.0 ), m_magFieldSvc( nullptr )
{
}
//=============================================================================
// Destructor
//=============================================================================
PrAddUTHitsTool::~PrAddUTHitsTool() {}

//=========================================================================
//
//=========================================================================
StatusCode PrAddUTHitsTool::initialize()
{
  StatusCode sc = GaudiTool::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;         // error printed already by GaudiAlgorithm
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Initialize" << endmsg;

  m_magFieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );

  m_invMajAxProj2 = 1 / ( p_majAxProj * p_majAxProj );

  m_utDet = getDet<DeUTDetector>(DeUTDetLocation::UT);
  // Make sure we precompute z positions/sizes of the layers/sectors
  registerCondition(m_utDet->geometry(), &PrAddUTHitsTool::recomputeGeometry);

  return StatusCode::SUCCESS;
}

StatusCode PrAddUTHitsTool::recomputeGeometry() {
  LHCb::UTDAQ::computeGeometry(*m_utDet, m_layers, m_sectorsZ);
  return StatusCode::SUCCESS;
}

//=========================================================================
//  Add the TT hits on the track, only the ids.
//=========================================================================
StatusCode PrAddUTHitsTool::addUTHits( Track& track ) const
{

  LHCb::State state = track.closestState( p_zUTProj );
  double chi2 = 0;

  UT::Mut::Hits myUTHits = returnUTHits( state, chi2, track.p() );

  // -- Only add hits if there are 3 or more
  if ( myUTHits.size() < 3 ) return StatusCode::SUCCESS;

  for ( const auto& hit : myUTHits ) {

    // ----------------------------------
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "--- Adding Hit in Layer: " << hit.HitPtr->planeCode() << " with projection: " << hit.projection << endmsg;
    // ----------------------------------

    track.addToLhcbIDs( hit.HitPtr->lhcbID() );
  }

  m_hitsAddedCounter += myUTHits.size();
  m_tracksWithHitsCounter++;

  return StatusCode::SUCCESS;
}
//=========================================================================
//  Return the TT hits
//=========================================================================
UT::Mut::Hits PrAddUTHitsTool::returnUTHits( LHCb::State& state, double& finalChi2, double p ) const
{
  // -- If no momentum is given, use the one from the state
  if ( p < 1e-10 ) {
    p = state.p();
  }

  UT::Mut::Hits UTHits;
  UTHits.reserve(4);

  double bestChi2 = p_maxChi2Tol + p_maxChi2Slope / ( p - p_maxChi2POffset );
  double chi2     = 0.;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "--- Entering returnUTHits ---" << endmsg;

  // -- Get the container with all the hits compatible with the tack
  UT::Mut::Hits selected = selectHits( state, p);
  // -- If only two hits are selected, end algorithm
  if ( selected.size() < 3 ) {
    UTHits    = selected;
    finalChi2 = 0;
    return UTHits;
  }

  std::sort( selected.begin(), selected.end(), UT::Mut::IncreaseByProj );

  // -- Loop over all hits and make "groups" of hits to form a candidate
  for ( auto itBeg = selected.cbegin(); itBeg + 2 < selected.end() ; ++itBeg ) {

    const double firstProj = ( *itBeg ).projection;
    UT::Mut::Hits goodUT;
    goodUT.reserve(4);
    int nbPlane = 0;
    std::array<int, 4> firedPlanes{};
    auto itEnd = itBeg;

    // -- If |firstProj| > m_majAxProj, the sqrt is ill defined
    double maxProj = firstProj;
    if ( fabs( firstProj ) < p_majAxProj ) {
      // -- m_invMajAxProj2 = 1/(m_majAxProj*m_majAxProj), but it's faster like this
      maxProj = firstProj + sqrt( p_minAxProj * p_minAxProj * ( 1 - firstProj * firstProj * m_invMajAxProj2 ) );
    }

    // -- This means that there would be less than 3 hits, which does not work, so we can skip this right away
    if ( ( *( itBeg + 2 ) ).projection > maxProj ) continue;

    // -- Make "group" of hits which are within a certain distance to the first hit of the group
    while ( itEnd != selected.end() ) {

      if ( ( *itEnd ).projection > maxProj ) break;

      if ( 0 == firedPlanes[( *itEnd ).HitPtr->planeCode()] ) {
        firedPlanes[( *itEnd ).HitPtr->planeCode()] = 1; // -- Count number of fired planes
        ++nbPlane;
      }

      goodUT.push_back( *itEnd++ );
    }

    if ( 3 > nbPlane ) continue; // -- Need at least hits in 3 planes
    // -- group of hits has to be at least as large than best group at this stage
    if ( UTHits.size() > goodUT.size() ) continue;

    // ----------------------------------
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "Start fit, first proj " << firstProj << " nbPlane " << nbPlane << " size " << goodUT.size() << endmsg;
    // ----------------------------------

    // -- Set variables for the chi2 calculation

    double dist = 0;
    chi2        = 1.e20;

    calculateChi2( chi2, bestChi2, dist, p, goodUT );

    // -- If this group has a better chi2 than all the others
    // -- and is at least as large as all the others, then make this group the new candidate
    if ( bestChi2 > chi2 && goodUT.size() >= UTHits.size() ) {

      // ----------------------------------
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) printInfo( dist, chi2, state, goodUT );
      // ----------------------------------
      UTHits   = goodUT;
      bestChi2 = chi2;
    }
  }

  // -- Assign the final hit container and chi2 to the variables which are returned.
  finalChi2 = bestChi2;

  return UTHits;
}
//=========================================================================
// Select the hits in a certain window
//=========================================================================
UT::Mut::Hits PrAddUTHitsTool::selectHits( const LHCb::State& state, const double p) const
{

  // -- Define the tolerance parameters
  const double yTol = p_yTolSlope / p;
  const double xTol = p_xTol + p_xTolSlope / p;
  UT::Mut::Hits selected;
  selected.reserve(10);

  // -- Define the parameter that describes the bending
  // -- in principle the call m_magFieldSvc->signedRelativeCurrent() is not needed for every track...
  const double bendParam = p_utParam * -1 * m_magFieldSvc->signedRelativeCurrent() * state.qOverP();

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "State z " << state.z() << " x " << state.x() << " y " << state.y() << " tx " << state.tx() << " ty "
            << state.ty() << " p " << p << endmsg;

  const double stateX  = state.x();
  const double stateZ  = state.z();
  const double stateTy = state.ty();
  const double stateY  = state.y();
  const double stateTx = state.tx();

  boost::container::small_vector<std::pair<int, int>,9> sectors;

  for ( int iStation = 0; iStation < 2; ++iStation ) {
    for ( int iLayer = 0; iLayer < 2; ++iLayer ) {

      const unsigned int layerIndex = 2*iStation + iLayer;
      const float zLayer = m_layers[layerIndex].z;
      const double yPredLay = stateY + ( zLayer - stateZ ) * stateTy;
      const double xPredLay = stateX + ( zLayer - stateZ ) * stateTx + bendParam * ( zLayer - p_zUTField );

      LHCb::UTDAQ::findSectors(layerIndex, xPredLay, yPredLay, xTol, yTol, m_layers[layerIndex], sectors);
      std::pair prevSector{-1, -1};
      for (auto& sector : sectors) {
        // sectors can be duplicated in the list, but they are ordered
        if (sector == prevSector) continue;
        prevSector = sector;
        for (auto& hit : m_HitHandler.get()->hits( iStation+1, iLayer+1, sector.first, sector.second ) ) {
          const double yPred = stateY + ( hit.zAtYEq0() - stateZ ) * stateTy;

          if ( !hit.isYCompatible( yPred, yTol ) ) continue;

          const auto y  = stateY + ( hit.zAtYEq0() - stateZ ) * stateTy ;
          auto xx =  hit.xAt(y);

          const double xPred = stateX + ( hit.zAtYEq0() - stateZ ) * stateTx + bendParam * ( hit.zAtYEq0() - p_zUTField );
          if ( xx > xPred + xTol ) break;
          if ( xx < xPred - xTol ) continue;

          const double projDist = ( xPred - xx) * ( p_zUTProj - p_zMSPoint ) / ( hit.zAtYEq0() - p_zMSPoint );
          selected.emplace_back(&hit, xx, hit.zAtYEq0(), projDist, Tf::HitBase::UsedByPatMatch);
        }
      }
      // -- would not have hits in 3 layers like this
      if ( iStation == 1 && selected.empty() ) break;
    }
  }
  return selected;
}
//=========================================================================
// Calculate Chi2
//=========================================================================
void PrAddUTHitsTool::calculateChi2( double& chi2, const double& bestChi2, double& finalDist, const double& p, UT::Mut::Hits& goodUT ) const
{

  // -- Fit a straight line to the points and calculate the chi2 of the hits with respect to the fitted track

  UT::Mut::Hits::iterator worst;

  double dist = 0;
  chi2        = 1.e20;

  const double xTol        = p_xTol + p_xTolSlope / p;
  const double fixedWeight = 9. / ( xTol * xTol );

  unsigned int       nHits         = goodUT.size();
  const unsigned int maxIterations = nHits;
  unsigned int       counter       = 0;

  // -- Loop until chi2 has a reasonable value or no more outliers can be removed to improve it
  // -- (with the counter as a sanity check to avoid infinite loops).

  unsigned int nDoF = 0;
  std::array<unsigned int, 4> differentPlanes;
  differentPlanes.fill( 0 );
  double worstDiff = -1.0;
  double mat[6], rhs[3];

  mat[0] = fixedWeight; // -- Fix X = 0 with fixedWeight
  mat[1] = 0.;
  mat[2] = fixedWeight * ( p_zUTProj - p_zMSPoint ) *
           ( p_zUTProj - p_zMSPoint ); // -- Fix slope by point at multiple scattering point
  mat[3] = 0.;
  mat[4] = 0.;
  mat[5] = fixedWeight; // -- Fix Y = 0 with fixedWeight
  rhs[0] = 0.;
  rhs[1] = 0.;
  rhs[2] = 0.;

  for ( const auto& ut : goodUT ) {
    const double w     = ut.HitPtr->weight();
    const double dz    = ut.z - p_zUTProj;
    const double t     = ut.HitPtr->sinT();
    const double dist2 = ut.projection;
    mat[0] += w;
    mat[1] += w * dz;
    mat[2] += w * dz * dz;
    mat[3] += w * t;
    mat[4] += w * dz * t;
    mat[5] += w * t * t;
    rhs[0] += w * dist2;
    rhs[1] += w * dist2 * dz;
    rhs[2] += w * dist2 * t;

    if ( 0 == differentPlanes[ut.HitPtr->planeCode()]++ ) ++nDoF;
  }

  // -- Loop to remove outliers
  // -- Don't loop more often than number of hits in the selection
  // -- The counter protects infinite loops in very rare occasions.
  while ( chi2 > 1e10 && counter < maxIterations ) {

    worstDiff = -1.0;

    // -- This is needed since 'CholeskyDecomp' overwrites rhs
    // -- which is needed later on
    const double saveRhs[3] = {rhs[0], rhs[1], rhs[2]};

    CholeskyDecomp<double, 3> decomp( mat );
    if ( UNLIKELY( !decomp ) ) {
      chi2 = 1e42;
      break;
    } else {
      decomp.Solve( rhs );
    }

    const double offset  = rhs[0];
    const double slope   = rhs[1];
    const double offsetY = rhs[2];

    rhs[0] = saveRhs[0];
    rhs[1] = saveRhs[1];
    rhs[2] = saveRhs[2];

    chi2 = fixedWeight * ( offset * offset + offsetY * offsetY +
                           ( p_zUTProj - p_zMSPoint ) * ( p_zUTProj - p_zMSPoint ) * slope * slope );

    for ( auto itSel = goodUT.begin(); goodUT.end() != itSel; ++itSel ) {
      const auto ut = *itSel;
      const double   w  = ut.HitPtr->weight();
      const double   dz = ut.z - p_zUTProj;
      dist              = ut.projection - offset - slope * dz - offsetY * ut.HitPtr->sinT();
      if ( ( 1 < differentPlanes[ut.HitPtr->planeCode()] || nDoF == nHits ) && worstDiff < w * dist * dist ) {
        worstDiff = w * dist * dist;
        worst     = itSel;
      }
      chi2 += w * dist * dist;
    }

    chi2 /= nDoF;

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) && worstDiff > 0. ) {
      info() << format( " chi2 %10.2f nDoF%2d wors %8.2f proj %6.2f offset %8.3f slope %10.6f offsetY %10.6f", chi2,
                        nDoF, worstDiff, ( *worst ).projection, offset, slope, offsetY )
             << endmsg;
    }

    // -- Remove last point (outlier) if bad fit...
    if ( worstDiff > 0. && bestChi2 < chi2 && nHits > 3 ) {

      const auto ut    = *worst;
      const double   w     = ut.HitPtr->weight();
      const double   dz    = ut.z - p_zUTProj;
      const double   t     = ut.HitPtr->sinT();
      const double   dist2 = ut.projection;
      mat[0] -= w;
      mat[1] -= w * dz;
      mat[2] -= w * dz * dz;
      mat[3] -= w * t;
      mat[4] -= w * dz * t;
      mat[5] -= w * t * t;
      rhs[0] -= w * dist2;
      rhs[1] -= w * dist2 * dz;
      rhs[2] -= w * dist2 * t;

      if ( 1 == differentPlanes[ut.HitPtr->planeCode()]-- ) --nDoF;
      --nHits;

      goodUT.erase( worst );
      chi2 = 1.e11; // --Start new iteration
    }

    // -- Increase the sanity check counter
    ++counter;
  }

  finalDist = dist;
}

//=========================================================================
// Print out info
//=========================================================================
void PrAddUTHitsTool::printInfo( double dist, double chi2, const LHCb::State& state, const UT::Mut::Hits& goodUT ) const
{

  // -- Print some information at the end
  info() << "*** Store this candidate, nbTT = " << goodUT.size() << " chi2 " << chi2 << endmsg;
  for ( const auto& ut : goodUT ) {
    double z     = ut.z;
    double mPred = ut.x + dist;
    info() << ut.HitPtr->planeCode() << format( " z%7.0f  x straight %7.2f pred %7.2f  x %7.2f diff %7.2f ", z,
                                         state.x() + state.tx() * ( z - state.z() ), mPred, ut.HitPtr->xAtYMid(), dist )
           << endmsg;
  }
}
