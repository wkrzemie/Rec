/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONTRACKALIHMONITOR_H
#define MUONTRACKALIGMONITOR_H 1

#include <vector>
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Consumer.h"
#include "Event/MuonPID.h"

/** @class MuonTrackAligMonitor MuonTrackAligMonitor.h
 *
 *
 *  @author
 *  @date   2009-02-25
 */
struct ITrackExtrapolator;
struct ITrackChi2Calculator;
class DeMuonDetector;

class MuonTrackAligMonitor final
    : public Gaudi::Functional::Consumer<
          void(const LHCb::MuonPIDs &, const LHCb::Tracks &),
          Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {

public:
  MuonTrackAligMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void operator()(const LHCb::MuonPIDs&, const LHCb::Tracks&) const override;

private:
  AIDA::IHistogram1D  *m_h_chi2,  *m_h_p;
  std::vector<AIDA::IHistogram1D*> m_h_resxL_a,m_h_resyL_a,m_h_resxL_c,m_h_resyL_c;
  std::vector<AIDA::IHistogram1D*> m_h_resxM_a,m_h_resyM_a,m_h_resxM_c,m_h_resyM_c;
  AIDA::IHistogram2D  *m_h_xy, *m_h_txty;

  AIDA::IProfile1D *m_p_resxx, *m_p_resxy, *m_p_resxtx,*m_p_resxty, *m_p_restxx, *m_p_restxy, *m_p_restxtx,
    *m_p_restxty, *m_p_resyx, *m_p_resyy, *m_p_resytx, *m_p_resyty, *m_p_restyx, *m_p_restyy, *m_p_restytx, *m_p_restyty,
    *m_resxhsL, *m_resyhsL,*m_resxhsM, *m_resyhsM;

  DeMuonDetector* m_muonDet;
  bool m_LongToMuonMatch{true};
  bool m_IsCosmics{false};
  double m_pCut{0.};
  double m_chi2nCut{3};
  double m_chi2matchCut{10};
  double m_zM1;
  std::string m_histoLevel{"OfflineFull"};

  bool m_notOnline{true};
  bool m_expertMode;

  ToolHandle<ITrackExtrapolator> m_extrapolator{"TrackMasterExtrapolator", this};
  ToolHandle<ITrackChi2Calculator> m_chi2Calculator{"TrackChi2Calculator", this};
};

#endif // MUONTRACKALIGMONITOR_H
