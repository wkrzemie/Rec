/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCH_DEFINITIONS_H
#define MUONMATCH_DEFINITIONS_H 1

// STL
#include <vector>

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// LHCb
#include "Event/State.h"
#include "Event/Track.h"


namespace MuonMatch {

  /// Track aliases
  using Track  = LHCb::Event::v2::Track;
  using Tracks = std::vector<Track>;

  /// Definition of the muon chamber indexes.
  enum MuonChamber { M2, M3, M4, M5 };

  /// Definition of the track types
  enum TrackType { VeryLowP, LowP, MediumP, HighP };

  /// Return the track type associated to a given momentum
  inline TrackType trackTypeFromMomentum( double p )
  {
    // The following quantities have been set from the references
    // for offline muons in the MuonID, giving room for resolution.

    if ( p < 2.5*Gaudi::Units::GeV ) // 3 GeV/c for offline muons
      return VeryLowP;
    else if ( p < 7*Gaudi::Units::GeV ) // 6 GeV/c for offline muons
      return LowP;
    else if ( p < 12*Gaudi::Units::GeV )  // 10 GeV/c for offline muons
      return MediumP;
    else
      return HighP;
  }

  /** @class Hit Definitions.h
   *  Store the values required for the "kink" method.
   *
   *  @author Miguel Ramos Pernas
   *  @date   2017-10-05
   */

  struct Hit {
    double x, dx, y, dy, z;
  };

  using Hits = std::vector<Hit>;

  /** @class MagnetPositionXZ Definitions.h
   *  Magnet position in X and Z.
   *
   *  @author Miguel Ramos Pernas
   *  @date   2017-10-05
   */

  struct MagnetPositionXZ {
    double x, z;
  };

  /// Return the sine of the angle of the track with respect to "z"
  double sinTrack( const LHCb::State* state )
  {
    return sqrt(1. - 1./(1. + state->tx()*state->tx() + state->ty()*state->ty()));
  }

  /// Calculate the projection in the magnet for the "x" axis
  double xStraight( const LHCb::State* state, double z )
  {
    double dz = z - state->z();
    return state->x() + dz*state->tx();
  }

  /// Calculate the projection in the magnet for the "y" axis
  double yStraight( const LHCb::State* state, double z )
  {
    double dz = z - state->z();
    return state->y() + dz*state->ty();
  }

  /// Calculate the projection in the magnet for the "y" axis
  std::pair<double, double> yStraightWindow( const LHCb::State* state, double z, double yw )
  {
    double dz = z - state->z();
    double y  = state->y() + dz*state->ty();
    double r  = dz*sqrt(state->errTy2()) + yw;

    return {y - r, y + r};
  }

}

#endif // MUONMATCH_DEFINITIONS_H
