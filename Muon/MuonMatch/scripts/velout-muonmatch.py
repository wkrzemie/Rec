#!/usr/bin/env python
###############################################################################
# (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Script to compute different quantities related to the VeloUT-Muon matching
algorithm. To be run with Brunel as:

$ lb-run Brunel/<version> python velout-muonmatch.py <options>
'''

__author__ = 'Miguel Ramos Pernas'
__email__  = 'miguel.ramos.pernas@cern.ch'

# Root
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

# Gaudi
from Configurables import (
    Brunel,
    GaudiSequencer, HistogramPersistencySvc,
    FTRawBankDecoder,
    PrForwardTool, PrVeloUT,
    )
from Gaudi.Configuration import appendPostConfigAction
from GaudiPython import AppMgr, PyAlgorithm, SUCCESS
from PRConfig import TestFileDB

# Python
import argparse
import array
import IPython
import numpy as np
import os
import pandas
import root_numpy
import warnings
from collections import namedtuple
from scipy.stats import chi2
from traitlets.config.loader import Config

# Output file to store the n-tuple
__output_file__ =  __file__[:-3] + '-ntuple.root'

# Classes needed to store ROOT objects, preventing the python garbage collector
# to destroy them.
FitConvPdf = namedtuple('FitConvPdf', ['function', 'pdfs'])
FitPdf     = namedtuple('FitPdf', ['function', 'vars'])
FitModel   = namedtuple('FitModel', ['pdf', 'dataset', 'fitvar'])


def find_member( sequence, member ):
    '''
    Find a member in a sequence.
    '''
    gs = GaudiSequencer.allConfigurables[sequence]

    for m in gs.Members:
        if m.name() == member:
            return m

    raise LookupError('Unable to find member "{}" in "{}"'.format(member, sequence))


def find_tool( sequence, member, tool ):
    '''
    Find the tool in the given sequence and member name.
    '''
    m = find_member(sequence, member)

    for t in m.getTools():
        if t.name() == '{}.{}'.format(member, tool):
            return t

    raise LookupError('Unable to find tool "{}" in member "{}" from sequence "{}"'.format(tool, member, sequence))


def get_component( ancestors, ttype ):
    '''
    Get the component with track type "ttype" in a list of
    ancestors.
    '''
    for t in ancestors:
        if t.type() == ttype:
            return t


def get_long_track_seed( long_track, container ):
    '''
    Get the seed track which was used to create the long track
    from a container.
    '''
    for seed in container:
        if long_track.containsLhcbIDs(seed):
            return seed

    return None


def track_ip( track, vertices ):
    '''
    Calculate the smallest impact parameter of a track with respect to
    a set of vertices.
    '''
    best = np.infty
    for v in vertices:

        p = v.position()

        s = track.closestState(p.Z())
        a = s.position()
        n = s.slopes().unit()

        u = a - p

        ip = (u - n*(u.x()*n.x() + u.y()*n.y() + u.z()*n.z())).r()

        if ip < best:
            best = ip

    return best


def tracks_matching( evtsvc, tracks, abspid ):
    '''
    Get the tracks matching a certain MCParticle absolute PID.
    '''
    from GaudiPython.Bindings import gbl
    from LinkerInstances.eventassoc import linkedTo
    Track = gbl.LHCb.Track
    MCParticle = gbl.LHCb.MCParticle
    LT = linkedTo(MCParticle, Track, 'Rec/Track/Best')

    output = []
    if LT.notFound():
        return output

    for track in tracks:
        for mcpart in LT.range(track):
            if mcpart.particleID().abspid() == abspid:
                output.append(track)
                break

    return output


def start_ipython( **kwargs ):
    '''
    Function to start an ipython session, passing all the values
    specified in "kwargs" to the IPython environment.
    '''
    cfg = Config()
    cfg.TerminalInteractiveShell.banner1 = ''

    IPython.embed(config=cfg, user_ns=kwargs)


class VeloUTMuonMatchTuple(PyAlgorithm):

    def __init__( self, gaudi, match_tracks ):
        '''
        Build the class.
        '''
        self.gaudi = gaudi

        self.match_tracks = match_tracks

        super(VeloUTMuonMatchTuple, self).__init__()

    def _dump_buffer( self ):
        '''
        Dump the current buffer into the output tree.
        '''
        root_numpy.array2tree(self.buffer, tree=self.tree)
        self._new_buffer()

    def _new_buffer( self ):
        '''
        Create a new buffer.
        '''
        self.buffer = np.empty(0, dtype=[
                ('Long_EndVelo_P', np.float64),
                ('Long_EndVelo_PT', np.float64),
                ('Long_EndVelo_x', np.float64),
                ('Long_EndVelo_z', np.float64),
                ('Long_EndVelo_tx', np.float64),
                ('Long_AtT_P', np.float64),
                ('Long_AtT_PT', np.float64),
                ('Long_AtT_x', np.float64),
                ('Long_AtT_z', np.float64),
                ('Long_AtT_tx', np.float64),
                ('Long_P', np.float64),
                ('Long_PT', np.float64),
                ('Long_zf', np.float64),
                ('Long_IP', np.float64),
                ('VeloUT_EndVelo_P', np.float64),
                ('VeloUT_EndVelo_PT', np.float64),
                ('VeloUT_EndVelo_x', np.float64),
                ('VeloUT_EndVelo_z', np.float64),
                ('VeloUT_EndVelo_tx', np.float64),
                ('VeloUT_P', np.float64),
                ('VeloUT_PT', np.float64),
                ('VeloUT_zf', np.float64),
                ('VeloUT_IP', np.float64),
                ])

    def initialize( self ):
        '''
        Initialize the algorithm.
        '''
        self.file = ROOT.TFile.Open(__output_file__, 'recreate')
        self.tree = ROOT.TTree('tracks', 'tracks', 0)
        self.pdg  = ROOT.TDatabasePDG()

        self._new_buffer()

        return super(VeloUTMuonMatchTuple, self).initialize()

    def execute( self ):
        '''
        Execute the algorithm.
        '''
        evtsvc = self.gaudi.evtsvc()

        long_tracks   = evtsvc['Rec/Track/Best']
        velout_tracks = evtsvc['Rec/Track/Keyed/Upstream']
        vertices      = evtsvc['Rec/Vertex/Primary']

        if not long_tracks or not velout_tracks or not vertices:
            return SUCCESS

        # TODO: match muon tracks
        # We are only interested in muon tracks
        if self.match_tracks:
            long_tracks = tracks_matching(evtsvc, long_tracks, self.pdg.GetParticle('mu-').PdgCode())

        for i, track in enumerate(long_tracks):
            # Skip non-long tracks
            if track.type() != ROOT.LHCb.Track.Long:
                continue

            # Skip tracks without VELO, UT or T-stations information
            if not all([track.hasT(), track.hasUT(), track.hasVelo()]):
                continue

            velout_track = get_long_track_seed(track, velout_tracks)

            if not velout_track:
                continue

            velo_state_from_velout = velout_track.stateAt(ROOT.LHCb.State.EndVelo)

            velo_state  = track.stateAt(ROOT.LHCb.State.EndVelo)
            t_state     = track.stateAt(ROOT.LHCb.State.AtT)

            focal_plane_z = lambda v_x, v_z, v_tx, t_x, t_z, t_tx: (v_x - t_x - v_tx*v_z + t_tx*t_z)*1./(t_tx - v_tx)

            # Calculate the value of the focal plane in "z" for the long track,
            # using the state at the VELO from the long track.
            v_x  = velo_state.x()
            v_z  = velo_state.z()
            v_tx = velo_state.tx()

            t_x  = t_state.x()
            t_z  = t_state.z()
            t_tx = t_state.tx()

            long_zf = focal_plane_z(v_x, v_z, v_tx, t_x, t_z, t_tx)

            # Calculate the value of the focal plane in "z" using values from
            # the VeloUT track. This is the value that actually matters,
            # since it is the one that will be used in the algorithm. The
            # values of the state at the T stations are taken from the
            # long track.
            vu_x  = velo_state_from_velout.x()
            vu_z  = velo_state_from_velout.z()
            vu_tx = velo_state_from_velout.tx()

            velout_zf = focal_plane_z(vu_x, vu_z, vu_tx, t_x, t_z, t_tx)

            # Calculate the impact parameters
            long_ip   = track_ip(track, vertices)
            velout_ip = track_ip(velout_track, vertices)

            # Fill array with the values
            values = np.empty(1, dtype=self.buffer.dtype)

            values['Long_EndVelo_P']    = velo_state.p()
            values['Long_EndVelo_PT']   = velo_state.pt()
            values['Long_EndVelo_x']    = v_x
            values['Long_EndVelo_z']    = v_z
            values['Long_EndVelo_tx']   = v_tx
            values['Long_AtT_P']        = t_state.p()
            values['Long_AtT_PT']       = t_state.pt()
            values['Long_AtT_x']        = t_x
            values['Long_AtT_z']        = t_z
            values['Long_AtT_tx']       = t_tx
            values['Long_P']            = track.p()
            values['Long_PT']           = track.pt()
            values['Long_zf']           = long_zf
            values['Long_IP']           = long_ip
            values['VeloUT_EndVelo_P']  = velo_state_from_velout.p()
            values['VeloUT_EndVelo_PT'] = velo_state_from_velout.pt()
            values['VeloUT_EndVelo_x']  = vu_x
            values['VeloUT_EndVelo_z']  = vu_z
            values['VeloUT_EndVelo_tx'] = vu_tx
            values['VeloUT_P']          = velout_track.p()
            values['VeloUT_PT']         = velout_track.pt()
            values['VeloUT_zf']         = velout_zf
            values['VeloUT_IP']         = velout_ip

            # Add information to the buffer, and update tree if necessary
            self.buffer = np.concatenate([self.buffer, values])

        if len(self.buffer) % 1000000 == 0:
            self._dump_buffer()

        return SUCCESS

    def finalize( self ):
        '''
        Finalize the algorithm.
        '''
        if len(self.buffer):
            self._dump_buffer()

        self.tree.AutoSave()

        self.file.Close()

        return super(VeloUTMuonMatchTuple, self).finalize()


def access_ntuple():
    '''
    Access the n-tuple created by the "create_ntuple" mode. Raise an error
    if the file does not exist.
    '''
    if not os.path.isfile(__output_file__):
        raise RuntimeError('Unable to find input file "{}"; run mode "create" before to create it'.format(__output_file__))

    return root_numpy.root2array(__output_file__, 'tracks')


def create_ntuple( nevts, tfdb, printfreq, interactive ):
    '''
    Main function to call.
    '''
    HistogramPersistencySvc().OutputFile = __file__[:-3] + '-Brunel-histos.root'

    # Configure Brunel
    brunel = Brunel()
    brunel.PrintFreq = printfreq
    brunel.EvtMax = nevts
    brunel.OutputType = 'NONE'

    brunel.MainSequence = ['ProcessPhase/Reco', 'ProcessPhase/MCLinks', 'ProcessPhase/Check']

    from Configurables import RecombineRawEvent
    RecombineRawEvent().Version = 4.2

    # Change decoding version of "FT" clusters
    def post_config():

        FTRawBankDecoder('createFTClusters').DecodingVersion = 2

        # Remove tracking cuts
        prVeloUT = find_member('RecoTrFastSeq', 'PrVeloUTFast')
        prVeloUT.minPT = 0

        prForward = find_tool('RecoTrFastSeq', 'PrForwardTrackingFast', 'PrForwardTool')
        prForward.PreselectionPT = 0
        prForward.MinPT = 0

        # Disable useless sequences
        GaudiSequencer('RecoRICHSeq').Members       = []
        GaudiSequencer('RecoCALOSeq').Members       = []
        GaudiSequencer('CheckRICHSeq').Members      = []
        GaudiSequencer('RecoPROTOSeq').Members      = []
        GaudiSequencer('RecoSUMMARYSeq').Members    = []
        GaudiSequencer('MoniSeq').Members           = []
        GaudiSequencer('RecoCALOFUTURESeq').Members = []

    appendPostConfigAction(post_config)

    # The input files and tags are taken from the test file
    files_proxy = TestFileDB.test_file_db[tfdb]
    files_proxy.run(configurable=brunel)

    # We have to set the input type (it seems it is not set by TestFileDB)
    f = files_proxy.qualifiers['Format']
    if f == 'LDST':
        f = 'DST'
    brunel.InputType = f

    if brunel.InputType == 'DST':
        brunel.SplitRawEventInput = 4.3

    brunel.Simulation = files_proxy.qualifiers['Simulation']
    if brunel.Simulation:
        brunel.WithMC = True

    # Initialize the application manager
    gaudi = AppMgr()

    algo = VeloUTMuonMatchTuple(gaudi, match_tracks=brunel.Simulation)

    gaudi.addAlgorithm(algo)

    # Execute the application manager over the desired number of events
    gaudi.initialize()
    gaudi.run(nevts)

    if interactive:
        start_ipython(gaudi=gaudi)

    gaudi.stop()
    gaudi.finalize()


def parameters( batch, interactive ):
    '''
    Calculate the VeloUT-muon matching related parameters.
    '''
    data = access_ntuple()

    if batch:
        ROOT.gROOT.SetBatch(batch)

    # Fit to a polynomial
    deg = 2

    p, c = np.polyfit(data['VeloUT_EndVelo_tx'], data['VeloUT_zf'], deg, cov=True)

    pars = np.array(zip(p, np.sqrt(np.diagonal(c))))

    print('Parameters in fit:')
    for i, (p, s) in enumerate(pars):
        print(' {:<{}} = {:.2e} +- {:.2e}'.format('x^{}'.format(deg - i), len(str(deg)), p, s))

    canvas = ROOT.TCanvas('canvas', 'Magnet focal plane estimation in Z', 1200, 800)

    x = array.array('d', data['VeloUT_EndVelo_tx'])
    y = array.array('d', data['VeloUT_zf'])

    # Draw data points
    graph = ROOT.TGraph(len(x), x, y)
    graph.SetName('zf_vs_tx')
    graph.SetTitle('Focal plane estimation')

    graph.GetXaxis().SetRangeUser(-0.3, +0.3)
    graph.GetYaxis().SetRangeUser(4900, 5300)

    graph.GetXaxis().SetTitle('VeloUT t_{x} at EndVelo')
    graph.GetYaxis().SetTitle('Z_{f}')

    graph.Draw('AP')

    # Draw the profile
    profile = ROOT.TProfile('profile_zf_vs_tx', 'Profile for the focal plane estimation', 50, -0.3, +0.3)

    for ix, iy in zip(x, y):
        profile.Fill(ix, iy)

    profile.SetMarkerColor(ROOT.kBlue)

    profile.Draw('SAMEE1')

    # Draw the fitted function
    rpars   = tuple(reversed(pars))
    funcstr = str(rpars[0][0])
    for i, (p, _) in enumerate(rpars[1:]):
        funcstr += '+ {}'.format(p) + (i + 1)*'*x'

    func = ROOT.TF1('polynomial', funcstr, -0.3, +0.3)
    func.SetLineColor(ROOT.kRed)
    func.SetLineWidth(2)

    func.Draw('SAMEC')

    canvas.Print(__file__[:-3] + '-parameters.png')

    if interactive:
        start_ipython(ROOT=ROOT)
    elif not batch:
        raw_input('Press enter to continue')


def resolution( batch, interactive ):
    '''
    Process the created n-tuple and calculate the resolution.
    '''
    data = access_ntuple()

    if batch:
        ROOT.gROOT.SetBatch(batch)

    def fit_var( data, var, name ):
        '''
        Function to fit a variable and save the output.
        '''
        res = (data['VeloUT_EndVelo_' + var] - data['Long_EndVelo_' + var])/data['Long_EndVelo_' + var]

        title = '(VeloUT_{{{0}}} - Long_{{{0}}})/Long_{{{0}}}'.format(name)

        # Fit resolution to a convoluted Landau distribution
        res_var = ROOT.RooRealVar('resolution', title, -1, 3)

        ml = ROOT.RooRealVar('ml', 'mean landau', -4e-2, -0.3, +0.3)
        sl = ROOT.RooRealVar('sl', 'sigma landau', 7e-2, 1e-4, 1)

        landau = ROOT.RooLandau('landau', 'landau model', res_var, ml, sl)

        landau_pdf = FitPdf(landau, [ml, sl])

        mg = ROOT.RooRealVar('mg', 'center gauss', 0)
        sg = ROOT.RooRealVar('sg', 'sigma gauss', 0.1, 1e-4, 1)

        gauss = ROOT.RooGaussian('gauss', 'gaussian model', res_var, mg, sg)

        gauss_pdf = FitPdf(gauss, [mg, sg])

        lxg = ROOT.RooFFTConvPdf('lxg', 'resolution model', res_var, landau, gauss)

        lxg_pdf = FitConvPdf(lxg, [landau_pdf, gauss_pdf])

        # Build data sample

        argset = ROOT.RooArgSet(res_var)

        dataset = ROOT.RooDataSet('data', 'data', argset)

        for v in res:
            if v >= res_var.getMin() and v < res_var.getMax():
                res_var.setVal(v)
                dataset.addFast(argset)

        # Store the whole model in a class
        model = FitModel(lxg_pdf, dataset, res_var)

        # Do the fit
        fit_results = model.pdf.function.fitTo(dataset, ROOT.RooFit.Save(1))

        return {
            'res'         : res,
            'var'         : var,
            'name'        : name,
            'title'       : title,
            'model'       : model,
            'fit_results' : fit_results,
            }

    # Do the fits
    results_p  = fit_var(data, 'P', 'p')
    results_pt = fit_var(data, 'PT', 'p_{t}')

    # Create the canvas
    canvas = ROOT.TCanvas('resolution plots', 'resolution plots', 1400, 800)
    canvas.Divide(2, 2)

    plottables = []
    for i, (results, limit) in enumerate((
            (results_p  , 7e4),
            (results_pt , 4e3),
            )):

        model = results['model']
        name  = results['name']
        var   = results['var']

        def percentile( cdf, var, perc ):
            '''
            Calculate the value of an integration variable associated to
            a percentile of a PDF, given its CDF.
            '''
            for x in np.linspace(var.getMin(), var.getMax(), 10000):
                var.setVal(x)
                v = cdf.getValV(ROOT.RooArgSet(var))
                if v > perc:
                    return x

        cdf = model.pdf.function.createCdf(ROOT.RooArgSet(model.fitvar))

        pl_2s = chi2.sf(4, 1)
        xl_2s = percentile(cdf, model.fitvar, pl_2s)

        pl_1s = chi2.sf(1, 1)
        xl_1s = percentile(cdf, model.fitvar, pl_1s)

        pr_1s = 1. - pl_1s
        xr_1s = percentile(cdf, model.fitvar, pr_1s)

        pr_2s = 1. - pl_2s
        xr_2s = percentile(cdf, model.fitvar, pr_2s)

        model.fitvar.setRange('l_' + var, xl_2s, xl_1s)
        model.fitvar.setRange('m_' + var, xl_1s, xr_1s)
        model.fitvar.setRange('r_' + var, xr_1s, xr_2s)

        print('********** Fit results for variable "{}" **********'.format(results['var']))
        results['fit_results'].Print()

        c = percentile(cdf, model.fitvar, 0.5)

        out  = 'Median at: {:.3f}\n'.format(c)
        out += 'Intervals:\n'
        out += ' 2-sigma-left  ({:>2.0f} %): {:.3f}\n'.format(100*pl_2s, c - xl_2s)
        out += ' 1-sigma-left  ({:>2.0f} %): {:.3f}\n'.format(100*pl_1s, c - xl_1s)
        out += ' 1-sigma-right ({:>2.0f} %): {:.3f}\n'.format(100*pr_1s, xr_1s - c)
        out += ' 2-sigma-right ({:>2.0f} %): {:.3f}\n'.format(100*pr_2s, xr_2s - c)

        print(out)

        # Draw the resolution and the fit result
        canvas.cd(2*i + 1)
        frame = model.fitvar.frame(ROOT.RooFit.Title('resolution ({})'.format(name)))
        model.dataset.plotOn(frame, ROOT.RooFit.Binning(100))

        cfg = (
            ROOT.RooFit.NormRange('all'),
            ROOT.RooFit.FillStyle(3018),
            ROOT.RooFit.DrawOption('B'),
            ROOT.RooFit.Precision(1.e-5),
            )

        model.pdf.function.plotOn(frame,
                                  ROOT.RooFit.Range('l_' + var),
                                  ROOT.RooFit.FillColor(ROOT.kGreen + 2),
                                  *cfg
                                  )
        model.pdf.function.plotOn(frame,
                                  ROOT.RooFit.Range('m_' + var),
                                  ROOT.RooFit.FillColor(ROOT.kOrange),
                                  *cfg
                                  )
        model.pdf.function.plotOn(frame, ROOT.RooFit.Range('r_' + var),
                                  ROOT.RooFit.FillColor(ROOT.kGreen + 2),
                                  *cfg
                                  )
        model.pdf.function.plotOn(frame)

        frame.Draw()

        plottables.append(frame)

        # Reference variable
        avar = 'Long_EndVelo_' + results['var']

        # Draw the resolution as a function of the long-track momentum
        canvas.cd(2*(i + 1))
        h = ROOT.TH2D(
            'res_vs_mom_{}'.format(var),
            'resolution vs {}'.format(name),
            200, 0, limit,
            200, model.fitvar.getMin(), model.fitvar.getMax(),
            )

        rd = np.empty(len(results['res']), dtype=[(avar, np.float64), ('res', np.float64)])
        rd[avar]  = data[avar]
        rd['res'] = results['res']

        res_cond = np.logical_and(
            rd['res'] >= model.fitvar.getMin(),
            rd['res'] <  model.fitvar.getMax(),
            )

        cut = np.logical_and(rd[avar] < limit, res_cond)

        reduced = rd[cut]

        root_numpy.fill_hist(h, np.array([reduced[avar], reduced['res']]).T)

        h.GetXaxis().SetTitle('Long_{{{}}} (MeV/#it{{c}})'.format(name))
        h.GetYaxis().SetTitle(results['title'])
        h.Draw('ZCOL')

        plottables.append(h)

    canvas.Print(__file__[:-3] + '-resolution.png')

    if interactive:
        start_ipython(ROOT=ROOT)
    elif not batch:
        raw_input('Press enter to continue')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--interactive', '-i', action='store_true', default=False,
                        help='Whether to leave an open ipython session after execution')

    subparsers = parser.add_subparsers(help='Mode to run')

    # Parser for the "create_ntuple" mode
    parser_create_ntuple = subparsers.add_parser('create_ntuple', help=create_ntuple.__doc__)
    parser_create_ntuple.add_argument('--nevts', '-n', type=int, default=1000,
                               help='Number of events to process')
    parser_create_ntuple.add_argument('--printfreq', '-p', type=int, default=100,
                               help='Frequency to display processing messages')
    parser_create_ntuple.add_argument('--tfdb', type=str, default='upgrade-magdown-sim09c-up02-reco-up01-minbias-ldst',
                               help='TestFileDB entry to take the data from')
    parser_create_ntuple.set_defaults(func=create_ntuple)

    # Parser for the "parameters" mode
    parser_parameters = subparsers.add_parser('parameters', help=parameters.__doc__)
    parser_parameters.set_defaults(func=parameters)

    # Parser for the "resolution" mode
    parser_resolution = subparsers.add_parser('resolution', help=resolution.__doc__)
    parser_resolution.set_defaults(func=resolution)

    for p in (parser_parameters, parser_resolution):
        p.add_argument('--batch', '-b', action='store_true',
                       help='Whether to run ROOT in batch mode')

    args = parser.parse_args()

    cfg  = dict(vars(args))
    func = cfg.pop('func')

    func(**cfg)
