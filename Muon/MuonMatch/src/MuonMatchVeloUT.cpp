/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>
#include <numeric>
#include <tuple>
#include <utility>

// local
#include "MuonMatchVeloUT.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonMatchVeloUT
//
// 2010-12-02 : Roel Aaij, Vasileios Syropoulos
// 2017-10-05 : Miguel Ramos Pernas
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MuonMatchVeloUT )

// Some typedefs
namespace {
  using namespace MuonMatch;
  using WindowBounds = std::tuple<double, double, double, double>;
  using getHitValErr = std::function<std::tuple<double, double> ( const Hit& )>;
}

//=============================================================================
//
MuonMatchVeloUT::MuonMatchVeloUT( const std::string& name, ISvcLocator* pSvcLocator ) :
  Transformer(name, pSvcLocator,
	      {
		KeyValue{"SeedInput", LHCb::TrackLocation::VeloUT},
		  KeyValue{"MuonHitsInput", MuonHitHandlerLocation::Default}
	      },
	      KeyValue{"MatchOutput", LHCb::TrackLocation::Match})
{
}

//=============================================================================
//
std::optional<Track> MuonMatchVeloUT::match( const Track& seed, const MuonHitHandler& hit_handler ) const
{
  std::optional<Track> output;

  // Define the track type, if its momentum is too low, stop processing
  const TrackType tt = trackTypeFromMomentum(seed.p());

  if ( tt == VeryLowP ) return output;

  // Get the state at the Velo
  const LHCb::State* state = seed.stateAt(LHCb::State::Location::EndVelo);
  if ( !state )
    return output;

  // Parametrization of the z-position of the magnet’s focal plane as a function
  // of the direction of the velo track "tx2"
  const MagnetPositionXZ magnet = this->magnetFocalPlaneFromTx2(state);

  // To store the best candidate in "SetQOverP" mode
  std::optional<GlobalFitResult> best;

  // The iteration is done over all the possible initial stations. Only the best
  // is kept in case "SetQOverP" is true.
  for ( const auto ist : this->firstMuCh(tt) ) {

    const CommonMuonStation& first = hit_handler.station(ist);

    const auto [xMin, xMax, yMin, yMax] = this->firstStationWindow(seed, state, first, magnet);

    for ( unsigned int r = 0; r < first.nRegions(); ++r ) {

      if ( !first.overlaps(r, xMin, xMax, yMin, yMax) ) continue;

      for ( const auto& mhit : hit_handler.hits(xMin, xMax, ist, r) ) {

	if ( mhit.y() > yMax || mhit.y() < yMin ) continue;

	const auto hit = this->makeMuonHit(mhit);

	const auto corr_magnet = this->corrMagnetFocalPlane(state, hit, magnet);

	// Estimate the momentum.
	// TODO: The estimation is rather big (sometimes x4 what one gets
	// with "seed.p()").
	const auto slope = (hit.x - corr_magnet.x)/(hit.z - corr_magnet.z);
	const auto mom   = this->momentum(slope - state->tx());

	// Filter seeds with too high p_T
	if ( mom*sinTrack(state) > m_maxPt ) continue;

	// Momentum-dependent correction on magnet.z as a function of 1/p in
	// 1/GeV. The parametrization is obtained from MC as a function of the
	// first momentum estimate based on the single seed hit.
	auto correct_error = [](double p_inv, const std::array<double,3>& cor) -> double {

	  return cor[0] + p_inv*(cor[1] + p_inv*cor[2]);
	};

	// Define the "magnet hit".
	const Hit magnet_hit{
	  corr_magnet.x,
          correct_error(Gaudi::Units::GeV/mom, m_errCorrect_mx),
	  yStraight(state, corr_magnet.z),
	  correct_error(Gaudi::Units::GeV/mom, m_errCorrect_my),
	  corr_magnet.z};

	auto hits = matchingHits(magnet_hit, hit);

	// Add hits to the seed using the muon chambers designated for the
	// given track type. Break the iteration if no hits are found
	// in one of the chambers.
	const auto& muchs = this->afterKickMuChs(tt);
	const bool is_good = std::all_of(std::cbegin(muchs), std::cend(muchs),
					 [&] ( const auto& much ) {

					   auto h = this->findHit(much, state, magnet_hit, hit_handler, slope);

					   if ( h ) {
					     hits.emplace_back(*h);
					     return true;
					   }
					   else
					     return false;
					 });

	if ( !muchs.size() || !is_good ) continue;

	// The candidate is fitted if it is a "good" candidate
	const auto fitres = this->fitHits(hits, magnet_hit, state);

	if ( m_setQOverP ) {
	  // Set q/p mode, we have to find the "best" candidate
	  if ( !best ) {
	    // "best" candidate is not defined, check if the incoming
	    // passes the threshold
	    if ( fitres.chi2ndof < m_maxChi2DoFX )
	      best.emplace(fitres);
	  }
	  else if ( fitres.chi2ndof < best->chi2ndof )
	    // The candidate has a smaller chi2/ndof than the "best". Set
	    // as new "best".
	    best.emplace(fitres);
	}
	else {
	  // Do not set q/p
	  if ( fitres.chi2ndof < m_maxChi2DoFX ) {
	    // A candidate is found satisfying the requirements, it is
	    // directly returned

	    output.emplace(seed);
	    output->addToAncestors(seed);

	    return output;
	  }
	}
      }
    }
  }

  if ( best ) {
    //
    // Running with SetQOverP = True
    //
    output.emplace(seed);
    output->addToAncestors(seed);

    // TODO: properly add a new value for the MuonMatching additional info
    //output->addInfo(35, best->slope - bst->tx());

    const double down = m_fieldSvc->isDown() ? -1 : +1;
    const double q = down*((best->slope < state->tx()) - (best->slope > state->tx()));

    for ( auto& s : output->states() )
      s.setQOverP(q/best->p);
  }

  return output;
}

//=============================================================================
//
std::optional<Hit> MuonMatchVeloUT::findHit( const MuonChamber &much,
					     const LHCb::State* state,
					     const Hit& magnet_hit,
					     const MuonHitHandler& hit_handler,
					     const double slope ) const
{

  // First hit is in magnet
  const MagnetPositionXZ magnet{magnet_hit.x, magnet_hit.z};

  // Get the station we are looking at.
  const CommonMuonStation& station = hit_handler.station(much);

  const auto [xMin, xMax, yMin, yMax] = this->stationWindow(state, station, magnet, slope);

  // Look for the closest hit inside the search window
  std::optional<Hit> closest;

  double minDist2 = 0;

  for ( unsigned int r = 0; r < station.nRegions(); ++r ) {

    if ( !station.overlaps(r, xMin, xMax, yMin, yMax) ) continue;

    for ( const auto& mhit : hit_handler.hits(xMin, xMax, much, r) ) {

      if ( mhit.y() > yMax || mhit.y() < yMin ) continue;

      const Hit hit = this->makeMuonHit(mhit);

      const auto yMuon = yStraight(state, hit.z);

      const auto resx = (magnet.x + (hit.z - magnet.z)*slope) - hit.x;
      const auto resy = yMuon - hit.y;

      // Variable to define the closest hit
      const double dist2 = (resx*resx + resy*resy)/(hit.dx*hit.dx + hit.dy*hit.dy);

      if ( !closest || dist2 < minDist2 ) {
	closest.emplace(hit);
	minDist2 = dist2;
      }
    }
  }

  return closest;
}

//=============================================================================
//
inline WindowBounds MuonMatchVeloUT::firstStationWindow( const Track& track,
							 const LHCb::State* state,
							 const CommonMuonStation& station,
							 const MagnetPositionXZ& magnet ) const {

  const auto& [xw, yw] = m_window[station.station()];

  // Calculate window in y. In y I just extrapolate in a straight line.
  const auto [yMin, yMax] = yStraightWindow(state, station.z(), yw);

  // Calculate window in x
  const double dz = (station.z() - magnet.z);

  double xMin, xMax;

  if ( track.hasUT() ) {

    // Use VeloUT info to get the charge of the seed.

    const int mag = m_fieldSvc->isDown() ? -1 : +1;
    const int charge = track.charge();

    const double tan = charge*mag < 0 ? this->tanMax(track, state) : this->tanMin(track, state);

    xMin = magnet.x + dz*tan;
    xMax = xStraight(state, station.z());
    if ( xMin > xMax )
      std::swap(xMin, xMax);

    xMin -= xw;
    xMax += xw;
  }
  else {
    // If can not estimate the charge, do as in old VeloMuonMatch
    const double tanMin = this->tanMin(track, state);
    xMin = magnet.x + dz*tanMin - xw;
    const double tanMax = this->tanMax(track, state);
    xMax = magnet.x + dz*tanMax + xw;
  }

  return std::make_tuple(xMin, xMax, yMin, yMax);
}

//=============================================================================
//
FitResult MuonMatchVeloUT::fit( const getHitValErr hitValErr,
				const Hit& magnetHit,
				const Hits& hits ) const {

  // Calculate some sums
  double S = 0., Sz = 0., Sc = 0.;
  for ( const auto& hit : hits ) {

    const auto [c, s] = std::invoke(hitValErr, hit);

    const double is2 = 1./(s*s);

    S  += is2;
    Sz += (hit.z - magnetHit.z)*is2;
    Sc += c*is2;
  }
  const double alpha = Sz/S;

  // Calculate the estimate for the slope
  double b = 0., Stt = 0.;
  for ( const auto& hit : hits ) {

    const auto [c, s] = std::invoke(hitValErr, hit);

    const double t_i = (hit.z - magnetHit.z - alpha)/s;

    Stt += t_i*t_i;

    b += t_i*c/s;
  }
  b /= Stt;

  const double a = (Sc - Sz*b)/S;

  // Calculate the chi2
  const double chi2 = std::accumulate(hit_iteration::cbegin(hits),
				      hit_iteration::cend(hits),
				      0.,
				      [&] ( const double prev, const auto& hit ) {

					const auto [c, s] = std::invoke(hitValErr, hit);

					const auto d = (c - a - b*(hit.z - magnetHit.z))/s;

					return prev + d*d;
				      });

  return {a, b, chi2, hits.size() - 2};
}

//=============================================================================
//
GlobalFitResult MuonMatchVeloUT::fitHits( const Hits& hits, const Hit& magnetHit, const LHCb::State* state ) const
{
  // Do the fit in X and also fit Y, or get the chi^2 from the extrapolation.
  auto fitX = fit([] (const Hit& h) {return std::make_tuple(h.x, h.dx);}, magnetHit, hits);
  FitResult fitY;
  if ( m_fitY )
    fitY = fit([] (const Hit& h) {return std::make_tuple(h.y, h.dy);}, magnetHit, hits);
  else {
    // In y, if we assume the velo extrapolation is correct the calculate the
    // residuals and chi^2 can be calculated with respect to that. This is NOT the
    // final chi2 of the candidate.
    // We skip the magnet hit because it would only add noise.
    const double chi2 = std::accumulate(hit_iteration::cbegin_skip_magnet_hit(hits),
					hit_iteration::cend(hits),
					0.,
					[&state] ( const double prev, const Hit& hit ) {

					  const auto ye = yStraight(state, hit.z);

					  const auto c = (hit.y - ye)/hit.dy;

					  return prev + c*c;
					});

    fitY = FitResult{magnetHit.y, state->ty(), chi2, hits.size()};
  }

  const double p = this->momentum(fitX.slope - state->tx());

  const double chi2_ndof = (fitX.chi2 + fitY.chi2)/(fitX.ndof + fitY.ndof);

  return GlobalFitResult{fitX.slope, p, chi2_ndof};
}

//=============================================================================
//
WindowBounds MuonMatchVeloUT::stationWindow( const LHCb::State* state,
					     const CommonMuonStation& station,
					     const MagnetPositionXZ &magnet,
					     const double slope ) const {

  const auto& [xRange, yRange] = m_window[station.station()];

  const double yMuon = yStraight(state, station.z());

  const double yMin = yMuon - yRange;
  const double yMax = yMuon + yRange;

  const double xMuon = (station.z() - magnet.z)*slope + magnet.x;

  const double xMin = xMuon - xRange;
  const double xMax = xMuon + xRange;

  return {xMin, xMax, yMin, yMax};
}
