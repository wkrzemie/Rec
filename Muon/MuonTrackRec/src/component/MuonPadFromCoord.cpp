/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <vector>

#include "GaudiKernel/IIncidentSvc.h"
#include "Kernel/MuonTileID.h"
#include "Event/MuonCoord.h"

// local
#include "MuonInterfaces/MuonLogHit.h"
#include "MuonInterfaces/MuonLogPad.h"
#include "MuonPadFromCoord.h"
using namespace LHCb;
using namespace std;

//-----------------------------------------------------------------------------
// Implementation file for class : MuonPadFromCoord
//
//  2014-03-11: G.Graziani
//              interface from MuonCoords to MuonLogPad objects
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MuonPadFromCoord )


MuonPadFromCoord::MuonPadFromCoord( const std::string& type,
                                    const std::string& name,
                                    const IInterface* parent )
  : base_class ( type, name , parent )
{
  declareInterface<IMuonPadRec>(this);
  m_tiles.clear(); m_tiles.reserve(50000);
  m_hits.clear(); m_hits.reserve(50000);
  m_pads.clear(); m_pads.reserve(50000);
}


void MuonPadFromCoord::clearPads()
{
  m_tiles.clear();
  m_hits.clear();
  m_pads.clear();
}

void MuonPadFromCoord::handle ( const Incident& incident )
{
  if ( IncidentType::EndEvent == incident.type() ) {
    if ( msgLevel(MSG::DEBUG) )debug() << "End Event: clear pads"<<endmsg;
    clearPads() ;
    m_padsReconstructed = false;
  }
}

//=============================================================================
StatusCode 	MuonPadFromCoord::initialize ()
{
  StatusCode sc = GaudiTool::initialize() ;
  if (!sc) return sc;

  // -- retrieve the pointer to the muon detector
  if ( msgLevel(MSG::DEBUG) ) debug() << "Retrieve MuonDet" << endmsg;
  m_muonDetector = getDet<DeMuonDetector>(DeMuonLocation::Default);
  if(!m_muonDetector){
    error()<<"error retrieving Muon Detector geometry "<<endmsg;
    return StatusCode::FAILURE;
  }

  // geometry stuff --->
  auto nStations = m_muonDetector->stations();
  if ( nStations == 5 ) {
    // Run 1 or 2
    m_stationOffset = 0;
  } else if ( nStations == 4 ) {
    // Upgrade
    m_stationOffset = 1;
    debug() << "Got " << nStations << " muon stations => setting offset to 1" << endmsg;
  } else {
    err() << "Wrong station number: " << nStations << endmsg;
    return StatusCode::FAILURE;
  }

  incSvc()->addListener( this, IncidentType::EndEvent );
  return StatusCode::SUCCESS ;
}


StatusCode 	MuonPadFromCoord::finalize ()
{
  return   GaudiTool::finalize() ;

}

const std::vector<MuonLogPad>& MuonPadFromCoord::pads() {
  if (UNLIKELY(!m_padsReconstructed)) buildLogicalPads({});
  return m_pads;
}



StatusCode MuonPadFromCoord::buildLogicalPads(const std::vector<MuonLogHit>& ) {
  MuonCoords* coords = get<MuonCoords>(MuonCoordLocation::MuonCoords);
  if ( !coords ) {
    err() << " ==> Cannot retrieve MuonCoords " << endmsg;
    return StatusCode::FAILURE;
  }

  // make sure we reserve enough so that the contents are stable
  // i.e. the vector only allocates memory ONCE
  m_hits.reserve( 2*coords->size() );
  m_pads.reserve( 2*coords->size() );
  // loop over the coords
  for (const auto&  coord : *coords) {
    auto tile = MuonTileID(coord->key());
    auto station = tile.station() + m_stationOffset; // offset means 0 means M1 even in the upgrade
    auto region = tile.region();
    auto uncross = (station == 0 || ((station>2)&&(region==0))) ? false : coord->uncrossed();
    int time1= (int) coord->digitTDC1();
    int time2= (int) coord->digitTDC2();

    if(uncross) {
      m_tiles.push_back( tile );
      m_hits.emplace_back( tile );
      m_hits.back().setTime( time1 );
      m_pads.emplace_back( &m_hits.back() );  // m_hits was reserved with enough capacity, so yes, this works
    } else {
      const std::vector<MuonTileID > thetiles= coord->digitTile();
      tile = MuonTileID(thetiles[0]);
      m_tiles.push_back( tile );
      m_hits.emplace_back( tile );
      auto hit1 = &m_hits.back(); // m_hits was reserved with enough capacity, so yes, this works
      m_hits.back().setTime(time1);
      if (thetiles.size() == 1) {
        m_pads.emplace_back(hit1);
        m_pads.back().settruepad();
      } else {
        tile = MuonTileID(thetiles[1]);
        m_tiles.push_back(tile);
        m_hits.emplace_back(tile);
        m_hits.back().setTime(time2);
        m_pads.emplace_back(hit1,&m_hits.back()); // m_hits was reserved with enough capacity, so yes, this works
      }
    }
  }
  debug() << "Got from MuonCoords " << m_hits.size()<<" MuonLogHits and " <<
    m_pads.size() << " MuonLogPads" << endmsg;
  m_padsReconstructed=true;
  return StatusCode::SUCCESS;
}
