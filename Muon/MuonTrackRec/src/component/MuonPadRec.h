/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONPADREC_H
#define MUONPADREC_H 1
#include <map>
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "MuonInterfaces/IMuonPadRec.h"            // Interface

class DeMuonDetector;

/** @class MuonPadRec MuonPadRec.h
 *  special reconstruction tool for Monitoring
 *  code based on MuonRec by A.Satta et al.
 *
 *  @author G.Graziani
 *  @date   2008-01-25
 */


class MuonPadRec : public GaudiTool, virtual public IMuonPadRec , virtual public IIncidentListener
{
public:
  /// Standard constructor
  MuonPadRec( const std::string& type,
              const std::string& name,
              const IInterface* parent);

  StatusCode buildLogicalPads(const std::vector<MuonLogHit>& myhits ) override;
  const std::vector<MuonLogPad>& pads() override;


  // from GaudiTool
  StatusCode initialize() override;

  // from IIncidentListener
  void handle ( const Incident& incident ) override;
private:
  Gaudi::Property<std::vector<long int>> m_TileVeto {this, "TileVeto"};
  Gaudi::Property<bool> m_getfirsthit {this, "FirstComeFirstServed", false};
  std::vector<MuonLogPad> m_pads;
  bool m_padsReconstructed = false;
  DeMuonDetector* m_muonDetector = nullptr;
  std::map<long int, bool> m_TileIsVetoed;
  void clearPads();

  StatusCode addCoordsNoMap(std::vector<MuonLogHit*> &hits,
                            int station,
                            int region);
  StatusCode addCoordsCrossingMap(std::vector<MuonLogHit*> &hits,
                                  int station,
                                  int region);
  StatusCode makeStripLayouts(int station, int region,
                              MuonLayout &layout1,
                              MuonLayout &layout2);
  void removeDoubleHits(std::vector<MuonLogHit*> &hits);
};
#endif // MUONPADREC_H
