/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONLOGPAD_H
#define MUONLOGPAD_H 1

/** @class MuonLogPad MuonLogPad.h
 *
 *  Muon logical pad class for standalone cosmics reconstruction
 *  @author Giacomo
 *  @date   2007-10-26
 */


#include "MuonDet/DeMuonDetector.h"
#include "Kernel/MuonTileID.h"
#include "MuonInterfaces/MuonLogHit.h"
#include <vector>
#include <ostream>

class MuonLogPad final {
 public:
  /// standard constructor
  MuonLogPad()  = default;
  /// constructor for uncrossed pad
  MuonLogPad( MuonLogHit* hit )
  : m_tile{ hit->tile() },
    m_padhits{ hit, nullptr },
    m_time{ hit->time() },
    m_intime{ hit->intime() }
  {
  }

  /// constructor fot crossed pad, note that hit1 must be the strip (giving X coordinate)
  ///  and hit2 must be the pad  (giving Y coordinate)
  MuonLogPad( MuonLogHit* hit1,
              MuonLogHit* hit2 )
  : m_crossed{true},
    m_tile{ hit1->tile().intercept(hit2->tile()) },
    m_padhits{hit1,hit2},
    m_truepad{true},
    m_intime{ hit1->intime() && hit2->intime() }
  {
    assignTimes(hit1->time(), hit2->time() );
  }


  // public member functions
  typedef enum { NOX=0, XONEFE=1, XTWOFE=2, UNPAIRED=3} LogPadType;
  /// returns the type of logical pad (uncrossed, crossed with 1 or 2 FE channels, or unpaired log. channel)
  inline LogPadType type() const {
    if (!m_truepad) return UNPAIRED;
    LogPadType t=XONEFE;
    if (m_tile.station() == 0 ||
        (m_tile.station()>2 && m_tile.region()==0) ) {
      t=NOX; // single log. channel
    } else {
      if (m_tile.station()>0 && m_tile.station()<3 && m_tile.region()<2)
        t=XTWOFE; // double readout for M23R12
    }
    return t;
  }

  inline LHCb::MuonTileID tile() const
  {
    return m_tile;
  }

  inline void assignTimes(float t1, float t2) {
    m_time  = 0.5*( t1 + t2 );
    m_dtime = 0.5*( t1 - t2 );
  }



  inline void shiftTimes(float d1, float d2) {
    if (!m_padhits.second) return;
    float t1=(m_padhits.first)->time() + d1;
    float t2=(m_padhits.second)->time() + d2;
    assignTimes(t1, t2);
  }

  inline void shiftTime(float delay) {
    m_time += delay;
  }

  /// return the pad time (averaged for crossed pads), in TDC bits
  inline float time() const
  {
    return m_time;
  }
  inline float timeX() const {
    LogPadType t=type();
    return (float) ( (t == XONEFE || t == XTWOFE) ? m_time+m_dtime : -9999. );
  }
  inline float timeY() const {
    LogPadType t=type();
    return (float) ( (t == XONEFE || t == XTWOFE) ? m_time-m_dtime : -9999. );
  }
  /// return the half difference of the two hit times for crossed pads, in TDC bits
  inline float dtime() const
  {
    return m_dtime;
  }
  /// true if this is the crossing of two logical channels
  inline bool crossed() const
  {
    return m_crossed;
  }
  /// true if it's a real logical pad (if false, it's an unpaired logical channel)
  inline bool truepad() const
  {
    return m_truepad;
  }
  inline void settruepad() {
    m_truepad = true;
  }
  inline bool intime() const {
    return m_intime;
  }
  inline MuonLogHit* getHit(unsigned int i) {
    return i==0 ? m_padhits.first
         : i==1 ? m_padhits.second
         : nullptr;
  }
  inline std::vector<MuonLogHit*> getHits() const {
    return m_padhits.second ? std::vector<MuonLogHit*>{m_padhits.first, m_padhits.second}
                            : std::vector<MuonLogHit*>(1, m_padhits.first );
  }

 private:
  bool m_crossed = false;
  LHCb::MuonTileID m_tile = 0;
  std::pair<MuonLogHit*,MuonLogHit*> m_padhits;
  float m_time = 0;
  float m_dtime = 0;
  bool m_truepad = false;
  bool m_intime = false;
};

#endif // MUONLOGPAD_H
