/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ICOMMONMUONTOOL_H_
#define ICOMMONMUONTOOL_H_

#include "GaudiKernel/IAlgTool.h"
#include "MuonID/CommonMuonHit.h"
#include "MuonID/MuonHitHandler.h"


struct ICommonMuonTool : extend_interfaces<IAlgTool> {
  DeclareInterfaceID(ICommonMuonTool, 2, 0);

  using MuonTrackExtrapolation = std::vector<std::pair<float, float>>;
  using MuonTrackOccupancies = std::vector<unsigned>;

  virtual MuonTrackExtrapolation extrapolateTrack(const LHCb::Track&) const  = 0;
  virtual bool preSelection(const LHCb::Track&) const noexcept = 0;
  virtual bool inAcceptance(const MuonTrackExtrapolation&) const noexcept = 0;
  virtual std::tuple<CommonConstMuonHits, MuonTrackOccupancies> hitsAndOccupancies(
    const LHCb::Track&, const MuonTrackExtrapolation&, const MuonHitHandler& hitHandler) const = 0;
  virtual std::tuple<CommonConstMuonHits, MuonTrackOccupancies>extractCrossed(
    const CommonConstMuonHits&) const noexcept  = 0;
  virtual bool isMuon(const MuonTrackOccupancies&, float) const noexcept  = 0;
  virtual bool isMuonLoose(const MuonTrackOccupancies&, float) const noexcept = 0;
  virtual std::pair<float, float>foi(unsigned int, unsigned int, float) const noexcept  = 0;
  virtual unsigned int getFirstUsedStation() const noexcept = 0;
};

#endif  // ICOMMONMUONTOOL_H_
