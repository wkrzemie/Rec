/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//
// This class is obsolete, use PrepareMuonHits instead. Kept only for
// compatibility with Hlt.
//

/** Implementation of CommonMuonHitManager.
 *
 * 2010-12-07: Roel Aaij
 */
#include "CommonMuonHitManager.h"

#include <vector>
#include <algorithm>

#include <boost/numeric/conversion/cast.hpp>

#include "Event/MuonCoord.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/IMuonFastPosTool.h"

DECLARE_COMPONENT( CommonMuonHitManager )

CommonMuonHitManager::CommonMuonHitManager(const std::string& type,
                                           const std::string& name,
                                           const IInterface* parent)
: base_class(type, name, parent), m_muonDet{nullptr}, m_loaded{false}
{
  m_prepared.reset();
}

StatusCode CommonMuonHitManager::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return sc;

  incSvc()->addListener(this, IncidentType::BeginEvent);

  m_muonDet =
      getDet<DeMuonDetector>("/dd/Structure/LHCb/DownstreamRegion/Muon");
  auto nStations = boost::numeric_cast<size_t>(m_muonDet->stations());
  m_stations.reserve(nStations);
  assert(nStations <= 5);

  // Number of regions should be 5 for Run II and 4 or 5 for upgrade
  if (m_xRegions.size() != 5 && m_xRegions.size() != nStations) {
     return Error(std::string{"XRegions defined for "} + std::to_string(m_xRegions.size())
                  + " stations, should be "
                  + (nStations == 4 ? "5 (M1 will be ignored) or 4." : "5."),
                  StatusCode::FAILURE);
  }
  for (size_t i = 0; i < m_xRegions.size(); ++i) {
     const auto& bounds = m_xRegions[i];
     if(bounds.size() <= 1 || (bounds.size() % 2 != 0)) {
        return Error("Number of X regions for station " + std::to_string(i) +
                     " should be even and at least 2", StatusCode::FAILURE);
     }
  }

  for (size_t i = 0; i != nStations; ++i) {
    // the m_xRegions property has all regions starting from M1.
    // For the upgrade nStations == 4, and M1 has been removed, so use
    // 1-5, for Run II use 0-5
    m_stations.emplace_back(m_muonDet, i, m_xRegions[m_xRegions.size() - nStations + i]);
  }
  return sc;
}

void CommonMuonHitManager::handle(const Incident& incident) {
  if (IncidentType::BeginEvent != incident.type()) return;

  m_prepared.reset();
  m_loaded = false;
  std::for_each(std::begin(m_coords), std::end(m_coords),
		[](std::vector<const LHCb::MuonCoord*>& v) { v.clear(); });
}

CommonMuonHitRange CommonMuonHitManager::hits(float xmin, unsigned int station,
                                              unsigned int region) {
  if (!m_prepared[station]) prepareHits(station);
  return m_stations[station].hits(xmin, region);
}

CommonMuonHitRange CommonMuonHitManager::hits(float xmin, float xmax,
                                              unsigned int station,
                                              unsigned int region) {
  if (!m_prepared[station]) prepareHits(station);
  return m_stations[station].hits(xmin, xmax, region);
}

unsigned int CommonMuonHitManager::nRegions(unsigned int station) const {
  return m_stations[station].nRegions();
}

const CommonMuonStation& CommonMuonHitManager::station(unsigned int id) const {
  return m_stations[id];
}

void CommonMuonHitManager::prepareHits(unsigned int station) {
  if (!m_loaded) loadCoords();

  CommonMuonHits hits;
  hits.reserve(m_coords[station].size());
  for (const auto& coord : m_coords[station]) {
    double x = 0., dx = 0., y = 0., dy = 0., z = 0., dz = 0.;
    StatusCode sc = m_muonDet->Tile2XYZ(coord->key(), x, dx, y, dy, z, dz);
    if (sc.isFailure()) {
      Warning("Impossible MuonTileID");
      continue;
    }
    
    hits.emplace_back(coord->key(), x, dx, y, dy, z, dz, coord->uncrossed(), coord->digitTDC1(), coord->digitTDC1()-coord->digitTDC2());
  }
  // Put the hits in the station
  m_stations[station].setHits(std::move(hits));
  m_prepared.set(station, true);
}

void CommonMuonHitManager::loadCoords() {
  LHCb::MuonCoords* coords = getIfExists<LHCb::MuonCoords>(m_coordLocation);
  if (!coords) {
    Exception("Cannot retrieve MuonCoords ", StatusCode::FAILURE);
  }
  for (auto coord : *coords) {
    m_coords[coord->key().station()].emplace_back(coord);
  }
  m_loaded = true;
}
