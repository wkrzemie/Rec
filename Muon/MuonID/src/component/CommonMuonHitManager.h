/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COMMONMUONHITMANAGER_H_
#define COMMONMUONHITMANAGER_H_

#include <array>
#include <vector>

#include "Event/MuonCoord.h"
#include "GaudiAlg/GaudiTool.h"
#include "MuonDet/DeMuonDetector.h"

#include "MuonID/CommonMuonHit.h"
#include "MuonID/CommonMuonStation.h"
#include "MuonID/ICommonMuonHitManager.h"

/** @class CommonMuonHitManager CommonMuonHitManager.h
 *  Muon hit manager for Hlt1 and Offline.
 *
 *  Used to be Hlt/Hlt1Muons/Hlt1MuonHitManager.
 *
 *  @author Roel Aaij
 *  @author Kevin Dungs
 *  @date   2015-01-03
 */
class CommonMuonHitManager final: public extends<GaudiTool, ICommonMuonHitManager> {
 public:

  CommonMuonHitManager(const std::string& type, const std::string& name,
                       const IInterface* parent);

  ~CommonMuonHitManager() override = default;

  StatusCode initialize() override;

  void handle(const Incident& incident) override;

  virtual CommonMuonHitRange hits(float xmin, unsigned int station,
                                  unsigned int region) override;
  virtual CommonMuonHitRange hits(float xmin, float xmax,
                                  unsigned int station, unsigned int region) override;
 
  virtual unsigned int nRegions(unsigned int station) const override;

  virtual const CommonMuonStation& station(unsigned int id) const override;

 private:
  // Properties
  Gaudi::Property<std::string> m_coordLocation{this, "MuonCoordLocation",
                                               LHCb::MuonCoordLocation::MuonCoords};

  // X region boundaries
  // Define the regions, the last ones are increased a little to make sure
  // everything is caught.
  std::array<std::array<double,8>,5> m_xRegions {{
     {-3940*Gaudi::Units::mm, -1920*Gaudi::Units::mm, -960*Gaudi::Units::mm, -480*Gaudi::Units::mm, 480*Gaudi::Units::mm, 960*Gaudi::Units::mm, 1920*Gaudi::Units::mm, 3940*Gaudi::Units::mm},
     {-4900*Gaudi::Units::mm, -2400*Gaudi::Units::mm, -1200*Gaudi::Units::mm, -600*Gaudi::Units::mm, 600*Gaudi::Units::mm, 1200*Gaudi::Units::mm, 2400*Gaudi::Units::mm, 4900*Gaudi::Units::mm},
     {-5252*Gaudi::Units::mm, -2576*Gaudi::Units::mm, -1288*Gaudi::Units::mm, -644*Gaudi::Units::mm, 644*Gaudi::Units::mm, 1288*Gaudi::Units::mm, 2576*Gaudi::Units::mm, 5252*Gaudi::Units::mm},
     {-5668*Gaudi::Units::mm, -2784*Gaudi::Units::mm, -1392*Gaudi::Units::mm, -696*Gaudi::Units::mm, 696*Gaudi::Units::mm, 1392*Gaudi::Units::mm, 2784*Gaudi::Units::mm, 5668*Gaudi::Units::mm},
     {-6052*Gaudi::Units::mm, -2976*Gaudi::Units::mm, -1488*Gaudi::Units::mm, -744*Gaudi::Units::mm, 744*Gaudi::Units::mm, 1488*Gaudi::Units::mm, 2976*Gaudi::Units::mm, 6052*Gaudi::Units::mm}
   }};
  
 
  // Data members
  DeMuonDetector* m_muonDet = nullptr;
   
  std::vector<CommonMuonStation> m_stations;
  std::bitset<5> m_prepared;

  std::array<std::vector<const LHCb::MuonCoord*>, 5> m_coords;
  bool m_loaded;

  // Functions
  void prepareHits(unsigned int station);

  void loadCoords();
   
};
#endif  // COMMONMUONHITMANAGER_H_
