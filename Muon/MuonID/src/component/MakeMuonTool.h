/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MakeMuonTool_H
#define MakeMuonTool_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "MuonDet/MuonBasicGeometry.h"
#include "Event/MuonPID.h"
#include "Event/Track.h"

#include "MuonDet/DeMuonDetector.h"
#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "MuonID/ICommonMuonTool.h"

class IMuonMatchTool;

static const InterfaceID IID_MakeMuonTool("MakeMuonTool", 1, 0);

/** @class MakeMuonTool MakeMuonTool.h
 */
class MakeMuonTool final : public GaudiTool {
 public:
  static const InterfaceID& interfaceID() { return IID_MakeMuonTool; }
  /// Standard constructor
  MakeMuonTool(const std::string& type, const std::string& name,
               const IInterface* parent);

  virtual ~MakeMuonTool() = default;  ///< Destructor

  StatusCode initialize() override;

  StatusCode muonCandidate(
      const LHCb::Track&, LHCb::Track&,
      const ICommonMuonTool::MuonTrackExtrapolation&,
      const std::vector<LHCb::LHCbID>&);

  StatusCode makeStates(const LHCb::Track&);

//  void addLHCbIDsToMuTrack(LHCb::Track&, const std::vector<LHCb::LHCbID>&, 
//                           const ICommonMuonTool::MuonTrackExtrapolation&);

  // make muon track function
  LHCb::Track* makeMuonTrack(LHCb::MuonPID*, CommonConstMuonHits &,     
                             const ICommonMuonTool::MuonTrackExtrapolation&);

 protected:
  ITrackExtrapolator* m_extrapolator = nullptr;
  ICommonMuonTool *muonTool_ = nullptr;
  IMuonMatchTool *matchTool = nullptr;
  DeMuonDetector* m_mudet = nullptr;

  Gaudi::Property<bool> m_FindQuality {this, "FindQuality", false};
  Gaudi::Property<bool> m_ComputeChi2Properly {this, "ComputeChi2Properly", true};

  unsigned m_NStation = 0;

  //LHCb::State* m_mySeedState = nullptr;

  //std::vector<double> m_zstations;

  //std::vector<LHCb::State> m_states;

  LHCb::Track* makeLegacyMuonTrack(LHCb::MuonPID*, CommonConstMuonHits &,     
                                   const ICommonMuonTool::MuonTrackExtrapolation&);
  LHCb::Track* makeMuonTrackWithProperChi2(LHCb::MuonPID*, CommonConstMuonHits &,     
                                           const ICommonMuonTool::MuonTrackExtrapolation&);

  void addLHCbIDsToMuTrack(LHCb::Track*, CommonConstMuonHits &,
                           const ICommonMuonTool::MuonTrackExtrapolation&);

};

#endif  // MUIDTOOL_H
