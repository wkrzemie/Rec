/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PREPAREMUONHITS_H
#define PREPAREMUONHITS_H

#include <vector>

#include "Event/MuonCoord.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/IIncidentListener.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonID/MuonHitHandler.h"
#include "MuonID/CommonMuonHit.h"
#include "MuonID/CommonMuonStation.h"

/** @class Preparemuonhits Preparemuonhits.h
 *  Used to be CommonMuonHitManager.
 *
 *  @author Roel Aaij
 *  @author Kevin Dungs
 *  @date   2015-01-03
 */
class PrepareMuonHits final
   : public Gaudi::Functional::Transformer<MuonHitHandler(const LHCb::MuonCoords& coords)> {
public:

   PrepareMuonHits(const std::string& name, ISvcLocator* pSvcLocator);

   MuonHitHandler operator()(const LHCb::MuonCoords& coords) const override;

   StatusCode initialize() override;

private:

   // X region boundaries
   // Define the regions, the last ones are increased a little to make sure
   // everything is caught.
   std::array<std::array<double,8>,5> m_xRegions {{
     {-3940*Gaudi::Units::mm, -1920*Gaudi::Units::mm, -960*Gaudi::Units::mm, -480*Gaudi::Units::mm, 480*Gaudi::Units::mm, 960*Gaudi::Units::mm, 1920*Gaudi::Units::mm, 3940*Gaudi::Units::mm},
     {-4900*Gaudi::Units::mm, -2400*Gaudi::Units::mm, -1200*Gaudi::Units::mm, -600*Gaudi::Units::mm, 600*Gaudi::Units::mm, 1200*Gaudi::Units::mm, 2400*Gaudi::Units::mm, 4900*Gaudi::Units::mm},
     {-5252*Gaudi::Units::mm, -2576*Gaudi::Units::mm, -1288*Gaudi::Units::mm, -644*Gaudi::Units::mm, 644*Gaudi::Units::mm, 1288*Gaudi::Units::mm, 2576*Gaudi::Units::mm, 5252*Gaudi::Units::mm},
     {-5668*Gaudi::Units::mm, -2784*Gaudi::Units::mm, -1392*Gaudi::Units::mm, -696*Gaudi::Units::mm, 696*Gaudi::Units::mm, 1392*Gaudi::Units::mm, 2784*Gaudi::Units::mm, 5668*Gaudi::Units::mm},
     {-6052*Gaudi::Units::mm, -2976*Gaudi::Units::mm, -1488*Gaudi::Units::mm, -744*Gaudi::Units::mm, 744*Gaudi::Units::mm, 1488*Gaudi::Units::mm, 2976*Gaudi::Units::mm, 6052*Gaudi::Units::mm}
   }}; 

   // Data members
   DeMuonDetector* m_muonDet = nullptr;
   size_t m_nStations;

};
#endif  // PREPAREMUONHITS_H
