/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** Implementation of PrepareMuonHits.
 * Transformer supposed to take muon coord as input and
 * muon hits as output
 **/
#include <algorithm>
#include <boost/numeric/conversion/cast.hpp>

#include "PrepareMuonHits.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT(PrepareMuonHits)

PrepareMuonHits::PrepareMuonHits(const std::string& name,
				 ISvcLocator* pSvcLocator)
 : Transformer(name, pSvcLocator,
               KeyValue{"CoordLocation", LHCb::MuonCoordLocation::MuonCoords},
               KeyValue{"Output", MuonHitHandlerLocation::Default}) { }

//=============================================================================
// Initialization
//=============================================================================

StatusCode PrepareMuonHits::initialize() {
   StatusCode sc = Transformer::initialize();
   if (sc.isFailure()) return sc;

   m_muonDet = getDet<DeMuonDetector>(DeMuonLocation::Default);

   m_nStations = boost::numeric_cast<size_t>(m_muonDet->stations());
   assert(m_nStations <= 5);

   // Number of regions should be 5 for Run II and 4 or 5 for upgrade
   if (m_xRegions.size() != 5 && m_xRegions.size() != m_nStations) {
      return Error(std::string{"XRegions defined for "} + std::to_string(m_xRegions.size())
                   + " stations, should be "
                   + (m_nStations == 4 ? "5 (M1 will be ignored) or 4." : "5."),
                   StatusCode::FAILURE);
   }
   for (size_t i = 0; i < m_xRegions.size(); ++i) {
      const auto& bounds = m_xRegions[i];
      if(bounds.size() <= 1 || (bounds.size() % 2 != 0)) {
         return Error("Number of X regions for station " + std::to_string(i) +
                      " should be even and at least 2", StatusCode::FAILURE);
      }
   }

   return sc;
}

MuonHitHandler PrepareMuonHits::operator()(const LHCb::MuonCoords& muonCoords) const {
   if (msgLevel(MSG::DEBUG)) {
      debug() << "==> Execute" << endmsg;
      debug()<<  "Detector version used: "<< m_muonDet->version()<<endmsg;
   }

   std::array<CommonMuonStation, 5> stations;

   std::vector<std::vector<const LHCb::MuonCoord*>> sortedCoords(m_muonDet->stations());
   for (auto& coords : sortedCoords) {
      coords.reserve(10);
   }

   for (auto coord : muonCoords) {
      sortedCoords[coord->key().station()].emplace_back(coord);
   }

   for (unsigned int station = 0; station < m_nStations; ++station) {
      CommonMuonHits hits;
      hits.reserve(sortedCoords[station].size());
      for (const auto& coord : sortedCoords[station]) {
         double x = 0., dx = 0., y = 0., dy = 0., z = 0., dz = 0.;
         assert(coord->key().station() == station);
         StatusCode sc = m_muonDet->Tile2XYZ(coord->key(), x, dx, y, dy, z, dz);
         if (sc.isFailure()) {
            Warning("Impossible MuonTileID").ignore();
            continue;
         }
         hits.emplace_back(coord->key(), x, dx, y, dy, z, dz, coord->uncrossed(), coord->digitTDC1(), coord->digitTDC1()-coord->digitTDC2());
      }
     
      // create a MuonHitHandler to be returned and stored in the TES
      // the m_xRegions property has all regions starting from M1.
      // For the upgrade nStations == 4, and M1 has been removed, so use
      // 1-5, for Run II use 0-5
      auto region = m_xRegions.size() - m_nStations + station;
      stations[station] = CommonMuonStation{m_muonDet, station,
                                            m_xRegions[region],
                                            std::move(hits)};
   }

   return MuonHitHandler{std::move(stations)};
}
