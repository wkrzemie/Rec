/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <stdlib.h>

// local
#include "MuonIDPlusTool.h"
#include "MuonID/CommonMuonHit.h"
#include "MuonID/IMuonMatchTool.h"

#include "Event/ODIN.h"
#include "Event/MuonPID.h"

#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/IMuonFastPosTool.h"
#include "MuonDet/MuonBasicGeometry.h"
#include "MuonInterfaces/IMuonPadRec.h"
#include "MuonInterfaces/IMuonClusterRec.h"
#include "MuonInterfaces/MuonLogPad.h"
#include "MuonInterfaces/MuonHit.h"
#include "Event/MuonCoord.h"
#include "Event/Track.h"
#include "Event/MuonPID.h"

#include "Event/State.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "Kernel/ParticleID.h"

namespace {
  static const LHCb::ParticleID theMuon { 13 };
}

using namespace LHCb;

StatusCode MuonIDPlusTool::initialize() {
  const StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  debug() << "initializing MuonIDPlusTool" <<endmsg;

  m_mudet=getDet<DeMuonDetector>(DeMuonLocation::Default);
  auto nStations = m_mudet->stations();

  m_muhits.resize(nStations);
  m_stationHasLargeCluster.resize(nStations);
  m_matchM.resize(nStations);
  m_matchSigma.resize(nStations);
  m_matchX.resize(nStations);
  m_matchY.resize(nStations);
  m_matchT.resize(nStations);
  m_matchdT.resize(nStations);
  m_clusize.resize(nStations);
  m_isoM.resize(nStations);

  m_extrapolator = tool<ITrackExtrapolator>( "TrackMasterExtrapolator", this );
  m_posTool = tool<IMuonFastPosTool>("MuonFastPosTool");
  m_padrectool = tool<IMuonPadRec>("MuonPadFromCoord");
  m_clustertool=tool<IMuonClusterRec>("MuonClusterRec");
  m_matchTool = tool<IMuonMatchTool>(m_matchToolName, this);

  return sc;
}


void MuonIDPlusTool::clearPIDs() {
  // need to delete created objects if they are not handled by TES
  if(!m_ReleaseObjectOwnership) {
    for (auto& ipid : m_muonPIDs) {
      if(ipid) {
        delete ipid->muonTrack();
        delete ipid;
      }
    }
  }
  m_muonPIDs.clear();
}

StatusCode MuonIDPlusTool::eventInitialize()
{
  const LHCb::ODIN* odin = getIfExists<LHCb::ODIN>(evtSvc(),LHCb::ODINLocation::Default);
  if ( !odin ) { odin = getIfExists<LHCb::ODIN>(evtSvc(),LHCb::ODINLocation::Default,false); }
  if ( !odin ) {
    // should always be available ...
    return Error( "Cannot load the ODIN data object", StatusCode::SUCCESS );
  }

  if (odin->runNumber()   != m_lastRun ||
      odin->eventNumber() != m_lastEvent) {
    // initialize the event doing preliminary matching between all tracks and all muon hits
    //TODO/BUG: the state generated here should be stored in the event store,
    //          and not as member data of this tool!!!
    debug() << "Begin Event: load muon hits"<<endmsg;
    clearPIDs();
    getMuonHits();
    matchHitsToTracks();

    m_lastRun = odin->runNumber();
    m_lastEvent = odin->eventNumber();
  }
  return StatusCode::SUCCESS;

}

void MuonIDPlusTool::initVariables() {
  m_nSmatched=m_friendShares=0;
  m_medianClusize=0.;
  m_maxisoM=-1.;
  std::fill(m_matchM.begin(),m_matchM.end(),0);
  std::fill(m_clusize.begin(),m_clusize.end(),0);
  std::fill(m_isoM.begin(),m_isoM.end(),999.);
  std::fill(m_matchX.begin(), m_matchX.end(),0);
  std::fill(m_matchY.begin(), m_matchY.end(),0);
  std::fill(m_matchT.begin(), m_matchT.end(),0);
  std::fill(m_matchdT.begin(), m_matchdT.end(),0);
  m_isMuonLoose = m_isMuon = m_isMuonTight = false;
}



void MuonIDPlusTool::checkMuIsolation(const LHCb::Track *pTrack, const std::vector<CommonConstMuonHits>& mucoord) {
  std::map<const LHCb::Track*, int> nStcloseTr;
  std::map<const LHCb::Track*, bool> closeTr;
  debug() << "--- starting mu isolation check---"<<endmsg;

  assert(m_isoM.size()==mucoord.size());
  for (unsigned int s=0; s<m_isoM.size(); s++) {
    closeTr.clear();
    m_isoM[s]=999.;
    const auto& sthits = mucoord[s];
    debug() << "size hits in M"<<s+1<<"  ="<<sthits.size()<<endmsg;
    for (const auto& link : sthits) {
      if(link->clusterSize() > m_maxCluSize) {
        m_isoM[s] = 0.; // large clusters are not isolated by definition
        continue;
      }
      debug() << "ckMuIso M"<<s+1<<" There are "<<m_mutrkmatchTable[link].size()<<" matches for this hit"<<endmsg;
      if(m_mutrkmatchTable[link].size() > 1) {
        for (auto imatch=m_mutrkmatchTable[link].begin() ; imatch < m_mutrkmatchTable[link].end(); imatch++) {
          if (imatch->first == pTrack) continue;
          // require that competing track has enough momentum for this station
          debug() << "concurrent track with momentum "<<imatch->first->p()/Gaudi::Units::GeV<<" GeV"<<endmsg;
          if (imatch->first->p() < ( m_mudet->getStationZ(s) > 18*Gaudi::Units::m ? 6*Gaudi::Units::GeV :
                                     (  m_mudet->getStationZ(s) > 17*Gaudi::Units::m ?  5*Gaudi::Units::GeV : m_minTrackMomentum.value())) )  continue;
          if (imatch->second < m_isoM[s]) m_isoM[s] = imatch->second;
          closeTr[imatch->first]=true;
        }
      }
    }
    for (auto ict=closeTr.begin() ; ict != closeTr.end(); ict++) {
      nStcloseTr[ict->first]++;
    }
    debug() << "isoM station "<<s+1<<" = "<<m_isoM[s]<<endmsg;
  }
  int maxctr=-1;
  const LHCb::Track* tfriend=nullptr;
  for (auto isct=nStcloseTr.begin() ; isct != nStcloseTr.end(); isct++) {
    if (isct->second > maxctr) {
      maxctr=isct->second;
      tfriend=isct->first;
    }
  }
  if(tfriend) {
    m_friendShares=maxctr;
    debug() << "closest sharing track is "<<*tfriend<<" with "<<m_friendShares <<" common hits"<<endmsg;
  }
  else {
    m_friendShares=0;
  }

  m_maxisoM=0.;
  for (unsigned int s=1; s<m_isoM.size() ; s++) {
    if(m_matchM[s] == 2 && m_isoM[s]>m_maxisoM)
      m_maxisoM =  m_isoM[s];
  }

}

float MuonIDPlusTool::medianClusize() {
  float mcl=0.;
  std::vector<float> thiscls;
  for (unsigned int s=0; s<m_matchM.size() ; s++)
    if(m_matchM[s] == 2) thiscls.push_back(m_clusize[s]);
  if(thiscls.size()>1) {
    std::sort (thiscls.begin(), thiscls.end());
    if (thiscls.size()%2 == 1)
      mcl=thiscls[thiscls.size()/2];
    else
      mcl=(thiscls[thiscls.size()/2-1]+thiscls[thiscls.size()/2])/2.;
  }
  return mcl;
}





LHCb::MuonPID* MuonIDPlusTool::getMuonID(const LHCb::Track* track) {
  initVariables();
  if (!track) return nullptr;


  // check that the current event has been intialized
  eventInitialize();

  auto muPid = std::make_unique<MuonPID>();

  m_muonPIDs.push_back(muPid.get());
  muPid->setIDTrack(track);
  muPid->setPreSelMomentum(track->p() > m_minTrackMomentum);
  unsigned int nStations = m_muhits.size();
  bool inAcc = ( m_trkInAcceptance.count(track) && m_trkInAcceptance[track][nStations] );
  muPid->setInAcceptance(inAcc);
  m_lasttrack = track; m_lastPID = muPid.get();

  // run the matching tool for this track
  StatusCode matchStatus = m_matchTool->run(track, &(m_trkmumatchTable[track]), &(m_trkmumatchTableSpares[track]));
  if(matchStatus.isFailure()) {
    warning() << " Failed to match track to Muon Detector! " << endmsg;
    return muPid.release();
  }

  // now make the muonTrack
  auto mtrack = std::make_unique<LHCb::Track>(muPid->key());
  mtrack->addToAncestors(*track);
  mtrack->addToStates(track->closestState(m_mudet->getStationZ(0)));

  // get informations from the best matched hit in each station
  CommonConstMuonHits matchedMuonHits = m_matchTool->getListofCommonMuonHits(-1, true);
  m_nSmatched =0;
  m_nVmatched =0;


  for (const auto& hit : matchedMuonHits ) {
    ++m_nSmatched;
    mtrack->addToLhcbIDs(hit->tile());

    unsigned int station = hit->station();
    m_matchM[station] = hit->uncrossed() ? 1 : 2;
    m_nVmatched += m_matchM[station];
    m_matchX[station] = hit->x();
    m_matchY[station] = hit->y();
    m_matchT[station] = hit->time();
    m_matchdT[station] = hit->deltaTime();
    m_clusize[station] = hit->clusterSize();
    m_matchSigma[station] = m_matchTool->getMatchSigma(station);
    debug()<<"station M"<<station+1<<" matched with type "<<m_matchM[station]<<" sigma="<<m_matchSigma[station]<<endmsg;
  }

  debug() <<"There are "<<m_nSmatched<<" matched stations for this track" << endmsg;


  if(m_nSmatched>0) {
    auto [ chi2, ndof ] = m_matchTool->getChisquare();
    mtrack->setChi2AndDoF (chi2, ndof);
    mtrack->setType(LHCb::Track::Types::Muon);
    mtrack->setHistory(LHCb::Track::History::MuonID);
    mtrack->addInfo(LHCb::Track::AdditionalInfo::MuonMomentumPreSel, m_minTrackMomentum);
    mtrack->addInfo(LHCb::Track::AdditionalInfo::MuonInAcceptance, muPid->InAcceptance());
    mtrack->addInfo(LHCb::Track::AdditionalInfo::IsMuonLoose, muPid->IsMuonLoose());
    mtrack->addInfo(LHCb::Track::AdditionalInfo::IsMuon, muPid->IsMuon());
    mtrack->addInfo(LHCb::Track::AdditionalInfo::IsMuonTight, muPid->IsMuonTight());
    mtrack->addInfo(LHCb::Track::AdditionalInfo::MuonChi2perDoF, mtrack->chi2PerDoF());
    muPid->setMuonTrack(mtrack.release());

    m_medianClusize = medianClusize();

    // check that muon hits are not compatible with other tracks
    std::vector<CommonConstMuonHits> matchedHitsByStation;
    for (unsigned int s=0; s<m_muhits.size(); s++) {
      CommonConstMuonHits matchInstation = m_matchTool->getListofCommonMuonHits(s, true);
      matchedHitsByStation.push_back(matchInstation);
    }
    checkMuIsolation(track, matchedHitsByStation);

    // set binary flags
    setIsMuon(track->p(), muPid->muonTrack()->chi2PerDoF());
  }

  muPid->setIsMuon(m_isMuon);
  muPid->setIsMuonLoose(m_isMuonLoose);
  muPid->setIsMuonTight(m_isMuonTight);


  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // note that LLmuon, LLbg and Nshared are not (yet) defined by this tool
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  return muPid.release();
}


void MuonIDPlusTool::setIsMuon(float momentum, float chi2perdof ) {

  // this is very preliminary and untuned and not (yet) really used

  int requiredViews =  std::min( 7,  std::max ( 3, (int) (3 + 4*( momentum - 3*Gaudi::Units::GeV )/6*Gaudi::Units::GeV  )) );

  m_isMuonLoose = m_nVmatched > requiredViews;
  m_isMuon = m_isMuonLoose && chi2perdof<20;
  m_isMuonTight = m_isMuon && chi2perdof<5;

}


void MuonIDPlusTool::getMuonHits() {
//==========================================================================
  // get the Muon hits, compute the spatial coordinates and store them by station

  for (auto& i : m_muhits) i.clear();

  m_largeClusters=false;
  std::fill(m_stationHasLargeCluster.begin(),m_stationHasLargeCluster.end(), false);

  double x,dx,y,dy,z,dz;

  const std::vector<MuonLogPad>& muRawHits=m_padrectool->pads();
  // clusterize muon signals to assign cluster size to each hit
  // loop over the clusters and store hits by station
  unsigned int nStations = m_muhits.size();
  for (const auto& clu : m_clustertool->clusters(muRawHits)) {
    int cls=  clu.npads();
    int st= clu.station();
    if (st == 0 &&  nStations == 5 && !m_useM1) continue;
    if (cls>m_maxCluSize) {
      m_largeClusters=true; // remember that this event have hits that won't be used for track fitting but could be attached to tracks
      m_stationHasLargeCluster[st]=true;
    }
    for(const auto& pad : clu.logPads() ) {
      LHCb::MuonTileID tile= pad->tile();
      auto sc = m_posTool->calcTilePos(tile,x,dx,y,dy,z,dz);
      if (sc.isFailure()) {
        warning() << " Failed to get x,y,z of tile " << tile << endmsg;
        continue;
      }
      try {
        m_muhits[st].emplace_back(tile,x,dx,y,dy,z,dz,false,pad->time(),pad->dtime(),cls);
      } catch (std::bad_alloc&) {
        warning() << "maximum number of stored hits reached for station M"<<st+1<<endmsg;
      }
    }
  }

  // also store uncrossed log. hits
  int cl1v=1;
  for (const auto& onev : muRawHits ) {
    if (onev.truepad()) continue;
    LHCb::MuonTileID tile= onev.tile();
    try {
      m_mudet->Tile2XYZ(tile,x,dx,y,dy,z,dz);
      m_muhits[tile.station()].emplace_back(tile,x,dx,y,dy,z,dz,true,onev.time(),onev.dtime(),cl1v);
    } catch(std::bad_alloc&) {
      warning() << "maximum number of stored hits reached for station M"<<tile.station()+1<<endmsg;
    }
  }
  if (msgLevel(MSG::DEBUG)) {
      debug() <<"Muon hits per station:";
      int s=1;
      for(const auto& c : m_muhits ) {
          debug() <<" M" << s++
                  << ( c.size()<c.capacity()?" ":" >= ") << c.size();
      }
      debug() <<endmsg;
  }
}

bool MuonIDPlusTool::isTrackInsideStation(const LHCb::State& state, unsigned int istation) const {
  auto inside = [](double x, double sx2, double lo, double hi ) {
      return ( x>lo || std::pow(x-lo,2)<sx2 )
          && ( x<hi || std::pow(x-hi,2)<sx2 );
  };
  return inside( std::abs(state.x()), state.errX2(),
                 m_mudet->getInnerX(istation), m_mudet->getOuterX(istation) )
      && inside( std::abs(state.y()), state.errY2(),
                 m_mudet->getInnerY(istation), m_mudet->getOuterY(istation) );
}

void MuonIDPlusTool::matchHitsToTracks()
// check compatibility of each muon hit with all good long tracks
{
  m_mutrkmatchTable.clear();
  m_trkmumatchTable.clear();
  m_trkmumatchTableSpares.clear();

  unsigned nStations = m_muhits.size();
  std::vector<bool> matchInStation(nStations, false);

  const LHCb::Tracks* bestTracks = get<LHCb::Tracks>( m_BestTrackLocation );
  int nm=0;
  debug() << "Start matching tracks with muon hits"<<endmsg;
  for (const auto& trk : *bestTracks) {
    if( trk->type() != LHCb::Track::Types::Long &&  trk->type() != LHCb::Track::Types::Downstream)  continue;
    if( trk->chi2PerDoF() > m_maxTrackChi2)  continue;
    if( trk->ghostProbability() > m_maxGhostProb)  continue;
    if( trk->p() < m_minTrackMomentum)  continue;
    LHCb::State ExtraState = trk->closestState(m_mudet->getStationZ(0));
    std::vector<bool> inAcc(nStations+1,true); // inacceptance bit for each station + global

    for(unsigned int station = (m_useM1 ? 0 : 1); station<nStations ; station++) {
      if (fabs(m_mudet->getStationZ(station)-ExtraState.z())>1*Gaudi::Units::cm) {
        m_extrapolator->propagate(ExtraState, m_mudet->getStationZ(station), theMuon);
        verbose()<< "Track extrapolation to station M"<<station+1<<" has errors (in cm) "<<sqrt(ExtraState.errX2())/Gaudi::Units::cm<<"/"<<sqrt(ExtraState.errY2())/Gaudi::Units::cm<<endmsg;
      }
      inAcc[station]=isTrackInsideStation(ExtraState, station);  // is in acceptance of this station
      inAcc[nStations+1] = (inAcc[station] && inAcc[nStations+1]); // is in acceptance of all stations

      for ( auto& hit : m_muhits[station]) {
        bool spare =  (hit.uncrossed()) ||  // hit in single view, treat as spare
          (hit.clusterSize() > m_maxCluSize);     // treat also large clusters as spare hits
        auto dz = hit.z()-ExtraState.z();
        auto xs = ExtraState.x() + ExtraState.tx()*dz;
        auto ys = ExtraState.y() + ExtraState.ty()*dz;
        auto xdist = hit.x()-xs;
        auto ydist = hit.y()-ys;
        auto Err2x = (hit.dx()*hit.dx()*4)/12. +  ExtraState.errX2();
        auto Err2y = (hit.dy()*hit.dy()*4)/12. +  ExtraState.errY2();
        auto mtcSigmax2 = xdist*xdist/Err2x;
        auto mtcSigmay2 = ydist*ydist/Err2y;
        auto s2 = m_searchSigmaCut*m_searchSigmaCut;
        if (mtcSigmax2 < s2 && mtcSigmay2 < s2) { // found compatible muon hit with this track
          verbose() << "    found match in station "<<station+1 <<" sigma= "<<sqrt(mtcSigmax2+ mtcSigmay2)<<" clusize="<<hit.clusterSize()<<endmsg;
          auto dxy = sqrt(mtcSigmax2 + mtcSigmay2);
          m_mutrkmatchTable[&hit].emplace_back(trk,dxy);
          (spare ? m_trkmumatchTableSpares:m_trkmumatchTable )[trk].emplace_back(
                &hit, dxy, xs, ys );
          if (!matchInStation[station]) matchInStation[station]=true;
          nm++;
        }
      }
      m_trkInAcceptance[trk] = inAcc;
    }
  }
  debug() << "end of matchHitsToTracks found nmatch="<<nm<<endmsg;
}


float MuonIDPlusTool::muonIDPropertyD(const LHCb::Track* track, IMuonIDTool::ID_d prop, int station) {
  if (m_lasttrack != track) getMuonID(track); // this is not safe -- see 'ABA problem', eg. https://en.wikipedia.org/wiki/ABA_problem
  auto validStation = [=](int s) { return s>-1 && s< (int)m_isoM.size(); };
  switch (prop) {
    case  IMuonIDTool::ID_d::mediancs:  return m_medianClusize;
    case  IMuonIDTool::ID_d::maxIso:  return m_maxisoM;
    case  IMuonIDTool::ID_d::matchSigma:  return validStation(station) ? m_matchSigma[station] : -9999. ;
    case  IMuonIDTool::ID_d::iso:  return  validStation(station) ? m_isoM[station] : -9999.;
    case  IMuonIDTool::ID_d::time:  return validStation(station) ? m_matchT[station] : -9999.;
    case  IMuonIDTool::ID_d::dtime:  return validStation(station) ? m_matchdT[station] : -9999.;
    default : return -9999.;
  }
}

int MuonIDPlusTool::muonIDPropertyI(const LHCb::Track* track, IMuonIDTool::ID_i prop, int station) {
  if (m_lasttrack != track) getMuonID(track); // this is not safe -- see 'ABA problem', eg. https://en.wikipedia.org/wiki/ABA_problem
  auto validStation = [=](int s) { return s>-1 && s< (int)m_matchM.size(); };
  switch (prop) {
    case IMuonIDTool::ID_i::match: return validStation(station) ? m_matchM[station] : -9999.;
    case IMuonIDTool::ID_i::clusize: return validStation(station) ? m_clusize[station] : -9999.;
    case IMuonIDTool::ID_i::matchedStations: return m_nSmatched;
    case IMuonIDTool::ID_i::matchedViews:    return m_nVmatched;
    case IMuonIDTool::ID_i::MaxSharedHits:   return m_friendShares;
    default : return -9999.;
  }
}

DECLARE_COMPONENT( MuonIDPlusTool )
