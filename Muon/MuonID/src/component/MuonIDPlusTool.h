/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONIDPLUSTOOL_H
#define MUONIDPLUSTOOL_H 1

#include <map>
#include "boost/container/static_vector.hpp"

#include "GaudiAlg/GaudiTool.h"
#include "Event/MuonCoord.h"
#include "Event/Track.h"
#include "Event/MuonPID.h"
#include "Kernel/TrackDefaultParticles.h"
#include "MuonID/CommonMuonHit.h"
#include "MuonID/IMuonIDTool.h"
#include "MuonID/IMuonMatchTool.h"

// Forward declarations
namespace LHCb {
  class Particle;
  class MuonCoord;
  class MuonTileID;
  class State;
}
class DeMuonDetector;
struct ITrackExtrapolator;
class IMuonFastPosTool;
struct IMuonPadRec;
struct IMuonClusterRec;



/** @class MuonIDPlusTool MuonIDPlusTool.h
 *
 *  /brief Generic tool for MuonID, returning a MuonID object from a Track object
 *
 *  on the first call per event, this tool makes association tables between all tracks passing
 *  some preselction cuts (defined by options MinTrackMomentum, MaxGhostProb, MaxTrackChi2) and all muon hits.
 *  Matches within Nsigma < SearchSigmaCut are kept
 *
 *  Subsequent steps in muonID (association of best muon hits, chi^2 calculations, DLL, etc) are
 *  delegated to dedicated tools
 *
 *  Use ReleaseObjectOwnership = false
 *  if you want the tool to delete all created MuonID and Track objects (by default the tool assumes these objects will
 *  be stored in a container)
 *
 *
 *
 *  @author Giacomo Graziani
 *  @date   2015-11-10
 *
 */
class MuonIDPlusTool : public extends<GaudiTool, IMuonIDTool> {
public:
  /// Standard constructor
  using base_class::base_class;

  virtual ~MuonIDPlusTool(){ clearPIDs();}; ///< Destructor
  StatusCode initialize() override;

  StatusCode eventInitialize() override;

  LHCb::MuonPID* getMuonID(const LHCb::Track* track) override;

  float muonIDPropertyD(const LHCb::Track* track, IMuonIDTool::ID_d propertyName, int station=-1) override;
  int muonIDPropertyI(const LHCb::Track* track, IMuonIDTool::ID_i propertyName, int station=-1) override;

private:
  DeMuonDetector* m_mudet = nullptr;
  IMuonFastPosTool* m_posTool = nullptr;
  IMuonPadRec* m_padrectool = nullptr;
  IMuonClusterRec* m_clustertool = nullptr;
  ITrackExtrapolator* m_extrapolator = nullptr;
  IMuonMatchTool* m_matchTool = nullptr;

  unsigned int m_lastRun = 0;
  unsigned int m_lastEvent = 0;
  LHCb::Tr::PID theMuon = LHCb::Tr::PID::Muon();
  const LHCb::Track* m_lasttrack = nullptr;

  static const unsigned int MAXHITS = 1024;
  boost::container::static_vector<boost::container::static_vector<CommonMuonHit,MAXHITS>,5> m_muhits;                 // muon hits grouped by stations // ensure that pointers to stored hits won't change
  std::map<const CommonMuonHit*, std::vector< std::pair< const LHCb::Track*,float> > > m_mutrkmatchTable;
  std::map<const LHCb::Track*, std::vector< TrackMuMatch > > m_trkmumatchTable;
  std::map<const LHCb::Track*, std::vector< TrackMuMatch > > m_trkmumatchTableSpares;
  std::map<const LHCb::Track*, std::vector<bool>> m_trkInAcceptance;

  std::vector<LHCb::MuonPID*> m_muonPIDs;
  LHCb::MuonPID* m_lastPID = nullptr;

  bool m_largeClusters = false;
  boost::container::static_vector<bool,5> m_stationHasLargeCluster;

  void initVariables();
  void clearPIDs();
  void setIsMuon(float momentum, float chi2perdof);
  void checkMuIsolation(const LHCb::Track *pTrack, const std::vector<CommonConstMuonHits>& mucoord);

  void getMuonHits();
  void matchHitsToTracks();
  bool isTrackInsideStation(const LHCb::State& state, unsigned int istation) const;

  float medianClusize();

  // variables for tuple:
  boost::container::static_vector<int,5> m_matchM;
  boost::container::static_vector<float,5> m_matchSigma;
  boost::container::static_vector<int,5> m_matchX;
  boost::container::static_vector<int,5> m_matchY;
  boost::container::static_vector<float,5> m_matchT;
  boost::container::static_vector<float,5> m_matchdT;
  boost::container::static_vector<int,5> m_clusize;
  boost::container::static_vector<float,5> m_isoM;
  float m_medianClusize = 0.;
  float m_maxisoM = -1.;
  int m_friendShares = 0;
  int m_nSmatched = 0;
  int m_nVmatched = 0;

  bool m_isMuon = false;
  bool m_isMuonLoose = false;
  bool m_isMuonTight = false;

  // options:
  Gaudi::Property<bool> m_useM1
    {this, "UseFirstStation", false,
    "also use M1 hits for muonID"};

  Gaudi::Property<float> m_searchSigmaCut
    {this, "SearchSigmaCut", 9.,
    "number of sigma of extrap. error defining initial search window for muon hits"};

  Gaudi::Property<float> m_maxTrackChi2
    {this, "MaxTrackChi2", 10.,
    "max chi2/dof for considered tracks"};

  Gaudi::Property<float> m_maxGhostProb
    {this, "MaxGhostProb", .8,
    "max ghost prob. for considered tracks"};

  Gaudi::Property<float> m_minTrackMomentum
    {this, "MinTrackMomentum", 3.*Gaudi::Units::GeV,
    "minimum momentum for considered tracks"};

  Gaudi::Property<int> m_maxCluSize
    {this, "MaxCluSize", 8,
    "do not consider cluster above this size for track fitting (just attach after fit)"};

  Gaudi::Property<bool> m_ReleaseObjectOwnership
    {this, "ReleaseObjectOwnership", true,
    "Assume all created MuonTrack and MuonPID objects will be stored to TES"};

  Gaudi::Property<std::string> m_matchToolName
    {this, "MatchToolName", "MuonChi2MatchTool",
    "Tool for track extrapolation and match to muon stations"};

  Gaudi::Property<std::string> m_BestTrackLocation
    {this, "InputTracksLocation", LHCb::TrackLocation::Default,
    "address of best tracks container"};

};
#endif // MUONIDPLUSTOOL_H
