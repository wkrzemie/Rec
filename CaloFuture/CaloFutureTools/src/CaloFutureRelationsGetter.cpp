/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"


// local
#include "CaloFutureRelationsGetter.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureRelationsGetter
//
// 2013-10-04 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureRelationsGetter )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloFutureRelationsGetter::CaloFutureRelationsGetter( const std::string& type,
                                          const std::string& name,
                                          const IInterface* parent )
: GaudiTool ( type, name , parent )
{
  declareInterface<ICaloFutureRelationsGetter>(this);
  declareInterface<IIncidentListener>(this);
}

//=============================================================================


StatusCode CaloFutureRelationsGetter::initialize(){
  StatusCode sc = GaudiTool::initialize();
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )debug() << "Initialize CaloFutureRelationsGetter tool " << endmsg;
  // subscribe to the incidents
  IIncidentSvc* inc = incSvc() ;
  if ( inc )inc -> addListener  ( this , IncidentType::BeginEvent ) ;
  return sc;
}

StatusCode CaloFutureRelationsGetter::finalize(){
  IIncidentSvc* inc = incSvc() ;
  if ( inc ) { inc -> removeListener  ( this ) ; }
  return GaudiTool::finalize();
}

LHCb::CaloFuture2Track::ITrHypoTable2D* CaloFutureRelationsGetter::getTrHypoTable2D(std::string location){
  auto it = m_hypoTr.find(location);
  if( it == m_hypoTr.end() ) m_hypoTr[location] = getIfExists<LHCb::CaloFuture2Track::IHypoTrTable2D> (location);
  
  auto links = m_hypoTr[location]->relations();
  m_trHypo.i_clear().ignore(); 
  for(const auto& l : links) m_trHypo.i_push( l.to(), l.from(), l.weight() );
  m_trHypo.i_sort();
  return &m_trHypo;
}

LHCb::CaloFuture2Track::IHypoEvalTable* CaloFutureRelationsGetter::getHypoEvalTable(std::string location){
  auto it = m_hypoEval.find(location);
  if( it == m_hypoEval.end()) m_hypoEval[location] = getIfExists<LHCb::CaloFuture2Track::IHypoEvalTable> (location);
  return m_hypoEval[location];
}

LHCb::CaloFuture2Track::IClusTrTable* CaloFutureRelationsGetter::getClusTrTable(std::string location){
  auto it = m_clusTr.find(location);
  if( it == m_clusTr.end()) m_clusTr[location] = getIfExists<LHCb::CaloFuture2Track::IClusTrTable> (location);
  return m_clusTr[location];
}

void CaloFutureRelationsGetter::clean(){
  m_clusTr.clear();
  m_hypoEval.clear();
  m_hypoTr.clear();

}

