/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Part2CaloFuture.h,v 1.2 2010-06-10 12:46:38 cattanem Exp $
#ifndef PART2CALOFUTURE_H
#define PART2CALOFUTURE_H 1

// Include files
#include "Track2CaloFuture.h"
#include "Event/ProtoParticle.h"
// from Gaudi
#include "CaloFutureInterfaces/IPart2CaloFuture.h"



/** @class Part2CaloFuture Part2CaloFuture.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-11-30
 */
class Part2CaloFuture : public Track2CaloFuture, virtual public IPart2CaloFuture {
public:
  /// Standard constructor
  Part2CaloFuture( const std::string& type,
               const std::string& name,
               const IInterface* parent);

  StatusCode initialize() override;

  using Track2CaloFuture::match;
  bool match(const  LHCb::ProtoParticle* proto,
                     std::string det = DeCalorimeterLocation::Ecal,
                     CaloPlane::Plane plane = CaloPlane::ShowerMax,
                     double delta = 0.
                     ) override;
  bool match(const  LHCb::Particle* part,
                     std::string det = DeCalorimeterLocation::Ecal,
                     CaloPlane::Plane plane = CaloPlane::ShowerMax,
                     double delta = 0.
                     ) override;
  inline bool inAcceptance() override;


protected:
  bool setting (const  LHCb::Particle* part);
  bool setting (const  LHCb::ProtoParticle* proto);
  const LHCb::Particle*      m_particle;
  const LHCb::ProtoParticle* m_proto;
private:
};
#endif // PART2CALOFUTURE_H

inline bool Part2CaloFuture::inAcceptance(){
  if(m_det == DeCalorimeterLocation::Spd)  return m_proto->info(LHCb::ProtoParticle::additionalInfo::InAccSpd,  0.)!=0;
  if(m_det == DeCalorimeterLocation::Prs)  return m_proto->info(LHCb::ProtoParticle::additionalInfo::InAccPrs,  0.)!=0;
  if(m_det == DeCalorimeterLocation::Ecal) return m_proto->info(LHCb::ProtoParticle::additionalInfo::InAccEcal, 0.)!=0;
  if(m_det == DeCalorimeterLocation::Hcal) return m_proto->info(LHCb::ProtoParticle::additionalInfo::InAccHcal, 0.)!=0;
  return false;
}
