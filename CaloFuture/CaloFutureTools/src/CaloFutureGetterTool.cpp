/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// from Gaudi
// ============================================================================
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// ============================================================================
// local
// ============================================================================
#include "CaloFutureGetterTool.h"
// ============================================================================
/** @file
 *  Implementation file for class : CaloFutureGetterTool
 *
 *  @date 2009-04-17
 *  @author Olivier Deschamps
 */
// ============================================================================
// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureGetterTool )
// ============================================================================
// Standard constructor, initializes variables
// ============================================================================
CaloFutureGetterTool::CaloFutureGetterTool ( const std::string& type, const std::string& name,
                                 const IInterface* parent )
: base_class ( type, name , parent )
{
  declareInterface<ICaloFutureGetterTool>(this);

  std::string flag = context();
  if( std::string::npos != name.find ("HLT") ||
      std::string::npos != name.find ("Hlt") )flag="Hlt";

  // digits
  if((m_detMask&8)!=0)m_digiLoc.value().push_back( LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Hcal" , flag ) ) ;
  if((m_detMask&4)!=0)m_digiLoc.value().push_back( LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Ecal" , flag ) ) ;
  if((m_detMask&2)!=0)m_digiLoc.value().push_back( LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Prs"  , flag ) ) ;
  if((m_detMask&1)!=0)m_digiLoc.value().push_back( LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Spd"  , flag ) ) ;

  if((m_detMask&8)!=0)m_clusLoc.value().push_back( LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( "Hcal" , flag ) ) ;
  if((m_detMask&4)!=0)m_clusLoc.value().push_back( LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( "Ecal" , flag ) ) ;
  m_clusLoc.value().push_back( LHCb::CaloFutureAlgUtils::CaloFutureSplitClusterLocation( flag ) ) ;

  m_hypoLoc.value().push_back( LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation("Photons"      , flag ) );
  m_hypoLoc.value().push_back( LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation("Electrons"    , flag ) );
  m_hypoLoc.value().push_back( LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation("SplitPhotons" , flag ) );
  m_hypoLoc.value().push_back( LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation("MergedPi0s"   , flag ) );
}

// ============================================================================

StatusCode CaloFutureGetterTool::initialize()
{
  StatusCode sc = base_class::initialize();
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )debug() << "Initialize CaloFuture2CaloFuture tool " << endmsg;

  counterStat = tool<IFutureCounterLevel>("FutureCounterLevel");

  if( m_detMask != 0xF) info() << "Incomplete calorimeter detector is requested - mask = " << m_detMask.value() << endmsg;
  //
  if((m_detMask&8)!=0)m_provider["Hcal"] = tool<ICaloFutureDataProvider>( "CaloFutureDataProvider" , "FutureHcalDataProvider" );
  if((m_detMask&4)!=0)m_provider["Ecal"] = tool<ICaloFutureDataProvider>( "CaloFutureDataProvider" , "FutureEcalDataProvider" );
  if((m_detMask&2)!=0)m_provider["Prs"]  = tool<ICaloFutureDataProvider>( "CaloFutureDataProvider" , "FuturePrsDataProvider"  );
  if((m_detMask&1)!=0)m_provider["Spd"]  = tool<ICaloFutureDataProvider>( "CaloFutureDataProvider" , "FutureSpdDataProvider"  );
  //
  // subscribe to the incidents
  IIncidentSvc* inc = incSvc() ;
  if ( 0 != inc )
  {
    inc -> addListener  ( this , IncidentType::BeginEvent ) ;
    inc -> addListener  ( this , IncidentType::EndEvent   ) ;
  }
  // prepare the known locations:
  //
  // digits
  if ( m_digiUpd ){
    m_digits.clear() ;
    for ( std::vector<std::string>::iterator iloc = m_digiLoc.begin() ;m_digiLoc.end() != iloc; ++iloc ){
      m_digits[ *iloc ] = 0 ;
    }
  }
  // clusters
  if ( m_clusUpd ){
    m_clusters.clear() ;
    for ( std::vector<std::string>::iterator iloc = m_clusLoc.begin() ;m_clusLoc.end() != iloc; ++iloc){
      m_clusters[ *iloc ] = 0 ;
    }
  }
  // hypos
  if ( m_hypoUpd ){
    m_hypos.clear() ;
    for ( std::vector<std::string>::iterator iloc = m_hypoLoc.begin() ;m_hypoLoc.end() != iloc; ++iloc){
      m_hypos[ *iloc ] = 0 ;
    }
  }
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
StatusCode CaloFutureGetterTool::finalize()
{
  // un-subscribe to the incidents
  IIncidentSvc* inc = incSvc() ;
  if ( 0 != inc ) { inc -> removeListener  ( this ) ; }
  // clear data
  nullify () ;
  m_provider . clear () ;
  // finalize the base
  return base_class::finalize () ;
}
// ============================================================================
namespace
{
  // =========================================================================
  template <class TYPE>
  void __nullify ( std::map<std::string,TYPE*>& _map )
  {
    for ( typename std::map<std::string,TYPE*>::iterator ientry = _map.begin() ;
          _map.end() != ientry ; ++ientry ) { ientry->second = 0 ; }
  }
  // ==========================================================================
}
// ============================================================================
void CaloFutureGetterTool::nullify()
{
  __nullify ( m_digits   ) ;
  __nullify ( m_clusters ) ;
  __nullify ( m_hypos    ) ;
}
// ============================================================================
// getters
// ============================================================================
LHCb::CaloDigits* CaloFutureGetterTool::digits ( const std::string& loc )
{
  std::map<std::string,LHCb::CaloDigits*>::iterator it = m_digits.find( loc );
  //
  if ( m_digits.end () == it ){
    Error ( "Illegal Attempt to retrive digits   from '" + loc + "'" ).ignore() ;
    return 0 ;
  }
  //
  if ( 0 != it->second ) { return it -> second ; }
  //
  it -> second = getIfExists<LHCb::CaloDigits>( loc );
  if ( NULL != it->second ){
    if(counterStat->isQuiet())counter ("#Digits   @ " + loc ) += it->second->size() ;
    return it->second ;
  }
  //
  Error ("No Digits   at " + loc ).ignore() ;
  return 0 ;
}
// ============================================================================
LHCb::CaloClusters* CaloFutureGetterTool::clusters ( const std::string& loc )
{
  std::map<std::string,LHCb::CaloClusters*>::iterator it = m_clusters.find( loc );
  //
  if ( m_clusters.end () == it )
  {
    Error ( "Illegal attempt to retrive clusters from '" + loc + "'" ).ignore() ;
    return 0 ;
  }
  //
  if ( 0 != it->second ) { return it->second ; }
  //
  it -> second = getIfExists<LHCb::CaloClusters>( loc );
  if ( NULL != it->second )
  {
    if(counterStat->isQuiet())counter ("#Clusters @ " + loc  ) += it->second->size() ;
    return it->second ;
  }
  //
  Error ("No Clusters at " + loc ).ignore() ;
  return 0 ;
}
// ============================================================================
LHCb::CaloHypos*    CaloFutureGetterTool::hypos    ( const std::string& loc )
{
  std::map<std::string,LHCb::CaloHypos*>::iterator it = m_hypos.find( loc );
  //
  if ( m_hypos.end () == it )
  {
    Error ( "Illegal attempt to retrive hypos    from '" + loc + "'" ).ignore() ;
    return 0 ;
  }
  //
  if ( 0 != it->second ) { return it->second ; }
  //
  it -> second = getIfExists<LHCb::CaloHypos>( loc );
  if ( NULL != it->second )
  {
    if(counterStat->isQuiet())counter ("#Hypos    @ " + loc  ) += it->second->size() ;
    return it->second ;
  }
  //
  Error ("No Hypos    at " + loc ).ignore() ;
  return 0 ;
}
// ============================================================================
void CaloFutureGetterTool::update()
{
  // digits
  if ( m_digiUpd )
  {
    for(std::vector<std::string>::iterator iloc = m_digiLoc.begin();m_digiLoc.end() != iloc; ++iloc){
      if ( exist<LHCb::CaloDigits>(*iloc) && 0 == m_digits[*iloc] ){
        LHCb::CaloDigits* digits = get<LHCb::CaloDigits>( *iloc );
        m_digits[ *iloc ] =  digits ;
        if(counterStat->isQuiet())counter ("#Digits   @ " + (*iloc) ) += digits->size() ;
      }
    }
  }
  // clusters
  if( m_clusUpd ) {
    for(const auto& iloc : m_clusLoc) {
      LHCb::CaloClusters* clusters = getIfExists<LHCb::CaloClusters>( iloc );
      if ( clusters ) {
        m_clusters[ iloc ] = clusters ;
        if(counterStat->isQuiet())counter ("#Clusters @ " + iloc ) += clusters->size() ;
      }
    }
  }
  // hypos
  if( m_hypoUpd ) {
    for(const auto& iloc : m_hypoLoc) {
      LHCb::CaloHypos* hypos = getIfExists<LHCb::CaloHypos>( iloc );
      if ( hypos ) {
        m_hypos[ iloc ] = hypos ;
        if(counterStat->isQuiet())counter ("#Hypos    @ " + iloc ) += hypos->size() ;
      }
    }
  }
  // provider
  //  if( m_provUpd)
  // {
  //  for(std::map<std::string,ICaloFutureDataProvider*>::iterator ip = m_provider.begin();m_provider.end()!=ip;++ip)
  //  {
  //    const std::string& det = ip->first;
  //    ICaloFutureDataProvider* provider = ip->second;
  //    m_prov[det] = provider->getBanks();
  //  }
  //  }

}
