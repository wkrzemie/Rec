/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from  LHCb
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloHypo.h"
// local
#include "CheckCaloFutureHypoRef.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CheckCaloFutureHypoRef
//
// 2012-05-14 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CheckCaloFutureHypoRef )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CheckCaloFutureHypoRef::CheckCaloFutureHypoRef( const std::string& name,
                                    ISvcLocator* pSvcLocator)
: GaudiAlgorithm ( name , pSvcLocator )
{
  using namespace LHCb::CaloFutureAlgUtils;
  m_inputs.value() = {
    CaloFutureHypoLocation("Photons"   , context()),
    CaloFutureHypoLocation("Electrons" , context()),
    CaloFutureHypoLocation("MergedPi0s", context()) 
  };
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CheckCaloFutureHypoRef::execute() {

 if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

 for (const auto& loc :  m_inputs ) {
    const LHCb::CaloHypos* hypos = getIfExists<LHCb::CaloHypos> (loc);
    if ( !hypos ) continue;
    if(counterStat->isQuiet()) counter("#Hypos in " + loc) += hypos->size();
    int bLink=0;
    for (const auto& h : *hypos ) {
      for( const auto&  cluster : h->clusters() ) {
        if( !cluster ) bLink++;
        else if(counterStat->isVerbose())counter("Cluster energy " +loc)+=cluster->e();
      }
    }
    if(counterStat->isQuiet())counter("Broken SmarRef " +loc) += bLink;
    if(bLink != 0)Warning("CaloHypo -> CaloCluster* SmartReference is broken for "+loc,StatusCode::SUCCESS).ignore();
 }
  return StatusCode::SUCCESS;
}

//=============================================================================
