/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 

// LHCb
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/Line.h"
#include "LHCbMath/GeomFun.h"
#include "Event/Track.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
// local
#include "Track2CaloFuture.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Track2CaloFuture
//
// Simple tool to propagate track to Calo reference planes
//
// 2007-06-25 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( Track2CaloFuture )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Track2CaloFuture::Track2CaloFuture( const std::string& type,
                            const std::string& name,
                            const IInterface* parent )
  : GaudiTool ( type, name , parent )
{
  declareInterface<ITrack2CaloFuture>(this);
}

//=============================================================================
StatusCode Track2CaloFuture::initialize(){
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);
  m_extrapolator = tool<ITrackExtrapolator>( m_extrapolatorType,"Extrapolator",this );
  return StatusCode::SUCCESS;  
}
//=============================================================================
bool Track2CaloFuture::match(const LHCb::Track* track, std::string det,CaloPlane::Plane plane , double delta, const LHCb::Tr::PID pid ){
  m_status = setting(track);
  m_det    = det;
  m_calo   = getDet<DeCalorimeter>( det );
  m_state  = caloState(plane, delta, pid);
  m_cell   = m_calo->Cell( m_state.position() );
  m_valid  = m_calo->valid( m_cell );
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
    debug() << " Track2CaloFuture setting [" << *track <<","<< det<<"] status : " <<m_status << endmsg;
  return m_status;
}
//=============================================================================
LHCb::State Track2CaloFuture::caloState(CaloPlane::Plane plane , double delta, const LHCb::Tr::PID pid ){

  LHCb::State state; // empty state
  if( !m_status ) return state;
  
  // get caloPlane
  ROOT::Math::Plane3D refPlane = m_calo->plane( plane );
  // propagate state to refPlane
  LHCb::State calostate( m_track->closestState( refPlane ) );
  StatusCode sc = m_extrapolator->propagate ( calostate, refPlane , m_tolerance , pid  );
  if(sc.isFailure())return state;  

  if( 0. == delta)return calostate; 
  
  Gaudi::XYZVector dir (calostate.tx(), calostate.ty(), 1.);
  Gaudi::XYZPoint  point = calostate.position() + delta * dir/dir.R();
  // extrapolate to the new point
  sc = m_extrapolator->propagate ( calostate, point.z(), pid );
  if(sc.isFailure())return state;
  return calostate;
}
//=============================================================================
bool Track2CaloFuture::setting(const LHCb::Track* track){
  m_track   = track;
  return ( NULL == m_track) ? false : true;
}




LHCb::State Track2CaloFuture::closestState(LHCb::CaloCluster* cluster,const LHCb::Tr::PID pid  ){
  return closestState( cluster->position(),pid);
}

LHCb::State Track2CaloFuture::closestState(LHCb::CaloHypo* hypo,const LHCb::Tr::PID pid ){
  LHCb::State state ;//emtpy state
  LHCb::CaloPosition* calopos  = hypo->position();
  if(calopos == NULL)return state;
  return closestState( *calopos ,pid);
}
LHCb::State Track2CaloFuture::closestState(LHCb::CaloPosition calopos,const LHCb::Tr::PID pid ){
  double x = calopos.parameters()(LHCb::CaloPosition::Index::X);
  double y = calopos.parameters()(LHCb::CaloPosition::Index::Y);
  return closestState(x,y, pid);
}
LHCb::State Track2CaloFuture::closestState(LHCb::CaloCellID cellID,const LHCb::Tr::PID pid ){
  Gaudi::XYZPoint point = m_calo->cellCenter( cellID );
  return closestState(point.X(),point.Y(), pid);
}

//=============================================================================
LHCb::State Track2CaloFuture::closestState(double x, double y,const LHCb::Tr::PID pid){
  LHCb::State state; // empty state
  if( !m_status ) return state;
  
  // get state on Front of Ecal
  LHCb::State calostate = caloState( CaloPlane::Front);
  if(calostate.z() == 0 ) return state;

  // get frontPlane
  ROOT::Math::Plane3D frontPlane = m_calo->plane( CaloPlane::Front );

  // Define calo line (from transversal barycenter) and track line in Ecal
  typedef Gaudi::Math::Line<Gaudi::XYZPoint,Gaudi::XYZVector> Line;
  Gaudi::XYZVector normal = frontPlane.Normal();
  double zEcal = ( -normal.X()*x -normal.Y()*y - frontPlane.HesseDistance() )/normal.Z(); // tilt
  Gaudi::XYZPoint point( x , y , zEcal );
  Line cLine( point ,frontPlane.Normal() );
  Line tLine( calostate.position() , calostate.slopes() );

  // Find points of closest distance between calo Line and track Line
  Gaudi::XYZPoint cP,tP;
  Gaudi::Math::closestPoints<Line,Line,Gaudi::XYZPoint>(cLine,tLine,cP,tP);

  // propagate the state the new Z of closest distance
  StatusCode sc = m_extrapolator->propagate ( calostate, tP.Z() , pid);

  if(sc.isFailure())return state;
  return calostate;
}
