/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALOFUTUREPIDS_TOVECTOR_H 
#define CALOFUTUREPIDS_TOVECTOR_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL 
// ============================================================================
#include <vector>
#include <type_traits>
#include <string>
// ============================================================================
// Local 
// ============================================================================
#include "ToString.h"
// ============================================================================

namespace Gaudi
{
  namespace Utils 
  {
    // ========================================================================
    template <typename... TYPE>
    std::vector<typename std::common_type<TYPE...>::type> toVector( const TYPE&... o )
    { return { o... }; }
    // ========================================================================
    
    // ========================================================================
    template <typename... TYPE> 
    std::string toString( const TYPE&... o )
    { return toString( toVector( o... ) ) ; }
    // ========================================================================
    
  } // end of namespace Utils 
} // end of namespace Gaudi 

// ============================================================================
// The END 
// ============================================================================
#endif // TOVECTOR_H
// ============================================================================
