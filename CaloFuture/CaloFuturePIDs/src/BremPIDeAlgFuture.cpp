/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureID2DLL.h"

// ============================================================================
/** @class BremPIDeAlgFuture  BremPIDeAlgFuture.cpp
 *  The preconfigured instance of class CaloFutureID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================

class BremPIDeAlgFuture final : public CaloFutureID2DLL {
 public:
  /// Standard protected constructor
  BremPIDeAlgFuture(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureID2DLL(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

    Gaudi::Functional::updateHandleLocation(
        *this, "Input", CaloFutureIdLocation("BremChi2", context()));
    Gaudi::Functional::updateHandleLocation(
        *this, "Output", CaloFutureIdLocation("BremPIDe", context()));

    _setProperty("nVlong" , Gaudi::Utils::toString(200));
    _setProperty("nVvelo" , Gaudi::Utils::toString(200));
    _setProperty("nVupstr", Gaudi::Utils::toString(200));
    _setProperty("nMlong" , Gaudi::Utils::toString(50 * Gaudi::Units::GeV));
    _setProperty("nMvelo" , Gaudi::Utils::toString(50 * Gaudi::Units::GeV));
    _setProperty("nMupstr", Gaudi::Utils::toString(50 * Gaudi::Units::GeV));

    _setProperty("HistogramU"   , "DLL_Long");
    _setProperty("HistogramL"   , "DLL_Long");
    _setProperty("HistogramV"   , "DLL_Long");
    _setProperty("ConditionName", "Conditions/ParticleID/Calo/BremPIDe");

    _setProperty("HistogramU_THS", "CaloFuturePIDs/CALO/BREMPIDE/h3");
    _setProperty("HistogramL_THS", "CaloFuturePIDs/CALO/BREMPIDE/h3");
    _setProperty("HistogramV_THS", "CaloFuturePIDs/CALO/BREMPIDE/h3");

    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Velo, LHCb::Track::Types::Long,
                                     LHCb::Track::Types::Upstream));
  };
};

// ============================================================================

DECLARE_COMPONENT( BremPIDeAlgFuture )

// ============================================================================
