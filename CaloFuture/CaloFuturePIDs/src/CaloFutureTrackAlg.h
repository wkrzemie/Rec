/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREUTILS_CALOFUTURETrackAlg_H
#define CALOFUTUREUTILS_CALOFUTURETrackAlg_H 1

// Include files
#include <string>
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "Event/TrackUse.h"

// ============================================================================
/** @class CaloFutureTrackAlg CaloFutureTrackAlg.h
 *
 *  Helper base algorithm form implementation 'track'-related algorithms
 *
 *  It helps to select the appropriate tracks for further processing
 *  Track is 'selected' if it fullfills
 *
 *   - general features
 *   - category
 *   - "algorithm"
 *
 *  @see TrackUse
 *
 *  @see CaloFutureAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *
 *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
 *  @date   2004-10-26
 */
// ============================================================================

class TrackUse;
class Track;

class CaloFutureTrackAlg : public GaudiAlgorithm {
 public:
  /// standard algorithm initialization
  StatusCode initialize() override;

  void _setProperty(const std::string& p, const std::string& v);

  /// C++11 non-copyable idiom
  CaloFutureTrackAlg() = delete;
  CaloFutureTrackAlg(const CaloFutureTrackAlg&) = delete;
  CaloFutureTrackAlg& operator=(const CaloFutureTrackAlg&) = delete;

 protected:
  /// Standard constructor
  CaloFutureTrackAlg(const std::string& name, ISvcLocator* svcloc);

  virtual ~CaloFutureTrackAlg(){};
  
  /// check if the track to be used @see TrackUse
  inline bool use(const LHCb::Track* track) const { return m_use(track); }
  /// print the short infomration about track flags
  inline MsgStream& print(MsgStream& stream, const LHCb::Track* track) const;
  /// print the short infomration about track flags
  inline MsgStream& print(const LHCb::Track* track,
                          const MSG::Level level = MSG::INFO) const;

  ToolHandle<IFutureCounterLevel> counterStat{"FutureCounterLevel"};
  TrackUse m_use { *this };
};

// ============================================================================
/// print the short infomration about track flags
// ============================================================================

inline MsgStream& CaloFutureTrackAlg::print(MsgStream& stream,
                                      const LHCb::Track* track) const {
  return stream.isActive() ? m_use.print(stream, track) : stream;
}

// ============================================================================
/// print the short infomration about track flags
// ============================================================================

inline MsgStream& CaloFutureTrackAlg::print(const LHCb::Track* track,
                                      const MSG::Level level) const {
  return print(msgStream(level), track);
}

// ============================================================================
#endif  // CALOFUTURETrackAlg_H
// ============================================================================
