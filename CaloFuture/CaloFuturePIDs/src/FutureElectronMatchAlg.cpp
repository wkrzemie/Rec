/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureTrackMatchAlg.h"

// ============================================================================
/** @class FutureElectronMatchAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// ============================================================================
using TABLE = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
using CALOFUTURETYPES = LHCb::CaloHypos;

struct FutureElectronMatchAlg final : CaloFutureTrackMatchAlg<TABLE,CALOFUTURETYPES> {
  static_assert(std::is_base_of<LHCb::CaloFuture2Track::IHypoTrTable2D, TABLE>::value,
                "Table must inherit from IHypoTrTable2D");

  FutureElectronMatchAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureTrackMatchAlg<TABLE,CALOFUTURETYPES>(name, pSvc) {
    Gaudi::Functional::updateHandleLocation(*this, "Calos",  LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation("Electrons",context()));
    Gaudi::Functional::updateHandleLocation(*this, "Output", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation("ElectronMatch",context()));
    Gaudi::Functional::updateHandleLocation(*this, "Filter", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation("InEcal",context()));

    _setProperty("Tool", "CaloFutureElectronMatch/ElectronMatchFuture");
    _setProperty("Threshold", "10000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
    _setProperty("TableSize", "1000");
  }

};

// ============================================================================

DECLARE_COMPONENT( FutureElectronMatchAlg )

