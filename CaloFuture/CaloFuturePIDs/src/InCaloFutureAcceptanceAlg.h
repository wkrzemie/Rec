/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREPIDS_INCALOFUTUREACCEPTANCEALG_H 
#define CALOFUTUREPIDS_INCALOFUTUREACCEPTANCEALG_H 1

// Include files
#include "CaloFutureTrackAlg.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include "TrackInterfaces/IInAcceptance.h"
#include "Relations/Relation1D.h"
#include "ToVector.h"

// ============================================================================
/** @class InCaloFutureAcceptanceAlg InCaloFutureAcceptanceAlg.h
 *
 *  the trivial algorithm to fill "InCaloFutureAcceptance" table 
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

using Table =  LHCb::Relation1D<LHCb::Track,bool>;

class InCaloFutureAcceptanceAlg
    : public Gaudi::Functional::Transformer<
          Table(const LHCb::Tracks&),
          Gaudi::Functional::Traits::BaseClass_t<CaloFutureTrackAlg> >
{
  // check the proper convertability
  static_assert(std::is_base_of<LHCb::CaloFuture2Track::ITrAccTable, Table>::value,
                "Table must inherit from ITrAccTable");

  public:
    /// Standard constructor
    InCaloFutureAcceptanceAlg(const std::string& name, ISvcLocator* pSvc);
    /// algorithm execution
    Table operator()(const LHCb::Tracks&) const override;

    ToolHandle<IInAcceptance> m_tool {this, "Tool", "<NOT DEFINED>"};

    // counter
    mutable Gaudi::Accumulators::StatCounter<> m_nTracks{this, "#total tracks"};
    mutable Gaudi::Accumulators::StatCounter<> m_nAccept{this, "#tracks in acceptance"};
    mutable Gaudi::Accumulators::StatCounter<> m_nLinks {this, "#links in table"};
};

// ============================================================================
#endif  // CALOFUTUREPIDS_INCALOFUTUREACCEPTANCEALG_H
// ============================================================================
