/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureChi22ID.h"
#include "ToVector.h"

// ============================================================================
/** @class FutureEcalChi22ID FutureEcalChi22ID.cpp
 *  The preconfigured instance of class CaloFutureChi22ID
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
using TABLEI = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
using TABLEO = LHCb::Relation1D<LHCb::Track, float>;

struct FutureEcalChi22ID final : public CaloFutureChi22ID<TABLEI,TABLEO> {
  static_assert(
      std::is_base_of<LHCb::CaloFuture2Track::IHypoTrTable2D, TABLEI>::value,
      "TABLEI must inherit from IHypoTrTable2D");
  FutureEcalChi22ID(const std::string& name, ISvcLocator* pSvc)
    : CaloFutureChi22ID<TABLEI,TABLEO>(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;
    Gaudi::Functional::updateHandleLocation(*this,  "Input", CaloFutureIdLocation("ElectronMatch", context()));
    Gaudi::Functional::updateHandleLocation(*this, "Output", CaloFutureIdLocation("EcalChi2", context()));
    // @todo it must be in agrement with "Threshold" for ElectonMatchAlg
    _setProperty("CutOff", "10000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                   LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                   LHCb::Track::Types::Downstream));
  };
};

// ============================================================================
DECLARE_COMPONENT( FutureEcalChi22ID )
