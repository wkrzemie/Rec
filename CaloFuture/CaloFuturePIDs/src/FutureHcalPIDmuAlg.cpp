/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureID2DLL.h"

// ============================================================================
/** @class FutureHcalPIDmuAlg  FutureHcalPIDmuAlg.cpp
 *  The preconfigured instance of class CaloFutureID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================

class FutureHcalPIDmuAlg final : public CaloFutureID2DLL {
 public:
  FutureHcalPIDmuAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureID2DLL(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

    Gaudi::Functional::updateHandleLocation(
        *this, "Input", CaloFutureIdLocation("HcalE", context()));
    Gaudi::Functional::updateHandleLocation(
        *this, "Output", CaloFutureIdLocation("HcalPIDmu", context()));

    _setProperty("nVlong"  , Gaudi::Utils::toString(10 * Gaudi::Units::GeV));
    _setProperty("nVdown"  , Gaudi::Utils::toString(10 * Gaudi::Units::GeV));
    _setProperty("nVTtrack", Gaudi::Utils::toString(10 * Gaudi::Units::GeV));
    _setProperty("nMlong"  , Gaudi::Utils::toString(25 * Gaudi::Units::GeV));
    _setProperty("nMdown"  , Gaudi::Utils::toString(25 * Gaudi::Units::GeV));
    _setProperty("nMTtrack", Gaudi::Utils::toString(25 * Gaudi::Units::GeV));

    _setProperty("HistogramL"   , "DLL_Long");
    _setProperty("HistogramD"   , "DLL_Downstream");
    _setProperty("HistogramT"   , "DLL_Ttrack");
    _setProperty("ConditionName", "Conditions/ParticleID/Calo/HcalPIDmu");

    _setProperty("HistogramL_THS", "CaloFuturePIDs/CALO/HCALPIDM/h3");
    _setProperty("HistogramD_THS", "CaloFuturePIDs/CALO/HCALPIDM/h5");
    _setProperty("HistogramT_THS", "CaloFuturePIDs/CALO/HCALPIDM/h6");

    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                     LHCb::Track::Types::Downstream));
  };
};

// ============================================================================

DECLARE_COMPONENT( FutureHcalPIDmuAlg )

// ============================================================================
