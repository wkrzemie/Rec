/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "InCaloFutureAcceptance.h"

// ============================================================================
/** @class InHcalFutureAcceptance    
 *  The precofigured instance of InCaloFutureAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================

struct InHcalFutureAcceptance     final : InCaloFutureAcceptance {
  /// standard constructor
  InHcalFutureAcceptance(const std::string& type, const std::string& name,
                   const IInterface* parent)
      : InCaloFutureAcceptance(type, name, parent) {
    _setProperty("Calorimeter", DeCalorimeterLocation::Hcal);
    _setProperty("UseFiducial", "true");
    _setProperty("Tolerance", "10");  /// 10 * Gaudi::Units::mm
  };

  /// C++11 non-copyable idiom
  InHcalFutureAcceptance() = delete;
  InHcalFutureAcceptance(const InHcalFutureAcceptance&) = delete;
  InHcalFutureAcceptance& operator=(const InHcalFutureAcceptance&) = delete;
};

// ============================================================================

DECLARE_COMPONENT( InHcalFutureAcceptance     )

// ============================================================================
