/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREPIDS_CALOFUTURECHI22ID_H
#define CALOFUTUREPIDS_CALOFUTURECHI22ID_H 1

// Include files
#include "CaloFutureTrackAlg.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"

// ============================================================================
/** @class CaloFutureChi22ID CaloFutureChi22ID.h
 *
 *
 *  @author Ivan BELYAEV
 *  @date   2006-06-18
 */
// ============================================================================
template <typename TABLEI, typename TABLEO>
class CaloFutureChi22ID : public Gaudi::Functional::Transformer<TABLEO(const LHCb::Tracks&,const TABLEI&), Gaudi::Functional::Traits::BaseClass_t<CaloFutureTrackAlg> > 
{
  public:
    using CaloFutureTrackAlg::context;
    using CaloFutureTrackAlg::use;
    using CaloFutureTrackAlg::getProperty;
    using CaloFutureTrackAlg::_setProperty;
    using base_type = Gaudi::Functional::Transformer<TABLEO(const LHCb::Tracks&,const TABLEI&),Gaudi::Functional::Traits::BaseClass_t<CaloFutureTrackAlg> >;
    using KeyValue  = typename base_type::KeyValue;

    // standard constructor
    CaloFutureChi22ID(const std::string& name, ISvcLocator* pSvc);

    /// algorithm execution
    TABLEO operator()(const LHCb::Tracks& tracks, const TABLEI& input) const override;

  protected:
    Gaudi::Property<float> m_large{ this, "CutOff", 10000, "large value"};

    mutable Gaudi::Accumulators::StatCounter<>  m_nLinks {this, "#links in table"};
    mutable Gaudi::Accumulators::StatCounter<>  m_nTracks {this, "#total tracks"};
};
#endif  // CALOFUTURECHI22ID_H
// ============================================================================
