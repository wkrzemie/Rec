/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREENERGYFLOWMONITOR_H 
#define CALOFUTUREENERGYFLOWMONITOR_H 1

// Includes
#include "Event/CaloDigit.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureMoniAlg.h"

// =============================================================================

/** @class CaloFutureEFlowAlg CaloFutureEFlowAlg.h
 *  
 *
 *  The algorithm for dedicated "EnergyFlow" monitoring of "CaloDigits" containers.
 *  The algorithm produces the following histograms:
 *   1. CaloDigit multiplicity
 *   2. CaloDigit ocupancy 2D plot per area
 *   3. CaloDigit energy and transverse energy 2D plot per area
 *  The same set of histograms, but with cut on Et (or E), is produced if specified
 *
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @p "Name" is the name of the algorithm
 *
 *  @see   CaloFutureMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Aurelien Martens
 *  @date   2009-04-08
 */

namespace {
  using Input = LHCb::CaloDigit::Container;
}

class CaloFutureEFlowBase: public CaloFutureMoniAlg {

public:
  using CaloFutureMoniAlg::CaloFutureMoniAlg;
  StatusCode initialize() override;

protected:
  DeCalorimeter *m_calo = nullptr;

  // Helper method to determine input location from detector name
  std::string digitLocation() const {
    if(detData()     == "Ecal" ){ return LHCb::CaloDigitLocation::Ecal; }
    else if(detData()== "Hcal" ){ return LHCb::CaloDigitLocation::Hcal; }
    else if(detData()== "Prs"  ){ return LHCb::CaloDigitLocation::Prs; }
    else if(detData()== "Spd"  ){ return LHCb::CaloDigitLocation::Spd; }    
    return "";
  }

  std::string deCaloFutureLocation() const {
    if      ( "Ecal" == detData() ) { return DeCalorimeterLocation::Ecal; }  
    else if ( "Hcal" == detData() ) { return DeCalorimeterLocation::Hcal; }
    else if ( "Prs"  == detData() ) { return DeCalorimeterLocation::Prs; }  
    else if ( "Spd"  == detData() ) { return DeCalorimeterLocation::Spd; }  
    return "";
  }

  // Main computation to be shared by subclass
  void process_digits(const Input&) const;

private:
  Gaudi::Property<float> m_eFilterMin   { this, "EnergyFilterMin", -999};
  Gaudi::Property<float> m_etFilterMin  { this, "EtFilterMin"    , -999};
  Gaudi::Property<float> m_eFilterMax   { this, "EnergyFilterMax", -999};
  Gaudi::Property<float> m_etFilterMax  { this, "EtFilterMax"    , -999};
  Gaudi::Property<int>   m_ADCFilterMin { this, "ADCFilterMin"   , -999};
  Gaudi::Property<int>   m_ADCFilterMax { this, "ADCFilterMax"   , -999};

};
#endif // CALOFUTUREENERGYFLOWMONITOR_H

