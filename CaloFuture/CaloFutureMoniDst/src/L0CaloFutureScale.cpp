/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "GaudiAlg/Consumer.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloHypo.h"
#include "Event/CaloDataFunctor.h"
#include "Event/L0CaloCandidate.h"
#include "Event/L0DUBase.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureMoniAlg.h"

// ============================================================================

namespace {
  using Input = LHCb::CaloHypo::Container;
  using Inputs = LHCb::L0CaloCandidates;

  // Return True if this candidate match with the reference cellID
  bool l0calo_match( const LHCb::CaloCellID& id, const LHCb::L0CaloCandidate* cand ){
    const auto l0id = cand->id();
    // abort if area mismatched
    if( l0id.area() != id.area() || abs((int)l0id.row()-(int)id.row())>1 || abs((int)l0id.col()-(int)id.col())>1)
      return false;
    // abort if type mismatched
    const auto type = cand->type();
    if( type != L0DUBase::CaloType::Electron && type != L0DUBase::CaloType::Photon)
      return false;
    // This is the one, stop search
    return true;
  }
}

//------------------------------------------------------------------------------

class L0CaloFutureScale final
: public Gaudi::Functional::Consumer<void(const Input&, const Inputs&),
    Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>>
{
public:
  /// standard algorithm initialization
  StatusCode initialize() override;
  void operator()(const Input&, const Inputs&) const override;

  L0CaloFutureScale( const std::string &name, ISvcLocator *pSvcLocator );

private:
  Gaudi::Property<int>   m_ratBin { this, "RatioMin", 0.2};
  Gaudi::Property<float> m_ratMax { this, "RatioMax", 1.7};
  Gaudi::Property<float> m_ratMin { this, "RatioBin", 150};

  DeCalorimeter* m_ecal = nullptr;
};

// =============================================================================

DECLARE_COMPONENT( L0CaloFutureScale )

// =============================================================================

L0CaloFutureScale::L0CaloFutureScale( const std::string &name, ISvcLocator *pSvcLocator )
: Consumer( name, pSvcLocator, {
    KeyValue{ "Input" , "" },
    KeyValue{ "Inputs", LHCb::L0CaloCandidateLocation::Full },
})
{
  /*
  * During genconf.exe, the default name "DefaultName" is not well-supported
  * by the CaloFutureAlgUtils, returning the null string "" as a location path,
  * which will raise exception in `updateHandleLocation` --> `setProperty`.
  * To get around this, the location will only be updated outside the genconf.
  */
  if( name != "DefaultName" ){
    const auto Input = LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( name , context() );
    updateHandleLocation( *this, "Input", Input );
  }
}

// =============================================================================

StatusCode L0CaloFutureScale::initialize(){
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc; // error already printedby GaudiAlgorithm
  hBook1(  "0", "matched L0Calo type " + inputLocation(),  0   ,    2   , 2  );
  h1binLabel("0",1,"L0Electron");
  h1binLabel("0",2,"L0Photon");

  hBook1(  "1", "L0Calo(Et)/CaloHypo(Et) " + inputLocation(),  m_ratMin   ,    m_ratMax   , m_ratBin  );
  hBook1(  "2", "L0Calo(Et)/CaloCluster(Et) " + inputLocation(),  m_ratMin   ,    m_ratMax   , m_ratBin  );
  hBook1(  "3", "CaloCluster/CaloHypo(Et) " + inputLocation(),  m_ratMin   ,    m_ratMax   , m_ratBin  );
  m_ecal = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  return StatusCode::SUCCESS;
}


// =============================================================================

void L0CaloFutureScale::operator()(const Input& hypos, const Inputs& candidates) const {

  // produce histos ?
  if ( !produceHistos() ) return;

  // check data
  if ( hypos.empty() ){
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << "Found empty hypos at " << inputLocation() << endmsg;
    return; // StatusCode::SUCCESS;
  }

  // Begin loop
  initFutureCounters();
  for( const auto& hypo: hypos ){

    // Abort if ET too low
    LHCb::CaloMomentum momentum( hypo );
    const double et = momentum.pt();
    if(et < m_etFilter) continue;

    // Abort if no clusters found
    if( hypo->clusters().size() == 0 ) continue;

    // Retrieve the first cluster ID
    // SmartRef<LHCb::CaloCluster> cluster;
    const auto cluster = *(hypo->clusters().begin());
    const auto id = cluster->seed();

    // L0Calo matching
    LHCb::L0CaloCandidate* cand = nullptr;
    for( auto* cand0: candidates ){
      // This is the one, stop search
      if( l0calo_match( id, cand )){
        cand = cand0;
        break;
      }
    }

    // Abort if not found
    if( cand == nullptr ){
      if( m_counterStat->isQuiet()) counter("Matching L0cluster not found")+=1;
      continue;
    }

    const auto l0et = cand->etCode();
    const auto type = cand->type();

    // Calculate & fill histogram
    if( m_counterStat->isQuiet()) counter("Matching L0type") += type;
    double hratio = 20.*double(l0et) / et;
    double cratio = 20.*double(l0et) / (cluster->e() * m_ecal->cellSine( id )) ;
    double ratio  = (cluster->e() * m_ecal->cellSine( id )) / et;

    count(id);
    hFill1(id, "0", type    );
    hFill1(id, "1", hratio  );
    hFill1(id, "2", cratio  );
    hFill1(id, "3", ratio   );

    if(doHisto("4"))fillCaloFuture2D("4", id, hratio,  "L0Cluster(Et)/CaloHypo(Et) 2Dview"   );
    if(doHisto("5"))fillCaloFuture2D("5", id, cratio,  "L0Cluster(Et)/CaloCuster(Et) 2Dview" );
    if(doHisto("6"))fillCaloFuture2D("6", id, cratio,  "CaloCluster(Et)/CaloHypo(Et) 2Dview" );
  }

  return;
}
