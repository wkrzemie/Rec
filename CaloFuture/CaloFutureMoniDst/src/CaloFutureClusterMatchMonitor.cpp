/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "GaudiAlg/Consumer.h"
#include "Relations/RelationWeighted2D.h"
#include "Event/CaloCluster.h"
#include "Event/Track.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureMoniAlg.h"

#include "CaloFutureMoniUtils.h" // Local

// =============================================================================

/** @class CaloFutureClusterMatchMonitor CaloFutureClusterMatchMonitor.cpp
 *
 *  The algorithm for trivial monitoring of matching of
 *  "CaloFutureClusters" with Tracks.
 *  It produces 5 histograms:
 *
 *  <ol>
 *  <li> Total Link              distribution               </li>
 *  <li> Link multiplicity       distribution               </li>
 *  <li> Minimal Weight          distribution               </li>
 *  <li> Maximal Weight          distribution               </li>
 *  <li>         Weight          distribution               </li>
 *  </ol>
 *
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @ "Name" is the name of the algorithm
 *
 *  @see   CaloFutureMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

using Input = LHCb::RelationWeighted2D< LHCb::CaloCluster, LHCb::Track, float >;
using Clusters = LHCb::CaloCluster::Container;

class CaloFutureClusterMatchMonitor final
: public Gaudi::Functional::Consumer<void(const Input&, const Clusters&, const Clusters&),
    Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>>
{
public:
  /// standard algorithm initialization
  StatusCode initialize() override;
  void operator()(const Input&, const Clusters&, const Clusters&) const override;

  CaloFutureClusterMatchMonitor( const std::string &name, ISvcLocator *pSvcLocator );
};

// =============================================================================

DECLARE_COMPONENT( CaloFutureClusterMatchMonitor )

// =============================================================================

CaloFutureClusterMatchMonitor::CaloFutureClusterMatchMonitor( const std::string &name, ISvcLocator *pSvcLocator )
: Consumer( name, pSvcLocator, {
    KeyValue{ "Input"             , "" },
    KeyValue{ "InputClusters"     , "" },
    KeyValue{ "InputSplitClusters", "" }
}){
  updateHandleLocation( *this, "Input" , LHCb::CaloFutureAlgUtils::CaloFutureIdLocation("ClusterMatch", context() ));
  updateHandleLocation( *this, "InputClusters", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation(name,context() ));
  updateHandleLocation( *this, "InputSplitClusters", LHCb::CaloFutureAlgUtils::CaloFutureSplitClusterLocation( context() ));
}

// =============================================================================

StatusCode CaloFutureClusterMatchMonitor::initialize(){
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;

  hBook1( "1", "log10(#Links+1) '"+inputLocation()+"'", 0,    4, 100 );
  hBook1( "2", "Tracks per cluster",                    0,   25,  25 );
  hBook1( "3", "Minimal weight",                        0,  100, 200 );
  hBook1( "4", "Maximal weight",                        0, 1000, 200 );
  hBook1( "5", "Weights",                               0, 1000, 500 );
  if( m_split ){
    Warning( "No area spliting allowed for CaloFutureClusterMatchMonitor").ignore();
    m_split = false;
  }
  return StatusCode::SUCCESS;
}

// =============================================================================

void CaloFutureClusterMatchMonitor::operator()(const Input& table, const Clusters& clusters1, const Clusters& clusters2 ) const {

  // produce histos ?
  if ( !produceHistos() ) return;

  // total number of links
  hFill1( "1", log10( table.relations().size() + 1. ) );

  // loop over all clusters
  auto filler = [&table, this](auto& clusters){
    for( const auto& cluster: clusters ){
      const auto& range = table.relations( cluster );
      // number of related tracks
      hFill1( "2", range.size() );
      if ( range.empty() ) continue;
      // minimal weight
      hFill1( "3", range.front().weight() );
      // maximal weight
      hFill1( "4", range.back().weight() );
      // all weights
      for( const auto& relation: range ){
        hFill1( "5", relation.weight() );
      }
    } // end of loop over clusters
  };

  filler(clusters1);
  filler(clusters2);

  if(m_counterStat->isQuiet()){
    counter("Monitor " + inputLocation<0>()) += table.relations().size();
    counter("Monitor " + inputLocation<1>()) += clusters1.size();
    counter("Monitor " + inputLocation<2>()) += clusters2.size();
  }
  return;
}
