/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ===========================================================================
#ifndef CALOFUTURERECO_CALOFUTURECLUSTERCOVARIANCEALG_H
#define CALOFUTURERECO_CALOFUTURECLUSTERCOVARIANCEALG_H 1
/// ===========================================================================
// Include files
// from STL
#include <string>
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"

struct ICaloFutureClusterTool   ;
class FutureSubClusterSelectorTool ;

/** @class CaloFutureClusterCovarianceAlg CaloFutureClusterCovarianceAlg.h
 *
 *   Simple algorithm for evaluation of covariance matrix
 *   for CaloFutureCluster object
 *
 *  @author Vanya BElyaev Ivan Belyaev
 *  @date   04/07/2001
 */

class CaloFutureClusterCovarianceAlg : public GaudiAlgorithm
{
public:

  /** Standard constructor
   *  @param   name          algorith name
   *  @param   pSvcLocator   pointer to Service Locator
   */
  CaloFutureClusterCovarianceAlg
  ( const std::string& name  ,
    ISvcLocator*       pSvcLocator );
  
  /** standard initialization method
   *  @see GaudiAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode initialize() override;

  /** standard execution method
   *  @see GaudiAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode execute() override;

  /** standard finalization method
   *  @see GaudiAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode finalize() override;

protected:

  inline ICaloFutureClusterTool*       cov    () const { return m_cov    ; }
  inline ICaloFutureClusterTool*       spread () const { return m_spread ; }
  inline FutureSubClusterSelectorTool* tagger () const { return m_tagger ; }


private:

  /// default constructor is private
  CaloFutureClusterCovarianceAlg();

  /** copy constructor is private
   *  @param copy object to be copied
   */
  CaloFutureClusterCovarianceAlg
  ( const  CaloFutureClusterCovarianceAlg& copy );

  /** assignement operator is private
   *  @param copy object to be copied
   */
  CaloFutureClusterCovarianceAlg& operator=
  ( const  CaloFutureClusterCovarianceAlg& copy );

private:

  bool                    m_copy = false ;  /// copy flag

  // tool used for covariance matrix calculation
  Gaudi::Property<std::string> m_covType {this, "CovarianceType", "FutureClusterCovarianceMatrixTool"};
  Gaudi::Property<std::string> m_covName {this, "CovarianceName"};
  ICaloFutureClusterTool*       m_cov    = nullptr ; ///< tool
  FutureSubClusterSelectorTool* m_tagger = nullptr ;

  // tool used for cluster spread estimation
  Gaudi::Property<std::string> m_spreadType {this, "SpreadType", "FutureClusterSpreadTool"};
  Gaudi::Property<std::string> m_spreadName {this, "SpreadName"};
  ICaloFutureClusterTool*       m_spread = nullptr ; ///< tool

  Gaudi::Property<std::string> m_outputData {this, "OutputData"};
  Gaudi::Property<std::string> m_inputData  {this, "InputData"};
  Gaudi::Property<std::string> m_tagName    {this, "TaggerName"};

  // following properties are inherited by the selector tool when defined:
  Gaudi::Property<std::vector<std::string>> m_taggerE {this, "EnergyTags"};
  Gaudi::Property<std::vector<std::string>> m_taggerP {this, "PositionTags"};

  // collection of known cluster shapes
  std::map<std::string , std::string> m_clusterShapes;
  std::map<std::string,std::vector<double> > m_covParams;
  IFutureCounterLevel* counterStat = nullptr;
  };

#endif // CALOFUTURERECO_CALOFUTURECLUSTERCOVARIANCEALG_H
