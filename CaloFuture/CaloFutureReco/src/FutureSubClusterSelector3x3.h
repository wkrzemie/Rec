/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_SUBCLUSTERSELECTOR3x3_H
#define CALOFUTURERECO_SUBCLUSTERSELECTOR3x3_H 1
// Include files
#include "FutureSubClusterSelectorBase.h"
#include "CaloFutureUtils/CellMatrix3x3.h"

/** @class FutureSubClusterSelector3x3 FutureSubClusterSelector3x3.h
 *
 *  The simplest concrete "subcluster selector" -
 *  it just tag/select digits which form
 *  3x3 sub-matrix centered with the seed cells
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   07/11/2001
 */

class FutureSubClusterSelector3x3  : public FutureSubClusterSelectorBase{
public:

  StatusCode initialize() override;

  /** The main processing method
   *
   *  @see ICaloFutureSubClusterTag
   *
   *  Error codes:
   *    - 225   cluster points to NULL
   *    - 226   empty cell/digit container for given cluster
   *    - 227   SeedCell is not found
   *    - 228   Seed points to NULL
   *
   *  @see ICaloFutureClusterTool
   *  @see ICaloFutureSubClusterTag
   *
   *  @param cluster pointer to CaloFutureCluster object to be processed
   *  @return status code
   */

  StatusCode tag ( LHCb::CaloCluster* cluster) const override;

  /** The main method - "undo" of tagging from "tag" method
   *  @see ICaloFutureSubClusterTag
   *  @param cluster pointer to ClaoCluster object to be untagged
   *  @return status code
   */
  StatusCode untag      ( LHCb::CaloCluster* cluster ) const override;

  /** Standard Tool Constructor
   *  @param type type of the tool (useless ? )
   *  @param name name of the tool
   *  @param parent the tool parent
   */
  FutureSubClusterSelector3x3( const std::string& type   ,
                         const std::string& name   ,
                         const IInterface*  parent );

private:

  CellMatrix3x3        m_matrix;

};

#endif // SUBCLUSTERSELECTOR3x3_H
