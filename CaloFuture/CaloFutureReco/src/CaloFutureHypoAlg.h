/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CaloFutureReco_CaloFutureHypoAlg_H
#define CaloFutureReco_CaloFutureHypoAlg_H 1
// Include files
// from STL
#include <string>
#include <vector>

#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "GaudiAlg/GaudiAlgorithm.h"
// forward declaration
struct ICaloFutureHypoTool ;

/** @class CaloFutureHypoAlg CaloFutureHypoAlg.h
 *
 *  Generic CaloFutureHypo Algorithm.
 *  It is just applies a set of ICaloFutureHypoTools
 *  to a container of CaloFutureHypo objects
 *  @see ICaloFutureHypoTool
 *  @see  CaloFutureHypo
 *  @see  CaloFutureAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/09/2002
 */

class CaloFutureHypoAlg : public GaudiAlgorithm
{
public:

  /** standard algorithm initialization
   *  @see CaloFutureAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode initialize() override;

  /** standard algorithm execution
   *  @see CaloFutureAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode execute() override;

  /** standard algorithm finalization
   *  @see CaloFutureAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode finalize() override;

  /** Standard constructor
   *  @param   name   algorithm name
   *  @param   svcloc pointer to service locator
   */
  CaloFutureHypoAlg( const std::string& name   ,
              ISvcLocator*       svcloc );

private:

  typedef std::vector<std::string>    Names ;
  typedef std::vector<ICaloFutureHypoTool*> Tools ;

  /// list of tool type/names
  Gaudi::Property<Names> m_names
    {this, "Tools", {}, "The list of generic Hypo-tools to be applied"};

  /// list of tools
  Tools   m_tools ;
  Gaudi::Property<std::string> m_inputData {this, "InputData"};
  Gaudi::Property<std::string> m_type      {this, "HypoType"};
  IFutureCounterLevel* counterStat = nullptr;
};
// ============================================================================
#endif // CaloFutureHypoAlg_H
