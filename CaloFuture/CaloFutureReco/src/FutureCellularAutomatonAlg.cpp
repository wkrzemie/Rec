/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
#include "GaudiAlg/FunctionalUtilities.h"
// ============================================================================
// DetDesc
// ============================================================================
#include "DetDesc/IGeometryInfo.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/CaloDigit.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CellID.h"
// ============================================================================
// CaloFutureUtils
// ============================================================================
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// ============================================================================
// local
// ============================================================================
#include "FutureCellularAutomatonAlg.h"
// ============================================================================
/** @file
 *  Implementation file for class : FutureCellularAutomatonAlg
 *
 *  @date 2008-04-03
 *  @author Victor Egorychev
 */
// ============================================================================
// Declaration of the Algorithm Factory
// ============================================================================
DECLARE_COMPONENT( FutureCellularAutomatonAlg )
// ============================================================================
// Standard constructor, initializes variables
// ============================================================================
FutureCellularAutomatonAlg::FutureCellularAutomatonAlg( const std::string& name,
                                            ISvcLocator*       pSvcLocator )
: Transformer ( name, pSvcLocator,
                KeyValue{"InputData" , LHCb::CaloDigitLocation::Ecal },
                KeyValue{"OutputData", LHCb::CaloClusterLocation::EcalRaw } )
{
  // set default data as a function of detector
  m_detData = LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name ) ;

  updateHandleLocation(*this,"InputData" , LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation  ( name , context() ));
  updateHandleLocation(*this,"OutputData",
                       LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name , context(), "EcalRaw" ));
}

// ============================================================================
// Initialization
// ============================================================================
StatusCode FutureCellularAutomatonAlg::initialize()
{
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  /// Retrieve geometry of detector
  m_detector = getDet<DeCalorimeter>( m_detData );
  if( !m_detector ) { return StatusCode::FAILURE; }

  // Tool Interface
  m_tool      = tool<ICaloFutureClusterization>(m_toolName, this);

  return StatusCode::SUCCESS;
}
// ============================================================================
// Main execution
// ============================================================================
LHCb::CaloCluster::Container
FutureCellularAutomatonAlg::operator()(const LHCb::CaloDigits& digits) const   ///< Algorithm execution
{
  // Create the container of clusters
  LHCb::CaloCluster::Container output;
  // update the version number (needed for serialization)
  output.setVersion( 2 ) ;

  // create vector of pointers for CaloFutureCluster
  std::vector<LHCb::CaloCluster*> clusters;

  // clusterization tool which return the vector of pointers for CaloClusters
  unsigned int stepPass;
  if( m_neig_level> 0){
    std::vector<LHCb::CaloCellID> seeds;
    stepPass = m_tool->clusterize(clusters, digits, m_detector, seeds, m_neig_level) ;
  } else{
    stepPass = m_tool->clusterize(clusters, digits, m_detector) ;
  }

  // put to the container of clusters
  for ( const auto& clus : clusters ) { output.insert( clus ) ; }

  /** sort the sequence to simplify the comparison
   *  with other clusterisation techniques
   */
  if ( m_sort )  {
    if ( !m_sortByET ) {
      // sorting criteria: Energy
      // perform the sorting
      std::stable_sort    ( clusters.begin()            ,
                            clusters.end  ()            ,
                            LHCb::CaloDataFunctor::inverse( LHCb::CaloDataFunctor::Less_by_Energy ) ) ;
    } else {
      // sorting criteria : Transverse Energy
      LHCb::CaloDataFunctor::Less_by_TransverseEnergy<const DeCalorimeter*> Cmp ( m_detector ) ;
      // perform the sorting
      std::stable_sort   ( clusters.begin()            ,
                           clusters.end  ()            ,
                           LHCb::CaloDataFunctor::inverse( Cmp ) ) ;
    }
  }

  // statistics
  m_passes += stepPass;
  m_clusters += output.size();

  if (UNLIKELY( msgLevel( MSG::DEBUG) )){
    debug() << "Built " << clusters.size() <<" cellular automaton clusters  with "
            << stepPass << " iterations" <<endmsg;
    debug() << " ----------------------- Cluster List : " << endmsg;
    for(const auto& c : clusters ) {
      debug() << " Cluster seed " << c->seed()
              << " energy " << c->e()
              << " #entries " << c->entries().size()
              << endmsg;
    }
  }
  return output;
}
// ============================================================================
//  Finalize
// ============================================================================
StatusCode FutureCellularAutomatonAlg::finalize()
{
  info() << "Built <" << m_clusters.mean()
         <<"> cellular automaton clusters/event  with <"
         << m_passes.mean() << "> iterations (min,max)=(" << m_passes.min() << "," << m_passes.max() << ") on average " << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
// =============================================================================
// the END
// =============================================================================
