/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloDataFunctor.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloMomentum.h"

// local
#include "CaloFutureShowerOverlapTool.h"

#include <functional>
#include <utility>
namespace {
  template <typename... MapArgs, typename Predicate>
  void erase_if( std::map<MapArgs...>& map, Predicate&& predicate ) {
       auto i = map.begin();
       auto end = map.end(); // std::map::erase does not invalidate end -- so it can be cached
       while ( i != end ) {
           if (std::invoke(predicate,std::as_const(*i))) i = map.erase(i);
           else ++i;
       }
  }
}

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureShowerOverlapTool
//
// 2014-06-02 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureShowerOverlapTool )

//=============================================================================
// Initializer
//=============================================================================

StatusCode CaloFutureShowerOverlapTool::initialize() {
  StatusCode sc = GaudiTool::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( UNLIKELY(msgLevel(MSG::DEBUG)) ) debug() << "==> Initialize" << endmsg;
  m_det = getDet<DeCalorimeter> ( m_detLoc );

  return m_shape->setConditionParams(m_pcond, true);
}

//=============================================================================

void CaloFutureShowerOverlapTool::storeInitialWeights(const LHCb::CaloCluster& cl1,
                                                const LHCb::CaloCluster& cl2,
                                                std::map<LHCb::CaloCellID,double>& weights) const {
  for( const auto& i1 : cl1.entries() ) {
    weights[i1.digit()->cellID()]=i1.fraction();
    //info() << " part1 " << i1.digit()->cellID() << " : " << i1.fraction() << endmsg;
  }
  for( const auto& i2 : cl2.entries()) {
    LHCb::CaloCellID id2=i2.digit()->cellID();
    auto it = weights.find(id2);
    //info() << " part2 " << i2.digit()->cellID() << " : " << i2.fraction() << endmsg;
    if( it == weights.end() ) {
      weights[ id2 ]=i2.fraction();
    } else{
      it->second += i2.fraction();
    }
  }
  // check
  erase_if( weights, [](const auto& p) { return p.second==1.; });
}

double CaloFutureShowerOverlapTool::getInitialWeight(const LHCb::CaloCellID id,
                                               const std::map<LHCb::CaloCellID,double>& weights) const {
  if( weights.empty() ) return 1.;
  auto it = weights.find(id);
  return it != weights.end() ? it->second : 1. ;
}



void CaloFutureShowerOverlapTool::process(LHCb::CaloCluster& cl1, LHCb::CaloCluster& cl2, const int niter,
					  propagateInitialWeights propagateInitialWeights) const {

  if( cl1.entries().size() < m_minSize || cl2.entries().size() < m_minSize ){
    ++m_skippedSize;
    return;  // skip small clusters
  }

  int a1 = cl1.seed().area();
  int a2 = cl2.seed().area();

  std::map<LHCb::CaloCellID,double> ini_weights;
  if( propagateInitialWeights )storeInitialWeights(cl1, cl2, ini_weights);

  // 0 - evaluate parameters (applying photon hypo corrections for the position)
  evaluate(cl1);
  evaluate(cl2);

  if( cl1.e() <= 0. || cl2.e() <= 0){
    ++m_skippedEnergy;
    return;
  }

  if( msgLevel(MSG::VERBOSE) ){
    verbose() << " ======== Shower Overlap =======" << endmsg;
    verbose() << " CL1/CL2 : " << cl1.e() << " " << cl2.e() << " " << cl1.e()+cl2.e()<< endmsg;
    verbose() << " seed    : " << cl1.seed()  << " " << cl2.seed() << endmsg;
    verbose() << " area    : " << a1  << " " << a2 << endmsg;
    verbose() << " params  : " << cl1.position().parameters() << " / " <<   cl2.position().parameters() << endmsg;
  }

  int iter = 0;

  // 1 - determine the energy fractions of each entry
  while( iter < niter ){

    if( msgLevel(MSG::VERBOSE) ) verbose() << " ------ iter = " << iter << endmsg;

    subtract(cl1, cl2, propagateInitialWeights, ini_weights, a1, a2);

    if( msgLevel(MSG::VERBOSE) ) {
      verbose() << " >> CL1/CL2 : " << cl1.e() << " " << cl2.e() << "  " << cl1.e() + cl2.e() << endmsg;
      verbose() << " >> params  : " << cl1.position().parameters() << " / " <<   cl2.position().parameters() << endmsg;
      LHCb::CaloMomentum momentum;
      momentum.addCaloPosition(&cl1);
      momentum.addCaloPosition(&cl2);
      verbose() << " >> Mass : "  << momentum.mass() << endmsg;
    }

    iter++;
  }
  // 3 - reset cluster-like parameters
  evaluate(cl1,false);
  evaluate(cl2,false);

}


double CaloFutureShowerOverlapTool::fraction(const LHCb::CaloCluster& cluster,
                                       const LHCb::CaloDigit* digit,
                                       const int& a1, const int& a2,
                                       const int flag) const {

  if( digit == NULL)return 1.;
  const LHCb::CaloCellID cellID=digit->cellID();


  double size  = m_det->cellSize( cellID ) ;
  double xd    = m_det->cellX   ( cellID ) ;
  double yd    = m_det->cellY   ( cellID ) ;
  double xc = cluster.position().parameters()(  LHCb::CaloPosition::Index::X );
  double yc = cluster.position().parameters()(  LHCb::CaloPosition::Index::Y );
  double zc = cluster.position().z();
  double zd = (xc*xc + yc*yc + zc*zc - xc*xd - yc* yd )/zc;
  double d3d = std::sqrt( (xd - xc)*(xd - xc ) +
                          (yd - yc)*(yd - yc ) +
                          (zd - zc)*(zd - zc ) )/size;
  int area = (flag == 1) ? a1 : a2;
  double f = showerFraction( d3d, area);
  double ed = digit->e();
  double ec = f * cluster.position().parameters()( LHCb::CaloPosition::Index::E );
  double frac= (ed > ec ) ?  (ed - ec )/ ed : 0.;

  // info() << "        --> Digit : " << cellID << "  fraction : " << frac << endmsg;
  return frac;
}



void CaloFutureShowerOverlapTool::subtract(LHCb::CaloCluster& cl1, LHCb::CaloCluster& cl2,
                                     const propagateInitialWeights propagateInitialWeight,
                                     const std::map<LHCb::CaloCellID,double>& ini_weights,
                                     const int& a1, const int& a2) const {

  // cluster1  -> cluster2 spillover
  //info() << "     --- 1st cluster overlap ----- " << cl1->seed() <<  endmsg;
  for( auto& i2 : cl2.entries() ) {
    if( (LHCb::CaloDigitStatus::UseForEnergy & i2.status())==0 &&
        (LHCb::CaloDigitStatus::UseForPosition & i2.status())==0 )continue;
    double initialWeight = propagateInitialWeight ? getInitialWeight( i2.digit()->cellID(), ini_weights ) : 1.;
    i2.setFraction( fraction( cl1,  i2.digit(), a1, a2, 1)*initialWeight );
    //info() << "cl1 -> 2 : " << i2.digit()->cellID() << "  " << i2.digit()->e() << " " << i2.fraction() << endmsg;
  }

  // re-evaluate cluster2 accordingly
  evaluate(cl2);
  if( cl2.e() < 0)return; // skip negative energy "clusters"

  //info() << "     --- 2nd cluster overlap ----- " << cl2->seed() << endmsg;
  // cluster2  -> cluster1 spillover
  for( auto& i1 : cl1.entries() ) {
    const LHCb::CaloDigit* dig1 = i1.digit();
    if( (LHCb::CaloDigitStatus::UseForEnergy & i1.status()) ==0 &&
        (LHCb::CaloDigitStatus::UseForPosition & i1.status())==0 )continue;
    double initialWeight = propagateInitialWeight ? getInitialWeight( i1.digit()->cellID(), ini_weights ) : 1.;
    i1.setFraction( fraction( cl2, i1.digit(), a1, a2, 2)*initialWeight );
    //info() << "cl2 -> 1 : " << i1.digit()->cellID() << "  " << i1.digit()->e() << " " << i1.fraction() << endmsg;
    double eps = 1.e-4;
    // normalize the sum of partial weights in case of  shared cells
    for( auto& i2 : cl2.entries()) {
      const LHCb::CaloDigit* dig2 = i2.digit();
      if( !(dig2->cellID() == dig1->cellID()) )continue;
      if( (LHCb::CaloDigitStatus::UseForEnergy & i2.status())==0 &&
           (LHCb::CaloDigitStatus::UseForPosition & i2.status())==0 )continue;
      double f1 = i1.fraction();
      double f2 = i2.fraction();
      double sum = f1 + f2 ;
      if( fabs( sum - initialWeight) > eps ){
        if( sum < initialWeight && f2 == 0. )i2.setFraction( initialWeight - f1 );
        else if ( sum < initialWeight && f1  == 0.)i1.setFraction (initialWeight - f2);
        else{
          i1.setFraction( initialWeight*f1/(f1+f2) );
          i2.setFraction( initialWeight*f2/(f1+f2) );
        }
        //info() << "  -> SHARED " << f1 << " -> " << i1.fraction() << " | " << f2 << " -> " << i2.fraction() << " => " << i1.fraction()+i2.fraction() << endmsg;
      }
    }
  }

  // reevaluate  cluster1 & 2 accordingly
  evaluate(cl1);
  evaluate(cl2);

}

double CaloFutureShowerOverlapTool::showerFraction(const double d3d,
                                             const unsigned int area) const {
  LHCb::CaloCellID cellID(2,area,0,0); //fake cell
  double frac = m_shape->getCorrection(CaloFutureCorrection::profile, cellID , d3d ,0.) ;
  return std::max( 0., std::min( frac, 1. ) );
}

void CaloFutureShowerOverlapTool::evaluate(LHCb::CaloCluster& cluster,bool hypoCorrection) const {


  // 0 - reset z-position of cluster
  LHCb::ClusterFunctors::ZPosition zPosition( m_det );
  cluster.position().setZ( zPosition( &cluster )  );

  // 1 - 3x3 energy and energy-weighted barycenter
  double E, X, Y;
  StatusCode sc = LHCb::ClusterFunctors::calculateEXY( cluster.entries().begin() ,
                                                       cluster.entries().end  () ,
                                                       m_det , E , X , Y      );
  if( sc.isSuccess() ){
    cluster.position().parameters()( LHCb::CaloPosition::Index::E ) = E ;
    cluster.position().parameters()( LHCb::CaloPosition::Index::X ) = X ;
    cluster.position().parameters()( LHCb::CaloPosition::Index::Y ) = Y ;
  }
  else{
    if ( UNLIKELY(msgLevel(MSG::DEBUG)) ) debug() << " E,X and Y of cluster could not be evaluated " << endmsg;
    ++m_positionFailed;
  }


  if( cluster.e() < 0)return; // skip correction for negative energy "clusters"

  if( !hypoCorrection ) return; // do not apply 'photon' hypo correction

  // 2 - apply 'photon hypothesis' corrections

  // create a fake CaloHypo
  LHCb::CaloHypo hypo{};
  hypo.setHypothesis ( LHCb::CaloHypo::Hypothesis::Photon );
  hypo.addToClusters ( &cluster );
  hypo.setPosition   ( std::make_unique<LHCb::CaloPosition>(cluster.position()) );

  // Apply transversal corrections
  sc=m_stool->process(&hypo);
  if( sc.isSuccess() ){
    cluster.position().parameters()( LHCb::CaloPosition::Index::X ) = hypo.position()->parameters()( LHCb::CaloPosition::Index::X) ;
    cluster.position().parameters()( LHCb::CaloPosition::Index::Y ) = hypo.position()->parameters()( LHCb::CaloPosition::Index::Y) ;
  }else
    Error(" SCorrection could not be evaluated!",sc,1).ignore();

  // Apply longitudinal correction
  sc=m_ltool->process(&hypo);
  if( sc.isSuccess() ){
    cluster.position().setZ( hypo.position()->z() );
  }  else
    Error(" LCorrection could not be evaluated!",sc,1).ignore();

}


