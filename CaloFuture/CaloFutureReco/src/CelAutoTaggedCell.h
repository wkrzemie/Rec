/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALOFUTURECA_CELAUTOTAGGEDCEL_H
#define CALOFUTURECA_CELAUTOTAGGEDCEL_H 1 
// ============================================================================
// Include files
// ============================================================================
// STD & STL 
// ============================================================================
#include <vector>
// ============================================================================
// Kernel
// ============================================================================
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/CaloCellID.h"
// ============================================================================
/** @class CelAutoTaggedCell CelAutoTaggedCell.h
 *
 *  Object Tagged Cell 
 *
 *  @author  Nicole Brun 
 *  @date    27/02/2001
 */
// ============================================================================
class CelAutoTaggedCell final
{  
  // ==========================================================================  
public:
  // ==========================================================================  
  enum Tag
    {
      DefaultFlag ,
      Clustered   ,
      Edge        
    };
  
  enum FlagState
    {
      NotTagged ,
      Tagged    
    } ;
  // ==========================================================================
public:
  // ==========================================================================  
  // Constructor
  CelAutoTaggedCell (  )
  {
    m_seeds.reserve( 3 )   ;
  }
  // ==========================================================================  
  // Getters 
  const LHCb::CaloDigit*  digit () const { return m_digit              ; }
  const LHCb::CaloCellID& cellID() const { return digit() -> cellID () ; }
  double            e     () const { return digit() -> e      () ; }  
  bool isEdge() const{ return ( ( Tagged == m_status ) && ( Edge == m_tag ) ); }  
  bool isClustered() const{ return ( ( Tagged == m_status ) && ( Clustered == m_tag ) ); }  
  const LHCb::CaloCellID&   seedForClustered() const { return m_seeds[0]; }  
  const std::vector<LHCb::CaloCellID>& seeds() const { return m_seeds; }  
  size_t numberSeeds() const { return m_seeds.size(); }  
  bool isSeed() const{ return ( ( m_seeds.size() != 1 ) ? false : cellID() ==  m_seeds [0] ); }  
  bool isWithSeed ( const LHCb::CaloCellID& seed ){ return m_seeds.end() != std::find( m_seeds.begin() , m_seeds.end() , seed );}
  Tag tag( ) const{return m_tag;}
  FlagState status( ) const{return m_status;}
  // ==========================================================================
  // Setters
  void setIsSeed()
  {
    m_tag = Clustered;
    m_status = Tagged;
    m_seeds.push_back ( cellID() );
  }
  // ==========================================================================
  void setEdge      () { m_tag = Edge  ; } 
  void setClustered () { m_tag = Clustered ; }
  void setStatus    () { if ( ( Edge == m_tag ) || ( Clustered == m_tag ) ) 
  { m_status = Tagged; } }  
  void addSeed ( const LHCb::CaloCellID& seed ) { m_seeds.push_back ( seed ); }  
  // ==========================================================================  
  // operator
  CelAutoTaggedCell& operator=( const LHCb::CaloDigit* digit )
  {
    reset() ;
    m_digit    = digit ;    
    return *this ;
  }
  // ==========================================================================  
protected:
  // ==========================================================================  
  void reset()
  {
    m_seeds.clear()        ; 
    m_seeds.reserve( 3 )   ;
    m_tag    = DefaultFlag ;
    m_status = NotTagged   ;
    m_digit  = nullptr     ;
  }
  // ==========================================================================  
private:
  // ==========================================================================  
  Tag              m_tag      = DefaultFlag ;
  FlagState        m_status   = NotTagged;
  const LHCb::CaloDigit* m_digit = nullptr;
  // ==========================================================================
  // Ident.seed(s)  
  LHCb::CaloCellID::Vector m_seeds;
  // ==========================================================================
};
// ============================================================================
#endif // CALOFUTURECA_CELAUTOTAGGEDCELL_H
// ============================================================================
