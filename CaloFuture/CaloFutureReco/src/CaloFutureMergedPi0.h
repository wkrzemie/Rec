/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALOFUTURERECO_CALOFUTUREMERGEDPI0_H
#define CALOFUTURERECO_CALOFUTUREMERGEDPI0_H 1
// ============================================================================
// Include files
// ============================================================================
// from STL
// ============================================================================
#include <string>
#include <vector>
// ============================================================================
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloFutureInterfaces/ICaloFutureClusterTool.h"
#include "FutureSubClusterSelectorTool.h"
#include "CaloFutureInterfaces/ICaloFutureHypoTool.h"
#include "CaloFutureInterfaces/ICaloFutureShowerOverlapTool.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloCluster.h"
// ============================================================================

/** @class CaloFutureMergedPi0 CaloFutureMergedPi0.h
 *
 *  Merged pi0 reconstruction with iterativ Method
 *
 * NEW IMPLEMENTATION
 *
 *  @author Olivier Deschamps
 *  @date   05/05/2014
 */

class CaloFutureMergedPi0 : public GaudiAlgorithm{
public:

  CaloFutureMergedPi0( const std::string& name   , ISvcLocator*       svcloc );

  StatusCode initialize() override;
  StatusCode execute() override;
  StatusCode finalize() override;

private:
  bool isNeighbor(LHCb::CaloCellID id0, LHCb::CaloCellID id1);

  Gaudi::Property<std::string> m_clusters      {this, "InputData"    , LHCb::CaloClusterLocation::Ecal};
  Gaudi::Property<std::string> m_splitClusters {this, "SplitClusters", LHCb::CaloClusterLocation::EcalSplit};
  Gaudi::Property<std::string> m_mergedPi0s    {this, "MergedPi0s"   , LHCb::CaloHypoLocation::MergedPi0s};
  Gaudi::Property<std::string> m_splitPhotons  {this, "SplitPhotons" , LHCb::CaloHypoLocation::SplitPhotons};

  Gaudi::Property<float>  m_etCut {this, "EtCut", 1500 * Gaudi::Units::MeV};
  Gaudi::Property<int>    m_iter {this, "MaxIterations", 25};
  Gaudi::Property<bool>   m_createClusterOnly {this, "CreateSplitClustersOnly", false};

  // tools name
  Gaudi::Property<std::vector<std::string>> m_photonTools {this, "PhotonTools"};
  Gaudi::Property<std::vector<std::string>> m_pi0Tools    {this, "Pi0Tools"};

  // tools interfaces
  std::vector<ICaloFutureHypoTool*>  m_gTools ;
  std::vector<ICaloFutureHypoTool*>  m_pTools ;
  ICaloFutureShowerOverlapTool* m_oTool  = nullptr ;
  ICaloFutureClusterTool*       m_cov    = nullptr ;
  ICaloFutureClusterTool*       m_spread = nullptr ;
  FutureSubClusterSelectorTool* m_tagger = nullptr ;
  Gaudi::Property<std::vector<std::string>> m_taggerE {this, "EnergyTags"};
  Gaudi::Property<std::vector<std::string>> m_taggerP {this, "PositionTags"};
  Gaudi::Property<std::string> m_det {this, "Detector", DeCalorimeterLocation::Ecal};
  DeCalorimeter* m_detector = nullptr ;
  Gaudi::Property<bool> m_verbose {this, "Verbose", false};
  Gaudi::Property<float> m_minET {this, "SplitPhotonMinET", 0.};
  std::map<std::string,std::vector<double> > m_covParams;
  IFutureCounterLevel* counterStat = nullptr;
};
// ============================================================================
#endif // CALOFUTUREMERGEDPI0_H
