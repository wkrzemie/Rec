/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STL
// ============================================================================
#include <cmath>
#include <algorithm>
// ============================================================================
// from Gaudi
// ============================================================================
#include "GaudiKernel/SmartRef.h"
// ============================================================================
// local
// ============================================================================
#include "CaloFutureExtraDigits.h"
// ============================================================================
/** @file CaloFutureExtraDigits.cpp
 *
 *  Implementation file for class : CaloFutureExtraDigits
 *  @see CaloFutureExtraDigits
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 01/04/2002
 */
// ============================================================================
DECLARE_COMPONENT( CaloFutureExtraDigits )
// ============================================================================
CaloFutureExtraDigits::CaloFutureExtraDigits
( const std::string&  type   ,
  const std::string&  name   ,
  const IInterface*   parent )
  : GaudiTool            ( type, name , parent )
{
  //
  declareInterface<ICaloFutureHypoTool>     (this);
  //
  if ( std::string::npos != name.find ( "Prs"  ) )
  { m_toDet.value().push_back ( "Prs"  ) ; }
  if ( std::string::npos != name.find ( "Spd"  ) )
  { m_toDet.value().push_back ( "Spd"  ) ; }
  if ( std::string::npos != name.find ( "Hcal" ) )
  { m_toDet.value().push_back ( "Hcal" ) ; }
  if ( std::string::npos != name.find ( "Ecal" ) )
  { m_toDet.value().push_back ( "Ecal" ) ; }
}
// ============================================================================
StatusCode CaloFutureExtraDigits::initialize ()
{
  // initilaize the base class
  StatusCode sc = GaudiTool::initialize ();
  if( sc.isFailure() )
  { return Error ( "Could not initialize the base class GaudiTool!", sc ) ; }
  //
  if ( m_toDet.empty() )
  { return Error ( "List of 'ExtraDigitsFrom' is empty!" ) ; }
  //
  for ( std::vector<std::string>::iterator idet = m_toDet.begin();
        idet!=m_toDet.end();idet++)
  {
    m_toCaloFuture[*idet]=tool<ICaloFutureHypo2CaloFuture>("CaloFutureHypo2CaloFuture", "CaloFutureHypo2" + *idet , this );
    m_toCaloFuture[*idet]->setCalos(m_det,*idet);
  }
  //
  counterStat = tool<IFutureCounterLevel>("FutureCounterLevel");
  return StatusCode::SUCCESS ;
}
// ============================================================================
StatusCode CaloFutureExtraDigits::process    ( LHCb::CaloHypo* hypo  ) const
{ return (*this) ( hypo ); }
// ============================================================================
StatusCode CaloFutureExtraDigits::operator() ( LHCb::CaloHypo* hypo  ) const
{
  if ( 0 == hypo        ) { return Error("CaloHypo* points to NULL!" , StatusCode{200} ) ; }
  //
  for ( std::map<std::string,ICaloFutureHypo2CaloFuture*>::const_iterator
          idet = m_toCaloFuture.begin(); idet!=m_toCaloFuture.end();idet++)
  {
    ICaloFutureHypo2CaloFuture*    tool   = idet->second ;
    const std::string& toCaloFuture = idet->first  ;
    unsigned int count = 0 ;
    const std::vector<LHCb::CaloDigit*>& digits = tool->digits ( *hypo, toCaloFuture );
    for ( std::vector<LHCb::CaloDigit*>::const_iterator id = digits.begin() ;
          id != digits.end(); id++)
    {
      hypo->addToDigits( *id );
      ++count;
    }
    //
    if (UNLIKELY( msgLevel(MSG::DEBUG) ) )debug() << "Adding " << count << " digits from "<< *idet << endmsg;
    //
    if(counterStat->isVerbose() )counter ( toCaloFuture ) += count ;
  }
  return StatusCode::SUCCESS ;
}
