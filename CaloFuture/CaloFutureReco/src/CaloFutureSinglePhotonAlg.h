/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALOFUTUREALGS_CALOFUTURESINGLEPHOTONALG_H
#define CALOFUTUREALGS_CALOFUTURESINGLEPHOTONALG_H 1
// ============================================================================
#include <string>
#include "GaudiAlg/ScalarTransformer.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloHypo.h"
#include "Event/CaloCluster.h"
#include "GaudiKernel/Counters.h"
struct ICaloFutureClusterSelector;
struct ICaloFutureHypoTool;

/** @class CaloFutureSinglePhotonAlg CaloFutureSinglePhotonAlg.h
 *
 *  The simplest algorithm of reconstruction of
 *  single photon in electromagnetic calorimeter.
 *  The implementation is based on F.Machefert's codes.
 *
 *  @author Frederic Machefert machefer@in2p3.fr
 *  @author Vanya Belyaev      Ivan.Belyaev@itep.ru
 *  @date   31/03/2002
 */

class CaloFutureSinglePhotonAlg
  : public Gaudi::Functional::ScalarTransformer<CaloFutureSinglePhotonAlg, LHCb::CaloHypos(const LHCb::CaloClusters&)>
{
public:
  CaloFutureSinglePhotonAlg( const std::string&  name, ISvcLocator* pSvc );

  StatusCode initialize () override;
  StatusCode finalize   () override;

  using ScalarTransformer::operator();
  boost::optional<LHCb::CaloHypo> operator()(const LHCb::CaloCluster& ) const;
  void postprocess(const LHCb::CaloHypos&) const;

private:

  /// container of names
  typedef std::vector<std::string> Names;
  /// container of selector tools
  typedef std::vector<ICaloFutureClusterSelector*> Selectors;
  /// containers of hypo tools
  typedef std::vector<ICaloFutureHypoTool*> HypoTools;
  /// container of correction tools (S-,L-,...)
  typedef HypoTools Corrections;

  Selectors m_selectors;
  Gaudi::Property<Names> m_selectorsTypeNames {
    this, "SelectionTools", {
      "CaloFutureSelectCluster/PhotonCluster",
      "CaloFutureSelectNeutralClusterWithTracks/NeutralCluster"
    }, "List of tools for selection of clusters"};

  HypoTools m_hypotools;
  Gaudi::Property<Names> m_hypotoolsTypeNames {
    this, "HypoTools",
    { "CaloFutureExtraDigits/SpdPrsExtraG" },
    "List of generic Hypo-tools to apply to newly created hypos"};

  Corrections m_corrections;
  Gaudi::Property<Names> m_correctionsTypeNames {
    this, "CorrectionTools2", {
      "CaloFutureECorrection/ECorrection",
      "CaloFutureSCorrection/SCorrection",
      "CaloFutureLCorrection/LCorrection",
    }, "List of tools for 'fine-corrections' "};

  Gaudi::Property<std::string> m_detData
  {this, "Detector", LHCb::CaloFutureAlgUtils::DeCaloFutureLocation("Ecal")};

  Gaudi::Property<double> m_eTcut
  {this, "EtCut", 0., "Threshold on cluster & hypo ET"};

  LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> m_eT { nullptr } ; // TODO: can we combine this into Over_Et_Threshold and make it a property??

  const DeCalorimeter* m_det = nullptr;
  mutable Gaudi::Accumulators::StatCounter<> m_counterHypos{this, "hyposNumber"};
};
// ============================================================================
#endif // CALOFUTURESINGLEPHOTONALG_H
