/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Local
// ============================================================================
#include "CaloFutureSelectClusterWithSpd.h"
// ============================================================================
/** @class CaloFutureSelectNsutralClusterWithSpd
 *  Simple seleclton of newural clusters based on Spd information
 *  @author Olivier Deschamps
 *  @author Vanya BELYAEV
 */
// ============================================================================
class CaloFutureSelectNeutralClusterWithSpd : public CaloFutureSelectClusterWithSpd
{
public:
  // ==========================================================================
  bool select ( const LHCb::CaloCluster* cluster ) const override
  { return (*this) ( cluster ) ; }
  // ==========================================================================
  bool operator()( const LHCb::CaloCluster* cluster ) const override
  {
    if ( 0 == cluster )
    {
      Warning ( "CaloCluster* points to NULL, return false" );
      return false ;                                                  // RETURN
    }
    //

  bool sel = cut() >= n_hit ( *cluster ) ;
  if(counterStat->isVerbose())counter("selected clusters") += (int) sel;
  return sel;
  }
  // ==========================================================================
  /// constructor
  using CaloFutureSelectClusterWithSpd::CaloFutureSelectClusterWithSpd;
  // ==========================================================================
};
// ============================================================================
DECLARE_COMPONENT( CaloFutureSelectNeutralClusterWithSpd )
// ============================================================================
