/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/SystemOfUnits.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloCluster.h"
#include "CaloFutureSelectCluster.h"

DECLARE_COMPONENT( CaloFutureSelectCluster )

// ============================================================================
CaloFutureSelectCluster::CaloFutureSelectCluster
( const std::string& type, 
  const std::string& name,
  const IInterface*  parent)
  : GaudiTool (type ,name ,parent) 
{
  declareInterface<ICaloFutureClusterSelector> (this);
}
// ============================================================================

StatusCode CaloFutureSelectCluster::initialize (){  
  StatusCode sc = GaudiTool::initialize () ;
  return sc;
}

bool CaloFutureSelectCluster::select( const LHCb::CaloCluster* cluster ) const
{ 
  return (*this) (cluster); 
}

bool CaloFutureSelectCluster::operator()( const LHCb::CaloCluster* cluster) const 
{
  if ( 0 == cluster ) { Warning ( "CaloCluster* points to NULL!" ).ignore() ; return false ; }

  double e = cluster->e();
  LHCb::CaloMomentum moment = LHCb::CaloMomentum(cluster);
  double et = moment.momentum().Pt();
  int m = cluster->entries().size();

  if( UNLIKELY(msgLevel( MSG::DEBUG) ))debug() << "Cluster has " << m << " entries " 
                                      << " for a total energy of " << e <<  "(Et = " << et << ")" << endmsg;

  bool isSelected =  (e>m_cut) && (m < m_mult) && (et > m_etCut) && (m > m_multMin);
  if (isSelected ) { 
    ++m_counter;
  }
  return isSelected;
}
