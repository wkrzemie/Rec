/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CALOFUTUREDIGITSFILTERALG_H
#define CALOFUTURERECO_CALOFUTUREDIGITSFILTERALG_H 1
// Include files
// from STL
#include <string>
#include <vector>

// from GaudiAlg
#include "GaudiAlg/GaudiAlgorithm.h"


/** @class CaloFutureDigitsFilterAlg CaloFutureDigitsFilterAlg.h
 *
 *  Simple algorithm to perform filtering of CaloDigits
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   2002-06-11
 */
class CaloFutureDigitsFilterAlg :
  public GaudiAlgorithm
{
public:

  /** standard algorithm execution
   *  @see GaudiAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode execute() override;

  /** standard algorithm finalization
   *  @see GaudiAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode finalize() override;

  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

private:

  typedef std::vector<std::string> Addresses  ;
  typedef std::vector<int>         Statuses   ;

  Gaudi::Property<std::string> m_inputData {this, "InputData"};
  Gaudi::Property<Addresses>   m_hypos     {this, "Hypotheses"};
  Gaudi::Property<Addresses>   m_clusters  {this, "Clusters"};
  Gaudi::Property<Statuses>    m_statuses  {this, "Statuses", {
    LHCb::CaloDigitStatus::SeedCell,
    LHCb::CaloDigitStatus::LocalMaximum,
    LHCb::CaloDigitStatus::CentralCell,
  }};

};
// ============================================================================
#endif // CALOFUTURERECO_CALOFUTUREDIGITSFILTERALG_H
