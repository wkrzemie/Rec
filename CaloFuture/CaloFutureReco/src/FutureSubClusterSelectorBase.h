/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FutureSubClusterSelectorBase.h,v 1.5 2010-03-08 01:19:40 odescham Exp $

#ifndef CALOFUTURERECO_SUBCLUSTERSELECTORBASE_H
#define CALOFUTURERECO_SUBCLUSTERSELECTORBASE_H 1
// Include files
#include "CaloFutureInterfaces/ICaloFutureSubClusterTag.h"
#include "Event/CaloDigitStatus.h"
#include "GaudiAlg/GaudiTool.h"
class    CaloFutureCluster   ;


class FutureSubClusterSelectorBase : public virtual  ICaloFutureSubClusterTag , public GaudiTool{

public:

  static const LHCb::CaloDigitStatus::Status defaultStatus =
    LHCb::CaloDigitStatus::UseForEnergy |LHCb::CaloDigitStatus::UseForPosition | LHCb::CaloDigitStatus::UseForCovariance ;


  StatusCode initialize() override;
  StatusCode finalize() override;
  StatusCode process(LHCb::CaloCluster* cluster)const override;
  StatusCode operator()(LHCb::CaloCluster* cluster)const override;
  void setMask(const LHCb::CaloDigitStatus::Status mask)const override {
    m_mask=mask;
    if ( UNLIKELY(msgLevel(MSG::DEBUG)) )debug() << "The default status tag is changed to " << m_mask
                                                 << " -> use for Energy   : " << ((mask & LHCb::CaloDigitStatus::UseForEnergy) != 0)
                                                 << " | for Position : " << ((mask & LHCb::CaloDigitStatus::UseForPosition) != 0)
                                                 << " | for Covariance : " << ((mask & LHCb::CaloDigitStatus::UseForCovariance) != 0)
                                                 << endmsg;
  }
  unsigned int mask()const override {return m_mask;};



protected:


  /**  return  flag to modify the fractions
   *   @return flag to modify the fractions
   */
  inline bool modify() const { return m_modify ; }

  /** set new value for "modify" parameter
   *  @param value new value of modify parameter
   */
  inline void setModify( const bool value ) const { m_modify = value ; }

  inline const DeCalorimeter* det()  {return m_det; }


  protected:

  /** Standard Tool Constructor
   *  @param type type of the tool (useless ? )
   *  @param name name of the tool
   *  @param parent the tool parent
   */
  FutureSubClusterSelectorBase( const std::string& type   ,
                          const std::string& name   ,
                          const IInterface*  parent );

private:

  /// default constructor is private
  FutureSubClusterSelectorBase();
  /// copy    constructor is private
  FutureSubClusterSelectorBase( const FutureSubClusterSelectorBase& );
  /// assignement operator is private
  FutureSubClusterSelectorBase& operator=( const FutureSubClusterSelectorBase& );

private:
  mutable LHCb::CaloDigitStatus::Status m_mask;
  mutable Gaudi::Property<bool> m_modify {this, "ModifyFractions", false};
  Gaudi::Property<std::string> m_detData {this, "Detector", DeCalorimeterLocation::Ecal};
  const DeCalorimeter* m_det = nullptr;

};
// ============================================================================
#endif // SUBCLUSTERSELECTORBASE_H
