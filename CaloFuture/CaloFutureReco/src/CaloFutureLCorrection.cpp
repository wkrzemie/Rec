/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
#include "CaloFutureLCorrection.h"

/** @file
 *  Implementation file for class : CaloFutureLCorrection
 *
 *  @date 2003-03-10
 *  @author Xxxx XXXXX xxx@xxx.com
 */

DECLARE_COMPONENT( CaloFutureLCorrection )

// ============================================================================
/** Standard constructor
 *  @see GaudiTool
 *  @see  AlgTool
 *  @param type tool type (?)
 *  @param name tool name
 *  @param parent  tool parent
 */
// ============================================================================
CaloFutureLCorrection::CaloFutureLCorrection
( const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent )
  : CaloFutureCorrectionBase( type , name , parent )
{

  // define conditionName
  const std::string uName ( LHCb::CaloFutureAlgUtils::toUpper( name ) ) ;
  if ( uName.find( "ELECTRON" ) != std::string::npos  ) {
    m_conditionName = "Conditions/Reco/Calo/ElectronLCorrection";
  } else if ( uName.find( "MERGED" )  != std::string::npos   ||  uName.find( "SPLITPHOTON" )  != std::string::npos ) {
    m_conditionName = "Conditions/Reco/Calo/SplitPhotonLCorrection";
  } else if (  uName.find( "PHOTON" ) ) {
    m_conditionName = "Conditions/Reco/Calo/PhotonLCorrection";
  }

  /// interafces
  declareInterface<ICaloFutureHypoTool> ( this ) ;

  m_counterPerCaloFutureAreaDeltaZ.reserve(k_numOfCaloFutureAreas);
  m_counterPerCaloFutureAreaGamma.reserve(k_numOfCaloFutureAreas);
  m_counterPerCaloFutureAreaDelta.reserve(k_numOfCaloFutureAreas);
  std::vector<std::string> caloAreas = {"Outer" , "Middle" , "Inner" , "PinArea"};
  for (auto i = 0; i < k_numOfCaloFutureAreas; i++) {
    m_counterPerCaloFutureAreaDeltaZ.emplace_back(this, "Delta(Z) " + caloAreas[i]);
    m_counterPerCaloFutureAreaGamma.emplace_back(this, "<gamma> " + caloAreas[i]);
    m_counterPerCaloFutureAreaDelta.emplace_back(this, "<delta> " + caloAreas[i]);
  }
}

// ============================================================================
StatusCode CaloFutureLCorrection::finalize   ()
{
  m_hypos.clear();
  return CaloFutureCorrectionBase::finalize () ;
}
// ============================================================================
StatusCode CaloFutureLCorrection::initialize ()
{
  /// first initialize the base class
  StatusCode sc = CaloFutureCorrectionBase::initialize();
  if ( sc.isFailure() ) {
    return Error ( "Unable initialize the base class CaloFutureCorrectionBase !" , sc ) ;
  }

  if ( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "Condition name : " << m_conditionName << endmsg;

  return StatusCode::SUCCESS ;
}

// ============================================================================
StatusCode CaloFutureLCorrection::operator() ( LHCb::CaloHypo* hypo  ) const
{
  return process( hypo );
}

// ============================================================================
StatusCode CaloFutureLCorrection::process    ( LHCb::CaloHypo* hypo  ) const
{

  // check arguments
  if ( !hypo ) {
    return Warning ( " CaloHypo* points to NULL!" , StatusCode::SUCCESS) ;
  }

  // check the Hypo
  const auto h = std::find( m_hypos.begin() , m_hypos.end() , hypo->hypothesis() ) ;
  if ( m_hypos.end() == h )return Error ( "Invalid hypothesis -> no correction applied", StatusCode::SUCCESS ) ;

  // No correction for negative energy :
  if ( hypo->e() < 0.) {
    ++m_counterSkipNegativeEnergyCorrection;
    return StatusCode::SUCCESS;
  }


  // get Prs/Spd
  double ePrs = 0 ;
  double eSpd = 0 ;
  getPrsSpd(hypo, ePrs, eSpd);


  // get cluster energy (special case for SplitPhotons)
  const LHCb::CaloCluster* GlobalCluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo(hypo, false);
  const LHCb::CaloCluster* MainCluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo(hypo, true) ;

  /*
    Cluster information (e/x/y  and Prs/Spd digit)
  */
  if ( !MainCluster ) {
    return Error ( "CaloCLuster* points to NULL!" ) ;
  }

  // For Split Photon - share the Prs energy
  if (  LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 == hypo->hypothesis() ) {
    ePrs *= MainCluster->position().e() / GlobalCluster->position().e() ;
  }


  const double energy = hypo->e () ;

  const auto& entries = MainCluster->entries();
  const auto iseed =
    LHCb::ClusterFunctors::locateDigit ( entries.begin () , entries.end   () , LHCb::CaloDigitStatus::SeedCell );
  if ( entries.end() == iseed ) {
    return Error ( "The seed cell is not found !" ) ;
  }

  // get the "area" of the cluster (where seed is)
  const LHCb::CaloDigit*   seed    = iseed->digit();
  if ( !seed ) {
    return Error ( "Seed digit points to NULL!" ) ;
  }

  // Cell ID for seed digit
  const auto cellID = seed->cellID() ;


  /** here all information is available
   *
   *  (1) Ecal energy in 3x3     :    energy
   *  (2) Prs and Spd energies   :    ePrs, eSpd
   *  (3) weighted barycenter    :    xBar, yBar
   *  (4) Zone/Area in Ecal      :    area
   *  (5) SEED digit             :    seed
   *  (6) CellID of seed digit   :    cellID
   *  (7) Position of seed cell  :    seedPos
   */


  // Account for the tilt
  const auto plane = m_det->plane(CaloPlane::Front); // Ecal Front-Plane
  const LHCb::CaloPosition* pos = hypo->position() ;

  const auto xg = pos->x() - m_origin.X();
  const auto yg = pos->y() - m_origin.Y();
  const auto normal = plane.Normal();
  const auto Hesse = plane.HesseDistance();
  const auto z0 = (-Hesse - normal.X() * pos->x() - normal.Y() * pos->y()) / normal.Z();
  auto zg = z0 - m_origin.Z();

  // hardcoded inner offset (+7.5 mm)
  if ( cellID.area() == 2 ) {
    zg += 7.5;
  }

  // Uncorrected angle
  const auto aaa = std::pow(xg, 2) + std::pow(yg, 2);
  const auto tth = std::sqrt(aaa) / zg ;
  //double cth = mycos ( myatan2( std::sqrt(aaa) , zg ) ) ;
  auto cth = zg / std::sqrt( aaa + std::pow(zg, 2) );

  // Corrected angle

  const auto gamma0 = getCorrection(CaloFutureCorrection::gamma0, cellID);
  const auto delta0 = getCorrection(CaloFutureCorrection::delta0, cellID);
  const auto gammaP = getCorrection(CaloFutureCorrection::gammaP, cellID, ePrs);
  const auto deltaP = getCorrection(CaloFutureCorrection::deltaP, cellID, ePrs);
  const auto g = gamma0 - gammaP;
  const auto d = delta0 + deltaP;

  //double alpha = Par_Al1[zon] - myexp( Par_Al2[zon] - Par_Al3[zon] * ePrs/Gaudi::Units::MeV );
  //double beta  = Par_Be1[zon] + myexp( Par_Be2[zon] - Par_Be3[zon] * ePrs/Gaudi::Units::MeV  );

  /// assert(cellID.area()>=0 &&cellID.area()<4);
  m_counterPerCaloFutureAreaGamma.at(cellID.area()) += g;
  m_counterPerCaloFutureAreaDelta.at(cellID.area()) += d;

  const auto tgfps = ( energy > 0.0 ? g * mylog(energy / Gaudi::Units::GeV) + d : 0.0 );

  const auto bbb = ( 1. + tgfps * cth / zg );
  //cth = mycos( myatan2( tth , bbb ) ) ;
  cth = bbb / std::sqrt( std::pow(tth, 2) + std::pow(bbb, 2) );

  const auto dzfps = cth * tgfps ;
  /// assert(cellID.area()>=0 &&cellID.area()<4);
  m_counterPerCaloFutureAreaDeltaZ.at(cellID.area()) += dzfps;
  m_counterDeltaZ += dzfps;

  /*
    info() << " ======= Z0  FRONT-PLANE = " << z0 << " " << zg << endmsg;
    ROOT::Math::Plane3D planeSM = m_det->plane(CaloPlane::ShowerMax); // Ecal Front-Plane
    info() << " ======= Z0  SHOWERMAX = " << -planeSM.HesseDistance() <<endmsg;
    ROOT::Math::Plane3D planeM = m_det->plane(CaloPlane::Middle); // Ecal Middle
    info() << " ======= Z0  MIDDLE = " << -planeM.HesseDistance() <<endmsg;
  */


  // Recompute Z position and fill CaloFuturePosition
  const auto zCor = z0 + dzfps;

  if ( UNLIKELY( msgLevel(MSG::DEBUG) ) ) {
    debug() << "Hypothesis :" << hypo->hypothesis() << endmsg;
    debug() << " ENE  " << hypo->position ()->e() <<  " "
            << "xg "   << xg <<  " " << "yg "   << yg <<  endmsg;
    debug() << "zg "   << pos->z() << " "
            << "z0 "   << z0 <<  " "
            << "DeltaZ "   << dzfps <<  " "
            << "zCor "   << zCor
            << endmsg ;
  }

  hypo -> position() -> setZ( zCor ) ;

  return StatusCode::SUCCESS ;
}

// ============================================================================
