/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CaloFutureClusterCorrect3x3Position_H
#define CALOFUTURERECO_CaloFutureClusterCorrect3x3Position_H 1
// Include files
// from STL
#include <string>
#include <vector>
//
#include "GaudiAlg/GaudiAlgorithm.h"
//CaloFutureUtils
#include "CaloFutureUtils/CellMatrix3x3.h"
#include "CaloFutureUtils/CellNeighbour.h"
// forward declarations
class    CaloCluster   ;
struct  ICaloFutureHypoTool  ;

/** @class CaloFutureClusterCorrect3x3Position CaloFutureClusterCorrect3x3Position.h
 *
 *  Merged pi0 reconstruction with Iterativ Method
 *
 *  @author Olivier Deschamps
 *  @date   05/10/2002
 */

class CaloFutureClusterCorrect3x3Position : public GaudiAlgorithm
{
public:
  /** Standard constructor
   *  @param   name   algorithm name
   *  @param   svcloc pointer to service locator
   */
  CaloFutureClusterCorrect3x3Position( const std::string& name   ,
                                 ISvcLocator*       svcloc );

  /** standard algorithm execution
   *  @return status code
   */
  StatusCode execute   () override;

protected:

  /** helper function to calculate number of digits
   *  in the cluster with given status
   *  @param cluster pointet to the cluster object
   *  @param status  digit statsu to be checked
   *  @return number of digits with given status.
   *       In the case of errors it returns -1
   */
  long numberOfDigits ( const LHCb::CaloCluster*             cluster ,
                        const LHCb::CaloDigitStatus::Status& status  ) const ;

 private:
  CellMatrix3x3 m_cell3x3 ;
  CellNeighbour m_neighbour;
  Gaudi::Property<std::string> m_inputData {this, "InputData", LHCb::CaloClusterLocation::Ecal};
  Gaudi::Property<std::string> m_detData {this, "Detector", DeCalorimeterLocation::Ecal};
};

// ============================================================================
#endif // CaloFutureClusterCorrect3x3Position_H
