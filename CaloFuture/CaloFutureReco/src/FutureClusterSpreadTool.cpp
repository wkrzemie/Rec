/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "FutureClusterSpreadTool.h"
// ============================================================================
/** @file FutureClusterSpreadTool.cpp
 *
 *  Implementation file for class : FutureClusterSpreadTool
 * 
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru 
 *  @date 23/11/2001
 */
// ============================================================================
DECLARE_COMPONENT( FutureClusterSpreadTool )
// ============================================================================
/*  Standard constructor
 *  @param type tool type (useless)
 *  @param name tool name
 *  @param parent pointer to parent object (service, algorithm or tool)  
 */
// ============================================================================
FutureClusterSpreadTool::FutureClusterSpreadTool
( const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent )
  : GaudiTool    ( type , name , parent )
{
  // setup calo-dependent property
  m_detData    = LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name ) ;

  /// declare available interafces 
  declareInterface<ICaloFutureClusterTool>(this);
}
// ============================================================================
/*  standard initialization method 
 *  @return status code 
 */
// ============================================================================
StatusCode FutureClusterSpreadTool::initialize ()
{
  /// initialize the base class 
  StatusCode sc = GaudiTool::initialize();
  if( sc.isFailure() ) 
    { return Error("Could not initialize the base class ",sc);}
  ///  
  m_det = getDet<DeCalorimeter>( m_detData) ;
  /// configure the estimator 
  m_estimator.setDetector( m_det ) ;
  ///
  return StatusCode::SUCCESS;
}
// ============================================================================
/*  standard finalization method 
 *  @return status code 
 */
// ============================================================================
StatusCode FutureClusterSpreadTool::finalize   ()
{  
  if ( UNLIKELY(msgLevel ( MSG::DEBUG ) ) )
  {
    debug () << " Corrected Clusters, Ratio : " 
             << m_estimator.invalidRatio  () << endmsg ;
    debug () << " Corrected Clusters, Et    : " 
             << m_estimator.invalidEnergy () << endmsg ;
    debug () << " Corrected Clusters, Cells : " 
             << m_estimator.invalidCells  () << endmsg ;
  }
  /// finalize the base class
  return GaudiTool::finalize ();
}
// ============================================================================
/*  The main processing method 
 *  @param cluster pointer to CaloCluster object to be processed
 *  @return status code 
 */  
// ============================================================================
StatusCode FutureClusterSpreadTool::process    
( LHCb::CaloCluster* cluster ) const { return (*this)( cluster ); }
// ============================================================================
/*  The main processing method (functor interface) 
 *  @param cluster pointer to CaloCluster object to be processed
 *  @return status code 
 */  
// ============================================================================
StatusCode FutureClusterSpreadTool::operator() 
  ( LHCb::CaloCluster* cluster ) const
{
  /// check the argument 
  if( 0 == cluster                ) 
    { return Error( "CaloCluster*   points to NULL!") ; }
  if( 0 == m_estimator.detector() ) 
    { return Error( "DeCalorimeter* points to NULL!") ; }
  /// apply the estimator 
  return m_estimator( cluster );
}
// ============================================================================
// The END 
// ============================================================================

  
