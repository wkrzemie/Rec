/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CALOFUTUREEXTRADIGITS_H
#define CALOFUTURERECO_CALOFUTUREEXTRADIGITS_H 1
// Include files
// from STL
#include <string>
// Kernel
#include "GaudiAlg/GaudiTool.h"
#include "CaloFutureInterfaces/ICaloFutureHypoTool.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"

/** @class CaloFutureExtraDigits CaloFutureExtraDigits.h
 *
 *
 *  @author Vanya Belyaev Ivan Belyaev
 *  @date   31/03/2002
 */
class CaloFutureExtraDigits :
  public virtual     ICaloFutureHypoTool ,
  public                  GaudiTool
{
public:

  StatusCode initialize() override;
  StatusCode process    ( LHCb::CaloHypo* hypo  ) const override;
  StatusCode operator() ( LHCb::CaloHypo* hypo  ) const override;

  CaloFutureExtraDigits( const std::string& type,
                   const std::string& name,
                   const IInterface* parent);

private:

  Gaudi::Property<std::vector<std::string>> m_toDet {this, "ExtraDigitFrom"};
  std::map<std::string,ICaloFutureHypo2CaloFuture*> m_toCaloFuture;
  Gaudi::Property<std::string> m_det {this, "Detector", "Ecal"};
  IFutureCounterLevel* counterStat = nullptr;
};
#endif // CALOFUTUREEXTRADIGITS_H
