/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURECLUSTERIZATIONTOOL_H
#define CALOFUTURECLUSRERIZATIONTOOL_H 1
// ============================================================================
#include <string>
#include "GaudiAlg/GaudiTool.h"
#include "CaloKernel/CaloVector.h"
#include "CaloFutureInterfaces/ICaloFutureClusterization.h"
#include "GaudiKernel/Counters.h"
#include "CaloDet/DeCalorimeter.h"
#include "CelAutoTaggedCell.h"
#include "CaloFutureUtils/CellSelector.h"

/** @class CaloFutureClusterizationTool CaloFutureClusterizationTool.h
 *
 *
 *  @author Victor Egorychev
 *  @date   2008-04-03
 */
class CaloFutureClusterizationTool : public extends<GaudiTool,ICaloFutureClusterization> {

public:
  /// container to tagged  cells with direct (by CaloCellID key)  access
  typedef CaloVector<CelAutoTaggedCell*>  DirVector ;

  /// Extend ICaloFutureClusterization
  using extends<GaudiTool,ICaloFutureClusterization>::extends;

  unsigned int clusterize
  ( std::vector<LHCb::CaloCluster*>&      clusters   ,
    const LHCb::CaloDigits&               hits       ,
    const DeCalorimeter*                  detector   ,
    const std::vector<LHCb::CaloCellID>&  seeds      ,
    const unsigned int                    level      ) const override;

private:

  inline bool isLocMax
  ( const LHCb::CaloDigit*     digit ,
    const DirVector&     hits  ,
    const DeCalorimeter* det ) const ;

  inline void appliRulesTagger
  ( CelAutoTaggedCell*   taggedCell,
    DirVector&           taggedCellsDirect,
    const DeCalorimeter* detector,
    const bool           releaseBool) const ;

  inline StatusCode setEXYCluster( LHCb::CaloCluster&         cluster,const DeCalorimeter* detector ) const ;

  Gaudi::Property<bool> m_withET{this, "withET", false};
  Gaudi::Property<double> m_ETcut{this, "ETcut", -10. * Gaudi::Units::GeV};
  Gaudi::Property<std::string> m_usedE{this, "CellSelectorForEnergy", "3x3"};
  Gaudi::Property<std::string> m_usedP{this, "CellSelectorForPosition", "3x3"};
  Gaudi::Property<unsigned int> m_passMax{this, "MaxIteration", 10};

  mutable Gaudi::Accumulators::StatCounter<> m_clusterEnergy{this, "Cluster energy"};
  mutable Gaudi::Accumulators::StatCounter<> m_negativeSeed{this, "Negative seed energy"};
  mutable Gaudi::Accumulators::StatCounter<> m_clusterSize{this, "Cluster size"};
  };
#endif // CALOFUTURECLUSTERIZATIONTOOL_H
