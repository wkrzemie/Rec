/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//  ===========================================================================
#define CALOFUTURERECO_CALOFUTURECLUSTERCOVARIANCEALG_CPP 1 
/// ===========================================================================
// Include files
#include  "Event/CaloCluster.h"
#include  "CaloFutureInterfaces/ICaloFutureClusterTool.h"
#include  "FutureSubClusterSelectorTool.h"
#include  "CaloFutureUtils/CovarianceEstimator.h"
#include  "CaloFutureUtils/CaloFutureAlgUtils.h"
#include  "CaloFutureClusterCovarianceAlg.h"

// ===========================================================================
/** @file
 * 
 *  Implementation file for class CaloFutureClusterCovarianceAlg
 * 
 *  @see CaloFutureClusterCovarinanceAlg
 *  @see ICaloFutureClusterTool
 *  @see ICaloFutureSubClusterTag 
 * 
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 04/07/2001 
s */
// ===========================================================================

DECLARE_COMPONENT( CaloFutureClusterCovarianceAlg )

// ============================================================================
/** Standard constructor
 *  @param   name          algorithm name
 *  @param   pSVcLocator   pointer to Service Locator
*/
// ============================================================================
CaloFutureClusterCovarianceAlg::CaloFutureClusterCovarianceAlg
( const std::string& name,
  ISvcLocator* pSvcLocator)
  : GaudiAlgorithm        ( name , pSvcLocator            ) 
{
  // following properties might be inherited by the covariance tool
  declareProperty( "CovarianceParameters" , m_covParams    ) ; // KEEP IT UNSET ! INITIAL VALUE WOULD BYPASS DB ACCESS

  // set default data as a function of detector
  m_inputData = LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name , context() );

  std::string caloName =  LHCb::CaloFutureAlgUtils::CaloFutureNameFromAlg( name );
  m_covName    = caloName + "CovarTool" ;
  m_spreadName = caloName + "SpreadTool";
  m_tagName    = caloName + "ClusterTag";
}

// ===========================================================================
/** standard initialization method 
 *  @see GaudiAlgorithm
 *  @see     Algorithm 
 *  @see    IAlgorithm
 *  @return status code 
 */
// ===========================================================================
StatusCode CaloFutureClusterCovarianceAlg::initialize(){
  // try to initialize base class   
  StatusCode sc = GaudiAlgorithm::initialize();
  if( sc.isFailure() )
    { return Error("Could not initialize the base class GaudiAlgorithm"); }  

  // locate the tagger (inherit from relevant properties)
  m_tagger = tool<FutureSubClusterSelectorTool>( "FutureSubClusterSelectorTool" , m_tagName , this );

  // locate the tool for covariance matrix calculations 
  m_cov    = m_covName.empty() ?
    tool<ICaloFutureClusterTool>(    m_covType                   , this ) :
    tool<ICaloFutureClusterTool>(    m_covType    , m_covName    , this ) ;
  
  // locate the tool for cluster spread(2nd moments) estimation 
  m_spread = m_spreadName.empty() ? 
    tool<ICaloFutureClusterTool>(    m_spreadType                , this ) :
    tool<ICaloFutureClusterTool>(    m_spreadType , m_spreadName , this ) ;
  
  // copy flag
  m_copy = (!m_outputData.empty()) && (m_outputData.value() != m_inputData.value());

  counterStat = tool<IFutureCounterLevel>("FutureCounterLevel");
  return StatusCode::SUCCESS;
}



// ===========================================================================
StatusCode CaloFutureClusterCovarianceAlg::finalize(){
  // finalize the base class 
  return GaudiAlgorithm::finalize();
}

// ===========================================================================
StatusCode CaloFutureClusterCovarianceAlg::execute(){
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Execute" << endmsg;
  
  // useful typedefs
  typedef LHCb::CaloClusters        Clusters ;
  
  // locate input data
  Clusters* clusters =    get<Clusters> ( m_inputData );
  if( 0 == clusters ) { return StatusCode::FAILURE ; }
  //
  
  // define the output data 
  Clusters* output = 0;
  if( m_copy ){
    // make a copy of container 
    output = new Clusters();
    put( output , m_outputData );
    // make a copy 
    for( const LHCb::CaloCluster* i: *clusters ) {
      if ( i ) output->insert( new LHCb::CaloCluster(*i) ) ;
    }
  }
  else { output = clusters; } // update existing sequence
  
  // 
  for( Clusters::iterator cluster =  output->begin() ; output->end() != cluster ; ++cluster ){

    // skip nulls 
    if( 0 == *cluster  ) { continue ; }

    
    // == APPLY TAGGER
    StatusCode sc = m_tagger -> tag ( *cluster );
    if( sc.isFailure() ){
      Error("Error from tagger, skip cluster ", sc ).ignore() ; 
      if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << *cluster << endmsg ;
      continue ; 
    }

    // == APPLY COVARIANCE ESTIMATOR
    sc = cov() -> process( *cluster ) ;    
    if( sc.isFailure() ){ 
      Error("Error from cov,    skip cluster ", sc, 0 ).ignore() ; 
      if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << *cluster << endmsg ;
      continue ; 
    }

    // == APPLY SPREAD ESTIMATOR
    sc = spread() -> process( *cluster ) ;
    if( sc.isFailure() ){ 
      Error("Error from spread, skip cluster ", sc, 0 ).ignore() ; 
      if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << *cluster << endmsg ;
      continue ; 
    }
  }
  
  // == FUTURECOUNTER
  if(counterStat->isQuiet())counter ( "#Clusters from '" + m_inputData  + "'") += clusters->size() ;
  return StatusCode::SUCCESS ;
}

