/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
// ============================================================================
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "Event/CellID.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "CaloFutureInterfaces/ICaloFutureClusterSelector.h"
#include "CaloFutureInterfaces/ICaloFutureHypoTool.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureElectronAlg.h"
// ============================================================================
/** @file
 *
 *  Implementation file for class : CaloFutureElectronAlg
 *  The implementation is based on F.Machefert's codes.
 *  @see CaloFutureElectronAlg
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 31/03/2002
 */
// ============================================================================
namespace {
    template <typename Container, typename Arg, typename Parent>
    bool apply(const Container& c, Arg& arg, const char* errmsg, const Parent& parent) {
        bool r = std::all_of( std::begin(c), std::end(c),
                            [&](typename Container::const_reference elem)
                            { return (*elem)(&arg).isSuccess(); } );
        if (UNLIKELY(!r)) parent.Error( errmsg, StatusCode::FAILURE ).ignore();
        return r;
    }
}
DECLARE_COMPONENT( CaloFutureElectronAlg )
// ============================================================================
/*  Standard constructor
 *  @param name algorithm name
 *  @param pSvc service locator
 */
// ============================================================================
CaloFutureElectronAlg::CaloFutureElectronAlg
( const std::string& name ,
  ISvcLocator*       pSvcLocator )
  : ScalarTransformer( name, pSvcLocator,
                       KeyValue("InputData",{}), // note: context can not be used here -- only _after_ the baseclass is initialized!
                       KeyValue("OutputData",{}) )

{
  updateHandleLocation( *this, "InputData", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation("Ecal", context() ));
  updateHandleLocation( *this, "OutputData", LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation("Electrons", context() ));


  setProperty ( "PropertiesPrint" , true ) ;
}
// ============================================================================
/*   standard Algorithm initialization
 *   @return status code
 */
// ============================================================================
StatusCode CaloFutureElectronAlg::initialize()
{
  // initialize  the base class
  StatusCode sc = ScalarTransformer::initialize();
  if( sc.isFailure() ){ return Error("Could not initialize the base class GaudiAlgorithm!",sc);}

  // check the geometry information
  m_det = getDet<DeCalorimeter>( m_detData ) ;
  if( !m_det ) { return Error("Detector information is not available!");}

  m_eT = LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*>{ m_det } ; // TODO: can we combine this into Over_Et_Threshold and make it a property??

  // locate selector tools
  std::transform(m_selectorsTypeNames.begin(), m_selectorsTypeNames.end(),
                 std::back_inserter( m_selectors ),
                 [&](const std::string& name)
                 { return this->tool<ICaloFutureClusterSelector>( name , this ); } );
  if ( m_selectors.empty() )info()<<  "No Cluster Selection tools are specified!" <<endmsg ;
  // locate correction tools
  std::transform( m_correctionsTypeNames.begin(), m_correctionsTypeNames.end(),
                  std::back_inserter(m_corrections),
                  [&](const std::string& name)
                  { return this->tool<ICaloFutureHypoTool>( name , this ); } );
  if ( m_corrections.empty() )info() << "No Hypo Correction(1) tools are specified!" << endmsg ;
  // locate other hypo  tools
  std::transform( m_hypotoolsTypeNames.begin(), m_hypotoolsTypeNames.end(),
                  std::back_inserter(m_hypotools),
                  [&](const std::string& name)
                  { return this->tool<ICaloFutureHypoTool>( name , this ); } );
  if ( m_hypotools.empty() )info() << "No Hypo Processing(1) tools are specified!"  << endmsg ;
  // locate correction tools
  std::transform( m_correctionsTypeNames2.begin(), m_correctionsTypeNames2.end() ,
                  std::back_inserter(m_corrections2),
                  [&](const std::string& name)
                  { return this->tool<ICaloFutureHypoTool>( name , this ); } );
  if ( m_corrections2.empty() ) info() << "No Hypo Correction(2) tools are specified!"  << endmsg ;
  // locate other hypo  tools
  std::transform( m_hypotoolsTypeNames2.begin(), m_hypotoolsTypeNames2.end(),
                  std::back_inserter(m_hypotools2),
                  [&](const std::string& name)
                  { return this->tool<ICaloFutureHypoTool>( name , this ); } );
  if ( m_hypotools2.empty() )info() << "No Hypo    Processing(2) tools are specified!" << endmsg  ;
  ///
  counterStat = tool<IFutureCounterLevel>("FutureCounterLevel");
  return StatusCode::SUCCESS;
}
// ============================================================================
/*   standard Algorithm finalization
 *   @return status code
 */
// ============================================================================
StatusCode CaloFutureElectronAlg::finalize()
{
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug()<<"Finalize"<<endmsg;

  // clear containers
  m_selectors             .clear () ;
  m_corrections           .clear () ;
  m_hypotools             .clear () ;
  m_corrections2          .clear () ;
  m_hypotools2            .clear () ;
  m_selectorsTypeNames    .clear () ;
  m_correctionsTypeNames  .clear () ;
  m_hypotoolsTypeNames    .clear () ;
  m_correctionsTypeNames2 .clear () ;
  m_hypotoolsTypeNames2   .clear () ;
  // finalize the base class
  return ScalarTransformer::finalize() ;
}
// ============================================================================
/*   standard Algorithm execution
 *   @return status code
 */
// ============================================================================
boost::optional<LHCb::CaloHypo>
CaloFutureElectronAlg::operator()(const LHCb::CaloCluster& cluster) const{

  // loop over all selectors
  bool select = ( m_eT(&cluster) >= m_eTcut &&
                  std::all_of( m_selectors.begin(), m_selectors.end(),
                               [&](Selectors::const_reference sel)
                               { return (*sel)(&cluster); } ) );
  if ( !select ) return boost::none;

  // create "Hypo"/"Electron" object
  LHCb::CaloHypo hypo;
  hypo.setHypothesis( LHCb::CaloHypo::Hypothesis::EmCharged );
  hypo.addToClusters( &cluster );
  hypo.setPosition( std::make_unique<LHCb::CaloPosition>(cluster.position()) );

  //    return boost::none;
  return boost::make_optional(
			      apply( m_corrections, hypo,"Error from Correction Tool, skip the cluster " , *this ) // loop over all corrections and apply corrections
			      && apply( m_hypotools, hypo, "Error from Other Hypo Tool, skip the cluster " , *this) // loop over other hypo tools (e.g. add extra digits)
			      && apply( m_corrections2, hypo, "Error from Correction Tool 2 , skip the cluster " , *this) // loop over all corrections and apply corrections
			      && apply( m_hypotools2, hypo, "Error from Other Hypo Tool 2 , skip the cluster ", *this) // loop over other hypo tools (e.g. add extra digits)
			      && LHCb::CaloMomentum(&hypo).pt() >= m_eTcut,
			      hypo);

}

void
CaloFutureElectronAlg::postprocess(const LHCb::CaloHypos& hypos) const
{
  if(msgLevel(MSG::DEBUG))debug() << " # of created Electron  Hypos is  " << hypos.size()  << endmsg; // << "/" << clusters->size()<< endmsg ;
  if(counterStat->isQuiet()){
      std::string inputData; getProperty("InputData",inputData).ignore();
      counter ( inputData + "=>" + outputLocation()  ) += hypos.size() ;
  }
}

