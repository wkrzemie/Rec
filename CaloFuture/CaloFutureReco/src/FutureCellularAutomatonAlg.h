/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CELLULARAUTOMATONALG_H
#define CELLULARAUTOMATONALG_H 1

// Include files
// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"

#include "CaloFutureInterfaces/ICaloFutureClusterization.h"

/** @class FutureCellularAutomatonAlg FutureCellularAutomatonAlg.h
 *
 *
 *  @author Victor Egorychev
 *  @date   2008-04-03
 */
class FutureCellularAutomatonAlg
: public Gaudi::Functional::Transformer<LHCb::CaloCluster::Container(const LHCb::CaloDigits&)>
{

public:
  /// Standard constructor
  FutureCellularAutomatonAlg( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode finalize  () override;    ///< Algorithm finalization

  LHCb::CaloCluster::Container operator()(const LHCb::CaloDigits&) const override;    ///< Algorithm execution

private:
  Gaudi::Property<std::string> m_detData {this, "Detector"  , DeCalorimeterLocation::Ecal};
  const DeCalorimeter* m_detector = nullptr;

  Gaudi::Property<bool> m_sort     {this, "Sort"    , true};
  Gaudi::Property<bool> m_sortByET {this, "SortByET", false};

  Gaudi::Property<std::string> m_toolName {this, "Tool", "CaloFutureClusterizationTool"};
  ICaloFutureClusterization* m_tool = nullptr;

  Gaudi::Property<unsigned int> m_neig_level {this, "Level", 0};

  mutable Gaudi::Accumulators::StatCounter<> m_clusters{this, "# clusters"};
  mutable Gaudi::Accumulators::StatCounter<> m_passes{this, "# clusterization passes"};
};
#endif // CELLULARAUTOMATONALG_H
