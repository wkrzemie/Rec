/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL 
// ============================================================================
#include <algorithm>
// ============================================================================
// CaloFutureInterfaces 
// ============================================================================
#include "CaloFutureInterfaces/ICaloFutureHypoTool.h" 
// ============================================================================
// CaloFutureEvent/Event
// ============================================================================
#include "Event/CaloHypo.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// ============================================================================
// local
// ============================================================================
#include "CaloFutureHypoAlg.h"
// ============================================================================
/** @file CaloFutureHypoAlg.cpp
 * 
 *  Template implementation file for class : CaloFutureHypoAlg
 *  @see CaloFutureHypoAlg
 * 
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 02/09/2002
 */
// ============================================================================
DECLARE_COMPONENT( CaloFutureHypoAlg )
// ============================================================================
/*  Standard constructor
 *  @param   name   algorithm name 
 *  @param   svcloc pointer to service locator 
 */
// ============================================================================
CaloFutureHypoAlg::CaloFutureHypoAlg
( const std::string& name   ,
  ISvcLocator*       svcloc )
  : GaudiAlgorithm ( name , svcloc ) 
{
  setProperty ( "PropertiesPrint" , true ) ;
}
// ============================================================================
/*  standard algorithm initialization 
 *  @see CaloFutureAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *  @return status code 
 */
// ============================================================================
StatusCode CaloFutureHypoAlg::initialize() 
{  
  StatusCode sc = GaudiAlgorithm::initialize();
  if( sc.isFailure() ) 
  { return Error("Could not initialize the base class CaloFutureAlgorithm",sc);}

  // apply default context-dependent TES location (if not defined)
  if(  m_inputData.empty() && !m_type.empty() )m_inputData =  LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( m_type , context() );

  
  if ( m_inputData.empty() ) 
  { return Error ( "Empty 'InptuData'"  ) ; }
  //
  for( Names::const_iterator it = m_names.begin() ; 
       m_names.end() != it ; ++it ) 
  {
    ICaloFutureHypoTool* t = tool<ICaloFutureHypoTool>( *it , this ) ;
    if( 0 == t ) { return Error("Could not locate the tool!"); }
    m_tools.push_back( t );
  }
  //
  if ( m_tools.empty()     ) { Warning ( "Empty list of tools").ignore() ; }
  //   
  counterStat = tool<IFutureCounterLevel>("FutureCounterLevel");
  return StatusCode::SUCCESS;
}
// ============================================================================
/*  standard algorithm finalization 
 *  @see CaloFutureAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *  @return status code 
 */
// ============================================================================
StatusCode CaloFutureHypoAlg::finalize() 
{  
  // clear the container 
  m_tools.clear();  
  /// finalize the base class 
  return GaudiAlgorithm::finalize();
}
// ============================================================================
/*  standard algorithm execution 
 *  @see CaloFutureAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *  @return status code 
 */
// ============================================================================
StatusCode CaloFutureHypoAlg::execute() 
{
  // avoid long name and types 
  typedef LHCb::CaloHypos  Hypos;
  
  Hypos* hypos = get<Hypos>( m_inputData ) ;
  if( 0 == hypos ) { return StatusCode::FAILURE ; }
  
  for( Hypos::const_iterator hypo = hypos->begin() ; 
       hypos->end() != hypo ; ++hypo ) 
  {
    // skip NUULS 
    if( 0 == *hypo ) { continue ; }
    // loop over all tools
    StatusCode sc = StatusCode::SUCCESS ;
    for( Tools::const_iterator itool = m_tools.begin() ; 
         m_tools.end() != itool && sc.isSuccess() ; ++itool )
    { sc = (**itool)( *hypo ); }
    if( sc.isFailure() ) 
    { Error("Error from the Tool! skip hypo!",sc).ignore() ; continue ; }  
  }

  if(counterStat->isQuiet())counter ( "#Hypos from '" + m_inputData +"'") += hypos->size() ;
  //
  return StatusCode::SUCCESS ;
}
