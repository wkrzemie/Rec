/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CALOFUTUREElectronALG_H
#define CALOFUTURERECO_CALOFUTUREElectronALG_H 1
// Include files
// from STL
#include <string>
// from GaudiAlg
#include "GaudiAlg/ScalarTransformer.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloHypo.h" 
#include "Event/CaloCluster.h" 
#include "CaloFutureInterfaces/IFutureCounterLevel.h" 

// forward delcarations
struct ICaloFutureClusterSelector ;
struct ICaloFutureHypoTool        ;

/** @class CaloFutureElectronAlg CaloFutureElectronAlg.h
 *
 *  The simplest algorithm of reconstruction of
 *  electrons in electromagnetic calorimeter.
 *
 *  @author Vanya Belyaev      Ivan.Belyaev@itep.ru
 *  @date   31/03/2002
 */
class CaloFutureElectronAlg 
: public Gaudi::Functional::ScalarTransformer<CaloFutureElectronAlg,LHCb::CaloHypos(const LHCb::CaloClusters&)>
{
 public:
  
  CaloFutureElectronAlg(const std::string& name, ISvcLocator* pSvcLocator );
  


  StatusCode initialize() override;
  StatusCode finalize() override;

  using ScalarTransformer::operator();
  boost::optional<LHCb::CaloHypo> operator()(const LHCb::CaloCluster& ) const;
  void postprocess(const LHCb::CaloHypos&) const;  
private:

  /// container of names
  typedef std::vector<std::string>           Names       ;
  /// container of selector tools
  typedef std::vector<ICaloFutureClusterSelector*> Selectors   ;
  /// containers of hypo tools
  typedef std::vector<ICaloFutureHypoTool*>        HypoTools   ;
  /// container of correction tools (S-,L-,...)
  typedef HypoTools                          Corrections ;


  // cluster selectors
  Selectors              m_selectors;
  Gaudi::Property<Names> m_selectorsTypeNames {this, "SelectionTools", {
    "CaloFutureSelectCluster/ElectronCluster",
    "CaloFutureSelectChargedClusterWithSpd/ChargedWithSpd",
    "CaloFutureSelectClusterWithPrs/ClusterWithPrs",
    "CaloFutureSelectorNOT/ChargedWithTracks"
  }, "List of Cluster selector tools"};

  // corrections
  Corrections            m_corrections;
  Gaudi::Property<Names> m_correctionsTypeNames
    {this, "CorrectionTools", {}, "List of primary correction tools"};

  // other hypo tools
  HypoTools              m_hypotools;
  Gaudi::Property<Names> m_hypotoolsTypeNames
    {this, "HypoTools", {"CaloFutureExraDigits/SpdPrsExtraE"},
    "List of generic Hypo-tools to apply for newly created hypos"};

  // corrections
  Corrections            m_corrections2;
  Gaudi::Property<Names> m_correctionsTypeNames2 {this, "CorrectionTools2", {
    "CaloECorrection/ECorrection" ,
    "CaloSCorrection/SCorrection" ,
    "CaloLCorrection/LCorrection"
  }, "List of tools for 'fine-corrections"};

  // other hypo tools
  HypoTools    m_hypotools2;
  Gaudi::Property<Names> m_hypotoolsTypeNames2
    {this, "HypoTools2", {},
    "List of generi Hypo-tools to apply for corrected hypos"};

  Gaudi::Property<std::string> m_detData    {this, "Detector", LHCb::CaloFutureAlgUtils::DeCaloFutureLocation("Ecal")  };
  const DeCalorimeter*  m_det = nullptr;
  Gaudi::Property<float> m_eTcut {this, "EtCut", 0, "Threshold on cluster & hypo ET"};
  IFutureCounterLevel* counterStat = nullptr;
  
  LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> m_eT { nullptr } ; // TODO: can we combine this into Over_Et_Threshold and make it a property??

};

// ============================================================================
#endif // CALOFUTUREElectronALG_H
