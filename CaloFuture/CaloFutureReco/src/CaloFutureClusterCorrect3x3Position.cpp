/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// STD & STL
#include <algorithm>
// from Gaudi
#include "GaudiKernel/SmartDataPtr.h"
// CaloFutureInterfaces
#include "CaloFutureInterfaces/ICaloFutureClusterTool.h"
#include "CaloFutureInterfaces/ICaloFutureHypoTool.h"
// CaloDet
#include "CaloDet/DeCalorimeter.h"
// CaloFutureEvent
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/CaloPosition.h"
// CaloFutureUtils
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// Kernel
#include "Kernel/CaloCellID.h"
// local
#include "CaloFutureClusterCorrect3x3Position.h"

// ============================================================================
/** @file CaloFutureClusterCorrect3x3Position.cpp
 *
 *  Implementation file for class : CaloFutureClusterCorrect3x3Position
 *
 *  @author Olivier Deschamps
 *  @date 10/05/2002
 *
 *  Revision 1.2 2004/09/02 20:46:40  odescham
 * - Transv. Shape parameters in option file
 *
 */
// ============================================================================

DECLARE_COMPONENT( CaloFutureClusterCorrect3x3Position )

// ============================================================================
/** Standard constructor
 *  @param   name   algorithm name
 *  @param   svcloc pointer to service locator
 */
// ============================================================================
CaloFutureClusterCorrect3x3Position::CaloFutureClusterCorrect3x3Position
( const std::string& name    ,
  ISvcLocator*       svcloc  )
  : GaudiAlgorithm ( name , svcloc )
{
  // set default data as a function of detector & context
  m_detData= LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name ) ;
  m_inputData = LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name , context() );

}

// ============================================================================
/** helper function to calculate number of digits
 *  in the cluster with given status
 *  @param cluster pointet to the cluster object
 *  @param status  digit statsu to be checked
 *  @return number of digits with given status.
 *       In the case of errors it returns -1
 */
// ============================================================================
long CaloFutureClusterCorrect3x3Position::numberOfDigits
( const LHCb::CaloCluster*             cluster ,
  const LHCb::CaloDigitStatus::Status& status  ) const
{
  /// check arguments
  if( !cluster )
  { Error(" numberOfDigits! CaloCluster* points to NULL!").ignore(); return -1;}
  ///
  const auto& entries = cluster->entries();
  return std::count_if( entries.begin(), entries.end(),
                        [&](const LHCb::CaloClusterEntry& entry) -> bool {
                            return entry.status()&status;
                        } );
}

// ============================================================================
/** standard algorithm execution
 *  @return status code
 */
// ============================================================================
StatusCode CaloFutureClusterCorrect3x3Position::execute()
{
  /// avoid long names
  using namespace  LHCb::CaloDataFunctor;
  typedef LHCb::CaloClusters        Clusters;

  /// load data
  SmartDataPtr<DeCalorimeter> detector( detSvc() , m_detData );
  if( !detector )
    { return Error("could not locate calorimeter '"+m_detData+"'");}

  const DeCalorimeter* calo =  getDet<DeCalorimeter>( m_detData ) ;
  m_cell3x3.setDet ( calo ) ;
  m_neighbour.setDet ( calo );
  SmartDataPtr<Clusters>  clusters( eventSvc() , m_inputData );
  if( !clusters ) { 
      return Error("Could not load input data ='"+m_inputData+"'");
  }

  /// loop over all clusters
  for( LHCb::CaloCluster* cluster : *clusters ) {
      if( !cluster ) { continue ; }

      double Energy=0;
      double PosX=0;
      double PosY=0;
      /// Find Cluster Seed
      const auto iSeed =
        clusterLocateDigit( cluster->entries().begin() ,
                            cluster->entries().end  () ,
                            LHCb::CaloDigitStatus::SeedCell     );
      const LHCb::CaloDigit* seed = iSeed->digit() ;
      if( !seed) { continue ; }
      const LHCb::CaloCellID&  seedID = seed->cellID() ;
      // Loop over 3x3 digits
      for(const auto& entry : cluster->entries()) {
        const LHCb::CaloDigit*   cell = entry.digit();
        const LHCb::CaloCellID&  cellID = cell->cellID() ;
        if( 0 != m_cell3x3( seedID , cellID) && m_neighbour(seedID,cellID) ){
          Energy += cell->e() * entry.fraction();
          PosX += detector->cellX(cellID) * cell->e() * entry.fraction();
          PosY += detector->cellY(cellID) * cell->e() * entry.fraction();
        }
      }
      PosX=PosX/Energy;
      PosY=PosY/Energy;
      LHCb::CaloPosition pos = cluster->position();
      LHCb::CaloPosition::Parameters  params;

      params(LHCb::CaloPosition::Index::X)=PosX;
      params(LHCb::CaloPosition::Index::Y)=PosY;
      params(LHCb::CaloPosition::Index::E)=Energy;
      pos.setParameters( params );
      cluster->setPosition( pos );
  }
  return StatusCode::SUCCESS;
}

// ============================================================================
// The End
// ============================================================================
