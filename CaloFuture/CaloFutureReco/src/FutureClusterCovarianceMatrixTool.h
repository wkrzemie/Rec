/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CLUSTERCOVARIANCEMATRIXTOOL_H
#define CLUSTERCOVARIANCEMATRIXTOOL_H 1
// Include files
#include  "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"
#include  "CaloFutureUtils/CovarianceEstimator.h"
#include  "CaloFutureInterfaces/ICaloFutureClusterTool.h"
#include  "CaloFutureInterfaces/IFutureCounterLevel.h"
#include  "CaloFutureCorrectionBase.h"


/** @class FutureClusterCovarianceMatrixTool
 *         FutureClusterCovarianceMatrixTool.h
 *
 *  Concrete tool for calculation of covariance matrix
 *  for the whole cluster object
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

namespace CaloFutureCovariance{
  enum Parameters{
    Stochastic      = 0 ,  // stochastig term     Cov(EE)_i +=  [ S  * sqrt(E_i_GeV) ]^2
    GainError       = 1 ,  // constant term       Cov(EE)_i +=  [ G  * E_i           ]^2
    IncoherentNoise = 2 ,  // noise (inc.) term   Cov(EE)_i +=  [ iN * gain_i        ]^2
    CoherentNoise   = 3 ,  // noise (coh.) term   Cov(EE)_i +=  [ cN * gain_i        ]^2
    ConstantE       = 4 ,  // additional term     Cov(EE) +=  [ cE               ]^2
    ConstantX       = 5 ,  // additional term     Cov(XX) +=  [ cX               ]^2
    ConstantY       = 6 ,  // additional term     Cov(XX) +=  [ cY               ]^2
    Last
  };
  constexpr int nParams=Last+1;
  inline const std::string ParameterName[nParams] = {"Stochastic","GainError","IncoherentNoise","CoherentNoise",
                                                     "ConstantE" ,"ConstantX","ConstantY"};
  inline const std::string ParameterUnit[nParams] = {"Sqrt(GeV)" , "" , "ADC" , "ADC" , "MeV" , "mm" , "mm"};
  typedef std::map<std::string,std::vector<double> > ParameterMap;
  typedef SimplePropertyRef< ParameterMap >          ParameterProperty;
}




class FutureClusterCovarianceMatrixTool: public virtual ICaloFutureClusterTool , public GaudiTool , virtual public IIncidentListener{
public:

  StatusCode initialize() override;
  StatusCode finalize() override;
  StatusCode process    ( LHCb::CaloCluster* cluster ) const override;
  StatusCode operator() ( LHCb::CaloCluster* cluster ) const override;


  void handle(const Incident&  ) override {
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )debug() << "IIncident Svc reset" << endmsg;
    setEstimatorParams(); // reset estimator when parameters change
  }


protected:

  StatusCode getParamsFromOptions();
  StatusCode getParamsFromDB();
  void setEstimatorParams(bool init=false);

public:
  FutureClusterCovarianceMatrixTool( const std::string& type   ,
                               const std::string& name   ,
                               const IInterface*  parent );

private:

  CovarianceEstimator     m_estimator ;
  CaloFutureCovariance::ParameterMap m_parameters;
  Gaudi::Property<std::string> m_detData {this, "Detector"};
  const DeCalorimeter* m_det        = nullptr;
  Gaudi::Property<bool> m_useDB {this, "UseDBParameters", true};
  CaloFutureCorrectionBase*  m_dbAccessor = nullptr;
  Gaudi::Property<std::string> m_conditionName {this, "ConditionName"};
  std::map<unsigned int,std::string> m_source;
  IFutureCounterLevel* counterStat = nullptr;
}; ///< end of class FutureClusterCovarianceMatrixTool
// ============================================================================
#endif // CLUSTERCOVARIANCEMATRIXTOOL_H
