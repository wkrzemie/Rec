/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files 
// ============================================================================
// CaloFutureInterfaces
#include "CaloFutureInterfaces/ICaloFutureClusterTool.h"
// CaloDet 
#include "CaloDet/DeCalorimeter.h"
// CaloFutureEvent 
#include "Event/CaloCluster.h"
// local
#include "FutureSubClusterSelectorBase.h"

// ============================================================================
/** @file SubclusterSelectorBase.cpp
 * 
 *  Implementation file for class : FutureSubClusterSelectorBase
 * 
 *  @date 07/11/2001 
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru 
 */
// ============================================================================

// ============================================================================
/** Standard Tool Constructor
 *  @param type type of the tool (useless ? )
 *  @param name name of the tool 
 *  @param parent the tool parent 
 */
// ============================================================================
FutureSubClusterSelectorBase::FutureSubClusterSelectorBase( const std::string& type,
                                                const std::string& name,
                                                const IInterface* parent )
  : GaudiTool  ( type, name , parent ) 
  , m_mask(defaultStatus)
{
  /// declare the available interfaces
  declareInterface<ICaloFutureClusterTool>   ( this )    ;
  declareInterface<ICaloFutureSubClusterTag> ( this )    ;
}
// ============================================================================

// ============================================================================
/** standard finalization method 
 *  @return status code 
 */
// ============================================================================
StatusCode FutureSubClusterSelectorBase::finalize   ()
{ return GaudiTool::finalize(); }
// ============================================================================

// ============================================================================
/** standard initialization method 
 *  @return status code 
 */
// ============================================================================
StatusCode FutureSubClusterSelectorBase::initialize ()
{
  // initialize the base class
  StatusCode sc = GaudiTool::initialize() ;
  if( sc.isFailure() ) 
    { return Error("Could not initialize the base class!",sc);}
  // load and set the  detector
  m_det = getDet<DeCalorimeter>( m_detData ) ;
  // 
  return StatusCode::SUCCESS;
}
// ============================================================================

// ============================================================================
/** The main processing method (functor interface) 
 *  @param cluster pointer to CaloCluster object to be processed
 *  @return status code 
 */  
// ============================================================================
StatusCode FutureSubClusterSelectorBase::process     ( LHCb::CaloCluster* cluster ) const{ 
  return tag ( cluster ) ; 
}  

// ============================================================================

// ============================================================================
/** The main processing method 
 *  @see ICaloFutureSubClusterTag
 *  @see ICaloFutureClusterTool
 *  @param cluster pointer to CaloCluster object to be processed
 *  @return status code 
 */  
// ============================================================================
StatusCode FutureSubClusterSelectorBase::operator() ( LHCb::CaloCluster* cluster ) const{ 
  return tag ( cluster ) ; 
}  
// ============================================================================

