/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//
// ============================================================================
#ifndef CALOFUTURERECO_CALOFUTURESELECTCLUSTER_H
#define CALOFUTURERECO_CALOFUTURESELECTCLUSTER_H 1
// ============================================================================
#include <string>
#include "GaudiAlg/GaudiTool.h"
#include "CaloFutureInterfaces/ICaloFutureClusterSelector.h"
#include "GaudiKernel/Counters.h"
// ============================================================================

class CaloFutureSelectCluster :
  public virtual ICaloFutureClusterSelector,
  public GaudiTool
{
public:

  bool select( const LHCb::CaloCluster* cluster ) const override;
  virtual bool operator()( const LHCb::CaloCluster* cluster ) const override;
  StatusCode initialize () override;

  CaloFutureSelectCluster( const std::string& type,
                     const std::string& name,
                     const IInterface* parent);

private:
  Gaudi::Property<float> m_cut {this, "MinEnergy", 0.};
  Gaudi::Property<float> m_etCut {this, "MinEt", 0.};
  Gaudi::Property<int> m_mult {this, "MaxDigits", 9999};
  Gaudi::Property<int> m_multMin {this, "MinDigits", -9999};
  mutable Gaudi::Accumulators::Counter<> m_counter{this, "selected clusters"};
};
#endif // CALOFUTURERECO_CALOFUTURESELECTCLUSTER_H
