/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CALOFUTURESELECTORAND_H
#define CALOFUTURERECO_CALOFUTURESELECTORAND_H
// Include files
// from STL
#include <string>
// from GaudiAlg
#include "GaudiAlg/GaudiTool.h"
// From CaloFutureInterfaces
#include "CaloFutureInterfaces/ICaloFutureClusterSelector.h"

/** @class CaloFutureSelectorAND CaloFutureSelectorAND.h
 *
 *  Helper concrete tool for selection of calocluster objects
 *  This selector selects the cluster if
 *  all of its daughter selector select it!
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   27/04/2002
 */
class CaloFutureSelectorAND :
  public virtual ICaloFutureClusterSelector ,
  public          GaudiTool
{
public:
  /// container of types&names
  typedef std::vector<std::string>           Names     ;
  /// container of selectors
  typedef std::vector<ICaloFutureClusterSelector*> Selectors ;

public:

  /** "select"/"preselect" method
   *  @see ICaloFutureClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select
  ( const LHCb::CaloCluster* cluster ) const  override;

  /** "select"/"preselect" method (functor interface)
   *  @see ICaloFutureClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator ()
    ( const LHCb::CaloCluster* cluster ) const  override;

  /** standard initialization of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode initialize() override;

  /** standard finalization  of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode finalize() override;

  /** Standard constructor
   *  @see GaudiTool
   *  @see  AlgTool
   *  @see IAlgTool
   *  @param type   tool type (?)
   *  @param name   tool name
   *  @param parent tool parent
   */
  CaloFutureSelectorAND
  ( const std::string& type,
    const std::string& name,
    const IInterface* parent);

private:

  ///   default  constructor  is  private
  CaloFutureSelectorAND();
  ///   copy     constructor  is  private
  CaloFutureSelectorAND
  (const CaloFutureSelectorAND& );
  ///   assignement operator  is  private
  CaloFutureSelectorAND& operator=
  (const CaloFutureSelectorAND& );

private:

  Gaudi::Property<Names> m_selectorsTypeNames {this, "SelectorTools"};
  Selectors m_selectors;

};

// ============================================================================
// The END
// ============================================================================
#endif // CALOFUTURERECO_CALOFUTURESELECTORAND_H
// ============================================================================
