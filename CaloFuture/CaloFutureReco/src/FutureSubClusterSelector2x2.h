/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_SUBCLUSTERSELECTOR2x2_H
#define CALOFUTURERECO_SUBCLUSTERSELECTOR2x2_H 1
// Include files
#include "FutureSubClusterSelectorBase.h"
#include "CaloFutureUtils/CellMatrix2x2.h"


class FutureSubClusterSelector2x2: public FutureSubClusterSelectorBase{
public:

  StatusCode initialize() override;
  StatusCode tag        ( LHCb::CaloCluster* cluster ) const override;
  StatusCode untag      ( LHCb::CaloCluster* cluster ) const override;

protected:
  StatusCode choice2x2 (LHCb::CaloCluster* cluster,
                        CellMatrix2x2::SubMatrixType &type,
                        double &energy) const ;

public:
  FutureSubClusterSelector2x2( const std::string& type   ,
                         const std::string& name   ,
                         const IInterface*  parent );

private:
  const DeCalorimeter* m_det = NULL;
  CellMatrix2x2        m_matrix;
};

#endif // SUBCLUSTERSELECTOR2X2_H
