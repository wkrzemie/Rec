/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// from std
#include <vector>
 // from Gaudi
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// local
#include "CaloFutureShowerOverlap.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureShowerOverlap
//
// 2014-06-02 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloFutureShowerOverlap )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloFutureShowerOverlap::CaloFutureShowerOverlap( const std::string& name,
                                      ISvcLocator* pSvcLocator)
: Transformer ( name , pSvcLocator,
               KeyValue{"InputData" , LHCb::CaloClusterLocation::EcalRaw },
                KeyValue{"OutputData", LHCb::CaloClusterLocation::Ecal } )
{
  m_det   = LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name ) ;

  updateHandleLocation(*this,"InputData" ,
                       LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation(name, context(), "EcalRaw"));
  updateHandleLocation(*this,"OutputData",
                       LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation(name, context()));
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloFutureShowerOverlap::initialize() {
  StatusCode sc = Transformer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by Transformer

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  m_oTool   = tool<ICaloFutureShowerOverlapTool>("CaloFutureShowerOverlapTool","PhotonShowerOverlap",this);
  m_tagger  = tool<FutureSubClusterSelectorTool>( "FutureSubClusterSelectorTool" , "EcalClusterTag" , this );

  m_detector  = getDet<DeCalorimeter> ( m_det );
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::CaloCluster::Container CaloFutureShowerOverlap::operator()(const LHCb::CaloCluster::Container& clusters) const {

  if ( UNLIKELY(msgLevel(MSG::DEBUG) ) )debug() << "==> Execute" << endmsg;
  LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> eT(m_detector);

  // create new container
  LHCb::CaloCluster::Container outputClusters;
  // update the version number (needed for serialization)
  outputClusters.setVersion(2);
  outputClusters.reserve(clusters.size());
  // copy clusters to new container
  for (const auto* cl : clusters) {
      outputClusters.insert(new LHCb::CaloCluster(*cl));
  }

  // loop over new clusters and correct them
  for( auto i1 = outputClusters.begin() ; outputClusters.end() != i1 ; ++i1 ){
    auto* cl1 = *i1; // simplify syntax in the following
    double et1 = eT( cl1 );
    if( et1 < m_etMin )continue; // neglect overlap from/to low ET clusters
    const LHCb::CaloCellID id1 = cl1->seed();
      for( auto i2 = std::next(i1) ; outputClusters.end() != i2 ; ++i2 ){
      auto* cl2 = *i2; // simplify syntax in the following
      double et2=eT( cl2 );
      if(  et2 < m_etMin )continue; // neglect overlap from/to low ET clusters
      if(  et1 < m_etMin2 && et2 < m_etMin2 )continue; // require at least one cluster above threshold (speed-up)
      const LHCb::CaloCellID id2 = cl2->seed();
      if( id1.area() != id2.area() ) continue;
      if( abs( int(id1.col()) - int(id2.col()) ) > m_dMin || abs( int(id1.row()) - int(id2.row()) ) > m_dMin )continue;

      // initial weights for shared cells
      for( auto e1 = cl1->entries().begin() ; cl1->entries().end() != e1 ; ++e1 ){
        for( auto e2 = cl2->entries().begin() ; cl2->entries().end() != e2 ; ++e2 ){
          if( e1->digit()->cellID() == e2->digit()->cellID() ){
            const auto totE = ( cl1->e() + cl2->e() );
            e1->setFraction( cl1->e() / totE );
            e2->setFraction( cl2->e() / totE );
          }
        }
      }
      // tag the cluster position to have correct corrections
      const StatusCode sc = StatusCode{ m_tagger->tagPosition(  cl1  ) &&
                                        m_tagger->tagPosition(  cl2  ) };
      if( sc.isFailure() )Warning("Cluster tagging failed - keep the initial 3x3 tagging").ignore();
      // correct entry weight for shower overlap (assuming EM cluster)
      m_oTool->process(*cl1,*cl2, m_iter);
    }
  }
  return outputClusters;
}

//=============================================================================
