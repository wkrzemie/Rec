#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# $Id: Reconstruction.py,v 1.12 2010-05-20 09:47:06 odescham Exp $
# =============================================================================
## The major building blocks of CaloFuturerimeter Reconstruction
#  @author Vanya BELYAEV Ivan.Belyaev@nikhe.nl
#  @date 2008-07-17
# =============================================================================
"""
The major building blocks of CaloFuturerimeter Reconstruction
"""

# =============================================================================
__author__  = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
__version__ = "CVS tag $Name: not supported by cvs2svn $, version $Revision: 1.12 $"
# =============================================================================
__all__ = (
    'clustersReco'   , 
    'photonsReco'    , 
    'mergedPi0sReco' 
    )
# =============================================================================
from Gaudi.Configuration import *

from Configurables import GaudiSequencer

from GaudiKernel.SystemOfUnits import MeV, GeV


from CaloKernel.ConfUtils import ( prntCmp        ,
                                   addAlgs        ,
                                   hltContext     ,
                                   setTheProperty ,
                                   onDemand       ,
                                   caloOnDemand   ,
                                   getAlgo        ) 

import logging
_log = logging.getLogger('CaloFutureReco')

# =============================================================================
## prepare the digits for the recontruction
def digitsFutureReco  ( context            ,
                  enableRecoOnDemand ,
                  createADC          ) :
    """
    Prepare the digits for the recontruction
    """

    from CaloFutureDAQ.CaloFutureDigits  import caloDigits

    _log.warning('CaloFutureReco.digitsFutureReco is deprecated, use CaloFutureDigitConf instead')

    # warning : the caloDigits TES should be context-independent
    return caloDigits ( context            ,
                        enableRecoOnDemand ,
                        createADC          )

## ============================================================================
## define the recontruction of Ecal clusters
def clusterFutureReco ( context , enableRecoOnDemand , clusterPt = 0.  , fastReco = False , external = '', makeTag=False,
                  noSpdPrs=False , masksE=[] , masksP=[] , mergedPi0Pt = 2* GeV) :
    """
    Define the recontruction of Ecal Clusters
    """
    
    from Configurables import ( CaloFutureDigitFilterAlg       ,
                                CaloFutureDigitFilterTool      ,
                                FutureCellularAutomatonAlg     ,
                                CaloFutureShowerOverlap        ,
                                # CaloFutureSharedCellAlg        , # obsolete
                                CaloFutureClusterCovarianceAlg ,
                                CaloFutureClusterizationTool) 

    # cluster TES  is 'almost' context-independent : single HLT TES whatever the HLT-type else 'offline' TES
    ## Warning MUST be synchronous with CaloFutureAlgUtils TES settings
    ### Exception when external clusters location is given

    if external == '' :
        _cont = context
        if '' != context and 'OFFLINE' != _cont.upper() and _cont.upper().find( 'HLT' ) != -1 :
            context = 'Hlt'
        else :
            context = ''
    elif makeTag != '':
        context = makeTag
    

    ## Define the context-dependent sequencer
    seq   = getAlgo ( GaudiSequencer           , "ClusterFutureReco", context , "Rec/Calo/EcalClusters" , enableRecoOnDemand )

    seq.Members[:]=[]
    filter= getAlgo ( CaloFutureDigitFilterAlg       , "CaloFutureDigitFilter",context)
    if noSpdPrs :
        filter.PrsFilter=0
        filter.SpdFilter=0

    clust = getAlgo ( FutureCellularAutomatonAlg     , "FutureEcalClust"  , context )
#   share = getAlgo ( CaloFutureSharedCellAlg        , "EcalShare"  , context )   # obsolete
    share = getAlgo ( CaloFutureShowerOverlap        , "FutureEcalShare"  , context )  
    covar = getAlgo ( CaloFutureClusterCovarianceAlg , "FutureEcalCovar"  , context ) 

    if masksE != [] :
        share.EnergyTags = masksE
        covar.EnergyTags = masksE
    if masksP != [] :
        share.PositionTags = masksP
        covar.PositionTags = masksP

    if external != '' :                  # use non-default clusters 
        share.InputData = external
        covar.InputData = external
        if makeTag != '' :               # make the non-default clusters
            seq.Members += [ filter, clust ]
            clust.OutputData = external
    else :
        seq.Members += [ filter, clust ]        # make default clusters


    if clusterPt > 0 :
        from Configurables import CaloFutureClusterizationTool
        clust.addTool(CaloFutureClusterizationTool,'CaloFutureClusterizationTool')
        clust.CaloFutureClusterizationTool.ETcut = clusterPt
        clust.CaloFutureClusterizationTool.withET = True

    seq.Members += [ share , covar ]
    setTheProperty ( seq , 'Context' , context )


    ## setup onDemand for SplitClusters
    if enableRecoOnDemand : 
        splitSeq   = mergedPi0Reco( context , enableRecoOnDemand , True, False, False,mergedPi0Pt,False,'',noSpdPrs,masksE,masksP)
        onDemand ( 'Rec/Calo/EcalSplitClusters' , splitSeq , context ) 


    ## printout
    _log.debug ( 'Configure Ecal Cluster Reco Seq : %s   for %s : ' % (seq.name()  , context) )
    if enableRecoOnDemand : 
        _log.info    ('ClusterFutureReco onDemand')
    
    ##    
    return seq

# ============================================================================
## define the recontruction of  Single Photons
def photonFutureReco ( context , enableRecoOnDemand, useTracks = True , useSpd = False, usePrs = False , trackLocation = '', neutralID = True,
                 photonPt=50.*MeV, fastReco = False, external = '',noSpdPrs=False,matchTrTypes=[]) :
    """
    Define the recontruction of Single Photon Hypo
    """
    
    from Configurables import   CaloFutureSinglePhotonAlg                
    from Configurables import   CaloFutureHypoAlg     
    from Configurables import ( CaloFutureSelectCluster                  , 
                                CaloFutureSelectClusterWithPrs           ,
                                CaloFutureSelectNeutralClusterWithTracks , 
                                CaloFutureSelectNeutralClusterWithSpd    ,
                                CaloFutureSelectChargedClusterWithSpd    , 
                                CaloFutureSelectorNOT                    ,
                                CaloFutureSelectorOR                     )
    
    from Configurables import ( CaloFutureExtraDigits ,
                                CaloFutureECorrection , 
                                CaloFutureSCorrection , 
                                CaloFutureLCorrection )
                                 
    
    
    ## build the context-dependent sequence (  TrackMatch + SinglePhotonRec )
    seq = getAlgo ( GaudiSequencer , "PhotonFutureReco" , context , "Rec/Calo/Photons"  , enableRecoOnDemand   )
    seq.Members=[]

    # 1/ PhotonMatch from CaloFuturePIDs (if tracking is requested)
    if useTracks : 
        from CaloFuturePIDs.PIDs import trackMatch
        tm =  trackMatch ( context,enableRecoOnDemand, trackLocation , matchTrTypes, fastReco, external)
        addAlgs ( seq , tm ) 
    

    ##2/ SinglePhotonRec alg
    alg = getAlgo ( CaloFutureSinglePhotonAlg, "SinglePhotonRecFuture" , context )
    alg.PropertiesPrint = False
    if external != '' :
        alg.InputData = external
    
    # cluster selection tools:
    ### a/ generic selection (energy/multiplicity)
    alg.addTool ( CaloFutureSelectCluster  , "PhotonClusterFuture" )
    alg.SelectionTools = [ alg.PhotonClusterFuture ]
    alg.PhotonClusterFuture.MinEt     = photonPt
    alg.PhotonClusterFuture.MinDigits = 2  # skip single-cell clusters
    
    ### b/ Neutral cluster (track-based and/or Spd/Prs-based)    
    if   useTracks     :
        alg.addTool ( CaloFutureSelectNeutralClusterWithTracks , "NeutralClusterFuture" )
        alg.NeutralClusterFuture.MinChi2 = 4
        alg.SelectionTools += [ alg.NeutralClusterFuture ]
        _log.debug    ('CaloFutureReco/PhotonFutureReco: Configure Neutral Cluster Selector with Tracks     : %s' %  alg.NeutralClusterFuture.getFullName() )

    if usePrs :
        alg.addTool ( CaloFutureSelectClusterWithPrs           , "ClusterWithPrsFuture" )
        alg.ClusterWithPrsFuture.MinEnergy = 10.*MeV
        alg.SelectionTools += [ alg.ClusterWithPrsFuture ]
        _log.debug ('CaloFutureReco/PhotonFutureReco: Configure Cluster Selector with  Prs       : %s' %  alg.ClusterWithPrs.getFullName() )

    if  useSpd  :
        alg.addTool ( CaloFutureSelectNeutralClusterWithSpd    , "NeutralClusterWithSpdFuture"    )        
        alg.SelectionTools += [ alg.NeutralClusterWithSpdFuture ]
        _log.debug ('CaloFutureReco/PhotonFutureReco: Configure Neutral Cluster Selector with !Spd : %s ' %   alg.NeutralClusterWithSpdFuture.getFullName()  )
        
        
    ## hypo tools : add Spd/Prs digits

    if not noSpdPrs :
        alg.addTool ( CaloFutureExtraDigits , 'SpdPrsExtraG' )
        alg.HypoTools = [ alg.SpdPrsExtraG ]
    else :
        alg.HypoTools = []
        
    ## correction tools : E/S/L
    alg.addTool ( CaloFutureECorrection , 'ECorrection' )
    alg.addTool ( CaloFutureSCorrection , 'SCorrection' )
    alg.addTool ( CaloFutureLCorrection , 'LCorrection' )
    
    ecorr = alg.ECorrection
    scorr = alg.SCorrection
    lcorr = alg.LCorrection

    # tool configuration via condDB only (remove default configuration)
    alg.CorrectionTools2 = [ ecorr , scorr , lcorr ]                      

    # update the sequence
    addAlgs ( seq , alg ) 

    ## 3/ PhotonID
    from CaloFuturePIDs.PIDs import PhotonID
    if neutralID : 
        pID =  PhotonID ( context,enableRecoOnDemand, useTracks )
        addAlgs ( seq , pID ) 

    ## global context
    setTheProperty ( seq , 'Context' , context )

    ## printout
    _log.debug ( 'Configure Photon Reco Seq : %s  for : %s' % (seq.name() , context ) )
    if enableRecoOnDemand : 
        _log.info    ('PhotonFutureReco onDemand')
        
    return seq

# ============================================================================
## define the recontruction of Electorn Hypos
def electronFutureReco ( context , enableRecoOnDemand , useTracksE = True , useSpdE = True, usePrsE = True, trackLocation = '' ,
                   electronPt=50.*MeV , fastReco = False, external = '' ,noSpdPrs=False,matchTrTypes=[]) :
    """
    Define the reconstruction of
    """

    from Configurables import   CaloFutureElectronAlg                
    from Configurables import   CaloFutureHypoAlg 
    
    from Configurables import ( CaloFutureSelectCluster                  , 
                                CaloFutureSelectClusterWithPrs           ,
                                CaloFutureSelectNeutralClusterWithTracks , 
                                CaloFutureSelectNeutralClusterWithSpd    ,
                                CaloFutureSelectChargedClusterWithSpd    , 
                                CaloFutureSelectorNOT                    )
    
    from Configurables import ( CaloFutureExtraDigits ,
                                CaloFutureECorrection , 
                                CaloFutureSCorrection , 
                                CaloFutureLCorrection ) 
    

    ## build the context-dependent sequence (  TrackMatch + SingleElectronRec )
    seq = getAlgo ( GaudiSequencer , "ElectronFutureReco" , context  , 'Rec/Calo/Electrons' , enableRecoOnDemand   ) 
    seq.Members=[]

    # 1/ ElectronMatch from CaloFuturePIDs (if useTracks)
    if useTracksE :
        from CaloFuturePIDs.PIDs import trackMatch
        tm =  trackMatch ( context,enableRecoOnDemand, trackLocation , matchTrTypes, fastReco , external)
        addAlgs ( seq , tm ) 

    ## 2/ Electron Rec alg
    alg = getAlgo ( CaloFutureElectronAlg ,  'SingleElectronRecFuture', context  ) 
    alg.PropertiesPrint = False
    if external != '' :
        alg.InputData = external
        
    # cluster selection tools:

    ## 1/ generic selection (energy/multiplicity)
    alg.addTool ( CaloFutureSelectCluster               , "ElectronClusterFuture" )
    alg.SelectionTools = [ alg.ElectronClusterFuture ]
    alg.ElectronClusterFuture.MinEt = electronPt
    alg.ElectronClusterFuture.MinDigits = 2 # skip single-cell clusters

    ## 2/  hits in Spd
    if useSpdE : 
        alg.addTool ( CaloFutureSelectChargedClusterWithSpd , "ChargedClusterWithSpdFuture"  )
        alg.SelectionTools += [ alg.ChargedClusterWithSpdFuture ]
        _log.debug    ('CaloFutureReco/ElectronFutureReco: Configure Charged Cluster Selector with Spd     : %s' %  alg.ChargedClusterWithSpdFuture.getFullName() )

    ## 3/ energy in Prs
    if usePrsE :  
        alg.addTool ( CaloFutureSelectClusterWithPrs        , "ClusterWithPrsFuture"  )
        alg.ClusterWithPrsFuture.MinEnergy = 10 * MeV
        alg.SelectionTools += [ alg.ClusterWithPrsFuture ]
        _log.debug    ('CaloFutureReco/ElectronFutureReco: Configure Cluster Selector with Prs    : %s' %  alg.ClusterWithPrsFuture.getFullName() )

    ## 4/  match with tracks
    if useTracksE :
        alg.addTool ( CaloFutureSelectorNOT   , "ChargedClusterFuture"  )
        clnot = alg.ChargedClusterFuture
        clnot.addTool ( CaloFutureSelectNeutralClusterWithTracks , "NotNeutralClusterFuture" )
        clnot.NotNeutralClusterFuture.MinChi2 = 25
        clnot.SelectorTools = [ clnot.NotNeutralClusterFuture ]
        alg.SelectionTools += [ alg.ChargedClusterFuture ]
        _log.debug   ('CaloFutureReco/ElectronFutureReco: Configure Charged Cluster Selector with Tracks     : %s' %  alg.ChargedClusterFuture.getFullName() )


    ## hypo tools : add Spd/Prs digits
    if not noSpdPrs :
        alg.addTool ( CaloFutureExtraDigits , 'SpdPrsExtraEFuture' )
        alg.HypoTools = [ alg.SpdPrsExtraEFuture ]
    else :
        alg.HypoTools = []

    ## correction tools : E/S/L
    alg.addTool ( CaloFutureECorrection , 'ECorrectionFuture' )
    alg.addTool ( CaloFutureSCorrection , 'SCorrectionFuture' )
    alg.addTool ( CaloFutureLCorrection , 'LCorrectionFuture' )

    ecorr = alg.ECorrectionFuture
    scorr = alg.SCorrectionFuture
    lcorr = alg.LCorrectionFuture

    # tool configuration via condDB (remove default configuration)
    alg.CorrectionTools2 = [ ecorr , scorr , lcorr ]
    
    ## update the sequence
    addAlgs ( seq , alg ) 

    # global context 
    setTheProperty ( seq , 'Context' , context )

    ## printout
    _log.debug ( 'Configure Electron Reco Seq : %s  for : %s' % (seq.name() , context ))
    if enableRecoOnDemand : 
        _log.info    ('ElectronFutureReco onDemand')

    return seq

    
# =============================================================================
## define the reconstruction of Merged Pi0s Hypos 
def mergedPi0FutureReco ( context , enableRecoOnDemand , clusterOnly = False , neutralID = True , useTracks = True,
                    mergedPi0Pt = 2* GeV ,fastReco = False, external = '',noSpdPrs=False,
                    masksE=[] , masksP=[] ) :

    """
    Define the recontruction of Merged Pi0s
    """

    from Configurables import   CaloFutureMergedPi0 # CaloFutureMergedPi0Alg is obsolete (replaced by CaloFutureMergedPi0)
    from Configurables import   CaloFutureHypoAlg 

    from Configurables import ( CaloFutureExtraDigits ,
                                CaloFutureECorrection , 
                                CaloFutureSCorrection , 
                                CaloFutureLCorrection 
                                ) 


    # build the sequences
    seq = getAlgo ( GaudiSequencer , 'MergedPi0FutureReco' , context )
    sseq = getAlgo ( GaudiSequencer , 'SplitClusterFutureReco' , context )
        
    ## Merged Pi0
    if clusterOnly :
        pi0 = getAlgo ( CaloFutureMergedPi0 , 'SplitClustersRec', context )
        pi0.CreateSplitClustersOnly = True
        sseq.Members=[pi0]
    else :
        pi0 = getAlgo ( CaloFutureMergedPi0 , 'MergedPi0RecFuture', context )        

    if masksE != [] :
        pi0.EnergyTags = masksE
    if masksP != [] :
        pi0.PositionTags = masksP

    if external  != '' :
        pi0.InputData = external

    pi0.PropertiesPrint = False
    
    pi0.EtCut    = mergedPi0Pt

    if clusterOnly :
        setTheProperty ( sseq , 'Context' , context )
        return sseq
    
    ## MergedPi0 sequence
    
    ## 2/ SplitPhotons
    #    splitg = getAlgo ( CaloFutureHypoAlg , 'PhotonFromMergedRec' , context )    
    #    splitg.HypoType = "SplitPhotons";
    #    splitg.PropertiesPrint = False   
    
    ## Add Prs/Spd digits
    if not noSpdPrs :
        pi0.addTool ( CaloFutureExtraDigits , 'SpdPrsExtraSFuture' )
        pi0.addTool ( CaloFutureExtraDigits , 'SpdPrsExtraMFuture' )
    ## Pi0 correction tools
    pi0.addTool ( CaloFutureECorrection , 'ECorrectionFuture' )
    pi0.addTool ( CaloFutureSCorrection , 'SCorrectionFuture' )
    pi0.addTool ( CaloFutureLCorrection , 'LCorrectionFuture' )

    # tool configuration via condDB only (remove default configuration)
    if not noSpdPrs :
        pi0.PhotonTools = [ pi0.SpdPrsExtraSFuture , pi0.ECorrectionFuture,pi0.SCorrectionFuture, pi0.LCorrectionFuture ]
        pi0.Pi0Tools    = [ pi0.SpdPrsExtraMFuture ]
    else :       
        pi0.PhotonTools = [ pi0.ECorrectionFuture,pi0.SCorrectionFuture, pi0.LCorrectionFuture ]

    seq.Members=[pi0]
    
    ## 3/ (PhotonFrom)MergedID
    if neutralID : 
        from CaloFuturePIDs.PIDs import (MergedID , PhotonFromMergedID )
        idSeq = getAlgo( GaudiSequencer, "MergedIDSeq",context)
        mID =  MergedID ( context,enableRecoOnDemand, useTracks )
        pmID =  PhotonFromMergedID( context, enableRecoOnDemand, useTracks )
        idSeq.Members = [ mID, pmID ]  
        addAlgs ( seq , idSeq ) 

    # propagate the context
    setTheProperty ( seq , 'Context' , context )


    ## onDemand
    if enableRecoOnDemand :
            onDemand ( 'Rec/Calo/SplitPhotons'      , seq , context ) 
            onDemand ( 'Rec/Calo/MergedPi0s'        , seq , context )
            #onDemand ( 'Rec/Calo/EcalSplitClusters' , seq , context )  ## ??

    ## printout
    _log.debug ( 'Configure MergedPi0 Reco Seq : %s  for : %s' %( seq.name() , context ))
    if enableRecoOnDemand : 
        _log.info    ('MergedPi0FutureReco onDemand')

    return seq


# =============================================================================
if '__main__' == __name__ :
    print __doc__
