// $Id: Brunel.opts,v 1.29 2010-04-29 21:13:59 odescham Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $, version $Revision: 1.29 $ 
// ============================================================================

/** @file
 *  The job-options file to run  CaloFuturerimeter Reconstruction in Brunel
 *  @author    Olivier Deschamps odescham@in2p3.fr
 *           & Vanya Belyaev     Ivan.Belyaev@cern.ch
 *  @date 2006-26-05
 */

// The main stream :
RecoCALOFUTURESeq.Members +=     { 	"GaudiSequencer/CaloFutureDigits" ,
                              //"CaloFutureGetterInit/CaloFutureDigitGetter", // NO LONGER NEEDED
                              "GaudiSequencer/CaloFutureClusters",
                              "GaudiSequencer/CaloFutureTrackMatch" ,
                              "GaudiSequencer/CaloFutureRec",
                              "GaudiSequencer/CaloFuturePIDs" };

//------ Unpack the rawBanks & create CaloFutureDigit on TES 
CaloFutureDigits.Members = {  "CaloFutureZSupAlg/FutureEcalZSup",                 
                        "CaloFutureZSupAlg/FutureHcalZSup",                  
                        "CaloFutureDigitsFromRaw/FuturePrsFromRaw",        
                        "CaloFutureDigitsFromRaw/FutureSpdFromRaw"};


//------ Clusterize, clean & compute cluster parameters 
CaloFutureClusters.Members += { "CaloFutureDigitFilterAlg/CaloFutureDigitFilter",
                          "FutureCellularAutomatonAlg/EcalClust",
                          "CaloFutureSharedCellAlg/EcalShare",
                          "CaloFutureClusterCovarianceAlg/EcalCovar"};

//------ Track-Cluster 2D matching for Neutral/Charged cluster selection

CaloFutureTrackMatch.Members +=     { "GaudiSequencer/CaloFutureAcceptance",
                                "FuturePhotonMatchAlg/ClusterMatch"   } ;

//------ Acceptance Filters
CaloFutureAcceptance.Members = { "InSpdFutureAcceptance     Alg/InSPD" ,
                           "InPrsFutureAcceptance     Alg/InPRS" ,
                           "InEcalFutureAcceptance    Alg/InECAL", 
                           "InHcalFutureAcceptance    Alg/InHCAL" ,
                           "FutureInBremFutureAcceptanceAlg/InBREM" };


//------ Merged Pi0, Single Photon & Electron reconstruction

CaloFutureRec.Members += {"CaloFutureMergedPi0Alg/MergedPi0Rec", 
                    "CaloFutureHypoAlg/PhotonFromMergedRec",
                    "CaloFutureSinglePhotonAlg/SinglePhotonRec",
                    "CaloFutureElectronAlg/ElectronRec"};


#include "$CALOFUTURERECOOPTS/ClusterSelection.opts" 
#include "$CALOFUTURERECOOPTS/MergedPi0Rec.opts"
#include "$CALOFUTURERECOOPTS/PhotonFromMergedRec.opts"
#include "$CALOFUTURERECOOPTS/SinglePhotonRec.opts" 
#include "$CALOFUTURERECOOPTS/ElectronRec.opts" 
// Configure Tools
#include "$CALOFUTURERECOOPTS/CaloFutureECorrectionParam.opts"  
#include "$CALOFUTURERECOOPTS/CaloFutureSCorrectionParam.opts"  
#include "$CALOFUTURERECOOPTS/CaloFutureLCorrectionParam.opts"

//------- Track- charged/neutral cluster 3D matching for electron/bremStrahlung evaluation

CaloFuturePIDs.Members +=  
  { 
    // 3D Matching	
		"GaudiSequencer/CaloFutureMatch"
      // Energy Deposit along track path
      ,"GaudiSequencer/CaloFutureEnergy"
      // CaloFutureID DLL's
      ,"GaudiSequencer/CaloFutureDLLs"
      // Neutral IDs
      ,"GaudiSequencer/NeutralPIDs"
  } ;

NeutralPIDs.Members += {
  "CaloFuturePhotonIdAlg/PhotonID",
    "CaloFuturePhotonIdAlg/MergedID",
    "CaloFuturePhotonIdAlg/SplitPhotonID"
    };

MergedID.Type = "MergedID";
SplitPhotonID.Type= "PhotonFromMergedID";

CaloFutureMatch.Members +=
  {
    "FutureElectronMatchAlg/ElectronMatch"
    ,"BremMatchAlg/BremMatch"        
  };
CaloFutureEnergy.Members = 
  {
    "FutureTrack2SpdEAlg/SpdE"
    ,"FutureTrack2PrsEAlg/PrsE"
    ,"FutureTrack2EcalEAlg/EcalE"
    ,"FutureTrack2HcalEAlg/HcalE"
  };
CaloFutureDLLs.Members += 
  {
    // convert tables into plain Chi2's
      "FutureEcalChi22ID"
    , "BremChi22ID"
    , "FutureClusChi22ID"
    // evaluate proper DLL
    , "FuturePrsPIDeAlg/PrsPIDe" 
    , "BremPIDeAlg/BremPIDe" 
    , "FutureEcalPIDeAlg/EcalPIDe" 
    , "FutureHcalPIDeAlg/HcalPIDe" 
    , "FutureEcalPIDmuAlg/EcalPIDmu" 
    , "FutureHcalPIDmuAlg/HcalPIDmu" 
  };


CaloFutureDigits.MeasureTime     = true ;
CaloFutureClusters.MeasureTime   = true ;
CaloFutureTrackMatch.MeasureTime = true ;
CaloFutureRec.MeasureTime        = true ;
CaloFuturePIDs.MeasureTime       = true ;
CaloFutureAcceptance.MeasureTime = true ;
CaloFutureMatch.MeasureTime     = true ;
CaloFutureEnergy.MeasureTime     = true ;
CaloFutureDLLs.MeasureTime       = true ;
