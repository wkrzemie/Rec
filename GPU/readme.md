Produce input for HLT1 on GPUs
-------------------------------

These are instructions for how to produce input for [HLT1 on GPUs](https://gitlab.cern.ch/lhcb-parallelization/cuda_hlt) 
using the nightlies with lb-dev.

After logging in to lxplus7:

    LbLogin -c x86_64-centos7-gcc7-opt
    lb-dev --nightly lhcb-head Brunel/HEAD
    cd BrunelDev_HEAD/
    git lb-use Rec
    git lb-checkout Rec/master GPU
    git lb-checkout Rec/master Pr/PrMCTools
    make
    
`GPU` contains the dumpers for raw banks, geometries and hit objects, as well as
configuration and MC file scripts.
`Pr/PrMCTools` contains the PrTrackerDumper, from which MC information can be dumped
for every MCParticle. This is used for truth matching within HLT1 on GPUs.

To dump the raw banks, geometry files and muon hit objects for SciFi raw bank version 5
minimum bias MC:

    ./run gaudirun.py GPU/BinaryDumpers/options/dump_banks.py GPU/BinaryDumpers/options/upgrade-minbias-magdown-scifi-v5-local.py

If you want Bs->PhiPhi MC instead, use `GPU/BinaryDumpers/options/upgrade-bsphiphi-magdown-scifi-v5-local.py` as input.
Similarly, for dumping the MC information, run:

    ./run gaudirun.py GPU/BinaryDumpers/options/dump_MC_info.py GPU/BinaryDumpers/options/upgrade-minbias-magdown-scifi-v5-local.py

The number of events to be dumped can be specified in `dump_MC_info.py` and `dump_banks.py` respectively.
By default, the following output directories will be created in the current directory:

* `banks`: all raw banks (VP, UT, FTCluster, Muon)
* `muon_coords`, `muon_common_hits`: muon hit ojbects
* `geometry`: geometry description for the different sub-detectors needed within HLT1 on GPUs
* `MC_info`: binary files containing MC information for all dumped MCParticles
* `TrackerDumper`: ROOT files containing MC information for all dumped MCParticles as well as all hits in every sub-detector

For changing the output location, the OutputDirectory can be set in the configuration script, for example in dump_banks.py:
`dump_banks.OutputDirectory = "/afs/cern.ch/work/d/dovombru/public/gpu_input/test/banks"`
    
For the TrackerDumper, `OutputDirectory` is the directory for the ROOT files, `MCOutputDirectory` is the directory for the binary files.

Note: The "nightlies" might not be ready until after lunch time. Instead,
the build from a day before can be used. For example
`lb-dev --nightly lhcb-head Mon Brunel/HEAD --name BrunelDev_Mon`.
    
    
    
