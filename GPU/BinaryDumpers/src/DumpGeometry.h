/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DUMPGEOMETRY_H
#define DUMPGEOMETRY_H 1

#include <cstring>
#include <fstream>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>

#include <GaudiAlg/GetData.h>
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/IUpdateManagerSvc.h>
#include <GaudiKernel/Service.h>
#include <Kernel/ICondDBInfo.h>

#include "Utils.h"

/** @class DumpGeometry
 *  Base class for a Service that dumps a subdetector's geometry
 *
 *  @author Roel Aaij
 *  @date   2018-08-27
 */
template <typename DETECTOR>
class DumpGeometry : public Service {
 public:
  DumpGeometry(std::string name, ISvcLocator* loc, std::string detLoc)
      : Service(name, loc), m_location{std::move(detLoc)} {}

  StatusCode initialize() override;

  StatusCode dump() { return dumpGeometry(); }

  inline void Assert(const bool ok, const std::string& message = "",
                     const StatusCode sc = StatusCode(StatusCode::FAILURE,
                                                      true)) const {
    if (!ok) throw GaudiException(this->name() + ":: " + message, "", sc);
  }

 protected:
  virtual StatusCode dumpGeometry() const = 0;

  std::string outputDirectory() const { return m_outputDirectory.value(); }
  std::string geometrySuffix() const;

  const DETECTOR& detector() const { return *m_det; }

  template <class TOOL>
  inline TOOL* tool(const std::string& type, bool create = true) const {
    return tool<TOOL>(type, type, create);
  }

  template <class TOOL>
  inline TOOL* tool(const std::string& type, const std::string& name,
                    bool create = true) const {
    // for empty names delegate to another method
    if (name.empty()) return tool<TOOL>(type, create);
    Assert(m_toolSvc.isValid(), "tool():: IToolSvc* points to NULL!");
    // get the tool from Tool Service
    TOOL* t = nullptr;
    const auto sc = m_toolSvc->retrieveTool(type, name, t, m_toolSvc, create);
    if (sc.isFailure()) {
      throw GaudiException(this->name() + ":: " +
                           "tool():: Could not retrieve Tool '" + type +
                           "'/'" + name + "'","", sc);
    }
    if (!t) {
      throw GaudiException(this->name() + ":: " +
                           "tool():: Could not retrieve Tool '" + type +
                           "'/'" + name + "'", "", sc);
    }
    return t;
  }

  template <class T>
  typename Gaudi::Utils::GetData<T>::return_type getDet(
      std::string location) const {
    Gaudi::Utils::GetData<T> getter{};
    auto info = getter(*this, m_detSvc, location);
    if (!info) {
      error() << "Could not obtain detector data from " << location << endmsg;
      return nullptr;
    } else {
      return info;
    }
  }

 private:
  Gaudi::Property<std::string> m_outputDirectory{this, "OutputDirectory",
                                                 "geometry"};
  std::string m_location;
  std::map<std::string, std::string> m_tags;

  SmartIF<IDataProviderSvc> m_detSvc;
  SmartIF<ICondDBInfo> m_condDBInfo;
  SmartIF<IToolSvc> m_toolSvc;

  DETECTOR* m_det;
};

template <typename DETECTOR>
StatusCode DumpGeometry<DETECTOR>::initialize() {
  if (!DumpUtils::createDirectory(m_outputDirectory.value())) {
    error() << "Failed to create directory " << m_outputDirectory.value()
            << endmsg;
    return StatusCode::FAILURE;
  }

  // Facilitate derived services getting tools
  m_toolSvc = service("ToolSvc", true);

  // Get the DB tags in use
  m_condDBInfo = service("CondDBCnvSvc", true);
  std::vector<LHCb::CondDBNameTagPair> tags;
  m_condDBInfo->defaultTags(tags);
  for (auto&& entry : tags) {
    auto r = m_tags.emplace(std::move(entry.first), std::move(entry.second));
    debug() << "tag: " << r.first->first << " " << r.first->second << endmsg;
  }

  // Get the requested detector
  m_detSvc = service("DetectorDataSvc", true);
  if (!m_detSvc.isValid()) {
    error() << "Unable to obtain detector data service." << endmsg;
    return StatusCode::FAILURE;
  }

  m_det = getDet<DETECTOR>(m_location);

  // Register our callback to trigger the actual dumping
  auto updMgrSvc = service("UpdateManagerSvc", true).as<IUpdateManagerSvc>();
  updMgrSvc->registerCondition(this, m_det->geometry(),
                               &DumpGeometry<DETECTOR>::dump);
  updMgrSvc->update(this);

  return StatusCode::SUCCESS;
}

template <typename DETECTOR>
std::string DumpGeometry<DETECTOR>::geometrySuffix() const {
  auto sit = m_tags.find("SIMCOND");
  auto dit = m_tags.find("DDDB");
  auto cit = m_tags.find("LHCBCOND");
  if (dit == end(m_tags)) {
    return "UNKOWN";
  } else if (sit != end(m_tags)) {
    return dit->second + "_" + sit->second;
  } else {
    return dit->second + "_" + cit->second;
  }
}

#endif  // DUMPGEOMETRY_H
