###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Brunel
from Configurables import (TrackSys, GaudiSequencer)
from Configurables import FTRawBankDecoder
from Configurables import NTupleSvc
from Gaudi.Configuration import appendPostConfigAction

# DDDBtag = "dddb-20171010"
# CondDBtag = "sim-20170301-vc-md100" 

DDDBtag = "dddb-20171010"
CondDBtag = "sim-20180530-vc-md100" 

Evts_to_Run = 10  # set to -1 to process all

mbrunel = Brunel(DataType="Upgrade",
                 EvtMax=Evts_to_Run,
                 PrintFreq=1,
                 WithMC=True,
                 Simulation=True,
                 OutputType="None",
                 DDDBtag=DDDBtag,
                 CondDBtag=CondDBtag,
                 MainSequence=['ProcessPhase/Reco'],
                 RecoSequence=["Decoding", "TrFast"],
                 Detectors=["VP", "UT", "FT"],
                 InputType="DIGI")

TrackSys().TrackingSequence = ["Decoding", "TrFast"]
TrackSys().TrackTypes = ["Velo", "Upstream", "Forward"]
mbrunel.MainSequence += ['ProcessPhase/MCLinks', 'ProcessPhase/Check']

FTRawBankDecoder("createFTClusters").DecodingVersion = 5

NTupleSvc().Output = ["FILE1 DATAFILE='velo_states.root' TYP='ROOT' OPT='NEW'"]


def modifySequences():
    try:
        # empty the calo sequence
        GaudiSequencer("MCLinksCaloSeq").Members = []
        GaudiSequencer("MCLinksCaloSeq").Members = []
        from Configurables import TrackResChecker
        GaudiSequencer("CheckPatSeq").Members.remove(
            TrackResChecker("TrackResCheckerFast"))
        from Configurables import VectorOfTracksFitter
        GaudiSequencer("RecoTrFastSeq").Members.remove(
            VectorOfTracksFitter("ForwardFitterAlgFast"))
        from Configurables import MuonRec
        GaudiSequencer("RecoDecodingSeq").Members.append(
            MuonRec())
    except ValueError:
        None


appendPostConfigAction(modifySequences)


def AddDumpers():
    from Configurables import PrTrackerDumper, DumpVeloUTState
    dump_mc = PrTrackerDumper("DumpMCInfo", DumpToBinary=True)
    dump_vut = DumpVeloUTState("DumpVUT")
    GaudiSequencer("MCLinksTrSeq").Members += [dump_mc, dump_vut]


appendPostConfigAction(AddDumpers)
