/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FILTERFILLINGSCHEME_H
#define FILTERFILLINGSCHEME_H 1

// Include files
// from Gaudi
#include "GaudiAlg/FilterPredicate.h"
#include "Event/ODIN.h"

/** @class FilterFillingScheme FilterFillingScheme.h
 *
 *
 *  @author Jaap Panman
 *  @date   2011-08-09
 */
class FilterFillingScheme : public Gaudi::Functional::FilterPredicate<bool(const LHCb::ODIN&)>
{
public:
  /// Standard constructor
  FilterFillingScheme( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  bool operator()(const LHCb::ODIN&) const override;  ///< Algorithm execution

private:
  StatusCode registerDB();    ///< register DB conditions
  bool processDB( long bxid ) const;///< DB checking code

  StatusCode i_cacheFillingData();    ///< Function extracting data from Condition
  StatusCode i_cacheMagnetData();     ///< Function extracting data from Condition

  Gaudi::Property<std::string> m_beam { this, "Beam", "0" };     ///< Beam looked at
  Gaudi::Property<std::string> m_MagnetState { this, "MagnetState", "NONE" };     ///< Magnet state looked at (if empty: all)
  Gaudi::Property<int> m_BXOffset { this, "BXOffset", 0 };       ///< bcid offset imspected

  // database conditions and calibration factors
  SmartIF<IDetDataSvc> m_dds;         ///< DetectorDataSvc

  Condition *m_condMagnet = nullptr;            ///< Condition for magnet
  std::string m_parMagnetState;       ///< magnet state (UP/DOWN)

  Condition *m_condFilling = nullptr; ///< Condition for LHC filling scheme
  std::string m_B1FillingScheme;      ///< filled bunches 00101010....
  std::string m_B2FillingScheme;      ///< filled bunches 00101010....

};
#endif // FILTERFILLINGSCHEME_H
