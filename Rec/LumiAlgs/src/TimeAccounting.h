/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TIMEACCOUNTING_H
#define TIMEACCOUNTING_H 1

// Include files
#include <unordered_set>
// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "Kernel/SynchronizedValue.h"

// event model
#include "Event/TimeSpanFSR.h"


/** @class TimeAccounting TimeAccounting.h
 *
 *
 *  @author Jaap Panman
 *  @date   2009-01-19
 */
class TimeAccounting : public Gaudi::Functional::Consumer<void(const LHCb::RawEvent&, const LHCb::ODIN& )>
{
public:
  /// Standard constructor
  TimeAccounting( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  void operator()(const LHCb::RawEvent&, const LHCb::ODIN& ) const override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

private:
  /// Reference to file records data service
  SmartIF<IDataProviderSvc> m_fileRecordSvc;

  Gaudi::Property<std::string> m_FSRName{ this, "OutputDataContainer", LHCb::TimeSpanFSRLocation::Default };              // output location of summary data in FSR

  LHCb::TimeSpanFSRs* m_timeSpanFSRs = nullptr; // TDS container
  LHCb::TimeSpanFSR* m_timeSpanFSR = nullptr;   // FSR for current file

  mutable LHCb::cxx::SynchronizedValue<std::unordered_set<std::string>> m_files; // file names encountered

};
#endif // TIMEACCOUNTING_H
