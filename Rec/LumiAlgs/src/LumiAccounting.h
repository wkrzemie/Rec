/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMIACCOUNTING_H
#define LUMIACCOUNTING_H 1

// Include files
#include "Kernel/SynchronizedValue.h"

// from Gaudi
#include "GaudiAlg/Consumer.h"

// for DB
#include "GaudiKernel/IDetDataSvc.h"

// event model
#include "Event/HltLumiSummary.h"
#include "Event/LumiFSR.h"


/** @class LumiAccounting LumiAccounting.h
 *
 *
 *  @author Jaap Panman
 *  @date   2009-01-19
 */
class LumiAccounting : public Gaudi::Functional::Consumer<void(const LHCb::RawEvent& event,
                                                               const LHCb::HltLumiSummary& the_hltLumiSummary,
                                                               const LHCb::ODIN& odin )>
{
public:
  /// Standard constructor
  LumiAccounting( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  void  operator()(const LHCb::RawEvent& event,
                   const LHCb::HltLumiSummary& the_hltLumiSummary,
                   const LHCb::ODIN& odin ) const override;   ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

private:
  StatusCode registerDB();    ///< register DB conditions
  StatusCode i_cacheThresholdData();  ///< Function extracting data from Condition

private:
  /// Reference to file records data service
  SmartIF<IDataProviderSvc> m_fileRecordSvc;

  LHCb::LumiFSRs* m_lumiFSRs = nullptr;     ///< TDS container
  LHCb::LumiFSR* m_lumiFSR = nullptr;       ///< FSR for current file

  mutable LHCb::cxx::SynchronizedValue<std::unordered_set<std::string>> m_files; ///< files read

  // database conditions and calibration factors
  SmartIF<IDetDataSvc> m_dds;                   ///< DetectorDataSvc
  Condition *m_condThresholds = nullptr;                  ///< Condition for relative calibration
  std::vector<double> m_calibThresholds;        ///< relative calibration factors
  int m_statusThresholds = 0;                       ///<

  // it is assumed that we are only called for a single BXType and that the
  // output data container gets this name
  Gaudi::Property<std::string> m_FSRName { this, "OutputDataContainer", LHCb::LumiFSRLocation::Default }; ///< output location of summary data in FSR

};
#endif // LUMIACCOUNTING_H
