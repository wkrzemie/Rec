/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// event model
#include "Event/HltLumiSummary.h"
#include "Event/LumiCounters.h"
#include "Event/LumiMethods.h"

// local
#include "FilterOnLumiSummary.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FilterOnLumiSummary
//
// 2010-01-29 : Jaap Panman
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FilterOnLumiSummary )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FilterOnLumiSummary::FilterOnLumiSummary( const std::string& nam,
                                          ISvcLocator* pSvcLocator)
: FilterPredicate ( nam , pSvcLocator,
                    { KeyValue{ "InputDataContainer", LHCb::HltLumiSummaryLocation::Default } } )
{ }

//=============================================================================
// Initialization
//=============================================================================
StatusCode FilterOnLumiSummary::initialize()
{
  StatusCode sc = FilterPredicate::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( msgLevel(MSG::DEBUG) ) {
    debug() << "==> Initialize" << endmsg;
    debug() << "CounterName:       " << m_CounterName.value() << " "
            << "ValueName:         " << m_ValueName.value()   << endmsg;
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
bool FilterOnLumiSummary::operator()(const LHCb::HltLumiSummary& summary) const
{
  // look at the specified value
  return summary.info(m_Counter,-1) == m_Value;
}
