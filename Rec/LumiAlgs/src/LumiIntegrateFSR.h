/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMIINTEGRATEFSR_H
#define LUMIINTEGRATEFSR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// for DB
#include "GaudiKernel/IDetDataSvc.h"
#include "Kernel/IAccept.h"

// for TCK
#include "Kernel/IPropertyConfigSvc.h"

// event model
#include "Event/LumiFSR.h"
#include "Event/EventCountFSR.h"
#include "Event/TimeSpanFSR.h"
#include "Event/LumiIntegral.h"

// local
#include "GetLumiParameters.h"
#include "LumiIntegrator.h"

/** @class LumiIntegrateFSR LumiIntegrateFSR.h
 *
 *
 *  @author Jaap Panman
 *  @date   2009-02-27
 */
class LumiIntegrateFSR : public GaudiAlgorithm {

public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode stop() override;    ///< Algorithm stopping

protected:
  virtual StatusCode add_file();            ///< add the FSRs of one input file
  virtual StatusCode add_fsr(LHCb::LumiIntegral* r,
			     const std::string& addr,
			     float factor, unsigned int key);           ///< add/subtr one FSR for consistent group
  LHCb::TimeSpanFSR* trigger_event(const std::string& addr, unsigned int key); ///< trigger database update
  virtual void add_to_xml();          ///< add counters to xmlfile at Algorithm finalization

protected:
  /// Reference to run records data service
  IDataProviderSvc* m_fileRecordSvc = nullptr;    ///< file record service
  SmartIF<IDetDataSvc> m_dds;                     ///< DetectorDataSvc
  ILumiIntegrator *m_integratorTool = nullptr;    ///< tool to integrate luminosity
  ILumiIntegrator *m_rawIntegratorTool = nullptr; ///< tool to integrate luminosity
  IGetLumiParameters *m_databaseTool = nullptr;   ///< tool to query luminosity database
  IAccept *m_acceptTool = nullptr;                ///< Pointer to the IAccept tool

  std::vector<std::string> m_BXTypes;           ///< list of bunch crossing types
  int m_MuKey;                                  ///< int value of key for mu calculation

  // expect the data to be written at LHCb::LumiFSRLocation::Default
  Gaudi::Property<std::string> m_FileRecordName    { this, "FileRecordLocation", "/FileRecords" };  ///< location of FileRecords
  Gaudi::Property<std::string> m_FSRName           { this, "FSRName",            "/LumiFSR" };      ///< specific tag of summary data in FSR
  Gaudi::Property<std::string> m_EventCountFSRName { this, "EventCountFSRName", "/EventCountFSR"};  ///< specific tag of event summary data in FSR
  Gaudi::Property<std::string> m_TimeSpanFSRName   { this, "TimeSpanFSRName",   "/TimeSpanFSR"};    ///< specific tag of event summary data in FSR
  Gaudi::Property<std::string> m_ToolName          { this, "IntegratorToolName", "LumiIntegrator" };///< name of tool for normalization
  Gaudi::Property<std::string> m_RawToolName       { this, "RawIntegratorToolName", "RawLumiIntegrator" };          ///< name of tool for raw numbers
  Gaudi::Property<std::string> m_acceptToolName    { this, "AcceptTool",        "DQAcceptTool" };   ///< Name of the (public) IAccept data quality tool
  Gaudi::Property<std::string> m_PrimaryBXType     { this, "PrimaryBXType",     "BeamCrossing" };   ///< BXType to normalize
  Gaudi::Property<std::vector<std::string>> m_addBXTypes { this, "AddBXTypes" } ;                   ///< list of bunch crossing types to be added
  Gaudi::Property<std::vector<std::string>> m_subtractBXTypes { this, "SubtractBXTypes" } ;         ///< list of bunch crossing types to be subtracted
  Gaudi::Property<bool> m_accumulateMu             { this, "AccumulateMu",      false};             ///< flag to accumulate mu information
  Gaudi::Property<std::string> m_muKeyName         { this, "MuKeyName",         "PoissonRZVelo" };  ///< name of key for mu calculation
  Gaudi::Property<bool> m_ignoreDQFlags            { this, "IgnoreDQFlags",     false};             ///< flag to ignore data quality

private:
  // database calibration factors
  std::vector<double> m_calibRelative;          ///< relative calibration factors
  std::vector<double> m_calibCoefficients;      ///< usage factors
  std::vector<double> m_calibRelativeLog;       ///< relative calibration factors
  std::vector<double> m_calibCoefficientsLog;   ///< usage factors
  double m_statusScale;                         ///< absolute scale  set to zero if no lumi
  double m_calibScale;                          ///< absolute scale
  double m_calibScaleError;                     ///< absolute scale error
  double m_calibRevolutionFrequency;            ///< revolution frequency (Hz)
  double m_calibRandomFrequencyBB;              ///< random lumi event frequency of BB crossings (Hz)
  int    m_calibCollidingBunches;               ///< number of colliding bunches
  bool   m_DQaccepted;                          ///< DQ flag accepted

};
#endif // LUMIINTEGRATEFSR_H
