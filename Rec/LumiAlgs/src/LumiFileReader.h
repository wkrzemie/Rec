/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMIFILEREADER_H
#define LUMIFILEREADER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

#include "Event/RawEvent.h"
#include "Event/ODIN.h"
#include "Event/HltLumiSummary.h"
#include "Event/FileId.h"

// forward declarations
namespace LHCb {
  class RawBank ;
}


/** @class LumiFileReader LumiFileReader.h
 *
 *
 *  @author Jaap Panman
 *  @date   2009-10-06
 */
class LumiFileReader : public GaudiAlgorithm {
public:
  /// Standard constructor
  LumiFileReader( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

private:
  // data
  Gaudi::Property<std::string> m_rawEventLocation {this, "RawEventLocation", LHCb::RawEventLocation::Default };          ///< Location where we get the RawEvent
  Gaudi::Property<std::string> m_OutputFileName {this, "OutputFileName", "lumi.dat"};

  // Statistics
  double m_totDataSize = 0;
  int m_nbEvents = 0;
  LHCb::FileId m_fileId;

};
#endif // FILEREADER_H
