/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMICHECKCONDDB_H
#define LUMICHECKCONDDB_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// for incidents listener
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

// for DB
#include "GaudiKernel/IDetDataSvc.h"
#include "GetLumiParameters.h"

// for TCK
#include "Kernel/IPropertyConfigSvc.h"

/** @class LumiCheckCondDB LumiCheckCondDB.h
 *
 *  @author Jaap Panman
 *  @date   2010-10-20
 */

class LumiCheckCondDB final : public extends<GaudiAlgorithm, IIncidentListener>
{

public:

  /// Standard constructor
  using base_class::base_class;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode stop() override;    ///< Algorithm stopping
  StatusCode finalize() override;    ///< Algorithm finalization

  // ==========================================================================
  // IIncindentListener interface
  // ==========================================================================
  void handle ( const Incident& ) override;
  // ==========================================================================

private:

  void checkDB( const std::string& state ) const; ///< DB checking code - event loop

private:
  // expect the data to be written at LHCb::LumiFSRLocation::Default

  Gaudi::Property<unsigned long long> m_startTime { this, "StartTime", 1269817249 };    ///< start probing database here
  Gaudi::Property<unsigned long long> m_numberSteps { this, "NumberSteps", 80 };            ///< number of steps checked
  Gaudi::Property<unsigned long long> m_stepHours { this, "StepHours", 72 };            ///< number of hours per step

  // database conditions and calibration factors
  IGetLumiParameters   * m_dbTool = nullptr; ///< tool to query luminosity database
  IIncidentSvc         * m_incSvc = nullptr; ///< the incident service

  /// Reference to run records data service
  SmartIF<IDetDataSvc> m_dds = nullptr;

};
#endif // LUMICHECKCONDDB_H
