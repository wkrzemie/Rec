#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Place holder for testing the FSR info from an MDF
# At the moment I don't know quite what to write :S
# Just tests the Configurable, that's all
#
import os
from Gaudi.Configuration import *
import Configurables as Configs
from Configurables import LHCbApp, GaudiSequencer

#--- determine application to run
from LumiAlgs.Configuration import *

lSeq=GaudiSequencer("LumiSequence")

LumiAlgsConf().LumiSequencer=lSeq
LumiAlgsConf().OutputLevel=1

LHCbApp().EvtMax = 100

ApplicationMgr().TopAlg+=[lSeq]
