#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Minimal file for running LumiReader from python prompt
# Syntax is:
#   gaudirun.py ../job/LumiReader.py
# or just
#   ../job/LumiReader.py
#
import os
from Gaudi.Configuration import *
import Configurables as Configs
from Configurables import LHCbApp

#--- determine application to run
from LumiAlgs.LumiReaderConf import LumiReaderConf as LumiReader

#-- File catalogs. First one is read-write
FileCatalog().Catalogs = [ "xmlcatalog_file:MyCatalog.xml",
                           ]
# input file
files = [ "file:testout.mdf"]
ofilename='testout.dat'

LumiReader().inputFiles = files
LumiReader().outputFile =  ofilename
LumiReader().EvtMax =  20
LumiReader().Debug =  True 
LumiReader().OutputLevel =  DEBUG

from Configurables import RawEventDump
RawEventDump("InputDump").RawEventLocations=["DAQ/RawEvent"]

EventSelector().PrintFreq = 1
