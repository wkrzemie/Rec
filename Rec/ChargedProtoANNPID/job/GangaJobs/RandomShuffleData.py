#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import glob, os, shutil, sys, random

from ROOT import TFile, TTree, ROOT

ROOT.EnableImplicitMT()

def splitFile(fName,treeDir,treeName):

    # Open the input file and load the tree
    f = TFile.Open(fName,"READ")
    t = f.Get( treeDir+"/"+treeName )

    # Get number of entries in the file
    nEntries = t.GetEntries()
    
    # chuck size
    nChuck = 5000

    # list of split files
    splitFiles = [ ]

    # Out dir for this file
    fDir = fName+".d"
    if not os.path.exists(fDir) : os.makedirs(fDir)

    newFile = None
    newTree = None

    iChunk   = 0
    nInChuck = 0

    print "Splitting", fName, "into", nEntries/nChuck, "chunks"
    
    # Loop over the tree entries
    for i in range(0,nEntries-1) :
        
        # Do we need a new output file ?
        if newFile == None :
            # Split file name
            newFileName = fDir+"/"+str(iChunk).zfill(10)+".root"
            #print " ->", newFileName 
            # open the out file with same compression settings as input
            newFile = TFile.Open(newFileName,"RECREATE")
            newFile.SetCompressionSettings( f.GetCompressionSettings() )
            newFile.cd()
            # Make directory in the new file
            newFile.mkdir(treeDir).cd()
            # Clone the tree
            newTree = t.CloneTree(0)
            # add split file to list
            splitFiles += [newFileName]

        # load the entry
        t.GetEntry(i)

        # Save to output
        newTree.Fill()

        # count number saved to this file
        nInChuck += 1

        # New file next time ?
        if nInChuck >= nChuck :
            newTree.Write()
            newFile.Close()
            newFile  = None
            newTree  = None
            iChunk  += 1
            nInChuck = 0

    # Write the last file and tree
    if newFile != None :
        newTree.Write()
        newFile.Close()

    # Close the input file
    f.Close()

    # return the split list
    return splitFiles

# find input files
inDir = "/r02/lhcb/jonesc/ANNPID/ProtoParticlePIDtuples/MC/private"
inFiles = glob.glob( inDir+"/*/*.root" )

outDir = inDir+"/shuffled"
if os.path.exists(outDir) : shutil.rmtree(outDir)
os.makedirs(outDir)

treeDir  = "ANNPID"
treeName = "DecayTree"

# Split the files into small chunks
splits = [ ]
for inFile in inFiles : splits += splitFile(inFile,treeDir,treeName)
# testing just use one file...
#splits = splitFile(inFiles[0],treeDir,treeName)

# random sort the splits
random.shuffle(splits)

# Now put the files back together ...

# merge data
mergeFiles = { }

# target file size
#maxsize = 500 # MB
maxsize = 75 # MB

# group files to merge
splitid   = 0
splitsize = 0
for infile in splits :
    # file size in MB
    fsize = os.path.getsize(infile) / 1e6
    # Increment the split size with this file 
    splitsize = splitsize + fsize
    # If too big, increment split id to the next file and reset split size
    if splitsize > maxsize :
        splitid = splitid + 1
        splitsize = 0
    # Form output file name based on split id
    outfile = outDir + "/ANNPID." + str(splitid).zfill(8) + ".root"
    # Add to output dict the given input to the split output file
    if not outfile in mergeFiles.keys() : mergeFiles[outfile] = [ ]
    mergeFiles[outfile] += [infile]

# run the merge commands
for outfile,infiles in mergeFiles.iteritems() :
    #print "Merging", infiles, "to", outfile
    if os.path.exists(outfile) : os.remove(outfile)
    command = "hadd -ff " + outfile
    for infile in infiles : command += " "+infile
    #print command
    os.system(command)
    # clean up
    for infile in infiles :
        dir = os.path.dirname(infile)
        os.remove(infile)
        if os.listdir(dir) == [] : os.rmdir(dir)

