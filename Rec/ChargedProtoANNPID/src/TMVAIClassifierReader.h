/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

namespace ANNGlobalPID
{

#ifndef IClassifierReader__def
#define IClassifierReader__def

  /** @class IClassifierReader TMVAImpFactory.h
   *
   *  Interface class for TMVA C++ implementation of MVAs
   */
  class IClassifierReader
  {
  public:
    /// Destructor
    virtual ~IClassifierReader() = default;
    /// return classifier response
    virtual double GetMvaValue( const std::vector<double>& inputValues ) const = 0;
    /// returns classifier status
    bool IsStatusClean() const { return fStatusIsClean; }
  protected:
    bool fStatusIsClean{true}; ///< Status flag
  };
#endif
  
}
