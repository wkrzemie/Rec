/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <unordered_map>
#include <memory>

// base class
#include "ChargedProtoANNPIDToolBase.h"

// interfaces
#include "RecInterfaces/IChargedProtoANNPIDTool.h"

// Event
#include "Event/MCParticle.h"

namespace ANNGlobalPID
{

  /** @class ChargedProtoANNPIDTool ChargedProtoANNPIDTool.h
   *
   *  Tool to provide the ANN PID variable for a given object
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2014-06-27
   */

  class ChargedProtoANNPIDTool final : public ChargedProtoANNPIDToolBase,
                                       virtual public IChargedProtoANNPIDTool
  {

  public:

    /// Standard constructor
    ChargedProtoANNPIDTool( const std::string& type,
                            const std::string& name,
                            const IInterface* parent ) ;

    /// Destructor
    virtual ~ChargedProtoANNPIDTool( ) = default;

  public:

    // Compute the ANNPID value
    IChargedProtoANNPIDTool::RetType
    annPID( const LHCb::ProtoParticle * proto,
    const LHCb::ParticleID& pid,
    const std::string& annPIDTune ) const override;

  private:

    /// Access the NetConfig object for a given configuration
    const NetConfig * getANN( const std::string & trackType,
                              const std::string & pidType,
                              const std::string & netVersion ) const;

  private:

    /// Mapping type to link confgurations to NetConfig instances
    typedef std::unordered_map< std::string, std::unique_ptr<NetConfig> > Networks;

    /// Networks for a given ANNPID configuration
    mutable Networks m_annNets;

    /// Suppress all printout from the ANN experts
    bool m_suppressANNPrintout;

  };

}
