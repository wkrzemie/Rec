/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef yPIDIClassifierReader__def
#define yPIDIClassifierReader__def


/** @class yPIDIClassifierReader yPIDImpFactory.h
 *
 *  Interface class for all yPID solutions
 */
class yPIDIClassifierReader
{
public:
  /// Destructor
  virtual ~yPIDIClassifierReader() = default;
  /// return classifier response
  virtual double GetMvaValue( const std::vector<double>& inputValues ) const = 0;
  /// returns classifier status
  bool IsStatusClean() const { return fStatusIsClean; }
protected:
  bool fStatusIsClean{true}; ///< Status flag
};

#endif
