/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <cmath>

#include "preprocessing.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void IronTransformer::read_tranform(string fimename) {
	
	unsigned int i_line;
	string line;
	
	// Read file with iron transformer
	ifstream myfile;
	myfile.open (fimename);
	if (myfile.is_open()) {
		
		// Go through the lines
		i_line = 0;
		while ( getline (myfile,line) ){
			
			// Read feature names
			if( i_line == 0 ){
				feature_names.push_back(line);
			}
			
			// Read feature values
			if(i_line == 1){
				std::vector<double> vals;
				std::istringstream iss(line);
				double num;
				while(iss >> num){
					vals.push_back(num);
				}
				feature_values.push_back(vals);
			}
			
			// Read feature percentiles
			if(i_line == 2){
				std::vector<double> vals;
				std::stringstream iss(line);
				double num;
				while(iss >> num){
					vals.push_back(num);
				}
				feature_percentiles.push_back(vals);
			}
			
			// Recalculate row indexes
			if(i_line == 3){
				i_line = 0;
			}
			else {
				i_line = i_line + 1;
			}
			
		}
		
		// Close iron transformer file
		myfile.close();
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> IronTransformer::transform(std::vector<double> X, std::vector<string> names) {
	
	unsigned int i_v, i_name, if_name;
	double val1, val2, per1, per2, k, b;
	string aname, af_name;
	std::vector<double> new_X, af_values, af_percentiles;
	
	for (i_name=0; i_name < names.size(); i_name = i_name + 1) {
		
		// Find feature id in iron transformer
		for (if_name=0; if_name < feature_names.size(); if_name = if_name + 1) {
			aname = names[i_name];
			af_name = feature_names[if_name];
			if ( strcmp (aname.c_str(), af_name.c_str()) == 0 ) {
				break;
			}
		}
		
		// Transform value
		double ax, new_ax=0;
		ax = X[i_name];
		af_values = feature_values[if_name];
		af_percentiles = feature_percentiles[if_name];
		
		for (i_v=0; i_v < af_values.size(); i_v = i_v + 1) {
			
			if ( (i_v == 0) && (ax <= af_values[i_v]) ) {
				new_ax = af_percentiles[i_v];
				break;
			}
			
			if ( (i_v == af_values.size() - 1) && (ax >= af_values[i_v]) ) {
				new_ax = af_percentiles[i_v];
				break;
			}
			
			if ( ax < af_values[i_v] ) {
				
				val1 = af_values[i_v - 1];
				val2 = af_values[i_v];
				per1 = af_percentiles[i_v - 1];
				per2 = af_percentiles[i_v];
				
				k = (per2 - per1) / (val2 - val1);
				b = per1 - k * val1;
				
				new_ax = k * ax + b;
				break;
			}
			
		}
		
		new_ax = 2 * new_ax - 1;
		new_X.push_back(new_ax);
	}
	
	return new_X;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> select_features(std::vector<double> X, std::vector<string> names, std::vector<string> sel_names) {
	
	std::vector<double> new_X;
	double x;
	unsigned int isel_name, i_name;
	string aname, asel_name;
	
	for (isel_name=0; isel_name < sel_names.size(); isel_name = isel_name + 1) {
		asel_name = sel_names[isel_name];
		
		// Find proper feature id
		for (i_name=0; i_name < names.size(); i_name = i_name + 1) {
			aname = names[i_name];
			if ( strcmp (aname.c_str(), asel_name.c_str()) == 0 ) {
				x = X[i_name];
				new_X.push_back(x);
				break;
			}
		}
	}
	return new_X;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> convert_DLL_to_LL(std::vector<double> X) {
	
	unsigned int i;
	double max;
	
	// Find max value
	max = X[0];
	for (i=0; i < X.size(); i = i + 1) {
		if (X[i] > max) max = X[i];
	}
	
	// Subtract max value
	for (i=0; i < X.size(); i = i + 1)  {
		X[i] = X[i] - max;
	}
	
	// Take exp
	for (i=0; i < X.size(); i = i + 1)  {
		X[i] = exp(X[i]);
	}
	
	// Calculate sum
	double sum = 0;
	for (i=0; i < X.size(); i = i + 1)  {
		sum = sum + X[i];
	}
	
	// Divide on the sum
	for (i=0; i < X.size(); i = i + 1)  {
		X[i] = X[i]  / sum;
	}
	
	// Take log
	for (i=0; i < X.size(); i = i + 1)  {
		if (X[i] <= 0.000001) X[i] = 0.000001;
		if (X[i] >= 10) X[i] = 10;
		X[i] = log(X[i]);
	}
	
	return X;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<double> compute_cum_sum(std::vector<double> X) {
	
	unsigned int i;
	for (i=0; i < X.size(); i = i + 1)  {
		if (i == 0) continue;
		X[i] = X[i] + X[i-1];
	}
	
	return X;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double sum_features(std::vector<double> X, std::vector<string> names, std::vector<string> sel_names) {
	
	double new_x=0;
	unsigned int i;
	vector<double> X_sel; 
	
	X_sel = select_features(X, names, sel_names);
	for (i=0; i < X_sel.size(); i = i + 1) {
		new_x = new_x + X_sel[i];
	}
	
	return new_x;
	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void add_constructed_features(std::vector<double> &X, std::vector<string> &names) {
	
	// Define columns
	const char* f_dll[] = {"CombDLLmu", "CombDLLpi", "CombDLLp", "CombDLLe", "CombDLLk"};
	std::vector<std::string> features_DLL(f_dll, f_dll + 5);
	
	const char* f_dll_ll[] = {"CombDLLmu_LL", "CombDLLpi_LL", "CombDLLp_LL", "CombDLLe_LL", "CombDLLk_LL"};
	std::vector<std::string> features_DLL_LL(f_dll_ll, f_dll_ll + 5);
	
	const char* f_rich_dll[] = {"RichDLLpi", "RichDLLe", "RichDLLp", "RichDLLmu", "RichDLLk"};
	std::vector<std::string> features_RICH_DLL(f_rich_dll, f_rich_dll + 5);
	
	const char* f_rich_dll_ll[] = {"RichDLLpi_LL", "RichDLLe_LL", "RichDLLp_LL", "RichDLLmu_LL", "RichDLLk_LL"};
	std::vector<std::string> features_RICH_DLL_LL(f_rich_dll_ll, f_rich_dll_ll + 5);
	
	const char* f_acc[] = {"InAccSpd", "InAccPrs", "InAccBrem", "InAccEcal", "InAccHcal", "InAccMuon"};
	std::vector<std::string> features_acc(f_acc, f_acc + 6);
	
	const char* f_acc_n_full[] = {"acc_cum_sum_0", "acc_cum_sum_1", "acc_cum_sum_2", "acc_cum_sum_3", "acc_cum_sum_4", "acc_cum_sum_5"};
	std::vector<std::string> features_acc_n_full(f_acc_n_full, f_acc_n_full + 6);
	
	const char* f_acc_n[] = {"acc_cum_sum_3", "acc_cum_sum_5"};
	std::vector<std::string> features_acc_n(f_acc_n, f_acc_n + 2);
	
	// Do preprocessing
	std::vector<double> X_DLL;
	X_DLL = select_features(X, names, features_DLL);
	X_DLL = convert_DLL_to_LL(X_DLL);
	
	std::vector<double> X_RICH_DLL;
	X_RICH_DLL = select_features(X, names, features_RICH_DLL);
	X_RICH_DLL = convert_DLL_to_LL(X_RICH_DLL);
	
	std::vector<double> X_acc_full, X_acc;
	X_acc_full = select_features(X, names, features_acc);
	X_acc_full = compute_cum_sum(X_acc_full);
	X_acc.push_back(X_acc_full[3]);
	X_acc.push_back(X_acc_full[5]);
	
	// Concatenate results
	X.insert(X.end(), X_RICH_DLL.begin(), X_RICH_DLL.end());
	names.insert(names.end(), features_RICH_DLL_LL.begin(), features_RICH_DLL_LL.end());
	
	X.insert(X.end(), X_DLL.begin(), X_DLL.end());
	names.insert(names.end(), features_DLL_LL.begin(), features_DLL_LL.end());
	
	X.insert(X.end(), X_acc.begin(), X_acc.end());
	names.insert(names.end(), features_acc_n.begin(), features_acc_n.end());
	
	// Add more new features
	const char* f1[] = {"RichAbovePiThres", "RichAboveKaThres", "RichAboveElThres", "RichAboveMuThres"};
	std::vector<std::string> vf1(f1, f1 + 4);
	double new_x1 = sum_features(X, names, vf1);
	string new_f1 = "RichAboveSumPiKaElMuTHres";
	X.push_back(new_x1);
	names.push_back(new_f1);
	
	const char* f2[] = {"RichAboveKaThres", "RichAbovePrThres"};
	std::vector<std::string> vf2(f2, f2 + 2);
	double new_x2 = sum_features(X, names, vf2);
	string new_f2 = "RichAboveSumKaPrTHres";
	X.push_back(new_x2);
	names.push_back(new_f2);
	
	const char* f3[] = {"RichUsedR1Gas", "RichUsedR2Gas"};
	std::vector<std::string> vf3(f3, f3 + 2);
	double new_x3 = sum_features(X, names, vf3);
	string new_f3 = "RichUsedGas";
	X.push_back(new_x3);
	names.push_back(new_f3);
	
	const char* f4[] = {"CaloNeutralSpd", "InAccSpd"};
	std::vector<std::string> vf4(f4, f4 + 2);
	double new_x4 = sum_features(X, names, vf4);
	string new_f4 = "SpdCaloNeutralAcc";
	X.push_back(new_x4);
	names.push_back(new_f4);
	
	const char* f5[] = {"CaloChargedSpd", "InAccSpd"};
	std::vector<std::string> vf5(f5, f5 + 2);
	double new_x5 = sum_features(X, names, vf5);
	string new_f5 = "SpdCaloChargedAcc";
	X.push_back(new_x5);
	names.push_back(new_f5);
	
	const char* f6[] = {"CaloChargedSpd", "CaloNeutralSpd"};
	std::vector<std::string> vf6(f6, f6 + 2);
	double new_x6 = sum_features(X, names, vf6);
	string new_f6 = "SpdCaloChargedNeutral";
	X.push_back(new_x6);
	names.push_back(new_f6);
	
	const char* f7[] = {"CaloSpdE", "CaloPrsE"};
	std::vector<std::string> vf7(f7, f7 + 2);
	double new_x7 = sum_features(X, names, vf7);
	string new_f7 = "CaloSumSpdPrsE";
	X.push_back(new_x7);
	names.push_back(new_f7);
	
	const char* f8[] = {"EcalPIDmu", "HcalPIDmu"};
	std::vector<std::string> vf8(f8, f8 + 2);
	double new_x8 = sum_features(X, names, vf8);
	string new_f8 = "CaloSumPIDmu";
	X.push_back(new_x8);
	names.push_back(new_f8);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void preprocess(std::vector<double> &X, std::vector<string> &names, IronTransformer iron) {
	
	std::vector<double> new_X;
	add_constructed_features(X, names);
	new_X = iron.transform(X, names);
	X = new_X;
	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////