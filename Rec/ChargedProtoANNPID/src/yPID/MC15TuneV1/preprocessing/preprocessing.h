/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
using namespace std;


#ifndef PREPROCESSING_H
#define PREPROCESSING_H
// Transformer
class IronTransformer {
	
	public:
	
	std::vector<string> feature_names;
	std::vector< std::vector<double> > feature_values;
	std::vector< std::vector<double> > feature_percentiles;
	
	void read_tranform(string);
	std::vector<double> transform(std::vector<double>, std::vector<string>);
};

// Helping functions
std::vector<double> select_features(std::vector<double>, std::vector<string>, std::vector<string>);
std::vector<double> convert_DLL_to_LL(std::vector<double>);
std::vector<double> compute_cum_sum(std::vector<double>);
double sum_features(std::vector<double>, std::vector<string>, std::vector<string>);
void add_constructed_features(std::vector<double> &, std::vector<string> &);

// The main function
void preprocess(std::vector<double> &, std::vector<string> &, IronTransformer);

#endif