#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#  The set of basic objects from LoKiTrack library
#
#        This file is a part of LoKi project - 
#    "C++ ToolKit  for Smart and Friendly Physics Analysis"
#
#  The package has been designed with the kind help from
#  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas, 
#  contributions and advices from G.Raven, J.van Tilburg,
#  A.Golutvin, P.Koppenburg have been used in the design.
#
#  @author Vanya BELYAEV, Sascha Stahl
# =============================================================================
"""
The set of basic objects from LoKiTrack library

      This file is a part of LoKi project - 
``C++ ToolKit  for Smart and Friendly Physics Analysis''

The package has been designed with the kind help from
Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas, 
contributions and advices from G.Raven, J.van Tilburg, 
A.Golutvin, P.Koppenburg have been used in the design.
"""
__author__  = "Vanya BELYAEV, Sascha Stahl"
# =============================================================================

from   LoKiTrack_v2.functions import *
from   LoKiCore.functions   import equal_to

_name = __name__


# =============================================================================
## make the decoration of all objects fomr this module
def _decorate ( name = _name  ) :
    """
    Make the decoration of all objects from this module
    """
    import LoKiCore.decorators as _LoKiCore
    
    ## regular functors which accept the track
    _t  =  LHCb.Event.v2.Track
    _pt = 'LHCb::Event::v2::Track const*'
    
    # "function" : Track -> double 
    
    _decorated  = _LoKiCore.getAndDecorateFunctions ( 
        name                                   , ## module name  
        TrFunc                                 , ## the base
        LoKi.Dicts.FunCalls (_t)               , ## call-traits
        LoKi.Dicts.FuncOps  (_pt,_pt)          ) ## operators&operations
    
    # "predicate/cut" :  Track -> bool
    
    _decorated |= _LoKiCore.getAndDecoratePredicates ( 
        name                                   , ## module name  
        TrCuts                                 , ## the base
        LoKi.Dicts.CutCalls (_t)               , ## call-traits
        LoKi.Dicts.CutsOps  (_pt,_pt)          ) ## operators&operations

    # ## functional stuff
    # _vt = 'std::vector<LHCb::Event::v2 Track const>'      ## std.vector ( )    
    # _vd = 'std::vector<double>'                  ## std.vector ( 'double' )
    
    # # "map" :  vector<T> -> vector<double>
    # _decorated |= _LoKiCore.getAndDecorateMaps ( 
    #     name                                   , ## module name  
    #     LoKi.Functor (_vt,_vd)                 , ## the base
    #     LoKi.Dicts.MapsOps(_pt)                ) ## call-traits
    
    # # "pipe" : vector<T> -> vector<T>    
    # _decorated |= _LoKiCore.getAndDecoratePipes ( 
    #     name                                   , ## module name  
    #     LoKi.Functor   (_vt,_vt)               , ## the base
    #     LoKi.Dicts.PipeOps(_pt,_pt)            ) ## call-traits
  
    # # "funval" : vector<T> -> double    
    # _decorated |= _LoKiCore.getAndDecorateFunVals ( 
    #     name                                   , ## module name  
    #     LoKi.Functor   (_vt,'double')          , ## the base
    #     LoKi.Dicts.FunValOps(_pt)              ) ## call-traits

    # # "cutval" : vector<T> -> bool    
    # _decorated |= _LoKiCore.getAndDecorateCutVals ( 
    #     name                                   , ## module name  
    #     LoKi.Functor   (_vt,bool)              , ## the base
    #     LoKi.Dicts.CutValOps(_pt)              ) ## call-traits
    
    # # "source" : void -> vector<T>    
    # _decorated |= _LoKiCore.getAndDecorateSources ( 
    #     name                                   , ## module name  
    #     LoKi.Functor   ('void',_vt)            , ## the base
    #     LoKi.Dicts.SourceOps(_pt,_pt)          ) ## call-traits

    # ## smart info:    
    # _decorated |= _LoKiCore.getAndDecorateInfos      (
    #     name                                            , ## module name
    #     TrFunc                                          , ## the base 
    #     LoKi.Dicts.InfoOps (_pt)                        ) ## methods
    # ##


    ## primitive voids:
    
    _decorated  |= _LoKiCore.getAndDecoratePrimitiveVoids ( name ) 
    
    return _decorated                            ## RETURN


# =============================================================================
## perform the decoration 
_decorated = _decorate ()                         ## ATTENTION 
# =============================================================================

# ## @see LoKi::Cuts::TrDOWNSTREAM
# TrDOWNSTREAM  = equal_to ( TrTYPE , LHCb.Track.Downstream  )
# ## @see LoKi::Cuts::TrLONG
# TrLONG        = equal_to ( TrTYPE , LHCb.Track.Long        ) 
# ## @see LoKi::Cuts::TrMUON
# TrMUON        = equal_to ( TrTYPE , LHCb.Track.Muon        ) 
# ## @see LoKi::Cuts::TrTTRACK
# TrTTRACK      = equal_to ( TrTYPE , LHCb.Track.Ttrack      ) 
# ## @see LoKi::Cuts::TrUNKNOWN
# TrUNKNOWN     = equal_to ( TrTYPE , LHCb.Track.TypeUnknown ) 
# ## @see LoKi::Cuts::TrUPSTREAM
# TrUPSTREAM    = equal_to ( TrTYPE , LHCb.Track.Upstream    ) 
# ## @see LoKi::Cuts::TrVELO
# TrVELO        = equal_to ( TrTYPE , LHCb.Track.Velo        ) 
# ## @see LoKi::Cuts::TrVELOR
# TrVELOR       = equal_to ( TrTYPE , LHCb.Track.VeloR       ) 


# =============================================================================
if '__main__' == __name__ :
    
    print '*'*120
    print                      __doc__
    print ' Author  : %s ' %   __author__    
    print ' Number of properly decorated types: %s'%len(_decorated)
    print '*'*120
    


# =============================================================================
# The END 
# =============================================================================
