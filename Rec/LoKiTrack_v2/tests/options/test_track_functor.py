###############################################################################
# (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file Small script to test instantiation of functors for LHCb::Event::v2::Track class
#  @author Sascha Stahl
##
# =============================================================================

from Configurables import ApplicationMgr, LHCbApp, HiveWhiteBoard, EventDataSvc, CondDB, EventSelector
app = LHCbApp()

from Configurables import LoKi__VoidFilter, Pr__SelectTracks
filter_long = LoKi__VoidFilter("FilterLong",
        Code = "SIZE('Rec/Track/Long')>5")
filter_pvs = LoKi__VoidFilter("FilterPVs",
        Code = "SIZE('Rec/Vertex/Primary')>5")

select_long = Pr__SelectTracks("SelectLong")
select_long.Input = "Rec/Track/Best"
select_long.Output = "Rec/Track/Long"
select_long.Code = "(TrP>0.0) & (TrPT>0.0) & ~TrBACKWARD & (TrMINIP()>0.1) & (TrMINIPCUT(0.1)) & ~TrINVALID & (TrCHI2PDOF<3)"\
                   "&  (TrMINIPCHI2()>5) & TrMINIPCHI2CUT(5)"
# add producers
from Configurables import UnpackTrack, UnpackRecVertex
ApplicationMgr().TopAlg += [UnpackTrack(), UnpackRecVertex()]
# add consumers
ApplicationMgr().TopAlg += [select_long, filter_long, filter_pvs]

useHive = False
if not useHive:
    EventSelector()
    EventDataSvc().ForceLeaves = True
else:
    # To test HiveMode and print data dependencies
    from Configurables import HiveWhiteBoard
    whiteboard   = HiveWhiteBoard("EventDataSvc")
    whiteboard.EventSlots = 1
    whiteboard.ForceLeaves = True
    ApplicationMgr().ExtSvc.insert(0, whiteboard)
    from Configurables import HLTEventLoopMgr
    eventloopmgr = HLTEventLoopMgr()
    eventloopmgr.ThreadPoolSize = 1
    ApplicationMgr().EventLoop = eventloopmgr
    from Configurables import UpdateManagerSvc
    UpdateManagerSvc().WithoutBeginEvent = True

# Needed for LoKi to decode functors
from Configurables import AlgContextSvc
AlgContextSvc(BypassIncidents=True)

from GaudiConf import IOHelper
files = [
       '/data/ntuples/minbias1.ldst',
       '/data/ntuples/minbias2.ldst'
       ]
evtsel = IOHelper("ROOT").inputFiles(files)
app.EvtMax = 10
app.DDDBtag = "dddb-20171126"
app.CondDBtag = "sim-20171127-vc-md100"
LHCbApp().Simulation = True
CondDB().Upgrade = True

# Timing table to make sure things work as intended
from Configurables import AuditorSvc, LHCbTimingAuditor
ApplicationMgr().AuditAlgorithms = 1
if 'AuditorSvc' not in ApplicationMgr().ExtSvc:
    ApplicationMgr().ExtSvc.append('AuditorSvc')
AuditorSvc().Auditors.append(LHCbTimingAuditor(Enable = True))


# import GaudiPython
# from Configurables import ApplicationMgr
# appMgr = GaudiPython.AppMgr()
# evt = appMgr.evtsvc()
# appMgr.run(1)
