/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#pragma once
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Dicts.h"
#include "LoKi/Streamers.h"
#include "LoKi_v2/TrackTypes.h"
#include "LoKi_v2/Track.h"
#include "LoKi_v2/LoKiTrack.h"
// ============================================================================
#include "LoKi_v2/ITrackFunctorFactory.h"
#include "LoKi_v2/ITrackFunctorAntiFactory.h"
#include "LoKi_v2/TrackEngine.h"
#include "Event/Track_v2.h"
// ============================================================================
/** @file
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 */
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Dicts
  {
    // ========================================================================
    /** @class FunCalls Calls.h LoKi/Calls.h
     *  @author Vanya BELYAEV, Sascha Stahl
     */
    template <>
    class FunCalls<Pr::TrackType>
    {
    private:
      // ======================================================================
      using Type = Pr::TrackType;
      using Fun = Pr::Types::TrFunc;
      // ======================================================================
    public:
      // ======================================================================
      // __call__
      static Fun::result_type __call__
      ( const Fun& fun  , Type const*           o ) { return fun ( o ) ; }
    public:
      static Fun::result_type __rrshift__
      ( const Fun& fun  , Type const*           o ) { return fun ( o ) ; }
    public:
      // __rshift__
      static LoKi::FunctorFromFunctor<Type const*,double> __rshift__
      ( const Fun&                          fun  ,
        const LoKi::Functor<double,double>& o    ) {
          return fun >> o  ; 
        }
      // __rshift__
      static LoKi::FunctorFromFunctor<Type const*,bool>   __rshift__
      ( const Fun&                          fun  ,
        const LoKi::Functor<double,bool>&   o    ) { 
          return fun >> o  ;
        }
      // ======================================================================
    } ;
    // // ========================================================================
    template <>
      class CutCalls<Pr::TrackType>
    {
    private:
      // ======================================================================
      using Type = Pr::TrackType;
      using Fun = Pr::Types::TrCuts;
      // ======================================================================
    public:
      // ======================================================================
      // __call__
      static Fun::result_type __call__
      ( const Fun&            fun ,
        Type const*           o   ) { return fun ( o ) ;  }
    public:
      // __rrshift__
      static Fun::result_type  __rrshift__
      ( const Fun& fun  , Type const*           o ) { return fun ( o ) ; }
      // ======================================================================
    public:
      // ======================================================================
      static LoKi::FunctorFromFunctor<Type const*,bool> __rshift__
      ( const Fun& fun  , const Fun&                        o )
      { 
        return fun >> o  ; 
      }
      // ======================================================================
    } ;
    // ========================================================================
  } //                                             end of namespace LoKi::Dicts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
namespace
{
  // ==========================================================================
  struct _Instantiations
  {
    // ========================================================================
    // the basic functions
    LoKi::Dicts::Funcs     <LoKi::Pr::TrackType const *>               m_f1   ;
    // operators
    LoKi::Dicts::FuncOps   <LoKi::Pr::TrackType const *>               m_o1   ;
    LoKi::Dicts::CutsOps   <LoKi::Pr::TrackType const *>               m_co1  ;
    // calls
    LoKi::Dicts::FunCalls<LoKi::Pr::TrackType>                        m_cf1  ;
    LoKi::Dicts::CutCalls<LoKi::Pr::TrackType>                        m_cc1  ;
    /// trivia:
    LoKi::Functors::Empty     <LoKi::Pr::TrackType const *>            m_e1   ;
    LoKi::Functors::Size      <LoKi::Pr::TrackType const *>            m_s1   ;
    /// fictive constructor
    _Instantiations () ;
    // ========================================================================
  } ;
  // ==========================================================================
} //                                                 end of anonymous namespace
// ============================================================================
//                                                                      The END
// ============================================================================
// ============================================================================
