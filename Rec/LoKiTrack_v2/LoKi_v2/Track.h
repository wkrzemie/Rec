/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "GaudiKernel/Kernel.h"
#include "LoKi/Interface.h"
#include "LoKi/TES.h"
#include "LoKi_v2/TrackTypes.h"

// ============================================================================
namespace LoKi::Pr::Track
{
  // ==========================================================================
  /** @namespace LoKi::Pr::Track Track.h LoKi_v2/Track.h
   *  Namespace with few basic "track"-functors
   *  @see LHCb::Event::v2::Track
   *
   *  This file is a part of LoKi project -
   *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
   *
   *  The package has been designed with the kind help from
   *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
   *  contributions and advices from G.Raven, J.van Tilburg,
   *  A.Golutvin, P.Koppenburg have been used in the design.
   *
   *  @author Vanya BELYAEV, Sascha Stahl
   */
  // ========================================================================
  /** @class Momentum
   *  @see LoKi::Pr::Cuts::TrP
   *  simple evaluator of momentum of the track
   *  @author Vanya BELYAEV, Sascha Stahl
   */
  class GAUDI_API Momentum : public Types::TrFunc
  {
  public:
    // ======================================================================
    /// Default Constructor
    Momentum() : AuxFunBase{std::tie()} {}
    /// MANDATORY: clone method ("virtual constructor")
    Momentum* clone() const override { return new Momentum( *this ); }
    /// mandatory: the only one essential method
    result_type operator()( argument t ) const override { return t->p(); };
    /// OPTIONAL: the nice printout
    std::ostream& fillStream( std::ostream& s ) const override { return s << "TrP"; }
    // ======================================================================
  };
  // ========================================================================
  /** @class Momentum
   *  @see LoKi::Pr::Cuts::TrP
   *  simple evaluator of momentum of the track
   *  @author Vanya BELYAEV, Sascha Stahl
   */
  class GAUDI_API TransverseMomentum : public Types::TrFunc
  {
  public:
    // ======================================================================
    /// Default Constructor
    TransverseMomentum() : AuxFunBase{std::tie()} {}
    /// MANDATORY: clone method ("virtual constructor")
    TransverseMomentum* clone() const override { return new TransverseMomentum( *this ); }
    /// mandatory: the only one essential method
    result_type operator()( argument t ) const override { return t->pt(); };
    /// OPTIONAL: the nice printout
    std::ostream& fillStream( std::ostream& s ) const override { return s << "TrPT"; }
    // ======================================================================
  };
  class GAUDI_API Chi2 final : public Types::TrFunc
  {
  public:
    // ======================================================================
    /// Default Constructor
    Chi2() : AuxFunBase{std::tie()} {}
    /// MANDATORY: clone method ("virtual constructor")
    Chi2* clone() const override { return new Chi2( *this ); }
    /// mandatory: the only one essential method
    result_type operator()( argument t ) const override {return t->chi2();};
    /// OPTIONAL: the nice printout
    std::ostream& fillStream( std::ostream& s ) const override { return s << "TrCHI2"; }
    // ======================================================================
  };

  // ========================================================================
  /** @class Chi2PerDoF
   *  simple evaluator of the LHcb::Track::chi2PerDoF
   *  @see LoKi::Pr::Cuts::TrCHI2PDOF
   *  @author Vanya BELYAEV, Sascha Stahl
   */

  class GAUDI_API Chi2PerDoF final : public Types::TrFunc
  {
  public:
    // ======================================================================
    /// Default Constructor
    Chi2PerDoF() : AuxFunBase{std::tie()} {}
    /// MANDATORY: clone method ("virtual constructor")
    Chi2PerDoF* clone() const override { return new Chi2PerDoF( *this ); }
    /// mandatory: the only one essential method
    result_type operator()( argument t ) const override {return t->chi2PerDoF();};
    /// OPTIONAL: the nice printout
    std::ostream& fillStream( std::ostream& s ) const override { return s << "TrCHI2PDOF"; }
    // ======================================================================
  };
  // ========================================================================
  /** @class CheckFlag
   *  @see LoKi::Pr::Cuts::TrISFLAG
   *  @see LoKi::Pr::Cuts::TrBACKWARD
   *  @see LoKi::Pr::Cuts::TrINVALID
   *  @simple predicate to check the flag
   *  @author Vanya BELYAEV, Sascha Stahl
   */
  class GAUDI_API CheckFlag final : public Types::TrCuts
  {
  public:
    // ======================================================================
    /// constructor from the flag ;
    CheckFlag( TrackType::Flag flag );
    /// MANDATORY: clone method ("virtual constructor")
    CheckFlag* clone() const override { return new CheckFlag( *this ); }
    /// MANDATORY: the only one essential method
    result_type operator()( argument t ) const override { return t->checkFlag( m_flag ); };
    /// OPTIONAL: the nice printout
    std::ostream& fillStream( std::ostream& s ) const override;
    // ======================================================================
  private:
    // ======================================================================
    // the flag to be checked:
    TrackType::Flag m_flag; ///< the flag to be checked:
    // ======================================================================
  };
  // ========================================================================
  /** @class MinimalImpactParameter
   *  @see LoKi::Pr::Cuts::TrMinIP
   *  simple evaluator of minimal impact parameter to and PV of the track
   *  @author Sascha Stahl
   */
  class GAUDI_API MinimalImpactParameter final : public Types::TrFunc, public LoKi::TES::DataHandle<VertexContainer>
  {
  public:
    MinimalImpactParameter( const GaudiAlgorithm* algorithm, const std::string& location );
    MinimalImpactParameter( const GaudiAlgorithm* algorithm );
    MinimalImpactParameter* clone() const override { return new MinimalImpactParameter( *this ); };
    result_type operator()( argument track ) const override;
    std::ostream& fillStream( std::ostream& s ) const override;
    // ======================================================================
  };
  // ========================================================================
  /** @class MinimalImpactParameterGreaterThan
   *  @see LoKi::Pr::Cuts::TrMinIPGreaterThan
   *  simple evaluator of minimal impact parameter to and PV of the track
   *  @author Sascha Stahl
   */
  class GAUDI_API MinimalImpactParameterGreaterThan final : public Types::TrCuts,
                                                            public LoKi::TES::DataHandle<VertexContainer>
  {
  public:
    MinimalImpactParameterGreaterThan( const GaudiAlgorithm* algorithm, double cut_value, const std::string& location );
    MinimalImpactParameterGreaterThan( const GaudiAlgorithm* algorithm, double cut_value );
    MinimalImpactParameterGreaterThan* clone() const override
    {
      return new MinimalImpactParameterGreaterThan( *this );
    };
    result_type operator()( argument track ) const override;
    std::ostream& fillStream( std::ostream& s ) const override;

  private:
    double m_cut_value = 0.0;
    // ======================================================================
  };
  // ========================================================================

    // ========================================================================
    /** @class MinimalImpactParameterChi2
    *  @see LoKi::Tracks::TrMINIPCHI2
    *  Evaluate the minimum impact parameter chi2 of the track to any vertex in the given location.
    *  @author Olli Lupton
    */
    class GAUDI_API MinimalImpactParameterChi2 final : public Types::TrFunc, public LoKi::TES::DataHandle<VertexContainer>
    {
    public:
      MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm, const std::string& location );
      MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm );
      MinimalImpactParameterChi2* clone() const override { return new MinimalImpactParameterChi2( *this ); };
      result_type operator()( argument track ) const override;
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    // ========================================================================
    /** @class MinimalImpactParameterChi2Cut
      *  @see LoKi::Tracks::TrMINIPCHI2CUT
      *  Check whether the minimum impact parameter chi2 of the track to any vertex in the given location exceeds the
      *  given cut.
      *  @author Olli Lupton
      */
    class GAUDI_API MinimalImpactParameterChi2Cut final : public Types::TrCuts, public LoKi::TES::DataHandle<VertexContainer>
    {
      double m_ipchi2_cut{0.0};
    public:
      MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut, const std::string& location );
      MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut );
      MinimalImpactParameterChi2Cut* clone() const override { return new MinimalImpactParameterChi2Cut( *this ); };
      result_type operator()( argument track ) const override;
      std::ostream& fillStream( std::ostream& s ) const override;
    };


} //                                            end of namespace LoKi::Pr::Track
