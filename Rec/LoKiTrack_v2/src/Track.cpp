/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <sstream>
// ============================================================================
// Event
// ============================================================================
// Track Interfaces
// ============================================================================
// ============================================================================
// LoKi
// ============================================================================
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "TrackKernel/TrackVertexUtils.h"
#include "LoKi/Constants.h"
#include "LoKi_v2/Track.h"
#include "LoKi_v2/TrackTypes.h"
// ============================================================================
/** @file
 *  Implementation file for classes from the namespace LoKi::Pr::Track
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV, Sascha Stahl
 *
 */
// ============================================================================
// ============================================================================
// mandatory: the only one essential method
// ============================================================================
namespace LoKi::Pr::Track
{
  // ============================================================================
  // CheckFlag functor
  // ============================================================================
  CheckFlag::CheckFlag( TrackType::Flag flag )
      : LoKi::AuxFunBase( std::make_tuple( LoKi::StrKeep( "Flag::" + toString( flag ) ) ) ), m_flag( flag )
  {
  }

  std::ostream& CheckFlag::fillStream( std::ostream& s ) const
  {
    switch ( m_flag ) {
    case TrackType::Flag::Backward:
      return s << "TrBACKWARD"; // RETURN
    case TrackType::Flag::Invalid:
      return s << "TrINVALID"; // RETURN
    case TrackType::Flag::Clone:
      return s << "TrCLONE"; // RETURN
    case TrackType::Flag::Used:
      return s << "TrUSED"; // RETURN
    case TrackType::Flag::IPSelected:
      return s << "TrIPSELECTED"; // RETURN
    case TrackType::Flag::PIDSelected:
      return s << "TrPIDSELECTED"; // RETURN
    case TrackType::Flag::Selected:
      return s << "TrSELECTED"; // RETURN
    case TrackType::Flag::L0Candidate:
      return s << "TrL0CANDIDATE"; // RETURN
    default:
      break; // BREAK
    }
    //
    return s << "TrISFLAG(" << (int)m_flag << ")";
  }

  namespace
  {
    template <typename Vertex>
    auto impactParameter( LHCb::State const& state, Vertex const& vertex )
    {
      return Gaudi::Math::impactParameter( vertex.position(), Gaudi::Math::Line{state.position(), state.slopes()} );
    }
  }
  // ============================================================================
  // MinimalImpactParameter functor
  // ============================================================================

  MinimalImpactParameter::MinimalImpactParameter( const GaudiAlgorithm* algorithm, const std::string& location )
      : LoKi::AuxFunBase( std::tie( algorithm, location ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, location )
  {
  }

  MinimalImpactParameter::MinimalImpactParameter( const GaudiAlgorithm* algorithm )
      : LoKi::AuxFunBase( std::tie( algorithm ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, LHCb::RecVertexLocation::Primary )
  {
  }

  MinimalImpactParameter::result_type MinimalImpactParameter::operator()( argument track ) const
  {
    auto const* state = track->stateAt( LHCb::State::ClosestToBeam );
    if ( UNLIKELY(state == nullptr) ) {
      return std::numeric_limits<result_type>::max();
    }
    auto vertices = get();
    return std::accumulate(
        std::begin( *vertices ), std::end( *vertices ), std::numeric_limits<result_type>::max(),
        [&state]( result_type ip, auto const& vertex ) { return std::min( ip, impactParameter( *state, vertex ) ); } );
  }

  std::ostream& MinimalImpactParameter::fillStream( std::ostream& s ) const
  {
    s << " TrMINIP( ";
    Gaudi::Utils::toStream( location(), s );
    return s << " ) ";
  }
  // ============================================================================
  // MinimalImpactParameterGreaterThan functor
  // ============================================================================

  MinimalImpactParameterGreaterThan::MinimalImpactParameterGreaterThan( const GaudiAlgorithm* algorithm,
                                                                        double cut_value, const std::string& location )
      : LoKi::AuxFunBase( std::tie( algorithm, location ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, location )
      , m_cut_value( cut_value )
  {
  }

  MinimalImpactParameterGreaterThan::MinimalImpactParameterGreaterThan( const GaudiAlgorithm* algorithm,
                                                                        double                cut_value )
      : LoKi::AuxFunBase( std::tie( algorithm ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, LHCb::RecVertexLocation::Primary )
      , m_cut_value( cut_value )
  {
  }

  MinimalImpactParameterGreaterThan::result_type MinimalImpactParameterGreaterThan::operator()( argument track ) const
  {
    auto const* state = track->stateAt( LHCb::State::ClosestToBeam );
    if ( UNLIKELY( state == nullptr ) ) {
      return std::numeric_limits<result_type>::max();
    }
    auto vertices = get();
    return std::none_of( std::begin( *vertices ), std::end( *vertices ), [&state, this]( auto const& vertex ) {
      return impactParameter( *state, vertex ) < m_cut_value;
    } );
  }

  std::ostream& MinimalImpactParameterGreaterThan::fillStream( std::ostream& s ) const
  {
    s << " TrMINIPCUT( ";
    Gaudi::Utils::toStream( location(), s );
    return s << " ) ";
  }

    // ============================================================================
  // MinimalImpactParameterChi2 functor
  // ============================================================================
  MinimalImpactParameterChi2::MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm, const std::string& location )
      : LoKi::AuxFunBase( std::tie( algorithm, location ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, location )
  {
  }

  MinimalImpactParameterChi2::MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm )
      : LoKi::AuxFunBase( std::tie( algorithm ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, LHCb::RecVertexLocation::Primary )
  {
  }

  MinimalImpactParameterChi2::result_type MinimalImpactParameterChi2::operator()( argument track ) const
  {
    if ( UNLIKELY( track == nullptr ) ) {
      return std::numeric_limits<result_type>::max();
    }

    auto const* state = track->stateAt( LHCb::State::ClosestToBeam );
    if ( UNLIKELY( state == nullptr ) ) {
      return std::numeric_limits<result_type>::max();
    }

    auto vertices = get();
    if ( UNLIKELY( vertices == nullptr ) ) {
        return std::numeric_limits<result_type>::max();
      }

    auto calc_ipchi2 = [state]( auto const& vertex ) {
      return LHCb::TrackVertexUtils::vertexChi2( *state, vertex.position(), vertex.covMatrix() );
    };

    return std::accumulate(
        std::begin( *vertices ), std::end( *vertices ), std::numeric_limits<result_type>::max(),
        [&calc_ipchi2]( result_type ipchi2, auto const& vertex ) { return std::min( ipchi2, calc_ipchi2( vertex ) ); }
        );
  }

  std::ostream& MinimalImpactParameterChi2::fillStream( std::ostream& s ) const
  {
    s << "TrMINIPCHI2( ";
    Gaudi::Utils::toStream( location(), s ); // this handles the quoting
    return s << " )";
  }

  // ============================================================================
  // MinimalImpactParameterChi2Cut functor
  // ============================================================================
  MinimalImpactParameterChi2Cut::MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut, const std::string& location )
      : LoKi::AuxFunBase( std::tie( algorithm, location ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, location )
      , m_ipchi2_cut( ipchi2_cut )
  {
  }

  MinimalImpactParameterChi2Cut::MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut )
      : LoKi::AuxFunBase( std::tie( algorithm ) )
      , LoKi::TES::DataHandle<VertexContainer>( algorithm, LHCb::RecVertexLocation::Primary )
      , m_ipchi2_cut( ipchi2_cut )
  {
  }

  MinimalImpactParameterChi2Cut::result_type MinimalImpactParameterChi2Cut::operator()( argument track ) const
  {
    if ( UNLIKELY( track == nullptr ) ) {
      return std::numeric_limits<result_type>::max();
    }

    auto const* state = track->stateAt( LHCb::State::ClosestToBeam );
    if ( UNLIKELY( state == nullptr ) ) {
      return std::numeric_limits<result_type>::max();
    }

    auto vertices = get();
    if ( UNLIKELY( vertices == nullptr ) ) {
      return std::numeric_limits<result_type>::max();
    }

    auto small_ipchi2 = [this, state]( auto const& vertex ) {
      return LHCb::TrackVertexUtils::vertexChi2( *state, vertex.position(), vertex.covMatrix() ) < m_ipchi2_cut;
    };

    return std::none_of( std::begin( *vertices ), std::end( *vertices ), small_ipchi2 );
  }

  std::ostream& MinimalImpactParameterChi2Cut::fillStream( std::ostream& s ) const
  {
    s << "TrMINIPCHI2CUT( " << m_ipchi2_cut << ", ";
    Gaudi::Utils::toStream( location(), s ); // this handles the quoting
    return s << " )";
  }
}
