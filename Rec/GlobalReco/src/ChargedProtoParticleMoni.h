/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleMoni.h
 *
 * Header file for algorithm ChargedProtoParticleMoni
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_ChargedProtoParticleMoni_H
#define GLOBALRECO_ChargedProtoParticleMoni_H 1

// STL
#include <sstream>
#include <vector>
#include <utility>
#include <map>

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
// Event
#include "Event/Track.h"
#include "Event/ProtoParticle.h"

// boost
#include "boost/format.hpp"

// Rich Utils
#include "RichUtils/RichPoissonEffFunctor.h"
#include "RichUtils/RichStatDivFunctor.h"

//-----------------------------------------------------------------------------
/** @class ChargedProtoParticleMoni ChargedProtoParticleMoni.h
 *
 *  Algorithm to build charged ProtoParticles from charged Tracks.
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */
//-----------------------------------------------------------------------------

class ChargedProtoParticleMoni final : public GaudiHistoAlg
{

public:

  /// Standard constructor
  ChargedProtoParticleMoni( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override final;    ///< Algorithm execution
  StatusCode finalize() override final;    ///< Algorithm finalization

private: // data

  /// Location of the ProtoParticles in the TES
  std::string m_protoPath;

  /// Location in TES of input Tracks
  std::string m_tracksPath;

private: // classes

  /// Simple utility tally class
  class TrackTally
  {
  public:
    /// Default constructor
    TrackTally() = default;
  public:
    unsigned long long totTracks = 0;      ///< Number of considered tracks
    unsigned long long selTracks = 0;      ///< Number of tracks selected to creaste a ProtoParticle from
    unsigned long long ecalTracks = 0;     ///< Number of ProtoParticles created with CALO ECAL info
    unsigned long long bremTracks = 0;     ///< Number of ProtoParticles created with CALO BREM info
    unsigned long long spdTracks = 0;      ///< Number of ProtoParticles created with CALO SPD info
    unsigned long long prsTracks = 0;      ///< Number of ProtoParticles created with CALO PRS info
    unsigned long long hcalTracks = 0;     ///< Number of ProtoParticles created with CALO HCAL info
    unsigned long long richTracks = 0;     ///< Number of ProtoParticles created with RICH info
    unsigned long long muonTracks = 0;     ///< Number of ProtoParticles created with MUON info
    unsigned long long velodEdxTracks = 0; ///< Number of ProtoParticles created with VELO dE/dx info
  };

private:

  /// Print statistics
  void printStats( const MSG::Level level = MSG::INFO ) const;

  /// Find the ProtoParticle created from a given Track
  const LHCb::ProtoParticle * getProto( const LHCb::ProtoParticles * protos,
                                        const LHCb::Track * track ) const;

  //WARNING: reference is invalidated as soon as an insert happens
  inline TrackTally & trackTally( const LHCb::Track::Types key )
  {
    return m_nTracks[key];
  }

 private:

  /// Event count
  unsigned long long m_nEvts = 0;

  /// Map type containing tally for various track types
  /// Total number of tracks considered and selected
  std::map< LHCb::Track::Types, TrackTally > m_nTracks;

};

#endif // GLOBALRECO_ChargedProtoParticleMoni_H
