/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
#include "NeutralProtoPAlg.h"
// ============================================================================
/** @file
 *  Implementation file for class NeutralProtoPAlg
 *  @date 2006-06-09
 *  @author Olivier Deschamps
 */
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( NeutralProtoPAlg )
// ============================================================================
// Standard constructor, initializes variables
// ============================================================================
NeutralProtoPAlg::NeutralProtoPAlg( const std::string& name,
                                    ISvcLocator* pSvcLocator)
: GaudiAlgorithm ( name , pSvcLocator )
{
  // declare the properties
  declareProperty ( "HyposLocations"        , m_hyposLocations   ) ;
  declareProperty ( "ProtoParticleLocation" ,  m_protoLocation   ) ;
  declareProperty ( "LightMode"      , m_light_mode = false,
                    "Use 'light' mode and do not collect all information. Useful for Calibration." ) ;

  // default location from context()
  using namespace LHCb::CaloHypoLocation;
  m_hyposLocations.push_back( LHCb::CaloAlgUtils::PathFromContext( context() , MergedPi0s   ) ) ; // put it 1st to speed-up photon mass retrieval
  m_hyposLocations.push_back( LHCb::CaloAlgUtils::PathFromContext( context() , Photons      ) ) ;
  m_hyposLocations.push_back( LHCb::CaloAlgUtils::PathFromContext( context() , SplitPhotons ) ) ;
  //  m_protoLocation = LHCb::CaloAlgUtils::PathFromContext( flag , LHCb::ProtoParticleLocation::Neutrals );
  m_protoLocation = LHCb::ProtoParticleLocation::Neutrals ;
}
// ============================================================================
// Initialization
// ============================================================================
StatusCode NeutralProtoPAlg::initialize()
{
  const StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  for ( const auto & loc : m_hyposLocations )
  { info() << " Hypothesis loaded from " << loc << endmsg; }

  counterStat = tool<ICounterLevel>("CounterLevel");

  if ( lightMode() )
    info() << "Neutral protoparticles will be created in 'Light' Mode" << endmsg ;

  m_estimator = tool<ICaloHypoEstimator>("CaloHypoEstimator","CaloHypoEstimator",this);
  m_estimator->hypo2Calo()->_setProperty("Seed", "false").ignore();
  m_estimator->hypo2Calo()->_setProperty("PhotonLine", "true").ignore();
  m_estimator->hypo2Calo()->_setProperty("AddNeighbors", "false").ignore();

  m_mass.clear();
  m_setMass = false;

  return sc;
}

double NeutralProtoPAlg::getMass( const int cellCode ) const
{
  if( !m_setMass )
  {
    Warning("You should process MergedPi0 protoparticles first to speed up retrieval of photon 'mass'",StatusCode::SUCCESS,1).ignore();
    // === storing masses
    m_setMass = true;
    using namespace LHCb::CaloHypoLocation;
    const auto loc =  LHCb::CaloAlgUtils::PathFromContext( context() , MergedPi0s   );
    const auto * hypos = getIfExists<LHCb::CaloHypos>( loc );
    if ( !hypos )return 0.;
    for ( const auto& hypo : *hypos )
    {
      if ( !hypo ) continue;
      using namespace CaloDataType;
      const auto cellCode = int(m_estimator->data(hypo, CellID ));
      m_mass[cellCode]=m_estimator->data(hypo, HypoM );
    }
  }
  const auto it = m_mass.find(cellCode);
  return ( it == m_mass.end() ? 0.0 : it->second );
}

// ============================================================================
// Main execution
// ============================================================================
StatusCode NeutralProtoPAlg::execute()
{

  // create and register the output container
  LHCb::ProtoParticles* protos = nullptr;
  if ( !lightMode() && exist<LHCb::ProtoParticles>(m_protoLocation) )
  {
    Warning( "Existing ProtoParticle container at " +m_protoLocation+ " found -> Will replace", StatusCode::SUCCESS, 1).ignore();
    if(counterStat->isQuiet())counter("Replaced Proto")+=1;
    protos = get<LHCb::ProtoParticles>(m_protoLocation);
    protos->clear();
  }
  else
  {
    protos = new LHCb::ProtoParticles();
    put( protos , m_protoLocation ) ;
  }

  if ( !protos )return Warning("NeutralProto container points to NULL ",StatusCode::SUCCESS);

  // -- reset mass storage
  m_mass.clear();
  m_setMass=false;
  //------ loop over all caloHypo containers
  for ( const auto & loc : m_hyposLocations )
  {

    //==  Load the CaloHypo container
    const auto * hypos = getIfExists<LHCb::CaloHypos>( loc );
    if ( ! hypos )
    {
      if( msgLevel(MSG::DEBUG) ) debug()<< "No CaloHypo at '" << loc << "'" << endmsg ;
      if ( counterStat->isQuiet() ) counter("No " + loc + " container") += 1;
      continue;
    }

    // no cluster mass storage when no MergedPi0
    if ( LHCb::CaloAlgUtils::toUpper(loc).find("MERGED") != std::string::npos &&
         hypos->empty() ) { m_setMass = true; }

    if ( msgLevel(MSG::DEBUG) )
      debug() << "CaloHypo loaded at " << loc << " (# " << hypos->size()<<")"<< endmsg;
    int count = 0 ;

    // == Loop over CaloHypos
    for ( const auto * hypo : *hypos )
    {
      if ( ! hypo ) { continue ; }
      count++;

      // == create and store the corresponding ProtoParticle
      auto * proto = new LHCb::ProtoParticle() ;
      protos->insert( proto ) ;

      // == link CaloHypo to ProtoP
      using namespace CaloDataType;
      proto-> addToCalo( hypo ) ;

      // ===== add data to protoparticle
      if ( !lightMode() )
      {
        std::ostringstream type("");
        type << hypo->hypothesis();
        const auto hypothesis = type.str();
        // extra data :
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralID  ,hypo, CellID   );   // seed cellID
        // retrieve HypoM for photon
        const auto cellCode = int(m_estimator->data(hypo, CellID ));
        if( hypo -> hypothesis() == LHCb::CaloHypo::Hypothesis::Photon )
        {
          const auto mass  = getMass( cellCode );
          if( mass > 0. )
          {
            proto->addInfo( LHCb::ProtoParticle::additionalInfo::ClusterMass, mass );
            if(counterStat->isVerbose())counter("ClusterMass for Photon") += mass;
          }
        }
        else if ( hypo -> hypothesis() == LHCb::CaloHypo::Hypothesis::Pi0Merged )
        {
          pushData(proto, LHCb::ProtoParticle::additionalInfo::ClusterMass, hypo, HypoM);
          m_setMass=true;
          m_mass[cellCode] = m_estimator->data(hypo, HypoM );
        }
        if( hypo -> hypothesis() != LHCb::CaloHypo::Hypothesis::Pi0Merged ){
          pushData(proto, LHCb::ProtoParticle::additionalInfo::ClusterAsX, hypo, ClusterAsX, 0);
          pushData(proto, LHCb::ProtoParticle::additionalInfo::ClusterAsY, hypo, ClusterAsY, 0);
        }

        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralPrs, hypo, HypoPrsE );
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal,hypo, ClusterE );

        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloTrMatch, hypo, ClusterMatch , +1.e+06 );  // ** input to neutralID
        pushData(proto, LHCb::ProtoParticle::additionalInfo::ShowerShape, hypo, Spread);         // ** input to neutralID  && isPhoton (as Fr2)
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralSpd, hypo, HypoSpdM );   // ** input to neutralID
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralHcal2Ecal, hypo, Hcal2Ecal); // ** input to neutralID
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloClusterCode, hypo , ClusterCode);
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloClusterFrac, hypo , ClusterFrac,1);
        pushData(proto, LHCb::ProtoParticle::additionalInfo::Saturation, hypo, Saturation, 0);



        auto dep = (m_estimator->data(hypo,  ToSpdM ) > 0) ? -1. : +1.;
        dep *= m_estimator->data(hypo,  ToPrsE );
        proto -> addInfo ( LHCb::ProtoParticle::additionalInfo::CaloDepositID   , dep ) ;     // ** input to neutralID toPrsE=|caloDepositID|

        // DLL-based neutralID (to be obsolete)  :
        pushData(proto, LHCb::ProtoParticle::additionalInfo::PhotonID , hypo, NeutralID , -1. ,true    ); // old DLL-based neutral-ID // FORCE

        // isNotX  inputs :
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralE49   , hypo, E49 );
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralE19   , hypo, E19 );      // ** input to neutralID
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE49, hypo, PrsE49  );  // ** input to neutralID
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE19, hypo, PrsE19  );  // ** input to neutralID
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE4max,hypo, PrsE4Max); // ** input to neutralID
        pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralPrsM  , hypo, HypoPrsM);  // ** input to neutralID
        // isNotX output :
        pushData(proto, LHCb::ProtoParticle::additionalInfo::IsNotH   , hypo, isNotH  , -1. , true     ); // new NN-based neutral-ID (anti-H) // FORCE
        pushData(proto, LHCb::ProtoParticle::additionalInfo::IsNotE   , hypo, isNotE  , -1. , true     ); // new NN-based neutral-ID (anti-E) // FORCE

        // isPhoton inputs (photon & mergedPi0 only)
        if( hypo -> hypothesis() != LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 )
        {
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloShapeFr2r4     ,hypo, isPhotonFr2r4);  // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloShapeAsym      ,hypo, isPhotonAsym);   // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloShapeKappa     ,hypo, isPhotonKappa);  // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloShapeE1        ,hypo, isPhotonEseed);     // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloShapeE2        ,hypo, isPhotonE2);     // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeE2     ,hypo, isPhotonPrsE2);     // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeEmax   ,hypo, isPhotonPrsEmax);   // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeFr2    ,hypo, isPhotonPrsFr2);    // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeAsym   ,hypo, isPhotonPrsAsym);   // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM           ,hypo, isPhotonPrsM);      // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM15         ,hypo, isPhotonPrsM15);    // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM30         ,hypo, isPhotonPrsM30);    // -- input to isPhoton
          pushData(proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM45         ,hypo, isPhotonPrsM45);    // -- input to isPhoton
          // isPhoton output :
          pushData(proto, LHCb::ProtoParticle::additionalInfo::IsPhoton , hypo, isPhoton , +1. , true    ); // NN-based neutral-ID (anti-pi0) // FORCE to +1 when missing (i.e. PT < 2 GeV)
          pushData(proto, LHCb::ProtoParticle::additionalInfo::IsPhotonXGB , hypo, isPhotonXGB , +1. , true    ); // XGBoost-based neutral-ID (anti-pi0) // FORCE to +1 when missing (i.e. PT < 2 GeV)

        }
      }// lightmode
    } // loop over CaloHypos
    if ( counterStat->isQuiet() ) counter( loc + "=>" + m_protoLocation) += count;
  } // loop over HyposLocations


  if ( msgLevel(MSG::DEBUG) )debug() << "# Neutral ProtoParticles created : " << protos -> size() << endmsg;
  if(counterStat->isQuiet())counter ("#protos in " + m_protoLocation) += protos->size() ;
  return StatusCode::SUCCESS;
}
// ============================================================================
//  Finalize
// ============================================================================

StatusCode NeutralProtoPAlg::finalize(){
  //
  if ( lightMode() )
  { info() << "Neutral protoparticles have been created in 'Light' Mode" << endmsg ; }

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
