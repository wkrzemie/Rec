/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleCALOFUTUREBaseAlg.h
 *
 * Header file for algorithm FutureChargedProtoParticleCALOFUTUREBaseAlg
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_FutureChargedProtoParticleCALOFUTUREBaseAlg_H
#define GLOBALRECO_FutureChargedProtoParticleCALOFUTUREBaseAlg_H 1

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// Event
#include "Event/ProtoParticle.h"

// from CaloUtils
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "Event/CaloDataFunctor.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"

// Relations
#include "Relations/IRelation.h"
#include "Relations/IRelationWeighted2D.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"

/** @class FutureChargedProtoParticleCALOFUTUREBaseAlg FutureChargedProtoParticleCALOFUTUREBaseAlg.h
 *
 *  Base class for ProtoParticle algorithms that add CALOFUTURE information
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class FutureChargedProtoParticleCALOFUTUREBaseAlg : public GaudiAlgorithm
{

public:

  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization

protected:
  IFutureCounterLevel* counterStat;

  /// Loads a CALOFUTURE relations table
  template < typename TYPE >
  inline bool loadCaloTable( TYPE *& table, const std::string & location ) const
  {
    table = getIfExists<TYPE>( location );
    if ( !table )
    {
      Warning("No CALOFUTURE "+System::typeinfoName(typeid(TYPE))+
              " table at '"+location+"'", StatusCode::SUCCESS ).ignore();
    }
    return ( nullptr != table );
  }

protected:

  // Add extra info from CaloDigits (Spd+Prs)
  ICaloFutureHypoEstimator * m_estimator = nullptr;

};

#endif // GLOBALRECO_FutureChargedProtoParticleCALOFUTUREBaseAlg_H
