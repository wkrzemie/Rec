/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleTupleAlg.cpp
 *
 * Implementation file for algorithm ChargedProtoParticleTupleAlg
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2006-11-15
 */
//-----------------------------------------------------------------------------

// local
#include "ChargedProtoParticleTupleAlg.h"

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ChargedProtoParticleTupleAlg )

//=============================================================================
// Main execution
//=============================================================================
StatusCode ChargedProtoParticleTupleAlg::execute()
{
  // Load the charged ProtoParticles
  const auto * protos = getIfExists<LHCb::ProtoParticles>( m_protoPath.value() );
  if ( !protos )
    return Warning( "No ProtoParticles at '" + m_protoPath.value() + "'",
                    StatusCode::SUCCESS );

  // Loop over the protos
  for ( const auto * proto : *protos ) {

    // Check this is a charged track ProtoParticle
    const auto * track = proto->track();
    if ( !track ) continue;

    // make a tuple
    Tuple tuple = nTuple("protoPtuple","ProtoParticle PID Information");

    // Status Code for filling the ntuple
    StatusCode sc = StatusCode::SUCCESS;

    // reco variables

    // some track info
    if ( sc ) sc = tuple->column( "TrackP",          track->p()  );
    if ( sc ) sc = tuple->column( "TrackPt",         track->pt() );
    if ( sc ) sc = tuple->column( "TrackChi2PerDof", track->chi2PerDoF() );
    if ( sc ) sc = tuple->column( "TrackNumDof",     track->nDoF() );
    if ( sc ) sc = tuple->column( "TrackType",       track->type() );
    if ( sc ) sc = tuple->column( "TrackHistory",    track->history() );
    if ( sc ) sc = tuple->column( "TrackGhostProb",  track->ghostProbability() );
    if ( sc ) sc = tuple->column( "TrackLikelihood", track->likelihood() );
    if ( sc ) sc = tuple->column( "TrackCloneDist",  track->info(LHCb::Track::AdditionalInfo::CloneDist,9e10) );

    // rich
    static LHCb::RichPID tmpRPID;
    tmpRPID.setPidResultCode( static_cast<int>(proto->info(LHCb::ProtoParticle::additionalInfo::RichPIDStatus,0)) );
    if ( sc ) sc = tuple->column( "RichDLLe",      proto->info ( LHCb::ProtoParticle::additionalInfo::RichDLLe,  0 ) );
    if ( sc ) sc = tuple->column( "RichDLLmu",     proto->info ( LHCb::ProtoParticle::additionalInfo::RichDLLmu, 0 ) );
    if ( sc ) sc = tuple->column( "RichDLLpi",     proto->info ( LHCb::ProtoParticle::additionalInfo::RichDLLpi, 0 ) );
    if ( sc ) sc = tuple->column( "RichDLLk",      proto->info ( LHCb::ProtoParticle::additionalInfo::RichDLLk,  0 ) );
    if ( sc ) sc = tuple->column( "RichDLLp",      proto->info ( LHCb::ProtoParticle::additionalInfo::RichDLLp,  0 ) );
    if ( sc ) sc = tuple->column( "RichDLLd",      proto->info ( LHCb::ProtoParticle::additionalInfo::RichDLLd,  0 ) );
    if ( sc ) sc = tuple->column( "RichDLLbt",     proto->info ( LHCb::ProtoParticle::additionalInfo::RichDLLbt, 0 ) );
    if ( sc ) sc = tuple->column( "RichUsedAero",     tmpRPID.usedAerogel()  );
    if ( sc ) sc = tuple->column( "RichUsedR1Gas",    tmpRPID.usedRich1Gas() );
    if ( sc ) sc = tuple->column( "RichUsedR2Gas",    tmpRPID.usedRich2Gas() );
    if ( sc ) sc = tuple->column( "RichAboveElThres", tmpRPID.electronHypoAboveThres() );
    if ( sc ) sc = tuple->column( "RichAboveMuThres", tmpRPID.muonHypoAboveThres() );
    if ( sc ) sc = tuple->column( "RichAbovePiThres", tmpRPID.pionHypoAboveThres() );
    if ( sc ) sc = tuple->column( "RichAboveKaThres", tmpRPID.kaonHypoAboveThres() );
    if ( sc ) sc = tuple->column( "RichAbovePrThres", tmpRPID.protonHypoAboveThres() );
    if ( sc ) sc = tuple->column( "RichAboveDeThres", tmpRPID.deuteronHypoAboveThres() );
    if ( sc ) sc = tuple->column( "RichBestPID",      (int)tmpRPID.bestParticleID() );

    // muon
    static LHCb::MuonPID tmpMPID;
    tmpMPID.setStatus( static_cast<int>(proto->info(LHCb::ProtoParticle::additionalInfo::MuonPIDStatus,0)) );
    if ( sc ) sc = tuple->column( "MuonBkgLL",    proto->info ( LHCb::ProtoParticle::additionalInfo::MuonBkgLL, 0 ) );
    if ( sc ) sc = tuple->column( "MuonMuLL",     proto->info ( LHCb::ProtoParticle::additionalInfo::MuonMuLL,  0 ) );
    if ( sc ) sc = tuple->column( "MuonNShared",  proto->info ( LHCb::ProtoParticle::additionalInfo::MuonNShared, 0 ) );
    if ( sc ) sc = tuple->column( "MuonIsLooseMuon", tmpMPID.IsMuonLoose() );
    if ( sc ) sc = tuple->column( "MuonIsMuon",      tmpMPID.IsMuon() );
    if ( sc ) sc = tuple->column( "MuonInAcc",  proto->info ( LHCb::ProtoParticle::additionalInfo::InAccMuon, false ) );

    // calo
    if ( sc ) sc = tuple->column( "InAccSpd",   proto->info ( LHCb::ProtoParticle::additionalInfo::InAccSpd,  false ) );
    if ( sc ) sc = tuple->column( "InAccPrs",   proto->info ( LHCb::ProtoParticle::additionalInfo::InAccPrs,  false ) );
    if ( sc ) sc = tuple->column( "InAccEcal",  proto->info ( LHCb::ProtoParticle::additionalInfo::InAccEcal, false ) );
    if ( sc ) sc = tuple->column( "InAccHcal",  proto->info ( LHCb::ProtoParticle::additionalInfo::InAccHcal, false ) );
    if ( sc ) sc = tuple->column( "InAccBrem",  proto->info ( LHCb::ProtoParticle::additionalInfo::InAccBrem, false ) );
    if ( sc ) sc = tuple->column( "CaloTrMatch",       proto->info ( LHCb::ProtoParticle::additionalInfo::CaloTrMatch, 0 ) );
    if ( sc ) sc = tuple->column( "CaloElectronMatch", proto->info ( LHCb::ProtoParticle::additionalInfo::CaloElectronMatch, 0 ) );
    if ( sc ) sc = tuple->column( "CaloBremMatch",     proto->info ( LHCb::ProtoParticle::additionalInfo::CaloBremMatch, 0 ) );
    if ( sc ) sc = tuple->column( "CaloChargedSpd",    proto->info ( LHCb::ProtoParticle::additionalInfo::CaloChargedSpd, 0 ) );
    if ( sc ) sc = tuple->column( "CaloChargedPrs",    proto->info ( LHCb::ProtoParticle::additionalInfo::CaloChargedPrs, 0 ) );
    if ( sc ) sc = tuple->column( "CaloChargedEcal",   proto->info ( LHCb::ProtoParticle::additionalInfo::CaloChargedEcal, 0 ) );
    if ( sc ) sc = tuple->column( "CaloSpdE",   proto->info ( LHCb::ProtoParticle::additionalInfo::CaloSpdE,   0 ) );
    if ( sc ) sc = tuple->column( "CaloPrsE",   proto->info ( LHCb::ProtoParticle::additionalInfo::CaloPrsE,   0 ) );
    if ( sc ) sc = tuple->column( "CaloEcalChi2", proto->info ( LHCb::ProtoParticle::additionalInfo::CaloEcalChi2, 0 ) );
    if ( sc ) sc = tuple->column( "CaloClusChi2", proto->info ( LHCb::ProtoParticle::additionalInfo::CaloClusChi2, 0 ) );
    if ( sc ) sc = tuple->column( "CaloBremChi2", proto->info ( LHCb::ProtoParticle::additionalInfo::CaloBremChi2, 0 ) );
    if ( sc ) sc = tuple->column( "CaloPrsE",   proto->info ( LHCb::ProtoParticle::additionalInfo::CaloPrsE,   0 ) );
    if ( sc ) sc = tuple->column( "CaloEcalE",  proto->info ( LHCb::ProtoParticle::additionalInfo::CaloEcalE,  0 ) );
    if ( sc ) sc = tuple->column( "CaloHcalE",  proto->info ( LHCb::ProtoParticle::additionalInfo::CaloHcalE,  0 ) );
    if ( sc ) sc = tuple->column( "CaloTrajectoryL", proto->info ( LHCb::ProtoParticle::additionalInfo::CaloTrajectoryL, 0 ) );
    if ( sc ) sc = tuple->column( "EcalPIDe",   proto->info ( LHCb::ProtoParticle::additionalInfo::EcalPIDe,  0 ) );
    if ( sc ) sc = tuple->column( "HcalPIDe",   proto->info ( LHCb::ProtoParticle::additionalInfo::HcalPIDe,  0 ) );
    if ( sc ) sc = tuple->column( "PrsPIDe",    proto->info ( LHCb::ProtoParticle::additionalInfo::PrsPIDe,   0 ) );
    if ( sc ) sc = tuple->column( "BremPIDe",   proto->info ( LHCb::ProtoParticle::additionalInfo::BremPIDe,  0 ) );
    if ( sc ) sc = tuple->column( "EcalPIDmu",  proto->info ( LHCb::ProtoParticle::additionalInfo::EcalPIDmu, 0 ) );
    if ( sc ) sc = tuple->column( "HcalPIDmu",  proto->info ( LHCb::ProtoParticle::additionalInfo::HcalPIDmu, 0 ) );

    // combined DLLs
    if ( sc ) sc = tuple->column( "CombDLLe",   proto->info ( LHCb::ProtoParticle::additionalInfo::CombDLLe,  0 ) );
    if ( sc ) sc = tuple->column( "CombDLLmu",  proto->info ( LHCb::ProtoParticle::additionalInfo::CombDLLmu, 0 ) );
    if ( sc ) sc = tuple->column( "CombDLLpi",  proto->info ( LHCb::ProtoParticle::additionalInfo::CombDLLpi, 0 ) );
    if ( sc ) sc = tuple->column( "CombDLLk",   proto->info ( LHCb::ProtoParticle::additionalInfo::CombDLLk,  0 ) );
    if ( sc ) sc = tuple->column( "CombDLLp",   proto->info ( LHCb::ProtoParticle::additionalInfo::CombDLLp,  0 ) );
    if ( sc ) sc = tuple->column( "CombDLLd",   proto->info ( LHCb::ProtoParticle::additionalInfo::CombDLLd,  0 ) );

    // ANN PID Probabilities
    if ( sc ) sc = tuple->column( "ProbNNe",     proto->info ( LHCb::ProtoParticle::additionalInfo::ProbNNe,  -1 ) );
    if ( sc ) sc = tuple->column( "ProbNNmu",    proto->info ( LHCb::ProtoParticle::additionalInfo::ProbNNmu, -1 ) );
    if ( sc ) sc = tuple->column( "ProbNNpi",    proto->info ( LHCb::ProtoParticle::additionalInfo::ProbNNpi, -1 ) );
    if ( sc ) sc = tuple->column( "ProbNNk",     proto->info ( LHCb::ProtoParticle::additionalInfo::ProbNNk,  -1 ) );
    if ( sc ) sc = tuple->column( "ProbNNp",     proto->info ( LHCb::ProtoParticle::additionalInfo::ProbNNp,  -1 ) );
    if ( sc ) sc = tuple->column( "ProbNNghost", proto->info ( LHCb::ProtoParticle::additionalInfo::ProbNNghost, -1 ) );

    // VeloCharge
    if ( sc ) sc = tuple->column( "VeloCharge", proto->info ( LHCb::ProtoParticle::additionalInfo::VeloCharge, 0 ) );

    // MCParticle information
    const auto * mcPart = m_truth->mcParticle( track );
    if ( sc ) sc = tuple->column( "MCParticleType", mcPart ? mcPart->particleID().pid() : 0 );
    if ( sc ) sc = tuple->column( "MCParticleP",    mcPart ? mcPart->p() : -99999 );
    if ( sc ) sc = tuple->column( "MCParticlePt",   mcPart ? mcPart->pt() : -99999 );
    if ( sc ) sc = tuple->column( "MCVirtualMass",  mcPart ? mcPart->virtualMass() : -99999 );

    // write the tuple for this protoP
    if ( sc ) sc = tuple->write();

    if ( sc.isFailure() ) return sc;

  } // loop over protos

  return StatusCode::SUCCESS;
}

//=============================================================================
