/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddHcalInfo.cpp
 *
 * Implementation file for algorithm FutureChargedProtoParticleAddHcalInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------

// local
#include "FutureChargedProtoParticleAddHcalInfo.h"

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FutureChargedProtoParticleAddHcalInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FutureChargedProtoParticleAddHcalInfo::
FutureChargedProtoParticleAddHcalInfo( const std::string& name,
                                 ISvcLocator* pSvcLocator)
  : FutureChargedProtoParticleCALOFUTUREBaseAlg ( name , pSvcLocator )
{
  // default locations from context()

  using namespace LHCb::CaloFuture2Track;
  using namespace LHCb::CaloFutureIdLocation;
  using namespace LHCb::CaloFutureAlgUtils;

  m_protoPath         = LHCb::ProtoParticleLocation::Charged ;
  m_inHcalPath        = PathFromContext( context() , InHcal              );
  m_hcalEPath         = PathFromContext( context() , HcalE               );
  m_hcalPIDePath      = PathFromContext( context() , HcalPIDe            );
  m_hcalPIDmuPath     = PathFromContext( context() , HcalPIDmu           );

  declareProperty("ProtoParticleLocation"      , m_protoPath      );
  declareProperty("InputInHcalLocation"        , m_inHcalPath     );
  declareProperty("InputHcalELocation"         , m_hcalEPath      );
  declareProperty("InputHcalPIDeLocation"      , m_hcalPIDePath   );
  declareProperty("InputHcalPIDmuLocation"     , m_hcalPIDmuPath  );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode FutureChargedProtoParticleAddHcalInfo::execute()
{
  // Load the HCAL data
  const bool sc = getHcalData();
  if ( !sc )
  {
    return Warning( "No HCAL data -> ProtoParticles will not be changed.", StatusCode::SUCCESS );
  }

  // ProtoParticle container
  auto * protos = getIfExists<LHCb::ProtoParticles>(m_protoPath);
  if ( !protos )
  {
    if( msgLevel(MSG::DEBUG) ) debug() << "No existing ProtoParticle container at "
                                       <<  m_protoPath<<" thus do nothing."<<endmsg;
    return StatusCode::SUCCESS;
  }

  // Loop over proto particles and update HCAL info
  for ( auto * proto : *protos ) { addHcal(proto); }

  if ( counterStat->isQuiet() )
    counter("HcalPIDs("+context()+") ==> " + m_protoPath )+= protos->size();

  return StatusCode::SUCCESS;
}

//=============================================================================

//=============================================================================
// Loads the Calo Hcal data
//=============================================================================
bool FutureChargedProtoParticleAddHcalInfo::getHcalData()
{
  const bool sc1 = loadCaloTable(m_InHcalTable   , m_inHcalPath);
  const bool sc2 = loadCaloTable(m_HcalETable    , m_hcalEPath);
  const bool sc3 = loadCaloTable(m_dlleHcalTable , m_hcalPIDePath);
  const bool sc4 = loadCaloTable(m_dllmuHcalTable, m_hcalPIDmuPath);
  const bool sc  = sc1 && sc2 && sc3 && sc4;
  if ( sc && msgLevel(MSG::DEBUG) ) debug() << "HCAL PID SUCCESSFULLY LOADED" << endmsg;
  return sc;
}

//=============================================================================
// Add Calo Hcal info to the protoparticle
//=============================================================================
bool FutureChargedProtoParticleAddHcalInfo::addHcal( LHCb::ProtoParticle * proto ) const
{
  // clear HCAL info
  proto->removeCaloHcalInfo();

  bool hasHcalPID = false;

  const auto aRange = m_InHcalTable -> relations ( proto->track() ) ;
  if ( !aRange.empty() )
  {
    hasHcalPID = aRange.front().to();
    if( hasHcalPID )
    {
      if ( msgLevel(MSG::VERBOSE) )verbose() << " -> The track is in Hcal acceptance"  << endmsg;
      proto->addInfo(LHCb::ProtoParticle::additionalInfo::InAccHcal, true );

      // Get the HcalE (intermediate) estimator
      {
      const auto vRange = m_HcalETable -> relations ( proto->track() ) ;
      if ( !vRange.empty() ) { proto->addInfo(LHCb::ProtoParticle::additionalInfo::CaloHcalE,  vRange.front().to() ); }
      }

      // Get the Hcal DLL(e)
      {
        const auto vRange = m_dlleHcalTable -> relations ( proto->track() ) ;
        if ( !vRange.empty() ) { proto->addInfo(LHCb::ProtoParticle::additionalInfo::HcalPIDe , vRange.front().to() ); }
      }

      // Get the Hcal DLL(mu)
      {
        const auto vRange = m_dllmuHcalTable -> relations ( proto->track() ) ;
        if ( !vRange.empty() ) { proto->addInfo(LHCb::ProtoParticle::additionalInfo::HcalPIDmu , vRange.front().to() ); }
      }

      if ( msgLevel(MSG::VERBOSE) )
        verbose() << " -> Hcal PID  : "
                  << " HcalE      =" <<  proto->info(LHCb::ProtoParticle::additionalInfo::CaloHcalE, -999.)
                  << " Dlle (Hcal) =" <<  proto->info(LHCb::ProtoParticle::additionalInfo::HcalPIDe, -999.)
                  << " Dllmu (Hcal) =" <<  proto->info(LHCb::ProtoParticle::additionalInfo::HcalPIDmu, -999.)
                  << endmsg;

    }
    else
    {
      if ( msgLevel(MSG::VERBOSE) )verbose() << " -> The track is NOT in Hcal acceptance"  << endmsg;
    }
  }
  else
  {
    if ( msgLevel(MSG::VERBOSE) )verbose() << " -> No entry for that track in the Hcal acceptance table"  << endmsg;
  }

  return hasHcalPID;
}
