/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RECINIT_H
#define RECINIT_H 1

// Include files
#include "GaudiAlg/Transformer.h"
#include "Kernel/LbAppInit.h"

class IGenericTool;
class IIncidentSvc;

/** @class RecInit RecInit.h
 *  Algorithm to initialize the reconstruction sequence
 *  Creates RecHeader and ProcStatus
 *
 *  @author Marco Cattaneo, Sebastien Ponce
 */
class RecInit final :
public Gaudi::Functional::MultiTransformer<
    std::tuple<LHCb::RecHeader, LHCb::ProcStatus> (const LHCb::ODIN&, const LHCb::RawEvent&),
    Gaudi::Functional::Traits::BaseClass_t<LbAppInit>> {
public:
  /// Standard constructor
  RecInit(const std::string& name, ISvcLocator* pSvcLocator);

  /// Algorithm initialization
  StatusCode initialize() override;

  /// Algorithm execution
  std::tuple<LHCb::RecHeader, LHCb::ProcStatus> operator()(const LHCb::ODIN&,
                                                           const LHCb::RawEvent&) const override;

private:
  IGenericTool* m_memoryTool = nullptr;   ///< Pointer to (private) memory histogram tool

  Gaudi::Property<bool> m_abortOnFID{this, "AbortOnFID", true, "If I can't find the raw file ID, do I abort ?"};

};
#endif // RECINIT_H
