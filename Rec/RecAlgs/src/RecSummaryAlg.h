/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RECSUMMARYALG_H
#define RECSUMMARYALG_H 1

// STL
#include <string>
#include <map>
#include <algorithm>
#include <optional>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/AnyDataHandle.h"

// Event Model
#include "Event/Track.h"
#include "Event/RecSummary.h"
#include "Event/RecVertex.h"
#include "Event/STCluster.h"
#include "Event/UTCluster.h"
#include "Event/VeloCluster.h"
#include "Event/VeloLiteCluster.h"
#include "Event/VPLightCluster.h"
#include "Event/FTLiteCluster.h"
#include "Event/CaloDigit.h"
#include "Event/MuonCoord.h"

// RICH
#include "RichFutureUtils/RichDecodedData.h"

// tool interfaces
//#include "RichInterfaces/IRichRawBufferToSmartIDsTool.h"
#include "OTDAQ/IOTRawBankDecoder.h"
#include "L0Interfaces/IL0DUFromRawTool.h"
#include "Kernel/ICountContainedObjects.h"

// boost
#include <boost/numeric/conversion/cast.hpp>

/** @class RecSummaryAlg RecSummaryAlg.h
 *
 *  Fill the LHCb::RecSummary class with summary information from the event
 *  reconstruction.
 *
 *  @author Chris Jones
 *  @date   2011-01-19
 */
class RecSummaryAlg final : public GaudiAlgorithm
{

 public:

  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override final; ///< Algorithm execution
  StatusCode initialize() override final; ///<  Algorithm initialization

 private:

  /// Adds the number of objects at the given TES location to the summary object
  template<class CLASS, class FALLBACK = CLASS>
    inline void addSizeSummary( LHCb::RecSummary * summary,
                                const LHCb::RecSummary::DataTypes id,
                                const std::string& location ) const
  {
    std::optional<int> size;
    {
      const CLASS * data = getIfExists<CLASS>(location);
      if (data) size = boost::numeric_cast<int>(data->size());
    }
    if (!size && !std::is_same<CLASS, FALLBACK>::value) {
      const FALLBACK * data = getIfExists<FALLBACK>(location);
      if (data) size = boost::numeric_cast<int>(data->size());
    }
    if (size) {
      summary->addInfo( id, *size );
    } else {
      summary->addInfo( id, 0 );
      Warning( "No data at '" + location + "'", StatusCode::FAILURE, 0 ).ignore();
    }
  }

  /// Access on demand the OT decoder
  const IOTRawBankDecoder* otTool()
  {
    if ( !m_otTool )
    {
      m_otTool = tool<IOTRawBankDecoder>("OTRawBankDecoder");
    }
    return m_otTool;
  }

  /// Access on-demand the Velo track counter
  inline const ICountContainedObjects* countVeloTracks()
  {
    if ( !m_countVeloTracks )
    {
      m_countVeloTracks = tool<ICountContainedObjects>("CountVeloTracks");
    }
    return m_countVeloTracks;
  }

  /// Get the number of RICH hits for the given detector
  inline unsigned int nRichHits( const Rich::DetectorType rich )
  {
    // First see if the future decoded data is available via the TES
    auto base = getIfExists<AnyDataWrapperBase>(Rich::Future::DAQ::L1MapLocation::Default);
    const auto rDH = dynamic_cast< AnyDataWrapper<Rich::Future::DAQ::L1Map>* >(base);
    const auto rD  = ( rDH ? &rDH->getData() : nullptr );
    if ( !rD ) { Exception("Failed to load RICH data"); }
    return rD->nTotalHits(rich);
  }

 private:

  /// List of sub-detectors to add
  Gaudi::Property<std::vector<std::string>> m_dets { this,  "Detectors", {"RICH1", "RICH2", "VELO", "TT", "IT", "OT", "SPD", "MUON"} };

  /// List of known-detectors to add
  std::vector<std::string> m_knownDets = {"RICH1", "RICH2", "VELO", "TT", "IT", "OT", "SPD", "MUON", "VP", "UT", "FT", "RICH1PMT", "RICH2PMT"};

  /// TES location to save the summary object
  Gaudi::Property<std::string> m_summaryLoc { this, "SummaryLocation", LHCb::RecSummaryLocation::Default };

  /// Location in the TES to load the recosntructed tracks from
  Gaudi::Property<std::string> m_trackLoc { this, "TracksLocation", LHCb::TrackLocation::Default };

  /// Are split track locations used in the HLT
  Gaudi::Property<bool> m_split { this, "HltSplitTracks", false };

  /// Location in the TES of split long tracks in the HLT
  Gaudi::Property<std::string> m_trackLongLoc { this,  "SplitLongTracksLocation", LHCb::TrackLocation::Default };

  /// Location in the TES of split downstream tracks in the HLT
  Gaudi::Property<std::string> m_trackDownLoc { this,  "SplitDownTracksLocation", LHCb::TrackLocation::Default };

  /// Location in the TES to load the reconstructed PVs from
  Gaudi::Property<std::string> m_pvLoc { this,  "PVsLocation", LHCb::RecVertexLocation::Primary };

  /// TES location of Velo clusters
  Gaudi::Property<std::string> m_veloLoc { this,  "VeloClustersLocation", LHCb::VeloClusterLocation::Default };

  /// TES location of VP clusters
  Gaudi::Property<std::string> m_vpLoc { this,  "VPClustersLocation", LHCb::VPClusterLocation::Light };

  /// TES location of IT clusters
  Gaudi::Property<std::string> m_itLoc { this,  "ITClustersLocation", LHCb::STClusterLocation::ITClusters };

  /// TES location of TT clusters
  Gaudi::Property<std::string> m_ttLoc { this,  "TTClustersLocation", LHCb::STClusterLocation::TTClusters };

  /// TES location of UT clusters
  Gaudi::Property<std::string> m_utLoc { this,  "UTClustersLocation", LHCb::UTClusterLocation::UTClusters };

  /// TES location of FT clusters
  Gaudi::Property<std::string> m_ftLoc { this,  "FTClustersLocation", LHCb::FTLiteClusterLocation::Default };

  /// TES location of CaloDigits
  Gaudi::Property<std::string> m_spdLoc { this,  "SpdDigitsLocation", LHCb::CaloDigitLocation::Spd };

  /// TES location of Muon Coords
  Gaudi::Property<std::string> m_muonCoordsLoc { this,  "MuonCoordsLocation", LHCb::MuonCoordLocation::MuonCoords };

  /// TES location of Muon Tracks
  Gaudi::Property<std::string> m_muonTracksLoc { this,  "MuonTracksLocation", LHCb::TrackLocation::Muon };

  /// OT tool
  const IOTRawBankDecoder* m_otTool = nullptr;

  /// CountVeloTracks tool
  const ICountContainedObjects* m_countVeloTracks = nullptr;

};

#endif // RECSUMMARYALG_H
