/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/Incident.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiAlg/IGenericTool.h"
#include "GaudiKernel/IOpaqueAddress.h"

// from EventBase
#include "Event/ProcessHeader.h"

// from DAQEvent
#include "Event/RawEvent.h"
#include "Event/RawBank.h"
#include "Event/ODIN.h"

// from RecEvent
#include "Event/RecHeader.h"
#include "Event/ProcStatus.h"

// local
#include "RecInit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RecInit
//
// Marco Cattaneo, Sebastien Ponce
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RecInit )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RecInit::RecInit(const std::string& name,
                 ISvcLocator* pSvcLocator) :
MultiTransformer(name, pSvcLocator,
            { KeyValue{"InputOdinName", LHCb::ODINLocation::Default},
              KeyValue{"RawEventLocations",
                       Gaudi::Functional::concat_alternatives(LHCb::RawEventLocation::Calo,
                                                              LHCb::RawEventLocation::Default)} },
            { KeyValue{"OutputRecHeaderName", LHCb::RecHeaderLocation::Default},
              KeyValue{"OutputProcStatusName", LHCb::ProcStatusLocation::Default}}) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode RecInit::initialize() {
  // must be executed first
  const StatusCode sc = LbAppInit::initialize();
  if (sc.isFailure()) return sc;

  if (msgLevel(MSG::DEBUG)) debug() << "==> Initialize" << endmsg;

  // Private tool to plot the memory usage
  m_memoryTool = tool<IGenericTool>("MemoryTool", "BrunelMemory", this, true);

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<LHCb::RecHeader, LHCb::ProcStatus>
RecInit::operator()(const LHCb::ODIN& odin,
                    const LHCb::RawEvent& rawEvent) const {
  if (msgLevel(MSG::DEBUG)) debug() << "==> Execute" << endmsg;
  increaseEventCounter();

  // memory check
  LbAppInit::checkMem();

  // Plot the memory usage, this call is thread safe
  m_memoryTool->execute();

  const auto runNumber = odin.runNumber();
  const auto evtNumber = odin.eventNumber();

  this->printEventRun(evtNumber, runNumber, 0, odin.eventTime());

  // Initialize the random number
  const auto seeds = getSeeds(runNumber, evtNumber);
  if (this->initRndm(seeds).isFailure()) {
    throw GaudiException("RecInit", "Failure while calling initRndm",
                         StatusCode::FAILURE);  // error printed already by initRndm
  }
  // get the file ID from the raw event
  std::string event_fname;

  auto* eAddr = rawEvent.registry()->address();
  // obtain the fileID
  if (eAddr) {
    event_fname = eAddr->par()[0];
    if (msgLevel(MSG::DEBUG)) debug() << "RunInfo record from Event: " << event_fname << endmsg;
  } else {
    if (m_abortOnFID) {
      throw GaudiException("RecInit", "Registry cannot be loaded from Event, fileID cannot be found",
                           StatusCode::FAILURE);
    }
  }
  const auto& rawID = event_fname;
  
  // Create the Reconstruction event header
  LHCb::RecHeader header;
  header.setApplicationName(this->appName());
  header.setApplicationVersion(this->appVersion());
  header.setRunNumber(runNumber);
  header.setEvtNumber(evtNumber);
  header.setCondDBTags(this->condDBTags());
  header.setGpsTime(odin.gpsTime());
  header.setRawID(rawID);

  return std::make_tuple(std::move(header), LHCb::ProcStatus());
}
