/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_TRACKHYBRIDLOCK_H
#define LOKI_TRACKHYBRIDLOCK_H 1
// ============================================================================
// $URL$
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Interface.h"
#include "LoKi/ITrackFunctorAntiFactory.h"
#include "LoKi/Context.h"
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Hybrid
  {
    // ========================================================================
    /** @class TrackFactoryLock LoKi/TrackFactoryLock.h
     *
     *  Helper class (sentry) to connect ITrackFunctorAntiFactory to TrackEngine
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2007-06-09
     *
     */
    class TrackFactoryLock
    {
    public:
      // ======================================================================
      /// contructor : Lock
      TrackFactoryLock  
      ( const LoKi::ITrackFunctorAntiFactory* tool    , 
        const LoKi::Context&                  context ) ;
      /// destructor : UnLock
      virtual ~TrackFactoryLock () ;                    // destrcutor : UnLock
      // ======================================================================
    private:
      // ======================================================================
      /// no default constructor
      TrackFactoryLock () ;                     // no default constructor
      /// no copy constructor
      TrackFactoryLock ( const TrackFactoryLock& ) ; // no copy constructor
      /// no assignement opeartor
      TrackFactoryLock& operator = ( const TrackFactoryLock& ) ;
      // ======================================================================
    private:
      // ======================================================================
      /// the tool itself
      LoKi::Interface<LoKi::ITrackFunctorAntiFactory> m_tool ; // the tool
      // ======================================================================
    } ;
    // ========================================================================
  } //                                        The end of namespace LoKi::Hybrid
  // ==========================================================================
} //                                                  The end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_HYBRIDLOCK_H
// ============================================================================
