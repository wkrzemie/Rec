/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_ITRACKHYBRIDFACTORY_H
#define LOKI_ITRACKHYBRIDFACTORY_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <string>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/StatusCode.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/TrackTypes.h"
// ============================================================================
/** @file
 *  This file is part of LoKi project:
 *   ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 */
namespace LoKi
{
  // ==========================================================================
  /** @class ITrackFunctorFactory LoKi/ITrackFunctorFactory.h
   *  The abstract interface to "hybrid factory"
   *  @author Vanya BELYAEV ibelayev@physics.syr.edu
   *  @date   2007-06-10
   */
  struct ITrackFunctorFactory : virtual IAlgTool
  {
    // ========================================================================
    /// InterfaceID
    DeclareInterfaceID ( ITrackFunctorFactory , 2 , 0 ) ;
    // ========================================================================
    /** "Factory": get the the object form python code
     *  @param pycode the python pseudo-code of the function
     *  @param cuts the placeholder for the result
     *  @param context the context lines to be executed
     *  @return StatusCode
     */
    virtual StatusCode get( const std::string&   pycode       ,
                            LoKi::Types::TrCut&  cuts         ,
                            const std::string&   context = "" ) = 0 ;
    // ========================================================================
    /** "Factory": get the the object form python code
     *  @param pycode the python pseudo-code of the function
     *  @param func the placeholder for the result
     *  @param context the context lines to be executed
     *  @return StatusCode
     */
    virtual StatusCode get ( const std::string&   pycode       ,
                             LoKi::Types::TrFun&  func         ,
                             const std::string&   context = "" ) = 0 ;
    // ========================================================================
    // functional part for LHCb::Track
    // ========================================================================
    /** "Factory": get the the object form python code
     *  @param pycode the python pseudo-code of the function
     *  @param func the placeholder for the result
     *  @param context the context lines to be executed
     *  @return StatusCode
     */
    virtual StatusCode get ( const std::string&   pycode       ,
                             LoKi::Types::TrMap&  func         ,
                             const std::string&   context = "" ) = 0 ;
    // ========================================================================
    /** "Factory": get the the object form python code
     *  @param pycode the python pseudo-code of the function
     *  @param func the placeholder for the result
     *  @param context the context lines to be executed
     *  @return StatusCode
     */
    virtual StatusCode get ( const std::string&   pycode       ,
                             LoKi::Types::TrPipe& func         ,
                             const std::string&   context = "" ) = 0 ;
    // ========================================================================
    /** "Factory": get the the object form python code
     *  @param pycode the python pseudo-code of the function
     *  @param func the placeholder for the result
     *  @param context the context lines to be executed
     *  @return StatusCode
     */
    virtual StatusCode get ( const std::string&     pycode       ,
                             LoKi::Types::TrFunVal& func         ,
                             const std::string&     context = "" ) = 0 ;
    // ========================================================================
    /** "Factory": get the the object form python code
     *  @param pycode the python pseudo-code of the function
     *  @param func the placeholder for the result
     *  @param context the context lines to be executed
     *  @return StatusCode
     */
    virtual StatusCode get ( const std::string&     pycode       ,
                             LoKi::Types::TrCutVal& func         ,
                             const std::string&     context = "" ) = 0 ;
    // ========================================================================
    /** "Factory": get the the object form python code
     *  @param pycode the python pseudo-code of the function
     *  @param func the placeholder for the result
     *  @param context the context lines to be executed
     *  @return StatusCode
     */
    virtual StatusCode get ( const std::string&     pycode       ,
                             LoKi::Types::TrSource& func         ,
                             const std::string&     context = "" ) = 0 ;
    // ========================================================================
  } ;
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_ITRACKHYBRIDFACTORY_H
// ============================================================================
