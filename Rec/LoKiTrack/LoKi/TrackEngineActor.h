/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ===========================================================================
#ifndef LOKI_TRACKENGINEACTOR_H
#define LOKI_TRACKENGINEACTOR_H 1
// ===========================================================================
// Include files
// ===========================================================================
// STD&STL
// ===========================================================================
#include <stack>
// ===========================================================================
// LoKi
// ===========================================================================
#include "LoKi/Interface.h"
#include "LoKi/ITrackFunctorAntiFactory.h"
#include "LoKi/Context.h"
// ===========================================================================
namespace LoKi
{
  // =========================================================================
  namespace Hybrid
  {
    // =======================================================================
    /** @class TrEngineActor LoKi/TrackEngineActor.h
     *
     *  This file is a part of LoKi project -
     *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
     *
     *  The package has been designed with the kind help from
     *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
     *  contributions and advices from G.Raven, J.van Tilburg,
     *  A.Golutvin, P.Koppenburg have been used in the design.
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2004-06-29
     */
    class TrackEngineActor
    {
    public:
      // ======================================================================
      // get the static instance
      static TrackEngineActor& instance() ;
      // ======================================================================
      /// connect the hybrid tool for code translation
      StatusCode connect    ( const LoKi::ITrackFunctorAntiFactory* tool    ,
                              const LoKi::Context&                  context ) ;
      // ======================================================================
      /// disconnect the tool
      StatusCode disconnect ( const LoKi::ITrackFunctorAntiFactory* tool ) ;
      // ======================================================================
      /** get the current context
       *  contex is valid only inbetween <code>connect/disconnect</code>
       *  @return the current active context 
       */
      const LoKi::Context* context () const ;
      // ======================================================================
    public:
      // ======================================================================
      /// propagate the cut to the tool
      StatusCode process
      ( const std::string&          name ,
        const LoKi::Types::TrCuts&  cut  ) const ;
      // ======================================================================
      /// propagate the function to the tool
      StatusCode process
      ( const std::string&          name ,
        const LoKi::Types::TrFunc&  func ) const ;
      // ======================================================================
    public:
      // ======================================================================
      // functional part for LHCb::Track
      // ======================================================================
      /// add the function
      StatusCode process
      ( const std::string&             name ,
        const LoKi::Types::TrMaps&     fun  ) const ;
      // ======================================================================
      /// add the function
      StatusCode process
      ( const std::string&             name ,
        const LoKi::Types::TrPipes&    fun  ) const ;
      // ======================================================================
      /// add the function
      StatusCode process
      ( const std::string&             name ,
        const LoKi::Types::TrFunVals&  fun  ) const ;
      // ======================================================================
      /// add the function
      StatusCode process
      ( const std::string&             name ,
        const LoKi::Types::TrSources&  fun  ) const ;
      // ======================================================================
    protected:
      // ======================================================================
      /// Standard constructor
      TrackEngineActor() ;
      // ======================================================================
    private:
      // ======================================================================
      /// just to save some lines
      template <class TYPE>
      inline StatusCode _add
      ( const std::string& name ,
        const TYPE&        cut )  const ; // save some lines
      // ======================================================================
      /// the copy contructor is disabled
      TrackEngineActor           ( const TrackEngineActor& ) = delete; // no-copy
      /// the assignement operator is disabled
      TrackEngineActor& operator=( const TrackEngineActor& ) = delete; // no-assignement
      // ======================================================================
    private:
      // ======================================================================
      typedef LoKi::Interface<LoKi::ITrackFunctorAntiFactory> Tool  ;
      typedef std::pair<Tool,LoKi::Context>                   Entry ;
      typedef std::stack<Entry>                               Stack ;
      ///  the stack of active factories 
      Stack m_stack {} ; // the stack of active factories 
      // ======================================================================
    } ;
    // ========================================================================
  } //                                        The end of namespace LoKi::Hybrid
  // ==========================================================================
} //                                                  The end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_CUTSHOLDER_H
// ============================================================================
