/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_TRACK_H
#define LOKI_TRACK_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
#include "Event/RecVertex.h"
// ============================================================================
// Track Interfaces
// ============================================================================
#include "TrackInterfaces/ITrackSelector.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/ExtraInfo.h"
#include "LoKi/TES.h"
#include "LoKi/TrackTypes.h"
#include "LoKi/Interface.h"
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  /** @namespace LoKi::Track Track.h LoKi/Track.h
   *  Namespace with few basic "track"-functors
   *  @see LHCb::Track
   *
   *  This file is a part of LoKi project -
   *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
   *
   *  The package has been designed with the kind help from
   *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
   *  contributions and advices from G.Raven, J.van Tilburg,
   *  A.Golutvin, P.Koppenburg have been used in the design.
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */
  namespace Track
  {
    using VertexContainer = std::vector<LHCb::RecVertex>;

    // ========================================================================
    /** @class Key
     *  simple evaluator of the "key"
     *  @see LoKi::Cuts::TrKEY
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-06-08
     */
    class GAUDI_API Key final : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      Key() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       Key* clone() const override;
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const  override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class InTES
     *  Simple predicate which check is teh obejct registered in TES
     *  @see LoKi::Cuts::TrINTES
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-06-08
     */
    class GAUDI_API InTES final : public LoKi::BasicFunctors<const LHCb::Track*>::Predicate
    {
    public:
      // ======================================================================
      /// Default Constructor
      InTES() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       InTES* clone() const override { return new InTES(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrINTES" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class Charge
     *  simple evaluator of the charge of the track
     *  @see LoKi::Cuts::TrQ
     *  @author Vanya BELYAEV ibelyaev@physiocs.syr.edu
     *  @date   2007-06-08
     */
    class GAUDI_API Charge final : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      Charge() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       Charge* clone() const override { return new Charge(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrQ" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class TransverseMomentum
     *  simple evaluator of transverse momentum of the track
     *  @see LoKi::Cuts::TrPT
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-06-08
     */
    class GAUDI_API TransverseMomentum final
      : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      TransverseMomentum() : AuxFunBase( std::tie() ) { }
      /// MANDATORY: clone method ("virtual constructor")
       TransverseMomentum* clone() const override
      { return new TransverseMomentum(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrPT" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class Momentum
     *  @see LoKi::Cuts::TrP
     *  simple evaluator of momentum of the track
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-06-08
     */
    class GAUDI_API Momentum : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      Momentum() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       Momentum* clone() const override { return new Momentum(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrP" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class Phi
     *  @see LoKi::Cuts::TrPHI
     *  @see LHCb::Track::phi
     *  simple evaluator of phi angle for the track
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2015-01-30
     */
    class GAUDI_API Phi final : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      Phi() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       Phi* clone() const override { return new Phi(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrPHI" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class Eta
     *  @see LoKi::Cuts::TrETA
     *  @see LHCb::Track::pseudoRapidity
     *  simple evaluator of pseudorapidity for the track
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2015-01-30
     */
    class GAUDI_API Eta final : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      Eta () : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       Eta* clone() const override { return new Eta(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrETA" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class MomentumX
     *  @see LoKi::Cuts::TrPX
     *  simple evaluator of momentum of the track
     *  @author Vanya BELYAEV Ivan.BElyaev@cern.ch
     *  @date   2011-03-18
     */
    class GAUDI_API MomentumX final : public LoKi::Track::Momentum
    {
    public:
      // ======================================================================
      /// Default Constructor
      MomentumX() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       MomentumX* clone() const override { return new MomentumX(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrPX" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class MomentumY
     *  @see LoKi::Cuts::TrPY
     *  simple evaluator of momentum of the track
     *  @author Vanya BELYAEV Ivan.BElyaev@cern.ch
     *  @date   2011-03-18
     */
    class GAUDI_API MomentumY final : public LoKi::Track::Momentum
    {
    public:
      // ======================================================================
      /// Default Constructor
      MomentumY() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       MomentumY* clone() const override { return new MomentumY(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrPY" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class MomentumZ
     *  @see LoKi::Cuts::TrPZ
     *  simple evaluator of momentum of the track
     *  @author Vanya BELYAEV Ivan.BElyaev@cern.ch
     *  @date   2011-03-18
     */
    class GAUDI_API MomentumZ final : public LoKi::Track::Momentum
    {
    public:
      // ======================================================================
      /// Default Constructor
      MomentumZ() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       MomentumZ* clone() const override { return new MomentumZ(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrPZ" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class CheckFlag
     *  @see LoKi::Cuts::TrISFLAG
     *  @see LoKi::Cuts::TrBACKWARD
     *  @see LoKi::Cuts::TrINVALID
     *  @see LoKi::Cuts::TrCLONE
     *  @see LoKi::Cuts::TrUSED
     *  @see LoKi::Cuts::TrIPSELECTED
     *  @see LoKi::Cuts::TrPIDSELECTED
     *  @see LoKi::Cuts::TrSELECTED
     *  @see LoKi::Cuts::TrL0CANDIDATE
     *  @simple predicate to check the flag
     *  @author Vanya BELYAEV ibelyaev@physiocs.syr.edu
     *  @date   2007-06-08
     */
    class GAUDI_API CheckFlag final
      : public LoKi::BasicFunctors<const LHCb::Track*>::Predicate
    {
    public:
      // ======================================================================
      /// constructor form the flag ;
      CheckFlag ( LHCb::Track::Flags flag ) ;
      /// MANDATORY: clone method ("virtual constructor")
       CheckFlag* clone() const override { return new CheckFlag (*this) ; }
      /// MANDATORY: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      // the flag to be checked:
      LHCb::Track::Flags m_flag ; ///< the flag to be checked:
      // ======================================================================
    } ;
    // ========================================================================
    /** @class Selector
     *  Simple class GAUDI_API to use "track-selector"
     *  @see ITrackSelector
     *  @see LoKi:Cuts::TrSELECTOR
     *  @author Vanya BELYAEV ibelayev@physics.syr.edu
     *  @date 2007-06-10
     */
    class GAUDI_API Selector
      : public LoKi::BasicFunctors<const LHCb::Track*>::Predicate
    {
    public :
      // ======================================================================
      /// default constructor
      Selector ( ) ;
      /// constructor form the tool
      Selector ( const ITrackSelector*  tool ) ;
      /// constructor form the tool
      Selector ( const LoKi::Interface<ITrackSelector>& tool ) ;
      /// MANDATORY: clone method ("virtual constructor")
       Selector* clone() const override { return new Selector(*this) ; }
      /// MANDATORY: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// conversion operator to the tool
      operator const LoKi::Interface<ITrackSelector>&() const { return m_tool ;}
      const LoKi::Interface<ITrackSelector>& selector() const { return m_tool ;}
      // ======================================================================
      /// the only one essential method
      bool eval ( const LHCb::Track* t ) const override;
      // ======================================================================
    protected:
      // ======================================================================
      /// set new selector tool
      void setSelector ( const ITrackSelector* selector ) const ;
      // ======================================================================
    protected:
      // ======================================================================
      /// the tool itself
      mutable LoKi::Interface<ITrackSelector> m_tool ;            // the tool itself
      // ======================================================================
    } ;
    // ========================================================================
    /** @class HasInfo
     *  Trivial predicate which evaluates LHCb::Track::hasInfo
     *  function
     *
     *  It relies on the method LoKi::Info::hasInfo
     *
     *  @see LHCb::Track
     *  @see LoKi::Cuts::TrHASINFO
     *  @see LoKi::ExtraInfo::CheckInfo
     *  @see LoKi::ExtraInfo::hasInfo
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-15
     */
    class GAUDI_API HasInfo final : public LoKi::ExtraInfo::CheckInfo<const LHCb::Track*>
    {
    public:
      // ======================================================================
      /** constructor from "info"
       *  @param key info index/mark/key
       */
      HasInfo ( const int key ) ;
      /// clone method (mandatory!)
      HasInfo* clone() const override { return new HasInfo(*this) ; }
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class Info
     *  Trivial function which evaluates LHCb::Track::info
     *
     *  It relies on the method LoKi::ExtraInfo::info
     *
     *  @see LHCb::Track
     *  @see LoKi::Cuts::TrINFO
     *  @see LoKi::ExtraInfo::GeInfo
     *  @see LoKi::ExtraInfo::info
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-15
     */
    class GAUDI_API Info final : public LoKi::ExtraInfo::GetInfo<const LHCb::Track*>
    {
    public:
      // ======================================================================
      /** constructor from "info"
       *  @param key info index/mark/key
       *  @param def default value for missing key/invalid object
       */
      Info ( const int    key , const double def ) ;
      /// clone method (mandatory!)
      Info* clone() const override { return new Info(*this); }
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class SmartInfo
     *  Trivial function which:
     *    - checks the presence of informnation in LHCb::Track::extraInfo
     *    - if the information present, it returns it
     *    - for missing infomation, use function to evaluate it
     *    - (optionally) fill th emissing field
     *
     *  @see LHCb::Track
     *  @see LoKi::Cuts::TrSINFO
     *  @see LoKi::ExtraInfo::GetSmartInfo
     *  @see LoKi::ExtraInfo::info
     *  @see LoKi::ExtraInfo::hasInfo
     *  @see LoKi::ExtraInfo::setInfo
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-15
     */
    class GAUDI_API SmartInfo final
      : public LoKi::ExtraInfo::GetSmartInfo<const LHCb::Track*>
    {
    public:
      // ======================================================================
      /** constructor from fuction, key and update-flag
       *  @param index the key in LHCb::Track::extraInfo table
       *  @param fun functionto be evaluated for missing keys
       *  @param update the flag to allow the insert of mnissing information
       */
      SmartInfo
      ( const int                                                index          ,
        const LoKi::BasicFunctors<const LHCb::Track*>::Function& fun            ,
        const bool                                               update = false ) ;
      /// clone method (mandatory!)
       SmartInfo* clone() const override { return new SmartInfo(*this); }
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class Chi2
     *  simple evaluator of the LHcb::Track::chi2
     *  @see LoKi::Cuts::TrCHI2
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-06-08
     */
    class GAUDI_API Chi2 : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      Chi2() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       Chi2* clone() const override { return new Chi2(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrCHI2" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class Chi2PerDoF
     *  simple evaluator of the LHcb::Track::chi2PerDoF
     *  @see LoKi::Cuts::TrCHI2PDOF
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date   2007-06-08
     */
    class GAUDI_API Chi2PerDoF final : public Chi2
    {
    public:
      // ======================================================================
      /// Default Constructor
      Chi2PerDoF() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       Chi2PerDoF* clone() const override { return new Chi2PerDoF(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrCHI2PDOF" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class ProbChi2
     *  simple evaluator of the LHCb::Track::probChi2
     *  @see LoKi::Cuts::TrPROBCHI2
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-06-08
     */
    class GAUDI_API ProbChi2 final : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      ProbChi2() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       ProbChi2* clone() const override { return new ProbChi2(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrPROBCHI2" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class GhostProb
     *  simple evaluator of the LHCb::Track::ghostProbability
     *  @see LoKi::Cuts::TrGHOSTPROB
     *  @author Sascha Stahl sascha.stahl@cern.ch
     *  @date   2016-01-12
     */
    class GAUDI_API GhostProb final : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      GhostProb() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       GhostProb* clone() const override { return new GhostProb(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrGHOSTPROB" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class HasStateAt
     *  Simple predicate which evaluates LHCb::Track::HasStateAt
     *  @see LoKi::Cuts::TrHASSTATE
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-06-08
     */
    class GAUDI_API HasStateAt final : public LoKi::BasicFunctors<const LHCb::Track*>::Predicate
    {
    public:
      // ======================================================================
      /// constructor
      HasStateAt ( const LHCb::State::Location& loc ) ;
      /// MANDATORY: clone method ("virtual constructor")
       HasStateAt* clone() const override { return new HasStateAt(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrHASSTATE(" << loc () << ")" ; }
      // ======================================================================
    public:
      // ======================================================================
      /// get the location
      int loc () const { return m_loc ; }
      // ======================================================================
    private:
      // ======================================================================
      /// the location
      LHCb::State::Location m_loc ;                             // the location
      // ======================================================================
    } ;
    // ========================================================================
    /** @class IsOnTrack
     *  Simple predicate which evaluates LHCb::Track::isOnTrack
     *  @see LoKi::Cuts::TrISONTRACK
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-06-08
     */
    class GAUDI_API IsOnTrack final : public LoKi::BasicFunctors<const LHCb::Track*>::Predicate
    {
    public:
      // ======================================================================
      IsOnTrack ( const LHCb::LHCbID& id ) ;
      /// MANDATORY: clone method ("virtual constructor")
       IsOnTrack* clone() const override { return new IsOnTrack(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrIsOnTRACK(LHCb.LHCbID(" << m_id.lhcbID() << "))" ; }
      // ======================================================================
    private:
      // ======================================================================
      LHCb::LHCbID m_id ;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class Type
     *  simple evaluator of the LHCb::Track::type
     *  @see LHCb::Track::type
     *  @see LHCb::Track::Types
     *  @see LoKi::Cuts::TrTYPE
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-06-08
     */
    class GAUDI_API Type final : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      Type() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       Type* clone() const override { return new Type(*this) ; }
      /// mandatory: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrTYPE" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class StateZ
     *  check Z-position for the given state
     *  @see LoKi::Cuts::TrSTATEZ
     *  @see LoKi::Cuts::TrFIRSTHITZ
     *  @see LHCb::State::Location
     *  @author Vanya Belyaev@nikhef.nl
     *  @date 2010-06-02
     */
    class GAUDI_API StateZ final
      : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// constructor with the state indicator
      StateZ
      ( const LHCb::State::Location location = LHCb::State::Location::FirstMeasurement ) ;
      /// constructor with the state indicator & bad value
      StateZ ( const LHCb::State::Location location ,
               const double       bad      ) ;
      /// MANDATORY: clone method ("virtual constructor")
       StateZ* clone () const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// get the string representation of the state
      const std::string& state() const ; // the string representation
      // ======================================================================
    private:
      // ======================================================================
      /// the state itself
      LHCb::State::Location m_state ;                       // the state itself
      /// the bad value
      double       m_bad   ;                                //    the bad value
      /// the state
      mutable std::string m__state ;                        //        the state
      // ======================================================================
    } ;
    // ========================================================================
    /** @class Cov2
     *  Get the element of covarinace element for the track state
     *  @see Tr_COV2
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2010-12-09
     */
    class GAUDI_API Cov2 final
      : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from indices
      Cov2 ( const unsigned short i ,
             const unsigned short j ) ;
      /// constructor from indices & state location
      Cov2 ( const LHCb::State::Location location ,
             const unsigned short        i        ,
             const unsigned short        j        ) ;
      /// constructor from the indices and Z-position:
      Cov2 ( const double                z        ,
             const unsigned short        i        ,
             const unsigned short        j        ) ;
      /// MANDATORY: clone method ("virtual constructor")
       Cov2* clone () const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructor is disabled
      Cov2 () ;                          // the default constructor is disabled
      // ======================================================================
    private:
      // ======================================================================
      enum _Case {
        _First    ,
        _Location ,
        _Z
      } ;
      // ======================================================================
    private:
      // ======================================================================
      _Case                 m_case ;
      double                m_z    ;
      LHCb::State::Location m_loc  ;
      unsigned  short       m_i    ;
      unsigned  short       m_j    ;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class NVeloMissed
     *  @see Hlt::MissedVeloHits
     *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
     *  @date 2011-01-28
     */
    class GAUDI_API NVeloMissed final
      : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      NVeloMissed() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method (virtual constructor)
       NVeloMissed* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override
      { return s << "TrNVELOMISS" ; }
      // ======================================================================
    } ;
    // ========================================================================
    /** @class NTHits
     *  Count number of effective T-hits : 2x#IT + #OT
     *  @see LoKi::Cuts::TrNTHITS
     *  @see LoKi::Cuts::TrTNORMIDC
     *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
     *  @date 2011-02-02
     */
    class GAUDI_API NTHits final
      : public LoKi::BasicFunctors<const LHCb::Track*>::Function
    {
    public:
      // ======================================================================
      /// Default Constructor
      NTHits() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method (virtual constructor)
       NTHits* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class HasT
     *  Check if track is of a type that goes thro T stations
     *  @see LHCb::Track::hasT
     *  @see LoKi::Cuts::TrHAST
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2011-03-18
     */
    class GAUDI_API HasT
      : public LoKi::BasicFunctors<const LHCb::Track*>::Predicate
    {
    public:
      // ======================================================================
      /// Default Constructor
      HasT() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       HasT* clone() const override { return new HasT ( *this ) ; }
      /// MANDATORY: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class HasVelo
     *  Check if track is of a type that goes thro Velo
     *  @see LHCb::Track::hasVelo
     *  @see LoKi::Cuts::TrHASVELO
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2011-03-18
     */
    class GAUDI_API HasVelo final : public LoKi::Track::HasT
    {
    public:
      // ======================================================================
      /// Default Constructor
      HasVelo() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       HasVelo* clone() const override { return new HasVelo ( *this ) ; }
      /// MANDATORY: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class HasTT
     *  Check if track is of a type that goes thro TT
     *  @see LHCb::Track::hasTT
     *  @see LoKi::Cuts::TrHASTT
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2011-03-18
     */
    class GAUDI_API HasTT final : public LoKi::Track::HasT
    {
    public:
      // ======================================================================
      /// Default Constructor
      HasTT() : AuxFunBase{ std::tie() } { }
      /// MANDATORY: clone method ("virtual constructor")
       HasTT* clone() const override { return new HasTT ( *this ) ; }
      /// MANDATORY: the only one essential method
      result_type operator() ( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;

    // ========================================================================
    /** @class MinimalImpactParameterChi2
    *  @see LoKi::Track::TrMINIPCHI2
    *  Evaluate the minimum impact parameter chi2 of the track to any vertex in the given location.
    *  @author Olli Lupton
    */
    class GAUDI_API MinimalImpactParameterChi2 final : public TrackTypes::TrFunc, public LoKi::TES::DataHandle<VertexContainer>
    {
    public:
      MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm, const std::string& location );
      MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm );
      MinimalImpactParameterChi2* clone() const override { return new MinimalImpactParameterChi2( *this ); };
      result_type operator()( argument track ) const override;
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    // ========================================================================
    /** @class MinimalImpactParameterChi2Cut
      *  @see LoKi::Track::TrMINIPCHI2CUT
      *  Check whether the minimum impact parameter chi2 of the track to any vertex in the given location exceeds the
      *  given cut.
      *  @author Olli Lupton
      */
    class GAUDI_API MinimalImpactParameterChi2Cut final : public TrackTypes::TrCuts, public LoKi::TES::DataHandle<VertexContainer>
    {
      double m_ipchi2_cut{0.0};
    public:
      MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut, const std::string& location );
      MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut );
      MinimalImpactParameterChi2Cut* clone() const override { return new MinimalImpactParameterChi2Cut( *this ); };
      result_type operator()( argument track ) const override;
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    // ========================================================================
    /// hash
    GAUDI_API std::size_t hash ( const LHCb::Track* track ) ;
    // ========================================================================
  } //                                            end of namespace LoKi::Track
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_TRACKS_H
// ============================================================================
