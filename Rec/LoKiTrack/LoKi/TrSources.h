/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_TRSOURCES_H
#define LOKI_TRSOURCES_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IDataProviderSvc.h"
// ============================================================================
// TrackEvent/RecEvent
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/BasicFunctors.h"
#include "LoKi/Interface.h"
#include "LoKi/TrackTypes.h"
#include "LoKi/Sources.h"
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Track
  {
    // ========================================================================
    /** @class SourceTES
     *
     *  @see LoKi::Cuts::TrSOURCE
     *  simple "source for the tracks from TES "
     *  @author Vanya BELYAEV ibelyav@physics.syr.edu
     *  @date 2006-12-07
     */
    class SourceTES
      : public LoKi::Functors::Source<LHCb::Track              ,
                                      LHCb::Track::ConstVector ,
                                      LHCb::Track::Container   >
    {
      // ======================================================================
      typedef LoKi::BasicFunctors<const LHCb::Track*>::Source    _Source ;
      typedef LoKi::Functors::Source<LHCb::Track              ,
                                     LHCb::Track::ConstVector ,
                                     LHCb::Track::Container   >  _Base   ;
      // ======================================================================
      static_assert(std::is_base_of<_Source,_Base>::value, "Invalid base") ;
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from the service & TES location 
      SourceTES
      ( const IDataProviderSvc*         svc  ,
        const std::string&              path ) ;
      /// constructor from the service, TES location and cuts
      SourceTES
      ( const IDataProviderSvc*         svc  ,
        const std::string&              path ,
        const LoKi::TrackTypes::TrCuts& cuts ) ;
      /// constructor from the algorithm & TES location 
      SourceTES
      ( const GaudiAlgorithm*           svc                  ,
        const std::string&              path                 ,
        const bool                      useRootInTES  = true ) ;
      /// constructor from the algorithm , TES location and cuts
      SourceTES
      ( const GaudiAlgorithm*           svc                  ,
        const std::string&              path                 ,      
        const LoKi::TrackTypes::TrCuts& cuts                 ,
        const bool                      useRootInTES  = true ) ;
      /// MANDATORY: clone method ("virtual constructor")
       SourceTES* clone() const  override;
      /// MANDATORY: the only essential method:
      result_type operator() ( /* argument */ ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& o ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// get the cuts :
      const LoKi::TrackTypes::TrCuts& cut() const { return m_cut ; }
      // ======================================================================
    public:
      // ======================================================================
      /// get the tracks from the certain  TES location
      LHCb::Track::Range get   ( const bool exc = true ) const ;
      std::size_t        count ( const bool exc = true ) const ;
      // ======================================================================
    private:
      // ======================================================================
      /// 'on-flight' filter
      LoKi::TrackTypes::TrCut   m_cut ;              //      'on-flight' filter
      // ======================================================================
    } ;
    // ========================================================================
    /** @class TESData
     *  special source that relies on DataHandle to access data in TES 
     *  @see LoKi::TES::DataHanble
     *  @see DataObjectReadHandle
     *  @see LoKi::Cuts::TrTESDATA
     *  @author Vanya BELYAEV Ivan.BElyaev@itep.ru
     *  @date    2018-08-20
     */
    class TESData 
      : public LoKi::BasicFunctors<const LHCb::Track*>::Source
      , public LoKi::TES::DataHandle<Gaudi::Range_<LHCb::Track::ConstVector> >
        // , public LoKi::TES::DataHandle<LHCb::Track::Range> 
    {
      typedef Gaudi::Range_<LHCb::Track::ConstVector> Range_ ;
      // typedef LHCb::Track::Range Range_ ;
    public:
      // ======================================================================
      /// constructor
      TESData  ( const GaudiAlgorithm*           algorithm , 
                 const std::string&              location  ) ;
      /// constructor with cuts 
      TESData  ( const GaudiAlgorithm*           algorithm , 
                 const std::string&              location  , 
                 const LoKi::TrackTypes::TrCuts& cuts      ) ;
      /// MANDATORY: clone method ("virtual constructor")
      TESData* clone() const override ;
      /// MANDATORY: the only essential method:
      result_type operator() () const override ;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& o ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// 'on-flight' filter
      LoKi::TrackTypes::TrCut m_cuts ; // 'on-flight' filter
      // ======================================================================
    } ;
    // ========================================================================
    /** @class TESCounter
     *  simple functor to count number of 'good'-objects form TES
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TrNUM
     *  @date   2010-10-24
     */
    class TESCounter : public LoKi::Functor<void,double>
    {
    public:
      // ======================================================================
      /// constructor from the source 
      explicit TESCounter 
        ( const LoKi::BasicFunctors<const LHCb::Track*>::Source& s ) ;
      // =============================================================================
      /// MANDATORY: clone method ("virtual constructor")
      TESCounter* clone () const  override;
      /// MANDATORY: the only essential method:
      double operator() () const  override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& o ) const  override;
      // ======================================================================
    private:
      // ======================================================================
      /// the actual source
      LoKi::Assignable_t<LoKi::BasicFunctors<const LHCb::Track*>::Source> 
      m_source ;                                         // the actual source
      // ======================================================================
    } ;
    // ========================================================================
  } //                                         The end of namespace LoKi::Track
  // ==========================================================================
} //                                                  The end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_TRSOURCES_H
// ============================================================================
