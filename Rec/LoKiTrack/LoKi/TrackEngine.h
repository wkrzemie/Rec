/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ===========================================================================
#ifndef LOKI_TRACKENGINE_H
#define LOKI_TRACKENGINE_H 1
// ===========================================================================
// Include files
// ===========================================================================
#include <string>
// ===========================================================================
// LoKi
// ===========================================================================
#include "LoKi/TrackTypes.h"
#include "LoKi/Context.h"
// ===========================================================================
namespace LoKi
{
  // =========================================================================
  namespace Hybrid
  {
    // =======================================================================
    /** @class TrackEngine LoKi/TrackEngine.h
     *
     *  Helper class for implementation of Hybrid Tools
     *
     *  This file is a part of LoKi project -
     *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
     *
     *  The package has been designed with the kind help from
     *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
     *  contributions and advices from G.Raven, J.van Tilburg,
     *  A.Golutvin, P.Koppenburg have been used in the design.
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2004-06-30e
     */
    class TrackEngine
    {
    public:
      // ======================================================================
      /// add the cut
      StatusCode process
      ( const std::string&          name ,
        const LoKi::Types::TrCuts&  cut  ) const ;
      // ======================================================================
      /// add the function
      StatusCode process
      ( const std::string&          name ,
        const LoKi::Types::TrFunc&  func ) const ;
      // ======================================================================
    public:
      // ======================================================================
      // functional part
      // ======================================================================
      /// add the function
      StatusCode process
      ( const std::string&             name ,
        const LoKi::Types::TrMaps&     fun  ) const ;
      // ======================================================================
      /// add the function
      StatusCode process
      ( const std::string&             name ,
        const LoKi::Types::TrPipes&    fun  ) const ;
      // ======================================================================
      /// add the function
      StatusCode process
      ( const std::string&             name ,
        const LoKi::Types::TrFunVals&  fun  ) const ;
      // ======================================================================
      /// add the function
      StatusCode process
      ( const std::string&             name ,
        const LoKi::Types::TrSources&  fun  ) const ;
      // ======================================================================
    public:
      // ======================================================================
      /// get the current context 
      const LoKi::Context* context () const ;
      // ======================================================================      
    } ;
    // ========================================================================
  } //                                        The end of namespace LoKi::Hybrid
  // ==========================================================================
} //                                                  The end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_CUTSHOLDERHELPER_H
// ============================================================================
