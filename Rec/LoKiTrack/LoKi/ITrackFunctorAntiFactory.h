/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_ITRACKHYBRIDTOOL_H
#define LOKI_ITRACKHYBRIDTOOL_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <string>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/StatusCode.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/TrackTypes.h"
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  /** @class ITrackFunctorAntiFactory LoKi/ITrackfunctorAntiFactory
   *
   *  Helper interface for implementation of C++/Python "Hybrid" solution
   *
   *  This file is a part of LoKi project -
   *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
   *
   *  The package has been designed with the kind help from
   *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
   *  contributions and advices from G.Raven, J.van Tilburg,
   *  A.Golutvin, P.Koppenburg have been used in the design.
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2004-06-29
   */
  struct ITrackFunctorAntiFactory : virtual IAlgTool
  {
    // ========================================================================
    /// InterfaceID
    DeclareInterfaceID ( ITrackFunctorAntiFactory , 2 , 0 ) ;
    // ========================================================================
    /// set the C++ predicate for LHCb::Track
    virtual void set ( const LoKi::Types::TrCuts&    cut ) = 0 ;
    /// set the C++ function for LHCb::Track
    virtual void set ( const LoKi::Types::TrFunc&    fun ) = 0 ;
    // ========================================================================
    // the functional part
    /// set the C++ "maps"     for Tracks
    virtual void set ( const LoKi::Types::TrMaps&     fun ) = 0 ;
    /// set the C++ "pipes"    for Tracks
    virtual void set ( const LoKi::Types::TrPipes&    fun ) = 0 ;
    /// set the C++ "funvals"  for Tracks
    virtual void set ( const LoKi::Types::TrFunVals&  fun ) = 0 ;
    /// set the C++ "cutvals"  for Tracks
    virtual void set ( const LoKi::Types::TrCutVals&  fun ) = 0 ;
    /// set the C++ "sources"  for Tracks
    virtual void set ( const LoKi::Types::TrSources&  fun ) = 0 ;
    // ========================================================================
  };
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_ITRHYBRIDTOOL_H
// ============================================================================
