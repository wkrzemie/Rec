/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VELORECMONITORS_VELOIPRESOLUTIONMONITORNT_H
#define VELORECMONITORS_VELOIPRESOLUTIONMONITORNT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/Consumer.h"

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/IPVOfflineTool.h"
#include "TrackInterfaces/IMaterialLocator.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVeloExpectation.h"

#include "Event/RecVertex.h"

#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Kernel/Particle2MCLinker.h"

/** @class VeloIPResolutionMonitorNT VeloIPResolutionMonitorNT.h
 *
 *  An algorithm to monitor IP resolutions as a function of 1/PT.
 *  The IP of tracks used to make a primary vertex is taken on that primary vertex.
 *  Assuming these tracks are from prompt particles, the true IP should be 0, so the
 *  calculated IP is in fact the resolution on the IP. Histograms produced are the
 *  mean of the absolute unsigned 3D IP resolution against 1/PT,
 *  and the width of a Gaussian fitted to each 1D IP resolution against 1/PT.
 *
 *  @author Michael Thomas Alexander
 *  @author Sebastien Ponce
 */

namespace Velo
{

  class VeloIPResolutionMonitorNT :
    public Gaudi::Functional::Consumer<void(const LHCb::Tracks&, const LHCb::RecVertices&),
                                            Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
  public:
    /// Standard constructor
    VeloIPResolutionMonitorNT(const std::string& name, ISvcLocator* pSvcLocator);


    /// initialize
    StatusCode initialize() override;

    /// finalize
    StatusCode finalize() override;

    /// Algorithm execution
    void operator()(const LHCb::Tracks&, const LHCb::RecVertices&) const override;

  private:

    bool m_withMC = false;
    bool m_refitPVs = true;
    bool m_checkIDs = false;
    float m_checkFrac = 0.7;
    std::string m_trackExtrapolatorType ;
    std::string m_materialLocatorType ;

    ITrackExtrapolator* m_trackExtrapolator = nullptr;
    IPVOfflineTool* m_pvtool = nullptr;
    IMaterialLocator* m_materialLocator = nullptr;
    IHitExpectation* m_TTExpectTool = nullptr;
    IVeloExpectation* m_VeloExpectTool = nullptr;

    bool m_printToolProps = false;

    // Counters
    mutable Gaudi::Accumulators::Counter<> m_eventsAnalysedCount{this, "Events Analysed"};
    mutable Gaudi::Accumulators::Counter<> m_eventsSelectedCount{this, "Events Selected"};
    mutable Gaudi::Accumulators::Counter<> m_pvsAnalysedCount{this, "PVs Analysed"};
    mutable Gaudi::Accumulators::Counter<> m_pvsSelectedCount{this, "PVs Selected"};
    mutable Gaudi::Accumulators::Counter<> m_tracksSelectedCount{this, "Tracks selected"};
    mutable Gaudi::Accumulators::Counter<> m_noVertexCount{this, "No PVs"};
    mutable Gaudi::Accumulators::Counter<> m_noTrackCount{this, "No Tracks"};
    mutable Gaudi::Accumulators::Counter<> m_ghostTrackCount{this, "Ghost tracks"};
    mutable Gaudi::Accumulators::Counter<> m_noMCPVCount{this, "No MC primary vertex"};
    mutable Gaudi::Accumulators::Counter<> m_5sigMCCount{this, "Prompt assoc. MC w/ MCPV > 5 sigma from rec PV"};
    mutable Gaudi::Accumulators::Counter<> m_MCPartCount{this, "Prompt assoc. MC particles"};
    mutable Gaudi::Accumulators::Counter<> m_2ndMCPartCount{this, "Secondary assoc. MC particles"};

    StatusCode calculateIPs(const LHCb::RecVertex*, const LHCb::Track*,
                            double&, double&, double&, double&, double&, double&, LHCb::State&, LHCb::State&) const;
    void distance(const LHCb::RecVertex*, LHCb::State, double&, double&, int) const;

    StatusCode checkMCAssoc(const LHCb::Track*, const LHCb::RecVertex*,
                            const LHCb::MCVertex*&, Gaudi::LorentzVector&,
                            unsigned int&) const;

    const LHCb::Track* matchTrack(const LHCb::Track&, const LHCb::RecVertex&) const ;
  };
}

#endif // VELORECMONITORS_VELOIPRESOLUTIONMONITOR_H
