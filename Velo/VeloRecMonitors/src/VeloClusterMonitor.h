/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VELORECMONITORS_VELOCLUSTERMONITOR_H 
#define VELORECMONITORS_VELOCLUSTERMONITOR_H 1

// Include files
// -------------
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TROOT.h"

#include "Event/VeloCluster.h"

#include "VeloMonitorBase.h"

/** @class VeloClusterMonitor VeloClusterMonitor.h
 *  
 *  @author Shanzhen Chen
 *  @date   2015-08-10
 *
 *  @author Eduardo Rodrigues
 *  @date   2008-08-18
 *
 *  @author Mark Tobin, Kazu Akiba
 *  @date   2008-06-28
 *
 *  Based on original version by
 *  @author Aras Papadelis, Thijs Versloot
 *  @date   2006-04-30
 *
 */

namespace Velo
{
  
  class VeloClusterMonitor : public VeloMonitorBase {
    
  public:
    /// Standard constructor
    VeloClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );
    
    virtual ~VeloClusterMonitor( );     ///< Destructor
    
    StatusCode initialize() override;    ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;    ///< Algorithm finalization
   
  protected:
    
  private:

    // Retrieve the VeloClusters
    StatusCode veloClusters();

    // Monitor the VeloClusters
    void monitorClusters();

    // Fill the high-multiplicity histogram
    void fillHighMultiplicity(unsigned int nClustersDecoded);

    // Get number of clusters from raw event
    unsigned int getNClustersFromRaw();

  private:
    // Data members
    std::string m_tae;

    LHCb::VeloClusters* m_clusters          =   nullptr;
    std::vector<unsigned int> m_nClustersPerSensor;

    // Counters
    mutable Gaudi::Accumulators::Counter<> m_eventsCount{this, "# events"};
    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_clutersCount{this, "# VeloClusters"};
    
    // Histograms
    AIDA::IHistogram1D* m_hNCluEvt          =   nullptr;
    AIDA::IHistogram1D* m_hNCluEvtZoom      =   nullptr;
    AIDA::IHistogram1D* m_hNClustersHM      =   nullptr;
    AIDA::IHistogram1D* m_hCluSize          =   nullptr;
    AIDA::IHistogram2D* m_hCluSizeSens      =   nullptr;
    AIDA::IHistogram2D* m_hCluADCSens       =   nullptr;
    AIDA::IHistogram1D* m_hCluADCR          =   nullptr;
    AIDA::IHistogram1D* m_hCluADCPhi        =   nullptr;
    AIDA::IHistogram1D* m_hSeedADC          =   nullptr;
    AIDA::IHistogram1D* m_hSeedADCR         =   nullptr;
    AIDA::IHistogram1D* m_hSeedADCPhi       =   nullptr;
    AIDA::IHistogram1D* m_hIncADC           =   nullptr;
    AIDA::IHistogram1D* m_hIncADCR          =   nullptr;
    AIDA::IHistogram1D* m_hIncADCPhi        =   nullptr;
    AIDA::IHistogram2D* m_hActiveLinkSens   =   nullptr;
    TH1D* m_histCluADC                      =   nullptr;
    TH1D* m_histCluADC_Sensor[84]           =   {nullptr};
    TH1D* m_clusterStrip_Sensor[84]         =   {nullptr};//
    TH1D* m_histCluADC_Sensor_Radius[84][2] =   {{nullptr}};//
    TH1D* m_histEmptyCluFracSens            =   nullptr;//
//    TH1D* m_histCluADC_FitParLandauWidth;
//    TH1D* m_histCluADC_FitParMPV;
//    TH1D* m_histCluADC_FitParArea;
//    TH1D* m_histCluADC_FitParGSigma;
    TH1D* m_histCluADC_Sensor_FitParLandauWidth =   nullptr;
    TH1D* m_histCluADC_Sensor_FitParMPV         =   nullptr;
    TH1D* m_histCluADC_Sensor_FitParArea        =   nullptr;
    TH1D* m_histCluADC_Sensor_FitParGSigma      =   nullptr;
    TH1D* m_histCluADC_Sensor_MPV               =   nullptr;
    TH1D* m_histCluADC_Sensor_FWHM              =   nullptr;
    TH1D* m_histCluADC_Sensor_FitFunction[84]   =   {nullptr};
    TF1* func[84]                               =   {nullptr};

    //std::vector<AIDA::IHistogram1D*> m_hNCluSens;
    TH1D* m_hNCluSens[84]                       =   {nullptr};//

    // Job options
    std::string m_clusterCont;
    bool        m_perSensorPlots;
    bool        m_veloOfflineDQPlots;
    bool        m_highMultiplicityPlot;
    bool        m_ADCFitParameters;
  };
}

#endif // VELORECMONITORS_VELOCLUSTERMONITOR_H
