/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VELORECMONITORS_VELOSAMPLINGMONITOR_H
#define VELORECMONITORS_VELOSAMPLINGMONITOR_H 1

// Include files
// -------------
#include "VeloDet/DeVelo.h"

// from VeloEvent
#include "VeloEvent/VeloTELL1Data.h"

// from DigiEvent
#include "Event/VeloCluster.h"

#include "TH1D.h"
#include "TH2D.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

// local
#include "VeloMonitorBase.h"

/** @class VeloSamplingMonitor VeloSamplingMonitor.h
 *
 *
 *  @author Eduardo Rodrigues
 *  @date   2008-08-20
 */

namespace Velo
{

  class VeloSamplingMonitor : public VeloMonitorBase {

  public:
    /// Standard constructor
    VeloSamplingMonitor( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;    ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;    ///< Algorithm finalization

  protected:

  private:

    // Retrieve the VeloClusters
    LHCb::VeloClusters* veloClusters( std::string samplingLocation );

    // Retrieve the VeloTell1Data
    LHCb::VeloTELL1Datas* veloTell1Data( std::string samplingLocation );

    // Monitor the VeloClusters
    void monitorClusters( std::string samplingLocation,
                          LHCb::VeloClusters* clusters );

    // Monitor the VeloTell1Data
    void monitorTell1Data( std::string samplingLocation,
                           LHCb::VeloTELL1Datas* tell1Datas );

    // Monitor difference in MPV of ADC between TAEs
    void monitorTAEDiff();

    // Data members
    const DeVelo* m_velo = nullptr;

    AIDA::IHistogram2D* m_hClusADCSampR = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampRC = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampRA = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampPhi = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampPhiC = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampPhiA = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampPU = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampPUC = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampPUA = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampTop = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampTopC = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampTopA = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampBot = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampBotC = nullptr;
    AIDA::IHistogram2D* m_hClusADCSampBotA = nullptr;
    AIDA::IHistogram2D* m_hChanADCSamp = nullptr;
    TH2D* m_histClusADCSampAll = nullptr;
    TH1D* m_histTaeADCDiffNext = nullptr;
    TH1D* m_histTaeADCDiffPrev = nullptr;
    TH1D* m_histTaeADCDiffDiff = nullptr;

    // Counters
    mutable Gaudi::Accumulators::Counter<> m_eventsCount{this, "# events"};

    // Job options
    std::vector<std::string> m_samplingLocations =
      {"Prev7","Prev6","Prev5","Prev4","Prev3",
       "Prev2","Prev1","","Next1","Next2","Next3",
       "Next4","Next5","Next6","Next7"};
    bool m_useNZS = true;

  };

}

#endif // VELORECMONITORS_VELOSAMPLINGMONITOR_H
