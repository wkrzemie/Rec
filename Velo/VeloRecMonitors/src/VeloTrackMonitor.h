/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VELORECMONITORS_VELOTRACKMONITOR_H 
#define VELORECMONITORS_VELOTRACKMONITOR_H 1

// Include files
//--------------

//from DigiEvent
#include "Event/VeloCluster.h"

//from TrackEvent
#include "Event/Track.h"
#include "Event/State.h"

//from TrackInterfaces
#include "TrackInterfaces/IVeloClusterPosition.h"
#include "TrackInterfaces/IVeloExpectation.h"

//from Gaudi
#include "GaudiAlg/IHistoTool.h"
#include "GaudiAlg/GaudiTupleAlg.h"

#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

// local
#include "VeloMonitorBase.h"

/** @class VeloTrackMonitor VeloTrackMonitor.h
 *
 *
 *  @author Zhou Xing
 *  @date   2012-02-03
 *  Low ADC Monitoring histogram added
 *  @author Sadia Khalil
 *  @date   2008-09-15
 *  Based on original version by
 *  @author Aras Papadelis, Thijs Versloot
 *  @date   2006-04-30
 */

namespace Velo
{
  class VeloTrackMonitor : public VeloMonitorBase{
  public:
    /// Standard constructor
    VeloTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator );
    virtual StatusCode initialize() override;    ///< Algorithm initialization
    virtual StatusCode execute   () override;    ///< Algorithm execution
    virtual StatusCode finalize  () override;    ///< Algorithm finalization

  private:

  protected:

    StatusCode getVeloClusters();
    StatusCode getVeloTracks();
    StatusCode MCInfo();
    StatusCode monitorTracks();
    StatusCode unbiasedResiduals(LHCb::Track *track);
    void fillM2Hits(const LHCb::VeloCluster *clus,
		    Gaudi::XYZPoint const &point,
		    const DeVeloRType & sensor);
    DeVelo* m_veloDet = nullptr;

    // Data members
    LHCb::Tracks* m_tracks;
    LHCb::VeloClusters* m_clusters;

    IVeloClusterPosition* m_clusterTool;
    IVeloExpectation* m_expectTool;

    AIDA::IHistogram1D *m_h_aliMon_Mean_R_A = nullptr;
    AIDA::IHistogram1D *m_h_aliMon_Mean_R_C = nullptr;
    AIDA::IHistogram1D *m_h_aliMon_Mean_P_A = nullptr;
    AIDA::IHistogram1D *m_h_aliMon_Mean_P_C = nullptr;
    AIDA::IHistogram1D *m_h_aliMon_Sigma_R_A = nullptr;
    AIDA::IHistogram1D *m_h_aliMon_Sigma_R_C = nullptr;
    AIDA::IHistogram1D *m_h_aliMon_Sigma_P_A = nullptr;
    AIDA::IHistogram1D *m_h_aliMon_Sigma_P_C = nullptr;
    std::vector<AIDA::IProfile1D*> m_prof_res;
    AIDA::IProfile1D* m_prof_unsensors = nullptr;
    AIDA::IProfile1D* m_prof_pos_mom_unres = nullptr;
    AIDA::IProfile1D* m_prof_neg_mom_unres = nullptr;
    AIDA::IProfile1D* m_prof_sensors = nullptr;
    AIDA::IProfile1D* m_prof_pos_mom_res = nullptr;
    AIDA::IProfile1D* m_prof_neg_mom_res = nullptr;
    AIDA::IProfile1D* m_prof_thetaR = nullptr;
    AIDA::IProfile1D* m_prof_thetaTot = nullptr;
    AIDA::IProfile1D* m_prof_pseudoEffsens = nullptr;
    AIDA::IHistogram1D* m_track_theta = nullptr;
    AIDA::IHistogram1D* m_track_phi = nullptr;
    AIDA::IHistogram1D* m_track_eta = nullptr;
    AIDA::IHistogram1D* m_track_adcsum = nullptr;
    AIDA::IHistogram1D* m_track_radc = nullptr;
    AIDA::IHistogram1D* m_track_phiadc = nullptr;
    AIDA::IHistogram1D* m_track_pseudoeff = nullptr;
    AIDA::IHistogram1D* m_track_pseudoeff_int = nullptr;
    AIDA::IHistogram1D* m_used_sensors = nullptr;
    AIDA::IHistogram1D* m_phi_diff = nullptr;
    AIDA::IHistogram1D* m_track_rcoord = nullptr;
    AIDA::IHistogram1D* m_half_phi_a = nullptr;
    AIDA::IHistogram1D* m_half_phi_c = nullptr;
    AIDA::IHistogram1D* m_nmeasure = nullptr;
    AIDA::IHistogram1D* m_nposmeas = nullptr;
    AIDA::IHistogram1D* m_nnegmeas = nullptr;
    AIDA::IHistogram1D* m_mod_mismatch = nullptr;
    AIDA::IHistogram1D* m_nrclus = nullptr;
    AIDA::IHistogram1D* m_nrphiclus = nullptr;
    AIDA::IHistogram2D* m_xy_hitmap = nullptr;
    AIDA::IHistogram2D* m_zx_hitmap = nullptr;
    AIDA::IProfile1D* m_Rate_DistToM2 = nullptr;
    AIDA::IProfile1D* m_Rate_DistToOutStrip = nullptr;

    // Job options
    unsigned long m_event = 0;
    unsigned long m_resetProfile = 1000;
    std::string m_clusterCont;
    std::string m_trackCont;
    bool m_hitmapHistos;
    bool m_biasedResidualProfile;
    bool m_unbiasedResidualProfile;
    bool m_ACDC;
    bool m_xPlots;
    bool m_EventClusterInfo;
    bool m_alignMoniBasic;
    unsigned int m_alignNMeasMin;
  };
}

#endif // VELORECMONITORS_VELOTRACKMONITOR_H
