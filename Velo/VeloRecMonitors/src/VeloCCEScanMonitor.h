/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VELOCCESCANMONITOR_H 
#define VELOCCESCANMONITOR_H 1

// Include files
#include "GaudiAlg/GaudiAlgorithm.h"
#include "VeloEvent/VeloTELL1Data.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/IVeloClusterPosition.h"
#include "Kernel/IVeloCCEConfigTool.h"

/** @class VeloCCEScanMonitor
 * 
 * Class for Velo CCE scan monitoring ntuple
 *  @author Dermot Moran, Jon Harrison, Shanzhen Chen
 *  @date   16-05-2016
 */                 
                                                           
namespace Velo
{
  class VeloCCEScanMonitor : public GaudiTupleAlg {
                                                                             
  public:
                                                                             
    typedef IVeloClusterPosition::Direction Direction;

    /** Standard construtor */
    VeloCCEScanMonitor( const std::string& name, ISvcLocator* pSvcLocator );
                                                                             
    /** Destructor */
    virtual ~VeloCCEScanMonitor();

    /** Algorithm execute */
    StatusCode execute() override;

    /** Algorithm initialize */
    StatusCode initialize() override;

  private:
    void fillCFEtuple(const LHCb::Track& track);
  
    IVeloClusterPosition* m_clusterTool = nullptr;
    ITrackExtrapolator* m_linExtrapolator = nullptr;
    IVeloCCEConfigTool* m_cceTool = nullptr;
    LHCb::VeloClusters* m_rawClusters = nullptr;
    LHCb::VeloTELL1Datas* m_recorded = nullptr;
    std::string m_tracksInContainer = LHCb::TrackLocation::Default;
    std::string m_recordLoc; 
    std::string m_killSensorList = "";
    const DeVelo* m_veloDet = nullptr;

    bool m_CCEscan = true;
    unsigned int m_testsensor = 0;
    int m_ClusThres = 0;
    double m_numstripsCCE = 2;
    double m_numstripsCFE = 5;

  protected:

    bool checkBCList(unsigned int CheckStrip);
    double projAngleR(const Direction& locDirection, 
		      const Gaudi::XYZPoint& aLocPoint);
    double projAnglePhi(const Direction& locDirection,
                        const DeVeloPhiType* phiSensor,
                        unsigned int centreStrip);
    double angleOfInsertion(const Direction& localSlopes,
                            Gaudi::XYZVector& parallel2Track) const;
    Direction localTrackDirection(const Gaudi::XYZVector& globalTrackDir,
                                  const DeVeloSensor* sensor) const;
    
    LHCb::Tracks* m_tracks = nullptr;
    std::vector<int> m_killSensorListVec;
    std::vector<int> m_badStripList;
    bool m_CenStripOK = 0;
    unsigned int m_evt = 0;
    int m_CCEstep = -1;
    int m_Voltage = -1;
    int m_sizeCluster = 0;
    double m_interStripFr = -1;
    double m_unbiasedResid = -1;
    double m_chi2_unbiasedres = -1;
    double m_projAngle = 0;
  };
}
#endif // VELOCCESCANMONITOR_H
