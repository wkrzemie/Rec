###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from os import environ
from Gaudi.Configuration import *
import GaudiKernel.ProcessJobOptions
from TrackSys.Configuration import *
from GaudiKernel.SystemOfUnits import mm
from GaudiKernel.SystemOfUnits import GeV

# Sanity checks
def CheckTrackTypes(trackTypes, subDets, seqType):
    if trackTypes is None:
      trackTypes = TrackSys().getProp("TrackTypes")

    if "Velo" in trackTypes:
        if not (("VP" in subDets)):
            raise RuntimeError("Specify VP.")

    if "Forward" in trackTypes:
        if not ("Velo" in trackTypes):
            log.warning("Velo tracks added to tracking sequence.")
            trackTypes += ["Velo"]
        if not (("FT" in subDets)):
            raise RuntimeError("Specify T-Stations.")

    if seqType == "Best":
        if "Upstream" in trackTypes:
            if not ("Velo" in trackTypes):
                log.warning("Velo tracks added to tracking sequence.")
                trackTypes += ["Velo"]
                if not (("UT" in subDets)):
                    raise RuntimeError("Specify UT.")

        if "Downstream" in trackTypes:
            if not ("Seeding" in trackTypes):
                log.warning("Seed tracks added to tracking sequence.")
                trackTypes += ["Seeding"]
                if not (("UT" in subDets)):
                    raise RuntimeError("Specify UT.")


        if "Seeding" in trackTypes:
            if not ("FT" in subDets) :
                raise RuntimeError("Specify T-Stations.")

        if "Match" in trackTypes:
            if not ("Velo" in trackTypes):
                log.warning("Velo tracks added to tracking sequence.")
                trackTypes += ["Velo"]

            if not ("Seeding" in trackTypes):
                log.warning("Seed tracks added to tracking sequence.")
                trackTypes += ["Seeding"]

    return trackTypes

# Get sub detectors
def GetSubDets():
    subDets = []
    from Configurables import LHCbApp
    #Test if LHCbApp has this method (same revision as property)
    if hasattr(LHCbApp(),"Detectors"):
        if LHCbApp().isPropertySet("Detectors"):
            subDets = LHCbApp().upgradeDetectors()

    return subDets

#Decoding
def DecodeTracking(subDets):
    from DAQSys.Decoders import DecoderDB
    from DAQSys.DecoderClass import decodersForBank
    decs = [ ]
    # Are these the right decoders?
    if "VP" in subDets:
        decs += decodersForBank(DecoderDB, "VP")
    if "UT" in subDets:
        decs += decodersForBank(DecoderDB, "UT")
        from Configurables import UTOfflinePosition
        UT = UTOfflinePosition('ToolSvc.UTClusterPosition')
        UT.DetType = "UT"
    if "FT" in subDets:
        decs += decodersForBank(DecoderDB,"FTCluster")
    algs = [d.setup() for d in decs]

    # In case we have UT and FT clusters we can run the GECFilter, GEC filter applied before actually doing the FT/UT cluster creation, it runs on raw banks
    from Configurables import PrGECFilter
    GecFilter = PrGECFilter()
    if TrackSys().getProp("ConfigHLT1")["GEC"] > 0 :
        GecFilter.NumberFTUTClusters = TrackSys().getProp("ConfigHLT1")["GEC"]
    algs.insert(0, GecFilter)
    #if not TrackSys().getProp("VeloUpgradeOnly"):
    if "UT" in subDets:
        from Configurables import PrStoreUTHit
        storeUT = PrStoreUTHit()
        if TrackSys().getProp("BestThroughput") :
            storeUT.skipBanksWithErrors = True
        algs.append(storeUT)

    if "FT" in subDets:
        from Configurables import PrStoreFTHit
        algs.append(PrStoreFTHit())

    return algs

# Set sequence for Velo Pix
def RecoVeloPr(fit = True, output_tracks="Rec/Track/Velo", output_tracks_fitted = "Rec/Track/FittedHLT1VeloTracks", velo_clustering = "VPClus"):
    from Configurables import PrPixelTracking, VPClus, VSPClus
    prPixelTracking = PrPixelTracking("PrPixelTrackingFast")
    prPixelTracking.OutputTracksName = output_tracks
    prPixelTracking.ClosestToBeamStateKalmanFit = True
    veloConfig = TrackSys().getProp("ConfigHLT1")["VeloHLT1"]
    prPixelTracking.AlgoConfig =   veloConfig["Strategy"]  #[see Configuration.py for the default here]

    if veloConfig["Configuration"] == "Default" :
        prPixelTracking.HardFlagging = False
        prPixelTracking.SkipLoopSens = False
        prPixelTracking.MaxMissedOnTrack = 3
        prPixelTracking.MaxMissedConsecutive = 1
        prPixelTracking.PhiWindow = 5.5 #0.0959931
        prPixelTracking.PhiWindowExtrapolation = 5.5 #0.0959931
        prPixelTracking.EarlyKill3HitTracks = False
        prPixelTracking.BoostPhysics = False
    elif veloConfig["Configuration"] == "BestThroughput" :
        prPixelTracking.HardFlagging = True
        prPixelTracking.SkipLoopSens = True
        prPixelTracking.MaxMissedOnTrack = 2
        prPixelTracking.MaxMissedConsecutive = 1
        prPixelTracking.PhiWindow = 2.5 #0,0436332
        prPixelTracking.PhiWindowExtrapolation = 2.5 #0,0436332
        prPixelTracking.ModulesToSkip = [] #OPTIONAL, NOT FULLY TESTED
        prPixelTracking.EarlyKill3HitTracks = True
        prPixelTracking.UsePhiPerRegionsForward = False
        prPixelTracking.BoostPhysics = False
    elif veloConfig["Configuration"] == "BestPhysics" :
        prPixelTracking.BoostPhysics = True
        prPixelTracking.UsePhiPerRegionsForward = True
        prPixelTracking.MaxMissedOnTrack = 3
        prPixelTracking.MaxMissedConsecutive = 1
        prPixelTracking.PhiWindowsPerRegionForward = [5.0, 15.0, 10.0, 30.0] #[0,0872665 :  0,261799 , 0,174533 , 0,523599 ]
        prPixelTracking.PhiWindowExtrapolation  = 5.5 #0.0959931
        prPixelTracking.PhiWindow = 5.5 # 0.0959931
        prPixelTracking.HardFlagging = False
    else :
        log.warning("using default from code Velo tracking, please choose one configuration ")
    #prPixelTracking.EndVeloStateKalmanFit = True
    #prPixelTracking.AddFirstLastMeasurementStatesKalmanFit = True

#Kalman filter for VELO tracks, needed for PV reconstruction
#    if fit:
#        from Configurables import TrackEventFitter, TrackMasterFitter
#        from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
#        veloFitter = TrackEventFitter('VeloOnlyFitterAlg'+seqType)
#        veloFitter.TracksInContainer = output_tracks
#        veloFitter.TracksOutContainer = output_tracks_fitted
#        veloFitter.addTool(TrackMasterFitter, name="Fitter")
#        ConfiguredMasterFitter( veloFitter.Fitter)
#        GaudiSequencer("RecoTr"+seqType+"Seq").Members += [ veloFitter ]

    seq = []
    if TrackSys().getProp("BestThroughput"):
        return [ VSPClus("VSPClustering") , prPixelTracking ]
    if( velo_clustering == "VPClus"):
        return [ VPClus("VPClustering")   , prPixelTracking ]
    elif( velo_clustering == "VSPClus"):
        return [ VSPClus("VSPClustering") , prPixelTracking ]
    else :
        print "FAILED TO CONFIGURE VELO CLUSTERING"
        raise

# Set Primary Vertex reconstruction
def RecoPV(input_tracks = "Rec/Track/FittedHLT1VeloTracks"):
    from Configurables import PatPV3D, PVOfflineTool, AdaptivePV3DFitter, PVSeedTool
    from Configurables import LHCb__Converters__Track__v1__fromV2TrackV1TrackVector as FromV2TrackV1TrackVector
    converter = FromV2TrackV1TrackVector("ConverterV1V2ForPVReco")
    converter.InputTracksName=input_tracks
    converter.OutputTracksName=input_tracks+"AsTrackV1"
    log.warning("Pre-appending Velo track copy in TES from vector<Track::v2> to vector<Track::v1>, this should be a temporary solution")

    from Configurables import PatPV3D, PVOfflineTool, AdaptivePV3DFitter, PVSeedTool
    pvAlg = PatPV3D("PatPV3D")
    pvAlg.InputTracks = converter.OutputTracksName
    pvAlg.addTool(PVOfflineTool,"PVOfflineTool")
    pvAlg.PVOfflineTool.addTool(AdaptivePV3DFitter,"AdaptivePV3DFitter")
    pvAlg.PVOfflineTool.PVFitterName = "AdaptivePV3DFitter"
    pvAlg.PVOfflineTool.addTool(PVSeedTool,"PVSeedTool")
    pvAlg.PVOfflineTool.PVSeedingName = "PVSeedTool"
    pvAlg.PVOfflineTool.PVSeedTool.minClusterMult = 4
    pvAlg.PVOfflineTool.PVSeedTool.minCloseTracksInCluster = 3
    pvAlg.PVOfflineTool.PVSeedTool.highMult = 10
    pvAlg.PVOfflineTool.PVSeedTool.ratioSig2HighMult = 1.0
    pvAlg.PVOfflineTool.PVSeedTool.ratioSig2LowMult = 0.9
    pvAlg.PVOfflineTool.UseBeamSpotRCut = True
    pvAlg.PVOfflineTool.BeamSpotRCut = 0.6
    pvAlg.PVOfflineTool.BeamSpotRHighMultiplicityCut = 0.4
    pvAlg.OutputVerticesName = "Rec/Vertex/Vector/Primary"

    #pvAlg.PrimaryVertexLocation = "Rec/Vertex/PrimaryVertices"
    return [ converter, pvAlg ]

def RecoPVFuture(input_tracks = "Rec/Track/FittedHLT1VeloTracks"):
    from Configurables import PatPV3DFuture
    pvAlg = PatPV3DFuture("PatPV3D")
    pvAlg.InputTracks = input_tracks
    pvAlg.minClusterMult = 4
    pvAlg.minCloseTracksInCluster = 3
    pvAlg.highMult = 10
    pvAlg.ratioSig2HighMult = 1.0
    pvAlg.ratioSig2LowMult = 0.9
    pvAlg.UseBeamSpotRCut = True
    pvAlg.BeamSpotRCut = 0.6
    pvAlg.BeamSpotRHighMultiplicityCut = 0.4
    pvAlg.OutputVerticesName = "Rec/Vertex/Vector/Primary"
    return [ pvAlg ]

# Set Upstream tracking
def RecoUpstream(min_pt = 0.0, input_tracks = "Rec/Track/Velo",
    output_tracks = "Rec/Track/Upstream", simplifiedGeometry = True):
    from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter

    from Configurables import PrVeloUT
    prVeloUT = PrVeloUT("PrVeloUTFast")
    prVeloUT.InputTracksName = input_tracks
    prVeloUT.OutputTracksName = output_tracks
    prVeloUT.minPT   = min_pt
    prVeloUT.doIPCut = TrackSys().getProp("ConfigHLT1")["VeloUTHLT1"]["IPCut"]
    prVeloUT.minIP   = TrackSys().getProp("ConfigHLT1")["VeloUTHLT1"]["IPCutVal"]
    prVeloUT.FinalFit = TrackSys().getProp("ConfigHLT1")["UseMomentumGuidedSearchWindow"]

    prVeloUT.InputVerticesName = "Rec/Vertex/Vector/Primary"
    from Configurables import TrackMasterFitter
    prVeloUT.addTool(TrackMasterFitter,"Fitter")
    ConfiguredMasterFitter(prVeloUT.Fitter, SimplifiedGeometry = simplifiedGeometry)
    return [ prVeloUT ]

#Set Foward tracking
def RecoForward(seqType = "Fast",
                min_pt=0.05*GeV,
                input_tracks = "Rec/Track/Velo",
                output_tracks = "Rec/Track/Forward",
                fit = True,
                simplifiedGeometry = True,
                output_tracks_fitted = "Rec/Track/FittedForward",
                tuning = 0):

    from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
    from Configurables import TrackUsedLHCbID
    from Configurables import PrForwardTracking
    prFwdTracking = PrForwardTracking("PrForwardTracking"+seqType)
    prFwdTracking.InputName = input_tracks
    prFwdTracking.OutputName = output_tracks
    prFwdTracking.MinPT = min_pt
    if seqType == "Fast":
        if tuning == 0:
          prFwdTracking.UseMomentumEstimate = True
          prFwdTracking.UseMomentumGuidedSearchWindow = TrackSys().getProp("ConfigHLT1")["UseMomentumGuidedSearchWindow"]
          prFwdTracking.Preselection = True
          prFwdTracking.PreselectionPT = TrackSys().getProp("ConfigHLT1")["ForwardHLT1"]["PreselectionPT"]
          prFwdTracking.TolYTriangleSearch = 20.
          prFwdTracking.TolYCollectX = 3.5
          prFwdTracking.TolYSlopeCollectX = 0.001
          prFwdTracking.MinXHits = 5
          prFwdTracking.MaxXWindow = 1.
          prFwdTracking.MaxXWindowSlope = 0.002
          prFwdTracking.MaxXGap = 1.
          prFwdTracking.SecondLoop = TrackSys().getProp("ConfigHLT1")["ForwardHLT1"]["SecondLoop"]
          prFwdTracking.MinXHits2nd = 4
          prFwdTracking.MaxXWindow2nd = 1.5
          prFwdTracking.MaxXWindowSlope2nd = 0.002
          prFwdTracking.MaxXGap2nd = 0.5
        if tuning == 1:
          prFwdTracking.UseMomentumEstimate = True
          prFwdTracking.UseMomentumGuidedSearchWindow = TrackSys().getProp("ConfigHLT1")["UseMomentumGuidedSearchWindow"]
          prFwdTracking.Preselection = True
          prFwdTracking.PreselectionPT = TrackSys().getProp("ConfigHLT1")["ForwardHLT1"]["PreselectionPT"]
          prFwdTracking.TolYTriangleSearch = 20.
          prFwdTracking.TolYCollectX = 2.7
          prFwdTracking.TolYSlopeCollectX = 0.0007
          prFwdTracking.MinXHits = 5
          prFwdTracking.MaxXWindow = 0.7
          prFwdTracking.MaxXWindowSlope = 0.0015
          prFwdTracking.MaxXGap = 0.3
          prFwdTracking.SecondLoop = TrackSys().getProp("ConfigHLT1")["ForwardHLT1"]["SecondLoop"]
          prFwdTracking.MinXHits2nd = 4
          prFwdTracking.MaxXWindow2nd = 0.7
          prFwdTracking.MaxXWindowSlope2nd = 0.0015
          prFwdTracking.MaxXGap2nd = 0.3

    else:
        prFwdTracking.UseMomentumEstimate = False
        prFwdTracking.Preselection = False
        prFwdTracking.TolYTriangleSearch = 20.
        prFwdTracking.TolYCollectX = 4.1
        prFwdTracking.TolYSlopeCollectX = 0.0018
        prFwdTracking.MinXHits = 5
        prFwdTracking.MaxXWindow = 1.2
        prFwdTracking.MaxXWindowSlope = 0.002
        prFwdTracking.MaxXGap = 1.2
        prFwdTracking.SecondLoop = True
        prFwdTracking.MinXHits2nd = 4
        prFwdTracking.MaxXWindow2nd = 1.5
        prFwdTracking.MaxXWindowSlope2nd = 0.002
        prFwdTracking.MaxXGap2nd = 0.5

    algs = [ prFwdTracking ]

    if fit:
        from Configurables import LHCb__Converters__Track__v1__fromV2TrackV1TrackVector as FromV2TrackV1TrackVector
        converter = FromV2TrackV1TrackVector("ConverterV1V2ForForwardFit")
        converter.InputTracksName=output_tracks
        converter.OutputTracksName=output_tracks+"AsTrackV1"

        if "ParameterizedKalman" in TrackSys().getProp("ExpertTracking") and seqType == "Fast":
          from Configurables import ParameterizedKalmanFit
          ParameterizedKalman = ParameterizedKalmanFit('ForwardFitterAlgParam'+seqType)
          ParameterizedKalman.InputName = output_tracks
          ParameterizedKalman.OutputName = output_tracks_fitted
          ParameterizedKalman.MaxNumOutlier = 2

          from Configurables import TrackMasterExtrapolator, SimplifiedMaterialLocator
          ParameterizedKalman.addTool( TrackMasterExtrapolator, name="extr")
          ParameterizedKalman.extr.ApplyMultScattCorr = True
          ParameterizedKalman.extr.ApplyEnergyLossCorr = False
          ParameterizedKalman.extr.ApplyElectronEnergyLossCorr = True
          ParameterizedKalman.extr.addTool(SimplifiedMaterialLocator, name = "MaterialLocator")

          algs.append(ParameterizedKalman)
        elif "vectorFitter" in TrackSys().getProp("ExpertTracking") or \
          "minimalVectorFitterFast" in TrackSys().getProp("ExpertTracking"):
          from Configurables import VectorOfTracksFitter, TrackVectorFitter, TrackMasterFitter
          forwardFitter = VectorOfTracksFitter('ForwardFitterAlgVector'+seqType)
          forwardFitter.TracksInContainer = converter.OutputTracksName
          forwardFitter.TracksOutContainer = output_tracks_fitted
          vectorFitter = TrackVectorFitter()
          vectorFitter.MaxNumberOutliers = 2
          from Configurables import TrackParabolicExtrapolator, TrackMasterExtrapolator, SimplifiedMaterialLocator
          # Fetch options from MasterFitter MeasProvider
          ConfiguredMasterFitter(TrackMasterFitter(), SimplifiedGeometry = simplifiedGeometry)
          vectorFitter.MeasProvider = TrackMasterFitter().MeasProvider
          if "useParabolicExtrapolator" in TrackSys().getProp("ExpertTracking"):
            vectorFitter.addTool(TrackParabolicExtrapolator, name="Extrapolator")
          else:
            vectorFitter.addTool(TrackMasterExtrapolator, name="Extrapolator")
          vectorFitter.addTool(SimplifiedMaterialLocator, name = "MaterialLocator")
          vectorFitter.Extrapolator.addTool(SimplifiedMaterialLocator, name="MaterialLocator")

          if "minimalVectorFitterFast" in TrackSys().getProp("ExpertTracking"):
            vectorFitter.MaxNumberOutliers = 0
            vectorFitter.NumberFitIterations = 2
            vectorFitter.AddDefaultReferenceNodes = False

          forwardFitter.addTool(vectorFitter, name="Fitter")
          algs.append( converter )
          algs.append(forwardFitter)
        else:
          from Configurables import VectorOfTracksFitter, TrackMasterFitter
          from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
          forwardFitter = VectorOfTracksFitter('ForwardFitterAlg'+seqType)
          forwardFitter.addTool(TrackMasterFitter, name="Fitter")
          ConfiguredMasterFitter( forwardFitter.Fitter, SimplifiedGeometry = simplifiedGeometry)
          forwardFitter.TracksInContainer = converter.OutputTracksName
          forwardFitter.TracksOutContainer = output_tracks_fitted
          algs.append( converter )
          algs.append( forwardFitter )

    return algs

# Set Seeding
def RecoSeeding(output_tracks = "Rec/Track/Seed"):
    from Configurables import PrHybridSeeding
    prHybridSeeding = PrHybridSeeding("PrHybridSeedingBest")
    prHybridSeeding.OutputName = output_tracks
    return [ prHybridSeeding ]

# Set Matching
def RecoMatch(output_tracks = "Rec/Track/Match", input_seed = "Rec/Track/Seed", input_velo = "Rec/Track/Velo"):
    from Configurables import PrMatchNN
    prMatch = PrMatchNN("PrMatchNNBest")
    prMatch.MatchOutput = output_tracks
    prMatch.VeloInput = input_velo
    prMatch.SeedInput = input_seed
    return [ prMatch ]

# Set Downstream
def RecoDownstream(output_tracks= "Rec/Track/Downstream", input_seed = "Rec/Track/Seed"):
    from Configurables import PrLongLivedTracking
    prDownstream = PrLongLivedTracking("PrLongLivedTrackingBest")
    prDownstream.InputLocation = input_seed
    prDownstream.OutputLocation = output_tracks
    return [ prDownstream ]

# Fast tracking reconstruction, to be HLT1 like
def RecoFastTrackingStage(defTracks = {}, sequence = None, simplifiedGeometryFit = True, trackTypes = None,
    fitForward = True, includePVs = True, decoding_sequence = None):
    from copy import deepcopy
    defTracks = deepcopy(defTracks)

    ## Start TransportSvc, needed by track fit  ???
    ApplicationMgr().ExtSvc.append("TransportSvc")
    subDets = GetSubDets()
    if trackTypes is None:
      trackTypes = TrackSys().getProp("TrackTypes")

    ### Sanity checks
    sType = "Fast"
    trackTypes = CheckTrackTypes(trackTypes, subDets, sType)

    algs = [ ]

    ### Do the decoding of the detectors
    decoding_algs = DecodeTracking(subDets)

    veloOnly = TrackSys().getProp("VeloUpgradeOnly")
    ### Define the pattern recognition
    if "Velo" in trackTypes:
        algs += RecoVeloPr(output_tracks = defTracks["Velo"]["Location"],
                           output_tracks_fitted = defTracks["VeloFitted"]["Location"],
                           velo_clustering = TrackSys().getProp("VeloClustering"))
        if includePVs and not veloOnly:
          algs += RecoPVFuture(input_tracks = defTracks["Velo"]["Location"])
        defTracks["Velo"]["BestUsed"] = True

    if not veloOnly:
        if "Upstream" in trackTypes:
            algs += RecoUpstream(min_pt = TrackSys().getProp("ConfigHLT1")["VeloUTHLT1"]["minPT"],
                                 simplifiedGeometry = simplifiedGeometryFit,
                                 input_tracks = defTracks["Velo"]["Location"],
                                 output_tracks = defTracks["Upstream"]["Location"])

            defTracks["Upstream"]["BestUsed"] = True

        if "Forward" in trackTypes:
            inType = 'Upstream' if 'Upstream' in trackTypes else 'Velo'
            algs += RecoForward(seqType=sType,
                                min_pt = TrackSys().getProp("ConfigHLT1")["ForwardHLT1"]["minPT"],
                                input_tracks = defTracks[inType]["Location"],
                                output_tracks = defTracks["ForwardFast"]["Location"],
                                fit = fitForward,
                                simplifiedGeometry = simplifiedGeometryFit,
                                output_tracks_fitted = defTracks["ForwardFastFitted"]["Location"])
            defTracks["ForwardFastFitted"]["BestUsed"] = True

    ### Do we have a different sequence for decoding?
    if decoding_sequence is not None:
        decoding_sequence.Members += decoding_algs
    else:
        algs = decoding_algs + algs

    ### The sequencer we're setting up
    if sequence is not None:
        sequence.Members += algs
        sequence.IgnoreFilterPassed = True # because of the PV reco.
        return defTracks
    else:
        return defTracks, algs

# Best tracking reconstruction, to be like HLT2
def RecoBestTrackingStage(tracklists = [], defTracks = {}, sequence = None, simplifiedGeometryFit = True,
    trackTypes = None, includePVs = True):
    from copy import deepcopy
    defTracks = deepcopy(defTracks)

    ## Start TransportSvc, needed by track fit  ???
    ApplicationMgr().ExtSvc.append("TransportSvc")
    subDets = GetSubDets()
    if trackTypes is None:
      trackTypes = TrackSys().getProp("TrackTypes")
    ### Sanity checks
    sType = "Best"
    trackTypes = CheckTrackTypes(trackTypes, subDets, sType)

    algs = [ ]

    ### Define the pattern recognition
    if "Forward" in trackTypes:
        # PLEASE NOTE: For now we need to take all VELO tracks, including that used in the fast stage
        algs += RecoForward(seqType = sType,
                    min_pt = 0.05*GeV,
                    input_tracks = defTracks["Velo"]["Location"],
                    output_tracks = defTracks["ForwardBest"]["Location"],
                    fit = False)
        defTracks["ForwardBest"]["BestUsed"] = True

    if "Seeding" in trackTypes:
        algs += RecoSeeding(output_tracks = defTracks["Seeding"]["Location"])
        defTracks["Seeding"]["BestUsed"] = True

    if "Match" in trackTypes:
        algs += RecoMatch(output_tracks = defTracks["Match"]["Location"],
                          input_seed = defTracks["Seeding"]["Location"],
                          input_velo = defTracks["Velo"]["Location"])
        defTracks["Match"]["BestUsed"] = True

    if "Downstream" in trackTypes:
        algs += RecoDownstream(input_seed = defTracks["Seeding"]["Location"],
                               output_tracks = defTracks["Downstream"]["Location"])
        defTracks["Downstream"]["BestUsed"] = True



    ### Were we given a sequence to configure?
    if sequence is not None:
        sequence.Members += algs
        return defTracks
    else:
        return defTracks, algs

def RecoBestTrackCreator(defTracks = { }, tracksToConvert = [], defTracksConverted = {}, simplifiedGeometryFit = True, sequence = None,
    out_container = "Rec/Track/Best"):
    tracklists = []
    for tr in defTracks:
        if ( defTracks[tr]["BestUsed"] == True ):
            tracklists.append(defTracksConverted[tr]["Location"])

    # TrackBestTrackCreator's behaviour is weakly dependent on the ordering of the input containers.
    # The next few lines guarantee that the ordering does not depend on the iteration order of defTracks,
    # and avoid introducing diffs w.r.t. the old tests
    # TODO: a small optimisation may be possible by ordering the containers from "probably-best" to "probably-worst"
    # (e.g. long, downstream, upstream, T-track, velo?) as this could give std::stable_sort less to do inside the
    # algorithm -- to be checked when the correct performance benchmarks are available
    approved_order = [
        "Rec/Track/Keyed/Velo",
        "Rec/Track/ForwardFastFitted",
        "Rec/Track/Keyed/ForwardBest",
        "Rec/Track/Keyed/Upstream",
        "Rec/Track/Downstream",
        "Rec/Track/Seed",
        "Rec/Track/Match"
        ]

    tracklists = [ x for x in approved_order if x in tracklists ] \
        + sorted([ x for x in tracklists if x not in approved_order ])

    algs = [ ]

    from Configurables import LHCb__Converters__Track__v1__fromV2TrackV1Track as FromV2TrackV1Track
    seq_converters = GaudiSequencer("TrackConverterSeq")
    for tracktype in defTracks:
        if tracktype in tracksToConvert:
            log.warning("Pre-appending "+tracktype+" track copy in TES from vector<Track> to KeyedContainer, this should be a temporary solution")
            trconverter = FromV2TrackV1Track(tracktype+"Converter")
            trconverter.InputTracksName = defTracks[tracktype]["Location"]
            trconverter.OutputTracksName = defTracksConverted[tracktype]["Location"]
            #insert in the sequence the converter for the tracks listed in UpgrateTracksToConvert
            seq_converters.Members += [ trconverter]
    if "Velo" in tracksToConvert:
        log.warning("Pre-appending vertex copy in TES from vector<RecVertex> to KeyedContainer, this should be a temporary solution")
        from Configurables import LHCb__Converters__RecVertex__v2__fromVectorLHCbRecVertex as FromVectorLHCbRecVertex
        vertexConverter = FromVectorLHCbRecVertex("VertexConverter")
        vertexConverter.InputVerticesName = "Rec/Vertex/Vector/Primary"
        vertexConverter.InputTracksName = defTracksConverted["Velo"]["Location"]
        vertexConverter.OutputVerticesName = "Rec/Vertex/Primary"
        seq_converters.Members += [ vertexConverter ]

    algs += [seq_converters]

    # Do the Clone Killing and create Best tracks container
    from Configurables import TrackBestTrackCreator, TrackMasterFitter
    from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
    bestTrackCreator = TrackBestTrackCreator("TrackBestTrackCreator")
    bestTrackCreator.TracksInContainers = tracklists
    bestTrackCreator.FitTracks = True
    bestTrackCreator.InitTrackStates = False
    bestTrackCreator.DoNotRefit = True
    bestTrackCreator.TracksOutContainer = out_container

    if "vectorFitter" in TrackSys().getProp("ExpertTracking"):
      from Configurables import TrackVectorFitter
      vectorFitter = TrackVectorFitter()
      vectorFitter.MaxNumberOutliers = 2
      from Configurables import TrackParabolicExtrapolator, TrackMasterExtrapolator, SimplifiedMaterialLocator
      if "useParabolicExtrapolator" in TrackSys().getProp("ExpertTracking"):
        vectorFitter.addTool(TrackParabolicExtrapolator, name="Extrapolator")
      else:
        vectorFitter.addTool(TrackMasterExtrapolator, name="Extrapolator")
      vectorFitter.addTool(SimplifiedMaterialLocator, name = "MaterialLocator")
      vectorFitter.Extrapolator.addTool(SimplifiedMaterialLocator, name="MaterialLocator")
      bestTrackCreator.addTool(vectorFitter, name="Fitter")
    else:
      bestTrackCreator.addTool(TrackMasterFitter, name="Fitter")
      ConfiguredMasterFitter( bestTrackCreator.Fitter, SimplifiedGeometry = simplifiedGeometryFit )
    algs.append(bestTrackCreator)

    if sequence is not None:
      sequence.Members += algs
    else:
      return algs

def ExtraInformations():
    ## Extra track information sequence
    algs = [ ]
    extraInfos = TrackSys().getProp("TrackExtraInfoAlgorithms")
    if len(extraInfos) > 0 :
        ## ghost probability using a Neural Net
        if "GhostProbability" in extraInfos :
            from Configurables import TrackAddNNGhostId
            ghostID = TrackAddNNGhostId()
            ghostID.GhostIdTool = "UpgradeGhostId"
            algs.append(ghostID)
    return algs
