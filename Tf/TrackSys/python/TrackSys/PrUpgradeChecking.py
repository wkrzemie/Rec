###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import GaudiSequencer
from TrackSys.Configuration import *
from GaudiKernel.SystemOfUnits import mm


def PrUpgradeChecking(defTracks={}, tracksToConvert = [], defTracksConverted = {}):
    # match hits and tracks
    log.warning("Run upgrade checkers.")

    from Configurables import (UnpackMCParticle, UnpackMCVertex,
                               PrLHCbID2MCParticle)
    # Check if VP is part of the list of detectors.
    from Configurables import LHCbApp, VPCluster2MCParticleLinker, VPFullCluster2MCParticleLinker, VPClusFull
    withVP = False
    if hasattr(LHCbApp(), "Detectors"):
        if LHCbApp().isPropertySet("Detectors"):
            if 'VP' in LHCbApp().upgradeDetectors():
                withVP = True
    trackTypes = TrackSys().getProp("TrackTypes")

    if "Truth" in trackTypes:
        truthSeq = GaudiSequencer("RecoTruthSeq")
        truthSeq.Members = [UnpackMCParticle(), UnpackMCVertex()]
        if withVP:
            truthSeq.Members += [VPCluster2MCParticleLinker()]
        truthSeq.Members += [PrLHCbID2MCParticle()]
    else:
        if withVP:
            if TrackSys().getProp("VPLinkingOfflineClusters"):
                #re-run VPClustering for FULL Velo cluster [ with info of pixels to clusters] and its own linker algorithm
                GaudiSequencer("MCLinksTrSeq").Members = [ 
                    VPClusFull() , VPFullCluster2MCParticleLinker(), PrLHCbID2MCParticle() 
                ]
            else:
                GaudiSequencer("MCLinksTrSeq").Members = [
                    VPCluster2MCParticleLinker(), PrLHCbID2MCParticle()]
        else:
            GaudiSequencer("MCLinksTrSeq").Members = [PrLHCbID2MCParticle()]
        
    newDefTracks = defTracks
    #Re-create a keyed container for a sub-set of tracks
    from Configurables import PrTrackAssociator
    from Configurables import LHCb__Converters__Track__v1__fromV2TrackV1Track as FromV2TrackV1Track
    from Configurables import LHCb__Converters__Track__v1__fromVectorLHCbTrack as FromV1VectorV1Tracks
    for tracktype in defTracks:
        seq = GaudiSequencer(tracktype+"Checker")
        if tracktype in tracksToConvert:
            log.warning("Pre-appending "+tracktype+" track copy in TES from vector<Track> to KeyedContainer to allow truth matching, this is an hack, not a final solution")
            if "ParameterizedKalman" in TrackSys().getProp("ExpertTracking") and tracktype == "ForwardFastFitted":
                trconverter = FromV1VectorV1Tracks(tracktype+"Converter")
            else:
                trconverter = FromV2TrackV1Track(tracktype+"Converter")
            trconverter.InputTracksName   =  defTracks[tracktype]["Location"]
            trconverter.OutputTracksName =  defTracksConverted[tracktype]["Location"]
            seq.Members+= [ trconverter]
            #insert in the sequence the converter for the tracks listed in UpgrateTracksToConvert
            trassociator                 = PrTrackAssociator(tracktype+"Associator")
            trassociator.SingleContainer = defTracksConverted[tracktype]["Location"]
            #update the track locations for the PrChecker
            seq.Members+= [ trassociator]
            GaudiSequencer("MCLinksTrSeq").Members+= [seq]
            newDefTracks[tracktype]["Location"] = defTracksConverted[tracktype]["Location"]

    from Configurables import LHCb__Converters__RecVertex__v2__fromVectorLHCbRecVertex as FromVectorLHCbRecVertex
    vertexConverter = FromVectorLHCbRecVertex("VertexConverter")
    vertexConverter.InputVerticesName = "Rec/Vertex/Vector/Primary"
    vertexConverter.InputTracksName = defTracksConverted["Velo"]["Location"]
    vertexConverter.OutputVerticesName = "Rec/Vertex/Primary"
    GaudiSequencer("MCLinksTrSeq").Members+= [ vertexConverter ]

    #PrTrackAssociator uses by default whatever it finds in RootOfContainers
    GaudiSequencer("MCLinksTrSeq").Members+= [ PrTrackAssociator("PrTrackAssociator") ]
    # PrChecker2
    from Configurables import PrChecker2
    from Configurables import LoKi__Hybrid__MCTool
    MCHybridFactory = LoKi__Hybrid__MCTool("MCHybridFactory")
    MCHybridFactory.Modules = ["LoKiMC.decorators"]
    # FAST
    checker2 = PrChecker2("PrChecker2Fast",
                          VeloTracks="",
                          MatchTracks="",
                          SeedTracks="",
                          DownTracks="",
                          UpTracks="",
                          TriggerNumbers=True,
                          BestTracks="")
    PrChecker2("PrChecker2Fast").addTool(MCHybridFactory)
    PrChecker2("PrChecker2Fast").Upgrade = True
    PrChecker2("PrChecker2Fast").VeloTracks = newDefTracks["Velo"]["Location"]
    #PrChecker2("PrChecker2Fast").NewTracks  = defTracks["VeloFitted"]["Location"]
    PrChecker2("PrChecker2Fast").ForwardTracks = newDefTracks["ForwardFast"]["Location"]
    PrChecker2("PrChecker2Fast").UpTracks      = newDefTracks["Upstream"]["Location"]
    PrChecker2("PrChecker2Fast").Eta25Cut = True
    PrChecker2("PrChecker2Fast").GhostProbCut = 0.9
    GaudiSequencer("CheckPatSeq").Members += [checker2]

    # PrChecker2("PrChecker2Fast").WriteTexOutput = True
    # PrChecker2("PrChecker2Fast").TexOutputFolder = "texfilesFast/"
    # PrChecker2("PrChecker2Fast").TexOutputName = "PrChecker2Fast"
    # BEST
    GaudiSequencer("CheckPatSeq").Members += [PrChecker2("PrChecker2")]
    PrChecker2("PrChecker2").addTool(MCHybridFactory)
    PrChecker2("PrChecker2").Upgrade = True
    PrChecker2("PrChecker2").ForwardTracks = newDefTracks["ForwardBest"]["Location"]
    PrChecker2("PrChecker2").MatchTracks = newDefTracks["Match"]["Location"]
    PrChecker2("PrChecker2").DownTracks = newDefTracks["Downstream"]["Location"]
    PrChecker2("PrChecker2").SeedTracks = newDefTracks["Seeding"]["Location"]
    #PrChecker2("PrChecker2").UpTracks      = newDefTracks["Upstream"]["Location"]
    PrChecker2("PrChecker2").Eta25Cut = True
    PrChecker2("PrChecker2").GhostProbCut = 0.9
    # PrChecker2("PrChecker2").WriteTexOutput = True
    # PrChecker2("PrChecker2").TexOutputFolder = "texfiles/"
    # PrChecker2("PrChecker2").TexOutputName = "PrChecker2"
    #dedicated Velo Checking
    checkervelo = GetVeloChecker( newDefTracks["Velo"]["Location"])
    GaudiSequencer("CheckPatSeq").Members += [ checkervelo]

    #Track resolution checker fast stage
    from Configurables import TrackResChecker
    if not TrackSys().getProp("VeloUpgradeOnly"):
        GaudiSequencer("CheckPatSeq").Members  += [ TrackResChecker("TrackResCheckerFast")];
        TrackResChecker("TrackResCheckerFast").HistoPrint = False
        TrackResChecker("TrackResCheckerFast").TracksInContainer = defTracksConverted["ForwardFastFitted"]["Location"]
        from Configurables import PrimaryVertexChecker
        PVCheck = PrimaryVertexChecker("PVChecker")
        if not PVCheck.isPropertySet('inputVerticesName'):
            PVCheck.inputVerticesName = "Rec/Vertex/Primary"
        if not PVCheck.isPropertySet('matchByTracks'):
            PVCheck.matchByTracks = False
        if not PVCheck.isPropertySet('nTracksToBeRecble'):
            PVCheck.nTracksToBeRecble = 4
        if not PVCheck.isPropertySet('inputTracksName'):
            PVCheck.inputTracksName = defTracksConverted["VeloFitted"]["Location"]
        GaudiSequencer("CheckPatSeq").Members += [PVCheck]
    
def GetVeloChecker( velotracklocation):
    from Configurables import PrChecker2
    from Configurables import LoKi__Hybrid__MCTool
    MCHybridFactory = LoKi__Hybrid__MCTool("MCHybridFactory")
    MCHybridFactory.Modules = ["LoKiMC.decorators"]
    # FAST

    from Configurables import PrChecker2
    velochecker = PrChecker2("VeloPrChecker", VeloTracks = "", MatchTracks = "", SeedTracks = "", DownTracks = "", UpTracks = "", TriggerNumbers = False, BestTracks = "")
    velochecker.VeloTracks = velotracklocation
    velochecker.Upgrade = True
    velochecker.Eta25Cut = False
    velochecker.VetoElectrons = False
    cutDictionary = {  "Electron"    : "(isElectron)" ,
                       "notElectron" : "(isNotElectron)" ,
                       "Forward"     : "(MCETA>0)",
                       "Backward"    : "(MCETA<0)",
                       "Eta25"       : "(MCETA>2.0) & (MCETA<5.0)",
                       "p>5GeV"      : "(MCP>5000)",
                       "p>3GeV"      : "(MCP>3000)",
                       "p<5GeV"      : "(MCP<5000)",
                       "pt>400MeV"   : "(MCPT>400)",
                       "pt>600MeV"   : "(MCPT>600)",
                       "pt>800MeV"   : "(MCPT>800)",
                       "pt>1000MeV"  : "(MCPT>1000)",
                       "Velo"        : "(isVelo)",
                       "Long"        : "(isLong)",
                       "FromB"       : "(fromB)",
                       "FromD"       : "(fromD)",
                       "strange"     : "(strange)"
    }
    all_of = lambda args : " & ".join( [ cutDictionary[i] for i in args ] )
    
    velochecker.MyVeloCuts = {
        "01_notElectron_Velo" :                        all_of( [ "notElectron", "Velo" ]),
        "02_notElectron_Velo_Forward" :                all_of( [ "notElectron", "Velo", "Forward"]),
        "03_notElectron_Velo_Backward":                all_of( [ "notElectron", "Velo", "Backward"]),
        "04_notElectron_Velo_Eta25"   :                all_of( [ "notElectron", "Velo", "Eta25" ]), 

        "05_notElectron_Long_Eta25"   :                all_of( [ "notElectron", "Long", "Eta25"]),
        "06_notElectron_Long_Eta25 p>5GeV" :           all_of( [ "notElectron", "Long", "Eta25", "p>5GeV"]),
        "07_notElectron_Long_Eta25 p<5GeV" :           all_of( [ "notElectron", "Long", "Eta25", "p<5GeV"]),
        "08_notElectron_Long_Eta25 p>3GeV pt>400MeV" : all_of( [ "notElectron", "Long", "Eta25", "p>3GeV", "pt>400MeV"]),

        "09_notElectron_Long_FromB_Eta25"                 : all_of( [ "notElectron", "Long", "Eta25" , "FromB"]),
        "10_notElectron_Long_FromB_Eta25 p>5GeV"          : all_of( [ "notElectron", "Long", "Eta25" , "FromB", "p>5GeV"]),
        "11_notElectron_Long_FromB_Eta25 p<5GeV"          : all_of( [ "notElectron", "Long", "Eta25" , "FromB", "p<5GeV"]),
        "12_notElectron_Long_FromB_Eta25 p>3GeV pt>400MeV": all_of( [ "notElectron", "Long", "Eta25" , "FromB", "p>3GeV", "pt>400MeV"]),
        
        "13_notElectron_Long_FromD_Eta25"                 : all_of( [ "notElectron", "Long", "Eta25" , "FromD"]),
        "14_notElectron_Long_FromD_Eta25 p>5GeV"          : all_of( [ "notElectron", "Long", "Eta25" , "FromD", "p>5GeV"]),
        "15_notElectron_Long_FromD_Eta25 p<5GeV"          : all_of( [ "notElectron", "Long", "Eta25" , "FromD", "p<5GeV"]),
        "16_notElectron_Long_FromD_Eta25 p>3GeV pt>400MeV": all_of( [ "notElectron", "Long", "Eta25" , "FromD", "p>3GeV", "pt>400MeV"]),
        
        "17_notElectron_Long_strange_Eta25"                 : all_of( [ "notElectron", "Long", "Eta25" , "strange"]),
        "18_notElectron_Long_strange_Eta25 p>5GeV"          : all_of( [ "notElectron", "Long", "Eta25" , "strange", "p>5GeV"]),
        "19_notElectron_Long_strange_Eta25 p<5GeV"          : all_of( [ "notElectron", "Long", "Eta25" , "strange", "p<5GeV"]),
        "20_notElectron_Long_strange_Eta25 p>3GeV pt>400MeV": all_of( [ "notElectron", "Long", "Eta25" , "strange", "p>3GeV", "pt>400MeV"]),
        
        "21_Electron_Velo" :                               all_of( [ "Electron", "Velo" ]),
        "22_Electron_Velo_Forward" :                       all_of( [ "Electron", "Velo", "Forward"]),
        "23_Electron_Velo_Backward":                       all_of( [ "Electron", "Velo", "Backward"]),
        "24_Electron_Velo_Eta25"   :                       all_of( [ "Electron", "Velo", "Eta25" ]), 

        "25_Electron_Long_Eta25"   :                       all_of( [ "Electron", "Long", "Eta25"]),
        "26_Electron_Long_Eta25 p>5GeV" :                  all_of( [ "Electron", "Long", "Eta25", "p>5GeV"]),
        "27_Electron_Long_Eta25 p<5GeV" :                  all_of( [ "Electron", "Long", "Eta25", "p<5GeV"]),
        "28_Electron_Long_Eta25 p>3GeV pt>400MeV" :        all_of( [ "Electron", "Long", "Eta25", "p>3GeV", "pt>400MeV"]),

        "29_Electron_Long_FromB_Eta25"                 :   all_of( [ "Electron", "Long", "Eta25" , "FromB"]),
        "30_Electron_Long_FromB_Eta25 p>5GeV"          :   all_of( [ "Electron", "Long", "Eta25" , "FromB", "p>5GeV"]),
        "31_Electron_Long_FromB_Eta25 p<5GeV"          :   all_of( [ "Electron", "Long", "Eta25" , "FromB", "p<5GeV"]),
        "32_Electron_Long_FromB_Eta25 p>3GeV pt>400MeV":   all_of( [ "Electron", "Long", "Eta25" , "FromB", "p>3GeV", "pt>400MeV"]),
        
        "33_Electron_Long_FromD_Eta25"                 :   all_of( [ "Electron", "Long", "Eta25" , "FromD"]),
        "34_Electron_Long_FromD_Eta25 p>5GeV"          :   all_of( [ "Electron", "Long", "Eta25" , "FromD", "p>5GeV"]),
        "35_Electron_Long_FromD_Eta25 p<5GeV"          :   all_of( [ "Electron", "Long", "Eta25" , "FromD", "p<5GeV"]),
        "36_Electron_Long_FromD_Eta25 p>3GeV pt>400MeV":   all_of( [ "Electron", "Long", "Eta25" , "FromD", "p>3GeV", "pt>400MeV"]),
        
        "37_Electron_Long_strange_Eta25"                 : all_of( [ "Electron", "Long", "Eta25" , "strange"]),
        "38_Electron_Long_strange_Eta25 p>5GeV"          : all_of( [ "Electron", "Long", "Eta25" , "strange", "p>5GeV"]),
        "39_Electron_Long_strange_Eta25 p<5GeV"          : all_of( [ "Electron", "Long", "Eta25" , "strange", "p<5GeV"]),
        "40_Electron_Long_strange_Eta25 p>3GeV pt>400MeV": all_of( [ "Electron", "Long", "Eta25" , "strange", "p>3GeV", "pt>400MeV"]),

    }
    return velochecker
