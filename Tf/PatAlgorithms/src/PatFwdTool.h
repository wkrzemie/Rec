/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATFWDTOOL_H
#define PATFWDTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "PatFwdTrackCandidate.h"
#include "PatFwdPlaneCounter.h"
#include "PatFwdRegionCounter.h"

#include "PatKernel/PatForwardHit.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "Kernel/STLExtensions.h"
// from TrackEvent
#include "Event/StateParameters.h"


static const InterfaceID IID_PatFwdTool ( "PatFwdTool", 1, 0 );

  /** @class PatFwdTool PatFwdTool.h
   *  This tool holds the parameters to extrapolate tracks
   *  in the field.
   *
   *  @author Olivier Callot
   *  @date   2005-04-01 Initial version
   *  @date   2007-08-20 Update for A-Team tracking framework
   */

class PatFwdTool : public GaudiTool {
public:

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_PatFwdTool; }

  /// Standard constructor
  PatFwdTool( const std::string& type,
              const std::string& name,
              const IInterface* parent);

  StatusCode initialize() override;

  double zReference() const { return m_zReference; }

  const std::vector<double>& zOutputs() const { return m_zOutputs; }

  bool fitXCandidate( PatFwdTrackCandidate& track, double maxChi2, int minPlanes ) const; // FIXME: for now, this updates m_zMagnet

  bool fitStereoCandidate( PatFwdTrackCandidate& track, double maxChi2, int minPlanes ) const;

  void setRlDefault ( PatFwdTrackCandidate& track,
                      PatFwdHits::const_iterator itBeg,
                      PatFwdHits::const_iterator itEnd ) const;

  void setXAtReferencePlane( const PatFwdTrackCandidate& track, LHCb::span<PatForwardHit*> range) const;

  template <typename T> T driftResidual( int amb, T dist, T dx ) const {
    // Take closest distance if amb == 0
    auto smallest = [](T a, T b) { return std::abs(a)<std::abs(b) ? a : b ; };
    return amb == 0 ? smallest( dist+dx, dist-dx ) : ( dist + amb * dx );
  }

  double distanceForXFit( const PatFwdTrackCandidate& track, const PatFwdHit* hit, double ica) const {
    auto dist = hit->x() - track.x( hit->z() - zReference() );
    return hit->isOT() ? driftResidual( hit->rlAmb(), dist, ica*hit->driftDistance() ) : dist;
  }

  double distanceForYFit( const PatFwdTrackCandidate& track, const PatFwdHit* hit, double ica) const {
    auto dist =  distanceHitToTrack( track, hit );
    if ( hit->isOT() ) dist *= ica ;
    return - dist / hit->hit()->dxDy();
  }

  double chi2Hit( const PatFwdTrackCandidate& track, const PatFwdHit* hit) const {
    auto dist = distanceHitToTrack( track, hit );
    return dist * dist * hit->hit()->weight();
  }

  double distanceHitToTrack( const PatFwdTrackCandidate& track, const PatFwdHit* hit) const {
    auto dist = hit->x() - track.x( hit->z() - zReference() );
    return hit->isOT() ? driftResidual( hit->rlAmb(), dist*track.cosAfter(), hit->driftDistance() ) : dist ;
  }

  double chi2Magnet( const PatFwdTrackCandidate& track) const {
      //== Error component due to the contraint of the magnet centre
      double dist      = distAtMagnetCenter( track );
      double errCenter = m_xMagnetTol + track.dSlope() * track.dSlope() * m_xMagnetTolSlope;
      return  dist * dist / errCenter;
  }

  double distAtMagnetCenter( const PatFwdTrackCandidate& track ) const {
    auto dz = m_zMagnet - zReference();
    return track.xStraight( m_zMagnet ) - track.xMagnet( dz );
  }

  double chi2PerDoF( PatFwdTrackCandidate& track ) const;

  bool removeYIncompatible(PatFwdTrackCandidate& track, double tol, int minPlanes ) const {
    bool hasChanged = false;
    for ( PatFwdHit* hit : track.coords() ) {
      if ( !hit->isSelected() ) continue;
      if ( !hit->hit()->isYCompatible( track.y( hit->z() - zReference() ), tol ) ) {
        hit->setSelected( false );
        hasChanged = true;
      }
    }
    if ( hasChanged ) return fitStereoCandidate( track, 1000000., minPlanes );

    PatFwdPlaneCounter planeCount( track.coordBegin(), track.coordEnd() );
    return planeCount.nbDifferent() >= minPlanes;
  }

  double changeInY(const  PatFwdTrackCandidate& track ) const {
    double yOriginal = track.yStraight( zReference() );
    if (LIKELY(!m_withoutBField))
      yOriginal += track.dSlope() * track.dSlope() * track.slY() * m_yParams[0];
    return yOriginal - track.y( 0. );
  }

  double qOverP( const PatFwdTrackCandidate& track ) const;

  //=========================================================================
  //  Returns center of magnet for velo track
  //=========================================================================
  double zMagnet( const PatFwdTrackCandidate& track ) const {
    //== correction behind magnet neglected
    return  ( m_zMagnetParams[0] +
              m_zMagnetParams[2] * track.slX2() +
              m_zMagnetParams[4] * track.slY2() );
  }
  double magscalefactor() const { return m_magFieldSvc->signedRelativeCurrent();} ;

private:
  double  updateTrackAndComputeZMagnet( PatFwdTrackCandidate& track, const PatFwdHit* hit ) const;

  bool fitXProjection ( PatFwdTrackCandidate& track,
                        PatFwdHits::const_iterator itBeg,
                        PatFwdHits::const_iterator itEnd,
                        bool onlyXPlanes  ) const;
  bool fitYProjection( PatFwdTrackCandidate& track,
                       PatFwdHits::const_iterator itBeg,
                       PatFwdHits::const_iterator itEnd ) const ;


private:
  ILHCbMagnetSvc*     m_magFieldSvc;
  mutable double      m_zMagnet;

  Gaudi::Property<std::vector<double>> m_zMagnetParams { this, "ZMagnetParams",  {}};
  Gaudi::Property<std::vector<double>> m_xParams { this,  "xParams",  {}};
  Gaudi::Property<std::vector<double>> m_yParams { this,  "yParams",  {}};
  Gaudi::Property<std::vector<double>> m_momentumParams { this, "momentumParams"  ,  {}};
  Gaudi::Property<double>              m_zReference { this, "ZReference", StateParameters::ZMidT }; // TODO: move this into PatFwdTrackCandidate, make all methods 'const'
  Gaudi::Property<std::vector<double>> m_zOutputs { this, "ZOutput",{StateParameters::ZBegT,StateParameters::ZMidT,StateParameters::ZEndT}};
  Gaudi::Property<double>              m_xMagnetTol { this, "xMagnetTol", 3. };
  Gaudi::Property<double>              m_xMagnetTolSlope { this, "xMagnetTolSlope" , 40. };

  Gaudi::Property<bool> m_withoutBField { this, "withoutBField", false };
  Gaudi::Property<bool> m_ambiguitiesFromPitchResiduals { this, "AmbiguitiesFromPitchResiduals", true };
};

#endif // PATFWDTOOL_H
