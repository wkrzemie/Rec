/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "PatBBDTSeedClassifier.h"
#include "Kernel/STLExtensions.h"
#include "GaudiKernel/IFileAccess.h"
#include <numeric>
#include <utility>
#include <stdexcept>
#include <cstdlib>

#include "range/v3/view.hpp"
#include "range/v3/front.hpp"
#include "range/v3/numeric.hpp"
#include "range/v3/algorithm.hpp"
#include "range/v3/getlines.hpp"

DECLARE_COMPONENT( PatBBDTSeedClassifier )


namespace
{
  using namespace std::string_literals;
  using namespace PatBBDTSeedClassifier_details;
  using namespace ranges;

  static const auto s_binsEdgeMap = std::array{
    std::pair{"seed_chi2PerDoF"s, std::array{0.984236677055,1.50321304937,2.42723090951} },
    std::pair{"seed_p"s, std::array{3306.52056882,6433.94177793,14679.2279654} },
    std::pair{"seed_pt"s, std::array{974.757029327,1150.16476634,1398.70883468}},
    std::pair{"seed_nLHCbIDs"s, std::array{12.0,19.0,22.0}},
    std::pair{"abs_seed_x"s, std::array{165.914375049,391.313089922,812.675652024}},
    std::pair{"abs_seed_y"s, std::array{84.4601830606,194.563503148,465.430832506}},
    std::pair{"abs_seed_tx"s, std::array{0.0719119714649,0.169597614236,0.34911262278}},
    std::pair{"abs_seed_ty"s, std::array{0.011964398121,0.0283512657104,0.0636668621569}},
    std::pair{"seed_r"s, std::array{std::pow(267.974713881,2),
                                          std::pow(565.104115682,2),
                                          std::pow(1041.65791307,2)}},
    std::pair{"pseudo_rapidity"s, std::array{std::tanh(0.0841431458013),std::tanh(0.183059022997),std::tanh(0.35202555464)}} };

  static constexpr int s_featuresNumber = s_binsEdgeMap.size();
  static constexpr int s_binPerFeature = s_binsEdgeMap[0].second.size();
  static const auto observables = view::keys(s_binsEdgeMap)|view::join( ',')|to_<std::string>();

  std::string getEnvVar( const char* key ) {
    const char *val = getenv( key );
    return val ? val : "" ;
  }

  int combine_indices(int offset, int indx) {
    return offset*(s_binPerFeature+1)+indx;
  }

  std::pair<int,float> toOffsetValue(const std::string& line) {
    auto tokens = view::split(line,',') | view::chunk(s_featuresNumber);
    auto stoi = [](const std::string& s) { return std::stoi(s); };
    auto index = accumulate( front(tokens), 0, combine_indices, stoi );
    if (index>=s_nBins) throw std::invalid_argument("index out of range");
    return { index, std::stof( to_<std::string>( front(*next(begin(tokens))) ) ) };
  }

  template <typename Range>
  int getOffset( const Range& params ) {
    assert( params.size() == s_featuresNumber );
    auto get_index = [](const auto& edges, double value) {
      return distance(begin(edges),lower_bound(edges, value));
    };
    return inner_product( view::values(s_binsEdgeMap) , params, 0,
                          combine_indices, get_index );
  }
}

StatusCode PatBBDTSeedClassifier::initialize()
{
    StatusCode sc =  GaudiTool::initialize();
    if(sc.isFailure()) return Error("Failed to initialize", sc);

    // allocate storage
    m_tupleClassifier.resize( PatBBDTSeedClassifier_details::s_nBins, 0.0f );

    auto fileAccess = service<IFileAccess>( "VFSSvc" );
    if ( !fileAccess ) {
      error() << "Unable to locate IFileAccess('VFSSvc') service" << endmsg ;
      return StatusCode::FAILURE;
    }

    std::string classifierPath = getEnvVar("PARAMFILESROOT")+"/data/"+m_lookupTableLocation;
    auto file = fileAccess->open ( classifierPath ) ;
    if (  !file->good() ) {
      error() << "Unable to open file '" << classifierPath << "'" << endmsg ;
      return StatusCode::FAILURE;
    }

    info()<<"open file: " << classifierPath<<endmsg;
    try {
      auto lines = getlines(*file);
      info()<<"input features list "<< lines.cached() <<endmsg;
      if (!equal(lines.cached(),observables+",pred")) {
        throw std::invalid_argument( "expected \"" + observables
                                      + ",pred\" but got \"" + lines.cached() + "\" instead" );
      }
      int n=0;
      for_each(lines|view::drop_exactly(1)|view::transform(toOffsetValue),
               [&](const std::pair<int,float>& offsetValue) {
        m_tupleClassifier[offsetValue.first] = offsetValue.second;
        ++n;
      } );
      if (n!=s_nBins) {
        throw std::invalid_argument(  "expected " + std::to_string(s_nBins )
                  + " entries, got " + std::to_string(n) + "instead" );
      }
      return StatusCode::SUCCESS;
    } catch (const std::invalid_argument& e) {
      error() << "problem parsing data: " << e.what() << endmsg;
      return StatusCode::FAILURE;
    }
}

double PatBBDTSeedClassifier::getMvaValue(const LHCb::Track& track) const
{
  //============================================================================
  // Evaluate the Seed Classifier discriminant for the T-seeds
  //============================================================================

  const auto position = track.position();
  const auto slopes  = track.slopes();

  const auto seed_r2 = position.x()*position.x()+position.y()*position.y();
  const auto tanh_pseudo_rapidity = track.pt()/track.p();

  return m_tupleClassifier[ getOffset( std::array{ track.chi2PerDoF(),track.p(),  track.pt(),
                                     static_cast<double>(track.nLHCbIDs()),
                                     std::abs(position.x()), std::abs(position.y()),
                                     std::abs(slopes.x()), std::abs(slopes.y()),
                                     seed_r2, tanh_pseudo_rapidity }) ];
}
