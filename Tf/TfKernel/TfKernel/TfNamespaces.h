/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TfNamespaces.h,v 1.2 2007-08-20 11:07:07 jonrob Exp $

// This file does nothing other than provide doxygen with a comment for the
// Tf namespaces

//-----------------------------------------------------------------------------
/** @namespace Tf
 *
 *  General namespace for Tf (Track Finding) software
 *
 *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
 *  @date   08/07/2004
 */
//-----------------------------------------------------------------------------
