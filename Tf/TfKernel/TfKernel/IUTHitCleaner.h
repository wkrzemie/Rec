#ifndef TFTOOLS_IUTHITCLEANER_H
#define TFTOOLS_IUTHITCLEANER_H 1

// from UTL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// From Tf
#include "TfKernel/UTHit.h"
//#include "TfKernel/Region.h"


namespace Tf
{

  /** @class IUTHitCleaner IUTHitCleaner.h TfKernel/IUTHitCleaner.h
   *
   *  Interface to tool that 'cleans' ranges of UTHits
   *
   *  @author A. Beiter (based on code by S. Hansmann-Menzemer, 
   *  W. Hulsbergen, C. Jones, K. Rinnert)
   *  @date   2018-09-04
   */
  struct IUTHitCleaner : extend_interfaces<IAlgTool>
  {
    /// Interface ID
    DeclareInterfaceID( IUTHitCleaner, 2, 0 );


    /** Clean the given range of UT hits
     *  @param[in]  input The range of UT hits to clean
     *  @param[out] output The selected hits
     *  @return The number of removed hits
     */
    template<class INPUTDATA >
    inline UTHits cleanHits( const INPUTDATA & input ) const
    {
      return cleanHits ( input.begin(), input.end() );
    }

    /** Clean the given range of hits
     *  @param[in] begin Iterator to the start of a range of UT hits
     *  @param[in] end   Iterator to the start of a range of UT hits
     *  @param[out] output The selected hits   
     *  @return The number of removed hits
     */
    virtual UTHits cleanHits( const UTHits::const_iterator begin,
                              const UTHits::const_iterator end ) const = 0;

  };

}

#endif // TFTOOLS_IUTHITCLEANER_H
