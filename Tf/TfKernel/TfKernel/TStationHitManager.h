/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file TStationHitManager.h
 *
 *  Header file for class : Tf::TStationHitManager
 *
 *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
 *  @date   2007-06-01
 */
//-----------------------------------------------------------------------------
#ifndef TFTOOLS_TSTATIONHITMANAGER_H
#define TFTOOLS_TSTATIONHITMANAGER_H 1

// Include files
#include <functional>
#include <memory>
#include "range/v3/view/transform.hpp"
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

// Tf framework
#include "TfKernel/IndexedHitContainer.h"
#include "TfKernel/IndexedBitArray.h"
#include "TfKernel/IOTHitCreator.h"
#include "TfKernel/IITHitCreator.h"
#include "TfKernel/IOTHitCleaner.h"
#include "TfKernel/ISTHitCleaner.h"
#include "TfKernel/LineHit.h"
#include "TfKernel/HitExtension.h"
#include "TfKernel/RecoFuncs.h"
#include "TfKernel/RegionID.h"
#include "TfKernel/TfIDTypes.h"

/// Static interface ID
static const InterfaceID IID_TStationHitManager ( "TStationHitManager", 2, 0 );

namespace Tf
{
  /** @class TStationHitManager TStationHitManager.h TfKernel/TStationHitManager.h
   *
   *  T station hit manager. Used to manage extended hit objects for the T
   *  Stations (OT and IT).
   *
   *  Methods are provided to return the hits in a selected part of the detectors.
   *  E.g.
   *
   *  @code
   *  // Get all the hits in the T stations
   *  Tf::TStationHitManager::HitRange range = hitMan->hits();
   *
   *  // Get all the hits in one specific T station
   *  Tf::TStationID sta = ...;
   *  Tf::TStationHitManager::HitRange range = hitMan->hits(sta);
   *
   *  // Get all the hits in one specific layer of one T station
   *  Tf::TStationID sta = ...;
   *  Tf::TLayerID   lay = ...;
   *  Tf::TStationHitManager::HitRange range = hitMan->hits(sta,lay);
   *
   *  // Get all the hits in a specific 'region' of one layer of one T station
   *  Tf::TStationID sta = ...;
   *  Tf::TLayerID   lay = ...;
   *  Tf::TRegionID  reg = ...;
   *  Tf::TStationHitManager::HitRange range = hitMan->hits(sta,lay,reg);
   *  @endcode
   *
   *  In all cases the returned Range object acts like a standard vector or container :-
   *
   *  @code
   *   // Iterate over the returned range
   *  for ( Tf::TStationHitManager::HitRange::const_iterator iR = range.begin();
   *        iR != range.end(); ++iR )
   *  {
   *    // do something with the hit
   *  }
   *  @endcode
   *
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-06-01
   **/

  template < class Hit >
  class TStationHitManager : public GaudiTool,
                             public IIncidentListener
  {

  public:
    typedef  IndexedHitContainer< Tf::RegionID::OTIndex::kNStations,
                                  Tf::RegionID::OTIndex::kNLayers,
                                  Tf::RegionID::OTIndex::kNRegions + Tf::RegionID::ITIndex::kNRegions,
                                  Hit* > Hits;
    /// range object for Hits
    typedef typename Hits::HitRange HitRange;

  public:

    /// InterfaceID for this tool
    static const InterfaceID& interfaceID() { return IID_TStationHitManager; }

    /// Standard Constructor
    TStationHitManager( const std::string& type,
                        const std::string& name,
                        const IInterface* parent ) ;

    /// Tool initialization
    StatusCode initialize ( ) override;

    /// Tool finalization
    StatusCode finalize ( ) override;

    /// Handle method for incident service callbacks
    void handle ( const Incident& incident ) override
    {
      if ( IncidentType::BeginEvent == incident.type() ) clearHits() ;
    }

  public:

    /** Load the hits for a given region of interest
     *
     *  @param[in] sta    Station ID
     *  @param[in] lay    Station layer ID
     *  @param[in] region Region within the layer
     *
     *  @return Range object for the hits in the selected region of interest
     */
    inline HitRange hits( const TStationID sta,
                          const TLayerID   lay,
                          const TRegionID  region ) const
    {
      if ( UNLIKELY(!m_hits_ready) ) { prepareHits(); }
      return m_hits.range( sta,lay, region );
    }

    /** Load the hits for a given region of interest
     *
     *  @param[in] sta    Station ID
     *  @param[in] lay    Station layer ID
     *
     *  @return Range object for the hits in the selected region of interest
     */
    inline HitRange hits( const TStationID sta,
                          const TLayerID   lay ) const
    {
      if ( UNLIKELY(!m_hits_ready) ) { prepareHits(); }
      return m_hits.range(sta,lay) ;
    }

    /** Load the hits for a given region of interest
     *
     *  @param[in] sta    Station ID
     *
     *  @return Range object for the hits in the selected region of interest
     */
    inline HitRange hits( const TStationID sta ) const
    {
      if ( UNLIKELY(!m_hits_ready) ) { prepareHits(); }
      return m_hits.range(sta);
    }


    /** Load the all hits
     *  @return Range object for the hits in the selected region of interest
     */
    inline HitRange hits( ) const
    {
      if ( UNLIKELY(!m_hits_ready) ) { prepareHits(); }
      return m_hits.range();
    }

    // Not clear to me if these following methods  should be in the common interface
    // Are they pat specific, so should move to the pat implemation ?

    /** Load the hits for a given region of interest
     *  In addition, specify a minimum x value
     *
     *  @param[in] xMin   Minimum x value of hit (at y=0)
     *  @param[in] sta    Station ID
     *  @param[in] lay    Station layer ID
     *  @param[in] region Region within the layer
     *
     *  @return Range object for the hits in the selected region of interest
     */
    inline HitRange hitsWithMinX( const double xMin,
                                  const TStationID sta,
                                  const TLayerID   lay,
                                  const TRegionID  region ) const
    {
      if ( UNLIKELY(!m_hits_ready) ) { prepareHits(); }
      auto range = m_hits.range(sta,lay,region);
      return { std::lower_bound( range.begin(), range.end(),
                                 xMin, Tf::compByX() ),
               range.end() };
    }

    /** Load the hits for a given region of interest
     *  In addition, specify an x range
     *
     *  @param[in] xMin   Minimum x value of hit (at y=0)
     *  @param[in] xMax   Maximum x value of hit (at y=0)
     *  @param[in] sta    Station ID
     *  @param[in] lay    Station layer ID
     *  @param[in] region Region within the layer
     *
     *  @return Range object for the hits in the selected region of interest
     */
    inline HitRange hitsInXRange( const double xMin, const double xMax,
                                  const TStationID sta,
                                  const TLayerID   lay,
                                  const TRegionID  region ) const
    {
      if ( UNLIKELY(!m_hits_ready) ) { prepareHits(); }
      auto range = m_hits.range(sta,lay,region);
      auto first = std::lower_bound( std::begin(range), std::end(range),
                                     xMin, Tf::compByX() ) ;
      return { first, std::upper_bound( first, std::end(range),
                                        xMax, Tf::compByX() ) } ;
    }

    /** Retrieve the Region for a certain IT or OT region ID. The region
     *   knows its 'size' and gives access to its hits.
     *
     *  Note that the method returns a pointer to the base class Tf::EnvelopeBase
     *  This object knows most basic questions, but for more complex tasks the user
     *  can dynamic cast it to the actual region object for OT or IT
     *
     *  @param[in] iStation Station ID
     *  @param[in] iLayer   Station layer ID
     *  @param[in] iRegion  Region within the layer
     *
     *  @return Pointer to the Tf::EnvelopeBase object
     */
    inline const Tf::EnvelopeBase* region( const TStationID iStation,
                                           const TLayerID   iLayer,
                                           const TRegionID  iRegion ) const
    {
      return ( iRegion > maxOTRegions()-1 ?
               static_cast<const Tf::EnvelopeBase*>(this->m_ithitcreator->region(iStation,
                                                                                 iLayer,
                                                                                 iRegion.itRegionID())) :
               static_cast<const Tf::EnvelopeBase*>(this->m_othitcreator->region(iStation,
                                                                                 iLayer,
                                                                                 iRegion.otRegionID())) );
    }

  private:

    /// Clear the hit containers for a new event or for running on
    /// full event after decoding on demand
    void clearHits () const;

  protected:

    /** Are all the hits ready
     *  @return boolean indicating if all the hits in the given region are ready or not
     *  @retval TRUE  Hits are ready
     *  @retval FALSE Hits are not ready
     */
    inline bool allHitsPrepared() const { return m_hits_ready; }

    /// Access the maximum number of stations
    static constexpr TStationID maxStations()  { return TStationID(Tf::RegionID::OTIndex::kNStations);   }
    /// Access the maximum number of layers
    static constexpr TLayerID maxLayers()      { return TLayerID(Tf::RegionID::OTIndex::kNLayers);     }
    /// Access the maximum number of OT regions
    static constexpr OTRegionID maxOTRegions() { return OTRegionID(Tf::RegionID::OTIndex::kNRegions); }
    /// Access the maximum number of IT regions
    static constexpr ITRegionID maxITRegions() { return ITRegionID(Tf::RegionID::ITIndex::kNRegions); }
    /// Access the maximum number of regions
    static constexpr TRegionID maxRegions()    { return TRegionID( Tf::RegionID::OTIndex::kNRegions + Tf::RegionID::ITIndex::kNRegions );    }

  private:

    Tf::OTHits cleanHits( Tf::OTHitRange hits ) const { return m_otCleaner->cleanHits( hits ); }
    Tf::STHits cleanHits( Tf::STHitRange hits ) const { return m_itCleaner->cleanHits( hits ); };

    /// Is OT hit cleaning activated
    bool cleanOTHits() const { return m_cleanOTHits; }

    /// Is IT hit cleaning activated
    bool cleanITHits() const { return m_cleanITHits; }

    template <bool,bool> void i_prepareHits() const;

  public:
    /** Initialise all the hits for the current event
     */
    virtual void prepareHits() const;

  private:

    /// The underlying OT hit creator
    Tf::IOTHitCreator* m_othitcreator = nullptr;

    /// The underlying IT hit creator
    Tf::IITHitCreator* m_ithitcreator = nullptr;

    /// The OT hit cleaner
    Tf::IOTHitCleaner * m_otCleaner = nullptr;

    /// The ST hit cleaner
    Tf::ISTHitCleaner * m_itCleaner = nullptr;

    mutable Hits m_hits { 16384 };// initial capacity of container...

    // Flags to indicate which hits are ready
    mutable bool m_hits_ready { false };

    /// Should OT hit cleaning be performed ?
    Gaudi::Property<bool> m_cleanOTHits { this, "CleanOTHits", false };

    /// Should IT hit cleaning be performed ?
    Gaudi::Property<bool> m_cleanITHits { this, "CleanITHits", false };

  };


  template<class Hit>
  TStationHitManager<Hit>::TStationHitManager( const std::string& type,
                                               const std::string& name,
                                               const IInterface* parent)
  : GaudiTool (type, name, parent)
  {
    declareInterface<TStationHitManager<Hit> >(this);
  }

  template<class Hit>
  StatusCode TStationHitManager<Hit>::initialize ( )
  {
    StatusCode sc = GaudiTool::initialize();
    if ( sc.isFailure() ) return sc;

    // set up to be told about each new event
    incSvc()->addListener(this, IncidentType::BeginEvent);

    // load our hit creators

    m_othitcreator = tool<IOTHitCreator>("Tf::OTHitCreator", "OTHitCreator");
    m_ithitcreator = tool<IITHitCreator>("Tf::STHitCreator<Tf::IT>", "ITHitCreator");
    if (!m_othitcreator || !m_ithitcreator) return StatusCode::FAILURE;

    // load private hit cleaners, if needed
    if ( cleanOTHits() ) m_otCleaner = this->tool<Tf::IOTHitCleaner>("Tf::OTHitCleaner","OTHitCleaner",this);
    if ( cleanITHits() ) m_itCleaner = this->tool<Tf::ISTHitCleaner>("Tf::STHitCleaner","ITHitCleaner",this);

    // make sure we are ready for first event
    this->clearHits();

    return sc;
  }

  template<class Hit>
  inline StatusCode TStationHitManager<Hit>::finalize ( )
  {
    this->clearHits();
    return GaudiTool::finalize();
  }

  template<class Hit>
  inline void TStationHitManager<Hit>::clearHits() const
  {
    HitRange rng = m_hits.range();
    std::for_each( rng.begin(), rng.end(), std::default_delete<Hit>() ) ;
    m_hits.clear();
    m_hits_ready = false;
  }

  template<class Hit>
  void TStationHitManager<Hit>::prepareHits() const
  {
    if ( UNLIKELY(m_hits_ready) ) return;
    if ( UNLIKELY(cleanOTHits()) ) {
        LIKELY(cleanITHits()) ? i_prepareHits<true,true>()  : i_prepareHits<true,false>();
    } else  {
        UNLIKELY(cleanITHits()) ? i_prepareHits<false,true>() : i_prepareHits<false,false>();
    }
    m_hits_ready = true;
  }

  template <typename Hit>
  template <bool cleanOT, bool cleanIT>
  void TStationHitManager<Hit>::i_prepareHits() const
  {
    TRegionID ot_begin = OTRegionID{0};
    TRegionID ot_end   = OTRegionID{Tf::RegionID::OTIndex::kNRegions};
    TRegionID it_begin = ITRegionID{0};
    TRegionID it_end   = ITRegionID{Tf::RegionID::ITIndex::kNRegions};

    auto make_hits = [&](const auto& container) {
      return ranges::view::transform( container, [](const auto* hit) { return new Hit(*hit); } );
    };

    for ( int i = 0; i!= Tf::RegionID::OTIndex::kNStations*Tf::RegionID::OTIndex::kNLayers; ++i) {
      TStationID sta = i/Tf::RegionID::OTIndex::kNLayers;
      TLayerID   lay = i%Tf::RegionID::OTIndex::kNLayers;
      for ( auto region = ot_begin; region != ot_end; ++region ) {
        auto hits = m_othitcreator->hits(sta,lay,region.otRegionID());
        if ( cleanOT ) {
          m_hits.insert( sta, lay, region, make_hits(cleanHits(hits) ));
        } else {
          m_hits.insert( sta, lay, region, make_hits(hits) );
        }
        auto rng = m_hits.range_(sta,lay,region);
        std::sort ( rng.first, rng.second, Tf::increasingByXAtYEq0<>() );
      }
      for ( auto region = it_begin; region != it_end; ++region ){
        auto hits = m_ithitcreator->hits(sta,lay,region.itRegionID());
        if ( cleanIT ) { // clean hits and convert those selected
          m_hits.insert( sta, lay, region, make_hits(cleanHits(hits)) );
        } else {
          m_hits.insert( sta, lay, region, make_hits(hits) );
        }
        auto rng = m_hits.range_(sta,lay,region);
        std::sort ( rng.first, rng.second, Tf::increasingByXAtYEq0<>() );
      }
    }
  }

  //--------------------------------------------------------------------------------------------

} // end Tf namespace

#endif // TFTOOLS_TSTATIONHITMANAGER_H
