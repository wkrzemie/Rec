/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TsaBaseAlg.h,v 1.1.1.1 2007-08-14 13:50:47 jonrob Exp $
#ifndef _TSABASEALG_H_
#define _TSABASEALG_H_

//#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>
//#include "TsaKernel/stopwatch.h"

namespace Tf
{
  namespace Tsa
  {

    /** @class BaseAlg
     *
     *  Base class for Tsa Initialization
     *
     *  @author M.Needham
     *  @date   07/03/2005
     */
    class BaseAlg: public GaudiAlgorithm
    {

    public:

      // Constructors and destructor
      BaseAlg(const std::string& name,
                 ISvcLocator* pSvcLocator);
      virtual ~BaseAlg();

      // IAlgorithm members
      StatusCode initialize() override;
      StatusCode finalize() override;

    };

  }
}

#endif // _TSABASEALG_H_
