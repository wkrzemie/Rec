/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TsaBaseAlg.h"

using namespace Tf::Tsa;

BaseAlg::BaseAlg(const std::string& name,
                 ISvcLocator* pSvcLocator):
  GaudiAlgorithm(name, pSvcLocator)
{
  //  declareProperty("time", m_time = true);
}

BaseAlg::~BaseAlg()
{
  // BaseAlg destructor
}

StatusCode BaseAlg::initialize()
{
  // Initializes BaseAlg at the begin of program execution.

  StatusCode sc = GaudiAlgorithm::initialize();
  if (sc.isFailure()){
    return Error("Failed to initialize");
  }

  return StatusCode::SUCCESS;
}


StatusCode BaseAlg::finalize(){

  //  if (m_time == true){
  //   info() << "*** Processed: " << m_nEvent<< endmsg;
  //    info() << "Time per event" << timer().sum()/(m_nEvent-1) << endmsg;
  // }

  return GaudiAlgorithm::finalize();
}

