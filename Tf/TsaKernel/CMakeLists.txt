###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TsaKernel
################################################################################
gaudi_subdir(TsaKernel v3r8)

gaudi_depends_on_subdirs(Det/OTDet
                         Det/STDet
                         Event/DigiEvent
                         Event/TrackEvent
                         Kernel/LHCbMath
                         Tf/TfKernel)

find_package(GSL)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(TsaKernel
                  src/*.cpp
                  PUBLIC_HEADERS TsaKernel
                  INCLUDE_DIRS GSL Event/DigiEvent Tf/TfKernel
                  LINK_LIBRARIES GSL OTDetLib STDetLib TrackEvent LHCbMathLib)

gaudi_add_dictionary(TsaKernel
                     dict/TsaKernelDict.h
                     dict/TsaKernelDict.xml
                     INCLUDE_DIRS GSL Event/DigiEvent Tf/TfKernel
                     LINK_LIBRARIES GSL OTDetLib STDetLib TrackEvent LHCbMathLib TsaKernel
                     OPTIONS "-U__MINGW32__")

