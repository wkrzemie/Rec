/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: SeedHit.cpp,v 1.3 2007-10-09 17:56:26 smenzeme Exp $
// Include files 

// local
#include "TsaKernel/SeedHit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SeedHit
//
// 2007-07-17 : Chris Jones
//-----------------------------------------------------------------------------

std::ostream& Tf::Tsa::SeedHit::fillStream(std::ostream& s) const
{
  return s << "{"
    //<< " Key=" << this->key()
           << " " << lhcbID()
           << " x,y,z,r=" << z() << "," << y() << "," << z() << "," << r()
           << " x(Max,Min)=" << xMax() << "," << xMin() 
           << " y(Max,Min)=" << yMax() << "," << yMin()
           << " }";
}
