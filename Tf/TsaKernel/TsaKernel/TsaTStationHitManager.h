/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file TsaTStationHitManager.h
 *
 *  Header file for class : Tf::Tsa::TStationHitManager
 *
 *  @author S. Hansmann-Menzemer, W. Houlsbergen, C. Jones, K. Rinnert
 *  @date   2007-06-01
 */
//-----------------------------------------------------------------------------

#ifndef TSAALGORITHMS_TSATSTATIONHITMANAGER_H
#define TSAALGORITHMS_TSATSTATIONHITMANAGER_H 1

#ifdef __INTEL_COMPILER         // Disable ICC warning
  #pragma warning(disable:654)  // Tf::Tsa::ITsaSeedStep::execute only partially overridden
  #pragma warning(push)
#endif
#include "TfKernel/TStationHitManager.h"
#ifdef __INTEL_COMPILER         // Re-enable ICC warning 654
  #pragma warning(pop)
#endif

#include "TsaKernel/TsaSeedingHit.h"

namespace Tf
{
  namespace Tsa
  {

    /** @class TStationHitManager TsaTStationHitManager.h
     *
     *  Derived implementation of an OT hit manager for Tsa.
     *
     *  @author Chris Jones
     *  @date   2007-08-16
     */
    struct TStationHitManager : Tf::TStationHitManager<Tf::Tsa::SeedingHit>
    {

      /// Standard constructor
      TStationHitManager( const std::string& type,
                          const std::string& name,
                          const IInterface* parent);

    };

  }
}

#endif // TSAALGORITHMS_TSATSTATIONHITMANAGER_H
