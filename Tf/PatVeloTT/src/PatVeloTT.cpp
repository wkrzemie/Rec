/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "PatVeloTTTool.h"

// local
#include "PatVeloTT.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PatVeloTT
//
// 2007-05-08 : Mariusz Witek
//-----------------------------------------------------------------------------

namespace {
    struct compChi2  {
      bool operator() (LHCb::Track* first, LHCb::Track* second ) const {
        return fabs(first->chi2PerDoF()) < fabs(second->chi2PerDoF()) ;
      }
    };
}


// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PatVeloTT )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PatVeloTT::PatVeloTT( const std::string& name,
                      ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
{
  if ( "Hlt" == context() ) { //@TODO/FIXME: is context() already set at this point????
    m_inputTracksName =  "";
    m_outputTracksName = "";
  } else {
    m_inputTracksName =  LHCb::TrackLocation::Velo;
    m_outputTracksName = LHCb::TrackLocation::VeloTT;
  }
  declareProperty("InputTracksName"     , m_inputTracksName);
  declareProperty("OutputTracksName"    , m_outputTracksName);
  declareProperty("removeUsedTracks"    , m_removeUsedTracks = true);
  declareProperty("InputUsedTracksNames", m_inputUsedTracksNames);
  declareProperty("fitTracks"           , m_fitTracks = true);
  declareProperty("Fitter"              , m_fitterName = "TrackMasterFitter" );
  declareProperty("maxChi2"             , m_maxChi2          = 5.);
  declareProperty("TrackSelectorName"   , m_trackSelectorName = "None");
  declareProperty("TimingMeasurement"   , m_doTiming = false);
  declareProperty("AddMomentumEstimate" , m_AddMomentumEstimate = false);
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PatVeloTT::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if(msgLevel(MSG::DEBUG)) debug() << "==> Initialize" << endmsg;

  m_veloTTTool = tool<ITracksFromTrack>("PatVeloTTTool", "PatVeloTTTool");

  m_trackSelector = ( m_trackSelectorName != "None" ?
                            tool<ITrackSelector>( m_trackSelectorName, this) :
                            nullptr );
  m_tracksFitter = tool<ITrackFitter>( m_fitterName, "Fitter", this );

  m_ttHitManager   = tool<Tf::TTStationHitManager <PatTTHit> >("PatTTStationHitManager");

  info() << " InputTracksName    = " << m_inputTracksName            << endmsg;
  info() << " OutputTracksName   = " << m_outputTracksName           << endmsg;

  if ( m_doTiming) {
    m_timerTool = tool<ISequencerTimerTool>( "SequencerTimerTool" );
    m_timerTool->increaseIndent();
    m_veloTTTime = m_timerTool->addTimer( "Internal VeloTT Tracking" );
    m_timerTool->decreaseIndent();
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PatVeloTT::execute() {

  if ( m_doTiming ) m_timerTool->start( m_veloTTTime );

  m_ttHitManager->prepareHits();

  LHCb::Tracks* outputTracks  = new LHCb::Tracks();
  put(outputTracks, m_outputTracksName);


  LHCb::Tracks* inputTracks   = get<LHCb::Tracks>( m_inputTracksName );

  // collect tracks pointers in local vector
  std::vector<LHCb::Track*> veloTracks;
  std::copy_if(  inputTracks->begin(), inputTracks->end(),
                 std::back_inserter(veloTracks),
                 [&](const LHCb::Track* t) { return acceptTrack(*t); } );

  if ( m_removeUsedTracks ) removeUsedTracks( veloTracks);

  // reconstruct tracks in TT

  outputTracks->reserve(veloTracks.size());
  for(auto* velotr : veloTracks ) {

    std::vector<LHCb::Track*> tmptracks;
    m_veloTTTool->tracksFromTrack(*velotr, tmptracks).ignore();

    std::vector<LHCb::Track*> fittracks;
    for (auto *ptr : tmptracks ) {
      auto sc = m_fitTracks ? m_tracksFitter->operator()(*ptr) : StatusCode::SUCCESS;

      if (sc.isSuccess()) {
        fittracks.push_back(ptr);
      } else {
        delete ptr;
      }
    }

    // choose best track
    if(!fittracks.empty()) {
      std::nth_element(fittracks.begin(),std::next(fittracks.begin()),fittracks.end(),compChi2());
      auto bestTrack = fittracks.front();
      auto tracks = fittracks.begin();

      if(bestTrack && bestTrack->chi2PerDoF() < m_maxChi2) {

        if(m_AddMomentumEstimate){
          //qop estimate
          //Get qop from VeloTT track
          const LHCb::State& state_VELOTT = *(bestTrack->stateAt(LHCb::State::Location::EndVelo));
          double qop = state_VELOTT.qOverP();

          //Find track state for Velo track - will write out qop to it
          LHCb::Track* veloTr = nullptr;
          for( auto& anc : bestTrack->ancestors() ) { //@TODO: why the loop?
            veloTr = anc;
          }

          // Add the qop estimate to all Velo track states
          if( veloTr ){
            for( const LHCb::State* state : veloTr->states() ) {
              (const_cast<LHCb::State*>(state))->setQOverP( qop ) ;
            }
          }
        }
        outputTracks->insert(bestTrack);
        ++tracks;
      }

      for (; tracks != fittracks.end(); tracks++) delete *tracks;
    }
    tmptracks.clear();
    fittracks.clear();
  }

  if ( m_doTiming ) m_timerTool->stop( m_veloTTTime );

  return StatusCode::SUCCESS;
}

//=============================================================================
bool PatVeloTT::acceptTrack(const LHCb::Track& track) const
{
  bool ok = !(track.checkFlag( LHCb::Track::Flags::Invalid) );
  ok = ok && (!(track.checkFlag( LHCb::Track::Flags::Backward) ));
  if (m_trackSelector)
    ok = ok && (m_trackSelector->accept(track));

  if(msgLevel(MSG::DEBUG)) debug() << " accept track " << ok << endmsg;
  return ok;
}

//=========================================================================
// Remove Velo tracks that has been used by other algorithms
//=========================================================================
void PatVeloTT::removeUsedTracks( std::vector<LHCb::Track*>& veloTracks){

  // collect tracks from indicated containers
  std::vector< LHCb::Track*> usedTracks;

  for (const auto& itTrName : m_inputUsedTracksNames ) {

    // Retrieving tracks
    LHCb::Tracks* stracks = get<LHCb::Tracks>(itTrName);
    if(!stracks) {
      Warning("Tracks not found at given location: ",StatusCode::SUCCESS).ignore();
      if(msgLevel(MSG::DEBUG)) debug() <<"Tracks not found at location: " << itTrName << endmsg;
      continue;
    }
    std::copy( stracks->begin(), stracks->end(), std::back_inserter(usedTracks) );
  }

  if(msgLevel(MSG::DEBUG)) debug() << " # used tracks to check: " << usedTracks.size() << endmsg;

  if( usedTracks.empty() ) return;

  auto end = std::remove_if(veloTracks.begin(),veloTracks.end(),
                            [&](const LHCb::Track *cand) {
                            return std::any_of( usedTracks.begin(), usedTracks.end(),
                                                [&](const LHCb::Track* t)
                                                { return matchingTracks(*cand,*t); } );
                            } );
  if(msgLevel(MSG::DEBUG)) debug() << " # used tracks found: " << std::distance(end,veloTracks.end()) << endmsg;
  veloTracks.erase( end, veloTracks.end() );
}


//=========================================================================
// Check if two tracks have same VElo hits
//=========================================================================
bool PatVeloTT::matchingTracks( const LHCb::Track& vttcand, const LHCb::Track& trackused) const
{
  const std::vector<LHCb::LHCbID>& ids1 = vttcand.lhcbIDs();
  const std::vector<LHCb::LHCbID>& ids2 = trackused.lhcbIDs();

  auto isVelo = [](const LHCb::LHCbID& id) {
                    return id.checkDetectorType(LHCb::LHCbID::channelIDtype::Velo) ||
                           id.checkDetectorType(LHCb::LHCbID::channelIDtype::VP);
                };

  // Calculate the number of common LHCbIDs -- use the fact that the ranges are sorted!
  int nCommon = 0;
  int nvelo1 = 0;
  int nvelo2 = 0;
  auto first1 = ids1.begin() ;
  auto last1  = ids1.end() ;
  auto first2 = ids2.begin() ;
  auto last2  = ids2.end() ;
  while (first1 != last1 && first2 != last2) {
    if ( *first1 < *first2 ) {
      if (isVelo(*first1)) ++nvelo1;
      ++first1;
    } else if ( *first2 < *first1 ) {
      if (isVelo(*first2)) ++nvelo2;
      ++first2;
    } else {
      if (isVelo(*first1)) { ++nCommon ; ++nvelo1; ++nvelo2; }
      ++first1;
      ++first2;
    }
  }
  // (at least) one of the ranges is exhausted at this point, but
  // don't know which one -- but counting an empty range has as
  // no more overhead than doing an explicit if, so we just defer
  // to count_if unconditionally...
  nvelo1+=std::count_if(first1,last1,isVelo);
  nvelo2+=std::count_if(first2,last2,isVelo);

  return nvelo1 != 0 && nvelo2 != 0 && (  1.0*nCommon > 0.9*std::max(nvelo1,nvelo2) );
}

//=============================================================================
