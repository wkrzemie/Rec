/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATVELOTT_H
#define PATVELOTT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiAlg/GaudiAlgorithm.h"
// from TrackInterfaces
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackSelector.h"
#include "TfKernel/TTStationHitManager.h"

// local
#include "PatKernel/PatTTHit.h"


  /** @class PatVeloTT PatVeloTT.h
   *
   *  Pat VeloTT algorithm
   *  
   *  @author Mariusz Witek
   *  @date   2007-05-08
   *  @update for A-Team framework 2007-08-20 SHM
   *
   */

  class PatVeloTT : public GaudiAlgorithm {
  public:
    /// Standard constructor
    PatVeloTT( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;    ///< Algorithm initialization
    StatusCode execute   () override;    ///< Algorithm execution

  private:
    bool acceptTrack(const LHCb::Track& track) const;
    void removeUsedTracks( std::vector<LHCb::Track*>& veloTracks);
    bool matchingTracks(const LHCb::Track& vttcand, const LHCb::Track& trackused) const;

  private:

    ITrackSelector* m_trackSelector = nullptr; // tool to accept a track

    Tf::TTStationHitManager<PatTTHit> *      m_ttHitManager = nullptr;

    std::string m_inputTracksName;    ///< input container name
    std::string m_outputTracksName;   ///< output container name
    std::string m_trackSelectorName; ///< name of the tool to accept a track
    bool m_removeUsedTracks;
    /// The fitter tool
    std::string m_fitterName;
    ITrackFitter* m_tracksFitter;
    ITracksFromTrack* m_veloTTTool = nullptr;
    std::vector< std::string > m_inputUsedTracksNames;
    double m_maxChi2;
    bool m_fitTracks; 
    ISequencerTimerTool* m_timerTool = nullptr;
    int  m_veloTTTime = 0;
    bool m_doTiming;
    bool m_AddMomentumEstimate;
  };

#endif // PATVELOTT_H
