/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INCLUDE_TF_PATVELORHITMANAGER_H
#define INCLUDE_TF_PATVELORHITMANAGER_H 1

#include <bitset>

#include "PatVeloHit.h"
#include "TfKernel/ExtendedVeloRHitManager.h"

// for CCE scan
#include "Kernel/IVeloCCEConfigTool.h"

namespace Tf {

  static const InterfaceID IID_PatVeloRHitManager( "PatVeloRHitManager", 1, 0 );

  /** @class PatVeloRHitManager PatVeloRHitManager.h
   * Mananges specialised r hits for VELO tracking. 
   *
   * @author Kurt Rinnert <kurt.rinnert@cern.ch>
   * @date   2007-08-08
   */
  class PatVeloRHitManager : public Tf::ExtendedVeloRHitManager<PatVeloRHit> {

    public:

      /// Retrieve interface ID
      static const InterfaceID& interfaceID() { return IID_PatVeloRHitManager; }

      /// Standard Constructor
      PatVeloRHitManager(const std::string& type,
          const std::string& name,
          const IInterface* parent);

      StatusCode initialize() override; ///< Tool initialization

      void prepareHits() override; ///< Prepare all hits

      void prepareHits(Station* station);        ///< Prepare hits for one station only, implementation

      void resetUsedFlagOfHits(); ///< Reset all used flags to unused

    private:

      /// Max number of clusters in R zone before region killed as too hot
      unsigned int m_maxRClustersZone;
      /// Use CCE scan pattern recognition
      bool m_CCEscan;
      /// Comma separated list of killed sensors to override CondDB
      std::string m_killSensorList;
      /// CCE scan sensor list to kill, set from CondDB
      std::vector<int> m_killSensorListVec;
      /// CCE sensor tool
      IVeloCCEConfigTool* m_cceTool;
  };
}
#endif // INCLUDE_TF_PATVELORHITMANAGER_H
