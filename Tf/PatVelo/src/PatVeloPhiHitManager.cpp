/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/IIncidentSvc.h"

#include "PatVeloPhiHitManager.h"

#include "Event/ODIN.h"
#include "Kernel/IVeloCCEConfigTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PatVeloPhiHitManager
//
// 2007-08-08 : Kurt Rinnert <kurt.rinnert@cern.ch>
//-----------------------------------------------------------------------------

namespace {
    template <typename Inserter>
    Inserter append_string_of_ints(const std::string& s, Inserter i, char sep = ',') {
        std::stringstream ss(s);
        while (ss.good()) {
            std::string substr;
            getline(ss,substr,sep);
            *i++ = std::stoi(substr);
        }
        return i;
    }

}

namespace Tf {
  DECLARE_COMPONENT( PatVeloPhiHitManager )


  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  PatVeloPhiHitManager::PatVeloPhiHitManager(const std::string& type,
      const std::string& name,
      const IInterface* parent)
    : Tf::ExtendedVeloPhiHitManager<PatVeloPhiHit>(type, name, parent)
    {
      declareInterface<PatVeloPhiHitManager>(this);
      // currently disarmed : only 682 strips
      declareProperty("MaxClustersPhiInner", m_maxPhiInner = 9999) ;
      // currently disarmed : only 1780 strips
      declareProperty("MaxClustersPhiOuter", m_maxPhiOuter = 9999) ;
      declareProperty("CCEscan", m_CCEscan = false) ;
      declareProperty("KillSensorList", m_killSensorList = "");
    }

  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode PatVeloPhiHitManager::initialize()
  {
    StatusCode sc = Tf::ExtendedVeloPhiHitManager<PatVeloPhiHit>::initialize(); // must be executed first
    if (sc.isFailure()) return sc;  // error printed already by GaudiTool

    debug() << "==> Initialize" << endmsg;

    if( m_maxPhiInner < 512 ){
      info() << "Kill hits in Inner Phi zone with more than " <<  m_maxPhiInner
        << " clusters" << endmsg;
    }
    if( m_maxPhiOuter < 512 ){
      info() << "Kill hits in Outer Phi zone with more than " <<  m_maxPhiOuter
        << " clusters" << endmsg;
    }

    if ( m_CCEscan ) {
      debug() << "Setting up CCE tool" << endmsg;
      m_cceTool = tool<IVeloCCEConfigTool>("VeloCCEConfigTool");
    }

    return StatusCode::SUCCESS;
  }


  //=============================================================================
  // Preparation of measurements
  //=============================================================================
  void PatVeloPhiHitManager::prepareHits()
  {
    if ( m_CCEscan ){
       int cceStep = -1;
       // Set the killed sensor list:
       LHCb::ODIN *odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
       if ( odin ){
          cceStep = odin->calibrationStep();
          if ( cceStep>=0 && m_killSensorList.empty()) {
             m_cceTool->findKilledSensors(cceStep,m_killSensorListVec);
          } else {
            append_string_of_ints( m_killSensorList, std::back_inserter(m_killSensorListVec));
          }
       } else {
          fatal() << "There is no ODIN bank" << endmsg;
       }
    }

    for (unsigned int half=0; half<m_nHalfs; ++half) { // loop over velo halfs

      for (DefaultStation* defaultStation : m_defaultHitManager->stationsHalf(half) ) {
        unsigned int defaultStationNumber = defaultStation->stationNumber();
        Station* station =  m_stationsHalf[half][defaultStationNumber];
        if (station->hitsPrepared()) continue;
        station->clear();

        for (unsigned int zone=0; zone<m_nZones; ++zone) { // loop over inner/outer zones
          auto hits = defaultStation->hits(zone);
          bool markUsed = false;
          if ( ( zone == 0 && hits.size() > m_maxPhiInner ) ||
              ( zone == 1 && hits.size() > m_maxPhiOuter ) ) {
            Warning("Very hot VELO Phi zone: ignoring clusters",
                StatusCode::SUCCESS, 0 ).ignore();
            markUsed = true;
          }

          if ( m_CCEscan ){
             if( std::binary_search(m_killSensorListVec.begin(),m_killSensorListVec.end(),defaultStation->sensorNumber()) ){
                markUsed = true;
             }
          }

          m_data[half][defaultStationNumber][zone].reserve( hits.size() );
          for (const auto& hit : hits ) {
            if ( hit->ignore() ) { continue; } // don't use hit if ignore flag is set
            if ( markUsed ) hit->setUsed(true); // hot region hit
            m_data[half][defaultStationNumber][zone].emplace_back(hit);
            station->zone(zone).push_back(&(m_data[half][defaultStationNumber][zone].back()));
          }
        }
        station->setHitsPrepared(true);
      }
    }
    m_dataValid = true;
  }

  //=============================================================================
  // Preparation of measurements for one station, actual implementation
  //=============================================================================
  void PatVeloPhiHitManager::prepareHits(Station* station)
  {
    if ( m_CCEscan ){
       int cceStep = -1;
       LHCb::ODIN *odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
       if ( odin ){
          cceStep = odin->calibrationStep();
          if ( cceStep>=0 && m_killSensorList.empty()) {
            m_cceTool->findKilledSensors(cceStep,m_killSensorListVec);
          } else {
            append_string_of_ints( m_killSensorList, std::back_inserter(m_killSensorListVec));
          }
       } else {
          fatal() << "There is no ODIN bank" << endmsg;
       }
    }

    DefaultStation*    defaultStation = m_defaultHitManager->stationNoPrep(station->sensorNumber());
    if (!defaultStation->hitsPrepared()) {
      m_defaultHitManager->prepareHits(defaultStation);
    }
    unsigned int defaultStationNumber = defaultStation->stationNumber();
    unsigned int half = station->veloHalf();

    for (unsigned int zone=0; zone<m_nZones; ++zone) { // loop over inner/outer zones
      Tf::VeloPhiHitRange hits = defaultStation->hits(zone);
      bool markUsed = false;
      if ( ( zone == 0 && hits.size() > m_maxPhiInner ) ||
          ( zone == 1 && hits.size() > m_maxPhiOuter ) ) {
        Warning("Very hot VELO Phi zone: ignoring clusters",
            StatusCode::SUCCESS, 0 ).ignore();
      }

      if ( m_CCEscan ){
         if( std::binary_search(m_killSensorListVec.begin(),m_killSensorListVec.end(),defaultStation->sensorNumber()) ){
            markUsed = true;
         }
      }

      m_data[half][defaultStationNumber][zone].reserve(hits.size());
      for (const auto& hit : hits) {
        if ( hit->ignore() ) { continue; } // don't use hit if ignore flag is set
        if ( markUsed ) hit->setUsed(true); // hot region hit
        m_data[half][defaultStationNumber][zone].emplace_back(hit);
        station->zone(zone).push_back(&(m_data[half][defaultStationNumber][zone].back()));
      }
    }
    station->setHitsPrepared(true);
  }

  //=============================================================================
  // Reset all used flags to unused
  //=============================================================================
  void PatVeloPhiHitManager::resetUsedFlagOfHits()
  {
    for (unsigned int half=0; half<m_nHalfs; ++half) { // loop over velo halfs
      for ( auto& station : m_defaultHitManager->stationsHalf(half) ) {
        for (unsigned int zone=0; zone<m_nZones; ++zone) { // loop over inner/outer zones
          auto hits = station->hits(zone);
          std::for_each( hits.begin(),hits.end(), [](Tf::VeloPhiHit* h) { h->resetUsedFlag(); });
        }
      }
    }
  }
}






