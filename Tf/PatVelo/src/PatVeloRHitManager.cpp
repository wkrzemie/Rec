/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "PatVeloRHitManager.h"

#include "Event/ODIN.h"
#include "Kernel/IVeloCCEConfigTool.h"

namespace Tf {

  //-----------------------------------------------------------------------------
  // Implementation file for class : PatVeloRHitManager
  //
  // 2007-08-08 : Kurt Rinnert <kurt.rinnert@cern.ch>
  //-----------------------------------------------------------------------------


  DECLARE_COMPONENT( PatVeloRHitManager )


  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  PatVeloRHitManager::PatVeloRHitManager(const std::string& type,
      const std::string& name,
      const IInterface* parent)
    : Tf::ExtendedVeloRHitManager<PatVeloRHit>(type, name, parent)
    {
      declareInterface<PatVeloRHitManager>(this);
      // currently disarmed: only 512 strips in a zone...
      declareProperty("MaxClustersRZone", m_maxRClustersZone = 999) ;
      declareProperty("CCEscan", m_CCEscan = false) ;
      declareProperty("KillSensorList", m_killSensorList = "");
    }

  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode PatVeloRHitManager::initialize()
  {
    StatusCode sc = Tf::ExtendedVeloRHitManager<PatVeloRHit>::initialize(); // must be executed first
    if (sc.isFailure()) return sc;  // error printed already by GaudiTool

    debug() << "==> Initialize" << endmsg;

    if( m_maxRClustersZone < 512 ) {
      info() << "Kill hits in R zone with more than " << m_maxRClustersZone
        << " clusters" << endmsg;
    }

    if ( m_CCEscan ) {
      debug() << "Setting up CCE tool" << endmsg;
      m_cceTool = tool<IVeloCCEConfigTool>("VeloCCEConfigTool");
    }

    return StatusCode::SUCCESS;
  }


  //=============================================================================
  // Reset all used flags to unused
  //=============================================================================
  void PatVeloRHitManager::resetUsedFlagOfHits()
  {
    for (unsigned int half=0; half<m_nHalfs; ++half) { // loop over velo halfs
      for ( auto& s : m_defaultHitManager->stationsHalf(half) ) {
        for (unsigned int zone=0; zone<m_nZones; ++zone) { // loop over inner/outer zones
          for ( auto& hit : s->hits(zone) ) {  hit->resetUsedFlag(); }// import all hits
        }
      }
    }
  }

  //=============================================================================
  // Preparation of measurements
  //=============================================================================
  void PatVeloRHitManager::prepareHits()
  {
    if ( m_CCEscan ){
       int cceStep = -1;
       // Set the killed sensor list:
       LHCb::ODIN *odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
       if ( odin ){
          cceStep = odin->calibrationStep();
          if ( cceStep>=0 && m_killSensorList.empty())
	  {
	    m_cceTool->findKilledSensors(cceStep,m_killSensorListVec);
          }
	  else
          {
            std::stringstream ss(m_killSensorList);
            while( ss.good() )
            {
              std::string substr;
              getline(ss, substr, ',');
              m_killSensorListVec.push_back(std::stoi(substr));
            }
          }
       }
       else {
          fatal() << "There is no ODIN bank" << endmsg;
       }
    }

    for (unsigned int half=0; half<m_nHalfs; ++half) { // loop over velo halfs
      for ( DefaultStation* defaultStation : m_defaultHitManager->stationsHalf(half)) {
        unsigned int defaultStationNumber = defaultStation->stationNumber();
        Station* station =  m_stationsHalf[half][defaultStationNumber];
        if (station->hitsPrepared()) continue;
        station->clear();

        for (unsigned int zone=0; zone<m_nZones; ++zone) { // loop over r sectors
          auto hits = defaultStation->hits(zone);
          bool markUsed = (hits.size() > m_maxRClustersZone);
          if ( markUsed ){
            Warning("Very hot VELO R zone: ignoring clusters",
                StatusCode::SUCCESS, 0 ).ignore();
          }

          if ( m_CCEscan ){
             if( std::binary_search(m_killSensorListVec.begin(),m_killSensorListVec.end(),defaultStation->sensorNumber()) ){
                markUsed = true;
             }
          }

          auto &d = m_data[half][defaultStationNumber][zone];
          d.reserve( hits.size() );
          for ( const auto& hit : hits )  { // import all hits
            if ( hit->ignore() ) { continue; } // don't use hit if ignore flag is set
            if ( markUsed ) hit->setUsed(true); // hot region hit
            d.emplace_back(hit);
            station->zone(zone).push_back(&d.back());
          }
        }
        station->setHitsPrepared(true);
      }
    }
    m_dataValid = true;
  }

  //=============================================================================
  // Preparation of measurements for one station, actual implementation
  //=============================================================================
  void PatVeloRHitManager::prepareHits(Station* station)
  {
    if ( m_CCEscan ){
       int cceStep = -1;
       // Set the killed sensor list:
       LHCb::ODIN *odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
       if ( odin ){
          cceStep = odin->calibrationStep();
          if ( cceStep>=0 && m_killSensorList.empty())
	  {
             m_cceTool->findKilledSensors(cceStep,m_killSensorListVec);
          }
	  else
          {
            std::stringstream ss(m_killSensorList);
            while( ss.good() )
            {
              std::string substr;
              getline(ss, substr, ',');
              m_killSensorListVec.push_back(std::stoi(substr));
            }
          }
       }
       else {
          fatal() << "There is no ODIN bank" << endmsg;
       }
    }

    DefaultStation*    defaultStation = m_defaultHitManager->stationNoPrep(station->sensorNumber());
    if (!defaultStation->hitsPrepared()) {
      m_defaultHitManager->prepareHits(defaultStation);
    }
    unsigned int defaultStationNumber = defaultStation->stationNumber();
    unsigned int half = station->veloHalf();

    for (unsigned int zone=0; zone<m_nZones; ++zone) { // loop over r sectors
      auto hits = defaultStation->hits(zone);
      bool markUsed = false;
      if ( hits.size() > m_maxRClustersZone ){
        Warning("Very hot VELO R zone: ignoring clusters",
            StatusCode::SUCCESS, 0 ).ignore();
        markUsed = true;
      }

      if ( m_CCEscan ){
         if( std::binary_search(m_killSensorListVec.begin(),m_killSensorListVec.end(),defaultStation->sensorNumber()) ){
            markUsed = true;
         }
      }


      m_data[half][defaultStationNumber][zone].reserve(hits.size());
      for (auto& hit : hits ) { // import all hits
        if ( hit->ignore() ) { continue; } // don't use hit if ignore flag is set
        if ( markUsed ) hit->setUsed(true); // hot region hit
        m_data[half][defaultStationNumber][zone].push_back(PatVeloRHit(hit));
        station->zone(zone).push_back(&(m_data[half][defaultStationNumber][zone].back()));
      }
    }
    station->setHitsPrepared(true);
  }
}
