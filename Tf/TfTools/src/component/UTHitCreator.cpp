//-----------------------------------------------------------------------------
/** @file UTHitCreator.cpp
 *
 *  Implementation file for class : Tf::UTHitCreator
 *
 *  @author A. Beiter (based on code by S. Hansmann-Menzemer, 
 *                     W. Hulsbergen, C. Jones, K. Rinnert)
 *  @date   2018-09-04
 */
//-----------------------------------------------------------------------------
#include <functional>
#include "GaudiKernel/IIncidentSvc.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTDataFunctor.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "Event/UTLiteCluster.h"

#include "TfKernel/RegionID.h"
#include "TfKernel/IUTHitCreator.h"

#include "UTHitCreator.h"
#include "HitCreatorGeom.h"

namespace Tf
{
  namespace HitCreatorGeom
  {

    class UTModule final : public Envelope<DeUTSector>
    {
    public:
      // typedefs, basically for template traits
      typedef DeUTDetector DetectorType ;
      typedef Tf::UTHit HitType ;
      typedef Tf::UTHitRange HitRangeType ;
      typedef std::vector<UTHit> HitContainer ;
      // constructor
      UTModule(const DeUTSector& detelement)  :  Envelope<DeUTSector>(detelement), m_det(&detelement), m_isloaded(false) {}
      void clearEvent() { m_ownedhits.clear() ; m_isloaded = false ;}
      void resetUsedFlagOfHits() { std::for_each( std::begin(m_ownedhits), std::end(m_ownedhits),
                                                  [](auto& hit) { hit.resetUsedFlag(); }); }
      const UTHitRange& hits() const { return m_hitrange ; }
      const DeUTSector& detelement() const { return *m_det ; }
      void setRange( UTHits::const_iterator begin, UTHits::const_iterator end ) { m_hitrange = UTHitRange(begin,end) ; }
      HitContainer& ownedhits() { return m_ownedhits ; }
    private:
      const DeUTSector* m_det ;
      HitContainer m_ownedhits ; // these are the hits which the module owns
      UTHitRange m_hitrange ;    // these are pointers in the global list of hits, used for navigation
      bool m_isloaded ;
    } ;

    class UTDetector ;

    typedef RegionOfModules<UTModule, UTDetector> UTRegionImp ;

    class UTDetector final : public Detector<UTRegionImp>
    {
    public:
      template <typename ClusterCallBack>
      UTDetector(const DeUTDetector& , ClusterCallBack&& );
      ~UTDetector() { clearEvent(); } // should ~Detector<UTRegionImp> not call its own clearEvent?
      void loadHits() ;
      void clearEvent() {
        m_clusters = nullptr ;
        Detector<UTRegionImp>::clearEvent() ;
      }
      const LHCb::UTLiteCluster::FastContainer* clusters() {
        if (UNLIKELY(!m_clusters)) m_clusters = m_callback();
	    return m_clusters;
      }
    private:
      std::function<const LHCb::UTLiteCluster::FastContainer*()> m_callback ;
      const LHCb::UTLiteCluster::FastContainer* m_clusters ;
    } ;

    template <typename ClusterFun>
    UTDetector::UTDetector(const DeUTDetector& utdetector, ClusterFun&& getClusters )
    : m_callback{ std::forward<ClusterFun>(getClusters) }
    , m_clusters{nullptr}
    {
      // copy the entire hierarchy
      for( const auto& sector : utdetector.sectors() ) {
        RegionID regionid{ LHCb::UTChannelID(sector->elementID()) } ;
        UTRegionImp* aregion = const_cast<UTRegionImp*>(region(regionid)) ;
        if(!aregion) aregion = insert( new UTRegionImp(regionid,*this) );
        aregion->insert( sector->elementID().sector(), new HitCreatorGeom::UTModule(*sector) ) ;
      }
    }

    struct compareHitX {
      bool operator()(const Tf::UTHit* lhs, const Tf::UTHit* rhs) const { return lhs->xT() < rhs->xT() ; }
    };

    void UTDetector::loadHits()
    {
      if(UNLIKELY(isLoaded())) return; // all (?) callers have checked this already...

      // retrieve clusters
      const auto* liteCont = clusters() ;

      // create hits clusters. don't assume anything about order
      HitCreatorGeom::UTModule* cachedSector{nullptr} ;
      unsigned int cachedSectorID(0xFFFFFFFF) ;

      for( const auto& clus : *liteCont ) {
        const LHCb::UTChannelID channelid = clus.channelID() ;
        // find the sector ;
        if( !cachedSector || channelid.uniqueSector() != cachedSectorID ) {
          UTRegionImp* aregion = region( RegionID{ channelid } ) ;
          cachedSector = aregion->modules()[channelid.sector()] ;
        }
        cachedSector->ownedhits().emplace_back( cachedSector->detelement(), clus ) ;
      }

      // now set up pointers for hit navigation. we don't want any hits
      // to change place. hits should be sorted now by (station, layer,
      // region, x)
      auto& thehits = hits() ;
      thehits.clear() ; // should not be necessary
      thehits.reserve(liteCont->size()) ; // to make sure things don't change place anymore.
      for(const auto& reg : regions() ) for( const auto& module : reg->modules() ) {
          auto begin = std::end(thehits) ;
          for( auto& hit : module->ownedhits() ) thehits.push_back( &hit ) ;
          auto end = std::end(thehits);
          std::sort( begin, end, compareHitX()) ;
          // now set the pointers from the module
          module->setRange( begin, end ) ;
      }
      setIsLoaded(true) ;

    }
  }

  template<class Trait>
  UTHitCreator<Trait>::UTHitCreator(const std::string& type,
				    const std::string& name,
				    const IInterface* parent)
  : GaudiTool(type, name, parent)
  , m_inputClusters{ Trait::defaultClusterLocation(), Gaudi::DataHandle::Reader, this }
  {
    // ... add data handle ... instead
    declareInterface<typename Trait::IUTHC>(this);
    declareProperty("DetectorLocation", m_detectorLocation=Trait::defaultDetectorLocation()) ;
    declareProperty("ClusterLocation", m_inputClusters);
  }

  template<class Trait>
  StatusCode UTHitCreator<Trait>::initialize()
  {

    const StatusCode sc = GaudiTool::initialize();
    if (sc.isFailure()) return Error("Failed to initialize",sc);

    // get geometry and copy the hierarchy to navigate hits
    m_utdet = getDet<DeUTDetector>(m_detectorLocation);
    // we may need to register to the conditions of all modules instead
    updMgrSvc()->registerCondition( this,  const_cast<IGeometryInfo*>(m_utdet->geometry()),
                                    &UTHitCreator<Trait>::updateGeometry );

    // reset pointer to list of clusters at beginevent
    incSvc()->addListener(this, IncidentType::BeginEvent);

    return sc;
  }

  template<class Trait>
  StatusCode UTHitCreator<Trait>::finalize()
  {
    m_detectordata.reset(nullptr);
    return GaudiTool::finalize();
  }

  template<class Trait>
  StatusCode UTHitCreator<Trait>::updateGeometry()
  {
    if(msgLevel(MSG::DEBUG)) debug() << "In UTHitCreator::updateGeometry()" << endmsg ;
    m_detectordata.reset(new HitCreatorGeom::UTDetector(*m_utdet, [=](){return this->m_inputClusters.get(); }));
    return StatusCode::SUCCESS ;
  }

  template<class Trait>
  void UTHitCreator<Trait>::handle ( const Incident& incident )
  {
    if ( IncidentType::BeginEvent == incident.type() && m_detectordata) m_detectordata->clearEvent() ;
  }

  template<class Trait>
  Tf::UTHitRange UTHitCreator<Trait>::hits(const typename Trait::StationID iStation,
                                           const typename Trait::LayerID iLayer) const
  {
    if( UNLIKELY(!m_detectordata->isLoaded()) ) m_detectordata->loadHits() ;
    return m_detectordata->stations()[iStation]->layers[iLayer]->hits() ;
  }

  template<class Trait>
  Tf::UTHitRange UTHitCreator<Trait>::hits(const typename Trait::StationID iStation,
                                           const typename Trait::LayerID iLayer,
                                           const typename Trait::RegionID iRegion) const
  {
    if( UNLIKELY(!m_detectordata->isLoaded()) ) m_detectordata->loadHits() ;
    return m_detectordata->stations()[iStation]->layers[iLayer]->regions[iRegion]->hits() ;
  }

  template<class Trait>
  Tf::UTHitRange UTHitCreator<Trait>::hits() const
  {
    if( UNLIKELY(!m_detectordata->isLoaded()) ) m_detectordata->loadHits() ;
    return m_detectordata->hits() ;
  }

  template<class Trait>
  Tf::UTHitRange UTHitCreator<Trait>::hitsLocalXRange( const typename Trait::StationID iStation,
						       const typename Trait::LayerID iLayer,
						       const typename Trait::RegionID iRegion,
						       const double xmin,
						       const double xmax ) const
  {
    if( UNLIKELY(!m_detectordata->isLoaded()) ) m_detectordata->loadHits() ;
    const auto* region = m_detectordata->region(iStation,iLayer,iRegion) ;
    return region->hitsLocalXRange(xmin,xmax) ;
  }

  template<class Trait>
  const typename UTHitCreator<Trait>::UTRegion*
  UTHitCreator<Trait>::region( const typename Trait::StationID iStation,
			       const typename Trait::LayerID iLayer,
			       const typename Trait::RegionID  iRegion ) const
  {
    return m_detectordata->region(iStation,iLayer,iRegion) ;
  }

  template<class Trait>
  Tf::UTHit UTHitCreator<Trait>::hit(LHCb::UTChannelID utid) const
  {
    const auto* clusters = m_detectordata->clusters() ;
    auto iclus =  clusters->find< LHCb::UTLiteCluster::findPolicy >( utid )  ;
    if( iclus == clusters->end() )
      throw GaudiException("UTHitCreator::hit cannot find cluster", "UTHitCreatorException" , StatusCode::FAILURE ) ;
    const DeUTSector* sector = m_utdet->findSector( utid ) ;
    return { *sector, *iclus } ;
  }

  //====================================================================================
  // template instantiations
  //====================================================================================

  struct UT {
    static std::string defaultDetectorLocation() { return DeUTDetLocation::location() ; }
    static std::string defaultClusterLocation() { return LHCb::UTLiteClusterLocation::UTClusters ; }
    typedef IUTHitCreator IUTHC ;
    typedef UTStationID StationID ;
    typedef UTLayerID   LayerID ;
    typedef UTRegionID RegionID ;
  } ;

  typedef UTHitCreator<UT> UTHC ;
  DECLARE_COMPONENT( UTHC )


  // ResetUsed flag for all OT hits
  template<class Trait>
  void UTHitCreator<Trait>::resetUsedFlagOfHits() {
     m_detectordata->resetUsedFlagOfHits();
  }
}
