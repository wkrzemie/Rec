/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file STHitCreator.h
 *
 *  Header file for class : Tf::STHitCreator
 *
 *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
 *  @date   2007-06-01
 */
//-----------------------------------------------------------------------------

#ifndef TFKERNEL_STHitCreator_H
#define TFKERNEL_STHitCreator_H 1

#include <string>

#include "GaudiAlg/GaudiTool.h"

#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "TfKernel/STHit.h"

namespace Tf

{

  // forward declaration of the class that is holding the hit data
  namespace HitCreatorGeom {
    class STDetector ;
  }

  /** @class STHitCreator STHitCreator.h
   *
   *  Implementation of Tf::IITHitCreator and Tf::ITTHitCreator
   *
   *  Creates the Tf::STHit objects for the Tf tracking framework
   *
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-06-01
   */

  template<class Trait>
  class STHitCreator final  : public GaudiTool,
                      virtual public Trait::ISTHitCreator,
                      virtual public IIncidentListener

  {

  public:
    /// The region type for TT hit (could get this from the Trait::ISTHitCreator)
    typedef Tf::Region<STHit> STRegion ;

    /// constructor
    STHitCreator(const std::string& type,
                 const std::string& name,
                 const IInterface* parent);

    /// initialize
    StatusCode initialize() override;

    /// finalize
    StatusCode finalize() override;
 private:
    /// incident service handle
    void handle( const Incident& incident ) override;
 public:
    /// update manager handle
    StatusCode updateGeometry() ;

    // RestUsed flag for all OT hits
    void resetUsedFlagOfHits() override;

    // Load all the IT hits
    STHitRange hits() const override;

    // Load the ST hits for a given region of interest
    STHitRange hits(const typename Trait::StationID iStation,
                    const typename Trait::LayerID iLayer) const override;

    // Load the ST hits for a given region of interest
    STHitRange hits(const typename Trait::StationID iStation,
                    const typename Trait::LayerID iLayer,
                    const typename Trait::RegionID iRegion) const override;


    // Load the ST hits for a given region of interest
    STHitRange hitsLocalXRange(const typename Trait::StationID iStation,
		       const typename Trait::LayerID iLayer,
		       const typename Trait::RegionID iRegion,
		       const double xmin,
		       const double xmax) const override;

    // Retrieve the STRegion for a certain region ID. The region
    const STRegion* region(const typename Trait::StationID iStation,
                           const typename Trait::LayerID iLayer,
                           const typename Trait::RegionID  iRegion) const override;

    // create a single STHit from an STChannelID
    Tf::STHit hit(LHCb::STChannelID stid) const override;

  private:

    /// Load the hits
    void loadHits() const ;

    const DeSTDetector* m_stdet = nullptr;
    mutable std::unique_ptr<HitCreatorGeom::STDetector> m_detectordata ;
    AnyDataHandle<LHCb::STLiteCluster::FastContainer> m_inputClusters;
    std::string m_detectorLocation;
  };

}

#endif // TFKERNEL_STHitCreator_H

