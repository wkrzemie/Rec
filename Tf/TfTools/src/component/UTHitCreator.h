//-----------------------------------------------------------------------------
/** @file UTHitCreator.h
 *
 *  Header file for class : Tf::UTHitCreator
 *
 *  @author A. Beiter (based on code by S. Hansmann-Menzemer,
 *                     W. Hulsbergen, C. Jones, K. Rinnert)
 *  @date   2018-09-04
 */
//-----------------------------------------------------------------------------

#ifndef TFKERNEL_UTHitCreator_H
#define TFKERNEL_UTHitCreator_H 1

#include <string>

#include "GaudiAlg/GaudiTool.h"

#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "TfKernel/UTHit.h"

namespace Tf

{

  // forward declaration of the class that is holding the hit data
  namespace HitCreatorGeom {
    class UTDetector ;
  }

  /** @class UTHitCreator UTHitCreator.h
   *
   *  Creates the Tf::UTHit objects for the Tf tracking framework
   *
   *  @author A. Beiter (based on code by S. Hansmann-Menzemer, 
   *                     W. Hulsbergen, C. Jones, K. Rinnert)
   *  @date   2018-09-04
   */

  template<class Trait>
  class UTHitCreator final  : public GaudiTool,
                      virtual public Trait::IUTHC,
                      virtual public IIncidentListener

  {

  public:
    /// The region type for UT hit (could get this from the Trait::IUTHitCreator)
    typedef Tf::Region<UTHit> UTRegion ;

    /// constructor
    UTHitCreator(const std::string& type,
                 const std::string& name,
                 const IInterface* parent);

    /// initialize
    StatusCode initialize() override;

    /// finalize
    StatusCode finalize() override;
 private:
    /// incident service handle
    void handle( const Incident& incident ) override;
 public:
    /// update manager handle
    StatusCode updateGeometry() ;

    // RestUsed flag for all OT hits
    void resetUsedFlagOfHits() override;

    // Load all the UT hits
    UTHitRange hits() const override;

    // Load the UT hits for a given region of interest
    UTHitRange hits(const typename Trait::StationID iStation,
                    const typename Trait::LayerID iLayer) const override;

    // Load the UT hits for a given region of interest
    UTHitRange hits(const typename Trait::StationID iStation,
                    const typename Trait::LayerID iLayer,
                    const typename Trait::RegionID iRegion) const override;


    // Load the UT hits for a given region of interest
    UTHitRange hitsLocalXRange(const typename Trait::StationID iStation,
		       const typename Trait::LayerID iLayer,
		       const typename Trait::RegionID iRegion,
		       const double xmin,
		       const double xmax) const override;

    // Retrieve the UTRegion for a certain region ID. The region
    const UTRegion* region(const typename Trait::StationID iStation,
                           const typename Trait::LayerID iLayer,
                           const typename Trait::RegionID  iRegion) const override;

    // create a single UTHit from an UTChannelID
    Tf::UTHit hit(LHCb::UTChannelID utid) const override;

  private:

    /// Load the hits
    void loadHits() const ;

    const DeUTDetector* m_utdet = nullptr;
    mutable std::unique_ptr<HitCreatorGeom::UTDetector> m_detectordata ;
    AnyDataHandle<LHCb::UTLiteCluster::FastContainer> m_inputClusters;
    std::string m_detectorLocation;
  };

}

#endif // TFKERNEL_UTHitCreator_H

