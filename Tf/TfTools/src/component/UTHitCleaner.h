//-----------------------------------------------------------------------------
/** @file UTHitCleaner.h
 *
 *  Header file for class : Tf::UTHitCleaner
 *
 *  @author Andy Beiter (based on code by Chris Jones)
 *  @date   2018-09-04
 */
//-----------------------------------------------------------------------------

#ifndef TFTOOLS_UTHitCleaner
#define TFTOOLS_UTHitCleaner 1

// UTL
#include <iterator>
#include <functional>
#include <map>

// From gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiAlg/GaudiTool.h"

// Tf
#include "TfKernel/IUTHitCleaner.h"

namespace Tf
{

  //-----------------------------------------------------------------------------
  /** @class UTHitCleaner UTHitCleaner.h
   *
   *  UTHit cleaner tool
   *
   *  @author A. Beiter (based on code by S. Hansmann-Menzemer, W. Houlsbergen, 
   *                     C. Jones, K. Rinnert)
   *
   *  @date   2018-09-04
   *
   *  @todo Is it possible to have a common interface for OT and UT (other?) cleaning tools
   **/
  //-----------------------------------------------------------------------------
  class UTHitCleaner : public extends<GaudiTool, IUTHitCleaner>
  {

  private:

    /// Max beetle occupancy job option
    unsigned int m_maxBeetleOcc;

  public:

    /// Standard Constructor
    UTHitCleaner( const std::string& type,
                  const std::string& name,
                  const IInterface* parent );

    /// Tool initialization
    StatusCode initialize ( ) override;

    // Clean the given range of hits
    UTHits cleanHits( const UTHits::const_iterator begin,
                      const UTHits::const_iterator end ) const override;

  private:

    // Clean out hot beetles
    UTHits removeHotBeetles( const UTHits::const_iterator begin,
                             const UTHits::const_iterator end ) const;

  }; // UTHitCleaner

} // namespace

#endif

