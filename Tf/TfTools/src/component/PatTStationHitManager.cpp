/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//From TfKernel
#include "TfKernel/OTHit.h"
#include "TfKernel/RecoFuncs.h"

// local
#include "PatKernel/PatTStationHitManager.h"

DECLARE_COMPONENT( PatTStationHitManager )

void PatTStationHitManager::prepareHits() const
{
  if ( allHitsPrepared() ) return;
  //explicitly trigger decoding -- this will implicitly set allHitsPrepared!
  Tf::TStationHitManager<PatForwardHit>::prepareHits();
  // and now sort, and flag next/prev OT hit
  for (Tf::TStationID sta=0; sta<maxStations(); ++sta) {
    for (Tf::TLayerID lay=0; lay<maxLayers(); ++lay) {
      for (Tf::OTRegionID reg=0; reg<maxOTRegions(); ++reg) {
        PatFwdHit* prevHit = nullptr;
        double lastCoord = -10000000.;
        for ( auto& hit : hits(sta,lay,reg) ) {
          if ( hit->hit()->xAtYEq0() - lastCoord < 3. ) {
            hit->setHasPrevious( true );
            prevHit->setHasNext( true ); // For the first iteration, when prevHit == nullptr,
                                         // the intial value of lastCoord is such that this
                                         // branch is never taken, and thus there is no
                                         // nullptr dereference here.
          }
          lastCoord = hit->hit()->xAtYEq0();
          prevHit = hit;
        }
      }
    }
  }
}
