// Include files

// local
#include "UTHitCleaner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UTHitCleaner
//
// 2018-09-04: Andy Beiter (based on code by Chris Jones)
//-----------------------------------------------------------------------------

using namespace Tf;

// Declaration of the Tool Factory
DECLARE_COMPONENT( UTHitCleaner )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UTHitCleaner::UTHitCleaner( const std::string& type,
                            const std::string& name,
                            const IInterface* parent )
  : base_class ( type , name , parent )
{
  declareInterface<IUTHitCleaner>(this);
  declareProperty( "MaxSectorOccupancy", m_maxBeetleOcc = 48 );
}


//=============================================================================
// initialize
//=============================================================================
StatusCode UTHitCleaner::initialize ( )
{
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  info() << "Max UT sector occupancy = " << m_maxBeetleOcc << endmsg;

  return sc;
}

UTHits UTHitCleaner::cleanHits( const UTHits::const_iterator begin,
                                const UTHits::const_iterator end ) const
{
  // clean out hot beetles
  auto output = removeHotBeetles( begin, end );
  // how many hits where cleaned
  if ( msgLevel(MSG::DEBUG) )
    debug() << "Selected " << output.size() << " out of "
            << std::distance(begin,end) << " UTHits" << endmsg;
  return output;
}

UTHits UTHitCleaner::removeHotBeetles( const UTHits::const_iterator begin,
                                       const UTHits::const_iterator end ) const
{
  UTHits output; output.reserve( std::distance(begin,end));
  auto currentend = begin ;
  while( currentend != end) {
    
    // select hits in this sector and add up total size
    auto currentbegin = currentend ;
    const DeUTSector& sector = (*currentbegin)->sector() ;

    unsigned int occ(0) ;
    do { 
      occ += (*currentend)->cluster().pseudoSize();
      ++currentend ; 
    } while(currentend != end && &((*currentend)->sector()) == &sector ) ;
    
    // add to output container if not too hot
    if ( msgLevel(MSG::VERBOSE) )
    {
      verbose() << "Sector " << sector.id() << " has occupancy " << occ << endmsg;
    }
    if ( occ <= m_maxBeetleOcc )
    {
      output.insert( output.end(), currentbegin, currentend );
    }
    else if ( msgLevel(MSG::DEBUG) )
    {
      debug() << "DeUTSector " << sector.id()
              << " suppressed due to high occupancy = " << occ 
              << " > " << m_maxBeetleOcc << " maximum"
              << endmsg;
    }
  }
  return output;
}

//=============================================================================
