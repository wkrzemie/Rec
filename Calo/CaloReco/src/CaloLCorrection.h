/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALORECO_CALOLCORRECTION_H
#define CALORECO_CALOLCORRECTION_H 1

// from STL
#include <string>
#include <cmath>

// Calo
#include "CaloInterfaces/ICaloHypoTool.h"
#include "CaloCorrectionBase.h"
#include "CaloDet/DeCalorimeter.h"
#include "GaudiKernel/Counters.h"

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// Event
#include "Event/CaloHypo.h"

/** @class CaloLCorrection CaloLCorrection.h
 *
 *
 *  @author Deschamps Olivier
 *  @date   2003-03-10
 *  revised 2010
 */

class CaloLCorrection :
  public virtual ICaloHypoTool,
  public CaloCorrectionBase
{
public:


  StatusCode process    ( LHCb::CaloHypo* hypo  ) const override;
  StatusCode operator() ( LHCb::CaloHypo* hypo  ) const override;

  StatusCode initialize() override;
  StatusCode finalize() override;

  /** Standard constructor
   *  @see GaudiTool
   *  @see  AlgTool
   *  @param type tool type (?)
   *  @param name tool name
   *  @param parent  tool parent
   */
  CaloLCorrection ( const std::string& type,
                    const std::string& name,
                    const IInterface*  parent);
private:
  using IncCounter =  Gaudi::Accumulators::Counter<>;
  using SCounter =  Gaudi::Accumulators::StatCounter<float>;

  mutable IncCounter m_counterSkipNegativeEnergyCorrection{this, "Skip negative energy correction"};
  mutable SCounter m_counterDeltaZ{this, "Delta(Z)"};

  static constexpr int k_numOfCaloAreas{4};
  mutable std::vector<SCounter> m_counterPerCaloAreaDeltaZ;
  mutable std::vector<SCounter> m_counterPerCaloAreaGamma;
  mutable std::vector<SCounter> m_counterPerCaloAreaDelta;
};
// ============================================================================
#endif // CALORECO_CALOLCORRECTION_H
