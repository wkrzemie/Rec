/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALORECO_CALOSCORRECTION_H
#define CALORECO_CALOSCORRECTION_H 1
// Include files
// Include files
#include <string>
#include "CaloInterfaces/ICaloHypoTool.h"
#include "CaloCorrectionBase.h"
#include "GaudiKernel/Counters.h"

/** @class CaloSCorrection CaloSCorrection.h
 *
 *
 *   @author Deschamps Olivier
 *  @date   2003-03-10
 */

class CaloSCorrection :
  public virtual ICaloHypoTool ,
  public              CaloCorrectionBase{
public:

  StatusCode process    ( LHCb::CaloHypo* hypo  ) const override;
  StatusCode operator() ( LHCb::CaloHypo* hypo  ) const override;

public:

  StatusCode initialize() override;
  StatusCode finalize() override;

  CaloSCorrection ( const std::string& type   ,
                    const std::string& name   ,
                    const IInterface*  parent ) ;

private:

  /// input variables calculated once in process() and passed to all calcSCorrection() calls
  struct SCorrInputParams {
    LHCb::CaloCellID  cellID;
    Gaudi::XYZPoint  seedPos;
    double                 z;
  };

  /// Jacobian elements returned from calcSCorrection() to process()
  struct SCorrOutputParams {
    double dXhy_dXcl;
    double dYhy_dYcl;
  };


  /// calculate corrected CaloHypo position depending on CaloCluster position
  void calcSCorrection( double  xBar, double  yBar, double &xCor, double &yCor,
                        const struct SCorrInputParams                  &params,
                        struct SCorrOutputParams                       *results ) const;

private:
  using IncCounter =  Gaudi::Accumulators::Counter<>;
  using SCounter =  Gaudi::Accumulators::StatCounter<float>;
  using MapOfCounters = std::map<std::string, SCounter >;

  mutable IncCounter m_counterSkipNegativeEnergyCorrection{this, "Skip negative energy correction"};
  mutable SCounter m_counterDeltaX{this, "Delta(X)"};
  mutable SCounter m_counterDeltaY{this, "Delta(Y)"};
};
// ============================================================================
#endif // CALORECO_CALOSCORRECTION_H
