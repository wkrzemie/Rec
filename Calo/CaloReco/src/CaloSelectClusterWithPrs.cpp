/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/SystemOfUnits.h"
#include "CaloSelectClusterWithPrs.h"

DECLARE_COMPONENT( CaloSelectClusterWithPrs )

// ============================================================================
CaloSelectClusterWithPrs::CaloSelectClusterWithPrs
( const std::string& type   , 
  const std::string& name   ,
  const IInterface*  parent )
  : GaudiTool ( type , name , parent )
{
  declareInterface<ICaloClusterSelector> ( this ) ;
}
// ============================================================================

StatusCode CaloSelectClusterWithPrs::initialize (){  
  // initialize the base class 
  StatusCode sc = GaudiTool::initialize () ;
  //
  m_toPrs = tool<ICaloHypo2Calo>("CaloHypo2Calo", "CaloHypo2Prs");
  m_toPrs->setCalos(m_det,"Prs");
  counterStat = tool<ICounterLevel>("CounterLevel");
  return sc;
}

// ============================================================================
/** @brief "select"  method 
 *
 *  Cluster is considered to be "selected" if there are Spd/Prs hit in front 
 *
 */
// ============================================================================
bool CaloSelectClusterWithPrs::select( const LHCb::CaloCluster* cluster ) const{ 
return (*this) ( cluster ); 
}
// ============================================================================
bool CaloSelectClusterWithPrs::operator()( const LHCb::CaloCluster* cluster   ) const{
  // check the cluster 
  if ( 0 == cluster ) { Warning ( "CaloCluster* points to NULL!" ).ignore() ; return false ; }
  double ePrs = m_toPrs->energy( *cluster, "Prs");
  int mPrs = m_toPrs->multiplicity();
  if ( UNLIKELY(msgLevel( MSG::DEBUG) ))debug() << "Found " << mPrs << "Prs hits " 
                                      << " for a total energy of " << ePrs <<  endmsg;

  bool sel = (ePrs>m_cut) && (mPrs>m_mult) ;
  if(counterStat->isVerbose())counter("selected clusters") += (int) sel;
  return sel;
}
