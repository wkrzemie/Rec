/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SUBCLUSTERSELECTORTOOL_H
#define SUBCLUSTERSELECTORTOOL_H 1


// Include files
// from Gaudi
#include  "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"
#include  "CaloDet/DeCalorimeter.h"
#include  "Event/CaloCluster.h"
#include  "CaloCorrectionBase.h"
#include  "CaloInterfaces/ICaloSubClusterTag.h"


namespace CaloClusterMask {
  enum  Mask{area3x3        = 0 ,  // MUST BE 0 FOR BACKWARD COMPATIBILITY
             area2x2        = 1 ,
             SwissCross     = 2 ,
             Last
  };
  constexpr int nMask = Last+1;
  inline const std::string maskName[nMask] = { "3x3", "2x2","SwissCross","Unknown" };
}



static const InterfaceID IID_SubClusterSelectorTool ( "SubClusterSelectorTool", 1, 0 );

/** @class SubClusterSelectorTool SubClusterSelectorTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-20
 */
class SubClusterSelectorTool : public GaudiTool, virtual public IIncidentListener {
public:

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_SubClusterSelectorTool; }

  /// Standard constructor
  SubClusterSelectorTool( const std::string& type,
                          const std::string& name,
                          const IInterface* parent);

  StatusCode initialize() override;
  StatusCode finalize() override;

  void handle(const Incident&  ) override {
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )debug() << "IIncident Svc reset" << endmsg;
    updateParamsFromDB();
  }

  StatusCode getParamsFromOptions();
  void updateParamsFromDB();

  StatusCode tag(LHCb::CaloCluster* cluster);
  StatusCode tagEnergy(LHCb::CaloCluster* cluster);
  StatusCode tagPosition(LHCb::CaloCluster* cluster);

private:

  std::vector<std::string>          m_taggerE  ;
  std::vector<std::string>          m_taggerP  ;
  std::vector<ICaloSubClusterTag*>  m_tagE      ;
  std::vector<ICaloSubClusterTag*>  m_tagP      ;

  // associate known cluster mask to tagger tool
  Gaudi::Property<std::map<std::string,std::string>> m_clusterTaggers
    {this, "ClusterTaggers", {
      {""          , "useDB"},
      {"useDB"     , "useDB"},
      {"3x3"       , "SubClusterSelector3x3"},
      {"2x2"       , "SubClusterSelector2x2"},
      {"SwissCross", "SubClusterSelectorSwissCross"},
    }, "associate known cluster mask to tagger tool"};

  Gaudi::Property<std::string> m_condition {this, "ConditionName", ""};
  Gaudi::Property<std::string> m_det       {this, "Detector"};

  DeCalorimeter*                    m_detector = nullptr;
  CaloCorrectionBase*               m_dbAccessor = nullptr;
  std::vector<std::string>          m_DBtaggerE  ;
  std::vector<std::string>          m_DBtaggerP  ;
  LHCb::CaloDigitStatus::Status     m_energyStatus = LHCb::CaloDigitStatus::UseForEnergy   |  LHCb::CaloDigitStatus::UseForCovariance;
  LHCb::CaloDigitStatus::Status     m_positionStatus = LHCb::CaloDigitStatus::UseForPosition |  LHCb::CaloDigitStatus::UseForCovariance;
  std::string m_sourceE;
  std::string m_sourceP;
};
#endif // SUBCLUSTERSELECTORTOOL_H
