/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALOSELECTCLUSTERWITHSPD_H
#define CALOSELECTCLUSTERWITHSPD_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// CaloInterfaces
// ============================================================================
#include "CaloInterfaces/ICaloClusterSelector.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloInterfaces/ICounterLevel.h"
// ============================================================================
/** @class CaloSelectClusterWithSpd
 *  Helper base class for "cluster selectioin with Spd" tools
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2009-07-18
 */
// ============================================================================
class CaloSelectClusterWithSpd :
  public virtual ICaloClusterSelector ,
  public          GaudiTool
{
public:
  // ==========================================================================
  /// initialize the tool
  StatusCode initialize() override;
  // ==========================================================================
public:
  // ==========================================================================
  /// number of hits in SPD
  int n_hits ( const LHCb::CaloCluster& cluster ) const ;
  /// number of hits in SPD
  int n_hit  ( const LHCb::CaloCluster& cluster ) const
  { return n_hits ( cluster ) ; }
  // ==========================================================================
  /// standard constructor
  CaloSelectClusterWithSpd
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) ;
  // ==========================================================================
protected:
  // ==========================================================================
  /// get number of hits
  int cut () const { return m_cut ; }
  /// get the tool
  ICaloHypo2Calo* calo2calo () const { return m_toSpd ; }
  /// get the calorimeter
  const std::string& det() const { return m_det   ; }
  ICounterLevel* counterStat = nullptr;
  // ==========================================================================
private:
  // ==========================================================================
  /// number of hits in spd
  Gaudi::Property<int> m_cut {this, "MinMultiplicity", 0, "number of hits in spd "};
  /// Calo -> Calo tool
  ICaloHypo2Calo* m_toSpd = nullptr;
  /// Calorimeter
  Gaudi::Property<std::string> m_det {this, "Detector", "Ecal"};
  // ==========================================================================
};
#endif // CALOSELECTCLUSTERWITHSPD_H
// ============================================================================
