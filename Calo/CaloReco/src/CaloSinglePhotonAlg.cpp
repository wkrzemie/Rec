/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
// ============================================================================
#include "GaudiKernel/SystemOfUnits.h"
#include "CaloUtils/CaloDataFunctor.h"
#include "Event/CellID.h"
#include "CaloInterfaces/ICaloClusterSelector.h"
#include "CaloInterfaces/ICaloHypoTool.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloSinglePhotonAlg.h"
#include "CaloUtils/CaloMomentum.h"
// ============================================================================
/** @file
 *
 *  Implementation file for class : CaloSinglePhotonAlg
 *  The implementation is based on F.Machefert's codes.
 *  @see CaloSinglePhotonAlg
 *
 *  @author Frederic Machefert machefer@in2p3.fr
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 31/03/2002
 */
// ============================================================================
namespace
{
template <typename Container, typename Arg, typename Parent>
bool apply(const Container& c, Arg& arg, const char* errmsg, const Parent& parent)
{
  bool r = std::all_of( std::begin(c), std::end(c),
  [&](typename Container::const_reference elem) {
    return (*elem)(&arg).isSuccess();
  } );
  if (UNLIKELY(!r)) parent.Error( errmsg, StatusCode::FAILURE ).ignore();
  return r;
}

template <typename T>
auto make_optional(bool flag, T&& t) {
    return flag ? std::optional{std::forward<T>(t)}
                : std::optional<T>{};
}
}

DECLARE_COMPONENT( CaloSinglePhotonAlg )
// ============================================================================
/*  Standard constructor
 *  @param name algorithm name
 *  @param pSvc service locator
 */
// ============================================================================
CaloSinglePhotonAlg::CaloSinglePhotonAlg ( const std::string& name,
    ISvcLocator*       pSvc )
  : ScalarTransformer( name, pSvc,
                       KeyValue("InputData", {}), // note: context can not be used here -- only _after_ the baseclass is initialized!
KeyValue("OutputData", {}) )
{
  // context() is only available _after_ the baseclass is fully initialized -- so we cannot put
  // the 'final' default which uses it  as argument to it...
  // bit tricky: as these are datahandles, they have multiple options.... what to do with the
  // not specified options? -- so we 'hide' the details inside 'updateHandleLocation'
  updateHandleLocation( *this, "InputData", LHCb::CaloAlgUtils::CaloClusterLocation("Ecal", context() ));
  updateHandleLocation( *this, "OutputData", LHCb::CaloAlgUtils::CaloHypoLocation("Photons", context() ));

  setProperty ( "PropertiesPrint", true ) ;
}
// ============================================================================
/*   standard Algorithm initialization
 *   @return status code
 */
// ============================================================================
StatusCode CaloSinglePhotonAlg::initialize()
{
  StatusCode sc = ScalarTransformer::initialize();
  if ( sc.isFailure() ) {
    return Error("Could not initialize the base class!", sc);
  }

  // check the geometry information
  m_det = getDet<DeCalorimeter>( m_detData ) ;
  if ( !m_det ) return Error("Detector information is not available!");

  m_eT = LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> { m_det } ; // TODO: can we combine this into Over_Et_Threshold and make it a property??

  // locate selector tools
  std::transform( m_selectorsTypeNames.begin(), m_selectorsTypeNames.end(),
                  std::back_inserter(m_selectors),
  [&](const std::string & name) {
    return this->tool<ICaloClusterSelector>( name, this );
  } );
  if ( m_selectors.empty() )info() << "No Cluster Selection tools are specified!" << endmsg ;
  // locate correction tools
  std::transform( m_correctionsTypeNames.begin(), m_correctionsTypeNames.end(),
                  std::back_inserter(m_corrections),
  [&](const std::string & name) {
    return this->tool<ICaloHypoTool>( name, this );
  } );
  if ( m_corrections.empty() )info() << "No Hypo Correction tools are specified!" << endmsg ;
  // locate other hypo tools
  std::transform( m_hypotoolsTypeNames.begin(), m_hypotoolsTypeNames.end(),
                  std::back_inserter(m_hypotools),
  [&](const std::string & name) {
    return this->tool<ICaloHypoTool>( name, this );
  } );
  if ( m_hypotools.empty() )info() << "No Hypo Processing tools are specified!" << endmsg ;
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/*   standard Algorithm finalization
 *   @return status code
 */
// ============================================================================
StatusCode
CaloSinglePhotonAlg::finalize()
{
  m_selectors.clear () ;
  m_corrections.clear () ;
  m_hypotools.clear () ;
  m_selectorsTypeNames.clear () ;
  m_correctionsTypeNames.clear () ;
  m_hypotoolsTypeNames.clear () ;

  return ScalarTransformer::finalize() ;
}
// ============================================================================
//   Algorithm execution
// ============================================================================
std::optional<LHCb::CaloHypo>
CaloSinglePhotonAlg::operator()(const LHCb::CaloCluster& cluster) const
{
  // loop over all selectors
  bool select = ( m_eT(&cluster) >= m_eTcut &&
                  std::all_of(m_selectors.begin(),
                              m_selectors.end(),
                              [&] (Selectors::const_reference sel) {
                                return (*sel)(&cluster);
                              }));
  if ( !select ) return {};

  LHCb::CaloHypo hypo;

  hypo.setHypothesis( LHCb::CaloHypo::Hypothesis::Photon );
  hypo.addToClusters( &cluster );
  hypo.setPosition( std::make_unique<LHCb::CaloPosition>(cluster.position()) ); //@FIXME: why not CaloPosition by (optional) value?

  return make_optional(
           apply( m_hypotools, hypo, "Error from Other Hypo Tool, skip the cluster  ", *this)    && // loop over other hypo tools (e.g. add extra digits)
           apply( m_corrections, hypo,  "Error from Correction Tool skip the cluster", *this) && // loop over all corrections and apply corrections
           LHCb::CaloMomentum(&hypo).pt() >= m_eTcut,  // check momentum after all corrections, and insert into container pass...
           std::move(hypo) );
}

void CaloSinglePhotonAlg::postprocess(const LHCb::CaloHypos& hypos) const
{
  if (msgLevel(MSG::DEBUG)) debug() << " # of created Photon  Hypos is  " << hypos.size()  << endmsg;
  m_counterHypos += hypos.size();
}
