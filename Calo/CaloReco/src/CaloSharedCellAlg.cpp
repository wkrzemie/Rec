/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ===========================================================================
// Include files
// from GaudiKernel
#include "GaudiKernel/MsgStream.h" 
// CaloGen 
#include "CaloKernel/CaloException.h"
//LHCb Kernel
#include "GaudiKernel/SystemOfUnits.h"
// CaloEvent 
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
// CaloUtils
#include "CaloUtils/Digit2ClustersConnector.h"
#include "CaloUtils/ClusterFunctors.h"
#include "CaloUtils/SharedCells.h"
#include "CaloUtils/CaloAlgUtils.h"
// local
#include "CaloSharedCellAlg.h"

// ============================================================================
/** @file CaloSharedCellAlg.cpp
 * 
 *  Implementation file for class : CaloSharedCellAlg
 *
 *  @see CaloSharedCellAlg
 *  @see Digit2ClustersConnector 
 *  @see SharedCells 
 * 
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 30/06/2001 
 */
// ============================================================================

DECLARE_COMPONENT( CaloSharedCellAlg )

// ============================================================================
// Standard creator, initializes variables
// ============================================================================
CaloSharedCellAlg::CaloSharedCellAlg( const std::string& name,
                                      ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
{
  // set default data as a function of detector
  m_detData= LHCb::CaloAlgUtils::DeCaloLocation( name ) ;
  m_inputData = LHCb::CaloAlgUtils::CaloClusterLocation( name , context() );
}

// ============================================================================
// Initialisation. Check parameters
// ============================================================================
StatusCode CaloSharedCellAlg::initialize() 
{
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Initialise" << endmsg;
  ///
  StatusCode sc = GaudiAlgorithm::initialize();
  ///
  if( sc.isFailure() ) 
    { return Error("could not initialize base class GaudiAlgorithm!");}
  
  /// copy mode ? 
  m_copy = (!m_outputData.empty()) && (m_outputData.value() != m_inputData.value());

  counterStat = tool<ICounterLevel>("CounterLevel");
  return StatusCode::SUCCESS;
}

// ============================================================================
// Main execution
// ============================================================================
StatusCode CaloSharedCellAlg::execute() 
{
  // avoid long names
  using namespace SharedCells     ;
  using namespace LHCb::CaloDigitStatus ;
  
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Execute" << endmsg;
  //

  // useful typedefs
  typedef LHCb::CaloClusters        Clusters ;
  typedef const DeCalorimeter Detector ;
  
  // locate input data
  Clusters* clusters = get<Clusters>( m_inputData );
  if( 0 == clusters ) { return StatusCode::FAILURE ; }
  
  // locate geometry (if needed) 
  Detector* detector = 0;
  if( m_useDistance )
  {    
    detector = getDet<DeCalorimeter> ( m_detData );
    if( 0 == detector ){ return StatusCode::FAILURE ;}
  }
  
  Clusters* output = 0;
  if( m_copy ) ///< make a new container 
  {
    output = new Clusters();
    put( output , m_outputData );
    // make a copy 
    for ( const LHCb::CaloCluster* i : *clusters )
    { if( i ) { output->insert( new LHCb::CaloCluster{ *i }); } }
  }
  else { output = clusters; } ///< update existing sequence
  
  /** build inverse connection table/object 
   *  keep only digits which have connections 
   *   with 2 clusters and more!
   */
  const unsigned int cutOff = 2 ;
  typedef Digit2ClustersConnector   Table;
  Table  table;
  StatusCode scc = table.load( output->begin() , output->end  () , cutOff ) ;
  if(scc.isFailure())warning()<<"Unable to load the table"<<endmsg;
  
  // sort digit by energy in order the subsequent processing is pointer address independent !
  std::vector<const LHCb::CaloDigit*> reOrder;
  for( Table::Map::iterator entry = table.map().begin() ; table.map().end() != entry ; ++entry ){
    const LHCb::CaloDigit* dig = entry->first ;
    reOrder.push_back( dig );
  }
  std::stable_sort( reOrder.begin(), reOrder.end  () ,LHCb::CaloDataFunctor::inverse( LHCb::CaloDataFunctor::Less_by_Energy ) ) ;
  

  /// loop over all digits in the table  
  for( std::vector<const LHCb::CaloDigit*>::const_iterator idig = reOrder.begin() ; reOrder.end() != idig ; ++idig ){    
    Table::Map::iterator entry = table.map().find( *idig );
    if( entry == table.map().end() ) continue;
    const LHCb::CaloDigit* dig = entry->first ;
    /// ignore  artificial zeros  
    if( 0 == dig ) { continue; }
    
    StatusCode sc = StatusCode::SUCCESS; 
    if      ( m_useSumEnergy &&  !m_useDistance ){    
      sc = summedEnergyAlgorithm ( entry->second   , m_numIterations  ) ; // do not apply weights at this stage 
    }
    else if ( !m_useSumEnergy && !m_useDistance ){ 
      sc = seedEnergyAlgorithm( entry->second   , SeedCell ) ; 
    }
    else if ( m_useSumEnergy &&   m_useDistance ){ 
      sc = summedDistanceAlgorithm( entry->second   , 
                                    detector        ,
                                    dig->cellID()   , 
                                    m_showerSizes   ,
                                    m_numIterations  ) ; 
    }
    else if ( !m_useSumEnergy &&  m_useDistance ){ 
      sc = seedDistanceAlgorithm( entry->second   ,
                                  detector        ,
                                  SeedCell        ,
                                  dig->cellID()   ,
                                  m_showerSizes                    ) ;
    }
    else { return Error("Funny condition :-)) "); }
    ///
    if( sc.isFailure() ){ return Error("Could not redistribute the energy!"); }
  }
  

  if(counterStat->isQuiet())counter ( "#Clusters from '" + m_inputData +"'") += clusters->size() ;
  
  ///
  return StatusCode::SUCCESS;
  ///  
}

StatusCode CaloSharedCellAlg::finalize() {

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}


