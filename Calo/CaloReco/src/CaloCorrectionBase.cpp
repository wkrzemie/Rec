/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 

#include "Event/ProtoParticle.h"
#include "CaloCorrectionBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloCorrectionBase
//
// 2010-05-07 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloCorrectionBase )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloCorrectionBase::CaloCorrectionBase( const std::string& type   , 
                                        const std::string& name   ,
                                        const IInterface*  parent ) 
  : GaudiTool ( type, name , parent )
{
  declareInterface<CaloCorrectionBase>(this);
  m_cmLoc = LHCb::CaloAlgUtils::CaloIdLocation("ClusterMatch", context());
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloCorrectionBase::initialize() {
  StatusCode sc = GaudiTool::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiTool

  if ( UNLIKELY (msgLevel(MSG::DEBUG) ) ) debug() << "==> Initialize" << endmsg;

  // register to incident service
  IIncidentSvc* inc = incSvc() ;
  if ( inc ) inc -> addListener( this , IncidentType::EndEvent ) ;

  // check the setting

  // transform vector of accepted hypos
  m_hypos.clear () ;
  for( const auto& hypo : m_hypos_ ) {
    if( hypo <= (int) LHCb::CaloHypo::Hypothesis::Undefined ||
        hypo >= (int) LHCb::CaloHypo::Hypothesis::Other      )
    { return Error("Invalid/Unknown  Calorimeter hypothesis object!" ) ; }
    m_hypos.push_back( LHCb::CaloHypo::Hypothesis( hypo ));
  }
  
  // locate and set and configure the Detector 
  m_det = getDet<DeCalorimeter>( m_detData ) ;
  if( !m_det ) { return StatusCode::FAILURE ; }
  m_calo.setCalo( m_detData);
  //
  if( m_hypos.empty() )return Error("Empty vector of allowed Calorimeter Hypotheses!" ) ; 
  
  // debug printout of all allowed hypos 
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) {
    debug() << " List of allowed hypotheses : " << endmsg;
    for( const auto& h : m_hypos ) {
      debug ()  <<  " -->" << h  << endmsg ; 
    }
    for( const auto& c : m_corrections ) {
      debug() << "Accepted corrections :  '" << c <<"'" << endmsg;
    }
  }

  // get external tools
  m_caloElectron = tool<ICaloElectron>("CaloElectron", this);
  m_pileup = tool<ICaloDigitFilterTool>("CaloDigitFilterTool","FilterTool");
  m_tables = tool<ICaloRelationsGetter>("CaloRelationsGetter","CaloRelationsGetter",this);
  counterStat = tool<ICounterLevel>("CounterLevel");
  return setConditionParams(m_conditionName);
}


//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloCorrectionBase::finalize()
{
  if ( UNLIKELY(msgLevel(MSG::DEBUG)) ) debug() << "==> Finalize" << endmsg;

  if( m_corrections.size() > 1 || *(m_corrections.begin()) != "All" ){
    for( const auto& c :  m_corrections ) {
      info() << "Accepted corrections :  '" << c <<"'" << endmsg;
    }
  }
  if( m_corrections.empty())warning() << "All corrections have been disabled for " <<  name() << endmsg;
  
    if( m_cond == nullptr )
      warning() << " Applied corrections configured via options for  " << name() <<endmsg;
    else if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << " Applied corrections configured via condDB  ('" << m_conditionName << "') for " 
              << name() << endmsg;
    
  for ( const auto& param : m_params ) 
  {
    if ( !param.active ) continue;
    const auto & type = param.type;
    const auto & vec  = param.data;
    if ( !vec.empty() )
    {
      int func = vec[0];
      int dim  = vec[1];
      if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
        debug() << " o  '" << type <<"'  correction as a '" << CaloCorrection::funcName[ func ] 
                << "' function of " << dim << " parameters" << endmsg;
    }
    else
    {
      warning() << " o '" << type << "' correction HAS NOT BEEN APPLIED  (badly configured)" << endmsg;
    }
  }

  return GaudiTool::finalize();  // must be called after all other actions
}

//=============================================================================©©ﬁ
StatusCode CaloCorrectionBase::setDBParams(){
  if(m_update)return StatusCode::SUCCESS; // don't update twice before next incident
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "Get params from CondDB condition = " << m_conditionName << endmsg;
  m_params = Params::Vector(CaloCorrection::nT);
  registerCondition(m_conditionName, m_cond, &CaloCorrectionBase::updParams);
  return runUpdate();  
}
// ============================================================================
StatusCode CaloCorrectionBase::setOptParams(){
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )debug() << "Get params from options " << endmsg;
  if( m_optParams.empty() && m_conditionName != "none"  ){
    info()<< "No default options parameters defined"<<endmsg;
    return StatusCode::SUCCESS;
  }
  m_params = Params::Vector(CaloCorrection::nT);
  for ( const auto&  p : m_optParams ) 
  {
    const std::string& name = p.first;
    if ( accept( name ) ) 
    {
      m_params[stringToCorrectionType(name)] = Params( name, p.second );
    }  
  }
  checkParams();
  return StatusCode::SUCCESS;
}
// ============================================================================
StatusCode CaloCorrectionBase::updParams(){
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "updParams() called" << endmsg;
  if ( !m_cond ) return Error("Condition points to NULL", StatusCode::FAILURE);

  // toDo
  for( const auto& paramName : m_cond->paramNames() ) {
    if( m_cond -> exists( paramName ) ){
      const auto & params = m_cond->paramAsDoubleVect( paramName ); 
      if ( accept( paramName ) ) 
      {
        m_params[ stringToCorrectionType(paramName) ] = Params( paramName, params );
      }
    }
  }
  m_update = true;
  checkParams(); 

  return StatusCode::SUCCESS;
}


CaloCorrection::Parameters 
CaloCorrectionBase::getParams(const CaloCorrection::Type type, const LHCb::CaloCellID id) const
{
  const auto & params = m_params[type];
  if ( !params.active ) return { CaloCorrection::Unknown, CaloCorrection::ParamVector{} };

  // get parameters
  const auto & pars = params.data;
  if ( pars.size() < 2 ) return { CaloCorrection::Unknown, CaloCorrection::ParamVector{} };
  
  // consistency of pars checked elsewhere - straight parsing here
  const auto&  func = pars[0];
  const auto&  dim  = pars[1];
  const int narea   = ( func != CaloCorrection::GlobalParamList ) ? 3         : 1;
  const int shift   = ( func != CaloCorrection::GlobalParamList ) ? id.area() : 0;
  int         pos   = 2 + shift;

  CaloCorrection::ParamVector v; v.reserve(dim);
  for ( int i = 0 ; i < dim ; ++i )
  {
    v.push_back( pars[pos] );
    pos += narea;
  }
  return { (CaloCorrection::Function) func , std::move(v) }; 
}


double CaloCorrectionBase::getCorrection(const CaloCorrection::Type type,  
                                         const LHCb::CaloCellID id,
                                         double var, double def) const
{

  const auto pars = getParams(type,id);

  if ( UNLIKELY(msgLevel( MSG::DEBUG) ) )
  {
    const auto& name =  CaloCorrection::typeName[ type ];
    debug() << "Correction type " << name << " to be applied on cluster (seed = " << id << ") is a '" 
            << CaloCorrection::funcName[ pars.first ] << "' function with params = " << pars.second
            << endmsg;
  }

  // compute correction 
  if( pars.first == CaloCorrection::Unknown ||  pars.second.empty() ) return def; 

  // list accessor - not correction :
  if( pars.first == CaloCorrection::ParamList || pars.first == CaloCorrection::GlobalParamList)
  {
    warning() << " Param accessor is a fake function - no correction to be applied - return default value" << endmsg;
    return def;
  }

  double cor = def; 
  // polynomial correction 
  const auto & temp = pars.second;

  // polynomial functions
  if (pars.first == CaloCorrection::Polynomial || 
      pars.first == CaloCorrection::InversPolynomial || 
      pars.first == CaloCorrection::ExpPolynomial ||
      pars.first == CaloCorrection::ReciprocalPolynomial ){
    double v = 1.;
    cor = 0.;
    for( auto  i = temp.begin() ; i != temp.end() ; ++ i){
      cor += (*i) * v;
      if( pars.first == CaloCorrection::ReciprocalPolynomial )
        v = (var == 0) ? 0. : v/var ;
      else
        v *= var;
#if defined(__clang__)
#warning "Activating workaround for FPE with clang"
      // Without this, clang optimiser does something with this loop that causes FPE...
      if ( UNLIKELY(msgLevel(MSG::VERBOSE)) ) verbose() << "cor = " << cor << endmsg;
#endif
    }
    if( pars.first == CaloCorrection::InversPolynomial) cor = ( cor == 0 ) ? def : 1./cor;
    if( pars.first == CaloCorrection::ExpPolynomial) cor = ( cor == 0 ) ? def : myexp(cor);
  }

  // sigmoid function
  else if( pars.first == CaloCorrection::Sigmoid ){
    if( temp.size() == 4){
      const auto&  a = temp[0];
      const auto&  b = temp[1];
      const auto&  c = temp[2];
      const auto&  d = temp[3];
      cor = a + b*mytanh(c*(var+d));
    }
    else{
      Warning("The power sigmoid function must have 4 parameters").ignore();
    }
  }

  // Sshape function
  else if( pars.first == CaloCorrection::Sshape || pars.first == CaloCorrection::SshapeMod ){
    if( temp.size() == 1){
      const auto& b = temp[0];
      constexpr double delta = 0.5;
      if( b > 0 ) {
        double arg = var/delta ;
        if      ( pars.first == CaloCorrection::SshapeMod ) { arg *= mysinh(delta/b); }
        else if ( pars.first == CaloCorrection::Sshape    ) { arg *= mycosh(delta/b); }        
        cor = b * mylog( arg + std::sqrt( arg*arg + 1.0 ) );
      }
    }
    else{
      Warning("The Sshape function must have 1 parameter").ignore();
    }  
  }

  // Shower profile function
  else if( pars.first == CaloCorrection::ShowerProfile ){
    if( temp.size() == 10){
      if( var > 0.5 ) {
        cor = temp[0]  * myexp( -temp[1]*var);
        cor += temp[2] * myexp( -temp[3]*var);
        cor += temp[4] * myexp( -temp[5]*var);
      }else{
        cor  = 2.;
        cor -= temp[6] * myexp( -temp[7]*var);
        cor -= temp[8] * myexp( -temp[9]*var);
      }
    }else{
      Warning("The ShowerProfile function must have 10 parameters").ignore();
    }  
  }

  // Sinusoidal function
  else if( pars.first == CaloCorrection::Sinusoidal ){
    if( temp.size() == 1){
      const double& A = temp[0];
      cor = A*mysin(2*M_PI*var);
    }
    else{
      Warning("The Sinusoidal function must have 1 parameter").ignore();
    }
  }

  if(counterStat->isVerbose())kounter(type,id.areaName()) += cor; 

  return cor;
}


void CaloCorrectionBase::checkParams(){
  if ( m_params.size() != CaloCorrection::nT ){
    warning() << "Corrections vector size != " << CaloCorrection::nT << endmsg;
  }
  for( auto& param : m_params ){
    if ( !param.active ) continue;
    const auto & type = param.type;
    // is the type registered
    bool ok = false;
    for( unsigned int i = 0 ; i != CaloCorrection::lastType ; ++i ){
      if( type == CaloCorrection::typeName[i]){
        ok = true;
        break;
      }      
    }
    if( !ok ){
      warning() << " o Type " << type << " is not registered" << endmsg;
      param.clear();
      continue;
    }
    
    const auto & vec = param.data;
    int func = CaloCorrection::Unknown;
    int dim  = 0;
    if( vec.size() < 3 ) ok = false;
    else{
      func = vec[0];
      dim  = vec[1];
      if( func >= CaloCorrection::Unknown )ok = false;
      int narea = ( func != CaloCorrection::GlobalParamList ) ? 3 : 1;
      if( narea*dim+2 != (int) vec.size())ok=false;    
      if( dim <= 0 ) ok = false;
    }
    if( !ok ){
      warning() << " o Parameters for correction '"<< type << "' are badly defined : [ " << vec << " ]"<< endmsg;
      param.clear();
    }else{
      if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
        debug() << " o Will apply correction '" << type <<"' as a '" << CaloCorrection::funcName[ func ] 
                << "' function of " << dim << " parameters" << endmsg;
    }
  }
}



double CaloCorrectionBase::incidence(const LHCb::CaloHypo* hypo, bool straight)const 
{
  const LHCb::CaloCluster* cluster = LHCb::CaloAlgUtils::ClusterFromHypo(hypo,true) ;
  double incidence = 0;
  if ( LHCb::CaloHypo::Hypothesis::EmCharged == hypo->hypothesis() && !straight )
  {
    // for electron hypothesis : get the matching track
    if ( exist<LHCb::Calo2Track::IClusTrTable>(m_cmLoc) ) 
    {
      auto * ctable = m_tables->getClusTrTable( m_cmLoc );
      const auto range = ctable -> relations(cluster);
      if ( !range.empty() )
      {
        const LHCb::Track * ctrack = range.front();
        // temporary protoParticle
        auto * proto = new LHCb::ProtoParticle();
        proto->setTrack( ctrack );
        proto->addToCalo( hypo );
        if ( m_caloElectron->set(proto) ) { incidence = m_caloElectron->caloState().momentum().Theta() ; }
        delete proto;
      }
    } 
  }
  else
  {
    // for neutrals :
    auto cMomentum = LHCb::CaloMomentum( hypo );
    incidence = cMomentum.momentum().Theta();    
  }
  return incidence;
}      

double CaloCorrectionBase::getCorrectionDerivative(const CaloCorrection::Type type,  
                                                   const LHCb::CaloCellID id ,
                                                   double var, double def) const
{
  const auto pars = getParams( type , id );

  if ( msgLevel( MSG::DEBUG) ) 
  { 
    const auto name =  CaloCorrection::typeName[ type ];
    debug() << "Derivative for Correction type " << name 
            << " to be calculated for cluster (seed = " << id
            << ") is a '" 
            << CaloCorrection::funcName[ pars.first ] 
            << "' function with params = " << pars.second << endmsg;
  }

  // compute correction 
  double cor = def; 
  if( pars.first == CaloCorrection::Unknown ||  pars.second.empty() ) return cor; 

  // polynomial correction 
  const auto & temp = pars.second;

  // polynomial functions
  double ds = 0.;
  if ( pars.first == CaloCorrection::Polynomial  )
  {
    ds  = 0.;
    double v = 1.;
    int cnt = 0;
    auto i = temp.begin();
    for( ++ i, cnt ++ ; i != temp.end() ; ++ i, cnt ++){
      ds += (*i) * cnt * v;
      v  *= var;
    }
  }

  else if (pars.first == CaloCorrection::InversPolynomial )
  {
    double v = 1.;
    cor = 0.;
    for( auto i = temp.begin() ; i != temp.end() ; ++ i){
      cor += (*i) * v;
      v *= var;
    }
    cor = ( cor == 0 ) ? def : 1./cor;

    v       = 1.;
    ds      = 0.;
    int cnt = 0;
    auto i = temp.begin();
    for( ++ i, cnt ++ ; i != temp.end() ; ++ i, cnt ++){
      ds  += (*i) * cnt * v;
      v *= var;
    }
    ds *= -cor*cor;
  }

  else if (pars.first == CaloCorrection::ExpPolynomial )
  {
    double v = 1.;
    cor = 0.;
    for( auto i = temp.begin() ; i != temp.end() ; ++ i){
      cor += (*i) * v;
      v *= var;
    }
    cor = ( cor == 0 ) ? def : myexp(cor);
 
    ds = 0.;
    v  = 1.;
    int cnt = 0;
    auto i = temp.begin();
    for( ++ i, cnt ++ ; i != temp.end() ; ++ i, cnt ++){
      ds  += (*i) * cnt * v;
      v *= var;
    }
 
    ds *= cor;
  }

  else if (pars.first == CaloCorrection::ReciprocalPolynomial )
  {
    ds = 0.;
    if ( var != 0 ){ 
      auto v = 1./(var*var);
      int cnt = 0;
      auto i = temp.begin();
      for( ++ i, cnt ++ ; i != temp.end() ; ++ i, cnt ++){
        ds  -= (*i) * cnt * v;
        v /= var;
      }
    }
  }

  // sigmoid function
  else if( pars.first == CaloCorrection::Sigmoid )
  {
    ds = 0.;
    if( temp.size() == 4){
      // double a = temp[0];
      const auto & b = temp[1];
      const auto & c = temp[2];
      const auto & d = temp[3];
      ds  = b*c*(1.- std::pow( mytanh(c*(var+d)),2) );
    }
    else{
      Warning("The power sigmoid function must have 4 parameters").ignore();
    }
  }


  // Sshape function
  else if( pars.first == CaloCorrection::Sshape || pars.first == CaloCorrection::SshapeMod ){
    ds = 0.;
    if( temp.size() == 1){
      const auto& b = temp[0];
      double delta = 0.5;
      if( b > 0 ) {
        double csh = 1.;
        if      ( pars.first == CaloCorrection::SshapeMod ) { csh = mysinh(delta/b); }
        else if ( pars.first == CaloCorrection::Sshape    ) { csh = mycosh(delta/b); }       
        const auto arg = var/delta * csh;
        ds = b / delta * csh / std::sqrt( arg*arg + 1. );
      }
    }
    else{
      Warning("The Sshape function must have 1 parameter").ignore();
    }
  }

  // Shower profile function
  else if( pars.first == CaloCorrection::ShowerProfile ){
    ds = 0.;
    if( temp.size() == 10){
      if( var > 0.5 ) {
        ds = -temp[0]*temp[1]*myexp( -temp[1]*var);
        ds+= -temp[2]*temp[3]*myexp( -temp[3]*var);
        ds+= -temp[4]*temp[5]*myexp( -temp[5]*var);
      }else{
        ds =  temp[6]*temp[7]*myexp( -temp[7]*var);
        ds+=  temp[8]*temp[9]*myexp( -temp[9]*var);
      }
    }else{
      Warning("The ShowerProfile function must have 10 parameters").ignore();
    }  
  }

  // Sinusoidal function
  else if( pars.first == CaloCorrection::Sinusoidal ){
    ds = 0.;
    if( temp.size() == 1){
      const auto& A = temp[0];
      constexpr double twopi=2.*M_PI;
      ds = A*twopi*mycos(twopi*var);
    }
    else{
      Warning("The Sinusoidal function must have 1 parameter").ignore();
    }

  }  

  return ds;
}
 
