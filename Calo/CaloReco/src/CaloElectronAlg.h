/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALORECO_CALOElectronALG_H
#define CALORECO_CALOElectronALG_H 1
// Include files
// from STL
#include <optional>
#include <string>
// from GaudiAlg
#include "GaudiAlg/ScalarTransformer.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloHypo.h"
#include "Event/CaloCluster.h"
#include "CaloInterfaces/ICounterLevel.h"

// forward delcarations
struct ICaloClusterSelector ;
struct ICaloHypoTool        ;

/** @class CaloElectronAlg CaloElectronAlg.h
 *
 *  The simplest algorithm of reconstruction of
 *  electrons in electromagnetic calorimeter.
 *
 *  @author Vanya Belyaev      Ivan.Belyaev@itep.ru
 *  @date   31/03/2002
 */
class CaloElectronAlg
: public Gaudi::Functional::ScalarTransformer<CaloElectronAlg,LHCb::CaloHypos(const LHCb::CaloClusters&)>
{
 public:

  CaloElectronAlg(const std::string& name, ISvcLocator* pSvcLocator );



  StatusCode initialize() override;
  StatusCode finalize() override;

  using ScalarTransformer::operator();
  std::optional<LHCb::CaloHypo> operator()(const LHCb::CaloCluster& ) const;
  void postprocess(const LHCb::CaloHypos&) const;
private:

  /// container of names
  typedef std::vector<std::string>           Names       ;
  /// container of selector tools
  typedef std::vector<ICaloClusterSelector*> Selectors   ;
  /// containers of hypo tools
  typedef std::vector<ICaloHypoTool*>        HypoTools   ;
  /// container of correction tools (S-,L-,...)
  typedef HypoTools                          Corrections ;


  // cluster selectors
  Selectors              m_selectors;
  Gaudi::Property<Names> m_selectorsTypeNames {this, "SelectionTools", {
    "CaloSelectCluster/ElectronCluster",
    "CaloSelectChargedClusterWithSpd/ChargedWithSpd",
    "CaloSelectClusterWithPrs/ClusterWithPrs",
    "CaloSelectorNOT/ChargedWithTracks"
  }, "List of Cluster selector tools"};

  // corrections
  Corrections            m_corrections;
  Gaudi::Property<Names> m_correctionsTypeNames
    {this, "CorrectionTools", {}, "List of primary correction tools"};

  // other hypo tools
  HypoTools              m_hypotools;
  Gaudi::Property<Names> m_hypotoolsTypeNames
    {this, "HypoTools", {"CaloExraDigits/SpdPrsExtraE"},
    "List of generic Hypo-tools to apply for newly created hypos"};

  // corrections
  Corrections            m_corrections2;
  Gaudi::Property<Names> m_correctionsTypeNames2 {this, "CorrectionTools2", {
    "CaloECorrection/ECorrection" ,
    "CaloSCorrection/SCorrection" ,
    "CaloLCorrection/LCorrection"
  }, "List of tools for 'fine-corrections"};

  // other hypo tools
  HypoTools    m_hypotools2;
  Gaudi::Property<Names> m_hypotoolsTypeNames2
    {this, "HypoTools2", {},
    "List of generi Hypo-tools to apply for corrected hypos"};

  Gaudi::Property<std::string> m_detData    {this, "Detector", LHCb::CaloAlgUtils::DeCaloLocation("Ecal")  };
  const DeCalorimeter*  m_det = nullptr;
  Gaudi::Property<float> m_eTcut {this, "EtCut", 0, "Threshold on cluster & hypo ET"};
  ICounterLevel* counterStat = nullptr;

  LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> m_eT { nullptr } ; // TODO: can we combine this into Over_Et_Threshold and make it a property??

};

// ============================================================================
#endif // CALOElectronALG_H
