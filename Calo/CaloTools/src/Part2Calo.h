/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Part2Calo.h,v 1.2 2010-06-10 12:46:38 cattanem Exp $
#ifndef PART2CALO_H
#define PART2CALO_H 1

// Include files
#include "Track2Calo.h"
#include "Event/ProtoParticle.h"
// from Gaudi
#include "CaloInterfaces/IPart2Calo.h"



/** @class Part2Calo Part2Calo.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-11-30
 */
class Part2Calo : public Track2Calo, virtual public IPart2Calo {
public:
  /// Standard constructor
  Part2Calo( const std::string& type,
               const std::string& name,
               const IInterface* parent);

  StatusCode initialize() override;

  using Track2Calo::match;
  bool match(const  LHCb::ProtoParticle* proto,
                     std::string det = DeCalorimeterLocation::Ecal,
                     CaloPlane::Plane plane = CaloPlane::ShowerMax,
                     double delta = 0.
                     ) override;
  bool match(const  LHCb::Particle* part,
                     std::string det = DeCalorimeterLocation::Ecal,
                     CaloPlane::Plane plane = CaloPlane::ShowerMax,
                     double delta = 0.
                     ) override;
  inline bool inAcceptance() override;


protected:
  bool setting (const  LHCb::Particle* part);
  bool setting (const  LHCb::ProtoParticle* proto);
  const LHCb::Particle*      m_particle;
  const LHCb::ProtoParticle* m_proto;
private:
};
#endif // PART2CALO_H

inline bool Part2Calo::inAcceptance(){
  if(m_det == DeCalorimeterLocation::Spd)  return m_proto->info(LHCb::ProtoParticle::additionalInfo::InAccSpd,  0.)!=0;
  if(m_det == DeCalorimeterLocation::Prs)  return m_proto->info(LHCb::ProtoParticle::additionalInfo::InAccPrs,  0.)!=0;
  if(m_det == DeCalorimeterLocation::Ecal) return m_proto->info(LHCb::ProtoParticle::additionalInfo::InAccEcal, 0.)!=0;
  if(m_det == DeCalorimeterLocation::Hcal) return m_proto->info(LHCb::ProtoParticle::additionalInfo::InAccHcal, 0.)!=0;
  return false;
}
