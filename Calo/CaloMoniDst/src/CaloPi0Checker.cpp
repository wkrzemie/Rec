/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "GaudiAlg/Consumer.h"
// #include "Relations/IRelationWeighted.h"
#include "Relations/RelationWeighted.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "Event/CaloHypo.h"
#include "Event/MCParticle.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloUtils/CaloMomentum.h"
#include "CaloUtils/ClusterFunctors.h"
#include "CaloMoniAlg.h"
#include "Relations/RelationWeighted1D.h"

// ============================================================================

/** @class CaloPi0Checker CaloPi0Checker.cpp
 *
 *  Simple MC pi0 monitoring algorithm
 *  It produces 2 histograms
 *  <ol>
 *  <li> "Raw" mass distribution of 2 photons </li>
 *  <li> Mass distribution of 2 photons after Pt cut for each photon </li>
 *  <li> Mass distribution of 2 photons after Pt cut for combination </li>
 *  </ol>
 *
 *  @see CaloAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

using Input   = LHCb::RelationWeighted1D<LHCb::CaloCluster,LHCb::MCParticle,float>;
using Inputs  = LHCb::CaloHypo::Container;
using Photon  = const LHCb::CaloHypo;

class CaloPi0Checker final
: public Gaudi::Functional::Consumer<void(const Input&, const Inputs&),
    Gaudi::Functional::Traits::BaseClass_t<CaloMoniAlg>>
{
public:
  StatusCode initialize() override;
  void operator()(const Input&, const Inputs&) const override;

  CaloPi0Checker( const std::string &name, ISvcLocator *pSvcLocator );

private:
  LHCb::ClusterFunctors::ClusterFromCalo m_calo = DeCalorimeterLocation::Ecal;

  LHCb::ParticleID m_pi0ID{0};

  Gaudi::Property<float> m_cut { this, "Cut", float(50 * Gaudi::Units::perCent), "photon purity cut"};

  Gaudi::Property<std::string> m_pi0Name {this, "Ancestor", "pi0"};
};

// =============================================================================

DECLARE_COMPONENT( CaloPi0Checker )

// =============================================================================

CaloPi0Checker::CaloPi0Checker( const std::string &name, ISvcLocator *pSvcLocator )
: Consumer( name, pSvcLocator, {
    KeyValue{ "Input" , "Relations/"+LHCb::CaloClusterLocation::Default },
    KeyValue{ "Inputs", LHCb::CaloHypoLocation::Photons }
}){
  // set the appropriate defualt value for detector data
  setDetData( DeCalorimeterLocation::Ecal );
}

// =============================================================================

StatusCode CaloPi0Checker::initialize(){
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc; // error already printedby GaudiAlgorithm

  // re-initialize the Ecal cluster selector
  m_calo.setCalo( detData() );

  // locate particle eproperty service
  auto ppS = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
  if ( 0 == ppS ) return StatusCode::FAILURE;

  const auto pp = ppS->find( m_pi0Name );
  if ( pp == nullptr ) return Error( "Could not locate particle '"+m_pi0Name+"'" );
  m_pi0ID = pp->pid();

  hBook1( "1", "Gamma-Gamma mass       " , 0, 1, 500 );
  hBook1( "2", "Gamma-Gamma mass (MCpi0 match)" , 0, 1, 500 );
  if( m_split ){
    Warning( "No area spliting allowed for CaloPi0Checker").ignore();
    m_split = false;
  }
  return StatusCode::SUCCESS;
}

// =============================================================================

void CaloPi0Checker::operator()(const Input& table, const Inputs& photons ) const {

  using namespace LHCb::ClusterFunctors;

  // loop over the first photon
  for( auto g1 = photons.begin(); photons.end() != g1; ++g1 ){
    Photon *photon1 = *g1;
    if ( photon1 == nullptr ) continue;
    LHCb::CaloMomentum momentum1( photon1 );

    // get Ecal cluster
    const auto clusters1 = photon1->clusters();
    if ( clusters1.empty() ) continue;
    const auto cluster1 = ( 1 == clusters1.size() ) ? clusters1.begin() :
      std::find_if( clusters1.begin(), clusters1.end(), m_calo );
    if ( clusters1.end() == cluster1 ) continue;

    // get all MCtruth information for this cluster
    const float cut1 = (float) ((*cluster1)->e() * m_cut);
    const auto range1 = table.relations( *cluster1, cut1, true );

    // loop over the second photon
    for( auto g2 = g1 + 1; photons.end() != g2; ++g2 ){
      Photon *photon2 = *g2;
      if ( photon2 == nullptr ) continue;
      LHCb::CaloMomentum momentum2( photon2 );

      // get Ecal cluster
      const auto clusters2 = photon2->clusters();
      if ( clusters2.empty() ) continue;
      auto cluster2 = ( 1 == clusters2.size() ) ? clusters2.begin() :
        std::find_if( clusters2.begin(), clusters2.end(), m_calo );
      if ( clusters2.end() == cluster2 ) continue;

      // get all MCtruth information for this cluster
      const float cut2 = (float)((*cluster2)->e() * m_cut);
      const auto range2 = table.relations( *cluster2, cut2, true );

      // double loop for search the common ancestor
      LHCb::MCParticle *pi0 = nullptr;
      for( auto mc1 = range1.begin();( pi0 == nullptr ) && ( range1.end() != mc1 ); ++mc1 ){
        if ( mc1->to() == nullptr ) continue;
        for ( auto mc2 = range2.begin(); ( pi0 == nullptr ) && ( range2.end() != mc2 ); ++mc2 ){
          if ( mc1->to() != mc2->to() ) continue; // common ancestor?
          if ( m_pi0ID == mc1->to()->particleID() ) pi0 = mc1->to();
        } // end of second MC loop
      } // end of first MC loop

      const double mass = (momentum1.momentum()+momentum2.momentum()).mass();
      hFill1( "1", mass/Gaudi::Units::GeV );
      if ( pi0 == nullptr ) continue;
      hFill1( "2", mass/Gaudi::Units::GeV );
    } // end of loop over second photon
  } // end of loop over first photon
  return; // StatusCode::SUCCESS;
}
