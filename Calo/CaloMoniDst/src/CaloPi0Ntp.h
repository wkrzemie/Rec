/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "Event/L0DUReport.h"
#include "Event/ODIN.h"
#include "Event/RecVertex.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloInterfaces/ICounterLevel.h"

// List of Consumers dependencies
namespace {
  using ODIN = LHCb::ODIN;
  using L0 = LHCb::L0DUReport;
  using Hypos = LHCb::CaloHypo::Container;
  using Vertices = LHCb::RecVertices;
}

// =============================================================================

class CaloPi0Ntp final
: public Gaudi::Functional::Consumer<void(const ODIN&, const L0&, const Hypos&, const Vertices&),
    Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>>
{
public:
  /// standard algorithm initialization
  CaloPi0Ntp( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void operator()(const ODIN&, const L0&, const Hypos&, const Vertices&) const override;

private:
  void hTuning(std::string, int, int, double, double,
               const Gaudi::LorentzVector, const LHCb::CaloCellID ,
               const Gaudi::LorentzVector, const LHCb::CaloCellID , int nVert) const;

  // Tools
  DeCalorimeter* m_calo = nullptr;
  ToolHandle<ICounterLevel> m_counterStat { "CounterLevel" };
  ToolHandle<IEventTimeDecoder> m_odin    { "OdinTimeDecoder/OdinDecoder", this };
  ToolHandle<ICaloHypo2Calo> m_toSpd      { "CaloHypo2Calo/CaloHypo2Spd" , this };
  ToolHandle<ICaloHypo2Calo> m_toPrs      { "CaloHypo2Calo/CaloHypo2Prs" , this };

  Gaudi::Property<std::pair<double,double>> m_ppt
    {this, "PhotonPt", {250., 15000.}};

  Gaudi::Property<std::pair<double,double>> m_isol
    {this, "Isolation", {0., 9999.}, "Warning: a cut biases the pi0 mass"};

  Gaudi::Property<std::pair<int   , int>>    m_conv { this, "Conversion", { 1   , 1}};
  Gaudi::Property<std::pair<double, double>> m_prsE { this, "PrsE"      , { 0.  , 9999.}};
  Gaudi::Property<std::pair<double, double>> m_pt   { this, "Pt"        , { 200., 15000}};
  Gaudi::Property<std::pair<double, double>> m_e    { this, "E"         , { 0.  , 500000}};
  Gaudi::Property<std::pair<double, double>> m_mass { this, "Mass"      , { 50. , 900.}};

  Gaudi::Property<float>  m_leBin {this, "leBin", 0.25};
  Gaudi::Property<float>  m_etBin {this, "etBin", 150.};
  Gaudi::Property<float>  m_thBin {this, "thBin", 0.005};

  Gaudi::Property<float> m_hMin    { this, "hMin"      , 0.};
  Gaudi::Property<float> m_hMax    { this, "hMax"      , 900.};
  Gaudi::Property<int>   m_hBin    { this, "hBin"      , 450};
  Gaudi::Property<int>   m_spdBin  { this, "spdBin"    , 50};
  Gaudi::Property<bool>  m_tuple   { this, "Tuple"     , true};
  Gaudi::Property<bool>  m_histo   { this, "Histo"     , true};
  Gaudi::Property<bool>  m_trend   { this, "Trend"     , false};
  Gaudi::Property<bool>  m_usePV3D { this, "UsePV3D"   , false};
  Gaudi::Property<bool>  m_bkg     { this, "Background", false};

};
