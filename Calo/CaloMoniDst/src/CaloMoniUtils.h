/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/Line.h"
#include "LHCbMath/GeomFun.h"
#include "CaloUtils/CaloMomentum.h"

inline bool inRange(const std::pair<double,double> range, double value){
  return (value>=range.first) && (value<=range.second);
}

inline bool inRange(const std::pair<int,int> range, int value){
  return (value==0 && range.first==1 ) || (value>0 &&range.second==1);
}

//==============================================================================

// Helper method to calculate intersection, when only interested in a point,
// assuming that the intersection exists.
// Usage:
//    const auto cross = intersection(line, plane);
inline Gaudi::XYZPoint intersection(const Gaudi::Math::Line<Gaudi::XYZPoint,Gaudi::XYZVector>& line, const Gaudi::Plane3D& plane){
  Gaudi::XYZPoint cross;
  double mu;
  Gaudi::Math::intersection<Gaudi::Math::Line<Gaudi::XYZPoint,Gaudi::XYZVector>,Gaudi::Plane3D,Gaudi::XYZPoint>(line, plane, cross, mu);
  return cross;
}

//==============================================================================

// Idiom I often encountered
// inline LHCb::CaloCellID firstClusterSeed( const LHCb::CaloHypo* hypo ){
inline SmartRef<LHCb::CaloCluster> firstCluster( const LHCb::CaloHypo* hypo ){
  SmartRef<LHCb::CaloCluster> cluster;
  if( hypo != nullptr ){
    const auto clusters = hypo->clusters();
    if( !clusters.empty() ){
      cluster = *clusters.begin();
    }
  }
  return cluster;
}

//==============================================================================

// Adapters for ntuple, guard against nullptr too
inline Gaudi::XYZPoint position3d( const LHCb::CaloHypo* obj ){
  if( obj == nullptr ) return Gaudi::XYZPoint();
  return Gaudi::XYZPoint( obj->position()->x(), obj->position()->y(), obj->position()->z() );
}
// e.g. from firstCluster(hypo)
inline Gaudi::XYZPoint position3d( const LHCb::CaloCluster* obj ){
  if( obj == nullptr ) return Gaudi::XYZPoint();
  return Gaudi::XYZPoint( obj->position().x(), obj->position().y(), obj->position().z() );
}
inline Gaudi::XYZPoint position3d( const LHCb::State& obj ){
  return obj.position();
}
inline Gaudi::LorentzVector position4d( const LHCb::CaloCluster* obj ){
  if( obj == nullptr ) return Gaudi::LorentzVector();
  return Gaudi::LorentzVector( obj->position().x(), obj->position().y(), obj->position().z(), obj->e() );
}

// e.g., from
inline Gaudi::LorentzVector momentum( const LHCb::CaloHypo* obj ){
  if( obj == nullptr ) return Gaudi::LorentzVector();
  return Gaudi::LorentzVector(LHCb::CaloMomentum(obj).momentum());
}
// e.g., from proto->track()
inline Gaudi::LorentzVector momentum( const LHCb::Track* obj ){
  if( obj == nullptr ) return Gaudi::LorentzVector();
  return Gaudi::LorentzVector(obj->momentum().x(), obj->momentum().y(), obj->momentum().z(), obj->p());
}
// e.g., from m_caloElectron->caloState()
inline Gaudi::LorentzVector momentum( const LHCb::State& obj ){
  return Gaudi::LorentzVector(obj.momentum().x(), obj.momentum().y(), obj.momentum().z(), obj.p());
}
// e.g., from m_caloElectron->bremCaloMomentum()
inline Gaudi::LorentzVector momentum( LHCb::CaloMomentum obj ){
  return Gaudi::LorentzVector( obj.momentum().x(), obj.momentum().y(),
                               obj.momentum().z(), obj.momentum().e());
}


//==============================================================================
