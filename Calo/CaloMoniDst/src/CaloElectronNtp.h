/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOELECTRONNTP_H 
#define CALOELECTRONNTP_H 1

// Include files
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "Event/ODIN.h" 
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloInterfaces/ICounterLevel.h"
#include "CaloUtils/ICaloElectron.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

// List of Consumers dependencies
namespace {
  using ODIN = LHCb::ODIN;
  using Protos = LHCb::ProtoParticles;
  using Vertices = LHCb::RecVertices;
}

//==============================================================================

/** @class CaloElectronNtp CaloElectronNtp.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2009-12-11
 */
class CaloElectronNtp final
: public Gaudi::Functional::Consumer<void(const ODIN&, const Protos&, const Vertices&),
    Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>>
{
public: 
  CaloElectronNtp( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void operator()(const ODIN&, const Protos&, const Vertices&) const override;

  /// C++11 non-copyable idiom
  CaloElectronNtp() = delete;
  CaloElectronNtp( const CaloElectronNtp& ) = delete;
  CaloElectronNtp &operator=( const CaloElectronNtp& ) = delete;

private:
  DeCalorimeter* m_calo = nullptr;
  std::string m_vertLoc;

  ToolHandle<ICounterLevel>      m_counterStat  { "CounterLevel" };
  ToolHandle<ICaloElectron>      m_caloElectron { "CaloElectron"                            , this };
  ToolHandle<IEventTimeDecoder>  m_odin         { "OdinTimeDecoder/OdinDecoder"             , this };
  ToolHandle<ICaloHypo2Calo>     m_toSpd        { "CaloHypo2Calo/CaloHypo2Spd"              , this };
  ToolHandle<ICaloHypo2Calo>     m_toPrs        { "CaloHypo2Calo/CaloHypo2Prs"              , this };
  ToolHandle<ITrackExtrapolator> m_extrapolator { "TrackRungeKuttaExtrapolator/Extrapolator", this };

  Gaudi::Property<std::pair<double, double>> m_e   { this, "EFilter"  , { 0.  , 99999999}};
  Gaudi::Property<std::pair<double, double>> m_et  { this, "EtFilter" , { 200., 999999.}};
  Gaudi::Property<std::pair<double, double>> m_prs { this, "PrsFilter", { 50. , 9999999.}};
  Gaudi::Property<std::pair<double, double>> m_eop { this, "EoPFilter", { 0.  , 2.5}};
  
  Gaudi::Property<bool> m_pairing {this, "ElectronPairing", true};
  Gaudi::Property<bool> m_histo   {this, "Histo", true};
  Gaudi::Property<bool> m_tuple   {this, "Tuple", true};
  Gaudi::Property<bool> m_trend   {this, "Trend", false};
  Gaudi::Property<bool> m_usePV3D {this, "UsePV3D", false};
  Gaudi::Property<bool> m_splitFEBs{this, "splitFEBs", false};
  Gaudi::Property<bool> m_splitE  {this, "splitE", false};

  Gaudi::Property<std::vector<int>> m_tracks 
    {this, "TrackTypes", {LHCb::Track::Types::Long, LHCb::Track::Types::Downstream}};

  bool set_and_validate( const LHCb::ProtoParticle* proto, bool count=false ) const;
  double invar_mass_squared( const LHCb::Track* t1, const LHCb::Track* t2 ) const;
  void fillH(double eOp,Gaudi::LorentzVector t, LHCb::CaloCellID id,std::string hat="") const;

};

#endif // CALOELECTRONNTP_H
