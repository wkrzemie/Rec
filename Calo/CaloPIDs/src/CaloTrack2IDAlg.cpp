/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloTrack2IDAlg.h"
#include <type_traits>

// ============================================================================

DECLARE_COMPONENT( CaloTrack2IDAlg )

// ============================================================================
/// Standard protected constructor
// ============================================================================

CaloTrack2IDAlg::CaloTrack2IDAlg(const std::string &name, ISvcLocator *pSvc)
    : Transformer(name, pSvc, {KeyValue{"Inputs",""},KeyValue{"Filter",""}}, KeyValue{"Output",""}) {
  _setProperty("StatPrint", "false");
  // track types:
  _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                   LHCb::Track::Types::Long, 
                                   LHCb::Track::Types::Downstream,
                                   LHCb::Track::Types::Ttrack));

  // context-dependent default track container
  Gaudi::Functional::updateHandleLocation(*this, "Inputs", LHCb::CaloAlgUtils::TrackLocations(context()).front());
}

// ============================================================================
/// standard algorithm execution
// ============================================================================
Table CaloTrack2IDAlg::operator()(const LHCb::Tracks& tracks, const Filter& filter) const {
  static_assert(std::is_base_of<LHCb::Calo2Track::ITrEvalTable, Table>::value, "Table must inherit from ITrEvalTable");

  Table table(100);

  // loop over all tracks
  for (const auto& track : tracks) {
    // use the track ?
    if (!use(track)) {
      continue;
    } // CONTINUE
    // use filter ?
    const auto r = filter.relations(track);
    // no positive information? skip!
    if (r.empty() || !r.front().to()) {
      continue;
    } // CONTINUE
    double value = 0;
    StatusCode sc = m_tool->process(track, value);
    if (sc.isFailure()) {
      Warning(" Failure from the tool, skip the track!", sc).ignore();
      continue;
    }
    // make a relations (fast, efficient, call for i_sort is mandatory!)
    table.i_push(track, value); // NB! i_push
  }
  // MANDATORY after i_push! // NB! i_sort
  table.i_sort();

  m_nTracks += tracks.size();
  auto links = table.i_relations();
  m_nLinks  += links.size();
  //for( const auto& link : links) m_nEnergy += link.to() / Gaudi::Units::GeV;
  auto c = m_nEnergy.buffer();
  for(const auto& link : links) c += link.to() / Gaudi::Units::GeV;

  return table;
}
