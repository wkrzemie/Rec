/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOUTILS_CALO_CALOTRACKTOOL_H
#define CALOUTILS_CALO_CALOTRACKTOOL_H 1

// Include files
#include <vector>
#include <string>
#include <algorithm>
#include "GaudiAlg/GaudiTool.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "Event/Track.h"
#include "Event/TrackFunctor.h"
#include "Event/State.h"
#include "Event/TrackUse.h"
#include "CaloInterfaces/ICaloTrackMatch.h"
#include "CaloInterfaces/ICaloDigits4Track.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "CaloDet/DeCalorimeter.h"

//==============================================================================

namespace Calo
{
  /** @class Calo::CaloTrackTool CaloTrackTool.h
   *
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-05-28
   */
  class CaloTrackTool : public GaudiTool
  {
  public:
    using TrackTypes = std::vector<LHCb::Track::Types>;
    using Line = ICaloDigits4Track::Line;

    /// initialize the tool
    StatusCode initialize() override;

    // Internal version. Raise warning if failure
    void _setProperty(const std::string& p ,const std::string& v);

    /// the default constructor is disabled ;
    CaloTrackTool() = delete;

  protected:
    /// standard constructor
    CaloTrackTool
    ( const std::string& type   ,
      const std::string& name   ,
      const IInterface*  parent ) ;

    /// protected destructor
    virtual ~CaloTrackTool() {}

    inline ITrackExtrapolator* extrapolator      () const ;
    inline ITrackExtrapolator* fastExtrapolator  () const ;

    /// Propagate track to a given 3D-place
    inline StatusCode propagate
    ( const LHCb::Track&      track ,
      const Gaudi::Plane3D&   plane ,
      LHCb::State&            state ,
      const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const ;

    /// Propagate state to a given 3D-place
    inline StatusCode propagate
    ( LHCb::State&            state ,
      const Gaudi::Plane3D&   plane ,
      const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const ;

    /// Propagate state to a given Z
    inline StatusCode propagate
    ( LHCb::State&            state ,
      const double            z     ,
      const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const ;

    /// construct the straight line from the state
    inline Line line ( const LHCb::State& state ) const
    { return Line ( state.position() , state.slopes () ) ; } ;

    /** get  a pointer to the satte for the given track at given location
     *  it shodul be faster then double usage of
     *  LHCb::Track::hasStateAt ( location )  and LHCb::stateAt ( location )
     *  In addition it scans the list of states fro the end -
     *  it is good for calorimeter
     */
    inline const LHCb::State* state
    ( const LHCb::Track&          track ,
      const LHCb::State::Location loc   ) const ;

    /** get  a pointer to the satte for the given track at given location
     *  it shodul be faster then double usage of
     *  LHCb::Track::hasStateAt ( location )  and LHCb::stateAt ( location )
     *  In addition it scans the list of states fro the end -
     *  it is good for calorimeter
     */
    inline       LHCb::State* state
    ( LHCb::Track&                track ,
      const LHCb::State::Location loc   ) const ;

    /// check if the track to be used @see TrackUse
    inline bool use  ( const LHCb::Track* track ) const { return m_use( track) ; }

    /// print the short infomration about track flags
    inline MsgStream& print
    ( MsgStream&         stream ,
      const LHCb::Track* track  ) const ;
    
    /// print the short infomration about track flags
    inline MsgStream& print
    ( const LHCb::Track* track             ,
      const MSG::Level   level = MSG::INFO ) const ;

    double tolerance() const { return m_tolerance ; }
    const std::string& detectorName() const { return m_detectorName ; }
    const DeCalorimeter* calo() const { return m_calo ; }

  private:

    // extrapolator
    mutable ToolHandle<ITrackExtrapolator> m_extrapolator
      {"TrackRungeKuttaExtrapolator/Regular", this};

    // fast extrapolator
    mutable ToolHandle<ITrackExtrapolator> m_fastExtrapolator
      {"TrackLinearExtrapolator/Linear"};

    Gaudi::Property<float> m_fastZ
      {this, "zForFastExtrapolator", 
      10.0 * Gaudi::Units::meter, 
      "z-position of 'linear' extrapolation"};

    Gaudi::Property<float> m_tolerance
      {this, "Tolerance", 
      2.0 * Gaudi::Units::mm, 
      "plane extrapolation tolerance"};

    Gaudi::Property<float> m_cosTolerance
      {this, "CosTolerance", 
      ::cos( 0.1 * Gaudi::Units::mrad ), 
      "plane extrapolation angular tolerance"};

    Gaudi::Property<unsigned int> m_maxIter
      {this, "MaxPlaneIterations", 5, 
      "maximal number of iterations"};

    Gaudi::Property<std::string> m_detectorName 
      {this, "Calorimeter"};

    // detector element
    const DeCalorimeter* m_calo = nullptr;

    // track selector
    TrackUse m_use { *this };

    // local storages
    mutable Gaudi::XYZPoint  m_pos;
    mutable Gaudi::XYZVector m_mom;
  };
}

// ============================================================================
// get the extrapolator
// ============================================================================

inline ITrackExtrapolator *Calo::CaloTrackTool::extrapolator() const {
  return m_extrapolator.get();
}

// ============================================================================
// get the fast extrapolator
// ============================================================================

inline ITrackExtrapolator *Calo::CaloTrackTool::fastExtrapolator() const {
  return m_fastExtrapolator.get();
}

// ============================================================================
// Propagate track to a given 3D-place
// ============================================================================

inline StatusCode
Calo::CaloTrackTool::propagate
( const LHCb::Track&      track ,
  const Gaudi::Plane3D&   plane ,
  LHCb::State&            state ,
  const LHCb::Tr::PID pid   ) const
{
  state = track.closestState ( plane ) ;
  if ( ::fabs( plane.Distance ( state.position() ) ) < tolerance() )
  { return StatusCode::SUCCESS ; }
  return propagate ( state , plane , pid ) ;
}

// ============================================================================
// Propagate state to a given 3D-place
// ============================================================================

inline StatusCode
Calo::CaloTrackTool::propagate
( LHCb::State&            state ,
  const Gaudi::Plane3D&   plane ,
  const LHCb::Tr::PID pid   ) const
{
  // check the plane: if it is "almost Z=const"-plane
  const Gaudi::XYZVector& normal = plane.Normal() ;
  if ( m_cosTolerance < ::fabs ( normal.Z() ) )
  {
    // use the standard method
    const double Z =  -1*plane.HesseDistance()/normal.Z() ;
    return propagate (  state , Z , pid ) ;
  }
  Gaudi::XYZPoint point ;
  for ( unsigned int iter = 0 ; iter < m_maxIter ; ++iter )
  {
    const double distance = ::fabs ( plane.Distance( state.position() ) );
    if ( distance <m_tolerance ) { return StatusCode::SUCCESS ; }   // RETURN
    double mu = 0.0 ;
    if ( !Gaudi::Math::intersection ( line ( state ) , plane , point , mu ) )
    { return Warning ( "propagate: line does not cross the place" ) ; }// RETURN
    StatusCode sc = propagate ( state , point.Z() , pid ) ;
    if ( sc.isFailure() )
    { return Warning ( "propagate: failure from propagate" , sc  ) ; } // RETURN
  }
  return Warning ( "propagate: no convergency has been reached" ) ;
}

// ============================================================================
// Propagate state to a given Z
// ============================================================================

inline StatusCode
Calo::CaloTrackTool::propagate
( LHCb::State&            state ,
  const double            z     ,
  const LHCb::Tr::PID pid   ) const
{
  if      ( std::max ( state.z() , z ) <  m_fastZ )
  { // use the regular extrapolator
    return extrapolator     () -> propagate ( state , z , pid ) ;
  }
  else if ( std::min ( state.z() , z ) > m_fastZ  )
  { // use the fast (linear) extrapolator
    return fastExtrapolator () -> propagate ( state , z , pid ) ;
  }
  // use the pair of extrapolators
  StatusCode sc1 ;
  StatusCode sc2 ;
  if ( state.z () < z )
  {
    sc1 = extrapolator     () -> propagate ( state  , m_fastZ , pid ) ;
    sc2 = fastExtrapolator () -> propagate ( state  , z       , pid ) ;
  }
  else
  {
    sc2 = fastExtrapolator () -> propagate ( state  , m_fastZ , pid ) ;
    sc1 = extrapolator     () -> propagate ( state  , z       , pid ) ;
  }

  StatusCode sc = StatusCode::SUCCESS;
  std::string errMsg;
  if ( sc2.isFailure() )
  {
    errMsg = "Error from FastExtrapolator";
    sc = sc2;
  }
  if ( sc1.isFailure() )
  {
    errMsg = "Error from extrapolator";
    sc = sc1;
  }
  //
  return  sc.isFailure() ? Warning( errMsg, sc ) : sc;
}

// ============================================================================
// print the short infomration about track flags
// ============================================================================

inline MsgStream&
Calo::CaloTrackTool::print
( MsgStream&         stream ,
  const LHCb::Track* track  ) const
{ return stream.isActive() ? m_use.print ( stream , track ) : stream ; }

// ============================================================================
// print the short infomration about track flags
// ============================================================================

inline MsgStream&
Calo::CaloTrackTool::print
( const LHCb::Track* track ,
  const MSG::Level   level ) const
{ return print ( msgStream ( level ) , track ) ; }

// ============================================================================
// get  a pointer to the state for the given track at given location
// ============================================================================

inline const LHCb::State*
Calo::CaloTrackTool::state
( const LHCb::Track& track , const LHCb::State::Location loc   ) const
{
  const auto& states = track.states()  ;
  // loop in reverse order: for calo should be a bit more efficient
  auto found = std::find_if( states.rbegin(), states.rend(),
                             [&](const LHCb::State* s)
                             { return s->checkLocation(loc); } );
  //
  return found != states.rend() ? *found : nullptr ;           // RETURN
}

// ============================================================================
#endif // CALOUTILS_CALO_CALOTRACKTOOL_H
// ============================================================================
