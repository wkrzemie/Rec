/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOENERGYFORTRACK_H
#define CALOENERGYFORTRACK_H 1

// Include files
#include <set>
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IIncidentListener.h"
#include "CaloInterfaces/ICaloTrackIdEval.h"
#include "CaloInterfaces/ICaloDigits4Track.h"
#include "Event/Track.h"
#include "Event/CaloDigit.h"
#include "CaloTrackTool.h"

// ============================================================================
/** @class CaloEnergyForTrack
 *  tool which accumulates the energy for the given track
 *  along the track line
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
class CaloEnergyForTrack : public virtual ICaloTrackIdEval,
                           public virtual ICaloDigits4Track,
                           public virtual IIncidentListener,
                           public Calo::CaloTrackTool {
 public:
  typedef LHCb::CaloDigit::Container Digits;
  typedef ICaloDigits4Track::Line Line;

  /// Inform that a new incident has occured @see IIncidentListener
  void handle(const Incident&) override { m_digits = 0; }

  StatusCode initialize() override;

  /** The main processing method
   *  It evaluated the Track ID estimators using the calorimeter information
   *  @param  track  pointer to the object to be processed
   *  @param  value  (return) the value of the estimator
   *  @return status code
   */
  StatusCode process(const LHCb::Track* track, double& value) const override;

  /** The main processing method (functor interface)
   *  It evaluated the Track ID estimators using the calorimeter information
   *  @param  track  pointer to the object to be processed
   */
  double operator()(const LHCb::Track* track) const override;

  /** collect the cellID-s along the line
   *  @param line   (INPUT)  the line
   *  @param cells  (OUTPUT) the container of cells
   *  @return status code
   */
  StatusCode collect(const Line& line,
                     LHCb::CaloCellID::Vector& cells) const override;

  /** collect the cellID-s along the line
   *  @param line   (INPUT)  the line
   *  @param cells  (OUTPUT) the container of cells
   *  @return status code
   */
  StatusCode collect(const Line& line,
                     LHCb::CaloCellID::Set& cells) const override;

  /** collect the cellID-s along the path of the tracks
   *  @param track  (INPUT)  the track
   *  @param cells  (OUTPUT) the container of cells
   *  @return status code
   */
  StatusCode collect(const LHCb::Track* track,
                     LHCb::CaloCellID::Vector& cells) const override;

  /** collect the cellID-s along the path of the tracks
   *  @param track  (INPUT)  the track
   *  @param cells  (OUTPUT) the container of cells
   *  @return status code
   */
  StatusCode collect(const LHCb::Track* track,
                     LHCb::CaloCellID::Set& cells) const override;

  /** collect the fired digits along the line
   *  @param line   (INPUT)  the line
   *  @param digits (OUTPUT) the container of digits
   *  @return status code
   */
  StatusCode collect(const Line& line,
                     LHCb::CaloDigit::Vector& digits) const override;

  /** collect the fired digits along the path of the tracks
   *  @param line   (INPUT)  the line
   *  @param digits (OUTPUT) the container of digits
   *  @return status code
   */
  StatusCode collect(const Line& line,
                     LHCb::CaloDigit::Set& digits) const override;

  /** collect the fired digits along the path of the tracks
   *  @param track  (INPUT)  the track
   *  @param digits (OUTPUT) the container of digits
   *  @return status code
   */
  StatusCode collect(const LHCb::Track* track,
                     LHCb::CaloDigit::Vector& digits) const override;

  /** collect the fired digits along the path of the tracks
   *  @param track  (INPUT)  the track
   *  @param digits (OUTPUT) the container of digits
   *  @return status code
   */
  StatusCode collect(const LHCb::Track* track,
                     LHCb::CaloDigit::Set& digits) const override;

  CaloEnergyForTrack(const std::string& type, const std::string& name,
                     const IInterface* parent);

 protected:
  // get the data form TES
  inline const Digits* digits() const;

  typedef std::vector<Gaudi::Plane3D> Planes;
  typedef std::vector<Gaudi::XYZPoint> Points;
  typedef std::vector<LHCb::State*> States;
  typedef LHCb::CaloCellID::Set Cells;

 private:
  mutable const Digits* m_digits = nullptr;
  mutable LHCb::State m_state;

  Gaudi::Property<std::string> m_address
    {this, "DataAddress", "", "TES-location of corresponding CaloDigits"};

  Gaudi::Property<double> m_bad
    {this, "BadValue", 1e+10, "The bad value to be returned"};

  LHCb::State::Location       m_location = LHCb::State::Location::Spd;
  Planes                      m_planes;
  Gaudi::Property<unsigned short> m_morePlanes   {this, "MorePlanes", 0};
  Gaudi::Property<unsigned short> m_addNeighbors {this, "AddNeighbours", 0};

};

// ============================================================================
// get the data form TES
// ============================================================================

inline const CaloEnergyForTrack::Digits* CaloEnergyForTrack::digits() const {
  if (m_digits == nullptr) {
    m_digits = get<Digits>(m_address);
  }
  return m_digits;
}

// ============================================================================
#endif  // CALOENERGYFORTRACK_H
// ============================================================================
