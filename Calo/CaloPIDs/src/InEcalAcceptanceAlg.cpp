/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "InCaloAcceptanceAlg.h"

// ============================================================================
/** @class InEcalAcceptanceAlg InEcalAcceptanceAlg.cpp
 *  the preconfigured instance of InCaloAcceptanceAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

struct InEcalAcceptanceAlg final : InCaloAcceptanceAlg {
  /// Standard constructor
  InEcalAcceptanceAlg(const std::string& name, ISvcLocator* pSvc)
      : InCaloAcceptanceAlg(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    Gaudi::Functional::updateHandleLocation(
        *this, "Output", CaloIdLocation("InEcal", context()));

    _setProperty("Tool", "InEcalAcceptance/InEcal");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
  }
};

// ============================================================================

DECLARE_COMPONENT( InEcalAcceptanceAlg )

// ============================================================================
