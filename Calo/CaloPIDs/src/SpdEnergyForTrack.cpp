/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloEnergyForTrack.h"

// ============================================================================
/** @class SpdEnergyForTrack
 *  The concrete preconfigured insatnce for CaloEnergyForTrack tool
 *  along the track line
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

class SpdEnergyForTrack final : public CaloEnergyForTrack {
 public:
  SpdEnergyForTrack(const std::string& type, const std::string& name,
                    const IInterface* parent)
      : CaloEnergyForTrack(type, name, parent) {
    _setProperty("DataAddress", LHCb::CaloDigitLocation::Spd);
    _setProperty("Tolerance", "2");  /// 2 * Gaudi::Units::mm
    _setProperty("Calorimeter", DeCalorimeterLocation::Spd);
  };
};

// ============================================================================

DECLARE_COMPONENT( SpdEnergyForTrack )
