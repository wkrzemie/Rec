/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloTrackTool.h"

// ============================================================================
/** @file
 *  Implementation file for class Calo::CaloTrackTool
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-05-28
 */
// ============================================================================
/// standard constructor
// ============================================================================
Calo::CaloTrackTool::CaloTrackTool( const std::string& type   , // ?
                                    const std::string& name   ,
                                    const IInterface*  parent )
  : GaudiTool ( type , name , parent )
{
  _setProperty("CheckTracks", "false");
  declareProperty("Extrapolator", m_extrapolator);
}

//==============================================================================
/// initialize the tool
//==============================================================================

StatusCode Calo::CaloTrackTool::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return sc ; }
  //
  if ( propsPrint() || msgLevel ( MSG::DEBUG ) || m_use.check() )
  { info () << m_use << endmsg ; } ;
  //
  if ( !m_detectorName.empty() )
  { m_calo = getDet<DeCalorimeter> ( detectorName()  ); }
  else { Warning("empty detector name!"); }

  // Retrieve tools
  sc = m_extrapolator.retrieve();
  if (sc.isFailure()) {return sc;}
  sc = m_fastExtrapolator.retrieve();
  if (sc.isFailure()) {return sc;}

  return StatusCode::SUCCESS ;
}

//==============================================================================

void Calo::CaloTrackTool::_setProperty(const std::string &p,
                                       const std::string &v) {
  StatusCode sc = setProperty(p, v);
  if (!sc) {
    warning() << " setting Property " << p << " to " << v << " FAILED"
              << endmsg;
  }
}
