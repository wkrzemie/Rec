/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOPIDS_INCALOACCEPTANCE_H
#define CALOPIDS_INCALOACCEPTANCE_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"
#include "CaloTrackTool.h"
#include "TrackInterfaces/IInAcceptance.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

// ============================================================================

/** @class InCaloAcceptance InCaloAcceptance.h
 *
 *  The general tool to determine if the reconstructed charged track
 *  "is in Calorimeter acceptance"
 *
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
class InCaloAcceptance : public virtual IInAcceptance,
                         public Calo::CaloTrackTool {
 public:
  /// initialization @see IAlgTool
  StatusCode initialize() override;

  /** check the track is in acceptance of given calorimeter
   *  @see IInAcceptance
   *  @param  track track to be checked
   *  @return true if the track is in acceptance
   */
  bool inAcceptance(const LHCb::Track* track) const override;

 protected:
  /// check if the point "is in acceptance"
  inline bool ok(const Gaudi::XYZPoint& point) const;
  /// get the plane
  const Gaudi::Plane3D& plane() const { return m_plane; }

 public:
  InCaloAcceptance(const std::string& type, const std::string& name,
                   const IInterface* parent);

private:
  /// use fiducial volume or real volume ?
  Gaudi::Property<bool> m_fiducial {this, "UseFiducial", true};
  LHCb::State::Location m_loc   ;
  ///
  Gaudi::Plane3D m_plane;
  /// Local storage
  mutable LHCb::State m_state;
};

// ============================================================================
/// check if the point "is in acceptance"
// ============================================================================

inline bool InCaloAcceptance::ok(const Gaudi::XYZPoint& point) const {
  const CellParam* cell = calo()->Cell_(point);
  if (cell==nullptr || !cell->valid()) {
    return false;
  }
  // check for fiducial volume ?
  if (!m_fiducial) {
    return true;
  }
  // check for neighbors
  const CaloNeighbors& neighbors = cell->neighbors();
  // regular cell: >= 8 valid neighbours
  if (8 <= neighbors.size()) {
    return true;
  }
  // incomplete neibors: border of 2 zones ?
  const unsigned int area = cell->cellID().area();
  for (auto inei = neighbors.begin() + 1; neighbors.end() != inei; ++inei) {
    if (area != inei->area()) {
      return true;
    }
  }
  // finally
  return false;
}

// ===========================================================================
#endif  // CALOPIDS_INCALOACCEPTANCE_H
// ============================================================================
