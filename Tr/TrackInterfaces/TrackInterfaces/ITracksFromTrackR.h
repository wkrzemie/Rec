/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_ITRACKSFROMTRACKR_H
#define TRACKINTERFACES_ITRACKSFROMTRACKR_H 1

// Include files
// from STL
#include <optional>
#include <vector>
#include <any>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

#include "Event/Track.h"

/** @class ITracksFromTrackR ITracksFromTrackR.h TrackInterfaces/ITracksFromTrackR.h
 *  Interface to the forward pattern tool, reentrant version
 */
struct ITracksFromTrackR : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( ITracksFromTrackR, 2, 0 );

  /// Create an instance of a state
  virtual std::any createState() const = 0 ;

  /// Take an existing track and make new tracks from it (usually with hits from more detectors)
  virtual StatusCode tracksFromTrack( const LHCb::Track& seed,
                                      std::optional<LHCb::Track>& track,
                                      ranges::v3::any& state) const  = 0;

};
#endif // TRACKINTERFACES_ITRACKSFROMTRACKR_H
