/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _IGhostProbability_H
#define _IGhostProbability_H

#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"


/** @class IGhostProbability
 *
 *  interface for the ghost probability calculation
 *
 *  @author P.Seyfert
 *  @date   27/01/2015
 */


struct IGhostProbability: extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IGhostProbability, 1, 0 );

  /** Add the reference information */
  virtual StatusCode execute(LHCb::Track& aTrack) const = 0;

  /** consider this the beginning of a new event */
  virtual StatusCode beginEvent() = 0;

  /** reveal the variable names for a track type */
  virtual std::vector<std::string> variableNames(LHCb::Track::Types type) const = 0;

  /** reveal the variable values for a track */
  virtual std::vector<float> netInputs(LHCb::Track& aTrack) const = 0;
};

#endif
