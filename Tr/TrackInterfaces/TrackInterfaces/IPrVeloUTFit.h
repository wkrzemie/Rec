/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_IPRVELOUTFIT_H
#define TRACKINTERFACES_IPRVELOUTFIT_H 1

#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"


/** @class IPrVeloUTFit IPrVeloUTFit.h
 *
 * provide a convenient interface to the internal fit used in the PrVeloUTFit
 * algorithm in the pattern reco
 *
 * @author Pavel Krokovny <krokovny@physi.uni-heidelberg.de>
 * @date   2009-03-09
 */
struct IPrVeloUTFit : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID(IPrVeloUTFit, 2, 0);

  virtual StatusCode fitVUT( LHCb::Track& track) const = 0;

};
#endif // INCLUDE_IPRVELOUTFIT_H
