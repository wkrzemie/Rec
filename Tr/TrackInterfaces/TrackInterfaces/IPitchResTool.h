/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_IPITCHRESTOOL_H
#define TRACKINTERFACES_IPITCHRESTOOL_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"

// Forward declarations
namespace LHCb {
  class OTChannelID;
}


/** @class IPitchResTool IPitchResTool.h
 *
 *  Interface for the pitch residual  tool for the OT
 *
 *  @author Johan Blouw
 *  @author Manuel Tobias Schiller
 *  @date   2009-09-04
 */
struct IPitchResTool : extend_interfaces<IAlgTool> {
  DeclareInterfaceID(IPitchResTool, 2, 0 );

  // calculate the pitch residuals for each of the OT layers.
  // It is based on the fact that each OT layer actually contains
  // 2 monolayers at a known and well-defined position from each other.
  // Using the track slope, one can calculate, independent from e.g.
  // misalignments, a residual. The 2nd mono-layer is staggered with
  // 0.5 pitch wrt the first monolayer. That means that the sum of the
  // 2 residuals is bound by the distance between the two wires projected
  // onto the track.
  virtual std::vector<std::pair<LHCb::OTChannelID, double> >
    calcPitchResiduals(const LHCb::Track* track) const = 0;

};

//==============================================================================
//   end of class
//==============================================================================

#endif // TRACKINTERFACES_IPITCHRESTOOL_H
