/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_IMEASUREMENTPROVIDER_H
#define TRACKINTERFACES_IMEASUREMENTPROVIDER_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include <vector>

#include "Kernel/STLExtensions.h"

#include "TrackKernel/ZTrajectory.h"
#include "Event/Track.h"

// Forward declarations
namespace LHCb {
 class LHCbID;
 class Measurement;
 class StateVector;
}


/** @class IMeasurementProvider IMeasurementProvider.h TrackInterfaces/IMeasurementProvider.h
 *
 *  Interface for the measurement provider tool
 *
 *  @author Jose Hernando
 *  @author Eduardo Rodrigues
 *  @date   2005-06-28
 */
struct IMeasurementProvider : extend_interfaces<IAlgTool> {

  DeclareInterfaceID(  IMeasurementProvider, 3, 0 );

  /** Load (=create) all the Measurements from the list of LHCbIDs
   *  on the input Track
   */
  virtual StatusCode load( LHCb::Track& track ) const = 0;

  /** Construct a Measurement of the type of the input LHCbID
   *  Note: this method is not for general use. A user should preferably call
   *  the "load( Track& track )" method to load=create "in one go" all the
   *  Measurements from the list of LHCbIDs on the Track.
   *  This method is in fact called internally by "load( Track& track )".
   *  @return Pointer the the Measurement created
   *  @param  id:  input LHCbID
   *  @param  localY: creates y trajectory for muon, if true
   */
  virtual LHCb::Measurement* measurement( const LHCb::LHCbID& id,
					  bool localY = false ) const = 0;

  /** Construct a measurement with a statevector. This takes care that
      things like errors depending on track angle are correctly
      set. */

  /** Construct a measurement with a reference trajectory. This takes care that
      things like errors depending on track angle are correctly set. */
  virtual LHCb::Measurement* measurement( const LHCb::LHCbID& id,
                                          const LHCb::ZTrajectory<double>& reftraj,
                                          bool localY = false ) const = 0;

  /** create measurements for a set of LHCbIDs **/
  virtual void addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                                  std::vector<LHCb::Measurement*>&,
                                  const LHCb::ZTrajectory<double>& ) const = 0 ;
};
#endif // TRACKINTERFACES_IMEASUREMENTPROVIDER_H
