/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _IVPExpectation_H
#define _IVPExpectation_H

#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"


/** @class IVPExpectation IVPExpectation.h TrackInterfaces/IVPExpectation.h
 *
 *  interface for selecting tracks....
 *
 *  @author M.Needham
 *  @date   11/03/2007
 */


struct IVPExpectation: extend_interfaces<IAlgTool> {

   DeclareInterfaceID( IVPExpectation, 1, 0 );

  /** Helper struct
  * n - Number of hits
  */
  struct Info final {
    unsigned int n;
    double firstR;
    double firstX;
    double firstY;
    double firstZ;
    std::vector<double> expectedZ;
  };

  /** Returns number of hits expected, from zFirst to endVelo
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return number of hits expected
   */
  virtual int nExpected ( const LHCb::Track& aTrack ) const = 0;

  /** Returns info on expected hits
   *
   *  @param aTrack Reference to the Track to test
   *  @return Info
   */
  virtual IVPExpectation::Info expectedInfo ( const LHCb::Track& aTrack ) const = 0;

  /** Returns number of hits expected, from zStart to zStop
   *
   *  @param aTrack Reference to the Track to test
   *  @param zStart --> start of scan range
   *  @param zStop --> end of scan range
   *  @return Info
   */
  virtual int nExpected ( const LHCb::Track& aTrack , const double zStart, const double zStop) const = 0;

  /** Returns expected hits info, from zStart to zStop
   *
   *  @param aTrack Reference to the Track to test
   *  @param zStart --> start of scan range
   *  @param zStop --> end of scan range
   *  @return Info
   */
  virtual IVPExpectation::Info expectedInfo ( const LHCb::Track& aTrack , const double zStart, const double zStop) const = 0;

  /** Returns number of hits missed, from zBeamLine to firstHit
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return number of hits missed before first hit
   */
  virtual int nMissed( const LHCb::Track& aTrack ) const = 0;

  /** Returns number of hits missed, from z to firstHit
   *
   *  @param aTrack Reference to the Track to test
   *  @param z --> z to start from
   *
   *  @return number of hits missed before first hit
   */
  virtual int nMissed( const LHCb::Track& aTrack, const double z) const = 0;

  /** Returns true if track passses through a working strip in the sensor
   *
   *
   *  @param aTrack Reference to the Track to test
   *  @param sensorNum ---> sensor number
   *
   *  @return true if crosses a working strip
   */
  virtual bool isInside(const LHCb::Track& aTrack,
                        const unsigned int sensorNum) const = 0;
};

#endif
