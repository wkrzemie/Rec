/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_ITRAJECTORYPROVIDER_H
#define TRACKINTERFACES_ITRAJECTORYPROVIDER_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// from TrackEvent
#include "Event/TrackTypes.h"

#include <memory>

// Forward declarations
namespace LHCb {
  class LHCbID;
  class Measurement;
  class State;
}


/** @class ITrajectoryProvider ITrajectoryProvider.h TrackInterfaces/ITrajectoryProvider.h
 *
 *  Interface for the trajectory provider tool
 *
 *  @author Eduardo Rodrigues
 *  @date   2006-02-17
 */
struct ITrajectoryProvider : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( ITrajectoryProvider, 2, 0 );

  /// Return a "Measurement Trajectory" from a Measurement
  virtual const LHCb::Trajectory<double>* trajectory( const LHCb::Measurement& meas ) const = 0;

  /** Return a "Measurement Trajectory" from an LHCbID
   *  Note: the meaning of the offset input depends on the sub-detector type
   *  @return Pointer to the Trajectory created
   *  @param  id:     input LHCbID
   *  @param  offset: input offset
   */
  virtual std::unique_ptr<LHCb::Trajectory<double>> trajectory( const LHCb::LHCbID& id,
                                                                double offset = 0 ) const = 0;
  /// Return a "State Trajectory" from a State
  virtual std::unique_ptr<LHCb::Trajectory<double>> trajectory( const LHCb::State& state ) const = 0;

  /// Return a "State Trajectory" from a State vector and a z-position
  virtual std::unique_ptr<LHCb::Trajectory<double>> trajectory( const Gaudi::TrackVector& stateVector,
                                                                double z ) const = 0;

};
#endif // TRACKINTERFACES_ITRAJECTORYPROVIDER_H
