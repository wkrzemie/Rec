/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKFITEVENT_FITNODE_H
#define TRACKFITEVENT_FITNODE_H 1

// from TrackEvent
#include "Event/State.h"
#include "Event/Measurement.h"
#include "Event/ChiSquare.h"
#include "LHCbMath/ValueWithError.h"

// From LHCbMath
#include "LHCbMath/MatrixManip.h"

namespace LHCb {

  /**
   *  This File contains the declaration of the SebFitNode.
   *
   *  A SebFitNode is a basket of objects at a given z position.
   *  The information inside the SebFitNode has to be sufficient
   *  to allow smoothing and refitting.
   */

  class SebFitNode final {
  public:

    /// enumerator for the type of Node
    enum Type { Unknown, HitOnTrack, Outlier, Reference };

    // important note: for the Forward fit, smoothed means
    // 'classical'. for the backward fit, it means 'bidirectional'.
    enum Direction {Forward=0, Backward=1} ;

    /// Constructor from a z position and a location
    SebFitNode(double zPos, LHCb::State::Location location = LHCb::State::Location::LocationUnknown);

    /// Constructor from a Measurement
    SebFitNode(Measurement& meas);

    /// set transport matrix
    void setTransportMatrix(const Gaudi::TrackMatrix& transportMatrix);

    /// set transport vector
    void setTransportVector(const Gaudi::TrackVector& transportVector) {
      m_transportVector = transportVector;
    }

    /// set noise matrix
    void setNoiseMatrix(const Gaudi::TrackSymMatrix& noiseMatrix) {
      m_noiseMatrix = noiseMatrix;
    }

    /// Update the reference vector
    void setRefVector(const Gaudi::TrackVector& value) {
      m_refVector.parameters() = value;
    }

    /// Sets the reference vector
    void setRefVector(const LHCb::StateVector& value) { m_refVector = value; };

    /// Retrieve the state
    const LHCb::State& state() const { return m_state; };

    /// retrieve the filtered state
    const LHCb::State& filteredState(int direction) const {
      return m_filteredState[direction];
    }

    /// Retrieve the residual
    double residual() const;

    /// Retrieve the error in the residual
    double errResidual() const;

     /// retrieve the total chi2 of the filter including this node
    const LHCb::ChiSquare& totalChi2(int direction) const { return m_totalChi2[direction%2]; }

    /// Update  Signed doca (of ref-traj). For ST/velo this is equal to minus (ref)residual
    void setDoca(double value) { m_doca = value; }

    /// Update  Unit vector perpendicular to state and measurement
    void setPocaVector(const Gaudi::XYZVector& value) { m_pocaVector = value; }

    /// Retrieve the reference to the measurement
    Measurement & measurement() const { return *m_measurement; }

    /// Set the measurement
    void setMeasurement(LHCb::Measurement& meas);

    /// Return the measure error squared
    double errMeasure2() const {
      return m_errMeasure2;
    }

    /// set the measure error squared
    void setErrMeasure2(double value) {
      m_errMeasure2 = value;
    }

    /// Return true if this Node has a valid pointer to measurement
    bool hasMeasurement() const { return (m_measurement != 0); }

    /// Remove measurement from the node
    void removeMeasurement();

    /// z position of Node
    double z() const { return m_refVector.z(); }

    /// Position of the State on the Node
    Gaudi::XYZPoint position() const;

    /// Set the location of the state in this node.
    void setLocation(LHCb::State::Location& location);

    /// Retrieve const  type of node
    const Type& type() const { return m_type; };

    /// Update  type of node
    void setType(const Type& value) { m_type = value; };

    /// Retrieve const  the reference vector
    const LHCb::StateVector& refVector() const { return m_refVector; };

    /// Retrieve const  the projection matrix
    const Gaudi::TrackProjectionMatrix& projectionMatrix() const {
      return m_projectionMatrix;
    };

    /// set the seed matrix
    void setSeedCovariance(const Gaudi::TrackSymMatrix& cov )
    {
      m_predictedState[Forward].covariance() = cov ;
      m_predictedState[Backward].covariance() = cov ;
    }

    /// update the projection
    void updateProjection( const Gaudi::TrackProjectionMatrix& H,
			   double refresidual) {
      m_projectionMatrix = H;
      m_refResidual = refresidual;
      m_residual = 0;
      m_errResidual = 0;
    }

    double chi2Seb() const {
      return m_residual*m_residual/(m_errResidual*m_errResidual);
    }

    /// set the delta-energy
    void setDeltaEnergy( double e) { m_deltaEnergy = e; }

    /// get the delta-energy
    double deltaEnergy() const { return m_deltaEnergy; }

    /// update node residual using a smoothed state
    Gaudi::Math::ValueWithError computeResidual(const LHCb::State& state, bool biased) const ;

    void computePredictedStateSeb( int direction , SebFitNode* prevNode, bool hasInfoUpstream) ;
    void computeFilteredStateSeb( int direction, SebFitNode* prevNode, int nTrackParam ) ;
    void computeBiSmoothedStateSeb(bool hasInfoUpstreamForward, bool hasInfoUpstreamBackWard) ;

  private:

    // ! check that the contents of the cov matrix are fine
    bool isPositiveDiagonal( const Gaudi::TrackSymMatrix& mat ) const;

    /// update node residual using a smoothed state
    void updateResidual(const LHCb::State& state) ;

    Type m_type;                      ///< type of node
    LHCb::State m_state;              ///< state
    LHCb::StateVector m_refVector;    ///< the reference vector
    LHCb::Measurement* m_measurement; ///< pointer to the measurement (not owner)
    double m_residual {0.0};          ///< the residual value
    double m_errResidual {0.0};       ///< the residual error
    double m_errMeasure2 {0.0};       ///< square root of the measurement error
    Gaudi::TrackProjectionMatrix m_projectionMatrix; ///< the projection matrix
    Gaudi::TrackMatrix    m_transportMatrix;       ///< transport matrix for propagation from previous node to this one
    Gaudi::TrackMatrix    m_invertTransportMatrix; ///< transport matrix for propagation from this node to the previous one
    Gaudi::TrackVector    m_transportVector;       ///< transport vector for propagation from previous node to this one
    Gaudi::TrackSymMatrix m_noiseMatrix;           ///< noise in propagation from previous node to this one
    double                m_deltaEnergy = 0;       ///< change in energy in propagation from previous node to this one
    double                m_refResidual = 0;       ///< residual of the reference
    State                 m_predictedState[2];     ///< predicted state of forward/backward filter
    State                 m_filteredState[2];      ///< filtered state of forward/backward filter
    LHCb::ChiSquare       m_totalChi2[2];          ///< total chi2 after this filterstep
    Gaudi::TrackMatrix    m_smootherGainMatrix ;   ///< smoother gain matrix (smoothedfit only)
    double                m_doca;             ///< Signed doca (of ref-traj). For ST/velo this is equal to minus (ref)residual
    Gaudi::XYZVector      m_pocaVector;       ///< Unit vector perpendicular to state and measurement

  };

} // namespace LHCb

#endif // TRACKFITEVENT_FITNODE_H

