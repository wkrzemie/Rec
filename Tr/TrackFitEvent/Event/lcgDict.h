/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: lcgDict.h,v 1.4 2009-07-08 13:43:01 wouter Exp $
#ifndef TRACKFITEVENT_LCGDICT_H
#define TRACKFITEVENT_LCGDICT_ 1

// Additional classes to be added to automatically generated lcgdict

// begin include files
#include "Event/FitNode.h"

// end include files

namespace {
  struct _Instantiations {
    // begin instantiations
    // end instantiations
  };
}

#endif // TRACKFITEVENT_LCGDICT_H
