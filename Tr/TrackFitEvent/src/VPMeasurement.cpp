/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "VPDet/DeVP.h"
#include "Kernel/LineTraj.h"

#include "Event/VPMeasurement.h"

using namespace LHCb;

//=============================================================================
// Constructor
//=============================================================================
VPMeasurement::VPMeasurement(const VPLightCluster& cluster,
                             const VPPositionInfo& info,
                             const VPMeasurement::VPMeasurementType& xy) :
    Measurement(Measurement::Type::VP, cluster.channelID(), 0),
    m_projection(xy),
    m_cluster(&cluster) {

  Gaudi::XYZPoint position(info.x, info.y, m_cluster->z());
  setZ(m_cluster->z());
  if (m_projection == VPMeasurementType::X) {
    setMeasure(info.x);
    setErrMeasure(info.dx);
    m_trajectory = std::make_unique<LineTraj<double>>(position, Trajectory<double>::Vector{0,1,0},
                                                      Trajectory<double>::Range{-info.dy, info.dy},
                                                      Trajectory<double>::DirNormalized{true});
  } else {
    setMeasure(info.y);
    setErrMeasure(info.dy);
    m_trajectory = std::make_unique<LineTraj<double>>(position, Trajectory<double>::Vector{1,0,0},
                                                      Trajectory<double>::Range{-info.dx, info.dx},
                                                      Trajectory<double>::DirNormalized{true});
  }

}

//=============================================================================
// Copy constructor
//=============================================================================
VPMeasurement::VPMeasurement(const VPMeasurement& other) :
    Measurement(other),
    m_projection(other.m_projection),
    m_cluster(other.m_cluster) {

}

