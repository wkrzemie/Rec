/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from FTDet
#include "FTDet/DeFTDetector.h"

// from Event
#include "Event/FTCluster.h"

// local
#include "Event/FTMeasurement.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : FTMeasurement
//
//  2012-11-27 Olivier Callot, from STMeasurement
//-----------------------------------------------------------------------------
void FTMeasurement::init( const DeFTDetector& geom ) {

  const DeFTMat* ftMat = geom.findMat( m_cluster.channelID() );
  m_detectorElement = ftMat;
  m_size = m_cluster.pseudoSize();
  m_errMeasure =  0.04  + 0.01 * m_size ; //need a better parametrization 
  m_trajectory = ftMat->trajectory(m_cluster.channelID(), m_cluster.fraction());
  m_z = ftMat->globalZ();
  m_measure = 0. ; // JvT: I believe this is purely historical. Should remove it?

}

