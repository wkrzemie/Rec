<?xml version='1.0' encoding='UTF-8'?>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!DOCTYPE gdd SYSTEM 'gdd.dtd'>
<gdd>
  <package name='TrackFitEvent'>

<!-- ============== VeloPhiMeasurement class definition ============== -->

    <class
      name        = 'VeloPhiMeasurement'
      author      = 'Jose Hernando, Eduardo Rodrigues'
      desc        = 'VeloPhiMeasurement is a measurement made from a VeloPhiCluster.'
      final       = 'TRUE'
      defaultconstructor ='FALSE'
      defaultdestructor = 'FALSE'
      serializers = 'FALSE'>

      <base name='LHCb::VeloMeasurement' />
      <import name='Event/VeloMeasurement' />
      <import name='TrackInterfaces/IVeloClusterPosition'/>
      <import name='StateVector' soft='FORWARDONLY'/>
      <import name='Event/VeloCluster' />
      <import name='DeVelo' namespace='::' soft='FORWARDONLY'/>
      <import name = "DeVeloPhiType" namespace = "::" soft= "FORWARDONLY"/>

      <constructor
        desc = 'Constructor from an VeloPhiCluster and a refVector'>
        <arg type = 'VeloCluster' name = 'aCluster' const='TRUE' />
        <arg type = 'DeVelo' name = 'det' const='TRUE' />
        <arg type = 'IVeloClusterPosition' name = 'clusPosTool' const='TRUE' />
        <arg type = 'LHCb::StateVector' name='refVector' const='TRUE'/>
      </constructor>

      <constructor
        desc = 'Constructor from an VeloPhiCluster without a refVector'>
        <arg type = 'VeloCluster' name = 'aCluster' const='TRUE' />
        <arg type = 'DeVelo' name = 'det' const='TRUE' />
        <arg type = 'IVeloClusterPosition' name = 'clusPosTool' const='TRUE' />
      </constructor>


      <attribute
        type = 'Gaudi::XYZPoint'
        name = 'origin'
        desc = 'Origin of the sensor'
        access = 'PRIVATE'
	getMeth = 'FALSE'
	setMeth = 'FALSE' />

      <method
        name   = 'init'
        desc   = 'Initialize the data members'
        access = 'PUBLIC'>
        <arg type = 'DeVelo' name = 'det' const='TRUE' />
        <arg type = 'IVeloClusterPosition::toolInfo' name = 'clusinfo'
	  const='TRUE' />
      </method>

      <method
        type    = 'double'
        name    = 'resolution'
        argList = 'Gaudi::XYZPoint /*point*/, Gaudi::XYZVector /*vec*/'
        desc    = 'Retrieve the measurement error'
        const   = 'TRUE'
        override = 'TRUE'>
      </method>

      <method
        type    = 'double'
        name    = 'resolution2'
        argList = 'Gaudi::XYZPoint /*point*/, Gaudi::XYZVector /*vec*/'
        desc    = 'Retrieve the measurement error squared'
        const   = 'TRUE'
        override = 'TRUE'>
      </method>

      <method
        type    = 'LHCb::VeloPhiMeasurement*'
        name    = 'clone'
        desc    = 'Clone the VeloPhiMeasurement'
        const   = 'TRUE'
        override = 'TRUE'>
        <code>
          return new LHCb::VeloPhiMeasurement(*this);
        </code>
      </method>

<method
type = 'const DeVeloPhiType&amp;'
name = 'sensor'
desc = 'Retrieve const the sector of the measurement'
const = 'TRUE'>
</method>


    </class>

  </package>
</gdd>
