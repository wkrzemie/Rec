/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UpgradeGhostId_H
#define UpgradeGhostId_H 1

// Include files
#include "Linker/LinkerTool.h"

#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVPExpectation.h"
#include "TrackInterfaces/IGhostProbability.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"


class IClassifierReader;
namespace Track{
  class TabulatedFunction1D;
}


/** @class UpgradeGhostId UpgradeGhostId.h
 *
 *  @author Paul Seyfert
 *  @date   30-12-2014
 */
class UpgradeGhostId : public extends<GaudiTool, IGhostProbability> {


public:
  /// Standard constructor
  using extends::extends;

  StatusCode finalize() override; ///< Algorithm initialization
  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute(LHCb::Track& aTrack) const override;
  StatusCode beginEvent() override {return countHits();};
  /** reveal the variable names for a track type */
  std::vector<std::string> variableNames(LHCb::Track::Types type) const override {
    if (LHCb::Track::Types::Velo==type) return veloVars();
    if (LHCb::Track::Types::Long==type) return longVars();
    if (LHCb::Track::Types::Upstream==type) return upstreamVars();
    if (LHCb::Track::Types::Downstream==type) return downstreamVars();
    if (LHCb::Track::Types::Ttrack==type) return ttrackVars();
    return std::vector<std::string>();
  }

  /** reveal the variable values for a track */
  std::vector<float> netInputs(LHCb::Track& ) const override {
    fatal() << "UpgradeGhostId::netInputs is NOT IMPLEMENTED" << endmsg;
    return std::vector<float>();
  }

protected:
  StatusCode countHits();

private:
  IVPExpectation   *m_vpExpectation;
  std::vector<IClassifierReader*> m_readers;


  std::vector<IHitExpectation*> m_Expectations;
  std::vector<std::string> m_inNames;
  Gaudi::Property<std::vector<std::string>> m_expectorNames { this, "Expectors" , {"UTHitExpectation","FTHitExpectation"} };
  std::vector<int>* m_expectedHits;


  int m_veloHits;
  //int m_ttHits;
  int m_utHits;
  //int m_ftHits;
  //int m_itHits;
  //int m_otHits;

  std::vector<std::string> veloVars() const ;
  std::vector<std::string> upstreamVars() const ;
  std::vector<std::string> downstreamVars() const ;
  std::vector<std::string> longVars() const ;
  std::vector<std::string> ttrackVars() const ;

};

#endif // UpgradeGhostId_H
