/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VELOMUONBUILDER_H
#define VELOMUONBUILDER_H 1

#include <memory>

#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/Tuples.h"
#include <string>

#include "Event/Track.h"
#include "Event/MuonPID.h"
#include "Event/MuonDigit.h"
#include "Event/MuonCoord.h"

#include"MuonDet/IMuonFastPosTool.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "Kernel/ILHCbMagnetSvc.h"

/** @class VeloMuonBuilder VeloMuonBuilder.h
 *
 * \brief  Make a ValoMuonTrack: match velo and muon tracks. afterwards apply kalman fit.
 *
 * Parameters:
 * - MuonLocation: where the muon stubs come from
 * -  VeloLocation: where the velo tracks come from
 * -  OutputLocation: where the velomuontracks go to
 * -  zmagnet: where the magnet's bending plane is (optimised for matching)
 * -  zmatch: where in the y-z plane matching is done
 * -  chamberhit: if for y-z matching the first muon measurement shall be used instead of zmatch
 * -  distancecut: obsolete, kept for compatibility. hard coded.
 * -  lhcbids: obsolete used for elaborate chi^2 fit in the muon stations with unknown hit pattern
 *             not applicable for existing muon algorithms but for private code
 * -  cut: if the hard coded distancecut shall be scaled
 * -  MaxVeloTracks: if there are more velo tracks, don't do anything (saving cpu time in hopeless events)
 * -  MaxMuonTracks: if there are more muon tracks, don't do anything (saving cpu time in hopeless events)
 *
 *  @author Paul Seyfert
 *  @date   2010-09-16
 */

class VeloMuonBuilder : public GaudiTupleAlg {

  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override;
  StatusCode execute() override;
  StatusCode buildVeloMuon(LHCb::Tracks& veloTracks, LHCb::Tracks& muonTracks, LHCb::Tracks* trackvector);
  LHCb::Tracks* buildVeloMuon(LHCb::Tracks& veloTracks, LHCb::Tracks& muonTracks);

private:

  Gaudi::Property<float> m_distcutmultiplyer { this, "cut", 1};
  Gaudi::Property<bool> m_chamberhit { this, "chamberhit", true };
  Gaudi::Property<double> m_zmagnet { this, "zmagnet", 5400. };
  Gaudi::Property<float> m_zmatch { this, "zmatch", 15000. };
  Gaudi::Property<std::string> m_muonpath { this, "MuonLocation", "Hlt1/Tracks/MuonSeg" };
  Gaudi::Property<std::string> m_velopath { this, "VeloLocation", LHCb::TrackLocation::Velo };
  Gaudi::Property<std::string> m_output { this, "OutputLocation", "Rec/Track/VeloMuon" };

  Gaudi::Property<unsigned int> m_maxmuons { this, "MaxMuonTracks", 30 };
  Gaudi::Property<unsigned int> m_maxvelos { this, "MaxVeloTracks", 1000 };

  Gaudi::Property<int> n_lhcbids { this, "lhcbids", 4 };

  IMuonFastPosTool* m_iPosTool = nullptr;
  ITrackExtrapolator* m_linearextrapolator = nullptr;
  ITrackFitter* m_tracksFitter = nullptr;
  ILHCbMagnetSvc* m_magFieldSvc = nullptr;
};

#endif
