/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class OTMeasurementProvider OTMeasurementProvider.cpp
 *
 * Implementation of VeloRMeasurementProvider tool
 * see interface header for description
 *
 *  @author Wouter Hulsbergen
 *  @date   30/12/2005
 */

#include "TrackInterfaces/IMeasurementProvider.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "Event/OTMeasurement.h"
#include "Event/StateVector.h"
#include "OTDet/DeOTDetector.h"
#include "OTDAQ/IOTRawBankDecoder.h"
#include "Event/TrackParameters.h"
#include "TrackKernel/TrackTraj.h"

class OTMeasurementProvider : public extends<GaudiTool, IMeasurementProvider>
{
public:

  OTMeasurementProvider( const std::string& type, const std::string& name,
                         const IInterface* parent );

  StatusCode initialize() override;
  StatusCode finalize() override;
  LHCb::Measurement* measurement( const LHCb::LHCbID& id, bool localY=false ) const  override;
  LHCb::Measurement* measurement( const LHCb::LHCbID& id, const LHCb::ZTrajectory<double>& refvector, bool localY=false) const override;

  void addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                          std::vector<LHCb::Measurement*>& measurements,
                          const LHCb::ZTrajectory<double>& reftraj) const override;

  StatusCode load( LHCb::Track&  ) const override {
    info() << "sorry, MeasurementProviderBase::load not implemented" << endmsg ;
    return StatusCode::FAILURE ;
  }

private:
  inline LHCb::OTMeasurement* otmeasurement( const LHCb::LHCbID& id ) const  ;

  const DeOTDetector* m_det = nullptr;
  ToolHandle<IOTRawBankDecoder> m_otdecoder = { "OTRawBankDecoder" };
  Gaudi::Property<bool> m_applyTofCorrection { this, "ApplyTofCorrection", true };
  double m_magnetZTofCorrection  = 5140*Gaudi::Units::mm;
  double m_tofCorrectionScale = 0.46;
} ;

//=============================================================================
// Declare to tool factory
//=============================================================================

DECLARE_COMPONENT( OTMeasurementProvider )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
OTMeasurementProvider::OTMeasurementProvider( const std::string& type,
                                              const std::string& name,
                                              const IInterface* parent )
: extends( type, name , parent )
{
  declareProperty("RawBankDecoder",m_otdecoder) ;
}

//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------

StatusCode OTMeasurementProvider::initialize()
{
  StatusCode sc = GaudiTool::initialize();
  if( sc.isFailure() ) { return Error( "Failed to initialize!", sc ); }

  // Retrieve the detector element
  m_det = getDet<DeOTDetector>( DeOTDetectorLocation::Default ) ;
  // Retrieve the raw bank decoder (for recreating the OTLiteTime)
  m_otdecoder.retrieve().ignore() ;

  return sc;
}

//-----------------------------------------------------------------------------
/// Finalize
//-----------------------------------------------------------------------------

StatusCode OTMeasurementProvider::finalize()
{
  m_otdecoder.release().ignore() ;
  return GaudiTool::finalize();
}

//-----------------------------------------------------------------------------
/// Create a measurement
//-----------------------------------------------------------------------------

inline LHCb::OTMeasurement* OTMeasurementProvider::otmeasurement( const LHCb::LHCbID& id ) const
{
  if( UNLIKELY(!id.isOT()) ) {
    error() << "Not an OT measurement" << endmsg ;
    return nullptr;
  }
  LHCb::OTChannelID otid = id.otID() ;
  const DeOTModule* module = m_det->findModule( otid ) ;
  return new LHCb::OTMeasurement(m_otdecoder->time(otid),*module) ;
}

LHCb::Measurement* OTMeasurementProvider::measurement( const LHCb::LHCbID& id, bool /*localY*/ ) const
{
  return otmeasurement(id) ;
}

//-----------------------------------------------------------------------------
/// Create a measurement with statevector. For now very inefficient.
//-----------------------------------------------------------------------------
LHCb::Measurement* OTMeasurementProvider::measurement( const LHCb::LHCbID& id,
                                                       const LHCb::ZTrajectory<double>& reftraj, bool /*localY*/ ) const
{
  // default implementation
  LHCb::OTMeasurement* meas = otmeasurement(id) ;
  if(meas) {
    // set the tof correction
    if(m_applyTofCorrection) {
      // make the time-of-flight correction
      const double zmagnet  = m_magnetZTofCorrection ;
      // straight line length
      LHCb::StateVector refvector = reftraj.stateVector(meas->z()) ;
      auto point = refvector.position() ;
      auto L0 = point.R() ;
      // Correction following the kick formula (to 2nd order in dtx)
      auto f = zmagnet/point.z() ;
      auto dtx = refvector.tx() - point.x()/point.z() ;
      auto dL = 0.5*(1-f)/f*dtx*dtx*L0 ;
      // The kick correction is actually too long. Until we find something better, we simply scale with a factor derived in MC:
      auto L = L0 + m_tofCorrectionScale * dL ;
      meas->setTimeOfFlight( L / Gaudi::Units::c_light ) ;
    }
  }
  return meas ;
}


//-----------------------------------------------------------------------------
/// Create measurements for list of LHCbIDs
//-----------------------------------------------------------------------------

void OTMeasurementProvider::addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                                               std::vector<LHCb::Measurement*>& measurements,
                                               const LHCb::ZTrajectory<double>& reftraj) const
{
  measurements.reserve( measurements.size() + ids.size());
  std::transform( ids.begin(), ids.end(),
                  std::back_inserter(measurements),
                  [&](const LHCb::LHCbID& id) {
                      return measurement(id,reftraj,false);
                  } );
}
