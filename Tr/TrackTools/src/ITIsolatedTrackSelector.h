/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKTOOLS_ITIsolatedTrackSelector_H
#define TRACKTOOLS_ITIsolatedTrackSelector_H

//-----------------------------------------------------------------------------
/** @class ITIsolatedTrackSelector ITIsolatedTrackSelector.h
 *
 *  Header file for reconstruction tool : ITIsolatedTrackSelector
 *
 *  @author M.Needham Matt.Needham@cern.ch
 *
 *  @date   30/12/2005
 */
//-----------------------------------------------------------------------------

#include "GaudiAlg/GaudiHistoTool.h"
#include "TrackInterfaces/ITrackSelector.h"
#include <string>

// GaudiKernel
#include "GaudiKernel/HashMap.h"

#include "Event/Track.h"

// boost
#include "boost/numeric/conversion/bounds.hpp"
#include "boost/limits.hpp"

struct ISTClusterCollector;

class ITIsolatedTrackSelector : public extends<GaudiHistoTool, ITrackSelector>
{
 public:
  /// constructor
  using extends::extends;

  /// Tool initialization
  StatusCode initialize() override;

  /** Returns if the given track is selected or not
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return boolean indicating if the track is selected or not
   *  @retval true  Track is selected
   *  @retval false Track is rejected
   */
  bool accept ( const LHCb::Track& aTrack ) const override;

  enum Category  {CSide = 1 , ASide =2, Bottom = 3, Top =4 , Mixed =5 };

 private:

  IHitExpectation* m_expectedHits = nullptr;
  std::vector< ISTClusterCollector* > m_collectors;

  Gaudi::Property<unsigned int> m_maxHitNbr { this,  "MaxHitNbr", 2 };
  Gaudi::Property<unsigned int> m_minNumITHits { this,  "MinITHits", 6 };
  Gaudi::Property<std::string> m_expectedHitsTool { this,  "ExpectedHitsTool", "ITHitExpectation" };

};

#endif // TRACKTOOLS_ITIsolatedTrackSelector_H
