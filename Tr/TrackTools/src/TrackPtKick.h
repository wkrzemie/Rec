/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKTOOLS_TRACKPTKICK_H
#define TRACKTOOLS_TRACKPTKICK_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from LHCbKernel
#include "Kernel/IBIntegrator.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackMomentumEstimate.h"

/** @class TrackPtKick TrackPtKick.h TrackTools/TrackPtKick.h
 *
 *  @author M. Needham
 *  @date   2000-08-16
 */
class TrackPtKick : public extends<GaudiTool, ITrackMomentumEstimate> {
public:
  using extends::extends;

  StatusCode initialize() override;

  // Estimate the momentum P of a State in T at ZAtMidT
  StatusCode calculate( const LHCb::State* tState, double& qOverP, double& sigmaQOverP,
                        bool tCubicFit ) const override;

  // Estimate the momentum P of a velo State and a State in T at ZAtMidT
  StatusCode calculate( const LHCb::State* veloState, const LHCb::State* tState,
				        double& qOverP, double& sigmaQOverP,
				        bool tCubicFit ) const override;// Estimate the momentum P of a State

private:

  PublicToolHandle<IBIntegrator> m_bIntegrator{ this, "BFieldIntegrator", "BIntegrator" };

  StatusCode determineFieldPolarity(); //FIXME: register as callback in case field conditions change!
  int m_FieldPolarity = 1;

  /// Define the parameters of the Z dependance
  Gaudi::Property<std::vector<double>> m_ParabolicCorrection { this,  "ParabolicCorrection", { 1.04,0.14 } };
  Gaudi::Property<std::vector<double>> m_resParams { this,  "resParams", { 0.015,0.29 } };
  Gaudi::Property<double> m_Constant { this,  "ConstantCorrection",  0.*Gaudi::Units::MeV };

};
#endif // TRACKTOOLS_TRACKPTKICK_H
