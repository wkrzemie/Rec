/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file DelegatingTrackSelector.h
 *
 *  Header file for reconstruction tool : DelegatingTrackSelector
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   30/06/2009
 */
//-----------------------------------------------------------------------------

#ifndef TRACKTOOLS_DelegatingTrackSelector_H
#define TRACKTOOLS_DelegatingTrackSelector_H

//-----------------------------------------------------------------------------
/** @class DelegatingTrackSelector DelegatingTrackSelector.h
 *
 *  General track Selection tool
 *
 *  Cuts can be applied on various quantities like p, hits, chi^2, pt, and track type.
 *
 *  @author C. Jones  Christopher.Rob.Jones@cern.ch
 *
 *  @date   30/06/2009
 */
//-----------------------------------------------------------------------------

// STL
#include <sstream>

// base class
#include "TrackSelectorBase.h"

class DelegatingTrackSelector : public TrackSelectorBase
{

public:

  /// constructer
  using TrackSelectorBase::TrackSelectorBase;

  /** Returns if the given track is selected or not
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return boolean indicating if the track is selected or not
   *  @retval true  Track is selected
   *  @retval false Track is rejected
   */
  bool accept ( const LHCb::Track & aTrack ) const override;

private:

  /// Access on demand the track selector for each track type
  ITrackSelector * trackSelector( const LHCb::Track& aTrack ) const;

  /// Track selector for each track type
  typedef GaudiUtils::HashMap < const LHCb::Track::Types, ITrackSelector* > TrackSelectors;
  mutable TrackSelectors m_trSels;

};

#endif // TRACKTOOLS_DelegatingTrackSelector_H
