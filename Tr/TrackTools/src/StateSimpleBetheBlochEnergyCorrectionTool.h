/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKTOOLS_STATESIMPLEBETHEBLOCHENERGYCORRECTIONTOOL_H
#define TRACKTOOLS_STATESIMPLEBETHEBLOCHENERGYCORRECTIONTOOL_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from TrackInterfaces
#include "TrackInterfaces/IStateCorrectionTool.h"

// from TrackEvent
#include "Event/State.h"

/** @class StateSimpleBetheBlochEnergyCorrectionTool
 *
 *  This state correction tool applies a dE/dx energy loss correction
 *  with a simplified version of the Bethe-Bloch equation.
 *
 *  @author Eduardo Rodrigues
 *  @date   2006-08-18
 *  (Original code taken from the master extrapolator)
 */
class StateSimpleBetheBlochEnergyCorrectionTool : public extends<GaudiTool, IStateCorrectionTool> {
public:
  /// Standard constructor
  using extends::extends;
  /// Correct a State for dE/dx energy losses with a simplified Bethe-Bloch equiaton
  void correctState( LHCb::State& state,
                     const Material* material,
                     std::any& cache,
                     double wallThickness,
                     bool upstream,
                     double ) const override;

private:
  // Job options
  Gaudi::Property<double> m_energyLossCorr { this, "EnergyLossFactor", 354.1 * Gaudi::Units::MeV*Gaudi::Units::mm2/Gaudi::Units::mole };               ///< tunable energy loss correction
  Gaudi::Property<double> m_maxEnergyLoss { this, "MaximumEnergyLoss", 100. * Gaudi::Units::MeV           };                ///< maximum energy loss in dE/dx correction
  Gaudi::Property<double> m_minMomentumAfterEnergyCorr { this, "MinMomentumAfterEnergyCorr", 10.*Gaudi::Units::MeV };   ///< minimum momentum after dE/dx correction
  Gaudi::Property<double> m_sqrtEError { this, "EnergyLossError", 1.7 * sqrt(Gaudi::Units::MeV) }; // proportional to sqrt(E);                   ///< sigma(dE)/sqrt(dE)
  Gaudi::Property<bool>   m_useEnergyLossError { this, "UseEnergyLossError", false} ;          ///< flag to turn on using error on energy loss
};
#endif // TRACKTOOLS_STATESIMPLEBETHEBLOChENERGYCORRECTIONTOOL_H
