/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include <cmath>
#include <numeric>

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// from TrackEvent
#include "FastMomentumEstimate.h"
#include "Event/TrackParameters.h"
#include "Event/State.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FastMomentumEstimate
//
// 2007-10-30 : S. Hansmann-Menzemer
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( FastMomentumEstimate )

//=============================================================================
// Initialization
//=============================================================================
StatusCode FastMomentumEstimate::initialize()
{
  StatusCode sc = extends::initialize();
  if (sc.isFailure()) return sc;  // error already reported by base class

  m_magFieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );

  //@FIXME: why, if one of the properties is not properly set, are all
  //        four of them preset to these defaults???

  if ( 4 != m_paramsTParab.size() || 4 != m_paramsTCubic.size() ||
       6 != m_paramsVeloTParab.size() || 6 != m_paramsVeloTCubic.size()){

    m_paramsTParab.clear();
    m_paramsVeloTParab.clear();
    m_paramsTCubic.clear();
    m_paramsVeloTCubic.clear();

    if (m_magFieldSvc->useRealMap()){
      m_paramsTParab      = { -6.30991, -4.83533, -12.9192, 4.23025e-08 } ;
      m_paramsVeloTParab  = { 1.20812, 0.636694, -0.251334, 0.414017, 2.87247, -20.0982 };
      m_paramsTCubic      = { -6.34025, -4.85287, -12.4491, 4.25461e-08 };
      m_paramsVeloTCubic  = { 1.21352, 0.626691, -0.202483, 0.426262, 2.47057, -13.2917 };
    } else {
      m_paramsTParab      = { -6.3453, -4.77725, -14.9039, 3.13647e-08 };
      m_paramsVeloTParab  = { 1.21909, 0.627841, -0.235216, 0.433811, 2.92798, -21.3909 };
      m_paramsTCubic      = { -6.31652, -4.46153, -16.694, 2.55588e-08 };
      m_paramsVeloTCubic  = { 1.21485, 0.64199, -0.27158, 0.440325, 2.9191, -20.4831 };
    }
  }

  return StatusCode::SUCCESS;
}
//=============================================================================
// Calculate momentum, given T state only
//=============================================================================
StatusCode FastMomentumEstimate::calculate( const LHCb::State* tState, double& qOverP,
                                            double& sigmaQOverP, bool tCubicFit ) const
{
  const double tx = tState->tx();
  const double ty = tState->ty();
  const double x0 = tState->x() - tx * tState->z();
  auto vars = { tx*tx, ty*ty, x0*x0 };

  const auto& params = ( tCubicFit ? m_paramsTCubic : m_paramsTParab );
  const auto  p = std::inner_product( next(begin(params)), end(params),
                                      begin(vars), params[0] );

  const auto scaleFactor = m_magFieldSvc->signedRelativeCurrent();
  const auto denom = p * scaleFactor * 1e6 * (-1);

  if (std::abs(scaleFactor) < 1e-6){
    qOverP = 0.01/Gaudi::Units::GeV;
    sigmaQOverP = 1.0/Gaudi::Units::MeV;
  } else {
    qOverP = x0/denom;
    sigmaQOverP = m_tResolution * std::fabs(qOverP);
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Calculate momentum, given T and Velo state (for matching)
//=============================================================================
StatusCode FastMomentumEstimate::calculate( const LHCb::State* veloState, const LHCb::State* tState,
                                            double& qOverP, double& sigmaQOverP,
                                            bool tCubicFit ) const
{
  const auto txT = tState->tx();
  const auto txV = veloState->tx();
  const auto tyV = veloState->ty();
  auto vars = { txT*txT, txT*txT*txT*txT,
                txT*txV,
                tyV*tyV, tyV*tyV*tyV*tyV };

  const auto& params = ( tCubicFit ? m_paramsVeloTCubic : m_paramsVeloTParab );
  const auto coef = std::inner_product( next(begin(params)), end(params),
                                        begin(vars), params[0] );

  const auto proj = sqrt( ( 1. + txV*txV + tyV*tyV ) / ( 1. + txV*txV ) );
  const auto scaleFactor = m_magFieldSvc->signedRelativeCurrent();

  if (std::abs(scaleFactor) < 1e-6){
    qOverP = 1.0/Gaudi::Units::GeV;
    sigmaQOverP = 1.0/Gaudi::Units::MeV;
  } else {
    qOverP = (txV-txT)/( coef * Gaudi::Units::GeV * proj*scaleFactor);
    sigmaQOverP = m_veloPlusTResolution * std::fabs(qOverP);
  }
  return StatusCode::SUCCESS;
}
