/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKTOOLS_STATETHINMSCORRECTIONTOOL_H
#define TRACKTOOLS_STATETHINMSCORRECTIONTOOL_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from TrackInterfaces
#include "TrackInterfaces/IStateCorrectionTool.h"

// from TrackEvent
#include "Event/State.h"

/** @class StateThinMSCorrectionTool StateThinMSCorrectionTool.h
 *
 *  This state correction tool applies a multiple scattering correction
 *  in the approximation of a thin scatter
 *
 *  @author Eduardo Rodrigues
 *  @date   2006-08-21
 *  (Original code taken from the master extrapolator)
 */
class StateThinMSCorrectionTool final : public extends<GaudiTool, IStateCorrectionTool> {
public:

  /// Standard constructor
  using extends::extends;

  /// Correct a State for multiple scattering in the approximation of a thin scatter
  void correctState( LHCb::State& state,
                     const Material* material,
                     std::any& cache,
                     double wallThickness,
                     bool upstream,
                     double ) const override;

private:
  // Job options
  Gaudi::Property<double> m_msff2{ this, "MSFudgeFactor2" , 1.0 };    ///< fudge factor for multiple scattering errors

};
#endif // TRACKTOOLS_STATETHINMSCORRECTIONTOOL_H
