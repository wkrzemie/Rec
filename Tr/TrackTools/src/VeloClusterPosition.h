/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VELOCLUSTERPOSITION_H
#define VELOCLUSTERPOSITION_H 1

// Include files
#include <optional>
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "TrackInterfaces/IVeloClusterPosition.h"            // Interface
#include "Kernel/VeloChannelID.h"
#include "Kernel/SiPositionInfo.h"
#include "Event/StateVector.h"
#include "GaudiMath/GaudiMath.h"

/** @class VeloClusterPosition VeloClusterPosition.h
 *
 *
 *  @author Tomasz Szumlak
 *  @date   2005-09-30
 */

class DeVelo;
class DeVeloSensor;

class VeloClusterPosition final: public extends<GaudiTool, IVeloClusterPosition> {
public:

  using extends::extends;

  // throughout the code LA stands for Linear Approximation
  // typedefs for object returned by tool

  enum conv{
    RAD_TO_DEG=180
  };
  //-- type of the error parametrization
  enum paraType{
    PITCH_PARA=1,
    ANGLE_PARA
  };

  typedef IVeloClusterPosition::toolInfo toolInfo;
  typedef IVeloClusterPosition::Direction Direction;
  typedef std::pair<double, double> Pair;
  // structure to keep resolution parametrisations
  typedef std::pair<double, Pair> ResPair;
  typedef std::vector<ResPair> ResVector;

  StatusCode initialize() override;    ///< Tool initialization
  //-- public interface ----------------------------------------------
  toolInfo position(const LHCb::VeloCluster* aCluster) const override;
  toolInfo position(const LHCb::VeloLiteCluster* aCluster) const override;

  toolInfo position(const LHCb::VeloCluster* aCluster,
                    const Gaudi::XYZPoint& aPoint,
                    const Direction& aDirection) const override;
  toolInfo position(const LHCb::VeloLiteCluster* aCluster,
                    const Gaudi::XYZPoint& aPoint,
                    const Direction& aDirection) const override;

  toolInfo position(const LHCb::VeloCluster* aCluster,
                    const LHCb::StateVector& aState) const override;
  toolInfo position(const LHCb::VeloLiteCluster* aCluster,
                    const LHCb::StateVector& aState) const override;

protected:

  /// full/lite cluster position implementation code
  toolInfo position(const LHCb::VeloChannelID &centreChannel,
		    const double &fractionalPos) const;

  /// full/lite cluster position implementation code with point and direction
  toolInfo position(const LHCb::VeloChannelID &centreChan,
		    const double & fracPos,
		    const Gaudi::XYZPoint& aGlobalPoint,
		    const Direction& aDirection, const int size) const;


  toolInfo weightedLAPos(const LHCb::VeloCluster* cluster) const;
  //-- if state is not available use simpler version of the
  //-- error estimation
  double meanResolution(double pitch) const;
  //-- this method can be used when full state is available
  double errorEstimate(const double projAngle, const double pitch, const int size, const bool isR) const;
  GaudiMath::Interpolation::Type splineType() const
  { return m_splineType.value(); }
  //-- calculates the value of the projected angle
  Pair projectedAngle(const DeVeloSensor* sensor,
                      const LHCb::VeloChannelID centreChan,
		      const Gaudi::XYZVector& trackDir,
		      const Gaudi::XYZPoint& gloPoint) const;
  double fracPosLA(const LHCb::VeloCluster* aCluster) const;
  double fracPosLA(const LHCb::VeloLiteCluster* aCluster) const;
  double angleOfTrack(const Direction& localSlopes,
                      Gaudi::XYZVector& parallel2Track) const;
  Direction localTrackDirection(const Gaudi::XYZVector& globalTrackDir,
                                const DeVeloSensor* sensor) const;
  StatusCode i_cacheConditions();

private:

  DeVelo* m_veloDet = nullptr;                        /// detector element
  std::vector<double> m_defaultResolution{std::vector<double>(24, 0.)};  /// resolution para one angle
  //Declare the vector of spline points
  std::vector<double> m_p0Values{std::vector<double>(24, 0.)};           /// vector of constants
  std::vector<double> m_p1Values{std::vector<double>(24, 0.)};           /// vector of slopes

  bool m_old = false;

  std::vector<double> m_errAnglePara;       /// angle projection para
  //Declare the splines
  std::optional<GaudiMath::SimpleSpline> m_p0 ;            /// spline for constants
  std::optional<GaudiMath::SimpleSpline> m_p1 ;            /// spline for slopes

  std::array<std::optional<GaudiMath::SimpleSpline>, 6> m_p_StripR ;
  std::array<std::optional<GaudiMath::SimpleSpline>, 6> m_p_StripPhi ;

  double m_minAngle = 0.;                        /// first angle - lower limit
  double m_maxAngle = 0.;                        /// last angle - higher limit
  double m_fracPos = 0.;                         /// fractional position
  double m_corrFactor = 1.;                      /// correct difference between sigma RMS
  std::string m_paraClass;                  /// decide if correction factor is needed
  std::string m_condPath = "Conditions/Calibration/Velo";

  Gaudi::Property<bool> m_calculateExactProjAngle{this, "CalculateExactProjAngle", false};
  Gaudi::Property<unsigned int> m_errorParaType{this, "ErrorParaType", PITCH_PARA};
  Gaudi::Property<GaudiMath::Interpolation::Type> m_splineType{this, "SplineType", GaudiMath::Interpolation::Type::Cspline};

};
#endif // VELOCLUSTERPOS_H
