/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file DelegatingTrackSelector.cpp
 *
 *  Implementation file for reconstruction tool : DelegatingTrackSelector
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   30/12/2005
 */
//-----------------------------------------------------------------------------

#include "GaudiKernel/SystemOfUnits.h"

// Tsa
#include "DelegatingTrackSelector.h"

using namespace LHCb;

DECLARE_COMPONENT( DelegatingTrackSelector )

//-----------------------------------------------------------------------------

bool DelegatingTrackSelector::accept ( const Track& aTrack ) const
{
  if ( msgLevel(MSG::VERBOSE) ) {
    verbose() << "Trying Track " << aTrack.key() << " " << aTrack.type()
              << " P=" << aTrack.p() << " Pt=" << aTrack.pt()
              << endmsg;
  }
  return ( checkTrackType(aTrack) && trackSelector(aTrack)->accept(aTrack) );
}

ITrackSelector *DelegatingTrackSelector::trackSelector( const LHCb::Track& aTrack ) const
{
  ITrackSelector *selector = nullptr;
  auto iS = m_trSels.find(aTrack.type());
  if ( iS == m_trSels.end() ) {
    std::ostringstream name;
    name << aTrack.type();
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << "Getting selector for " << name.str() << endmsg;
    if ( !name.str().empty() ) {
      selector = m_trSels[aTrack.type()] = tool<ITrackSelector>( "TrackSelector", name.str(), this );
    } else {
      Exception( "Empty track type" );
    }
  } else {
    selector = iS->second;
  }
  return selector;
}
