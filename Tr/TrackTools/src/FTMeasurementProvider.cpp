/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class FTMeasurementProvider FTMeasurementProvider.cpp
 *
 * Implementation of FTMeasurementProvider
 * see interface header for description
 *
 *  @author Wouter Hulsbergen
 *  @date   30/12/2005
 */

#include "TrackInterfaces/IMeasurementProvider.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "GaudiKernel/ToolHandle.h"

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "Event/FTMeasurement.h"
#include "Event/StateVector.h"
#include "FTDet/DeFTDetector.h"
#include "Event/TrackParameters.h"
#include "TrackKernel/TrackTraj.h"
#include "Event/FTLiteCluster.h"

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

class FTMeasurementProvider final : public extends< GaudiTool,
                                                    IMeasurementProvider >
{
public:

  /// constructor
  FTMeasurementProvider(const std::string& type,
                         const std::string& name,
                         const IInterface* parent);

  StatusCode initialize() override;
  LHCb::Measurement* measurement( const LHCb::LHCbID& id,
                                  bool localY=false ) const override;
  LHCb::Measurement* measurement( const LHCb::LHCbID& id,
                                  const LHCb::ZTrajectory<double>& refvector,
                                  bool localY=false) const override;
  inline LHCb::FTMeasurement* ftmeasurement( const LHCb::LHCbID& id ) const  ;

  const FTLiteClusters* clusters() const;

  void addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                          std::vector<LHCb::Measurement*>& measurements,
                          const LHCb::ZTrajectory<double>& reftraj) const override ;

  StatusCode load( LHCb::Track&  ) const override {
    return Error( "sorry, MeasurementProviderBase::load not implemented" );
  }

private:
  const DeFTDetector* m_det = nullptr;
  AnyDataHandle<FTLiteClusters> m_clustersDh { LHCb::FTLiteClusterLocation::Default,
                                               Gaudi::DataHandle::Reader, this };
} ;

//=============================================================================
// Declare to tool factory
//=============================================================================

DECLARE_COMPONENT( FTMeasurementProvider )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FTMeasurementProvider::FTMeasurementProvider( const std::string& type,
                                              const std::string& name,
                                              const IInterface* parent )
  :  extends( type, name , parent )
{
  declareProperty("ClusterLocation",m_clustersDh);
}


//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------

StatusCode FTMeasurementProvider::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if( sc.isFailure() ) { return Error( "Failed to initialize!", sc ); }

  // Retrieve the detector element
  m_det = getDet<DeFTDetector>( DeFTDetectorLocation::Default ) ;

  return sc;
}

//-----------------------------------------------------------------------------
/// Load clusters from the TES
//-----------------------------------------------------------------------------

const FTLiteClusters* FTMeasurementProvider::clusters() const
{
  return m_clustersDh.get();
}

//-----------------------------------------------------------------------------
/// Create a measurement
//-----------------------------------------------------------------------------
LHCb::FTMeasurement* FTMeasurementProvider::ftmeasurement( const LHCb::LHCbID& id ) const {

  if( !id.isFT() ) {
    error() << "Not an FT measurement" << endmsg ;
    return nullptr;
  }
  /// The clusters are not sorted anymore, so we can use a find_if
  /// to find the element corresponding to the channel ID
  const auto& c = clusters()->range();
  auto itH = std::find_if( c.begin(),  c.end(),
                          [&id](const LHCb::FTLiteCluster clus){
                            return clus.channelID() == id.ftID(); });
  return ( itH != c.end() ) ?  new LHCb::FTMeasurement( (*itH), *m_det ) : nullptr;
}

//-----------------------------------------------------------------------------
/// Return the measurement
//-----------------------------------------------------------------------------
LHCb::Measurement* FTMeasurementProvider::measurement( const LHCb::LHCbID& id,
                                                       bool /*localY*/ ) const {
  return ftmeasurement(id) ;
}

//-----------------------------------------------------------------------------
/// Create a measurement with statevector. For now very inefficient.
//-----------------------------------------------------------------------------

LHCb::Measurement* FTMeasurementProvider::measurement( const LHCb::LHCbID& id,
                                                       const LHCb::ZTrajectory<double>& /* reftraj */,
                                                       bool /*localY*/ ) const {
  // default implementation
  return ftmeasurement(id) ;
}

//-----------------------------------------------------------------------------
/// Create measurements for list of LHCbIDs
//-----------------------------------------------------------------------------

void FTMeasurementProvider::addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                                               std::vector<LHCb::Measurement*>& measurements,
                                               const LHCb::ZTrajectory<double>& reftraj) const
{
  measurements.reserve(measurements.size()+ids.size());
  std::transform( ids.begin(), ids.end(), std::back_inserter(measurements),
                  [&](const LHCb::LHCbID& id)
                  { return measurement(id,reftraj,false) ; });
}
