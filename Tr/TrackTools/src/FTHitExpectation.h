/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _FTHitExpectation_H
#define _FTHitExpectation_H

/** @class FTHitExpectation FTHitExpectation.h
 *
 * Implementation of FTHitExpectation tool
 * see interface header for description
 *
 *  @author D.Milanes
 *  @date   20/13/2013
 */


#include "THitExpectation.h"
#include "Event/Track.h"

namespace LHCb{
  class FTChannelID;
  class LHCbID;
}

class DeFTDetector;

/*
namespace Tf {
  namespace Tsa {
    class IITExpectedHits;
  }
}
*/
class FTHitExpectation: public THitExpectation {

public:

  /** constructer */
  using THitExpectation::THitExpectation;

  /** intialize */
  StatusCode initialize() override;

  /** Returns number of hits expected, from zFirst to inf
  *
  *  @param aTrack Reference to the Track to test
  *
  *  @return number of hits expected
  */
  unsigned int nExpected ( const LHCb::Track& aTrack ) const override;

  /** Returns number of hits expected
  *
  *  @param aTrack Reference to the Track to test
  *
  *  @return Info info including likelihood
  */
  IHitExpectation::Info expectation( const LHCb::Track& aTrack ) const override;

  /** Collect all the expected hits
   *
   * @param aTrack Reference to the Track to test
   * @param hits collected lhcbIDs
   *
   **/
  void collect(const LHCb::Track& aTrack,
               std::vector<LHCb::LHCbID>& ids ) const override;

private:

  bool findIntersectingMat(const DeFTLayer* layer, const LHCb::Track& aTrack,
      const DeFTMat*& mat, Gaudi::XYZPoint& intersectionPoint) const ;

  Gaudi::XYZPoint intersection(const Tf::Tsa::Line3D& line, const Gaudi::Plane3D& aPlane) const;

  DeFTDetector* m_ftDet = nullptr;
 
};

#endif
