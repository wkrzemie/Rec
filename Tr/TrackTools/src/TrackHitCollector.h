/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INCLUDE_TRACKHITCOLLECTOR_H
#define INCLUDE_TRACKHITCOLLECTOR_H 1

#include <vector>
#include <utility>
#include <memory>
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "TrackInterfaces/ITrackHitCollector.h"

#include "Kernel/Trajectory.h"
#include "Event/Track.h"

namespace LHCb {
    class TrackTraj;
    class State;
}

namespace Tf {
    template<class Hit> class TStationHitManager;
    template<class Hit> class TTStationHitManager;
    struct DefaultVeloRHitManager;
    struct DefaultVeloPhiHitManager;
    template<class A, class B, int C> class DefaultVeloHitManager;
    class LineHit;
    class VeloRHit;
    class VeloPhiHit;
}

class PatForwardHit;
class PatTTHit;
class DeVeloRType;
class DeVeloPhiType;
struct ITrajPoca;

/** @class TrackHitCollector TrackHitCollector.h
 *
 * collect Hits around a track
 *
 * @author Manuel Tobias Schiller <schiller@physi.uni-heidelberg.de>
 * @date   2010-01-27
 *
 * current limitation: does not collect Muon hits (yet)
 */
class TrackHitCollector : public extends<GaudiTool, ITrackHitCollector>
{
    public:
	typedef Tf::TStationHitManager<PatForwardHit> HitManT;
	typedef Tf::TTStationHitManager<PatTTHit> HitManTT;
	typedef Tf::DefaultVeloPhiHitManager HitManVeloPhi;
	typedef Tf::DefaultVeloRHitManager HitManVeloR;

	/// Standard Constructor
    using extends::extends;

	StatusCode initialize() override; ///< Tool initialization

	/// collect hits around a track
	/** collect hits around a track
	 *
	 * @param tr	track around which hits are to be collected
	 * @param ids	vector to hold collected hits with residuals
	 * @param collectVelo collect hits in Velo
	 * @param collectTT collect hits in TT
	 * @param collectIT collect hits in IT
	 * @param collectOT collect hits in OT
	 * @param collectMuon collect hits in Muon
	 * @return StatusCode::SUCCESS or StatusCode::FAILURE
	 */
	StatusCode execute(
		const LHCb::Track& tr, std::vector<IDWithResidual>& ids,
		bool collectVelo = true, bool collectTT = true,
		bool collectIT = true, bool collectOT = true,
		bool collectMuon = true) override;

  private:
	// hit managers
	HitManT* m_hitManagerT = nullptr;
	HitManTT* m_hitManagerTT = nullptr;
	HitManVeloR* m_hitManagerVeloR = nullptr;
	HitManVeloPhi* m_hitManagerVeloPhi = nullptr;
	// poca tool for Velo hits
	ITrajPoca* m_trajPoca = nullptr;

    // rough preselection of hits in the vincinity of the track
    // T and TT use TolX and TolY
    Gaudi::Property<double> m_tolX{ this,"TolX", 3.0 * Gaudi::Units::cm}; ///< initial tolerance when getting rough hit list
    Gaudi::Property<double> m_tolY{ this,"TolY", 15.0 * Gaudi::Units::cm};  ///< initial tolerance when getting rough hit list
    // Velo uses TolR and TolPhi
    Gaudi::Property<double> m_tolR{ this,"TolR", 0.5 * Gaudi::Units::cm};///< initial tolerance when getting rough hit list
    Gaudi::Property<double> m_tolPhi{ this,"TolPhi", 0.05}              ; ///< initial tolerance when getting rough hit list
    // blow up track uncertainties by nSigma
    Gaudi::Property<double> m_nSigma{ this,"nSigma", 5.}; ///< initial track pos. uncertainty scale for rough hit list
    // final cuts happen on residual in each subdetector
    Gaudi::Property<double> m_maxDistOT { this, "MaxDistOT", 1.0 * Gaudi::Units::cm};     ///< max. final dist. hit-track in OT
    Gaudi::Property<double> m_maxDistIT  { this, "MaxDistIT", 1.0 * Gaudi::Units::mm};    ///< max. final dist. hit-track in IT
    Gaudi::Property<double> m_maxDistTT { this, "MaxDistTT", 1.5 * Gaudi::Units::cm};     ///< max. final dist. hit-track in TT
    Gaudi::Property<double> m_maxDistVelo { this, "MaxDistVelo", 1.0 * Gaudi::Units::mm}; ///< max. final dist. hit-track in Velo

	/// collect line hits as used in IT, TT and OT
	template<typename HITMANAGER,
	    unsigned regMin, unsigned regMax,
	    unsigned staMin, unsigned staMax,
	    unsigned layMin, unsigned layMax>
	void collectLineHits(HITMANAGER* hitman,
		const LHCb::TrackTraj& ttraj,
		std::vector<IDWithResidual>& ids) const;

	/// compute residual between track and hit, return true if small enough
	bool computeResidAndErr(
		const PatForwardHit* hit, const LHCb::TrackTraj& ttraj,
		const LHCb::State& st, double& resid, double& err) const;
	/// compute residual between track and hit, return true if small enough
	bool computeResidAndErr(
		const PatTTHit* hit, const LHCb::TrackTraj& ttraj,
		const LHCb::State& st, double& resid, double& err) const;

	/// compute quick and dirty doca of track and hit in T/TT
	void docaInTandTT(const LHCb::TrackTraj& ttraj, const Tf::LineHit& lhit,
		const double refy, const double refz, Gaudi::XYZVector& doca) const;

	/// compute residual between track and hit, return true if small enough
	bool computeResidAndError(
		const Tf::VeloRHit* hit,
		const LHCb::Trajectory<double>& htraj,
		const LHCb::TrackTraj& ttraj, const Gaudi::XYZPoint& ptrack,
		const DeVeloRType* sensor,
		const double trerr2, const double trerrphi2,
		double& resid, double& err) const;
	/// compute residual between track and hit, return true if small enough
	bool computeResidAndError(
		const Tf::VeloPhiHit* hit,
		const LHCb::Trajectory<double>& htraj,
		const LHCb::TrackTraj& ttraj, const Gaudi::XYZPoint& ptrack,
		const DeVeloPhiType* sensor,
		const double trerr2, const double trerrphi2,
		double& resid, double& err) const;

	/// select correct coordinate range (halfbox frame)
	std::pair<double, double> selectVeloCoordinates(
		const DeVeloRType* sensor, const int iZone, const double r,
		const double rmin, const double rmax,
		const double phimin, const double phimax) const;
	/// select correct coordinate range (halfbox frame)
	std::pair<double, double> selectVeloCoordinates(
		const DeVeloPhiType* sensor, const int iZone, const double r,
		const double rmin, const double rmax,
		const double phimin, const double phimax) const;

	/// check if two ranges in phi overlap
	bool phiRangesOverlap(const std::pair<double, double>& r1,
		const std::pair<double, double>& r2) const;

	/// collect Velo hits (R/Phi depending on HITMANAGER)
	template<typename HITMANAGER>
	void collectVeloHits(HITMANAGER* hitman,
		const LHCb::TrackTraj& ttraj,
		std::vector<IDWithResidual>& ids) const;

	/// update list with proper unbiased residuals (if available)
	void updateWithProperResiduals(
		std::vector<IDWithResidual>& ids,
		const LHCb::Track& tr) const;
};
#endif // INCLUDE_TRACKHITCOLLECTOR_H

// vim: sw=4:tw=78:ft=cpp
