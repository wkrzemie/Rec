/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Kernel/HitPattern.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "Event/STCluster.h"
#include "Event/VeloCluster.h"
#include "Event/GhostTrackInfo.h"
#include "TMVA/FlattenDownstream.C"
#include "TMVA/FlattenLong.C"
#include "TMVA/FlattenTtrack.C"
#include "TMVA/FlattenUpstream.C"
#include "TMVA/FlattenVelo.C"

#include <map>
namespace {
  static const std::map<LHCb::Track::Types, std::vector<std::string> > s_varsMap = {
              { LHCb::Track::Types::Velo, { "tracks_OldImplementation_obsVELO",
                                     "tracks_TrackExtraInfo_FitVeloChi2",
                                     "tracks_TrackExtraInfo_FitVeloNDoF",
                                     "tracks_OldImplementation_veloHits",
                                     "tracks_OldImplementation_ttHits",
                                     "tracks_OldImplementation_itHits",
                                     "tracks_OldImplementation_otHits",
                                     "tracks_PT",
                                     "tracks_TRACK_CHI2",
                                     "tracks_TRACK_NDOF",
                                     "tracks_TRACK_CHI2NDOF" } },
              { LHCb::Track::Types::Long, { "tracks_OldImplementation_obsVELO",
                                     "tracks_TrackExtraInfo_FitVeloChi2",
                                     "tracks_TrackExtraInfo_FitVeloNDoF",
                                     "tracks_OldImplementation_obsTT",
                                     "tracks_OldImplementation_TToutlier",
                                     "tracks_OldImplementation_obsIT",
                                     "tracks_OldImplementation_obsOT",
                                     "tracks_OldImplementation_IToutlier",
                                     "tracks_OldImplementation_OTunused",
                                     "tracks_TrackExtraInfo_FitTChi2",
                                     "tracks_TrackExtraInfo_FitTNDoF",
                                     "tracks_OldImplementation_veloHits",
                                     "tracks_OldImplementation_ttHits",
                                     "tracks_OldImplementation_itHits",
                                     "tracks_OldImplementation_otHits",
                                     "tracks_PT",
                                     "tracks_TRACK_CHI2",
                                     "tracks_TRACK_NDOF",
                                     "tracks_TRACK_CHI2NDOF",
                                     "tracks_TrackExtraInfo_NCandCommonHits",
                                     "tracks_TrackExtraInfo_FitMatchChi2" } },
              { LHCb::Track::Types::Upstream, { "tracks_OldImplementation_obsVELO",
                                         "tracks_TrackExtraInfo_FitVeloChi2",
                                         "tracks_TrackExtraInfo_FitVeloNDoF",
                                         "tracks_OldImplementation_obsTT",
                                         "tracks_OldImplementation_TToutlier",
                                         "tracks_OldImplementation_veloHits",
                                         "tracks_OldImplementation_ttHits",
                                         "tracks_OldImplementation_itHits",
                                         "tracks_OldImplementation_otHits",
                                         "tracks_PT",
                                         "tracks_TRACK_CHI2",
                                         "tracks_TRACK_NDOF",
                                         "tracks_TRACK_CHI2NDOF" } },
              { LHCb::Track::Types::Downstream, { "tracks_OldImplementation_obsTT",
                                           "tracks_OldImplementation_TToutlier",
                                           "tracks_OldImplementation_obsIT",
                                           "tracks_OldImplementation_obsOT",
                                           "tracks_OldImplementation_IToutlier",
                                           "tracks_OldImplementation_OTunused",
                                           "tracks_TrackExtraInfo_FitTChi2",
                                           "tracks_TrackExtraInfo_FitTNDoF",
                                           "tracks_OldImplementation_veloHits",
                                           "tracks_OldImplementation_ttHits",
                                           "tracks_OldImplementation_itHits",
                                           "tracks_OldImplementation_otHits",
                                           "tracks_PT",
                                           "tracks_TRACK_CHI2",
                                           "tracks_TRACK_CHI2NDOF" } },
              { LHCb::Track::Types::Ttrack, { "tracks_OldImplementation_obsIT",
                                       "tracks_OldImplementation_obsOT",
                                       "tracks_OldImplementation_IToutlier",
                                       "tracks_OldImplementation_OTunused",
                                       "tracks_TrackExtraInfo_FitTChi2",
                                       "tracks_TrackExtraInfo_FitTNDoF",
                                       "tracks_OldImplementation_veloHits",
                                       "tracks_OldImplementation_ttHits",
                                       "tracks_OldImplementation_itHits",
                                       "tracks_OldImplementation_otHits",
                                       "tracks_PT",
                                       "tracks_TRACK_CHI2",
                                       "tracks_TRACK_NDOF",
                                       "tracks_TRACK_CHI2NDOF" } } };
}

#include "Event/RecSummary.h"
class IClassifierReader {

 public:

   // constructor
   IClassifierReader() : fStatusIsClean( true ) {}
   virtual ~IClassifierReader() = default;

   // return classifier response
   virtual float GetMvaValue( std::vector<float>& inputValues ) const = 0;

   // returns classifier status
   bool IsStatusClean() const { return fStatusIsClean; }

 protected:

   bool fStatusIsClean;
};


namespace VeloGhostProb {
#include "TMVA/TMVA_1_25nsLL_1d_MLP.class.C"
}
namespace LongGhostProb {
#include "TMVA/TMVA_3_25nsLL_1d_MLP.class.C"
}
namespace UpGhostProb {
#include "TMVA/TMVA_4_25nsLL_1d_MLP.class.C"
}
namespace DownGhostProb {
#include "TMVA/TMVA_5_25nsLL_1d_MLP.class.C"
}
namespace TGhostProb {
#include "TMVA/TMVA_6_25nsLL_1d_MLP.class.C"
}


#include "TMath.h"

// local
#include "Run2GhostId.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Run2GhostId
//
// 2015-02-06 : Paul Seyfert
// following an earlier version by Angelo Di Canto
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( Run2GhostId )

namespace {
 static const int largestChannelIDType = 1+std::max(LHCb::LHCbID::channelIDtype::OT,std::max(LHCb::LHCbID::channelIDtype::TT,std::max(LHCb::LHCbID::channelIDtype::Velo,LHCb::LHCbID::channelIDtype::IT)));
 static constexpr int largestTrackTypes = 1+LHCb::Track::Types::Ttrack;
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Run2GhostId::Run2GhostId( const std::string& type, const std::string& name, const IInterface* parent)
: extends ( type, name, parent )
{
  declareProperty("VeloClusterLocation",m_veloclusters);
  declareProperty("TTClusterLocation",m_ttclusters);
  declareProperty("ITClusterLocation",m_itclusters);
}
//=============================================================================
//

StatusCode Run2GhostId::finalize()
{
  m_vectorsizes.clear();
  m_readers.clear();
  m_flatters.clear();
  return GaudiTool::finalize();
}

StatusCode Run2GhostId::initialize()
{
  if( !GaudiTool::initialize() ) return StatusCode::FAILURE;

  if (largestTrackTypes<=std::max(LHCb::Track::Types::Ttrack,std::max(
     std::max(LHCb::Track::Types::Velo,LHCb::Track::Types::Upstream),
     std::max(LHCb::Track::Types::Ttrack,LHCb::Track::Types::Downstream))))
    return Warning("ARRAY SIZE SET WRONG (largestTrackTypes is smaller than enum LHCb::Track::Types",StatusCode::FAILURE);

  m_otHitCreator = tool<Tf::IOTHitCreator>("Tf::OTHitCreator", "OTHitCreator");
  // m_veloExpectation = tool<IVeloExpectation>("VeloExpectation");

  //for (auto entry : m_expectorNames) {
    //m_Expectations.push_back(tool<IHitExpectation>(entry));
  //}

  m_readers.clear();
  m_readers.resize(  largestTrackTypes  );
  m_readers[LHCb::Track::Types::Velo].reset(     new VeloGhostProb::ReadMLP(variableNames(LHCb::Track::Types::Velo)) );
  m_readers[LHCb::Track::Types::Long].reset(     new LongGhostProb::ReadMLP(variableNames(LHCb::Track::Types::Long)) );
  m_readers[LHCb::Track::Types::Upstream].reset( new UpGhostProb::ReadMLP(variableNames(LHCb::Track::Types::Upstream)) );
  m_readers[LHCb::Track::Types::Downstream].reset( new DownGhostProb::ReadMLP(variableNames(LHCb::Track::Types::Downstream)) );
  m_readers[LHCb::Track::Types::Ttrack].reset(   new TGhostProb::ReadMLP(variableNames(LHCb::Track::Types::Ttrack)) );
  m_flatters.clear();
  m_flatters.resize( largestTrackTypes );
  m_flatters[LHCb::Track::Types::Velo].reset( VeloTable() );
  m_flatters[LHCb::Track::Types::Long].reset( LongTable() );
  m_flatters[LHCb::Track::Types::Upstream].reset(  UpstreamTable() );
  m_flatters[LHCb::Track::Types::Downstream].reset(  DownstreamTable() );
  m_flatters[LHCb::Track::Types::Ttrack].reset(  TtrackTable() );

  m_vectorsizes.clear();
  m_vectorsizes.resize( largestTrackTypes, 0 );
  m_vectorsizes[LHCb::Track::Types::Velo] = 11 + 1;
  m_vectorsizes[LHCb::Track::Types::Long] = 21 + 1;
  m_vectorsizes[LHCb::Track::Types::Upstream] = 13 + 1;
  m_vectorsizes[LHCb::Track::Types::Downstream] = 15 + 1;
  m_vectorsizes[LHCb::Track::Types::Ttrack] = 14 + 1;
  return StatusCode::SUCCESS;
}

StatusCode Run2GhostId::beginEvent() {
  // countHits()
  /// only for qmtest
  if (UNLIKELY(m_DaVinci)) {
    const LHCb::RecSummary * rS =
      getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default);
    if ( !rS ) {
      rS = getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default,false);
    }
    if ( !rS ) {
      rS = getIfExists<LHCb::RecSummary>(evtSvc(),"/Event/Turbo/Rec/Summary",false);
    }
    if ( !rS ) {
      return StatusCode::FAILURE;
    }

    m_otHits = rS->info(LHCb::RecSummary::DataTypes::nOTClusters,-1);
    m_veloHits = rS->info(LHCb::RecSummary::DataTypes::nVeloClusters,-1);
    m_itHits = rS->info(LHCb::RecSummary::DataTypes::nITClusters,-1);
    m_ttHits = rS->info(LHCb::RecSummary::DataTypes::nTTClusters,-1);
  } else {
    auto veloCont = m_veloclusters.get();
    auto ttCont = m_ttclusters.get();
    auto itCont = m_itclusters.get();
    if (veloCont) m_veloHits = veloCont->size();
    if (ttCont) m_ttHits = ttCont->size();;
    if (itCont) m_itHits = itCont->size();;
    m_otHits = m_otHitCreator->hits().size();
    if (!(veloCont&&itCont&&ttCont)) return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

namespace {
  inline std::vector<float> subdetectorhits(const LHCb::Track& aTrack) {
    std::vector<float> returnvalue( largestChannelIDType, 0. );
    for (auto lhcbid : aTrack.lhcbIDs()) {
      if (lhcbid.detectorType()>=returnvalue.size()) {continue;} // may be a hit in a non-tracking detector
      returnvalue[lhcbid.detectorType()] += 1.;
    }
    return returnvalue;
  }
}


//=============================================================================
StatusCode Run2GhostId::execute(LHCb::Track& aTrack) const
{
  auto variables = netInputs(aTrack);
  //float netresponse = m_readers[aTrack.type()]->GetRarity(variables); // TODO rarity would be nice, see https://sft.its.cern.ch/jira/browse/ROOT-7050
  float netresponse = m_readers[aTrack.type()]->GetMvaValue(variables);
  netresponse = m_flatters[aTrack.type()]->value(0.5*(1.-netresponse));
  aTrack.setGhostProbability(netresponse);
  return StatusCode::SUCCESS;
}


std::vector<float> Run2GhostId::netInputs(LHCb::Track& aTrack) const
{
  /// code for debuging:
  ///if (true) {
  ///  int veloHits;
  ///  const LHCb::VPClusters* vpCont = NULL;
  ///  vpCont = getIfExists<LHCb::VPClusters>(LHCb::VPClusterLocation::Default);
  ///  if (vpCont) veloHits = vpCont->size();
  ///  if (veloHits != m_veloHits) return Error("This is very bad!!!",StatusCode::FAILURE,10);
  ///}

  std::vector<float> obsarray = subdetectorhits(aTrack);
  const LHCb::TrackFitResult* fit = aTrack.fitResult();

  std::vector<float> variables;
  variables.reserve(m_vectorsizes[aTrack.type()]);
  //if (LHCb::Track::Types::Velo == tracktype || LHCb::Track::Types::Long == tracktype || LHCb::Track::Types::Upstream == tracktype) {
  if (aTrack.hasVelo()) {
    variables.push_back(obsarray[LHCb::LHCbID::channelIDtype::Velo] );
    //variables.push_back(m_veloExpectation->nExpected(aTrack));
    variables.push_back(aTrack.info(LHCb::Track::AdditionalInfo::FitVeloChi2, -999));
    variables.push_back(aTrack.info(LHCb::Track::AdditionalInfo::FitVeloNDoF, -999));
  }
  //if (LHCb::Track::Types::Downstream == tracktype || LHCb::Track::Types::Long == tracktype || LHCb::Track::Types::Upstream == tracktype) {
  if (aTrack.hasTT()) { // includes longtracks w/o ut hits
    variables.push_back(obsarray[LHCb::LHCbID::channelIDtype::TT] );
    //variables.push_back(m_Expectations[0]->nExpected(aTrack));
    variables.push_back((fit->nMeasurements(LHCb::Measurement::Type::TTLite) - fit->nActiveMeasurements(LHCb::Measurement::Type::TTLite)));
  }
  //if (LHCb::Track::Types::Downstream == tracktype || LHCb::Track::Types::Long == tracktype || LHCb::Track::Types::Ttrack == tracktype) {
  if (aTrack.hasT()) {
    variables.push_back(obsarray[LHCb::LHCbID::channelIDtype::IT] );
    variables.push_back(obsarray[LHCb::LHCbID::channelIDtype::OT] );
    //variables.push_back(m_Expectations[1]->nExpected(aTrack)); // IT
    //variables.push_back(m_Expectations[2]->nExpected(aTrack)); // OT
    variables.push_back((fit->nMeasurements(LHCb::Measurement::Type::ITLite) - fit->nActiveMeasurements(LHCb::Measurement::Type::ITLite)));
    float nMeas = fit->nMeasurements( LHCb::Measurement::Type::OT );
    float nOTBad = nMeas-nMeas*aTrack.info(25 , 0 );// info 25 : FitFracUsedOTTimes
    variables.push_back(nOTBad);
    variables.push_back(aTrack.info(LHCb::Track::AdditionalInfo::FitTChi2, -999));
    variables.push_back(aTrack.info(LHCb::Track::AdditionalInfo::FitTNDoF, -999));
  }
  variables.push_back(m_veloHits);
  variables.push_back(  m_ttHits);
  variables.push_back(  m_itHits);
  variables.push_back(  m_otHits);
    variables.push_back(aTrack.pt());
  //if (LHCb::Track::Types::Long != aTrack.type() && LHCb::Track::Types::Downstream != aTrack.type()) {
    variables.push_back(aTrack.chi2());
    if (LHCb::Track::Types::Downstream != aTrack.type()) {
      variables.push_back(aTrack.nDoF());
    }
    variables.push_back(aTrack.chi2PerDoF());
  //}

  if (LHCb::Track::Types::Long == aTrack.type()) {
    variables.push_back(aTrack.info(LHCb::Track::AdditionalInfo::NCandCommonHits, -999));
    variables.push_back(aTrack.info(LHCb::Track::AdditionalInfo::FitMatchChi2, -999));
  }
  variables.push_back(1.f);
  return variables;
}

std::vector<std::string> Run2GhostId::variableNames(LHCb::Track::Types type) const {
  return  s_varsMap.at(type);
}
