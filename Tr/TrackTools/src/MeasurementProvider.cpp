/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// -------------
// Event model
#include "TrackKernel/TrackTraj.h"
#include "Event/Measurement.h"
#include "Event/TrackFitResult.h"

// local
#include "MeasurementProvider.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : MeasurementProvider
//
// 2005-04-14 : Jose Angel Hernando Morata
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( MeasurementProvider )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MeasurementProvider::MeasurementProvider( const std::string& type,
                                          const std::string& name,
                                          const IInterface* parent )
: extends ( type, name , parent )
{
  declareProperty( "VeloRProvider", m_veloRProvider ) ;
  declareProperty( "VeloPhiProvider", m_veloPhiProvider ) ;
  declareProperty( "VPProvider", m_vpProvider ) ;
  declareProperty( "TTProvider", m_ttProvider ) ;
  declareProperty( "UTProvider", m_utProvider ) ;
  declareProperty( "ITProvider", m_itProvider ) ;
  declareProperty( "OTProvider", m_otProvider ) ;
  declareProperty( "FTProvider", m_ftProvider ) ;
  declareProperty( "MuonProvider", m_muonProvider ) ;
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MeasurementProvider::initialize()
{
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "MeasurementProvider::initialize()" << endmsg;
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return sc;  // error already reported by base class

  m_providermap.fill(nullptr) ;

  if(!m_ignoreVelo) {
    sc = m_veloRProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::Type::VeloR] = &(*m_veloRProvider) ;

    sc = m_veloPhiProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::Type::VeloPhi] = &(*m_veloPhiProvider) ;
  } else {
    m_veloRProvider.disable();
    m_veloPhiProvider.disable();
  }

  if(!m_ignoreVP) {
    sc = m_vpProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::Type::VP] = &(*m_vpProvider) ;
  } else {
    m_vpProvider.disable();
  }

  if(!m_ignoreTT) {
    sc = m_ttProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::Type::TT] = &(*m_ttProvider) ;
  } else {
    m_ttProvider.disable();
  }

  if(!m_ignoreUT) {
    sc = m_utProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::Type::UT] = &(*m_utProvider) ;
  } else {
    m_utProvider.disable();
  }

  if(!m_ignoreIT) {
    sc = m_itProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::Type::IT] = &(*m_itProvider) ;
  } else {
    m_itProvider.disable();
  }

  if(!m_ignoreOT) {
    sc = m_otProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::Type::OT] = &(*m_otProvider) ;
  } else {
    m_otProvider.disable();
  }

  if(!m_ignoreFT) {
    sc = m_ftProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::Type::FT] = &(*m_ftProvider) ;
  } else {
    m_ftProvider.disable();
  }

  if(!m_ignoreMuon) {
    sc = m_muonProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::Type::Muon] = &(*m_muonProvider) ;
  } else {
    m_muonProvider.disable();
  }
  return sc ;
}


StatusCode MeasurementProvider::finalize()
{
  StatusCode sc;
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "In MeasurementProvider::finalize. Releasing tool handles." << endmsg ;
  // make sure to release all toolhandles
  if(!m_ignoreVelo) {
    sc = m_veloRProvider.release() ;
    if (sc.isFailure()) return sc;
    sc = m_veloPhiProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreVP) {
    sc = m_vpProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreTT) {
    sc = m_ttProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreUT) {
    sc = m_utProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreIT) {
    sc = m_itProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreOT) {
    sc = m_otProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreFT) {
    sc = m_ftProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreMuon) {
    sc = m_muonProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  sc = GaudiTool::finalize() ;
  return sc ;
}


//=============================================================================
// Load all the Measurements from the list of LHCbIDs on the input Track
//=============================================================================
StatusCode MeasurementProvider::load( Track& track ) const
{
  // make sure we have a place to store the result
  if( !track.fitResult()) track.setFitResult( new LHCb::TrackFitResult() ) ;

  std::vector<LHCbID> newids ; newids.reserve(track.lhcbIDs().size() );
  for ( const auto& id : track.lhcbIDs() ) {
    // First look if the Measurement corresponding to this LHCbID
    // is already in the Track, i.e. whether it has already been loaded!
    if ( track.fitResult()->measurement( id ) ) {
      Warning("Found measurements already loaded on track!",StatusCode::SUCCESS,0).ignore() ;
      if( msgLevel( MSG::DEBUG ) || msgLevel( MSG::VERBOSE ) )
        debug() << "Measurement had already been loaded for the LHCbID"
                << " channelID, detectorType = "
                << id.channelID() << " , " << id.detectorType()
                << "  -> Measurement loading skipped for this LHCbID!"
                << endmsg;
    } else newids.push_back( id ) ;
  }

  // create a reference trajectory
  LHCb::TrackTraj reftraj(track) ;

  // create all measurements for selected IDs
  LHCb::TrackFitResult::MeasurementContainer newmeasurements ;
  newmeasurements.reserve(newids.size());

  addToMeasurements(newids,newmeasurements,reftraj) ;

  // remove all zeros, just in case.
  auto newend = std::remove_if(newmeasurements.begin(),newmeasurements.end(),
                               [](const Measurement* m) { return m==nullptr; } );
  if(newend != newmeasurements.end()) {
    Warning("Some measurement pointers are zero: ",StatusCode::SUCCESS,0).ignore() ;
    if( msgLevel( MSG::DEBUG ) )
      debug() << "Some measurement pointers are zero: " << int(newmeasurements.end() - newend) << endmsg ;
    newmeasurements.erase(newend,newmeasurements.end()) ;
  }

  // add the measurements to the track
  track.fitResult()->addToMeasurements( newmeasurements ) ;

  // Update the status flag of the Track
  track.setPatRecStatus( Track::PatRecStatus::PatRecMeas );

  return StatusCode::SUCCESS;
}

namespace {
LHCb::Measurement::Type measurementtype(const LHCb::LHCbID& id)
{
  switch( id.detectorType() ) {
  case LHCb::LHCbID::channelIDtype::Velo: return id.isVeloR() ? LHCb::Measurement::Type::VeloR
                                               : LHCb::Measurement::Type::VeloPhi ;
  case LHCb::LHCbID::channelIDtype::VP:   return LHCb::Measurement::Type::VP      ;
  case LHCb::LHCbID::channelIDtype::TT:   return LHCb::Measurement::Type::TT      ;
  case LHCb::LHCbID::channelIDtype::UT:   return LHCb::Measurement::Type::UT      ;
  case LHCb::LHCbID::channelIDtype::IT:   return LHCb::Measurement::Type::IT      ;
  case LHCb::LHCbID::channelIDtype::OT:   return LHCb::Measurement::Type::OT      ;
  case LHCb::LHCbID::channelIDtype::FT:   return LHCb::Measurement::Type::FT      ;
  case LHCb::LHCbID::channelIDtype::Muon: return LHCb::Measurement::Type::Muon    ;
  default:                 return LHCb::Measurement::Type::Unknown ;
  }
}
}

//=============================================================================
// Construct a Measurement of the type of the input LHCbID
//=============================================================================

Measurement* MeasurementProvider::measurement ( const LHCbID& id, bool localY ) const
{
  const IMeasurementProvider* provider = m_providermap[measurementtype( id )] ;
  return provider ? provider->measurement(id, localY) : nullptr ;
}

Measurement* MeasurementProvider::measurement ( const LHCbID& id, const LHCb::ZTrajectory<double>& state, bool localY ) const
{
  const IMeasurementProvider* provider = m_providermap[measurementtype( id )] ;
  return provider ? provider->measurement(id,state, localY) : nullptr ;
}

//-----------------------------------------------------------------------------
/// Create a list of measurements from a list of LHCbIDs
//-----------------------------------------------------------------------------
void MeasurementProvider::addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                                             std::vector<LHCb::Measurement*>& measurements,
                                             const LHCb::ZTrajectory<double>& tracktraj ) const
{
  // dispatch the measurements according to their type.
  for ( unsigned i=0; i<m_providermap.size(); ++i ) {
    const auto* provider = m_providermap[i];
    if (!provider) continue;
    auto pivot = std::stable_partition( ids.begin(), ids.end(),
                                        [&](const LHCb::LHCbID& id)
                                        { return measurementtype(id) == i; } );
    auto n = std::distance( ids.begin(), pivot );
    provider->addToMeasurements( ids.first(n), measurements, tracktraj );
    ids = ids.subspan(n);
  }
}

//=============================================================================
