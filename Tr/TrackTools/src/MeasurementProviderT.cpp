/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class MeasurementProviderT MeasurementProviderT.cpp
 *
 * Implementation of templated MeasurementProvider tool
 * see interface header for description
 *
 *  @author W. Hulsbergen
 *  @date   07/06/2007
 */

#include "TrackInterfaces/IMeasurementProvider.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"
#include <type_traits>
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "Event/TrackParameters.h"
#include "PrKernel/UTHitHandler.h"

template <typename T>
class MeasurementProviderT : public extends< GaudiTool,
                                             IMeasurementProvider >
{
public:
  /// constructer
  MeasurementProviderT(const std::string& type, const std::string& name,
                       const IInterface* parent) : extends( type, name , parent ) { }

  StatusCode initialize() override final;
  LHCb::Measurement* measurement( const LHCb::LHCbID& id, bool /*localY*/ ) const override final;
  LHCb::Measurement* measurement( const LHCb::LHCbID& id, const LHCb::ZTrajectory<double>& refvector,
                                          bool /*localY*/  ) const override final;
  void addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                          std::vector<LHCb::Measurement*>& measurements,
                          const LHCb::ZTrajectory<double>& reftraj) const override final;

  StatusCode load( LHCb::Track&  ) const override final {
    info() << "sorry, MeasurementProviderBase::load not implemented" << endmsg ;
    return StatusCode::FAILURE ;
  }

private:
  DataObjectReadHandle<typename T::ClusterContainerType> m_clustersDH { this,  "ClusterLocation", T::defaultClusterLocation() };

  Gaudi::Property<bool> m_useReference { this, "UseReference", true };
  ToolHandle<typename T::PositionToolType> m_positiontool = { T::positionToolName() };
  const typename T::DetectorType* m_det = nullptr;
};

// local
#include "GaudiKernel/IIncidentSvc.h"
#include "Event/StateVector.h"
#include "Event/Measurement.h"
#include "TrackKernel/ZTrajectory.h"

//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------

template <typename T>
StatusCode MeasurementProviderT<T>::initialize()
{
  StatusCode sc = extends::initialize();
  if( sc.isFailure() ) { return Error( "Failed to initialize!", sc ); }

  // Retrieve the detector element
  m_det = getDet<typename T::DetectorType>( T::defaultDetectorLocation() );

  return sc;
}

////////////////////////////////////////////////////////////////////////////////////////
// Template instantiations using Traits classes
////////////////////////////////////////////////////////////////////////////////////////

#include "TrackInterfaces/IVeloClusterPosition.h"
#include "TrackInterfaces/IVPClusterPosition.h"
#include "TrackInterfaces/ISTClusterPosition.h"
#include "TrackInterfaces/IUTClusterPosition.h"

#include "Event/STCluster.h"
#include "Event/STLiteCluster.h"
#include "Event/UTCluster.h"
#include "Event/UTLiteCluster.h"
#include "Event/VeloCluster.h"
#include "Event/VeloLiteCluster.h"
#include "Event/VPCluster.h"
#include "Event/VPLightCluster.h"

#include "Event/VeloRMeasurement.h"
#include "Event/VeloLiteRMeasurement.h"
#include "Event/VeloPhiMeasurement.h"
#include "Event/VeloLitePhiMeasurement.h"
#include "Event/STMeasurement.h"
#include "Event/STLiteMeasurement.h"
#include "Event/UTLiteMeasurement.h"
#include "Event/VPMeasurement.h"

#include "STDet/DeSTDetector.h"
#include "UTDet/DeUTDetector.h"
#include "VeloDet/DeVelo.h"
#include "VPDet/DeVP.h"

namespace MeasurementProviderTypes {

  //////////// Velo
  struct VeloBase {
    typedef IVeloClusterPosition     PositionToolType ;
    typedef DeVelo                       DetectorType ;
    static std::string positionToolName() { return "VeloClusterPosition" ; }
    static const std::string& defaultDetectorLocation() { return DeVeloLocation::Default ; }
    static LHCb::VeloChannelID channelId( const LHCb::LHCbID& id ) { return id.veloID(); }
  };

  struct VeloPhiBase : VeloBase {
    static bool checkType(const LHCb::LHCbID& id) { return id.isVelo() && id.veloID().isPhiType() ; }
    static double nominalZ(const DeVelo& det, const LHCb::LHCbID& id) { return det.phiSensor( id.veloID() )->z() ; }
  };
  struct VeloRBase : VeloBase {
    static bool checkType(const LHCb::LHCbID& id) { return id.isVelo() && id.veloID().isRType() ; }
    static double nominalZ(const DeVelo& det, const LHCb::LHCbID& id) { return det.rSensor( id.veloID() )->z() ; }
  };

  struct VeloLiteBase {
    typedef LHCb::VeloLiteCluster::VeloLiteClusters   ClusterContainerType ;
    static const std::string& defaultClusterLocation() { return LHCb::VeloLiteClusterLocation::Default ; }
  };
  struct VeloFullBase {
    typedef LHCb::VeloClusters       ClusterContainerType ;
    static const std::string& defaultClusterLocation() { return LHCb::VeloClusterLocation::Default ; }
  };

  struct VeloR : VeloFullBase, VeloRBase {
    typedef LHCb::VeloRMeasurement   MeasurementType ;
  };
  struct VeloLiteR : VeloLiteBase, VeloRBase {
    typedef LHCb::VeloLiteRMeasurement   MeasurementType ;
  };
  struct VeloPhi : VeloFullBase, VeloPhiBase {
    typedef LHCb::VeloPhiMeasurement MeasurementType ;
  };
  struct VeloLitePhi : VeloLiteBase, VeloPhiBase {
    typedef LHCb::VeloLitePhiMeasurement MeasurementType ;
  };

  //////////// VP
  struct VP {
    typedef IVPClusterPosition     PositionToolType ;
    typedef DeVP                       DetectorType ;
    static std::string positionToolName() { return "VPClusterPosition"; }
    static const std::string& defaultDetectorLocation() { return DeVPLocation::Default ; }
    static LHCb::VPChannelID channelId( const LHCb::LHCbID& id ) { return id.vpID(); }
    typedef LHCb::VPLightClusters   ClusterContainerType ;
    static const std::string& defaultClusterLocation() { return LHCb::VPClusterLocation::Light; }
    typedef LHCb::VPMeasurement MeasurementType ;
    static bool checkType(const LHCb::LHCbID& id) { return id.isVP(); }
    static double nominalZ(const DetectorType& det, const LHCb::LHCbID& id) { return det.sensorOfChannel( id.vpID() )->z() ; }
  };

  //////////// ST
  struct STBase {
    typedef ISTClusterPosition       PositionToolType ;
    typedef DeSTDetector             DetectorType ;
    static LHCb::STChannelID channelId( const LHCb::LHCbID& id ) { return id.stID() ; }
    static double nominalZ(const DetectorType& det, const LHCb::LHCbID& id) {
        // extremely ugly. need more functionality in det elements to do this quicker.
        return det.findSector(id.stID())->globalCentre().z() ;
    }
  };

  struct STFullBase : STBase {
    typedef LHCb::STMeasurement      MeasurementType ;
    typedef LHCb::STClusters         ClusterContainerType ;
  };
  struct STLiteBase : STBase {
    typedef LHCb::STLiteMeasurement      MeasurementType ;
    typedef LHCb::STLiteCluster::STLiteClusters         ClusterContainerType ;
  };

  struct TTBase {
    static const std::string& defaultDetectorLocation() { return DeSTDetLocation::TT ; }
    static bool checkType(const LHCb::LHCbID& id) { return id.isTT() ; }
  };
  struct ITBase {
    static const std::string& defaultDetectorLocation() { return DeSTDetLocation::IT ; }
    static bool checkType(const LHCb::LHCbID& id) { return id.isIT() ; }
  };

  struct TT : STFullBase, TTBase {
    static std::string positionToolName() { return "STOfflinePosition/TTClusterPosition" ; }
    static const std::string& defaultClusterLocation() { return LHCb::STClusterLocation::TTClusters ; }
  };
  struct TTLite :STLiteBase, TTBase {
    static std::string positionToolName() { return "STOnlinePosition/TTLiteClusterPosition" ; }
    static const std::string& defaultClusterLocation() { return LHCb::STLiteClusterLocation::TTClusters ; }
  };

  struct UTLite {
    typedef IUTClusterPosition       PositionToolType ;
    typedef DeUTDetector             DetectorType ;
    static LHCb::UTChannelID channelId( const LHCb::LHCbID& id ) { return id.utID() ; }
    static double nominalZ(const DetectorType& det, const LHCb::LHCbID& id) {
        // extremely ugly. need more functionality in det elements to do this quicker.
        return det.findSector(id.utID())->globalCentre().z() ;
    }
    typedef LHCb::UTLiteMeasurement      MeasurementType ;
    typedef UT::HitHandler         ClusterContainerType ;
    static const std::string& defaultDetectorLocation() { return DeUTDetLocation::UT; }
    static bool checkType(const LHCb::LHCbID& id) { return id.isUT() ; }
    static std::string positionToolName() { return "UTOnlinePosition/UTLiteClusterPosition" ; }
    static const std::string& defaultClusterLocation() { return UT::Info::HitLocation ; }
  };

  struct IT : STFullBase, ITBase {
    static std::string positionToolName() { return "STOfflinePosition/ITClusterPosition" ; }
    static const std::string& defaultClusterLocation() { return LHCb::STClusterLocation::ITClusters ; }
  };
  struct ITLite : STLiteBase, ITBase {
    static std::string positionToolName() { return "STOnlinePosition/ITLiteClusterPosition" ; }
    static const std::string& defaultClusterLocation() { return LHCb::STLiteClusterLocation::ITClusters; }
  };
}

//-----------------------------------------------------------------------------
/// Create a measurement
//-----------------------------------------------------------------------------

template <typename T>
LHCb::Measurement* MeasurementProviderT<T>::measurement( const LHCb::LHCbID& id, bool ) const
{
  const auto* clus = ( LIKELY( T::checkType(id) ) ? m_clustersDH.get()->object( T::channelId(id) ) : nullptr );
  return LIKELY(clus!=nullptr) ? new typename T::MeasurementType( *clus, *m_det, *m_positiontool )
                               : nullptr;
}

template <>
LHCb::Measurement* MeasurementProviderT<MeasurementProviderTypes::VP>::measurement( const LHCb::LHCbID& id, bool localY ) const
{
  if(UNLIKELY(!id.isVP()))return nullptr;
  const auto& c = *(m_clustersDH.get());
  const auto clus = std::find_if( c.begin(),  c.end(),
                                  [id_to_look_for = id.vpID().channelID()](const LHCb::VPLightCluster& clus) {
                                    return clus.channelID() == id_to_look_for;
                                  });
  //info() << "Cluster1: " << clus->second.channelID() << " " << clus->second.x() << " " << clus->second.y() << " " << clus->second.z() << endmsg;
  return LIKELY(clus != c.end()) ?  new LHCb::VPMeasurement(*clus,
                                                            m_positiontool->position(*clus),
                                                            localY ?  LHCb::VPMeasurement::VPMeasurementType::Y : LHCb::VPMeasurement::VPMeasurementType::X)
                               : nullptr;
}

template <>
LHCb::Measurement* MeasurementProviderT<MeasurementProviderTypes::UTLite>::measurement( const LHCb::LHCbID& id, bool ) const
{
  const auto& hitHandler = *(m_clustersDH.get());
  auto id_to_look_for = id.utID();
  auto& hits = hitHandler.hits(id_to_look_for.station(),
                               id_to_look_for.layer(),
                               id_to_look_for.detRegion(),
                               id_to_look_for.sector());
  const auto& hit = std::find_if( hits.begin(), hits.end(),
                                  [id_to_look_for](const UT::Hit& hit) {
                                    return hit.chanID() == id_to_look_for;
                                  });
  if (hit == hits.end()) {
    return nullptr;
  } else {
    return new LHCb::UTLiteMeasurement(*hit, *m_det, *m_positiontool);
  }
}

//-----------------------------------------------------------------------------
/// Create a measurement with statevector. For now very inefficient.
//-----------------------------------------------------------------------------

template <typename T>
LHCb::Measurement* MeasurementProviderT<T>::measurement( const LHCb::LHCbID& id,
                                                         const LHCb::ZTrajectory<double>& reftraj,
                                                         bool localY ) const
{
  if( UNLIKELY(!m_useReference) ) return measurement( id, localY ) ;
  const auto* clus = ( LIKELY( T::checkType(id) ) ? m_clustersDH.get()->object( T::channelId(id) ) : nullptr );
  return LIKELY(clus!=nullptr) ? new typename T::MeasurementType( *clus, *m_det, *m_positiontool,
                                                 reftraj.stateVector( T::nominalZ(*m_det,id) ))
                               : nullptr;
}

template <>
LHCb::Measurement* MeasurementProviderT<MeasurementProviderTypes::VP>::measurement( const LHCb::LHCbID& id,
                                                         const LHCb::ZTrajectory<double>& reftraj,
                                                         bool localY ) const
{
  if( UNLIKELY(!m_useReference) ) return measurement( id, localY ) ;
  const auto& c = *(m_clustersDH.get());
  const auto clus = std::find_if( c.begin(),  c.end(),
                                  [id_to_look_for = id.vpID().channelID()](const LHCb::VPLightCluster& clus) {
                                    return clus.channelID() == id_to_look_for;
                                  });
  //info() << "Cluster2: " << clus->second.channelID() << " " << clus->second.x() << " " << clus->second.y() << " " << clus->second.z() << endmsg;
  if (UNLIKELY( clus==c.end() || !id.isVP() )) return nullptr;
  LHCb::StateVector sv = reftraj.stateVector(MeasurementProviderTypes::VP::nominalZ(*m_det,id));
  return new LHCb::VPMeasurement(*clus,
                                 m_positiontool->position(*clus, sv.position(), sv.tx(), sv.ty()),
                                 localY ?  LHCb::VPMeasurement::VPMeasurementType::Y : LHCb::VPMeasurement::VPMeasurementType::X);
}

template <>
LHCb::Measurement* MeasurementProviderT<MeasurementProviderTypes::UTLite>::measurement( const LHCb::LHCbID& id,
                                                                                        const LHCb::ZTrajectory<double>& reftraj,
                                                                                        bool localY ) const
{
  if( UNLIKELY(!m_useReference) ) return measurement( id, localY ) ;
  const auto& hitHandler = *(m_clustersDH.get());
  auto id_to_look_for = id.utID();
  auto& hits = hitHandler.hits(id_to_look_for.station(),
                               id_to_look_for.layer(),
                               id_to_look_for.detRegion(),
                               id_to_look_for.sector());
  const auto& hit = std::find_if( hits.begin(), hits.end(),
                                  [id_to_look_for](const UT::Hit& hit) {
                                    return hit.chanID() == id_to_look_for;
                                  });
  if (hit == hits.end()) {
    return nullptr;
  } else {
    return new LHCb::UTLiteMeasurement(*hit, *m_det, *m_positiontool,
                                       reftraj.stateVector( MeasurementProviderTypes::UTLite::nominalZ(*m_det,id) ));
  }
}

//-----------------------------------------------------------------------------
/// Create measurements for list of LHCbIDs
//-----------------------------------------------------------------------------

template <typename T>
void MeasurementProviderT<T>::addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                                                 std::vector<LHCb::Measurement*>& measurements,
                                                 const LHCb::ZTrajectory<double>& reftraj) const
{
  measurements.reserve(measurements.size()+ids.size());
  std::transform( ids.begin(), ids.end(),
                  std::back_inserter(measurements),
                  [&](const LHCb::LHCbID& id)
                  { return this->measurement(id,reftraj,false); } );
}

template <>
void MeasurementProviderT<MeasurementProviderTypes::VP>::addToMeasurements(LHCb::span<LHCb::LHCbID> ids,
                                                 std::vector<LHCb::Measurement*>& measurements,
                                                 const LHCb::ZTrajectory<double>& ref) const {
  measurements.reserve(measurements.size()+2*ids.size());
  std::for_each( ids.begin(), ids.end(),
                 [&](const LHCb::LHCbID& id) {
                    measurements.push_back(measurement(id, ref, false));
                    measurements.push_back(measurement(id, ref, true));
                 } );
}

typedef MeasurementProviderT<MeasurementProviderTypes::VeloR> VeloRMeasurementProvider ;
DECLARE_COMPONENT( VeloRMeasurementProvider )
typedef MeasurementProviderT<MeasurementProviderTypes::VeloLiteR> VeloLiteRMeasurementProvider ;
DECLARE_COMPONENT( VeloLiteRMeasurementProvider )
typedef MeasurementProviderT<MeasurementProviderTypes::VeloPhi> VeloPhiMeasurementProvider ;
DECLARE_COMPONENT( VeloPhiMeasurementProvider )
typedef MeasurementProviderT<MeasurementProviderTypes::VeloLitePhi> VeloLitePhiMeasurementProvider ;
DECLARE_COMPONENT( VeloLitePhiMeasurementProvider )
typedef MeasurementProviderT<MeasurementProviderTypes::VP> VPMeasurementProvider;
DECLARE_COMPONENT( VPMeasurementProvider )
typedef MeasurementProviderT<MeasurementProviderTypes::TT> TTMeasurementProvider ;
DECLARE_COMPONENT( TTMeasurementProvider )
typedef MeasurementProviderT<MeasurementProviderTypes::TTLite> TTLiteMeasurementProvider ;
DECLARE_COMPONENT( TTLiteMeasurementProvider )
typedef MeasurementProviderT<MeasurementProviderTypes::UTLite> UTLiteMeasurementProvider ;
DECLARE_COMPONENT( UTLiteMeasurementProvider )
typedef MeasurementProviderT<MeasurementProviderTypes::IT> ITMeasurementProvider ;
DECLARE_COMPONENT( ITMeasurementProvider )
typedef MeasurementProviderT<MeasurementProviderTypes::ITLite> ITLiteMeasurementProvider ;
DECLARE_COMPONENT( ITLiteMeasurementProvider )
