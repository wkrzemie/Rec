/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TrackCaloMatch_H
#define _TrackCaloMatch_H

/** @class TrackCaloMatch TrackCaloMatch.h
 *
 * Implementation of TrackCaloMatch tool
 * see interface header for description
 *
 *  @author M.Needham
 *  @date   30/12/2005
 */

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"

#include "TrackInterfaces/ITrackCaloMatch.h"

#include <string>
#include "Relations/IRelation.h"

#include "Event/Track.h"

class TrackCaloMatch: public extends<GaudiTool, ITrackCaloMatch, IIncidentListener>  {

public:

  /// constructor
  using extends::extends;

  StatusCode initialize() override;

  /// the method
  double energy(const LHCb::Track& aTrack) const override;


  /** Implement the handle method for the Incident service.
  *  This is used to inform the tool of software incidents.
  *
  *  @param incident The incident identifier
  */
  void handle( const Incident& incident ) override;

private:

  void initEvent() const;

  typedef IRelation<LHCb::Track,float> Table ;
  double energy(const LHCb::Track& aTrack, const TrackCaloMatch::Table* table ) const;

  mutable Table* m_ecalE = nullptr;
  mutable Table* m_hcalE = nullptr;
  mutable Table* m_psE = nullptr;

  std::string m_ecalLocation;
  std::string m_hcalLocation;
  std::string m_prsLocation;

  Gaudi::Property<double> m_alpha { this, "alpha", 8. };
  Gaudi::Property<double> m_beta { this, "beta", 1. };
  Gaudi::Property<double> m_gamma { this, "gamma", 1. };

  mutable bool m_configured = false;

};

#endif
