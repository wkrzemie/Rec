/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKEXTRAPOLATORS_TRACKMASTEREXTRAPOLATOR_H
#define TRACKEXTRAPOLATORS_TRACKMASTEREXTRAPOLATOR_H 1

// Include files

// local
#include "GaudiKernel/ToolHandle.h"
#include "TrackExtrapolator.h"
#include "DetDesc/ILVolume.h"

// Forward declarations
struct ITransportSvc;
struct IStateCorrectionTool;
struct ITrackExtraSelector;
struct IMaterialLocator ;
class Material;

/** @class TrackMasterExtrapolator TrackMasterExtrapolator.h \
 *         "TrackMasterExtrapolator.h"
 *
 *  A TrackMasterExtrapolator is a ITrackExtrapolator
 *  which calls the other extrapolators to do the extrapolating.
 *  It takes into account:
 *  @li Detector Material (multiple scattering , energy loss)
 *  @li Deals with electrons
 *  @li Checks the input state vector
 *  @li The actual extrapolation is chosen by the extrapolator selector \
 *       m_extraSelector
 *
 *  @author Edwin Bos (added extrapolation methods)
 *  @date   05/07/2005
 *  @author Jose A. Hernando
 *  @date   15-03-05
 *  @author Matt Needham
 *  @date   22-04-2000
 */

class TrackMasterExtrapolator: public TrackExtrapolator
{

public:
  /// Constructor
  TrackMasterExtrapolator( const std::string& type,
                           const std::string& name,
                           const IInterface* parent );

  /// intialize
  StatusCode initialize() override;

  /// finalize
  StatusCode finalize() override;

  using TrackExtrapolator::propagate;

  /// Propagate a state vector from zOld to zNew
  /// Transport matrix is calulated when transMat pointer is not NULL
  StatusCode propagate( Gaudi::TrackVector& stateVec,
                        double zOld,
                        double zNew,
                        Gaudi::TrackMatrix* transMat,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a state to a given z-position
  /// Transport matrix is calulated when transMat pointer is not NULL
  StatusCode propagate( LHCb::State& state,
                        double z,
                        Gaudi::TrackMatrix* transMat,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

private:
  /// extra selector
  ToolHandle<ITrackExtraSelector> m_extraSelector { "TrackDistanceExtraSelector",this };

  /// transport service
  IMaterialLocator* m_materialLocator = nullptr;

  // job options
  Gaudi::Property<std::string> m_materialLocatorname { this, "MaterialLocator", "DetailedMaterialLocator" } ;  ///< name of materialLocator
  Gaudi::Property<bool>   m_applyMultScattCorr { this,  "ApplyMultScattCorr", true };       ///< turn on/off multiple scattering correction
  Gaudi::Property<bool>   m_applyEnergyLossCorr { this,  "ApplyEnergyLossCorr", true };       ///< turn on/off dE/dx correction
  Gaudi::Property<double> m_maxStepSize { this,  "MaxStepSize", 1000.*Gaudi::Units::mm };   ///< maximum length of a step
  Gaudi::Property<double> m_maxSlope { this,  "MaxSlope", 5. };         ///< maximum slope of state vector
  Gaudi::Property<double> m_maxTransverse { this,  "MaxTransverse", 10.*Gaudi::Units::m };      ///< maximum x,y position of state vector

  /// turn on/off electron energy loss corrections
  Gaudi::Property<bool> m_applyElectronEnergyLossCorr { this, "ApplyElectronEnergyLossCorr", true };
  //Gaudi::Property<double> m_startElectronCorr{ this, "StartElectronCorr", 2500.*mm };  ///< z start for electron energy loss
  //Gaudi::Property<double> m_stopElectronCorr { this, "StopElectronCorr",  9000.*mm };  ///< z start for electron energy loss

};

#endif // TRACKEXTRAPOLATORS_TRACKMASTEREXTRAPOLATOR_H
