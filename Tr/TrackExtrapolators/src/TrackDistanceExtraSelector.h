/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TrackDistanceExtraSelector_H
#define TrackDistanceExtraSelector_H 1

// Include files

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackExtraSelector.h"

#include <string>

/** @class TrackDistanceExtraSelector "TrackDistanceExtraSelector.h"
*
*  Distance selection of one extrapolator
*
*/

class TrackDistanceExtraSelector: public extends<GaudiTool, ITrackExtraSelector> {

public:
  using base_class::base_class;

  StatusCode initialize() override;

  const ITrackExtrapolator* select( double zStart,
                                    double zEnd ) const override;

 private:
  const ITrackExtrapolator* m_shortDistanceExtrapolator = nullptr;
  const ITrackExtrapolator* m_longDistanceExtrapolator = nullptr;
  Gaudi::Property<double> m_shortDist { this,  "shortDist", 100.0*Gaudi::Units::mm };
  /// extrapolator to use for short transport in mag field
  Gaudi::Property<std::string> m_shortDistanceExtrapolatorType { this, "ShortDistanceExtrapolatorType", "TrackParabolicExtrapolator" };
  /// extrapolator to use for long transport in mag field
  Gaudi::Property<std::string> m_longDistanceExtrapolatorType { this, "LongDistanceExtrapolatorType", "TrackRungeKuttaExtrapolator" };
};

#endif // TrackDistanceExtraSelector_H
