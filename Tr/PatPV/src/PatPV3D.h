/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATPV_PATPV3D_H
#define PATPV_PATPV3D_H 1

// Include files
// -------------
// From Gaudi
#include "GaudiAlg/Transformer.h"
// Interfaces
#include "TrackInterfaces/IPVOfflineTool.h"
// Local
#include "Event/RecVertex.h"

/** @class PatPV3D PatPV3D.h
 *  Algorithm to find the primary vertices at the HLT.
 *
 *  @author Eduardo Rodrigues
 *  @author Sebastien Ponce
 */

//-----------------------------------------------------------------------------

typedef Gaudi::Functional::MultiTransformerFilter<
    std::tuple<std::vector<LHCb::RecVertex>>(const std::vector<LHCb::Track>&)
  > PatPV3DBaseClass;

class PatPV3D : public PatPV3DBaseClass {
public:
  /// Standard constructor
  PatPV3D(const std::string& name, ISvcLocator* pSvcLocator);

  /// Algorithm execution
  std::tuple<bool, std::vector<LHCb::RecVertex>> operator()(const std::vector<LHCb::Track>&) const override;

private:
  Gaudi::Property<bool> m_refitpv { this,  "RefitPV", false, "Flag to refit PVs when converting to type PrimaryVertex" };
  ToolHandle<IPVOfflineTool> m_pvsfit {"PVOfflineTool",this};   // PV fitting tool

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nbPVsCounter{ this, "Nb PVs" };
};
#endif // PATPV_PATPV3D_H
