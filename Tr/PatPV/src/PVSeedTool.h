/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATPV_PVSEEDTOOL_H
#define PATPV_PVSEEDTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "IPVSeeding.h"            // Interface
#include "Event/Track.h"


// auxiliary class for searching of clusters of tracks
struct vtxCluster final {

  double  z = 0;            // z of the cluster
  double  sigsq = 0;        // sigma**2 of the cluster
  double  sigsqmin = 0;     // minimum sigma**2 of the tracks forming cluster
  int     ntracks = 0;      // number of tracks in the cluster
  int     not_merged = 0;   // flag for iterative merging

  vtxCluster() = default;

};


/** @class PVSeedTool PVSeedTool.h tmp/PVSeedTool.h
 *
 *
 *  @author Mariusz Witek
 *  @date   2005-11-19
 */
class PVSeedTool : public extends<GaudiTool, IPVSeeding> {
public:

  /// Standard constructor
  using extends::extends;

  std::vector<Gaudi::XYZPoint>
  getSeeds(const std::vector<const LHCb::Track*>& inputTracks,
		   const Gaudi::XYZPoint& beamspot) const override;

private:

  std::vector<double> findClusters(std::vector<vtxCluster>& vclus) const;
  void errorForPVSeedFinding(double tx, double ty, double &sigzaq) const;

  double zCloseBeam(const LHCb::Track* track, const Gaudi::XYZPoint& beamspot) const;

  // steering parameters for merging procedure
  Gaudi::Property<double> m_maxChi2Merge { this,  "maxChi2Merge", 25.};
  Gaudi::Property<double> m_factorToIncreaseErrors { this,  "factorToIncreaseErrors" , 15. };

  // steering parameters for final cluster selection
  Gaudi::Property<int>    m_minClusterMult { this,  "minClusterMult", 3 };
  Gaudi::Property<double> m_dzCloseTracksInCluster { this,  "dzCloseTracksInCluster",  5.*Gaudi::Units::mm };
  Gaudi::Property<int>    m_minCloseTracksInCluster { this,  "minCloseTracksInCluster",  3 };
  Gaudi::Property<int>    m_highMult { this,  "highMult", 10 };
  Gaudi::Property<double> m_ratioSig2HighMult { this,  "ratioSig2HighMult", 1.0 };
  Gaudi::Property<double> m_ratioSig2LowMult { this,  "ratioSig2LowMult", 0.9 };

  double  m_scatCons = 0;     // calculated from m_x0MS
  Gaudi::Property<double> m_x0MS {this, "x0MS", 0.01,
      [=](Property&) {
        double X0 = this->m_x0MS;
        this->m_scatCons = (13.6*sqrt(X0)*(1.+0.038*log(X0)));
      },Gaudi::Details::Property::ImmediatelyInvokeHandler{true}
  };// X0 (tunable) of MS to add for extrapolation of
    // track parameters to PV

};
#endif // PATPV_PVSEEDTOOL_H
