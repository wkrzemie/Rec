/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "PatPV3DFuture.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "DetDesc/Condition.h"

DECLARE_COMPONENT(PatPV3DFuture)

PatPV3DFuture::PatPV3DFuture(const std::string& name,
                             ISvcLocator* pSvcLocator) :
MultiTransformerFilter(name , pSvcLocator,
                       KeyValue{"InputTracks", LHCb::TrackLocation::Default},
                       KeyValue("OutputVerticesName", LHCb::Event::v2::RecVertexLocation::Velo3D)) {
}


namespace {

  static const std::string s_beamSpotCond = "/dd/Conditions/Online/Velo/MotionSystem";
  constexpr double s_myZero = 1E-12;

  bool isChi2Separated( const PatPV3DFuture::RecVertex& rvtx,
                        LHCb::span<const PatPV3DFuture::RecVertex> outvtxvec,
                        double minAllowedChi2) {
    return std::none_of( outvtxvec.begin(), outvtxvec.end(),
                         [rz      = rvtx.position().z(),
                          sigma2z = rvtx.covMatrix()(2,2),
                          minAllowedChi2 ]
                         (const PatPV3DFuture::RecVertex& v) {
                           return std::pow(rz - v.position().z(), 2)/(sigma2z+v.covMatrix()(2,2)) < minAllowedChi2;
                         } );
  }

  bool  vtxcomp( vtxCluster *first, vtxCluster *second ) {
    return first->z < second->z;
  }
  bool  multcomp( vtxCluster *first, vtxCluster *second ) {
    return first->ntracks > second->ntracks;
  }

  // auxiliary class for merging procedure of tracks/clusters
  struct pair_to_merge final {
    vtxCluster* first = nullptr;  // pointer to first cluster to be merged
    vtxCluster* second = nullptr; // pointer to second cluster to be merged
    double chi2dist = 10.e+10;    // a chi2dist = zdistance**2/(sigma1**2+sigma2**2)
    pair_to_merge(vtxCluster* f, vtxCluster* s, double chi2) : first(f), second(s), chi2dist(chi2) {}
  };

  bool paircomp( const pair_to_merge &first, const pair_to_merge &second ) {
    return first.chi2dist < second.chi2dist;
  }

  constexpr static const int s_p2mstatic = 5000;

  double zCloseBeam(const LHCb::Event::v2::Track& track) {
    Gaudi::XYZVector unitVect = track.firstState().slopes().Unit();
    const LHCb::State& stateG = track.firstState();
    return stateG.z() - unitVect.z() *
      (unitVect.x() * stateG.x() + unitVect.y() * stateG.y()) /
      (1.0 - std::pow(unitVect.z(),2));
  }

  std::vector<PatPV3DFuture::RecVertex>
  storeDummyVertices(const std::vector<Gaudi::XYZPoint>& seeds,
                     const std::vector<const PatPV3DFuture::Track*>& rtracks)
  {
    std::vector<PatPV3DFuture::RecVertex> out; out.reserve(seeds.size());
    for (const auto& seed : seeds) {
      double errData[9]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
      Gaudi::SymMatrix3x3 errMat{errData, 9};
      out.emplace_back(seed, errMat, LHCb::Event::v2::Track::Chi2PerDoF{99999.0, 1});
      auto& tVertex = out.back();
      // Fill close tracks
      auto is_close = [z=seed.Z()](const auto* trk) {
        return std::abs(zCloseBeam(*trk)-z) < 3.0 * Gaudi::Units::mm;
      };
      for(const auto& trk : rtracks) {
        if (is_close(trk)) tVertex.addToTracks(trk);
      }
    }
    return out;
  }

  double zCloseBeamOfflineTool(const PatPV3DFuture::Track* track,
                               const Gaudi::XYZPoint& beamspot) {
    Gaudi::XYZPoint tpoint = track->position();
    Gaudi::XYZVector tdir = track->slopes();
    double wx = ( 1. + tdir.x() * tdir.x() ) / track->firstState().errX2();
    double wy = ( 1. + tdir.y() * tdir.y() ) / track->firstState().errY2();
    double x0 = tpoint.x() - tpoint.z() * tdir.x() - beamspot.X();
    double y0 = tpoint.y() - tpoint.z() * tdir.y() - beamspot.Y();
    double den = wx * tdir.x() * tdir.x() + wy * tdir.y() * tdir.y();
    double zAtBeam = - ( wx * x0 * tdir.x() + wy * y0 * tdir.y() ) / den ;
    double xb = tpoint.x() + tdir.x() * ( zAtBeam - tpoint.z() ) - beamspot.X();
    double yb = tpoint.y() + tdir.y() * ( zAtBeam - tpoint.z() ) - beamspot.Y();
    double r2AtBeam = xb*xb + yb*yb ;
    return r2AtBeam < 0.5*0.5 ? zAtBeam : 10e8;
  }

}

StatusCode PatPV3DFuture::initialize() {
  const StatusCode sc = MultiTransformerFilter::initialize();
  if ( sc.isFailure() ) return sc;
  if (m_useBeamSpotRCut.value() ) {
    IUpdateManagerSvc* m_updMgrSvc = svc<IUpdateManagerSvc>("UpdateManagerSvc", true);
    m_updMgrSvc->registerCondition(this, s_beamSpotCond, &PatPV3DFuture::UpdateBeamSpot);
    StatusCode scu = m_updMgrSvc->update(this);
    if(!scu.isSuccess())
      return Error("Failed to update conditions!",scu);
  }
  return StatusCode::SUCCESS;
}

StatusCode PatPV3DFuture::UpdateBeamSpot()
{
  if ( !exist<Condition>(detSvc(), s_beamSpotCond) ){
    Warning( "Unable to locate beam spot condition" ).ignore() ;
    return StatusCode::FAILURE;
  }
  Condition *myCond =  get<Condition>(detSvc(), s_beamSpotCond );
  //
  const double xRC = myCond -> paramAsDouble ( "ResolPosRC" ) ;
  const double xLA = myCond -> paramAsDouble ( "ResolPosLA" ) ;
  const double   Y = myCond -> paramAsDouble ( "ResolPosY"  ) ;
  //
  m_beamSpotX = ( xRC + xLA ) / 2;
  m_beamSpotY = Y ;

  m_veloClosed = (std::abs ( xRC - m_beamSpotX ) < m_resolverBound.value()  &&
                  std::abs ( xLA - m_beamSpotX ) < m_resolverBound.value() );

  return StatusCode::SUCCESS;
}

std::tuple<bool, std::vector<PatPV3DFuture::RecVertex>>
PatPV3DFuture::operator()(const std::vector<PatPV3DFuture::Track>& inputTracks) const {
  std::vector<PatPV3DFuture::RecVertex> rvts;
  rvts.reserve(20);
  bool filter = false;
  StatusCode scfit = reconstructMultiPV(inputTracks, rvts);
  if (scfit == StatusCode::SUCCESS) {
    filter = !rvts.empty();
  } else {
    Warning("reconstructMultiPV failed!",scfit).ignore();
  }

  m_nbPVsCounter += rvts.size();
  return std::make_tuple(filter, std::move(rvts));
}

StatusCode PatPV3DFuture::reconstructMultiPV
(const std::vector<PatPV3DFuture::Track>& inputTracks,
 std::vector<PatPV3DFuture::RecVertex>& outvtxvec) const {
  std::vector<const PatPV3DFuture::Track*> rtracks;
  std::for_each(inputTracks.begin(), inputTracks.end(),
                [&](const PatPV3DFuture::Track& trk) {
                  if( !m_requireVelo || trk.hasVelo() ) {
                    rtracks.push_back(&trk);
                  }
                } );
  return reconstructMultiPVFromTracks(rtracks, outvtxvec);
}

void PatPV3DFuture::removeTracks
(std::vector<const PatPV3DFuture::Track*>& tracks,
 const std::vector<const PatPV3DFuture::Track*>& tracks2remove) const {
  auto firstToErase = std::remove_if(begin(tracks), end(tracks),
                                     [&tracks2remove](auto trk){
                                       return std::find(begin(tracks2remove), end(tracks2remove), trk) != tracks2remove.end();
                                     });
  tracks.erase(firstToErase, std::end(tracks));
} 

namespace {
  class AdaptivePVTrack final
  {
  public:
    using Track = LHCb::Event::v2::Track;
    AdaptivePVTrack(const Track& track, const Gaudi::XYZPoint& vtx) ;
    void updateCache( const Gaudi::XYZPoint& vtx ) ;
    double weight() const { return m_weight ; }
    void setWeight(double w) { m_weight = w ;}
    const Gaudi::SymMatrix3x3&  halfD2Chi2DX2() const { return m_halfD2Chi2DX2 ; }
    const Gaudi::Vector3&  halfDChi2DX() const { return m_halfDChi2DX ; }
    double chi2() const { return m_chi2 ; }
    inline double chi2( const Gaudi::XYZPoint& vtx ) const ;
    const Track* track() const { return m_track ; }
  private:
    void invertCov();
    double m_weight ;
    const Track* m_track ;
    LHCb::State m_state ;
    Gaudi::SymMatrix2x2 m_invcov ;
    Gaudi::SymMatrix3x3 m_halfD2Chi2DX2 ;
    Gaudi::Vector3 m_halfDChi2DX ;
    double m_chi2 ;
    ROOT::Math::SMatrix<double,3,2> m_H;
  } ;

  AdaptivePVTrack::AdaptivePVTrack(const LHCb::Event::v2::Track& track,
                                   const Gaudi::XYZPoint& vtx)
    : m_track(&track)
  {
    // get the state
    m_state = track.firstState() ;
    if( m_state.location()!=LHCb::State::Location::ClosestToBeam ) {
      const LHCb::State* closestToBeam = track.stateAt(LHCb::State::Location::ClosestToBeam) ;
      if(closestToBeam) m_state = *closestToBeam ;
    }
    // do here things we could evaluate at z_seed. may add cov matrix here, which'd save a lot of time.
    m_H(0,0) = m_H(1,1) = 1 ;
    m_H(2,0) = - m_state.tx() ;
    m_H(2,1) = - m_state.ty() ;
    // update the cache
    updateCache( vtx ) ;
  }

  void AdaptivePVTrack::invertCov() {
    auto invdet = 1/(m_invcov(0,0)*m_invcov(1,1)-m_invcov(0,1)*m_invcov(1,0));
    auto new00 = m_invcov(1,1)*invdet;
    m_invcov(1,1) = m_invcov(0,0)*invdet;
    m_invcov(0,0) = new00;
    m_invcov(0,1) = m_invcov(1,0) = -m_invcov(0,1)*invdet;
  }

  void AdaptivePVTrack::updateCache(const Gaudi::XYZPoint& vtx)
  {
    // transport to vtx z
    m_state.linearTransportTo( vtx.z() ) ;

    // invert cov matrix
    m_invcov = m_state.covariance().Sub<Gaudi::SymMatrix2x2>(0,0) ;
    invertCov();

    // The following can all be written out, omitting the zeros, once
    // we know that it works.
    Gaudi::Vector2 res{ vtx.x() - m_state.x(), vtx.y() - m_state.y() };
    ROOT::Math::SMatrix<double,3,2> HW = m_H*m_invcov ;
    ROOT::Math::AssignSym::Evaluate(m_halfD2Chi2DX2, HW*ROOT::Math::Transpose(m_H) ) ;
    //m_halfD2Chi2DX2 = ROOT::Math::Similarity(H, invcov ) ;
    m_halfDChi2DX   = HW * res ;
    m_chi2          = ROOT::Math::Similarity(res,m_invcov) ;
  }

  inline double AdaptivePVTrack::chi2( const Gaudi::XYZPoint& vtx ) const
  {
    double dz = vtx.z() - m_state.z() ;
    Gaudi::Vector2 res{ vtx.x() - (m_state.x() + dz*m_state.tx()),
                        vtx.y() - (m_state.y() + dz*m_state.ty()) };
    return ROOT::Math::Similarity(res,m_invcov) ;
  }
}

StatusCode PatPV3DFuture::fitVertex(const Gaudi::XYZPoint& seedPoint,
                                    const std::vector<const PatPV3DFuture::Track*>& rTracks,
                                    std::vector<PatPV3DFuture::RecVertex>& outputVtxVec,
                                    std::vector<const PatPV3DFuture::Track*>& tracks2remove) const {
  tracks2remove.clear();
  // position at which derivatives are evaluated
  Gaudi::XYZPoint refpos = seedPoint ;
  // prepare tracks
  std::vector<AdaptivePVTrack> pvTracks ;
  pvTracks.reserve( rTracks.size() ) ;
  for( const auto& track : rTracks )
    if( track->hasVelo() ) {
      pvTracks.emplace_back( *track, refpos );
      if (pvTracks.back().chi2() >= m_maxChi2) pvTracks.pop_back();
    }
  if( pvTracks.size() < m_minTr ) {
    return StatusCode::FAILURE;
  }
  // current vertex position
  Gaudi::XYZPoint vtxpos = refpos ;
  // vertex covariance matrix
  Gaudi::SymMatrix3x3 vtxcov ;
  bool converged = false;
  double maxdz = m_maxDeltaZ;
  int nbIter = 0;
  Track::Chi2PerDoF chi2PerDoF;
  while( (nbIter < m_minIter) || (!converged && nbIter < m_Iterations) )
  {
    ++nbIter;
    Gaudi::SymMatrix3x3 halfD2Chi2DX2 ;
    Gaudi::Vector3 halfDChi2DX ;
    // update cache if too far from reference position. this is the slow part.
    if( std::abs(refpos.z() - vtxpos.z()) > m_maxDeltaZCache ) {
      refpos = vtxpos ;
      for( auto& trk : pvTracks ) trk.updateCache( refpos ) ;
    }

    // add contribution from all tracks
    double chi2(0) ;
    size_t ntrin(0) ;
    for( auto& trk : pvTracks ) {
      // compute weight
      double trkchi2 = trk.chi2(vtxpos) ;
      double weight = getTukeyWeight(trkchi2, nbIter) ;
      trk.setWeight(weight) ;
      // add the track
      if ( weight > m_minTrackWeight ) {
        ++ntrin;
        halfD2Chi2DX2 += weight * trk.halfD2Chi2DX2() ;
        halfDChi2DX   += weight * trk.halfDChi2DX() ;
        chi2 += weight * trk.chi2() ;
      }
    }

    // check nr of tracks that entered the fit
    if(ntrin < m_minTr) {
      return StatusCode::FAILURE;
    }

    // compute the new vertex covariance
    vtxcov = halfD2Chi2DX2 ;
    if (!vtxcov.InvertChol()) {
      return StatusCode::FAILURE;
    }
    // compute the delta w.r.t. the reference
    Gaudi::Vector3 delta = -1.0 * vtxcov * halfDChi2DX ;

    // note: this is only correct if chi2 was chi2 of reference!
    chi2  += ROOT::Math::Dot(delta,halfDChi2DX) ;

    // deltaz needed for convergence
    const double deltaz = refpos.z() + delta(2) - vtxpos.z() ;

    // update the position
    vtxpos.SetX( refpos.x() + delta(0) ) ;
    vtxpos.SetY( refpos.y() + delta(1) ) ;
    vtxpos.SetZ( refpos.z() + delta(2) ) ;
    int nDoF = 2*ntrin-3;
    chi2PerDoF = {chi2/nDoF, nDoF};

    // loose convergence criteria if close to end of iterations
    if ( 1.*nbIter > 0.8*m_Iterations ) maxdz = 10.*m_maxDeltaZ;
    converged = std::abs(deltaz) < maxdz ;

  } // end iteration loop
  if(!converged) return StatusCode::FAILURE;

  auto& vtx = outputVtxVec.emplace_back(vtxpos, vtxcov, chi2PerDoF);
  vtx.reserve(pvTracks.size());
  for( const auto& trk : pvTracks ) {
    if( trk.weight() > m_minTrackWeight)
      vtx.addToTracks( trk.track(), trk.weight() ) ;
    // remove track for next PV search
    if( trk.chi2(vtxpos) < m_trackMaxChi2Remove)
      tracks2remove.push_back( trk.track() );
  }
  vtx.setTechnique(RecVertex::RecVertexType::Primary);
  return StatusCode::SUCCESS;
}

double PatPV3DFuture::getTukeyWeight(double trchi2, int iter) const {
  if (iter<1 ) return 1.;
  double ctrv = m_trackChi * std::max(m_minIter -  iter,1);
  auto sq = std::pow(ctrv*m_TrackErrorScaleFactor, 2);
  return sq > trchi2 ? std::pow( 1. - (trchi2/sq), 2 ) : 0.;
}

double PatPV3DFuture::errorForPVSeedFinding(double tx,
                                           double ty) const {
  // the seeding results depend weakly on this eror parametrization
  double invPMean2 = 1. / (3000.*Gaudi::Units::MeV*3000.*Gaudi::Units::MeV);
  double tanTheta2 =  tx * tx + ty * ty;
  double invSinTheta2 =  1. / tanTheta2 + 1.;
  // assume that first hit in VD at 8 mm
  double distr        = 8.*Gaudi::Units::mm;
  double dist2        = distr*distr * invSinTheta2;
  double sigma_ms2    = m_scatCons * m_scatCons * dist2 * invPMean2;
  double fslope2      = 0.0005*0.0005;
  double sigma_slope2 = fslope2*dist2;
  return (sigma_ms2 + sigma_slope2) * invSinTheta2;
}

std::vector<double>
PatPV3DFuture::findClusters(std::vector<vtxCluster>& vclus) const {
  std::vector<double> zclusters;
  if(vclus.empty()) return zclusters;
  std::vector<vtxCluster*> pvclus;
  pvclus.reserve(vclus.size());
  for (auto& itvtx : vclus) {
    itvtx.sigsq *= m_factorToIncreaseErrors*m_factorToIncreaseErrors; // blow up errors
    itvtx.sigsqmin = itvtx.sigsq;
    pvclus.push_back(&itvtx);
  }
  std::sort(pvclus.begin(),pvclus.end(),vtxcomp);
  bool more_merging = true;
  while (more_merging) {
    // find pair of clusters for merging
    // refresh flag for this iteration
    for(auto ivc = pvclus.begin(); ivc != pvclus.end(); ivc++) {
      (*ivc)->not_merged = 1;
    }
    // for a given cluster look only up to a few consequtive ones to merge
    // "a few" might become a property?
    auto n_consequtive = std::min(5,static_cast<int>(pvclus.size()));
    auto ivc2up = std::next( pvclus.begin(), n_consequtive);
    std::vector<pair_to_merge> vecp2m; vecp2m.reserve( std::min(static_cast<int>(pvclus.size())*n_consequtive,s_p2mstatic) );
    for(auto ivc1 = pvclus.begin(); ivc1 != pvclus.end()-1; ivc1++) {
      if(ivc2up != pvclus.end()) ++ivc2up;
      for(auto ivc2 = std::next(ivc1); ivc2 != ivc2up; ivc2++) {
        double zdist = (*ivc1)->z-(*ivc2)->z;
        double chi2dist = zdist*zdist/((*ivc1)->sigsq+(*ivc2)->sigsq);
        if(chi2dist<m_maxChi2Merge && vecp2m.size()<s_p2mstatic) {
          vecp2m.emplace_back(*ivc1,*ivc2,chi2dist);
        }
      }
    }
    // done if no more pairs to merge
    if(vecp2m.empty()) {
      more_merging = false;
    } else {
      // sort if number of pairs reasonable. Sorting increases efficency.
      if(vecp2m.size()<100) std::sort(vecp2m.begin(), vecp2m.end(), paircomp);
      // merge pairs
      for(auto itp2m = vecp2m.begin(); itp2m != vecp2m.end(); itp2m++) {
        vtxCluster* pvtx1 = itp2m->first;
        vtxCluster* pvtx2 = itp2m->second;
        if(pvtx1->not_merged == 1 && pvtx2->not_merged == 1) {
          double z1 = pvtx1->z;
          double z2 = pvtx2->z;
          double s1 = pvtx1->sigsq;
          double s2 = pvtx2->sigsq;
          double s1min = pvtx1->sigsqmin;
          double s2min = pvtx2->sigsqmin;
          double sigsqmin = s1min;
          if(s2min<s1min) sigsqmin = s2min;
          double w_inv = (s1*s2/(s1+s2));
          double zmerge = w_inv*(z1/s1+z2/s2);
          pvtx1->z        = zmerge;
          pvtx1->sigsq    = w_inv;
          pvtx1->sigsqmin = sigsqmin;
          pvtx1->ntracks += pvtx2->ntracks;
          pvtx2->ntracks  = 0;  // mark second cluster as used
          pvtx1->not_merged = 0;
          pvtx2->not_merged = 0;
        }
      }
      // remove clusters which where used
      pvclus.erase( std::remove_if( pvclus.begin(), pvclus.end(),
                                    [](const vtxCluster* cl) { return cl->ntracks<1; }),
                    pvclus.end());
    }
  }
  // End of clustering.
  // Sort according to multiplicity
  std::sort(pvclus.begin(),pvclus.end(),multcomp);
  // Select good clusters.
  for(auto ivc=pvclus.begin(); ivc != pvclus.end(); ivc++) {
    int n_tracks_close = 0;
    for(auto itvtx = vclus.begin(); itvtx != vclus.end(); itvtx++) {
      if(fabs(itvtx->z - (*ivc)->z ) < m_dzCloseTracksInCluster ) n_tracks_close++;
    }
    double dist_to_closest = 1000000.;
    if(pvclus.size() > 1) {
      for(auto ivc1=pvclus.begin(); ivc1 != pvclus.end(); ivc1++) {
        if( ivc!=ivc1 && ( fabs((*ivc1)->z-(*ivc)->z) < dist_to_closest) ) {
          dist_to_closest = fabs((*ivc1)->z-(*ivc)->z);
        }
      }
    }
    // ratio to remove clusters made of one low error track and many large error ones
    double rat = (*ivc)->sigsq/(*ivc)->sigsqmin;
    bool igood = false;
    int ntracks = (*ivc)->ntracks;
    if( ntracks >= m_minClusterMult ) {
      if( dist_to_closest>10. && rat<0.95) igood=true;
      if( ntracks >= m_highMult && rat < m_ratioSig2HighMult)  igood=true;
      if( ntracks <  m_highMult && rat < m_ratioSig2LowMult )  igood=true;
    }
    // veto
    if( n_tracks_close < m_minCloseTracksInCluster ) igood = false;
    if(igood)  zclusters.push_back((*ivc)->z);
  }
  //  print_clusters(pvclus);
  return zclusters;
}

StatusCode PatPV3DFuture::reconstructMultiPVFromTracks
(std::vector<const PatPV3DFuture::Track*>& rtracks,
 std::vector<PatPV3DFuture::RecVertex>& outvtxvec) const {
  auto totaltime_guard = make_timeguard( timers_t::Total );
  outvtxvec.clear();
  if (m_saveSeedsAsPV) {
    auto seeds = getSeeds(rtracks, {m_beamSpotX, m_beamSpotY, 0.0 });
    outvtxvec = storeDummyVertices(seeds, rtracks);
    return StatusCode::SUCCESS;
  }
  bool goOn = true;
  while ( goOn ) {
    goOn = false;
    auto seeds = [&](){ auto seedingtime_guard = make_timeguard( timers_t::Seeding );
    // seeding
      return getSeeds(rtracks, {m_beamSpotX, m_beamSpotY, 0.0} );
    }();
    for (const auto& seed : seeds) {
      std::vector<const Track*> tracks2remove;
      { auto fittime_guard = make_timeguard( timers_t::Fitting );
        // fitting
        StatusCode scvfit = fitVertex( seed, rtracks, outvtxvec, tracks2remove );
        if (!scvfit.isSuccess()) {
          continue;
        }
      }
      if ( !isChi2Separated( outvtxvec.back(), { outvtxvec.data(), (long)(outvtxvec.size()-1) },
                             minAllowedChi2( outvtxvec.back() ) ) ) {
        outvtxvec.pop_back();
        continue;
      }
      if ( m_useBeamSpotRCut.value() && m_veloClosed ) {
        RecVertex& recvtx = outvtxvec.back();
        const auto& pos = recvtx.position();
        auto r2 = std::pow(pos.x()-m_beamSpotX,2) + std::pow( pos.y()-m_beamSpotY,2);
        auto r  = ( recvtx.tracks().size() <  m_beamSpotRMT.value() ? m_beamSpotRCut.value() :m_beamSpotRCutHMC.value() );
        if ( r2 > r*r ) {
          outvtxvec.pop_back();
          continue;
        }
      }
      goOn = true;
      removeTracks(rtracks, tracks2remove);
    }//iterate on seeds
  }//iterate on vtx
  return StatusCode::SUCCESS;
}

std::vector<Gaudi::XYZPoint>
PatPV3DFuture::getSeeds(const std::vector<const PatPV3DFuture::Track*>& inputTracks,
                        const Gaudi::XYZPoint& beamspot) const {
  std::vector<Gaudi::XYZPoint> seeds;
  if(inputTracks.size() < 3 ) return seeds;
  std::vector<vtxCluster> vclusters;
  for (const auto& trk : inputTracks) {
    double sigsq;
    double zclu;
    if ( trk->type() == Track::Type::VeloR) {
      zclu = trk->firstState().z() - trk->firstState().x()/trk->firstState().tx();
      sigsq = errorForPVSeedFinding(trk->firstState().tx(), 0.0);
    } else {
      zclu = zCloseBeamOfflineTool(trk,beamspot);
      sigsq = errorForPVSeedFinding(trk->firstState().tx(), trk->firstState().ty());
    }
    if ( fabs(zclu)>2000.) continue;
    vtxCluster clu;
    clu.z = zclu;
    clu.sigsq = sigsq;
    clu.sigsqmin = clu.sigsq;
    clu.ntracks = 1;
    vclusters.push_back(clu);
  }
  auto zseeds = findClusters(vclusters);
  seeds.reserve(zseeds.size());
  std::transform( zseeds.begin(), zseeds.end(),
                  std::back_inserter(seeds),
                  [&](double z) {
    return Gaudi::XYZPoint{ beamspot.X(), beamspot.Y(), z};
  });
  return seeds;
}
        
