/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
// -------------
// From Gaudi
#include "GaudiAlg/Transformer.h"
#include "Event/RecVertex_v2.h"
#include "Event/Track_v2.h"
#include "Event/State.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "GaudiAlg/ISequencerTimerTool.h"
#include <string>
#include <vector>

// auxiliary class for searching of clusters of tracks
struct vtxCluster final {
  double  z = 0;            // z of the cluster
  double  sigsq = 0;        // sigma**2 of the cluster
  double  sigsqmin = 0;     // minimum sigma**2 of the tracks forming cluster
  int     ntracks = 0;      // number of tracks in the cluster
  int     not_merged = 0;   // flag for iterative merging
};

class PVTrack final {
public:
  using Track = LHCb::Event::v2::Track;
  const Track* refTrack = nullptr;
  // Current state of the track at the current point
  LHCb::State stateG;
  // Normalized vector of slope
  Gaudi::XYZVector unitVect;
  // Flag if the track has been used in a previous vertex
  bool isUsed = false;

  // Result for impact parameter
  Gaudi::XYZVector vd0;          // Impact parameter vector
  double d0sq = 0;                   // Impact parameter squared
  double err2d0 = 0;                 // IP error squared
  double chi2 = 0;                   // chi2 = d02 / d0err**2
  double weight = 0;                 // Weight assigned to track
  Track::Type type = Track::Type::Velo;

  double zClose() const {
    return stateG.z() - unitVect.z() *
           (unitVect.x() * stateG.x() + unitVect.y() * stateG.y()) /
           (1.0 - std::pow(unitVect.z(),2));
  }
};

typedef std::vector<PVTrack> PVTracks;
typedef std::vector<PVTrack*> PVTrackPtrs;

struct PVVertex final {
  PVTrackPtrs pvTracks ;
  LHCb::Event::v2::RecVertex primVtx;
};

/** 
 *  Algorithm to find the primary vertices at the HLT.
 *
 *  This version is temporary and should be dropped and replaced
 *  with TrackBeamLineVertexFinder when it is ready. Its only purpose
 *  is to provide a PatPV using Tracks v2 and RecVertex v2 in the
 *  mean time.
 *  Note that this class integrates directly the code of the tools
 *  previously used by PatPV3D
 */
class PatPV3DFuture : public Gaudi::Functional::MultiTransformerFilter
<std::tuple<std::vector<LHCb::Event::v2::RecVertex>>(const std::vector<LHCb::Event::v2::Track>&)> {
public:

  using RecVertex = LHCb::Event::v2::RecVertex;
  using Track = LHCb::Event::v2::Track;  
  
  /// Standard constructor
  PatPV3DFuture(const std::string& name, ISvcLocator* pSvcLocator);

  StatusCode initialize() override;

  /// Algorithm execution
  std::tuple<bool, std::vector<RecVertex>> operator()(const std::vector<Track>&) const override;

private:
  ////////  From PVOfflineTool
  StatusCode reconstructMultiPV(const std::vector<Track>& inputTracks,
                                std::vector<RecVertex>& outvtxVec) const;

  StatusCode reconstructMultiPVFromTracks(std::vector<const Track*>& tracks2use,
					  std::vector<RecVertex>& outvtxVec) const;

  void removeTracks(std::vector<const Track*>& tracks,
                    const std::vector<const Track*>& tracks2remove) const;

  StatusCode UpdateBeamSpot();

  ////////  From AdaptivePV3DFitter
  StatusCode fitVertex(const Gaudi::XYZPoint& seedPoint,
                       const std::vector<const Track*>& tracks,
                       std::vector<PatPV3DFuture::RecVertex>& outputVtxVec,
                       std::vector<const Track*>& tracks2remove) const;

  // Get Tukey's weight
  double getTukeyWeight(double trchi2, int iter) const;

  ////////  From PVSeedTool
  std::vector<Gaudi::XYZPoint>
    getSeeds(const std::vector<const Track*>& inputTracks,
             const Gaudi::XYZPoint& beamspot) const;

  std::vector<double> findClusters(std::vector<vtxCluster>& vclus) const;
  double errorForPVSeedFinding(double tx, double ty) const;
  double zCloseBeam(const Track* track, const Gaudi::XYZPoint& beamspot) const;


private:
  ////////  From PVOfflineTool
  Gaudi::Property<bool> m_refitpv { this,  "RefitPV", false, "Flag to refit PVs when converting to type PrimaryVertex" };
  Gaudi::Property<bool> m_requireVelo { this,"RequireVelo", true, "Option to use tracks with VELO segment only" };
  Gaudi::Property<bool> m_saveSeedsAsPV {this,"SaveSeedsAsPV", false, "Save seeds as PVs (for monitoring" };
  Gaudi::Property<double> m_pvsChi2Separation { this, "PVsChi2Separation",  25.};
  Gaudi::Property<double> m_pvsChi2SeparationLowMult { this, "PVsChi2SeparationLowMult", 91. };
  Gaudi::Property<bool> m_useBeamSpotRCut { this, "UseBeamSpotRCut", false };
  Gaudi::Property<double> m_beamSpotRCut { this, "BeamSpotRCut", 0.2 };
  Gaudi::Property<double> m_beamSpotRCutHMC { this, "BeamSpotRHighMultiplicityCut", 0.4 };
  Gaudi::Property<unsigned int>  m_beamSpotRMT { this, "BeamSpotRMultiplicityTreshold", 10 };
  double m_beamSpotX = 0;
  double m_beamSpotY = 0;
  Gaudi::Property<double> m_resolverBound { this, "ResolverBound", 5 * Gaudi::Units::mm };
  bool m_veloClosed = false;

  ////////  From AdaptivePV3DFitter
  Gaudi::Property<size_t> m_minTr { this, "MinTracks",  4, "Minimum number of tracks to make a vertex" };
  Gaudi::Property<int>    m_Iterations{ this, "Iterations", 20, "Number of iterations for minimisation" };
  Gaudi::Property<int>    m_minIter { this, "minIter", 5, "Min number of iterations" };
  Gaudi::Property<double> m_maxDeltaZ { this, "maxDeltaZ", 0.0005 * Gaudi::Units::mm, "Fit convergence condition" }; //0.001 * Gaudi::Units::mm)
  Gaudi::Property<double> m_minTrackWeight{ this, "minTrackWeight", 0.00000001, "Minimum Tukey's weight to accept a track" }; //0.00001)
  Gaudi::Property<double> m_TrackErrorScaleFactor { this, "TrackErrorScaleFactor", 1.0 };
  Gaudi::Property<double> m_maxChi2{this, "maxChi2", 400.0, "max chi2 of track-to-vtx to be considered for fit" };
  Gaudi::Property<double> m_trackMaxChi2{ this, "trackMaxChi2", 12. };
  double m_trackChi ;     // sqrt of trackMaxChi2
  Gaudi::Property<double> m_trackMaxChi2Remove{ this, "trackMaxChi2Remove", 25.,
    [this](Property&){ this->m_trackChi = std::sqrt(this->m_trackMaxChi2); },
    Gaudi::Details::Property::ImmediatelyInvokeHandler{true}, // insure m_trackChi is in sync with m_trackMaxChi2
    "Max chi2 tracks to be removed from next PV search" };
  Gaudi::Property<double> m_maxDeltaZCache{ this, "maxDeltaZCache", 1 * Gaudi::Units::mm, "Update derivatives if distance of reference is more than this" };

  ////////  From PVSeedTool
  // steering parameters for merging procedure
  Gaudi::Property<double> m_maxChi2Merge { this,  "maxChi2Merge", 25.};
  Gaudi::Property<double> m_factorToIncreaseErrors { this,  "factorToIncreaseErrors" , 15. };
  // steering parameters for final cluster selection
  Gaudi::Property<int>    m_minClusterMult { this,  "minClusterMult", 3 };
  Gaudi::Property<double> m_dzCloseTracksInCluster { this,  "dzCloseTracksInCluster",  5.*Gaudi::Units::mm };
  Gaudi::Property<int>    m_minCloseTracksInCluster { this,  "minCloseTracksInCluster",  3 };
  Gaudi::Property<int>    m_highMult { this,  "highMult", 10 };
  Gaudi::Property<double> m_ratioSig2HighMult { this,  "ratioSig2HighMult", 1.0 };
  Gaudi::Property<double> m_ratioSig2LowMult { this,  "ratioSig2LowMult", 0.9 };
  double  m_scatCons = 0;     // calculated from m_x0MS
  Gaudi::Property<double> m_x0MS {this, "x0MS", 0.01,
      [=](Property&) {
        double X0 = this->m_x0MS;
        this->m_scatCons = (13.6*sqrt(X0)*(1.+0.038*log(X0)));
      },Gaudi::Details::Property::ImmediatelyInvokeHandler{true}
  };// X0 (tunable) of MS to add for extrapolation of
    // track parameters to PV

    // timing
  Gaudi::Property<bool>  m_doTiming { this, "TimingMeasurement", false };
  ToolHandle<ISequencerTimerTool> m_timerTool { "SequencerTimerTool/Timer", this };
  std::array<int,3> m_timer;

  enum class timers_t { Total = 0, Seeding, Fitting };
  class TimerGuard {
    ISequencerTimerTool* m_tool;
    int m_timer;
  public:
    TimerGuard( const ISequencerTimerTool* t, int i ) : m_tool(const_cast<ISequencerTimerTool*>(t)), m_timer(i)
    { m_tool->start(m_timer); }
    ~TimerGuard() { m_tool->stop(m_timer); }
  };
  std::optional<TimerGuard> make_timeguard( timers_t type ) const {
    if (!m_doTiming) return {};
    return TimerGuard{ m_timerTool.get(), m_timer[static_cast<int>(type)] };
  }
  // trivial accessor to minimum allowed chi2
  double minAllowedChi2( const RecVertex& rvtx ) const {
    return ( rvtx.tracks().size() < 7 ? std::max(m_pvsChi2Separation,m_pvsChi2SeparationLowMult )
             : m_pvsChi2Separation );
  }

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nbPVsCounter{ this, "Nb PVs" };
};
