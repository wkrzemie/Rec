/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LSADAPTPVFITTER_H
#define LSADAPTPVFITTER_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

// Interfaces
#include "IPVFitter.h"
#include "PVUtils.h"

// Track info
#include "Event/Track.h"
#include "Event/RecVertex.h"

// Forward declarations
struct ITrackExtrapolator;

class LSAdaptPV3DFitter : public extends<GaudiTool, IPVFitter> {

public:
  // Standard constructor
  using extends::extends;
  // Fitting
  StatusCode fitVertex(const Gaudi::XYZPoint& seedPoint,
                       const std::vector<const LHCb::Track*>& tracks,
                       LHCb::RecVertex& vtx,
                       std::vector<const LHCb::Track*>& tracks2remove) const override;
private:
  Gaudi::Property<double> m_maxDeltaZ { this, "maxDeltaZ", 0.0005 * Gaudi::Units::mm, "Fit convergence condition" };
  Gaudi::Property<double> m_minTrackWeight { this, "minTrackWeight", 0.00000001, "Minimum Tukey's weight to accept a track" }; //0.00001
  Gaudi::Property<double> m_TrackErrorScaleFactor{ this, "TrackErrorScaleFactor", 1.0 };
  Gaudi::Property<double> m_trackMaxChi2Remove{ this, "trackMaxChi2Remove", 25., "Max chi2 tracks to be removed from next PV search"};
  Gaudi::Property<double> m_maxChi2 { this, "maxChi2", 400.0, "maximal chi2 to accept track"}; // Chi2 of completely wrong tracks;       //
  Gaudi::Property<double> m_zVtxShift{ this, "zVtxShift", 0.0 };
  Gaudi::Property<int>    m_minTr{ this, "MinTracks", 5, "Minimum number of tracks to make a vertex" };
  Gaudi::Property<int>    m_Iterations{ this, "Iterations", 20, "Number of iterations for minimisation" };
  Gaudi::Property<int>    m_minIter { this, "minIter", 5, "Minimum number of iterations" };  // iterate at least m_minIter times
  Gaudi::Property<bool>   m_AddMultipleScattering{ this, "AddMultipleScattering", true, "add multiple scattering calculation to not fitted tracks" };

  Gaudi::Property<bool>   m_CalculateMultipleScattering{ this, "CalculateMultipleScattering", true, "calculate multiple scattering" };
  Gaudi::Property<bool>   m_UseFittedTracks{ this,  "UseFittedTracks", false , "use tracks fitted by kalman fit" };
  double m_scatCons       = 0.;      // calculated from m_x0MS and 3 GeV
  double m_scatConsNoMom; // calculated from m_x0MS to be divided by momemntum [MeV]
  Gaudi::Property<double> m_x0MS{ this, "x0MS", 0.02,
      [=](Property&) {
        double X0 = this->m_x0MS;
        this->m_scatConsNoMom = (13.6*std::sqrt(X0)*(1.+0.038*log(X0)));
        this->m_scatCons      = this->m_scatConsNoMom / ( 3.0 * Gaudi::Units::GeV );
      },Gaudi::Details::Property::ImmediatelyInvokeHandler{true}
  }; // X0 (tunable) of MS to add for extrapolation of
     // track parameters to PV
  double m_trackChi = 3.; // sqrt of m_trackMaxChi2
  Gaudi::Property<double> m_trackMaxChi2{ this, "trackMaxChi2", 9.,
      [=](Property&) { this->m_trackChi = std::sqrt(m_trackMaxChi2); },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
      "maximum chi2 track to accept track in PV fit" };

  ToolHandle<ITrackExtrapolator> m_linExtrapolator { this, "LinearExtrapolator","TrackLinearExtrapolator"} ;   // Linear extrapolator
  ToolHandle<ITrackExtrapolator> m_fullExtrapolator { this, "FullExtrapolator","TrackMasterExtrapolator"} ;  // Full extrapolator

  // Add track for PV
  void addTrackForPV(const LHCb::Track* str, PVTracks& pvTracks,
                           const Gaudi::XYZPoint& seed) const;

  double err2d0(const LHCb::Track* track, const Gaudi::XYZPoint& seed) const;
  // Get Tukey's weight
  double getTukeyWeight(double trchi2, int iter) const {
    if (iter<1 ) return 1.;
    auto ctrv = m_trackChi * std::max(m_minIter -  iter, 1);
    auto cT2  = trchi2 / std::pow(ctrv*m_TrackErrorScaleFactor,2);
    return cT2 < 1 ? std::pow( 1-cT2, 2 ) : 0.;
  }
};

#endif // LSADAPTPVFITTER_H
