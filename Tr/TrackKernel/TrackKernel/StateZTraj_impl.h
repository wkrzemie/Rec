/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
namespace LHCb
{
  template<typename FTYPE>
  inline
  void StateZTraj<FTYPE>::expansion( FTYPE z,
                                     typename StateZTraj<FTYPE>::Point& p,
                                     typename StateZTraj<FTYPE>::Vector& dp,
                                     typename StateZTraj<FTYPE>::Vector& ddp ) const
  {
    p.SetXYZ(   x(z), y(z), z );
    dp.SetXYZ(  tx(z), ty(z), 1 );
    ddp.SetXYZ( omegax(z), omegay(z), 0 );
  }

  template<typename FTYPE>
  inline
  auto updet(FTYPE det, FTYPE dir2)
    -> typename std::enable_if<not std::is_floating_point<FTYPE>::value,
                               FTYPE>::type {
    return select(det <= 0, dir2, det);
  }

  template<typename FTYPE>
  inline
  auto updet(FTYPE det, FTYPE dir2)
    -> typename std::enable_if<std::is_floating_point<FTYPE>::value,
                               FTYPE>::type {
    return det <= 0 ? det : dir2;
  }

  template<typename FTYPE>
  inline FTYPE
  StateZTraj<FTYPE>::muEstimate( const StateZTraj<FTYPE>::Point& p ) const
  {
    StateZTraj<FTYPE>::Vector dir = direction(p.z());
    StateZTraj<FTYPE>::Vector dx  = p - position(p.z());
    FTYPE dir2 = dir.mag2();
    FTYPE det  = dir2 - curvature(p.z()).Dot(dx);

    det = updet(det, dir2);

    return p.z() + dx.Dot(dir)/det;
  }

  template<typename FTYPE>
  inline typename StateZTraj<FTYPE>::Parameters
  StateZTraj<FTYPE>::parameters() const
  {
    return { m_cx[0], m_cy[0], m_cx[1], m_cy[1], m_qOverP };
  }

  template<typename FTYPE>
  inline StateZTraj<FTYPE>&
  StateZTraj<FTYPE>::operator+=(const Parameters& /*delta*/)
  {
    // to implement this we need the full b-field.
    std::cerr << __FUNCTION__ << " not yet implemented." << std::endl;
    return *this;
  }
}
