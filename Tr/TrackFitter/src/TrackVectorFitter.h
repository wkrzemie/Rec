/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Tr::TrackVectorFit
#include "TrackVectorFit/TrackVectorFit.h"

// Gaudi et al.
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbMath/LHCbMath.h"
#include "TrackKernel/TrackTraj.h"
#include "Kernel/ILHCbMagnetSvc.h"

// Event
#include "Event/TrackFunctor.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Event/OTMeasurement.h"
#include "Event/State.h"

// Interface base class
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackProjectorSelector.h"
#include "TrackInterfaces/ITrackProjector.h"
#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/IMaterialLocator.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "Event/Track.h"

// Forward declarations
struct IMaterialLocator;
struct ITrackExtrapolator;

namespace LHCb {
  class State;
  class TrackFitResult;
}

/** @class TrackVectorFitter TrackVectorFitter.h
 *  @author Daniel Campora
 *  @date   2017-02-13
 *  reusing much of TrackMasterFitter
 */
struct TrackVectorFitter : public extends<GaudiTool, ITrackFitter> {
  // Code profiling
  // mutable bool m_firstCall;

  // All properties in alphabetical order
  Gaudi::Property<bool> m_addDefaultRefNodes {this, "AddDefaultReferenceNodes", true};
  Gaudi::Property<bool> m_applyEnergyLossCorrections {this, "ApplyEnergyLossCorr", true};
  Gaudi::Property<bool> m_applyMaterialCorrections {this, "ApplyMaterialCorrections", true};
  Gaudi::Property<double> m_chi2Outliers {this, "Chi2Outliers", 9.0};
  Gaudi::Property<bool> m_fillExtraInfo {this, "FillExtraInfo", true};
  Gaudi::Property<bool> m_makeMeasurements {this, "MakeMeasurements", false};
  Gaudi::Property<bool> m_makeNodes {this, "MakeNodes", false};
  Gaudi::Property<std::string> m_fieldSvcName {this, "MagneticFieldSvc", "MagneticFieldSvc"};
  Gaudi::Property<double> m_maxDeltaChi2Converged {this, "MaxDeltaChiSqConverged", 0.01};
  Gaudi::Property<double> m_maxMomentumForScattering {this, "MaxMomentumForScattering", 500. * Gaudi::Units::GeV};
  Gaudi::Property<unsigned> m_memManagerStorageIncrements {this, "MemManagerStorageIncrements", 4096};
  Gaudi::Property<double> m_minMomentumForELossCorr {this, "MinMomentumELossCorr", 10. * Gaudi::Units::MeV};
  Gaudi::Property<double> m_minMomentumForScattering {this, "MinMomentumForScattering", 100. * Gaudi::Units::MeV};
  Gaudi::Property<size_t> m_minNumMuonHits {this, "MinNumMuonHitsForOutlierRemoval", 4};
  Gaudi::Property<size_t> m_minNumTHits {this, "MinNumTHitsForOutlierRemoval", 6};
  Gaudi::Property<size_t> m_minNumTTHits {this, "MinNumTTHitsForOutlierRemoval", 3};
  Gaudi::Property<size_t> m_minNumVeloPhiHits {this, "MinNumVeloPhiHitsForOutlierRemoval", 3};
  Gaudi::Property<size_t> m_minNumVeloRHits {this, "MinNumVeloRHitsForOutlierRemoval", 3};
  Gaudi::Property<unsigned> m_numFitIter {this, "NumberFitIterations", 10};
  Gaudi::Property<unsigned> m_numOutlierIter {this, "MaxNumberOutliers", 2};
  Gaudi::Property<double> m_scatteringP {this, "MomentumForScattering", -1};
  Gaudi::Property<double> m_scatteringPt {this, "TransverseMomentumForScattering", 400. * Gaudi::Units::MeV};
  Gaudi::Property<bool> m_stateAtBeamLine {this, "StateAtBeamLine", true};
  Gaudi::Property<bool> m_updateMaterial {this, "UpdateMaterial", false};
  Gaudi::Property<bool> m_upstream {this, "FitUpstream", true};
  Gaudi::Property<bool> m_useSeedStateErrors {this, "UseSeedStateErrors", false};
  // Tool Handles
  ToolHandle<ITrackExtrapolator> m_extrapolator{"TrackMasterExtrapolator", this}; // Extrapolator
  ToolHandle<ITrackExtrapolator> m_veloExtrapolator{"TrackLinearExtrapolator", this}; // Extrapolator for Velo-only tracks
  ToolHandle<IMeasurementProvider> m_measProvider{"MeasurementProvider", this};
  ToolHandle<IMaterialLocator> m_materialLocator{"DetailedMaterialLocator", this};
  ToolHandle<ITrackProjectorSelector> m_projectorSelector{"TrackProjectorSelector", this};
  ToolHandle<ITrackProjector> m_projector{"TrackProjector", this};
  // Attributes
  std::array<size_t, 5> m_minNumHits;
  SmartIF<ILHCbMagnetSvc> m_magneticFieldService;
  const LHCb::MagneticFieldGrid* m_magneticFieldGrid;
  bool m_rungeKuttaExtrapolatorEnabled;

  void printAttributes () const {
    debug() << "m_addDefaultRefNodes " << m_addDefaultRefNodes << endmsg;
    debug() << "m_applyEnergyLossCorrections " << m_applyEnergyLossCorrections << endmsg;
    debug() << "m_applyMaterialCorrections " << m_applyMaterialCorrections << endmsg;
    debug() << "m_chi2Outliers " << m_chi2Outliers << endmsg;
    debug() << "m_fillExtraInfo " << m_fillExtraInfo << endmsg;
    debug() << "m_makeMeasurements " << m_makeMeasurements << endmsg;
    debug() << "m_makeNodes " << m_makeNodes << endmsg;
    debug() << "m_maxDeltaChi2Converged " << m_maxDeltaChi2Converged << endmsg;
    debug() << "m_maxMomentumForScattering " << m_maxMomentumForScattering << endmsg;
    debug() << "m_minMomentumForELossCorr " << m_minMomentumForELossCorr << endmsg;
    debug() << "m_minMomentumForScattering " << m_minMomentumForScattering << endmsg;
    debug() << "m_minNumMuonHits " << m_minNumMuonHits << endmsg;
    debug() << "m_minNumTHits " << m_minNumTHits << endmsg;
    debug() << "m_minNumTTHits " << m_minNumTTHits << endmsg;
    debug() << "m_minNumVeloPhiHits " << m_minNumVeloPhiHits << endmsg;
    debug() << "m_minNumVeloRHits " << m_minNumVeloRHits << endmsg;
    debug() << "m_numFitIter " << m_numFitIter << endmsg;
    debug() << "m_numOutlierIter " << m_numOutlierIter << endmsg;
    debug() << "m_scatteringP " << m_scatteringP << endmsg;
    debug() << "m_scatteringPt " << m_scatteringPt << endmsg;
    debug() << "m_stateAtBeamLine " << m_stateAtBeamLine << endmsg;
    debug() << "m_updateMaterial " << m_updateMaterial << endmsg;
    debug() << "m_upstream " << m_upstream << endmsg;
    debug() << "m_useSeedStateErrors " << m_useSeedStateErrors << endmsg;
  }

  TrackVectorFitter (
    const std::string& type,
    const std::string& name,
    const IInterface* parent
  );

  StatusCode initialize () override;

  StatusCode finalize () override;

  StatusCode operator() (
    LHCb::Track& track,
    const LHCb::Tr::PID& pid = LHCb::Tr::PID::Pion()
  ) const override;

  StatusCode operator() (
    std::vector<std::reference_wrapper<LHCb::Track>>& tracks,
    const LHCb::Tr::PID& pid = LHCb::Tr::PID::Pion()
  ) const override;

  void populateTracks (
    std::list<std::reference_wrapper<Tr::TrackVectorFit::Track>>& tracks
  ) const;

  void generateFitResult (LHCb::Track& track) const;

  double closestToBeamLine (const LHCb::State& state) const;

  StatusCode makeNodes (LHCb::Track& track, const LHCb::Tr::PID& pid) const;

  void populateRefVectors (Tr::TrackVectorFit::Track& t) const;

  StatusCode initializeRefStates (LHCb::Track& track, const LHCb::Tr::PID& pid) const;

  void projectReference (std::list<Tr::TrackVectorFit::Sch::Blueprint<Tr::TrackVectorFit::vector_width()>>& scheduler) const;

  void updateMaterialCorrections (Tr::TrackVectorFit::Track& t, const LHCb::Tr::PID& pid) const;

  void updateTransport (std::list<Tr::TrackVectorFit::Sch::Blueprint<Tr::TrackVectorFit::vector_width()>>& scheduler) const;

  void setNTrackParameters (Tr::TrackVectorFit::Track& t) const;

  bool removeWorstOutlier (Tr::TrackVectorFit::Track& t) const;

  bool removeWorstOutlierSimplified (Tr::TrackVectorFit::Track& t) const;

  void updateReferenceNodeTransport (Tr::TrackVectorFit::Track& t) const;

  StatusCode calculateReferenceNodeStates (Tr::TrackVectorFit::Track& t) const;

  void determineStates (Tr::TrackVectorFit::Track& t) const;

  void fillExtraInfo (Tr::TrackVectorFit::Track& t) const;

  inline StatusCode failureInfo (const std::string& comment) const {
    if (msgLevel(MSG::DEBUG)) {
      debug() << "TrackVectorFitter failure: " + comment << endmsg;
    }
    return StatusCode::FAILURE;
  }

  inline unsigned hitType (const LHCb::Measurement::Type& type) const {
    enum HitType {VeloR=0, VeloPhi=1, TT=2, T=3, Muon=4, Unknown=5};

    switch (type) {
      case LHCb::Measurement::Type::VeloR:
      case LHCb::Measurement::Type::VeloLiteR:
      case LHCb::Measurement::Type::VP:
        return VeloR;

      case LHCb::Measurement::Type::VeloLitePhi:
      case LHCb::Measurement::Type::VeloPhi:
        return VeloPhi;

      case LHCb::Measurement::Type::TT:
      case LHCb::Measurement::Type::TTLite:
      case LHCb::Measurement::Type::UT:
      case LHCb::Measurement::Type::UTLite:
        return TT;

      case LHCb::Measurement::Type::IT:
      case LHCb::Measurement::Type::OT:
      case LHCb::Measurement::Type::ITLite:
      case LHCb::Measurement::Type::FT:
        return T;

      case LHCb::Measurement::Type::Muon:
        return Muon;

      case LHCb::Measurement::Type::Unknown:
      case LHCb::Measurement::Type::Calo:
      case LHCb::Measurement::Type::Origin:
      default:
        return Unknown;
    }
  }

  inline unsigned nActiveMeasurements (
    const Tr::TrackVectorFit::Track& t,
    const LHCb::Measurement::Type& type
  ) const {
    unsigned rc (0);
    for (const auto& n : t.nodes()) {
      if (n.node().type() == LHCb::Node::Type::HitOnTrack && n.node().measurement().type() == type) {
        ++rc;
      }
    }
    return rc ;
  }

  inline unsigned nActiveOTTimes (
    const Tr::TrackVectorFit::Track& t
  ) const {
    unsigned rc (0);
    for (const auto& n : t.nodes()) {
      if (n.node().type() == LHCb::Node::Type::HitOnTrack && n.node().measurement().type() == LHCb::Measurement::Type::OT) {
        const LHCb::OTMeasurement* otmeas = static_cast<const LHCb::OTMeasurement*>(&(n.node().measurement()));
        if (otmeas->driftTimeStrategy() == LHCb::OTMeasurement::DriftTimeStrategy::FitDistance ||
            otmeas->driftTimeStrategy() == LHCb::OTMeasurement::DriftTimeStrategy::FitTime) {
          ++rc;
        }
      }
    }
    return rc ;
  }

  inline const ITrackExtrapolator* extrapolator (LHCb::Track::Types tracktype) const {
    if (tracktype == LHCb::Track::Types::Velo || tracktype == LHCb::Track::Types::VeloR) {
      return &(*m_veloExtrapolator);
    }
    return &(*m_extrapolator);
  }
};
