/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

using namespace Mem::View;

// ---------------
// get methods
// ---------------

template<>
inline const TRACKVECTORFIT_PRECISION& Node::get<Op::NodeParameters, Op::ReferenceResidual> () const {
  return *(m_nodeParameters.m_referenceResidual);
}

template<>
inline const TRACKVECTORFIT_PRECISION& Node::get<Op::NodeParameters, Op::ErrMeasure> () const {
  return *(m_nodeParameters.m_errorMeasure);
}

template<>
inline const TrackVector& Node::get<Op::NodeParameters, Op::ReferenceVector> () const {
  return m_nodeParameters.m_referenceVector;
}

template<>
inline const TrackVector& Node::get<Op::NodeParameters, Op::ProjectionMatrix> () const {
  return m_nodeParameters.m_projectionMatrix;
}

template<>
inline const TRACKVECTORFIT_PRECISION& Node::get<Op::Forward, Op::Chi2> () const {
  return *(m_forwardState.m_chi2);
}

template<>
inline const TrackVector& Node::get<Op::Forward, Op::StateVector> () const {
  return m_forwardState.m_updatedState;
}

template<>
inline const TrackSymMatrix& Node::get<Op::Forward, Op::Covariance> () const {
  return m_forwardState.m_updatedCovariance;
}

template<>
inline const TrackSymMatrix& Node::get<Op::NodeParameters, Op::NoiseMatrix> () const {
  return m_nodeParameters.m_noiseMatrix;
}

template<>
inline const TrackVector& Node::get<Op::NodeParameters, Op::TransportVector> () const {
  return m_nodeParameters.m_transportVector;
}

template<>
inline const TrackMatrix<25>& Node::get<Op::Forward, Op::TransportMatrix> () const {
  return m_forwardTransportMatrix;
}

template<>
inline const TRACKVECTORFIT_PRECISION& Node::get<Op::Backward, Op::Chi2> () const {
  return *(m_backwardState.m_chi2);
}

template<>
inline const TrackVector& Node::get<Op::Backward, Op::StateVector> () const {
  return m_backwardState.m_updatedState;
}

template<>
inline const TrackSymMatrix& Node::get<Op::Backward, Op::Covariance> () const {
  return m_backwardState.m_updatedCovariance;
}

template<>
inline const TrackMatrix<25>& Node::get<Op::Backward, Op::TransportMatrix> () const {
  return m_backwardTransportMatrix;
}

template<>
inline const TrackVector& Node::get<Op::Smooth, Op::StateVector> () const {
  return m_smoothState.m_state;
}

template<>
inline const TrackSymMatrix& Node::get<Op::Smooth, Op::Covariance> () const {
  return m_smoothState.m_covariance;
}

template<>
inline const TRACKVECTORFIT_PRECISION& Node::get<Op::Smooth, Op::Residual> () const {
  return *(m_smoothState.m_residual);
}

template<>
inline const TRACKVECTORFIT_PRECISION& Node::get<Op::Smooth, Op::ErrResidual> () const {
  return *(m_smoothState.m_errResidual);
}

// -------------------------
// ref getters for the above
// -------------------------

template<>
inline TRACKVECTORFIT_PRECISION& Node::get<Op::NodeParameters, Op::ReferenceResidual> () {
  return *(m_nodeParameters.m_referenceResidual);
}

template<>
inline TRACKVECTORFIT_PRECISION& Node::get<Op::NodeParameters, Op::ErrMeasure> () {
  return *(m_nodeParameters.m_errorMeasure);
}

template<>
inline TrackVector& Node::get<Op::NodeParameters, Op::ReferenceVector> () {
  return m_nodeParameters.m_referenceVector;
}

template<>
inline TrackVector& Node::get<Op::NodeParameters, Op::ProjectionMatrix> () {
  return m_nodeParameters.m_projectionMatrix;
}

template<>
inline TRACKVECTORFIT_PRECISION& Node::get<Op::Forward, Op::Chi2> () {
  return *(m_forwardState.m_chi2);
}

template<>
inline TrackVector& Node::get<Op::Forward, Op::StateVector> () {
  return m_forwardState.m_updatedState;
}

template<>
inline TrackSymMatrix& Node::get<Op::Forward, Op::Covariance> () {
  return m_forwardState.m_updatedCovariance;
}

template<>
inline TrackSymMatrix& Node::get<Op::NodeParameters, Op::NoiseMatrix> () {
  return m_nodeParameters.m_noiseMatrix;
}

template<>
inline TrackVector& Node::get<Op::NodeParameters, Op::TransportVector> () {
  return m_nodeParameters.m_transportVector;
}

template<>
inline TrackMatrix<25>& Node::get<Op::Forward, Op::TransportMatrix> () {
  return m_forwardTransportMatrix;
}

template<>
inline TRACKVECTORFIT_PRECISION& Node::get<Op::Backward, Op::Chi2> () {
  return *(m_backwardState.m_chi2);
}

template<>
inline TrackVector& Node::get<Op::Backward, Op::StateVector> () {
  return m_backwardState.m_updatedState;
}

template<>
inline TrackSymMatrix& Node::get<Op::Backward, Op::Covariance> () {
  return m_backwardState.m_updatedCovariance;
}

template<>
inline TrackMatrix<25>& Node::get<Op::Backward, Op::TransportMatrix> () {
  return m_backwardTransportMatrix;
}

template<>
inline TrackVector& Node::get<Op::Smooth, Op::StateVector> () {
  return m_smoothState.m_state;
}

template<>
inline TrackSymMatrix& Node::get<Op::Smooth, Op::Covariance> () {
  return m_smoothState.m_covariance;
}

template<>
inline TRACKVECTORFIT_PRECISION& Node::get<Op::Smooth, Op::Residual> () {
  return *(m_smoothState.m_residual);
}

template<>
inline TRACKVECTORFIT_PRECISION& Node::get<Op::Smooth, Op::ErrResidual> () {
  return *(m_smoothState.m_errResidual);
}
