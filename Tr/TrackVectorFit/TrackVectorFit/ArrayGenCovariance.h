/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <type_traits>
#include "VectorConfiguration.h"

// #include "Math/SVector.h"
// #include "LHCbMath/MatrixManip.h"
// #include "Event/StateVector.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"

// ------------------
// Array constructors
// ------------------

namespace Tr {

namespace TrackVectorFit {

struct ArrayGen {
  template<size_t W>
  static inline constexpr uint16_t mask () {
    return (W==16 ? 0xFFFF : (W==8 ? 0xFF : (W==4 ? 0x0F : 0x03)));
  }

  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 15> InitialCovariance_values () {
    return {
      400,
      0, 400,
      0, 0, 0.01,
      0, 0, 0, 0.01,
      0, 0, 0, 0, 0.0001
    };
  }

  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 15> InitialCovariance_values (
    const size_t
  ) {
    return InitialCovariance_values();
  }

  static inline Gaudi::TrackSymMatrix InitialCovariance_SymMatrix () {
    Gaudi::TrackSymMatrix initialCovarianceSymMatrix;
    for (unsigned i=0; i<15; ++i) {
      initialCovarianceSymMatrix.Array()[i] = InitialCovariance_values()[i];
    }
    return initialCovarianceSymMatrix;
  }

  template<size_t W, std::size_t... Is>
  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 15*W> InitialCovariance_helper (
    std::index_sequence<Is...>
  ) {
    return {
      InitialCovariance_values(Is)[0]...,
      InitialCovariance_values(Is)[1]...,
      InitialCovariance_values(Is)[2]...,
      InitialCovariance_values(Is)[3]...,
      InitialCovariance_values(Is)[4]...,
      InitialCovariance_values(Is)[5]...,
      InitialCovariance_values(Is)[6]...,
      InitialCovariance_values(Is)[7]...,
      InitialCovariance_values(Is)[8]...,
      InitialCovariance_values(Is)[9]...,
      InitialCovariance_values(Is)[10]...,
      InitialCovariance_values(Is)[11]...,
      InitialCovariance_values(Is)[12]...,
      InitialCovariance_values(Is)[13]...,
      InitialCovariance_values(Is)[14]...
    };
  }

  template<size_t W>
  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 15*W> InitialCovariance () {
    return InitialCovariance_helper<W>(std::make_index_sequence<W>{});
  }
};

}

}
