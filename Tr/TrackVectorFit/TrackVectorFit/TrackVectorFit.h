/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <vector>
#include <array>
#include <iostream>
#include "Kernel/VectorSOAStore.h"
#include "Kernel/VectorSOAMatrixView.h"
#include "Kernel/VectorSOAIterator.h"
#include "MemView.h"
#include "Types.h"
#include "Scheduler.h"
#include "scalar/Predict.h"
#include "scalar/Update.h"
#include "scalar/Combined.h"
#include "scalar/Smoother.h"
#include "vector/Predict.h"
#include "vector/Update.h"
#include "vector/Smoother.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "ArrayGenStates.h"

namespace Tr {

namespace TrackVectorFit {

struct TrackVectorFit {
  unsigned m_memManagerStorageIncrements;
  bool m_debug;
  bool m_verbose;
  // The scheduler
  std::list<Sch::Blueprint<vector_width()>> m_scheduler;

  // VectorFit MemManagers local to this TrackVectorFit
  Mem::Store m_smoothStore;
  Mem::Store m_nodeParametersStore;
  Mem::Store m_transportForwardStore;
  Mem::Store m_transportBackwardStore;
  Mem::Store m_forwardStateStore;
  Mem::Store m_backwardStateStore;

  Mem::Iterator m_forwardNodeIterator;
  Mem::Iterator m_forwardStoreIterator;
  Mem::ReverseIterator m_smoothStoreIterator;
  Mem::ReverseIterator m_forwardMainStoreIterator;
  
  // Stores with TES ownership
  std::vector<Mem::Store>::iterator m_forwardStore;
  std::vector<Mem::Store>::iterator m_backwardStore;

  /**
   * @brief Constructor
   * 
   * @param memManagerStorageIncrements Storage increment for each new chunk of data
   * @param debug Debug flag
   */
  TrackVectorFit (
    const unsigned& memManagerStorageIncrements = 1024,
    const bool& debug = false,
    const bool& verbose = false
  ) : m_memManagerStorageIncrements(memManagerStorageIncrements),
    m_debug(debug), m_verbose(verbose) {
    m_smoothStore = Mem::Store(Mem::View::SmoothState::size(), memManagerStorageIncrements);
    m_nodeParametersStore = Mem::Store(Mem::View::NodeParameters::size(), memManagerStorageIncrements);
    m_transportForwardStore = Mem::Store(Mem::View::TrackMatrix<25>::size(), memManagerStorageIncrements);
    m_transportBackwardStore = Mem::Store(Mem::View::TrackMatrix<25>::size(), memManagerStorageIncrements);
    m_forwardStateStore = Mem::Store(Mem::View::State::size(), memManagerStorageIncrements);
    m_backwardStateStore = Mem::Store(Mem::View::State::size(), memManagerStorageIncrements);
  }

  /**
   * @brief      Reference getter for m_scheduler
   *
   * @return     The scheduler
   */
  std::list<Sch::Blueprint<vector_width()>>& scheduler () {
    return m_scheduler;
  }

  /**
   * @brief Resets the stores, add new stores to updatedStatesStore
   *        that will contain the forward and backward stores.
   * 
   * @param updatedStatesStore Stores for keeping the updated states,
   *                           both forward and backward.
   * @param debug Sets the debug flag across the fitter
   */
  void reset (
    const bool& debug=false,
    const bool& verbose=false
  ) {
    m_debug = debug;
    m_verbose = verbose;

    // Reserve updated states stores
    // updatedStatesStore.emplace_back(Mem::View::State::size(), m_memManagerStorageIncrements);
    // updatedStatesStore.emplace_back(Mem::View::State::size(), m_memManagerStorageIncrements);
    // m_forwardStore  = updatedStatesStore.end() - 2;
    // m_backwardStore = updatedStatesStore.end() - 1;
    
    // Only reset stores not resetted in initializeBasePointers
    m_smoothStore.reset();
    m_nodeParametersStore.reset();
    m_forwardStateStore.reset();
    m_backwardStateStore.reset();
  }

  /**
   * @brief Initializes the scheduler and base pointers
   *        for all tracks
   *        
   * @details A static scheduler schedules the execution of all nodes in the tracks
   *          for a machine configured with vector_width() SIMD width. The scheduler
   *          follows a DTA (Decreasing Time Algorithm) in order to minimize the
   *          number of iterations.
   *          
   *          Additionally, all base pointers are initialized for all nodes.
   *          Consequent accesses to any data member in the Nodes is possible.
   *          Iterators are also initialized in preparation of the upcoming fit.
   * 
   * @param tracks Tracks to be scheduled and initialized.
   * @tparam updateRefVectors Boolean to indicate whether to update reference vectors
   *                          prior to populating the smooth pointer.
   */
  template<bool updateRefVectors>
  void initializeBasePointers (
    std::list<std::reference_wrapper<Track>>& tracks,
    bool copyNoiseMatrix=false
  ) {
    m_transportForwardStore.reset();
    m_transportBackwardStore.reset();

    if (m_debug) {
      std::cout << "Initializing base pointers of " << tracks.size() << " tracks" << std::endl;
    }

    // Schedule what we are going to do
    m_scheduler = Sch::StaticScheduler<vector_width()>::generate(tracks);

    // Populates the base pointers with the scheduler provided
    auto populateBasePointers =
    [&] (
      std::list<Sch::Blueprint<vector_width()>>& sch
    ) {
      std::for_each(sch.begin(), sch.end(), [&] (Sch::Blueprint<vector_width()>& s) {
        auto& pool = s.pool;
        const auto& action = s.action;

        m_nodeParametersStore.getNewVector();
        m_forwardStateStore.getNewVector();
        m_backwardStateStore.getNewVector();
        m_smoothStore.getNewVector();
        m_transportForwardStore.getNewVector();
        m_transportBackwardStore.getNewVector();

        for (unsigned i=0; i<vector_width(); ++i) {
          if (action[i]) {
            Node& n = *(pool[i].node);

            // Note: We should evaluate the impact of this
            // It is probably worth copying, as now we iterate forward and
            // backward through the set
            if (copyNoiseMatrix) {
              auto noiseMatrix = n.get<Op::NodeParameters, Op::NoiseMatrix>();
              n.m_nodeParameters.setBasePointer(m_nodeParametersStore.getLastVector() + i);
              n.get<Op::NodeParameters, Op::NoiseMatrix>().copy(noiseMatrix);
            } else {
              n.m_nodeParameters.setBasePointer(m_nodeParametersStore.getLastVector() + i);
            }
            
            // Note: In c++17 we can even do
            //       if constexpr (updateRefVectors) { ... }
            if (updateRefVectors) {
              n.get<Op::NodeParameters, Op::ReferenceVector>().copy(n.get<Op::Smooth, Op::StateVector>());
            }

            n.m_forwardState.setBasePointer(m_forwardStateStore.getLastVector() + i);
            n.m_backwardState.setBasePointer(m_backwardStateStore.getLastVector() + i);
            n.m_smoothState.setBasePointer(m_smoothStore.getLastVector() + i);
            n.get<Op::Forward, Op::TransportMatrix>().setBasePointer(m_transportForwardStore.getLastVector() + i);
            n.get<Op::Backward, Op::TransportMatrix>().setBasePointer(m_transportBackwardStore.getLastVector() + i);
          }
        }
      });
    };

    // Align the stores to point to new vectors, and initialize iterator attributes
    m_forwardStateStore.align();
    m_backwardStateStore.align();
    m_smoothStore.align();
    m_nodeParametersStore.align();
    m_forwardNodeIterator = Mem::Iterator(m_nodeParametersStore, true);
    m_forwardStoreIterator = Mem::Iterator(m_forwardStateStore, true);

    populateBasePointers(m_scheduler);
    
    // Initialize the reverse mainstore iterators here
    m_forwardMainStoreIterator = Mem::ReverseIterator(m_forwardStateStore);
    m_smoothStoreIterator = Mem::ReverseIterator(m_smoothStore);
  }

  template<bool checkNodeType>
  void smoothFit (
    std::list<std::reference_wrapper<Track>>& tracks
  );
  
  /**
   * @brief Populate the nodes with the calculated fit information.
   *        This performs the AOSOA -> AOS conversion.
   */
  void populateNodes (
    Track& track
  ) {
    std::for_each(track.nodes().begin(), track.nodes().end(), [] (Node& n) {
      auto& node = n.node();

      if (node.type() == LHCb::Node::Type::HitOnTrack) {
        node.setErrMeasure(n.get<Op::NodeParameters, Op::ErrMeasure>());
        node.setProjectionMatrix((Gaudi::TrackProjectionMatrix) n.get<Op::NodeParameters, Op::ProjectionMatrix>());
        node.setForwardState(
          (Gaudi::TrackVector) n.get<Op::Forward, Op::StateVector>(),
          (Gaudi::TrackSymMatrix) n.get<Op::Forward, Op::Covariance>()
        );
        node.setBackwardState(
          (Gaudi::TrackVector) n.get<Op::Backward, Op::StateVector>(),
          (Gaudi::TrackSymMatrix) n.get<Op::Backward, Op::Covariance>()
        );
        node.setResidual(n.get<Op::Smooth, Op::Residual>());
        node.setErrResidual(n.get<Op::Smooth, Op::ErrResidual>());
        const auto smoothStateVector = (Gaudi::TrackVector) n.get<Op::Smooth, Op::StateVector>();
        node.setRefVector(smoothStateVector);
        node.setState(
          smoothStateVector,
          (Gaudi::TrackSymMatrix) n.get<Op::Smooth, Op::Covariance>(),
          node.z());
      }
    });
  }
};

}

}

#include "TrackVectorFit.h_impl"
