/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/VectorSOAMatrixView.h"
#include "Kernel/VectorSOAIterator.h"
#include "VectorConfiguration.h"

namespace Tr {

namespace TrackVectorFit {

// Operators accepted for templated get method
namespace Op {
  // Data container
  class NodeParameters;
  class Backward;
  class Forward;
  class Smooth;

  // Data type
  // NodeParameters
  class ReferenceVector;
  class ProjectionMatrix;
  class ReferenceResidual;
  class ErrMeasure;
  class TransportVector;
  class NoiseMatrix;

  // Forward, Backward, Smooth
  class StateVector;
  class Covariance;

  // Forward, Backward
  class TransportMatrix;
  class Chi2;

  // Smooth
  class Residual;
  class ErrResidual;
}

namespace Mem {

using Store = LHCb::Vector::Mem::Store<TRACKVECTORFIT_PRECISION>;
using Iterator = LHCb::Vector::Mem::Iterator<TRACKVECTORFIT_PRECISION>;
using ReverseIterator = LHCb::Vector::Mem::ReverseIterator<TRACKVECTORFIT_PRECISION>;

namespace View {

template<size_t N>
using TrackMatrix = LHCb::Vector::Mem::View::Matrix<TRACKVECTORFIT_PRECISION, N>;
using TrackVector = LHCb::Vector::Mem::View::TrackVector<TRACKVECTORFIT_PRECISION>;
using TrackSymMatrix = LHCb::Vector::Mem::View::TrackSymMatrix<TRACKVECTORFIT_PRECISION>;

struct NodeParameters {
  constexpr static size_t size () { return 32; }
  TRACKVECTORFIT_PRECISION* m_basePointer = nullptr;
  TrackVector m_referenceVector;
  TrackVector m_projectionMatrix;
  TrackVector m_transportVector;
  TrackSymMatrix m_noiseMatrix;
  TRACKVECTORFIT_PRECISION* m_referenceResidual;
  TRACKVECTORFIT_PRECISION* m_errorMeasure;

  NodeParameters () = default;
  NodeParameters (const NodeParameters& copy) = default;
  NodeParameters (TRACKVECTORFIT_PRECISION* m_basePointer) {
    setBasePointer(m_basePointer);
  }

  void setBasePointer (TRACKVECTORFIT_PRECISION* p) {
    m_basePointer = p;
    m_referenceVector = TrackVector(p); p += 5 * vector_width();
    m_projectionMatrix = TrackVector(p); p += 5 * vector_width();
    m_transportVector = TrackVector(p); p += 5 * vector_width();
    m_noiseMatrix = TrackSymMatrix(p); p += 15 * vector_width();
    m_referenceResidual = p; p += vector_width();
    m_errorMeasure = p;
  }

  void setBasePointer (const NodeParameters& s) {
    setBasePointer(s.m_basePointer);
  }

  void copy (const NodeParameters& s) {
    m_referenceVector.copy(s.m_referenceVector);
    m_projectionMatrix.copy(s.m_projectionMatrix);
    m_transportVector.copy(s.m_transportVector);
    m_noiseMatrix.copy(s.m_noiseMatrix);
    *m_referenceResidual = *s.m_referenceResidual;
    *m_errorMeasure = *s.m_errorMeasure;
  }
};

struct SmoothState {
  constexpr static size_t size () { return 22; }
  TRACKVECTORFIT_PRECISION* m_basePointer = nullptr;
  TrackVector m_state;
  TrackSymMatrix m_covariance;
  TRACKVECTORFIT_PRECISION* m_residual;
  TRACKVECTORFIT_PRECISION* m_errResidual;

  SmoothState () = default;
  SmoothState (const SmoothState& copy) = default;
  SmoothState (TRACKVECTORFIT_PRECISION* m_basePointer) {
    setBasePointer(m_basePointer);
  }

  void setBasePointer (TRACKVECTORFIT_PRECISION* p) {
    m_basePointer = p;
    m_state = TrackVector(p); p += 5 * vector_width();
    m_covariance = TrackSymMatrix(p); p += 15 * vector_width();
    m_residual = p; p += vector_width();
    m_errResidual = p;
  }

  void setBasePointer (const SmoothState& s) {
    setBasePointer(s.m_basePointer);
  }

  void copy (const SmoothState& s) {
    m_state.copy(s.m_state);
    m_covariance.copy(s.m_covariance);
    *m_residual = *s.m_residual;
    *m_errResidual = *s.m_errResidual;
  }
};

struct State {
  constexpr static size_t size () { return 21; }
  TRACKVECTORFIT_PRECISION* m_basePointer = nullptr;
  TrackVector m_updatedState;
  TrackSymMatrix m_updatedCovariance;
  TRACKVECTORFIT_PRECISION* m_chi2;

  State () = default;
  State (const State& copy) = default;
  State (TRACKVECTORFIT_PRECISION* m_basePointer) {
    setBasePointer(m_basePointer);
  }

  void setBasePointer (TRACKVECTORFIT_PRECISION* p) {
    m_basePointer = p;
    m_updatedState = TrackVector(p); p += 5 * vector_width();
    m_updatedCovariance = TrackSymMatrix(p); p += 15 * vector_width();
    m_chi2 = p;
  }

  void setBasePointer (const State& s) {
    setBasePointer(s.m_basePointer);
  }

  void copy (const State& s) {
    m_updatedState.copy(s.m_updatedState);
    m_updatedCovariance.copy(s.m_updatedCovariance);
    *m_chi2 = *(s.m_chi2);
  }
};

}

}

}

}
