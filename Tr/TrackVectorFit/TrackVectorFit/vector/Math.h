/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <array>
#include "../Types.h"
#include "../VectorConfiguration.h"

namespace Tr {

namespace TrackVectorFit {

namespace Vector {

template<unsigned W>
struct MathCommon {
  using vectype = typename Vectype<W>::type;
  using boolvectype = typename Vectype<W>::booltype;

  /**
   * @brief      Creates a boolvectype populated with true if the node type
   *             is HitOnTrack and false otherwise
   */
  template <size_t... Is>
  static constexpr typename Vectype<W>::booltype makeBoolHitOnTrack (
    const std::array<Sch::Item, W>& nodes,
    std::index_sequence<Is...>
  ) {
    return typename Vectype<W>::booltype(nodes[Is].node->node().type() == LHCb::Node::Type::HitOnTrack...);
  }

  /**
   * @brief      Similarity transform, manually horizontally vectorised
   *
   * @param[in]  tm  Transport matrix
   * @param[in]  uc  Previous update covariance
   * @param      pc  Predicted covariance
   */
  static inline void similarity_5_5 (
    fp_ptr_64_const tm_p,
    fp_ptr_64_const uc_p,
    fp_ptr_64 pc_p
  ) {
    std::array<vectype, 25> tm;
    std::array<vectype, 15> uc;
    std::array<vectype, 15> pc;
    std::array<vectype, 5> v;

    // Load tm
    tm[0].load_a(tm_p + 0*W);
    tm[1].load_a(tm_p + 1*W);
    tm[2].load_a(tm_p + 2*W);
    tm[3].load_a(tm_p + 3*W);
    tm[4].load_a(tm_p + 4*W);
    tm[5].load_a(tm_p + 5*W);
    tm[6].load_a(tm_p + 6*W);
    tm[7].load_a(tm_p + 7*W);
    tm[8].load_a(tm_p + 8*W);
    tm[9].load_a(tm_p + 9*W);
    tm[10].load_a(tm_p + 10*W);
    tm[11].load_a(tm_p + 11*W);
    tm[12].load_a(tm_p + 12*W);
    tm[13].load_a(tm_p + 13*W);
    tm[14].load_a(tm_p + 14*W);
    tm[15].load_a(tm_p + 15*W);
    tm[16].load_a(tm_p + 16*W);
    tm[17].load_a(tm_p + 17*W);
    tm[18].load_a(tm_p + 18*W);
    tm[19].load_a(tm_p + 19*W);
    tm[20].load_a(tm_p + 20*W);
    tm[21].load_a(tm_p + 21*W);
    tm[22].load_a(tm_p + 22*W);
    tm[23].load_a(tm_p + 23*W);
    tm[24].load_a(tm_p + 24*W);

    // Load uc
    uc[0].load_a(uc_p + 0*W);
    uc[1].load_a(uc_p + 1*W);
    uc[2].load_a(uc_p + 2*W);
    uc[3].load_a(uc_p + 3*W);
    uc[4].load_a(uc_p + 4*W);
    uc[5].load_a(uc_p + 5*W);
    uc[6].load_a(uc_p + 6*W);
    uc[7].load_a(uc_p + 7*W);
    uc[8].load_a(uc_p + 8*W);
    uc[9].load_a(uc_p + 9*W);
    uc[10].load_a(uc_p + 10*W);
    uc[11].load_a(uc_p + 11*W);
    uc[12].load_a(uc_p + 12*W);
    uc[13].load_a(uc_p + 13*W);
    uc[14].load_a(uc_p + 14*W);

    v[0] = uc[0] *tm[0]+uc[1] *tm[1]+uc[3] *tm[2]+uc[6] *tm[3]+uc[10]*tm[4];
    v[1] = uc[1] *tm[0]+uc[2] *tm[1]+uc[4] *tm[2]+uc[7] *tm[3]+uc[11]*tm[4];
    v[2] = uc[3] *tm[0]+uc[4] *tm[1]+uc[5] *tm[2]+uc[8] *tm[3]+uc[12]*tm[4];
    v[3] = uc[6] *tm[0]+uc[7] *tm[1]+uc[8] *tm[2]+uc[9] *tm[3]+uc[13]*tm[4];
    v[4] = uc[10]*tm[0]+uc[11]*tm[1]+uc[12]*tm[2]+uc[13]*tm[3]+uc[14]*tm[4];

    pc[0]  = tm[0] *v[0] + tm[1] *v[1] + tm[2] *v[2] + tm[3] *v[3] + tm[4] *v[4];
    pc[1]  = tm[5] *v[0] + tm[6] *v[1] + tm[7] *v[2] + tm[8] *v[3] + tm[9] *v[4];
    pc[3]  = tm[10]*v[0] + tm[11]*v[1] + tm[12]*v[2] + tm[13]*v[3] + tm[14]*v[4];
    pc[6]  = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    pc[10] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];

    v[0] =  uc[0] *tm[5]+uc[1] *tm[6]+uc[3] *tm[7]+uc[6] *tm[8]+uc[10]*tm[9];
    v[1] =  uc[1] *tm[5]+uc[2] *tm[6]+uc[4] *tm[7]+uc[7] *tm[8]+uc[11]*tm[9];
    v[2] =  uc[3] *tm[5]+uc[4] *tm[6]+uc[5] *tm[7]+uc[8] *tm[8]+uc[12]*tm[9];
    v[3] =  uc[6] *tm[5]+uc[7] *tm[6]+uc[8] *tm[7]+uc[9] *tm[8]+uc[13]*tm[9];
    v[4] =  uc[10]*tm[5]+uc[11]*tm[6]+uc[12]*tm[7]+uc[13]*tm[8]+uc[14]*tm[9];

    pc[2]  = tm[5] *v[0] + tm[6] *v[1] + tm[7] *v[2] + tm[8] *v[3] + tm[9] *v[4];
    pc[4]  = tm[10]*v[0] + tm[11]*v[1] + tm[12]*v[2] + tm[13]*v[3] + tm[14]*v[4];
    pc[7]  = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    pc[11] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];

    v[0] = uc[0] *tm[10]+uc[1] *tm[11]+uc[3] *tm[12]+uc[6] *tm[13]+uc[10]*tm[14];
    v[1] = uc[1] *tm[10]+uc[2] *tm[11]+uc[4] *tm[12]+uc[7] *tm[13]+uc[11]*tm[14];
    v[2] = uc[3] *tm[10]+uc[4] *tm[11]+uc[5] *tm[12]+uc[8] *tm[13]+uc[12]*tm[14];
    v[3] = uc[6] *tm[10]+uc[7] *tm[11]+uc[8] *tm[12]+uc[9] *tm[13]+uc[13]*tm[14];
    v[4] = uc[10]*tm[10]+uc[11]*tm[11]+uc[12]*tm[12]+uc[13]*tm[13]+uc[14]*tm[14];

    pc[5]  = tm[10]*v[0] + tm[11]*v[1] + tm[12]*v[2] + tm[13]*v[3] + tm[14]*v[4];
    pc[8]  = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    pc[12] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];

    v[0] = uc[0] *tm[15]+uc[1] *tm[16]+uc[3] *tm[17]+uc[6] *tm[18]+uc[10]*tm[19];
    v[1] = uc[1] *tm[15]+uc[2] *tm[16]+uc[4] *tm[17]+uc[7] *tm[18]+uc[11]*tm[19];
    v[2] = uc[3] *tm[15]+uc[4] *tm[16]+uc[5] *tm[17]+uc[8] *tm[18]+uc[12]*tm[19];
    v[3] = uc[6] *tm[15]+uc[7] *tm[16]+uc[8] *tm[17]+uc[9] *tm[18]+uc[13]*tm[19];
    v[4] = uc[10]*tm[15]+uc[11]*tm[16]+uc[12]*tm[17]+uc[13]*tm[18]+uc[14]*tm[19];

    pc[9]  = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    pc[13] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];

    v[0] = uc[0] *tm[20]+uc[1]*tm[21]+uc[3]*tm[22]+uc[6]*tm[23]+uc[10]*tm[24];
    v[1] = uc[1] *tm[20]+uc[2]*tm[21]+uc[4]*tm[22]+uc[7]*tm[23]+uc[11]*tm[24];
    v[2] = uc[3] *tm[20]+uc[4]*tm[21]+uc[5]*tm[22]+uc[8]*tm[23]+uc[12]*tm[24];
    v[3] = uc[6] *tm[20]+uc[7]*tm[21]+uc[8]*tm[22]+uc[9]*tm[23]+uc[13]*tm[24];
    v[4] = uc[10]*tm[20]+uc[11]*tm[21]+uc[12]*tm[22]+uc[13]*tm[23]+uc[14]*tm[24];

    pc[14]= tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];

    // Store pc
    pc[0].store_a(pc_p + 0*W);
    pc[1].store_a(pc_p + 1*W);
    pc[2].store_a(pc_p + 2*W);
    pc[3].store_a(pc_p + 3*W);
    pc[4].store_a(pc_p + 4*W);
    pc[5].store_a(pc_p + 5*W);
    pc[6].store_a(pc_p + 6*W);
    pc[7].store_a(pc_p + 7*W);
    pc[8].store_a(pc_p + 8*W);
    pc[9].store_a(pc_p + 9*W);
    pc[10].store_a(pc_p + 10*W);
    pc[11].store_a(pc_p + 11*W);
    pc[12].store_a(pc_p + 12*W);
    pc[13].store_a(pc_p + 13*W);
    pc[14].store_a(pc_p + 14*W);
  }

  /**
   * @brief      Vectorised update
   *
   * @param      us          Updated state
   * @param      uc          Updated covariance
   * @param      chi2        Chi2
   * @param[in]  ps          Predicted state
   * @param[in]  pc          Predicted covariance
   * @param[in]  Xref        Reference parameters
   * @param[in]  H           Projection matrix
   * @param[in]  refResidual Residual of reference
   * @param[in]  errorMeas2  Measurement error squared
   * @param[in]  nodes       Nodes
   */
  template<bool checkNodeType>
  static inline void update (
    fp_ptr_64_const rv_p,
    fp_ptr_64_const pm_p,
    fp_ptr_64_const rr_p,
    fp_ptr_64_const em_p,
    fp_ptr_64 us_p,
    fp_ptr_64 uc_p,
    fp_ptr_64 chi2_p,
    const std::array<Sch::Item, W>& n
  )  {
    std::array<vectype, 15> uc;
    std::array<vectype, 5> us;
    std::array<vectype, 5> Xref;
    std::array<vectype, 5> H;
    std::array<vectype, 5> cht;
    vectype res, refResidual, errorMeas, chi2;

    // Load predicted state
    us[0].load_a(us_p + 0*W);
    us[1].load_a(us_p + 1*W);
    us[2].load_a(us_p + 2*W);
    us[3].load_a(us_p + 3*W);
    us[4].load_a(us_p + 4*W);

    // Load predicted covariance
    uc[0].load_a(uc_p + 0*W);
    uc[1].load_a(uc_p + 1*W);
    uc[2].load_a(uc_p + 2*W);
    uc[3].load_a(uc_p + 3*W);
    uc[4].load_a(uc_p + 4*W);
    uc[5].load_a(uc_p + 5*W);
    uc[6].load_a(uc_p + 6*W);
    uc[7].load_a(uc_p + 7*W);
    uc[8].load_a(uc_p + 8*W);
    uc[9].load_a(uc_p + 9*W);
    uc[10].load_a(uc_p + 10*W);
    uc[11].load_a(uc_p + 11*W);
    uc[12].load_a(uc_p + 12*W);
    uc[13].load_a(uc_p + 13*W);
    uc[14].load_a(uc_p + 14*W);

    // Load Xref
    Xref[0].load_a(rv_p + 0*W);
    Xref[1].load_a(rv_p + 1*W);
    Xref[2].load_a(rv_p + 2*W);
    Xref[3].load_a(rv_p + 3*W);
    Xref[4].load_a(rv_p + 4*W);

    // Load H
    H[0].load_a(pm_p + 0*W);
    H[1].load_a(pm_p + 1*W);
    H[2].load_a(pm_p + 2*W);
    H[3].load_a(pm_p + 3*W);
    H[4].load_a(pm_p + 4*W);

    // load vrefResidual
    refResidual.load_a(rr_p);

    // load verrorMeas
    errorMeas.load_a(em_p);

    res = refResidual
      +  H[0] * (Xref[0] - us[0])
      +  H[1] * (Xref[1] - us[1])
      +  H[2] * (Xref[2] - us[2])
      +  H[3] * (Xref[3] - us[3])
      +  H[4] * (Xref[4] - us[4]);

    cht[0] = uc[0] *H[0] + uc[1] *H[1] + uc[3] *H[2] + uc[6] *H[3] + uc[10]*H[4];
    cht[1] = uc[1] *H[0] + uc[2] *H[1] + uc[4] *H[2] + uc[7] *H[3] + uc[11]*H[4];
    cht[2] = uc[3] *H[0] + uc[4] *H[1] + uc[5] *H[2] + uc[8] *H[3] + uc[12]*H[4];
    cht[3] = uc[6] *H[0] + uc[7] *H[1] + uc[8] *H[2] + uc[9] *H[3] + uc[13]*H[4];
    cht[4] = uc[10]*H[0] + uc[11]*H[1] + uc[12]*H[2] + uc[13]*H[3] + uc[14]*H[4];

    const vectype denominator = errorMeas * errorMeas
                                + H[0] * cht[0]
                                + H[1] * cht[1]
                                + H[2] * cht[2]
                                + H[3] * cht[3]
                                + H[4] * cht[4];
    // const vectype denNotZero = select(denominator != 0, denominator, 1.);
    // const vectype errorResInv = 1. / denNotZero;
    vectype errorResInv = cast(1.) / denominator;

    // This should be optimized at compile time
    // With c++17: if constexpr (checkNodeType) { ... }
    if (checkNodeType) {
      const boolvectype maskedVector = makeBoolHitOnTrack(n, std::make_index_sequence<W>());
      errorResInv = select(maskedVector, errorResInv, cast(0.));
    }

    // update the state vector and cov matrix
    const vectype w = res * errorResInv;

    us[0] += cht[0] * w;
    us[1] += cht[1] * w;
    us[2] += cht[2] * w;
    us[3] += cht[3] * w;
    us[4] += cht[4] * w;

    uc[0]  -= errorResInv * cht[0] * cht[0];
    uc[1]  -= errorResInv * cht[1] * cht[0];
    uc[2]  -= errorResInv * cht[1] * cht[1];
    uc[3]  -= errorResInv * cht[2] * cht[0];
    uc[4]  -= errorResInv * cht[2] * cht[1];
    uc[5]  -= errorResInv * cht[2] * cht[2];
    uc[6]  -= errorResInv * cht[3] * cht[0];
    uc[7]  -= errorResInv * cht[3] * cht[1];
    uc[8]  -= errorResInv * cht[3] * cht[2];
    uc[9]  -= errorResInv * cht[3] * cht[3];
    uc[10] -= errorResInv * cht[4] * cht[0];
    uc[11] -= errorResInv * cht[4] * cht[1];
    uc[12] -= errorResInv * cht[4] * cht[2];
    uc[13] -= errorResInv * cht[4] * cht[3];
    uc[14] -= errorResInv * cht[4] * cht[4];

    chi2 = res * res * errorResInv;

    // Store uc
    uc[0].store_a(uc_p + 0*W);
    uc[1].store_a(uc_p + 1*W);
    uc[2].store_a(uc_p + 2*W);
    uc[3].store_a(uc_p + 3*W);
    uc[4].store_a(uc_p + 4*W);
    uc[5].store_a(uc_p + 5*W);
    uc[6].store_a(uc_p + 6*W);
    uc[7].store_a(uc_p + 7*W);
    uc[8].store_a(uc_p + 8*W);
    uc[9].store_a(uc_p + 9*W);
    uc[10].store_a(uc_p + 10*W);
    uc[11].store_a(uc_p + 11*W);
    uc[12].store_a(uc_p + 12*W);
    uc[13].store_a(uc_p + 13*W);
    uc[14].store_a(uc_p + 14*W);

    // Store us
    us[0].store_a(us_p + 0*W);
    us[1].store_a(us_p + 1*W);
    us[2].store_a(us_p + 2*W);
    us[3].store_a(us_p + 3*W);
    us[4].store_a(us_p + 4*W);

    // Store chi2
    chi2.store_a(chi2_p);
  }

  static inline uint16_t average (
    fp_ptr_64_const X1_p,
    fp_ptr_64_const X2_p,
    fp_ptr_64_const C1_p,
    fp_ptr_64_const C2_p,
    fp_ptr_64 X_p,
    fp_ptr_64 C_p
  ) {
    std::array<vectype, 25> K;
    std::array<vectype, 5> X1;
    std::array<vectype, 5> X2;
    std::array<vectype, 15> C1;
    std::array<vectype, 15> C2;
    std::array<vectype, 15> Csum;
    std::array<vectype, 5> X;
    std::array<vectype, 15> C;
    std::array<vectype, 15> L;
    std::array<vectype, 15> inv;
    std::array<vectype, 5> diff;

    boolvectype success (true);

    // Load X1
    X1[0].load_a(X1_p + 0*W);
    X1[1].load_a(X1_p + 1*W);
    X1[2].load_a(X1_p + 2*W);
    X1[3].load_a(X1_p + 3*W);
    X1[4].load_a(X1_p + 4*W);

    // Load X2
    X2[0].load_a(X2_p + 0*W);
    X2[1].load_a(X2_p + 1*W);
    X2[2].load_a(X2_p + 2*W);
    X2[3].load_a(X2_p + 3*W);
    X2[4].load_a(X2_p + 4*W);

    // Load C1
    C1[0].load_a(C1_p + 0*W);
    C1[1].load_a(C1_p + 1*W);
    C1[2].load_a(C1_p + 2*W);
    C1[3].load_a(C1_p + 3*W);
    C1[4].load_a(C1_p + 4*W);
    C1[5].load_a(C1_p + 5*W);
    C1[6].load_a(C1_p + 6*W);
    C1[7].load_a(C1_p + 7*W);
    C1[8].load_a(C1_p + 8*W);
    C1[9].load_a(C1_p + 9*W);
    C1[10].load_a(C1_p + 10*W);
    C1[11].load_a(C1_p + 11*W);
    C1[12].load_a(C1_p + 12*W);
    C1[13].load_a(C1_p + 13*W);
    C1[14].load_a(C1_p + 14*W);

    // Load C2
    C2[0].load_a(C2_p + 0*W);
    C2[1].load_a(C2_p + 1*W);
    C2[2].load_a(C2_p + 2*W);
    C2[3].load_a(C2_p + 3*W);
    C2[4].load_a(C2_p + 4*W);
    C2[5].load_a(C2_p + 5*W);
    C2[6].load_a(C2_p + 6*W);
    C2[7].load_a(C2_p + 7*W);
    C2[8].load_a(C2_p + 8*W);
    C2[9].load_a(C2_p + 9*W);
    C2[10].load_a(C2_p + 10*W);
    C2[11].load_a(C2_p + 11*W);
    C2[12].load_a(C2_p + 12*W);
    C2[13].load_a(C2_p + 13*W);
    C2[14].load_a(C2_p + 14*W);

    // C sum
    Csum[0]  = C1[0]  + C2[0];
    Csum[1]  = C1[1]  + C2[1];
    Csum[2]  = C1[2]  + C2[2];
    Csum[3]  = C1[3]  + C2[3];
    Csum[4]  = C1[4]  + C2[4];
    Csum[5]  = C1[5]  + C2[5];
    Csum[6]  = C1[6]  + C2[6];
    Csum[7]  = C1[7]  + C2[7];
    Csum[8]  = C1[8]  + C2[8];
    Csum[9]  = C1[9]  + C2[9];
    Csum[10] = C1[10] + C2[10];
    Csum[11] = C1[11] + C2[11];
    Csum[12] = C1[12] + C2[12];
    Csum[13] = C1[13] + C2[13];
    Csum[14] = C1[14] + C2[14];

    // Factorization
    L[0] = Csum[0];
    success &= L[0] > cast(0.);
    // L[0] = sqrt(cast(1.) / L[0]);
    L[0] = sqrt(cast(1.) / select(success, L[0], cast(1.)));
  
    L[1]  = Csum[1]  * L[0];
    L[3]  = Csum[3]  * L[0];
    L[6]  = Csum[6]  * L[0];
    L[10] = Csum[10] * L[0];
  
    L[2] = Csum[2] - L[1]*L[1];
    success &= L[2] > cast(0.0);
    // L[2] = sqrt(1. / L[2]);
    L[2] = sqrt(cast(1.) / select(success, L[2], cast(1.)));
  
    L[4]  = (Csum[4]  - L[3] *L[1]) * L[2];
    L[7]  = (Csum[7]  - L[6] *L[1]) * L[2];
    L[11] = (Csum[11] - L[10]*L[1]) * L[2];
  
    L[5] = Csum[5] - L[3]*L[3] - L[4]*L[4];
    success &= L[5] > cast(0.);
    // L[5] = sqrt(1. / L[5]);
    L[5] = sqrt(cast(1.) / select(success, L[5], cast(1.)));
  
    L[8]  = (Csum[8]  - L[6] *L[3] - L[7] *L[4]) * L[5];
    L[12] = (Csum[12] - L[10]*L[3] - L[11]*L[4]) * L[5];
  
    L[9] = Csum[9] - L[6]*L[6] - L[7]*L[7] - L[8]*L[8];
    success &= L[9] > cast(0.);
    // L[9] = sqrt(1. / L[9]);
    L[9] = sqrt(cast(1.) / select(success, L[9], cast(1.)));
  
    L[13] = (Csum[13] - L[10]*L[6] - L[11]*L[7] - L[12]*L[8]) * L[9];
  
    L[14] = Csum[14] - L[10]*L[10] - L[11]*L[11] - L[12]*L[12] - L[13]*L[13];
    success &= L[14] > cast(0.);
    // L[14] = sqrt(1. / L[14]);
    L[14] = sqrt(cast(1.) / select(success, L[14], cast(1.)));

    // Inversion

    // Forward substitution
    inv[0]  = L[0];
    inv[1]  = (- L[1] *inv[0]) * L[2];
    inv[3]  = (- L[3] *inv[0] - L[4] *inv[1]) * L[5];
    inv[6]  = (- L[6] *inv[0] - L[7] *inv[1] - L[8] *inv[3]) * L[9];
    inv[10] = (- L[10]*inv[0] - L[11]*inv[1] - L[12]*inv[3] - L[13]*inv[6]) * L[14];

    inv[2]  = L[2];
    inv[4]  = (- L[4] *inv[2]) * L[5];
    inv[7]  = (- L[7] *inv[2] - L[8] *inv[4]) * L[9];
    inv[11] = (- L[11]*inv[2] - L[12]*inv[4] - L[13]*inv[7]) * L[14];
    inv[5]  = L[5];
    inv[8]  = (- L[8] *inv[5]) * L[9];
    inv[12] = (- L[12]*inv[5] - L[13]*inv[8]) * L[14];

    inv[9]  = L[9];
    inv[13] = (- L[13]*inv[9]) * L[14];

    inv[14] = L[14];

    // Backward substitution
    inv[10] = inv[10] * L[14];
    inv[6]  = (inv[6] - L[13]*inv[10]) * L[9];
    inv[3]  = (inv[3] - L[12]*inv[10]  - L[8]*inv[6]) * L[5];
    inv[1]  = (inv[1] - L[11]*inv[10]  - L[7]*inv[6]  - L[4]*inv[3]) * L[2];
    inv[0]  = (inv[0] - L[10]*inv[10]  - L[6]*inv[6]  - L[3]*inv[3]  - L[1]*inv[1]) * L[0];

    inv[11] = inv[11] * L[14];
    inv[7]  = (inv[7] - L[13]*inv[11]) * L[9];
    inv[4]  = (inv[4] - L[12]*inv[11]  - L[8]*inv[7]) * L[5];
    inv[2]  = (inv[2] - L[11]*inv[11]  - L[7]*inv[7]  - L[4]*inv[4]) * L[2];

    inv[12] = inv[12] * L[14];
    inv[8]  = (inv[8] - L[13]*inv[12]) * L[9];
    inv[5]  = (inv[5] - L[12]*inv[12]  - L[8]*inv[8]) * L[5];

    inv[13] = inv[13] * L[14];
    inv[9]  = (inv[9] - L[13]*inv[13]) * L[9];

    inv[14] = inv[14] * L[14];

    K[0]  = C1[0] *inv[0]  + C1[1] *inv[1]  + C1[3] *inv[3]  + C1[6] *inv[6]  + C1[10]*inv[10];
    K[1]  = C1[0] *inv[1]  + C1[1] *inv[2]  + C1[3] *inv[4]  + C1[6] *inv[7]  + C1[10]*inv[11];
    K[2]  = C1[0] *inv[3]  + C1[1] *inv[4]  + C1[3] *inv[5]  + C1[6] *inv[8]  + C1[10]*inv[12];
    K[3]  = C1[0] *inv[6]  + C1[1] *inv[7]  + C1[3] *inv[8]  + C1[6] *inv[9]  + C1[10]*inv[13];
    K[4]  = C1[0] *inv[10] + C1[1] *inv[11] + C1[3] *inv[12] + C1[6] *inv[13] + C1[10]*inv[14];
    K[5]  = C1[1] *inv[0]  + C1[2] *inv[1]  + C1[4] *inv[3]  + C1[7] *inv[6]  + C1[11]*inv[10];
    K[6]  = C1[1] *inv[1]  + C1[2] *inv[2]  + C1[4] *inv[4]  + C1[7] *inv[7]  + C1[11]*inv[11];
    K[7]  = C1[1] *inv[3]  + C1[2] *inv[4]  + C1[4] *inv[5]  + C1[7] *inv[8]  + C1[11]*inv[12];
    K[8]  = C1[1] *inv[6]  + C1[2] *inv[7]  + C1[4] *inv[8]  + C1[7] *inv[9]  + C1[11]*inv[13];
    K[9]  = C1[1] *inv[10] + C1[2] *inv[11] + C1[4] *inv[12] + C1[7] *inv[13] + C1[11]*inv[14];
    K[10] = C1[3] *inv[0]  + C1[4] *inv[1]  + C1[5] *inv[3]  + C1[8] *inv[6]  + C1[12]*inv[10];
    K[11] = C1[3] *inv[1]  + C1[4] *inv[2]  + C1[5] *inv[4]  + C1[8] *inv[7]  + C1[12]*inv[11];
    K[12] = C1[3] *inv[3]  + C1[4] *inv[4]  + C1[5] *inv[5]  + C1[8] *inv[8]  + C1[12]*inv[12];
    K[13] = C1[3] *inv[6]  + C1[4] *inv[7]  + C1[5] *inv[8]  + C1[8] *inv[9]  + C1[12]*inv[13];
    K[14] = C1[3] *inv[10] + C1[4] *inv[11] + C1[5] *inv[12] + C1[8] *inv[13] + C1[12]*inv[14];
    K[15] = C1[6] *inv[0]  + C1[7] *inv[1]  + C1[8] *inv[3]  + C1[9] *inv[6]  + C1[13]*inv[10];
    K[16] = C1[6] *inv[1]  + C1[7] *inv[2]  + C1[8] *inv[4]  + C1[9] *inv[7]  + C1[13]*inv[11];
    K[17] = C1[6] *inv[3]  + C1[7] *inv[4]  + C1[8] *inv[5]  + C1[9] *inv[8]  + C1[13]*inv[12];
    K[18] = C1[6] *inv[6]  + C1[7] *inv[7]  + C1[8] *inv[8]  + C1[9] *inv[9]  + C1[13]*inv[13];
    K[19] = C1[6] *inv[10] + C1[7] *inv[11] + C1[8] *inv[12] + C1[9] *inv[13] + C1[13]*inv[14];
    K[20] = C1[10]*inv[0]  + C1[11]*inv[1]  + C1[12]*inv[3]  + C1[13]*inv[6]  + C1[14]*inv[10];
    K[21] = C1[10]*inv[1]  + C1[11]*inv[2]  + C1[12]*inv[4]  + C1[13]*inv[7]  + C1[14]*inv[11];
    K[22] = C1[10]*inv[3]  + C1[11]*inv[4]  + C1[12]*inv[5]  + C1[13]*inv[8]  + C1[14]*inv[12];
    K[23] = C1[10]*inv[6]  + C1[11]*inv[7]  + C1[12]*inv[8]  + C1[13]*inv[9]  + C1[14]*inv[13];
    K[24] = C1[10]*inv[10] + C1[11]*inv[11] + C1[12]*inv[12] + C1[13]*inv[13] + C1[14]*inv[14];

    // X <- X1 + C1*inverse(C1+C2)*(X2-X1) =  X1 + K*(X2-X1) = X1 + K*d
    diff[0] = X2[0] - X1[0];
    diff[1] = X2[1] - X1[1];
    diff[2] = X2[2] - X1[2];
    diff[3] = X2[3] - X1[3];
    diff[4] = X2[4] - X1[4];

    X[0] = X1[0] + K[0] *diff[0] + K[1] *diff[1] + K[2] *diff[2] + K[3] *diff[3] + K[4] *diff[4];
    X[1] = X1[1] + K[5] *diff[0] + K[6] *diff[1] + K[7] *diff[2] + K[8] *diff[3] + K[9] *diff[4];
    X[2] = X1[2] + K[10]*diff[0] + K[11]*diff[1] + K[12]*diff[2] + K[13]*diff[3] + K[14]*diff[4];
    X[3] = X1[3] + K[15]*diff[0] + K[16]*diff[1] + K[17]*diff[2] + K[18]*diff[3] + K[19]*diff[4];
    X[4] = X1[4] + K[20]*diff[0] + K[21]*diff[1] + K[22]*diff[2] + K[23]*diff[3] + K[24]*diff[4];

    // C <-  C1 * inverse(C1+C2)  * C2 =  K * C2
    C[0]  = K[0] *C2[0]  + K[1] *C2[1]  + K[2] *C2[3]  + K[3] *C2[6]  + K[4] *C2[10];
    C[1]  = K[5] *C2[0]  + K[6] *C2[1]  + K[7] *C2[3]  + K[8] *C2[6]  + K[9] *C2[10];
    C[2]  = K[5] *C2[1]  + K[6] *C2[2]  + K[7] *C2[4]  + K[8] *C2[7]  + K[9] *C2[11];
    C[3]  = K[10]*C2[0]  + K[11]*C2[1]  + K[12]*C2[3]  + K[13]*C2[6]  + K[14]*C2[10];
    C[4]  = K[10]*C2[1]  + K[11]*C2[2]  + K[12]*C2[4]  + K[13]*C2[7]  + K[14]*C2[11];
    C[5]  = K[10]*C2[3]  + K[11]*C2[4]  + K[12]*C2[5]  + K[13]*C2[8]  + K[14]*C2[12];
    C[6]  = K[15]*C2[0]  + K[16]*C2[1]  + K[17]*C2[3]  + K[18]*C2[6]  + K[19]*C2[10];
    C[7]  = K[15]*C2[1]  + K[16]*C2[2]  + K[17]*C2[4]  + K[18]*C2[7]  + K[19]*C2[11];
    C[8]  = K[15]*C2[3]  + K[16]*C2[4]  + K[17]*C2[5]  + K[18]*C2[8]  + K[19]*C2[12];
    C[9]  = K[15]*C2[6]  + K[16]*C2[7]  + K[17]*C2[8]  + K[18]*C2[9]  + K[19]*C2[13];
    C[10] = K[20]*C2[0]  + K[21]*C2[1]  + K[22]*C2[3]  + K[23]*C2[6]  + K[24]*C2[10]; 
    C[11] = K[20]*C2[1]  + K[21]*C2[2]  + K[22]*C2[4]  + K[23]*C2[7]  + K[24]*C2[11]; 
    C[12] = K[20]*C2[3]  + K[21]*C2[4]  + K[22]*C2[5]  + K[23]*C2[8]  + K[24]*C2[12]; 
    C[13] = K[20]*C2[6]  + K[21]*C2[7]  + K[22]*C2[8]  + K[23]*C2[9]  + K[24]*C2[13]; 
    C[14] = K[20]*C2[10] + K[21]*C2[11] + K[22]*C2[12] + K[23]*C2[13] + K[24]*C2[14];

    // Store X
    X[0].store_a(X_p + 0*W);
    X[1].store_a(X_p + 1*W);
    X[2].store_a(X_p + 2*W);
    X[3].store_a(X_p + 3*W);
    X[4].store_a(X_p + 4*W);

    // Store C
    C[0].store_a(C_p + 0*W);
    C[1].store_a(C_p + 1*W);
    C[2].store_a(C_p + 2*W);
    C[3].store_a(C_p + 3*W);
    C[4].store_a(C_p + 4*W);
    C[5].store_a(C_p + 5*W);
    C[6].store_a(C_p + 6*W);
    C[7].store_a(C_p + 7*W);
    C[8].store_a(C_p + 8*W);
    C[9].store_a(C_p + 9*W);
    C[10].store_a(C_p + 10*W);
    C[11].store_a(C_p + 11*W);
    C[12].store_a(C_p + 12*W);
    C[13].store_a(C_p + 13*W);
    C[14].store_a(C_p + 14*W);

    return to_bits(success);
  }

  template<bool checkNodeType>
  static inline void updateResiduals (
    fp_ptr_64_const rv_p,
    fp_ptr_64_const pm_p,
    fp_ptr_64_const rr_p,
    fp_ptr_64_const em_p,
    fp_ptr_64_const ss_p,
    fp_ptr_64_const sc_p,
    fp_ptr_64 res_p,
    fp_ptr_64 errRes_p,
    const std::array<Sch::Item, W>& n
  ) {
    std::array<vectype, 5> rv;
    std::array<vectype, 5> pm;
    std::array<vectype, 5> ss;
    std::array<vectype, 15> sc;
    std::array<vectype, 5> v;
    vectype errRes, em, rr, error;

    // Load rv
    rv[0].load_a(rv_p + 0*W);
    rv[1].load_a(rv_p + 1*W);
    rv[2].load_a(rv_p + 2*W);
    rv[3].load_a(rv_p + 3*W);
    rv[4].load_a(rv_p + 4*W);

    // Load pm
    pm[0].load_a(pm_p + 0*W);
    pm[1].load_a(pm_p + 1*W);
    pm[2].load_a(pm_p + 2*W);
    pm[3].load_a(pm_p + 3*W);
    pm[4].load_a(pm_p + 4*W);

    // Load ss
    ss[0].load_a(ss_p + 0*W);
    ss[1].load_a(ss_p + 1*W);
    ss[2].load_a(ss_p + 2*W);
    ss[3].load_a(ss_p + 3*W);
    ss[4].load_a(ss_p + 4*W);

    // Load sc
    sc[0].load_a(sc_p + 0*W);
    sc[1].load_a(sc_p + 1*W);
    sc[2].load_a(sc_p + 2*W);
    sc[3].load_a(sc_p + 3*W);
    sc[4].load_a(sc_p + 4*W);
    sc[5].load_a(sc_p + 5*W);
    sc[6].load_a(sc_p + 6*W);
    sc[7].load_a(sc_p + 7*W);
    sc[8].load_a(sc_p + 8*W);
    sc[9].load_a(sc_p + 9*W);
    sc[10].load_a(sc_p + 10*W);
    sc[11].load_a(sc_p + 11*W);
    sc[12].load_a(sc_p + 12*W);
    sc[13].load_a(sc_p + 13*W);
    sc[14].load_a(sc_p + 14*W);

    // Load em
    em.load_a(em_p);

    // Load rr
    rr.load_a(rr_p);

    // HCH = pm[ 0]*_0 + pm[ 1]*_1 + pm[ 2]*_2 + pm[ 3]*_3 + pm[ 4]*_4;
    v[0] = sc[0] *pm[0]+sc[1] *pm[1]+sc[3] *pm[2]+sc[6] *pm[3]+sc[10]*pm[4];
    v[1] = sc[1] *pm[0]+sc[2] *pm[1]+sc[4] *pm[2]+sc[7] *pm[3]+sc[11]*pm[4];
    v[2] = sc[3] *pm[0]+sc[4] *pm[1]+sc[5] *pm[2]+sc[8] *pm[3]+sc[12]*pm[4];
    v[3] = sc[6] *pm[0]+sc[7] *pm[1]+sc[8] *pm[2]+sc[9] *pm[3]+sc[13]*pm[4];
    v[4] = sc[10]*pm[0]+sc[11]*pm[1]+sc[12]*pm[2]+sc[13]*pm[3]+sc[14]*pm[4];
    const vectype HCH = pm[0]*v[0] + pm[1]*v[1] + pm[2]*v[2] + pm[3]*v[3] + pm[4]*v[4];

    // const TrackVectorContiguous& refX = node.m_refVector.m_parameters;
    // value = node.m_refResidual + (pm * (rv - node.get<Op::Smooth, Op::StateVector>()));
    const vectype value = rr + (
        pm[0] * (rv[0] - ss[0]) +
        pm[1] * (rv[1] - ss[1]) +
        pm[2] * (rv[2] - ss[2]) +
        pm[3] * (rv[3] - ss[3]) +
        pm[4] * (rv[4] - ss[4])
    );

    // Bring back the changes
    value.store_a(res_p);

    // This should be optimized at compile time
    // With c++17: if constexpr (checkNodeType) { ... }
    if (checkNodeType) {
      // const TRACKVECTORFIT_PRECISION V = node.m_errMeasure * node.m_errMeasure;
      // const TRACKVECTORFIT_PRECISION sign = node.m_type == LHCb::Node::Type::HitOnTrack ? -1 : 1;
      const boolvectype maskedHitOnTrack = makeBoolHitOnTrack(n, std::make_index_sequence<W>());
      const vectype sign = select(maskedHitOnTrack, cast(-1.), cast(1.));

      // error = V + sign * HCH;
      error = em * em + sign * HCH;
    } else {
      // error = V - HCH;
      error = em * em - HCH;
    }

    errRes = sqrt(abs(error));
    errRes = select(error >= cast(0.), errRes, -errRes);
    errRes.store_a(errRes_p);
  }
};

template<class T>
struct Math {
  template<unsigned W>
  static inline void predictState (
    fp_ptr_64_const tm,
    fp_ptr_64_const tv,
    fp_ptr_64_const us,
    fp_ptr_64 ps
  );

  template<unsigned W>
  static inline void predictCovariance (
    fp_ptr_64_const tm,
    fp_ptr_64_const nm,
    fp_ptr_64_const uc,
    fp_ptr_64 pc
  );
};

template<>
struct Math<Op::Forward> {
  template<unsigned W>
  static inline void predictState (
    fp_ptr_64_const tm_p,
    fp_ptr_64_const tv_p,
    fp_ptr_64_const us_p,
    fp_ptr_64 ps_p
  ) {
    using vectype = typename Vectype<W>::type;

    std::array<vectype, 25> tm;
    std::array<vectype, 5> tv;
    std::array<vectype, 5> us;
    std::array<vectype, 5> ps;

    // Load tm
    tm[0].load_a(tm_p + 0*W);
    tm[1].load_a(tm_p + 1*W);
    tm[2].load_a(tm_p + 2*W);
    tm[3].load_a(tm_p + 3*W);
    tm[4].load_a(tm_p + 4*W);
    tm[5].load_a(tm_p + 5*W);
    tm[6].load_a(tm_p + 6*W);
    tm[7].load_a(tm_p + 7*W);
    tm[8].load_a(tm_p + 8*W);
    tm[9].load_a(tm_p + 9*W);
    tm[10].load_a(tm_p + 10*W);
    tm[11].load_a(tm_p + 11*W);
    tm[12].load_a(tm_p + 12*W);
    tm[13].load_a(tm_p + 13*W);
    tm[14].load_a(tm_p + 14*W);
    tm[15].load_a(tm_p + 15*W);
    tm[16].load_a(tm_p + 16*W);
    tm[17].load_a(tm_p + 17*W);
    tm[18].load_a(tm_p + 18*W);
    tm[19].load_a(tm_p + 19*W);
    tm[20].load_a(tm_p + 20*W);
    tm[21].load_a(tm_p + 21*W);
    tm[22].load_a(tm_p + 22*W);
    tm[23].load_a(tm_p + 23*W);
    tm[24].load_a(tm_p + 24*W);

    // Load tv
    tv[0].load_a(tv_p + 0*W);
    tv[1].load_a(tv_p + 1*W);
    tv[2].load_a(tv_p + 2*W);
    tv[3].load_a(tv_p + 3*W);
    tv[4].load_a(tv_p + 4*W);

    // Load us
    us[0].load_a(us_p + 0*W);
    us[1].load_a(us_p + 1*W);
    us[2].load_a(us_p + 2*W);
    us[3].load_a(us_p + 3*W);
    us[4].load_a(us_p + 4*W);

    ps[0] = tv[0] + tm[0]  * us[0] + tm[1]  * us[1] + tm[2]  * us[2] + tm[3]  * us[3] + tm[4]  * us[4];
    ps[1] = tv[1] + tm[5]  * us[0] + tm[6]  * us[1] + tm[7]  * us[2] + tm[8]  * us[3] + tm[9]  * us[4];
    ps[2] = tv[2] + tm[10] * us[0] + tm[11] * us[1] + tm[12] * us[2] + tm[13] * us[3] + tm[14] * us[4];
    ps[3] = tv[3] + tm[15] * us[0] + tm[16] * us[1] + tm[17] * us[2] + tm[18] * us[3] + tm[19] * us[4];
    ps[4] = tv[4] + tm[20] * us[0] + tm[21] * us[1] + tm[22] * us[2] + tm[23] * us[3] + tm[24] * us[4];

    // Store ps
    ps[0].store_a(ps_p + 0*W);
    ps[1].store_a(ps_p + 1*W);
    ps[2].store_a(ps_p + 2*W);
    ps[3].store_a(ps_p + 3*W);
    ps[4].store_a(ps_p + 4*W);
  }

  template<unsigned W>
  static inline void predictCovariance (
    fp_ptr_64_const tm,
    fp_ptr_64_const nm,
    fp_ptr_64_const uc,
    fp_ptr_64 pc
  ) {
    // Delegate to similarity_5_5 implementation
    MathCommon<W>::similarity_5_5(tm, uc, pc);

    using vectype = typename Vectype<W>::type;
    (vectype().load_a(pc +  0*W) + vectype().load_a(nm +  0*W)).store_a(pc +  0*W);
    (vectype().load_a(pc +  1*W) + vectype().load_a(nm +  1*W)).store_a(pc +  1*W);
    (vectype().load_a(pc +  2*W) + vectype().load_a(nm +  2*W)).store_a(pc +  2*W);
    (vectype().load_a(pc +  3*W) + vectype().load_a(nm +  3*W)).store_a(pc +  3*W);
    (vectype().load_a(pc +  4*W) + vectype().load_a(nm +  4*W)).store_a(pc +  4*W);
    (vectype().load_a(pc +  5*W) + vectype().load_a(nm +  5*W)).store_a(pc +  5*W);
    (vectype().load_a(pc +  6*W) + vectype().load_a(nm +  6*W)).store_a(pc +  6*W);
    (vectype().load_a(pc +  7*W) + vectype().load_a(nm +  7*W)).store_a(pc +  7*W);
    (vectype().load_a(pc +  8*W) + vectype().load_a(nm +  8*W)).store_a(pc +  8*W);
    (vectype().load_a(pc +  9*W) + vectype().load_a(nm +  9*W)).store_a(pc +  9*W);
    (vectype().load_a(pc + 10*W) + vectype().load_a(nm + 10*W)).store_a(pc + 10*W);
    (vectype().load_a(pc + 11*W) + vectype().load_a(nm + 11*W)).store_a(pc + 11*W);
    (vectype().load_a(pc + 12*W) + vectype().load_a(nm + 12*W)).store_a(pc + 12*W);
    (vectype().load_a(pc + 13*W) + vectype().load_a(nm + 13*W)).store_a(pc + 13*W);
    (vectype().load_a(pc + 14*W) + vectype().load_a(nm + 14*W)).store_a(pc + 14*W);
  }
};

template<>
struct Math<Op::Backward> {
  template<unsigned W>
  static inline void predictState (
    fp_ptr_64_const tm_p,
    fp_ptr_64_const tv_p,
    fp_ptr_64_const us_p,
    fp_ptr_64 ps_p
  ) {
    using vectype = typename Vectype<W>::type;

    std::array<vectype, 25> tm;
    std::array<vectype, 5> tv;
    std::array<vectype, 5> us;
    std::array<vectype, 5> ps;
    std::array<vectype, 5> s;

    // Load tm
    tm[0].load_a(tm_p + 0*W);
    tm[1].load_a(tm_p + 1*W);
    tm[2].load_a(tm_p + 2*W);
    tm[3].load_a(tm_p + 3*W);
    tm[4].load_a(tm_p + 4*W);
    tm[5].load_a(tm_p + 5*W);
    tm[6].load_a(tm_p + 6*W);
    tm[7].load_a(tm_p + 7*W);
    tm[8].load_a(tm_p + 8*W);
    tm[9].load_a(tm_p + 9*W);
    tm[10].load_a(tm_p + 10*W);
    tm[11].load_a(tm_p + 11*W);
    tm[12].load_a(tm_p + 12*W);
    tm[13].load_a(tm_p + 13*W);
    tm[14].load_a(tm_p + 14*W);
    tm[15].load_a(tm_p + 15*W);
    tm[16].load_a(tm_p + 16*W);
    tm[17].load_a(tm_p + 17*W);
    tm[18].load_a(tm_p + 18*W);
    tm[19].load_a(tm_p + 19*W);
    tm[20].load_a(tm_p + 20*W);
    tm[21].load_a(tm_p + 21*W);
    tm[22].load_a(tm_p + 22*W);
    tm[23].load_a(tm_p + 23*W);
    tm[24].load_a(tm_p + 24*W);

    // Load tv
    tv[0].load_a(tv_p + 0*W);
    tv[1].load_a(tv_p + 1*W);
    tv[2].load_a(tv_p + 2*W);
    tv[3].load_a(tv_p + 3*W);
    tv[4].load_a(tv_p + 4*W);

    // Load us
    us[0].load_a(us_p + 0*W);
    us[1].load_a(us_p + 1*W);
    us[2].load_a(us_p + 2*W);
    us[3].load_a(us_p + 3*W);
    us[4].load_a(us_p + 4*W);

    s[0] = us[0] - tv[0];
    s[1] = us[1] - tv[1];
    s[2] = us[2] - tv[2];
    s[3] = us[3] - tv[3];
    s[4] = us[4] - tv[4];

    ps[0] = tm[0]  * s[0] + tm[1]  * s[1] + tm[2]  * s[2] + tm[3]  * s[3] + tm[4]  * s[4];
    ps[1] = tm[5]  * s[0] + tm[6]  * s[1] + tm[7]  * s[2] + tm[8]  * s[3] + tm[9]  * s[4];
    ps[2] = tm[10] * s[0] + tm[11] * s[1] + tm[12] * s[2] + tm[13] * s[3] + tm[14] * s[4];
    ps[3] = tm[15] * s[0] + tm[16] * s[1] + tm[17] * s[2] + tm[18] * s[3] + tm[19] * s[4];
    ps[4] = tm[20] * s[0] + tm[21] * s[1] + tm[22] * s[2] + tm[23] * s[3] + tm[24] * s[4];

    // Store ps
    ps[0].store_a(ps_p + 0*W);
    ps[1].store_a(ps_p + 1*W);
    ps[2].store_a(ps_p + 2*W);
    ps[3].store_a(ps_p + 3*W);
    ps[4].store_a(ps_p + 4*W);
  }

  template<unsigned W>
  static inline void predictCovariance (
    fp_ptr_64_const tm,
    fp_ptr_64_const nm,
    fp_ptr_64_const uc,
    fp_ptr_64 pc
  ) {
    using vectype = typename Vectype<W>::type;

    _aligned std::array<TRACKVECTORFIT_PRECISION, 15*W> temp;
    (vectype().load_a(nm +  0*W) + vectype().load_a(uc +  0*W)).store_a(temp.data() +  0*W);
    (vectype().load_a(nm +  1*W) + vectype().load_a(uc +  1*W)).store_a(temp.data() +  1*W);
    (vectype().load_a(nm +  2*W) + vectype().load_a(uc +  2*W)).store_a(temp.data() +  2*W);
    (vectype().load_a(nm +  3*W) + vectype().load_a(uc +  3*W)).store_a(temp.data() +  3*W);
    (vectype().load_a(nm +  4*W) + vectype().load_a(uc +  4*W)).store_a(temp.data() +  4*W);
    (vectype().load_a(nm +  5*W) + vectype().load_a(uc +  5*W)).store_a(temp.data() +  5*W);
    (vectype().load_a(nm +  6*W) + vectype().load_a(uc +  6*W)).store_a(temp.data() +  6*W);
    (vectype().load_a(nm +  7*W) + vectype().load_a(uc +  7*W)).store_a(temp.data() +  7*W);
    (vectype().load_a(nm +  8*W) + vectype().load_a(uc +  8*W)).store_a(temp.data() +  8*W);
    (vectype().load_a(nm +  9*W) + vectype().load_a(uc +  9*W)).store_a(temp.data() +  9*W);
    (vectype().load_a(nm + 10*W) + vectype().load_a(uc + 10*W)).store_a(temp.data() + 10*W);
    (vectype().load_a(nm + 11*W) + vectype().load_a(uc + 11*W)).store_a(temp.data() + 11*W);
    (vectype().load_a(nm + 12*W) + vectype().load_a(uc + 12*W)).store_a(temp.data() + 12*W);
    (vectype().load_a(nm + 13*W) + vectype().load_a(uc + 13*W)).store_a(temp.data() + 13*W);
    (vectype().load_a(nm + 14*W) + vectype().load_a(uc + 14*W)).store_a(temp.data() + 14*W);

    // Delegate to similarity_5_5 implementation
    MathCommon<W>::similarity_5_5(tm, temp.data(), pc);
  }
};

}

}

}
