/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/VectorConfiguration.h"

namespace Tr {
namespace TrackVectorFit {
namespace Vector {

  template<std::size_t W>
  struct TrajPoca {
    using ftype = typename Vectype<W>::type;
    using ftype_bool = typename Vectype<W>::booltype;
    using itype = typename LHCb::Vector::Vectype<long int, W>::type;
    using itype_bool = typename LHCb::Vector::Vectype<long int, W>::booltype;

    using Point  = typename ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<ftype>,
                                                         ROOT::Math::DefaultCoordinateSystemTag>; ///< 3D cartesian point
    using Vector = typename ROOT::Math::DisplacementVector3D<ROOT::Math::Cartesian3D<ftype>,
                                                             ROOT::Math::DefaultCoordinateSystemTag>; ///< Cartesian 3D vector

    struct cache_t {
      Point p1, p2;
      Vector dp1dmu1, dp2dmu2;
      Vector d2p1dmu12, d2p2dmu22;
    };

    enum step_status_t { nan = -1, ok = 0, parallel, nearly_parallel, beyond_maxdist };

    static constexpr int m_maxnOscillStep = 5;
    static constexpr int m_maxnDivergingStep = 5;
    static constexpr int m_maxnStuck = 3;
    static constexpr int m_maxnTry = 100;
    static constexpr int m_maxDist = 100000000;

    template<typename TRAJ>
    static
    ftype_bool restrictToRange(ftype& l, const TRAJ& t)
    {
      ftype beg = t.beginRange();
      ftype end = t.endRange();
      // FIXME C++17: use std::clamp instead
      auto clamp = [](const auto& v, const auto& lo, const auto& hi ) -> decltype(auto)
        { return select(v < lo, lo, select(hi < v, hi, v) ); };

      const auto oldl = std::exchange( l, clamp( l, min(beg, end), max(beg, end)) );
      return oldl != l;
    }

    template<typename TMINMAX>
    static
    bool restrictToRangeScalar(double& l, TMINMAX&& minmax)
    {
      //const auto minmax = std::minmax( { t.beginRange(), t.endRange() } ) ;
      //C++17: use std::clamp instead...
      auto clamp = [](const auto& v, const auto& lo, const auto& hi ) -> decltype(auto)
        { return v<lo ? lo : hi<v ? hi : v; };
      const auto oldl = std::exchange( l, clamp( l, minmax.first, minmax.second ) );
      return oldl != l;
    }

    static inline
    auto stepTowardPoca(const LHCb::StateZTraj<ftype>& traj1, ftype& mu1, const bool restrictRange1,
                        const LHCb::LineTraj<ftype>& traj2, ftype& mu2, const bool restrictRange2,
                        const ftype tolerance,
                        cache_t& cache,
                        const double maxExtrapTolerance,
                        const double maxDist,
                        ftype_bool& finished ) {
      // for adl purpose between std and vcl
      using std::abs;
      using std::sqrt;
      using std::max;
      using std::min;

      auto ignored = finished;

      // a bunch of ugly, unitialized member variables
      traj1.expansion(mu1, cache.p1, cache.dp1dmu1, cache.d2p1dmu12);
      traj2.expansion(mu2, cache.p2, cache.dp2dmu2, cache.d2p2dmu22);
      const Vector d(cache.p1 - cache.p2);
      // if the distance between points is below 1e-4 mm, we consider the
      // minimisation to be converged
      auto dmag = d.mag2();
      auto sqthreshold = 1e-4 * Gaudi::Units::mm;
      auto threshold = sqthreshold * sqthreshold;

      ignored = ignored or (dmag < threshold); // update ignored "ok" slots
      if(horizontal_and(ignored)) return ;

      const std::array<ftype, 2> rhs = { -d.Dot(cache.dp1dmu1), d.Dot(cache.dp2dmu2) };

      std::array<ftype, 3> mat = { // keep all terms up to order mu1, mu2, mu1 * mu2
        cache.dp1dmu1.mag2() + d.Dot(cache.d2p1dmu12), -cache.dp2dmu2.Dot(cache.dp1dmu1),
        cache.dp2dmu2.mag2() - d.Dot(cache.d2p2dmu22),
      };

      std::array<ftype, 2> dmu = rhs;

      // cholesky 2x2 vectorized
      auto cholesky = [](const auto& mat, ftype& l10,
                         ftype& l00_inv, ftype& l11_inv)
        {
          ftype l00_2   = mat[0];
                l00_inv = 1./ sqrt(l00_2); // FIXME use fast rsqrt
                l10     = mat[1] * l00_inv;
          ftype l11_2   = mat[2] - l10 * l10;
                l11_inv = 1./ sqrt(l11_2); // FIXME use fast rsqrt

          return l00_2 > 0 && l11_2 > 0; // is decomposable
        };

      ftype l10, l00_inv, l11_inv;

      auto decomposable = cholesky(mat, l10, l00_inv, l11_inv);

      if(not horizontal_or(decomposable)) {
        // singular, or not pos. def; try again neglecting curvature
        mat[0] = select(decomposable, mat[0], cache.dp1dmu1.mag2());
        mat[1] = select(decomposable, mat[1], cache.dp2dmu2.mag2());

        decomposable = cholesky(mat, l10, l00_inv, l11_inv);
      }

      // mask to check either trajectories are (nearly) parallel or not
      auto traj_nearparallel = l00_inv * l11_inv > 1e8; // check product of eigenvalues of mat
      auto traj_parallel = not( decomposable );

      finished = finished or traj_nearparallel or traj_parallel;
      ignored = ignored or traj_nearparallel or traj_parallel;

      auto y0 = rhs[0] * l00_inv;
      auto y1 = ( rhs[1] - l10 * y0 ) * l11_inv;

      auto x1 = y1 * l11_inv;
      auto x0 = ( y0 - l10 * x1 ) * l00_inv;

      dmu[0] = x0;
      dmu[1] = x1;

      ftype pathDir1 = select(dmu[0] > 0, 1, -1);
      ftype pathDir2 = select(dmu[1] > 0, 1, -1);

      // Don't try going further than worst parabolic approximation will
      // allow. The tolerance is set by 'deltadoca', the expected
      // improvement in the doca. We bound this by tolerance from below
      // and maxExtrapTolerance from above.
      auto deltadoca = sqrt(abs(rhs[0] * dmu[0]) + abs(rhs[1] * dmu[1])); // std::abs only because of machine precision issues.
      auto extraptolerance = min(max(deltadoca, tolerance), maxExtrapTolerance);
      static constexpr double smudge = 1.01; // Factor to push just over border of piecewise traj (essential!)
      auto distToErr1 = smudge * traj1.distTo2ndError(mu1, extraptolerance);
      auto distToErr2 = smudge * traj2.distTo2ndError(mu2, extraptolerance);

      // Factor to push just over border of piecewise traj (essential!)
      {
        auto push_over_border = 0 < distToErr1 && distToErr1 < abs(dmu[0]);
        // choose solution for which dmu[0] steps just over border
        dmu[0] = select(push_over_border, distToErr1 * pathDir1, dmu[0]);
        // now recalculate dmu[1], given dmu[0]:
        dmu[1] = select(push_over_border, (rhs[1] - dmu[0] *  mat[1]) / mat[2], dmu[1]);
      }

      {
        auto push_over_border = 0 < distToErr2 && distToErr2 < abs(dmu[0]);
        // choose solution for which dmu[1] steps just over border
        dmu[1] = select(push_over_border, distToErr2 * pathDir2, dmu[1]);
        // now recalculate dmu[0], given dmu[1]:
        dmu[0] = select(push_over_border, (rhs[0] - dmu[1] * mat[1]) / mat[0], dmu[0]);
        // if still not okay,
        push_over_border = push_over_border && (0 < distToErr2 && distToErr2 < abs(dmu[0]));
        dmu[0] = select(push_over_border, distToErr1 * pathDir1, dmu[0]);
      }

      auto have_to_update_mu = andnot(not finished, ignored);

      mu1 = if_add(have_to_update_mu, mu1, dmu[0]);
      mu2 = if_add(have_to_update_mu, mu2, dmu[1]);

      // FIXME : these do not make any sense here. either we need to merge them with the lines above that restrict to the validity of the
      // expansion, or we need to move them out of here entirely.
      auto ll1 = mu1;
      auto ll2 = mu2;
      if (UNLIKELY(bool(restrictRange1)))
        restrictToRange(ll1, traj1);
      if (UNLIKELY(bool(restrictRange2)))
        restrictToRange(ll2, traj2);
      mu1 = select(have_to_update_mu, ll1, mu1);
      mu2 = select(have_to_update_mu, ll2, mu2);

      // another check for parallel trajectories
      auto beyondmax = min(abs(mu1), abs(mu2)) > maxDist;

      finished = finished or beyondmax;
    }

    static inline
    void minimize(const LHCb::Trajectory<ftype>& traj, ftype& mu, bool restrictRange,
                  Point&& pt,
                  Vector& distance )
    {
      // this does not work for non-linear Trajectories!
      mu = traj.muEstimate( pt ) ;
      if(restrictRange) restrictToRange( mu, traj ) ;
      distance = traj.position( mu ) - pt;
    }

    static inline
    void minimize(const LHCb::StateZTraj<ftype>& traj1, ftype& mu1, const bool restrictRange1,
                  const LHCb::LineTraj<ftype>& traj2, ftype& mu2, const bool restrictRange2,
                  Vector& distance, const ftype precision) {

      struct resultf {
        ftype mu1 = 0;
        ftype mu2 = 0;
        Vector distance;
        ftype_bool prevm = false;

        resultf(ftype mu1, ftype mu2, const Vector& distance) {
          this->mu1 = mu1;
          this->mu2 = mu2;
          this->distance = distance;
        }

        void updateDistanceIf(ftype_bool mask, const Vector& nvec) {
          this->distance.SetX( select(mask, nvec.X(), this->distance.X()) );
          this->distance.SetY( select(mask, nvec.Y(), this->distance.Y()) );
          this->distance.SetZ( select(mask, nvec.Z(), this->distance.Z()) );
        }

        // update slot i for each mask[i] == 1
        void update(ftype mu1, ftype mu2, const Vector& distance,
                    ftype_bool currentm) {
          ftype_bool mask = prevm ^ currentm; // get only those changed between previously and currently
          if(horizontal_or(mask) == false) return;

          this->mu1 = select(mask, mu1, this->mu1);
          this->mu2 = select(mask, mu2, this->mu2);

          updateDistanceIf(mask, distance);
          prevm = mask;
        }
      };

      resultf res{mu1, mu2, distance};

      ftype delta2(0.), prevdelta2(0.);
      itype nOscillStep(0);
      itype nDivergingStep(0);
      itype nStuck(0);
      ftype_bool finished = false;
      cache_t workspace;

      for (int istep = 0; LIKELY(horizontal_or(istep < m_maxnTry && !finished)); ++istep) {
        ftype prevflt1      = mu1;
        ftype prevflt2      = mu2;
        ftype prevprevdelta2 = std::exchange(prevdelta2, delta2);

        stepTowardPoca( traj1, mu1, restrictRange1,
                        traj2, mu2, restrictRange2,
                        precision, workspace,
                        1*Gaudi::Units::cm, 100000000, finished);

        res.update(mu1, mu2, distance, finished);

        if(horizontal_and(finished)) {
          mu1 = res.mu1;
          mu2 = res.mu2;
          distance = res.distance;
          break;
        }

        auto tmpdistance = traj1.position( mu1 ) - traj2.position( mu2 );

        delta2   = tmpdistance.Mag2();
        ftype step1 = mu1 - prevflt1;
        ftype step2 = mu2 - prevflt2;

        // Can we stop stepping?
        ftype distToErr1 = traj1.distTo1stError( prevflt1, precision );
        ftype distToErr2 = traj2.distTo1stError( prevflt2, precision );
        // converged if very small steps
        finished = finished || ( abs(step1) < distToErr1 && abs(step2) < distToErr2 );

        // we have to catch some problematic cases
        const itype_bool ostep = itype_bool(!finished && istep > 2 && delta2 > prevdelta2);
        if ( horizontal_or(ostep) ) {
          // we can get stuck if a flt range is restricted
          const itype_bool rrange = ostep || itype_bool((restrictRange1 && abs(step1) > 1.0e-10))
                                          || itype_bool((restrictRange2 && abs(step2) > 1e-10));

          itype_bool pdelta = ostep || itype_bool(prevdelta2 > prevprevdelta2);

          // -----
          if ( horizontal_or(rrange) ) {
            nStuck = if_add(rrange, nStuck, 1);
            const auto nstuck = rrange || (nStuck > m_maxnStuck);

            if ( horizontal_or(nstuck) ) {
              // downgrade to a point poca
              Vector dist(0., 0., 0.);
              restrictRange2 ?
                minimize(traj1, mu1, restrictRange1, traj2.position(mu2), dist) :
                minimize(traj2, mu2, restrictRange2, traj1.position(mu1), dist);

              // Minimization got stuck
              finished = finished || ftype_bool(nstuck);
            }
          // -----
          }
          // else if
          pdelta = andnot(pdelta, rrange);

          if ( horizontal_or(pdelta) ) {
            nDivergingStep = if_add(pdelta, nDivergingStep, 1);
            finished = finished || static_cast<decltype(finished)>(nDivergingStep > m_maxnDivergingStep);
          // -----
          }
          auto exec_else = andnot(not pdelta, rrange);
          if ( horizontal_or(exec_else) ) {
            nDivergingStep = select(exec_else, 0, nDivergingStep);

            // oscillating
            nOscillStep = if_add(exec_else, nOscillStep, 1);
            decltype(exec_else) oscillstep_over = nOscillStep > m_maxnOscillStep;

            // bail out of oscillation. since the previous step was
            // better, use that one.
            auto bailout_oscill = exec_else and oscillstep_over;

            mu1 = select(bailout_oscill, prevflt1, mu1);
            mu2 = select(bailout_oscill, prevflt2, mu2);

            // Minimization bailed out of oscillation
            finished = finished or ftype_bool(bailout_oscill);

            // we might be oscillating, but we could also just have
            // stepped over the minimum. choose a solution `in
            // between'.
            auto might_oscill = andnot(exec_else, oscillstep_over);

            mu1 = select(might_oscill, prevflt1 + 0.5 * step1, mu1);
            mu2 = select(might_oscill, prevflt2 + 0.5 * step2, mu2);
            auto ll1 = mu1;
            auto ll2 = mu2;
            if (UNLIKELY(bool(restrictRange1)))
              restrictToRange(ll1, traj1);
            if (UNLIKELY(bool(restrictRange2)))
              restrictToRange(ll2, traj2);
            mu1 = select(might_oscill, ll1, mu1);
            mu2 = select(might_oscill, ll2, mu2);

            tmpdistance = traj1.position( mu1 ) - traj2.position( mu2 );
            auto mg = tmpdistance.Mag2();
            delta2  = select(might_oscill, mg, delta2);
          }
        }

        // update mu1 mu2 distance
        res.update(mu1, mu2, tmpdistance, finished);
      }

      mu1 = res.mu1;
      mu2 = res.mu2;
      distance = res.distance;
    }
  };
}
}
}
