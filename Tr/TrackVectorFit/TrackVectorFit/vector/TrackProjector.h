/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

#include "Kernel/Trajectory.h"

// LHCb Kernel
#include "Kernel/LineTraj.h"

// Track Kernel
#include "TrackKernel/StateZTraj.h"

#include "TrackVectorFit/TrackVectorFit.h"
#include "TrackVectorFit/vector/TrajPoca.h"

namespace Tr {
namespace TrackVectorFit {
namespace Vector {
  template<typename U>
  void printv(U sMeas) {
    for(int i = 0; i < sMeas.size()-1; ++i) { std::cout << sMeas[i] << " ; "; }
    std::cout << sMeas[sMeas.size()-1] << std::endl;
  }
  template<std::size_t W>
  struct TrackProjector {
    using ftype = typename Vectype<W>::type;
    using boolvectype = typename Vectype<W>::booltype;

    using Point  = typename ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<ftype>,
                                                         ROOT::Math::DefaultCoordinateSystemTag>; ///< 3D cartesian point
    using Vector = typename ROOT::Math::DisplacementVector3D<ROOT::Math::Cartesian3D<ftype>,
                                                             ROOT::Math::DefaultCoordinateSystemTag>; ///< Cartesian 3D vector

    template <size_t... Is>
    static constexpr inline ftype getZ (
      const std::array<Sch::Item, W>& nodes,
      std::index_sequence<Is...>
      ) {
      return ftype{(nodes[Is].node)->node().z()...};
    }

    static constexpr inline
    Point getPosition(const std::array<Sch::Item, W>& nodes,
                      const ftype& zvec,
                      fp_ptr_64_const rv_p,
                      const Vector& bfield) {
    }

    template <size_t... Is>
    static constexpr inline
    ftype getBegins(const std::array<Sch::Item, W>& nodes,
                    std::index_sequence<Is...>
      ) {
      return ftype{(nodes[Is].node)->node().measurement().trajectory().beginRange()...};
    }

    template <size_t... Is>
    static constexpr inline
    ftype getEnds(const std::array<Sch::Item, W>& nodes,
                  std::index_sequence<Is...>
      ) {
      return ftype{(nodes[Is].node)->node().measurement().trajectory().endRange()...};
    }

    template <size_t... Is>
    static constexpr inline
    Vector getDir(const std::array<Sch::Item, W>& nodes,
                  std::index_sequence<Is...>
      ) {
      return { ftype{(nodes[Is].node)->node().measurement().trajectory().direction(0).x()...},
               ftype{(nodes[Is].node)->node().measurement().trajectory().direction(0).y()...},
               ftype{(nodes[Is].node)->node().measurement().trajectory().direction(0).z()...} };
    }

    template <size_t... Is>
    static constexpr inline
    ftype getTol(const std::array<Sch::Item, W>& nodes,
                  std::index_sequence<Is...>
      ) {
      constexpr double vptol = 0.0005*Gaudi::Units::mm;
      constexpr double deftol = 0.002*Gaudi::Units::mm;
      return ftype{ (nodes[Is].node)->node().measurement().type() == LHCb::Measurement::Type::VP ? vptol : deftol ... };
    }

    template <size_t... Is>
    static constexpr inline
    Point getPos(const std::array<Sch::Item, W>& nodes,
                 std::index_sequence<Is...>
      ) {
      return { ftype{(nodes[Is].node)->node().measurement().trajectory().position(0).x()...},
               ftype{(nodes[Is].node)->node().measurement().trajectory().position(0).y()...},
               ftype{(nodes[Is].node)->node().measurement().trajectory().position(0).z()...} };
    }

    template <typename TRAJTYPE, size_t... Is>
    static constexpr inline
    ftype getErrMeasure(const std::array<Sch::Item, W>& nodes,
                        TRAJTYPE&& refTraj,
                        ftype sState,
                        std::index_sequence<Is...>
      ) {
      decltype(auto) pos = refTraj.position(sState);
      decltype(auto) dir = refTraj.direction(sState);

      return ftype{(nodes[Is].node)->node().measurement().resolution2(Gaudi::XYZPoint(pos.x()[Is], pos.y()[Is], pos.z()[Is]),
                                                                      Gaudi::XYZVector(dir.x()[Is], dir.y()[Is], dir.z()[Is]))...};
    }

    static inline
    Gaudi::XYZVector getPoca(Vector& v,
                             int k) {
      return { v.x()[k], v.y()[k], v.z()[k] };
    }

    static inline
    void projectReference(std::array<Sch::Item, vector_width()>& nodes,
                          const LHCb::MagneticFieldGrid* magneticFieldGrid,
                          const bool useBField,
                          fp_ptr_64_const rv_p) {
      ftype tolerance = getTol(nodes, std::make_index_sequence<W>());
      // -- Project onto the reference. First create the StateTraj with or without BField information.
      //
      // Gaudi::XYZVector bfield(0,0,0) ;
      // if( m_useBField) m_pIMF -> fieldVector( statevector.position(), bfield ).ignore();
      // const LHCb::StateZTraj refTraj( statevector, bfield );

      ftype sState = getZ(nodes, std::make_index_sequence<W>());

      std::array<ftype, 5> stateVector;
      stateVector[0].load_a(rv_p + 0*W); // x
      stateVector[1].load_a(rv_p + 1*W); // y
      stateVector[2].load_a(rv_p + 2*W); // tx
      stateVector[3].load_a(rv_p + 3*W); // ty
      stateVector[4].load_a(rv_p + 4*W); // q/p

      const std::array<ftype, 3> spos { stateVector[0], stateVector[1], sState };
      std::array<ftype, 3> B { 0, 0, 0 };

      if(useBField)
        B = magneticFieldGrid->horizontallyVectorizedFieldVector<ftype, W>(spos);

      Vector bfield { B[0], B[1], B[2] };

      LHCb::StateZTraj<ftype> refTraj { stateVector[0], stateVector[1],
                                        stateVector[2], stateVector[3],
                                        stateVector[4], sState,
                                        bfield };

      // Point position = refTraj.position(sState); // --> Point == single scalar
      // double sMeas = measTraj.muEstimate(position);
      Point position = refTraj.position(sState);

      // get range begin
      ftype begins = getBegins(nodes, std::make_index_sequence<W>());
      // get range end
      ftype ends = getEnds(nodes, std::make_index_sequence<W>());

      Vector dir = getDir(nodes, std::make_index_sequence<W>());
      Point pos  = getPos(nodes, std::make_index_sequence<W>());

      LHCb::LineTraj<ftype> measTraj { pos, dir,
                                       { begins, ends },
                                       typename LHCb::Trajectory<ftype>::DirNormalized{false} };
      ftype sMeas = measTraj.muEstimate(position);

      // -- Determine the actual minimum with the Poca tool
      // Gaudi::XYZVector dist;
      // StatusCode sc = m_poca -> minimize(refTraj, sState, // restrictRange1,
      //                                    measTraj, sMeas, // restrictRange2,
      //                                    dist, m_tolerance);
      Vector dist{0., 0., 0.};

      TrajPoca<W>::minimize(refTraj, sState, false,
                            measTraj, sMeas, false,
                            dist, tolerance);

      // -- Set up the vector onto which we project everything. This should
      // -- actually be parallel to dist.
      // Gaudi::XYZVector unitPocaVector = (measTraj.direction(sMeas).Cross(refTraj.direction(sState))).Unit();
      Vector unitPocaVector = (measTraj.direction(sMeas).Cross(refTraj.direction(sState)));

      // hardcoded Unit() since not compatible with VCL types
      auto tot = unitPocaVector.R();
      tot = select(tot == 0, 1, tot);
      unitPocaVector *= (1./tot);

      // double doca = unitPocaVector.Dot(dist);
      ftype doca = unitPocaVector.Dot(dist);

      // -- compute the projection matrix from parameter space onto the (signed!) unit
      // Gaudi::TrackProjectionMatrix H = dual(unitPocaVector) * refTraj.derivative(sState);
      // -> ROOT::Math::SMatrix<double, 1, 5>
      ROOT::Math::SMatrix<ftype, 1, 3> upoca;
      unitPocaVector.GetCoordinates(upoca.Array());

      auto derivat= refTraj.derivative(sState);

      const ROOT::Math::SMatrix<ftype, 1, 5> hmatrix = upoca * derivat;
      auto& n0 = *(nodes[0].node);
      auto pm_ptr = n0.get<typename Tr::TrackVectorFit::Op::NodeParameters,
                           typename Tr::TrackVectorFit::Op::ProjectionMatrix>().m_basePointer;

      hmatrix(0,0).store_a(pm_ptr);
      hmatrix(0,1).store_a(pm_ptr + 1*W);
      hmatrix(0,2).store_a(pm_ptr + 2*W);
      hmatrix(0,3).store_a(pm_ptr + 3*W);
      hmatrix(0,4).store_a(pm_ptr + 4*W);

      // -- Set the error on the measurement so that it can be used in the fit
      // double errMeasure2 = meas.resolution2(refTraj.position(sState),
      //                                       refTraj.direction(sState));
      ftype errMeasure2 = getErrMeasure(nodes, refTraj, sState, std::make_index_sequence<W>());
      ftype errMeasure = sqrt(errMeasure2);
      auto refResidual = -doca;

      for (uint i = 0; i < Tr::TrackVectorFit::vector_width(); ++i) {
        auto& n = *(nodes[i].node);

        n.setErrMeasure( errMeasure[i] );
        n.setRefResidual( refResidual[i] );
        n.node().setPocaVector( getPoca(unitPocaVector, i) );
        n.node().setDoca( doca[i] );
      }
    }
};
}
}
}
