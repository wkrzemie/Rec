/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <array>
#include "../Types.h"
#include "Event/TrackParameters.h"

#include "GaudiKernel/PhysicalConstants.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "DetDesc/MagneticFieldGrid.h"

namespace Tr {

namespace TrackVectorFit {

namespace Vector {

template<unsigned W>
struct Extrapolator {
  using vectype = typename Vectype<W>::type;
  using boolvectype = typename Vectype<W>::booltype;

  template <size_t... Is>
  static constexpr inline boolvectype makeIn (
    const std::bitset<W>& in,
    std::index_sequence<Is...>
  ) {
    return boolvectype{in[Is]...};
  }

  template <size_t... Is>
  static constexpr inline vectype makePreviousZ (
    const std::array<Sch::Item, W>& nodes,
    const std::bitset<W>& in,
    std::index_sequence<Is...>
  ) {
    return vectype{in[Is] ? 0. : (nodes[Is].node - 1)->node().z()...};
  }

  template <size_t... Is>
  static constexpr inline vectype makeCurrentZ (
    const std::array<Sch::Item, W>& nodes,
    std::index_sequence<Is...>
  ) {
    return vectype{(nodes[Is].node)->node().z()...};
  }

  static constexpr inline vectype makeZDiffVector (
    const std::array<Sch::Item, W>& nodes,
    const std::bitset<W>& in
  ) {
    return makeCurrentZ(nodes, std::make_index_sequence<W>()) -
           makePreviousZ(nodes, in, std::make_index_sequence<W>());
  }

  template <size_t... Is>
  static constexpr inline vectype makeDeltaEnergy (
    const std::array<Sch::Item, W>& nodes,
    std::index_sequence<Is...>
  ) {
    return vectype(nodes[Is].node->deltaEnergy()...);
  }


  static inline std::array<vectype, 5> multiply_tm_state (
    const std::array<vectype, 25>& tm,
    fp_ptr_64_const prev_rv_p
  ) {
    // Load reference parameters
    std::array<vectype, 5> v;
    v[0].load_a(prev_rv_p + 0*W);
    v[1].load_a(prev_rv_p + 1*W);
    v[2].load_a(prev_rv_p + 2*W);
    v[3].load_a(prev_rv_p + 3*W);
    v[4].load_a(prev_rv_p + 4*W);

    std::array<vectype, 5> result;
    result[0] = tm[0]*v[0] + tm[1]*v[1] + tm[2]*v[2] + tm[3]*v[3] + tm[4]*v[4];
    result[1] = tm[5]*v[0] + tm[6]*v[1] + tm[7]*v[2] + tm[8]*v[3] + tm[9]*v[4];
    result[2] = tm[10]*v[0] + tm[11]*v[1] + tm[12]*v[2] + tm[13]*v[3] + tm[14]*v[4];
    result[3] = tm[15]*v[0] + tm[16]*v[1] + tm[17]*v[2] + tm[18]*v[3] + tm[19]*v[4];
    result[4] = tm[20]*v[0] + tm[21]*v[1] + tm[22]*v[2] + tm[23]*v[3] + tm[24]*v[4];

    return result;
  }

  static inline void invertAndStore (
    const std::array<vectype, 25> tm,
    fp_ptr_64 prev_btm_p
  ) {
    std::array<vectype, 25> itm;
    
    // Assume the whole thing works for the simplified case
    // write
    //       ( 1  0 |  S00 S01 | U0 )
    //       ( 0  1 |  S10 S01 | U1 )
    // tm =  ( 0  0 |  T00 T01 | V0 )
    //       ( 0  0 |  T10 T11 | V1 )
    //       ( 0  0 |   0   0  | 1  )
    // then we have
    // Tinv = T^{-1}
    
    // double det = tm(2,2)*tm(3,3)-tm(2,3)*tm(3,2);
    const vectype det = tm[12]*tm[18] - tm[13]*tm[17];

    // // x*5 + y
    // itm[12] = tm(3,3)/det;
    // itm[18] = tm(2,2)/det;
    // itm[13] = -tm(2,3)/det;
    // itm[17] = -tm(3,2)/det;
    itm[12] = tm[18] / det;
    itm[18] = tm[12] / det;
    itm[13] = -tm[13] / det;
    itm[17] = -tm[17] / det;

    // // Vinv = - T^-1 * V
    // itm[14] = -itm[12]*tm(2,4) -itm[13]*tm(3,4);
    // itm[19] = -itm[17]*tm(2,4) -itm[18]*tm(3,4);
    itm[14] = -itm[12]*tm[14] -itm[13]*tm[19];
    itm[19] = -itm[17]*tm[14] -itm[18]*tm[19];

    // // Uinv = S * T^-1 * V - U = - S * Vinv - U
    // itm[4] = -tm(0,4) - tm(0,2)*itm[14] - tm(0,3)*itm[19];
    // itm[9] = -tm(1,4) - tm(1,2)*itm[14] - tm(1,3)*itm[19];
    itm[4] = -tm[4] - tm[2]*itm[14] - tm[3]*itm[19];
    itm[9] = -tm[9] - tm[7]*itm[14] - tm[8]*itm[19];

    // // Sinv  = - S * T^{-1}
    // itm[2] = - tm(0,2)*itm[12] - tm(0,3)*itm[17];
    // itm[3] = - tm(0,2)*itm[13] - tm(0,3)*itm[18];
    // itm[7] = - tm(1,2)*itm[12] - tm(1,3)*itm[17];
    // itm[8] = - tm(1,2)*itm[13] - tm(1,3)*itm[18];
    itm[2] = - tm[2]*itm[12] - tm[3]*itm[17];
    itm[3] = - tm[2]*itm[13] - tm[3]*itm[18];
    itm[7] = - tm[7]*itm[12] - tm[8]*itm[17];
    itm[8] = - tm[7]*itm[13] - tm[8]*itm[18];

    // // Rest of elements
    // for (unsigned i=0; i<5; ++i) {
    //   itm[5*i] = tm.Array()[5*i];
    //   itm[5*i + 1] = tm.Array()[5*i + 1];
    // }
    for (unsigned i=0; i<5; ++i) {
      itm[5*i] = tm[5*i];
      itm[5*i + 1] = tm[5*i + 1];
    }

    // itm[22] = tm.Array()[22];
    // itm[23] = tm.Array()[23];
    // itm[24] = tm.Array()[24];
    itm[22] = tm[22];
    itm[23] = tm[23];
    itm[24] = tm[24];

    // Write itm to store
    itm[0].store_a(prev_btm_p + 0*W);
    itm[1].store_a(prev_btm_p + 1*W);
    itm[2].store_a(prev_btm_p + 2*W);
    itm[3].store_a(prev_btm_p + 3*W);
    itm[4].store_a(prev_btm_p + 4*W);
    itm[5].store_a(prev_btm_p + 5*W);
    itm[6].store_a(prev_btm_p + 6*W);
    itm[7].store_a(prev_btm_p + 7*W);
    itm[8].store_a(prev_btm_p + 8*W);
    itm[9].store_a(prev_btm_p + 9*W);
    itm[10].store_a(prev_btm_p + 10*W);
    itm[11].store_a(prev_btm_p + 11*W);
    itm[12].store_a(prev_btm_p + 12*W);
    itm[13].store_a(prev_btm_p + 13*W);
    itm[14].store_a(prev_btm_p + 14*W);
    itm[15].store_a(prev_btm_p + 15*W);
    itm[16].store_a(prev_btm_p + 16*W);
    itm[17].store_a(prev_btm_p + 17*W);
    itm[18].store_a(prev_btm_p + 18*W);
    itm[19].store_a(prev_btm_p + 19*W);
    itm[20].store_a(prev_btm_p + 20*W);
    itm[21].store_a(prev_btm_p + 21*W);
    itm[22].store_a(prev_btm_p + 22*W);
    itm[23].store_a(prev_btm_p + 23*W);
    itm[24].store_a(prev_btm_p + 24*W);
  }

  static inline void correctEnergyLossAndPopulate (
    std::array<Sch::Item, vector_width()>& nodes,
    std::array<vectype, 5> stateVector,
    const std::array<vectype, 25> tm,
    const double minMomentumForELossCorr,
    fp_ptr_64_const prev_rv_p,
    fp_ptr_64 tm_p,
    fp_ptr_64 tv_p,
    fp_ptr_64 prev_btm_p
  ) {
    // Correct for energy loss
    // const auto& dE = n.deltaEnergy();
    // if (std::abs(stateVector.qOverP()) > LHCb::Math::lowTolerance) {
    //   const auto charge = stateVector.qOverP() > 0 ? 1. :  -1.;
    //   const auto momnew = std::max(m_minMomentumForELossCorr.value(), std::abs(1/stateVector.qOverP()) + dE);
    //   if (std::abs(momnew) > m_minMomentumForELossCorr) {
    //     stateVector.setQOverP(charge/momnew);
    //   }
    // }
    const vectype deltaEnergy = makeDeltaEnergy(nodes, std::make_index_sequence<W>());
    const boolvectype do_correct = abs(stateVector[4]) > vectype{LHCb::Math::lowTolerance};
    const vectype charge = select(stateVector[4]>0, vectype{1.}, vectype{-1.});
    const vectype minMomentum = vectype{minMomentumForELossCorr};
    const vectype denNotZero = select(do_correct, stateVector[4], vectype{1.});
    const vectype dE_increase {abs(vectype{1.} / denNotZero) + deltaEnergy};
    const vectype momnew = max(minMomentum, dE_increase);
    stateVector[4] = select(do_correct && abs(momnew) > minMomentum, charge / momnew, stateVector[4]);

    std::array<vectype, 5> transportvector = multiply_tm_state(tm, prev_rv_p);
    transportvector[0] = stateVector[0] - transportvector[0];
    transportvector[1] = stateVector[1] - transportvector[1];
    transportvector[2] = stateVector[2] - transportvector[2];
    transportvector[3] = stateVector[3] - transportvector[3];
    transportvector[4] = stateVector[4] - transportvector[4];

    // Populate transport vector
    transportvector[0].store_a(tv_p + 0*W);
    transportvector[1].store_a(tv_p + 1*W);
    transportvector[2].store_a(tv_p + 2*W);
    transportvector[3].store_a(tv_p + 3*W);
    transportvector[4].store_a(tv_p + 4*W);

    // Populate tm
    tm[0].store_a(tm_p + 0*W);
    tm[1].store_a(tm_p + 1*W);
    tm[2].store_a(tm_p + 2*W);
    tm[3].store_a(tm_p + 3*W);
    tm[4].store_a(tm_p + 4*W);
    tm[5].store_a(tm_p + 5*W);
    tm[6].store_a(tm_p + 6*W);
    tm[7].store_a(tm_p + 7*W);
    tm[8].store_a(tm_p + 8*W);
    tm[9].store_a(tm_p + 9*W);
    tm[10].store_a(tm_p + 10*W);
    tm[11].store_a(tm_p + 11*W);
    tm[12].store_a(tm_p + 12*W);
    tm[13].store_a(tm_p + 13*W);
    tm[14].store_a(tm_p + 14*W);
    tm[15].store_a(tm_p + 15*W);
    tm[16].store_a(tm_p + 16*W);
    tm[17].store_a(tm_p + 17*W);
    tm[18].store_a(tm_p + 18*W);
    tm[19].store_a(tm_p + 19*W);
    tm[20].store_a(tm_p + 20*W);
    tm[21].store_a(tm_p + 21*W);
    tm[22].store_a(tm_p + 22*W);
    tm[23].store_a(tm_p + 23*W);
    tm[24].store_a(tm_p + 24*W);

    // Populate the backward TM
    invertAndStore(std::move(tm), prev_btm_p);
  }

  static inline void veloPropagate (
    std::array<Sch::Item, vector_width()>& nodes,
    const std::bitset<vector_width()>& in,
    std::array<vectype, 5>& stateVector,
    std::array<vectype, 25>& tm,
    fp_ptr_64_const prev_rv_p
  ) {
    // Load previous refParams
    std::array<vectype, 5> refParams;
    refParams[0].load_a(prev_rv_p + 0*W);
    refParams[1].load_a(prev_rv_p + 1*W);
    refParams[2].load_a(prev_rv_p + 2*W);
    refParams[3].load_a(prev_rv_p + 3*W);
    refParams[4].load_a(prev_rv_p + 4*W);

    const vectype zDiff = makeZDiffVector(nodes, in);
    const boolvectype do_propagate =
      !makeIn(in, std::make_index_sequence<W>())
      && (abs(zDiff) >= vectype{TrackParameters::propagationTolerance});
    const vectype dz = select(do_propagate, zDiff, 0.);

    // (*transMat)(0,2) = dz;
    // (*transMat)(1,3) = dz;
    tm[2] = dz;
    tm[8] = dz;

    // stateVector[0] += stateVector[2]*dz;
    // stateVector[1] += stateVector[3]*dz;
    stateVector[0] = refParams[0] + refParams[2] * dz;
    stateVector[1] = refParams[1] + refParams[3] * dz;
    stateVector[2] = refParams[2];
    stateVector[3] = refParams[3];
    stateVector[4] = refParams[4];
  }

  static inline void velo (
    std::array<Sch::Item, vector_width()>& nodes,
    const std::bitset<vector_width()>& in,
    const double minMomentumForELossCorr,
    fp_ptr_64_const prev_rv_p,
    fp_ptr_64 tm_p,
    fp_ptr_64 tv_p,
    fp_ptr_64 prev_btm_p
  ) {
    // Initialize tm with identity
    std::array<vectype, 25> tm;
    tm[0] = 1.;
    tm[1] = 0.;
    tm[2] = 0.;
    tm[3] = 0.;
    tm[4] = 0.;

    tm[5] = 0.;
    tm[6] = 1.;
    tm[7] = 0.;
    tm[8] = 0.;
    tm[9] = 0.;

    tm[10] = 0.;
    tm[11] = 0.;
    tm[12] = 1.;
    tm[13] = 0.;
    tm[14] = 0.;

    tm[15] = 0.;
    tm[16] = 0.;
    tm[17] = 0.;
    tm[18] = 1.;
    tm[19] = 0.;

    tm[20] = 0.;
    tm[21] = 0.;
    tm[22] = 0.;
    tm[23] = 0.;
    tm[24] = 1.;

    std::array<vectype, 5> stateVector;

    // Run extrapolator
    veloPropagate(
      nodes,
      in,
      stateVector,
      tm,
      prev_rv_p
    );

    correctEnergyLossAndPopulate(
      nodes,
      std::move(stateVector),
      std::move(tm),
      minMomentumForELossCorr,
      prev_rv_p,
      tm_p,
      tv_p,
      prev_btm_p
    );
  }

  static inline void parabolicUpdateTransportMatrix (
    std::array<Sch::Item, vector_width()>& nodes,
    std::array<vectype, 5> stateVector,
    const std::array<vectype, 3> B,
    const vectype fac,
    const vectype fact,
    const vectype ax,
    const vectype ay,
    const vectype dz,
    const boolvectype do_propagate,
    const double minMomentumForELossCorr,
    fp_ptr_64_const prev_rv_p,
    fp_ptr_64 tm_p,
    fp_ptr_64 tv_p,
    fp_ptr_64 prev_btm_p
  ) {
    // // to save some typing...
    // double Tx = stateVector[2];
    // double Ty = stateVector[3];
    // double norm2 = 1. + Tx*Tx + Ty*Ty;
    // double norm = std::sqrt( norm2 );
    const vectype Tx = stateVector[2];
    const vectype Ty = stateVector[3];
    const vectype norm2 = 1.0 + Tx*Tx + Ty*Ty;
    const vectype norm = sqrt(norm2);

    // //calculate derivatives of Ax, Ay
    // double dAx_dTx = (Tx*m_ax/norm2) + norm*( Ty*m_B.x()-(2.*Tx*m_B.y()));
    // double dAx_dTy = (Ty*m_ax/norm2) + norm*( Tx*m_B.x()+m_B.z());
    // double dAy_dTx = (Tx*m_ay/norm2) + norm*(-Ty*m_B.y()-m_B.z());
    // double dAy_dTy = (Ty*m_ay/norm2) + norm*(-Tx*m_B.y()+(2.*Ty*m_B.x()));
    const vectype dAx_dTx = (Tx*ax / norm2) + norm * (Ty*B[0] - (2.*Tx*B[1]));
    const vectype dAx_dTy = (Ty*ax / norm2) + norm * (Tx*B[0] + B[2]);
    const vectype dAy_dTx = (Tx*ay / norm2) + norm * (-Ty*B[1] - B[2]);
    const vectype dAy_dTy = (Ty*ay / norm2) + norm * (-Tx*B[1] + (2.*Ty*B[0]));

    // transMat(0,0) = 1 ;
    // transMat(0,1) = 0 ;
    // transMat(0,2) = dz + 0.5 * dAx_dTx * fact*dz;
    // transMat(0,3) =      0.5 * dAx_dTy * fact*dz;
    // transMat(0,4) =      0.5 * m_ax    * fac*dz;
    std::array<vectype, 25> transMat;
    const vectype vec_zero = vectype{0.};
    transMat[0] = 1.;
    transMat[1] = 0.;
    transMat[2] = select(do_propagate, dz + 0.5 * dAx_dTx * fact*dz, vec_zero);
    transMat[3] = select(do_propagate,      0.5 * dAx_dTy * fact*dz, vec_zero);
    transMat[4] = select(do_propagate,      0.5 * ax      * fac*dz, vec_zero);
    
    // transMat(1,0) = 0 ;
    // transMat(1,1) = 1 ;
    // transMat(1,2) =      0.5 * dAy_dTx * fact*dz;
    // transMat(1,3) = dz + 0.5 * dAy_dTy * fact*dz;
    // transMat(1,4) =      0.5 * m_ay    * fac*dz;
    transMat[5] = 0.;
    transMat[6] = 1.;
    transMat[7] =      select(do_propagate, 0.5 * dAy_dTx * fact*dz, vec_zero);
    transMat[8] = dz + select(do_propagate, 0.5 * dAy_dTy * fact*dz, vec_zero);
    transMat[9] =      select(do_propagate, 0.5 * ay      * fac*dz, vec_zero);

    // transMat(2,0) = 0 ;
    // transMat(2,1) = 0 ;
    // transMat(2,2) = 1.0 + dAx_dTx * fact;
    // transMat(2,3) =       dAx_dTy * fact;
    // transMat(2,4) =       m_ax    * fac;
    transMat[10] = 0.;
    transMat[11] = 0.;
    transMat[12] = 1.0 + select(do_propagate, dAx_dTx * fact, vec_zero);
    transMat[13] =       select(do_propagate, dAx_dTy * fact, vec_zero);
    transMat[14] =       select(do_propagate, ax      * fac, vec_zero);

    // transMat(3,0) = 0 ;
    // transMat(3,1) = 0 ;
    // transMat(3,2) =       dAy_dTx * fact;
    // transMat(3,3) = 1.0 + dAy_dTy * fact;
    // transMat(3,4) =       m_ay    * fac;
    transMat[15] = 0.;
    transMat[16] = 0.;
    transMat[17] =       select(do_propagate, dAy_dTx * fact, vec_zero);
    transMat[18] = 1.0 + select(do_propagate, dAy_dTy * fact, vec_zero);
    transMat[19] =       select(do_propagate, ay      * fac, vec_zero);

    // transMat(4,0) = 0 ;
    // transMat(4,1) = 0 ;
    // transMat(4,2) = 0 ;
    // transMat(4,3) = 0 ;
    // transMat(4,4) = 1 ;
    transMat[20] = 0.;
    transMat[21] = 0.;
    transMat[22] = 0.;
    transMat[23] = 0.;
    transMat[24] = 1.;

    correctEnergyLossAndPopulate(
      nodes,
      std::move(stateVector),
      std::move(transMat),
      minMomentumForELossCorr,
      prev_rv_p,
      tm_p,
      tv_p,
      prev_btm_p
    );
  }

  static inline void parabolic (
    std::array<Sch::Item, vector_width()>& nodes,
    const std::bitset<vector_width()>& in,
    const double minMomentumForELossCorr,
    const LHCb::MagneticFieldGrid* magneticFieldGrid,
    const bool usesGridInterpolation,
    fp_ptr_64_const prev_rv_p,
    fp_ptr_64 tm_p,
    fp_ptr_64 tv_p,
    fp_ptr_64 prev_btm_p
  ) {
    //   // get the B field at midpoint
    //   const double dz = zNew - zOld;
    //   const double xMid = stateVector[0] + (0.5*stateVector[2]*dz);
    //   const double yMid = stateVector[1] + (0.5*stateVector[3]*dz);
    //   XYZPoint P( xMid, yMid, zOld+(0.5*dz) );
    //   Gaudi::XYZVector m_B = fieldVector( P ) ;
    const vectype prevZ = makePreviousZ(nodes, in, std::make_index_sequence<W>());
    const vectype currentZ = makeCurrentZ(nodes, std::make_index_sequence<W>());
    const vectype dz = currentZ - prevZ;

    // Condition to propagate
    const boolvectype do_propagate = abs(dz) >= vectype{TrackParameters::propagationTolerance};

    // Load ref params
    std::array<vectype, 5> stateVector;
    stateVector[0].load_a(prev_rv_p + 0*W);
    stateVector[1].load_a(prev_rv_p + 1*W);
    stateVector[2].load_a(prev_rv_p + 2*W);
    stateVector[3].load_a(prev_rv_p + 3*W);
    stateVector[4].load_a(prev_rv_p + 4*W);

    const std::array<vectype, 3> point {
      vectype{stateVector[0] + (0.5 * stateVector[2] * dz)},
      vectype{stateVector[1] + (0.5 * stateVector[3] * dz)},
      vectype{prevZ + (0.5 * dz)}
    };
    
    // const std::array<vectype, 3> B = fieldVector(point);
    const std::array<vectype, 3> B = usesGridInterpolation ? 
        magneticFieldGrid->horizontallyVectorizedFieldVector<vectype, vector_width()>(point) : 
        magneticFieldGrid->horizontallyVectorizedFieldVectorClosestPoint<vectype, vector_width()>(point);
    
    // const double Tx   = stateVector[2];
    // const double Ty   = stateVector[3];
    // const double nTx2 = 1.0+Tx*Tx;
    // const double nTy2 = 1.0+Ty*Ty;
    // const double norm = std::sqrt( nTx2 + nTy2 - 1.0 );
    const vectype Tx = stateVector[2];
    const vectype Ty = stateVector[3];
    const vectype nTx2 = 1.0 + Tx * Tx;
    const vectype nTy2 = 1.0 + Ty * Ty;
    const vectype norm = sqrt(nTx2 + nTy2 - 1.0);

    // // calculate the A factors
    // double m_ax = norm*( Ty*(Tx*m_B.x()+m_B.z())-(nTx2*m_B.y()));
    // double m_ay = norm*(-Tx*(Ty*m_B.y()+m_B.z())+(nTy2*m_B.x()));
    // const double fac = eplus*c_light*dz;
    // const double fact = fac * stateVector[4];
    const vectype ax = norm * (Ty * (Tx * B[0] + B[2]) - (nTx2 * B[1]));
    const vectype ay = norm * (-Tx * (Ty * B[1] + B[2]) + (nTy2 * B[0]));
    const vectype fac = Gaudi::Units::eplus * Gaudi::Units::c_light * dz;
    const vectype fact = fac * stateVector[4];

    // // Update the state parameters (exact extrapolation)
    // stateVector[0] += dz*( Tx + 0.5 * m_ax * fact );
    // stateVector[1] += dz*( Ty + 0.5 * m_ay * fact );
    // stateVector[2] += m_ax * fact;
    // stateVector[3] += m_ay * fact;
    const vectype vec_zero = vectype{0.};
    stateVector[0] += select(do_propagate, dz * (Tx + 0.5 * ax * fact), vec_zero);
    stateVector[1] += select(do_propagate, dz * (Ty + 0.5 * ay * fact), vec_zero);
    stateVector[2] += select(do_propagate, ax * fact, vec_zero);
    stateVector[3] += select(do_propagate, ay * fact, vec_zero);

    parabolicUpdateTransportMatrix(
      nodes,
      std::move(stateVector),
      std::move(B),
      std::move(fac),
      std::move(fact),
      std::move(ax),
      std::move(ay),
      std::move(dz),
      std::move(do_propagate),
      minMomentumForELossCorr,
      prev_rv_p,
      tm_p,
      tv_p,
      prev_btm_p
    );
  }
};

}

}

}
