/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "../Types.h"
#include "Math.h"

namespace Tr {

namespace TrackVectorFit {

namespace Vector {

template<class T>
struct predict {
  template<size_t W>
  static inline void op (
    fp_ptr_64_const tv,
    fp_ptr_64_const nm,
    fp_ptr_64_const tm,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  );
};

template<>
struct predict<Op::Forward> {
  template<size_t W>
  static inline void op (
    fp_ptr_64_const tv,
    fp_ptr_64_const nm,
    fp_ptr_64_const tm,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  ) {
    Math<Op::Forward>::predictState<W> (
      tm,
      tv,
      us,
      ps
    );

    Math<Op::Forward>::predictCovariance<W> (
      tm,
      nm,
      uc,
      pc
    );
  }
};

template<>
struct predict<Op::Backward> {
  template<size_t W>
  static inline void op (
    fp_ptr_64_const tv,
    fp_ptr_64_const nm,
    fp_ptr_64_const tm,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  ) {
    Math<Op::Backward>::predictState<W> (
      tm,
      tv,
      us,
      ps
    );

    Math<Op::Backward>::predictCovariance<W> (
      tm,
      nm,
      uc,
      pc
    );
  }
};

}

}

}
