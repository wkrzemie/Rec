###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TrackVectorFit
################################################################################
gaudi_subdir(TrackVectorFit)

gaudi_depends_on_subdirs(GaudiKernel
                         Kernel/VectorClass
                         Kernel/LHCbKernel
                         Tr/TrackInterfaces)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(TrackVectorFit src/TrackVectorFit.cpp
                  PUBLIC_HEADERS TrackVectorFit
                  LINK_LIBRARIES TrackFitEvent LHCbKernel)

if(LCG_COMP STREQUAL gcc )
  set_property(SOURCE src/TrackVectorFit.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -fabi-version=0 " )
endif()
