/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKMUONMATCHMONITOR_H
#define TRACKMUONMATCHMONITOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "Event/Track.h"

class DeMuonDetector;
class ITrackEtrapolator;

/** @class TrackMuonMatchMonitor TrackMuonMatchMonitor.h
 *
 *
 *  @author Stefania Vecchi
 *  @date   2010-01-22
 */
class TrackMuonMatchMonitor : public GaudiHistoAlg {
public:
  /// Standard constructor
  using GaudiHistoAlg::GaudiHistoAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:

  DeMuonDetector*       m_muonDet = nullptr;
  ToolHandle<ITrackExtrapolator>   m_extrapolator{ this,  "Extrapolator", "TrackLinearExtrapolator"  };
  Gaudi::Property<std::string> m_tTracksLocation {this, "TracksLocation", LHCb::TrackLocation::Default};

  double m_zM1, m_MAXsizeX, m_MAXsizeY;
  Gaudi::Property<int> m_iMS {this, "WhichStation" , 0 };
  Gaudi::Property<double> m_maxErrX { this,  "MaxErrX",  5 * Gaudi::Units::mm };
  Gaudi::Property<double> m_maxErrY { this,  "MaxErrY", 20 * Gaudi::Units::mm };

  static constexpr int nREGIONS = 4;
  AIDA::IHistogram1D  *m_resx_a[nREGIONS], *m_resy_a[nREGIONS], *m_resx_c[nREGIONS], *m_resy_c[nREGIONS];
  double m_hisxmax[nREGIONS] ;

};
#endif // TRACKMUONMATCHMONITOR_H
