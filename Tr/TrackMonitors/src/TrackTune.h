/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TrackTune_H_
#define TrackTune_H_

#include "GaudiAlg/GaudiTupleAlg.h"
#include "Event/Track.h"
#include "Event/Particle.h"

namespace LHCb{
  class Particle;
}

class TrackTune: public GaudiTupleAlg{

 public:

  TrackTune(const std::string& name, ISvcLocator* pSvc);

  StatusCode initialize() override;

  StatusCode execute() override;


 private:

  const LHCb::Track* track(const LHCb::Particle& part) const;

  bool isFound(const LHCb::Track::Range& tracks, const LHCb::Particle& part) const;

  std::vector<const LHCb::Particle*> select( const LHCb::Particle::Range& input) const;

  bool inMassRange(const LHCb::Particle& particle) const;

  std::string m_particleLocation;
  std::string m_trackLocation;
  Gaudi::Property<std::string> m_resonanceName { this, "resonanceName", "J/psi(1S)" };

  Gaudi::Property<double> m_deltaMass{ this, "resonance" , 110. } ;
  double m_minMass = 0;
  double m_maxMass = 0;
  Gaudi::Property<bool> m_selectBest{ this, "selectBest", true };
  Gaudi::Property<double> m_minPurityCut{ this, "minPurityCut", 0.7 };


};


#endif
