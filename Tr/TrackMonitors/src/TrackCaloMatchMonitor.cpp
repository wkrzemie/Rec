/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/ToolHandle.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "Event/Track.h"
#include "Event/Node.h"
#include "Event/FitNode.h"
#include "Event/StateParameters.h"
#include "Event/State.h"
#include "Event/CaloCluster.h"
#include "Event/CaloPosition.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "CaloDet/DeCalorimeter.h"
#include "TrackVectorFit/Types.h"
#include "Kernel/VectorSOAMatrixView.h"
#include <optional>

#include "AIDA/IProfile1D.h"
#include "AIDA/IHistogram1D.h"


#ifdef _WIN32
#pragma warning ( disable : 4355 ) // This used in initializer list, needed for ToolHandles
#endif


class TrackCaloMatchMonitor : public GaudiHistoAlg
{
public:
   /** Standard construtor */
  using GaudiHistoAlg::GaudiHistoAlg;

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm execute */
  StatusCode execute() override;

private:
  Gaudi::Property<std::string> m_trackLocation{ this, "TrackLocation", LHCb::TrackLocation::Default  };
  Gaudi::Property<std::string> m_caloName { this, "CaloSystem",  "Ecal" };
  Gaudi::Property<bool> m_useClusters { this, "UseClusters", true  };
  Gaudi::Property<bool> m_useGeometricZ { this,  "UseGeometricZ",  true } ; // this you need for cosmics (MIPs)
  Gaudi::Property<bool> m_requireTHits { this,  "RequireTHits", true } ; // this you need for cosmics (MIPs)
  Gaudi::Property<double> m_clusterZCorrection { this, "ClusterZCorrection", 0 };
  std::string m_clusterLocation;
  std::string m_caloDigitLocation;
  DeCalorimeter* m_caloDet ;
  double m_geometricZ ;
  ToolHandle<ITrackExtrapolator> m_extrapolator { this, "Extrapolator", "TrackMasterExtrapolator" };

  IHistogram1D* m_dxASideH1[3] ;
  IHistogram1D* m_dyASideH1[3] ;
  IHistogram1D* m_dxCSideH1[3] ;
  IHistogram1D* m_dyCSideH1[3] ;
  IHistogram1D* m_dyVeloASideH1[3] ;
  IHistogram1D* m_dyVeloCSideH1[3] ;
  IHistogram1D* m_zH1[3] ;
  IHistogram1D* m_eOverPH1[3] ;
  IProfile1D* m_dyVsYPr ;
  IProfile1D* m_dyVsXPr ;
  IProfile1D* m_dyVsTyPr ;
  IProfile1D* m_dyVsTxPr ;
  IProfile1D* m_dxVsYPr ;
  IProfile1D* m_dxVsXPr ;
  IProfile1D* m_dxVsTyPr ;
  IProfile1D* m_dxVsTxPr ;

} ;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackCaloMatchMonitor )

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackCaloMatchMonitor::initialize()
{
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;             // error printed already by GaudiAlgorithm
  sc = m_extrapolator.retrieve() ;
  if ( sc.isFailure() ) {
    error() << "Could not retrieve extrapolator" << endmsg ;
    return sc;
  }

  if(m_caloName == "Ecal") {
    m_caloDet = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
    if( m_useClusters ) m_clusterLocation = LHCb::CaloClusterLocation::Ecal ;
    m_caloDigitLocation = LHCb::CaloDigitLocation::Ecal ;
    m_geometricZ = m_caloDet->geometry()->toGlobal(Gaudi::XYZPoint()).z() + m_caloDet->zOffset() ;
  } else if( m_caloName == "Hcal") {
    m_caloDet = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
    if( m_useClusters ) m_clusterLocation = LHCb::CaloClusterLocation::Hcal ;
    m_caloDigitLocation = LHCb::CaloDigitLocation::Hcal ;
    m_geometricZ = m_caloDet->geometry()->toGlobal(Gaudi::XYZPoint()).z() + m_caloDet->zOffset() ;
  } else if( m_caloName == "Prs") {
    m_caloDet = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs );
    m_caloDigitLocation = LHCb::CaloDigitLocation::Prs ;
    m_geometricZ = m_caloDet->geometry()->toGlobal(Gaudi::XYZPoint()).z() ;
  } else if( m_caloName == "Spd") {
    m_caloDigitLocation = LHCb::CaloDigitLocation::Spd ;
    m_caloDet = getDet<DeCalorimeter>( DeCalorimeterLocation::Spd );
    m_geometricZ = m_caloDet->geometry()->toGlobal(Gaudi::XYZPoint()).z() ;
  } else {
    error() << "Unknown calo system: " << m_caloName << endmsg ;
    sc = StatusCode::FAILURE ;
  }

  setHistoTopDir("Track/") ;
  char systitle[3][128] = { "outer","middle","inner"} ;
  char histitle[512] ;
  for(int i=0; i<3; ++i) {
    sprintf(histitle,"x%s - xTRK (%s A-side)",m_caloName.value().c_str(),systitle[i]) ;
    m_dxASideH1[i] = book1D(histitle,-200,200) ;
    sprintf(histitle,"y%s - yTRK (%s A-side)",m_caloName.value().c_str(),systitle[i]) ;
    m_dyASideH1[i] = book1D(histitle,-200,200) ;
    sprintf(histitle,"x%s - xTRK (%s C-side)",m_caloName.value().c_str(),systitle[i]) ;
    m_dxCSideH1[i] = book1D(histitle,-200,200) ;
    sprintf(histitle,"y%s - yTRK (%s C-side)",m_caloName.value().c_str(),systitle[i]) ;
    m_dyCSideH1[i] = book1D(histitle,-200,200) ;
    sprintf(histitle,"y%s - yVELO (%s A-side)",m_caloName.value().c_str(),systitle[i]) ;
    m_dyVeloASideH1[i] = book1D(histitle,-200,200) ;
    sprintf(histitle,"y%s - yVELO (%s C-side)",m_caloName.value().c_str(),systitle[i]) ;
    m_dyVeloCSideH1[i] = book1D(histitle,-200,200) ;
    sprintf(histitle,"zMatch (%s)",systitle[i]) ;
    m_zH1[i] = book1D(histitle,m_geometricZ - 400, m_geometricZ + 400 ) ;
    sprintf(histitle,"E over P (%s)",systitle[i]) ;
    m_eOverPH1[i] = book1D(histitle,-2,2) ;
  }

  m_dxVsXPr = bookProfile1D("dxVsX","dx versus x",-3500,3500) ;
  m_dxVsYPr = bookProfile1D("dxVsY","dx versus y",-3500,3500) ;
  m_dxVsTxPr = bookProfile1D("dxVsTx","dx versus tx",-0.6,0.6) ;
  m_dxVsTyPr = bookProfile1D("dxVsTy","dx versus ty",-0.3,0.3) ;

  m_dyVsXPr = bookProfile1D("dyVsX","dy versus x",-3500,3500) ;
  m_dyVsYPr = bookProfile1D("dyVsY","dy versus y",-3500,3500) ;
  m_dyVsTxPr = bookProfile1D("dyVsTx","dy versus tx",-0.6,0.6) ;
  m_dyVsTyPr = bookProfile1D("dyVsTy","dy versus ty",-0.3,0.3) ;

  if(msgLevel(MSG::DEBUG)) debug() << "CaloDet: center = "
	  << m_caloDet->geometry()->toGlobal(Gaudi::XYZPoint())
	  << " zoffset: " << m_caloDet->zOffset()
	  << " geometric Z: " << m_geometricZ
	  << " zsize:   " << m_caloDet->zSize() << endmsg ;

  return sc;
}

namespace {

  struct MyCaloPosition
  {
    MyCaloPosition( const LHCb::CaloCellID& _cell,
		    const LHCb::CaloPosition& _pos) : cell(_cell), pos(_pos) {}
    LHCb::CaloCellID cell ;
    LHCb::CaloPosition pos ;
  } ;

  const LHCb::State* unbiasedVeloState(const LHCb::Track& track)
  {
    auto nodes = track.nodes() ;
    if( !track.hasVelo() || nodes.empty() ) return nullptr;
    // find the most-downstream node still within velo
    const LHCb::Node *node = std::accumulate( nodes.begin(), nodes.end(),
                                              static_cast<const LHCb::Node*>(nullptr),
                                              [](const LHCb::Node* n, const LHCb::Node* i) {
                                                  if (i->z() >= StateParameters::ZEndVelo) return n;
                                                  return ( !n || ( i->z() > n->z() ) ) ? i : n;
                                              } );

    const bool upstream = ( (*nodes.begin())->z() > (*nodes.rbegin())->z() ); // FIXME: when we use range::span, we can use 'front' and 'back' again...
    const LHCb::FitNode* fnode = dynamic_cast<const LHCb::FitNode*>(node);
    if (fnode) {
      // TrackMasterFit
      return upstream ? &fnode->filteredStateBackward() : &fnode->filteredStateForward();
    }
    const Tr::TrackVectorFit::FitNode* vfnode = dynamic_cast<const Tr::TrackVectorFit::FitNode*>(node);
    if (vfnode) {
      // TrackVectorFit
      return upstream ? &vfnode->backwardState() : &vfnode->forwardState();
    }
    return nullptr;
  }
}

StatusCode TrackCaloMatchMonitor::execute()
{
  // make a list of calo positions, depending on the subsystem use clusters or cells
  std::vector< MyCaloPosition > calopositions ;

  if( !m_clusterLocation.empty() ) {
    const LHCb::CaloClusters* caloclusters = get<LHCb::CaloClusters>( m_clusterLocation ) ;
    calopositions.reserve( caloclusters->size()) ;
    for( const LHCb::CaloCluster* cluster: *caloclusters) {
      calopositions.emplace_back( cluster->seed(), cluster->position() ) ;
    }
  } else {
    const LHCb::CaloDigits* calodigits = get<LHCb::CaloDigits>( m_caloDigitLocation ) ;
    calopositions.reserve( calodigits->size()) ;
    for( const LHCb::CaloDigit* digit: *calodigits) {
      if (! m_caloDet->isNoisy(digit->cellID()) ) {
        Gaudi::XYZPoint center = m_caloDet->cellCenter(digit->cellID()) ;
        Gaudi::Vector3 parameters = { center.x(), center.y(), digit->e() };
        LHCb::CaloPosition pos ;
        pos.setZ( center.z() ) ;
        pos.setParameters(parameters) ;
        calopositions.emplace_back( digit->cellID(), pos ) ;
      }
    }
  }

  if(m_useGeometricZ) {
    for( MyCaloPosition& cluster: calopositions)
      cluster.pos.setZ( m_geometricZ ) ;
  }

  LHCb::Track::Range trackcontainer = get<LHCb::Track::Range>( m_trackLocation.value() ) ;
  for( const LHCb::Track* track: trackcontainer)
    if( !m_requireTHits || track->hasT() ) {
      const LHCb::State& closest = track->closestState( m_geometricZ  );
      LHCb::StateVector state = { closest.stateVector(), closest.z() };
      m_extrapolator->propagate( state, m_geometricZ ) ;

      const LHCb::State* fullvelostate = unbiasedVeloState(*track);

      std::optional<LHCb::StateVector> velostate ;
      if(fullvelostate) {
        velostate = LHCb::StateVector( fullvelostate->stateVector(), fullvelostate->z()) ;
        velostate->setQOverP( state.qOverP() ) ;
        m_extrapolator->propagate( *velostate, m_geometricZ ) ;
      }

      for( const MyCaloPosition& cluster: calopositions) {
        //state = &(track->closestState(pos.z())) ;
        double dz = cluster.pos.z() + m_clusterZCorrection.value() - state.z() ;
        double xtrack = state.x() + state.tx() * dz ;
        double ytrack = state.y() + state.ty() * dz ;
        double dx = cluster.pos.x() - xtrack ;
        double dy = cluster.pos.y() - ytrack ;
        if( std::abs(dy)<200 && std::abs(dx)<200 ) {
          if( xtrack > 0 ) {
            m_dxASideH1[cluster.cell.area()]->fill( dx ) ;
            m_dyASideH1[cluster.cell.area()]->fill( dy ) ;
          } else {
            m_dxCSideH1[cluster.cell.area()]->fill( dx ) ;
            m_dyCSideH1[cluster.cell.area()]->fill( dy ) ;
          }
          m_eOverPH1[cluster.cell.area()]->fill( cluster.pos.e() * state.qOverP() ) ;
          // compute the z-coordinate for which sqrt(dx^2+dy^2) is minimal
          double tx = state.tx() ;
          double ty = state.ty() ;
          double ddz = ( tx*dx + ty*dy ) / (tx*tx+ty*ty) ;
          m_zH1[cluster.cell.area()]->fill( cluster.pos.z() + ddz ) ;
          if( std::abs(dy)<200 && std::abs(dx)<100 ) {
            m_dxVsXPr->fill( xtrack,dx ) ;
            m_dxVsYPr->fill( ytrack,dx ) ;
            m_dxVsTxPr->fill( state.tx(),dx ) ;
            m_dxVsTyPr->fill( state.ty(),dx ) ;
            m_dyVsXPr->fill( xtrack,dy ) ;
            m_dyVsYPr->fill( ytrack,dy ) ;
            m_dyVsTxPr->fill( state.tx(),dy ) ;
            m_dyVsTyPr->fill( state.ty(),dy ) ;
          }
        }
        if(velostate && std::abs(dx)<200 ) {
          ytrack = velostate->y() + velostate->ty() * dz ;
          dy = cluster.pos.y() - ytrack ;
          if( cluster.pos.x() > 0 ) {
            m_dyVeloASideH1[cluster.cell.area()]->fill( dy ) ;
          } else {
            m_dyVeloCSideH1[cluster.cell.area()]->fill( dy ) ;
          }
        }
      }
    }
  return StatusCode::SUCCESS ;
}
