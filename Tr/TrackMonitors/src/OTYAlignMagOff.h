/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef OTYALIGNMAGOFF_H 
#define OTYALIGNMAGOFF_H 1

// Include files
#include "TrackMonitorBase.h"

class DeOTDetector;
class DeOTModule;

namespace LHCb{
  class State;
}

/** @class OTYAlignMagOff OTYAlignMagOff.h
 *  ...
 *
 *  @author Frederic Guillaume Dupertuis
 *  @date   2010-05-31
 */
class OTYAlignMagOff : public TrackMonitorBase {
public: 
  /// Standard constructor
  OTYAlignMagOff( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~OTYAlignMagOff( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

  bool inModule(const LHCb::State& state, const DeOTModule* module) const;
  
protected:

private:
  DeOTDetector* m_tracker;
  double m_xTol;
  double m_yTol;
  bool m_global;
  bool m_expected;
  bool m_printmoduleinfo;
};
#endif // OTYALIGNMAGOFF_H
