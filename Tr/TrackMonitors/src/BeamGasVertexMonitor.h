/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef BEAMGASVERTEXMONITOR_H
#define BEAMGASVERTEXMONITOR_H 1

#include <GaudiAlg/GaudiTupleAlg.h>
#include <VeloDet/DeVelo.h>
#include <Event/RecVertex.h>
#include <Event/MCVertex.h>


class BeamGasVertex {
public:
  BeamGasVertex() = default;
  BeamGasVertex(const LHCb::HltObjectSummary& recvtx_hos, const DeVelo* velo);
  BeamGasVertex(const LHCb::RecVertex& recvtx);
  BeamGasVertex(const LHCb::MCVertex& mcvtx);

  double x=0, y=0, z=0, chi2=-1;
  int ndof=-1, ntr=-1, ntrbw=-1;

private:
  bool isBackwardTrack(const LHCb::HltObjectSummary& track_hos, const DeVelo* velo);
};

struct BeamGasCandidate final {
  BeamGasCandidate() = default;
  BeamGasCandidate(const BeamGasVertex& pv) : pv(pv)  {};
  BeamGasCandidate(const BeamGasVertex& pv, const BeamGasVertex& sv1, const BeamGasVertex& sv2)
  : pv(pv), sv1(sv1), sv2(sv2) {};

  BeamGasVertex pv, sv1, sv2;
  std::set<std::string> selections;
};

typedef std::vector<BeamGasCandidate> BeamGasCandidates;
typedef std::vector<std::pair<std::string, BeamGasCandidates>> BeamGasCandidatesSet;


/** @class BeamGasVertexMonitor BeamGasVertexMonitor.h
 *  Algorithm to write beam-gas candidates to a tuple
 *
 *  @author Rosen Matev
 *  @date   2015-05-08
 */
class BeamGasVertexMonitor : public GaudiTupleAlg {
public:

  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:

  BeamGasCandidates getHltCandidates(const LHCb::HltSelReports& selReports);

  DeVelo* m_velo = nullptr;

  // properties
  Gaudi::Property<std::string> m_reportLocation { this, "ReportLocation", "Hlt1/SelReports" };
  Gaudi::Property<std::vector<std::string>> m_selNames { this, "Selections" };
  Gaudi::Property<std::vector<std::string>> m_svLocations { this, "SplitLocations" };
  Gaudi::Property<std::vector<std::string>> m_svPrefixes { this, "SplitPrefixes", };
  Gaudi::Property<double> m_maxMatchDeltaZ { this, "MaxMatchDeltaZ",  10.};

};
#endif // BEAMGASVERTEXMONITOR_H
