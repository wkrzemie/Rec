/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TrackBestTrackCreator.h"
#include <algorithm>
#include <functional>

DECLARE_COMPONENT( TrackBestTrackCreator )

/// hack to allow for tools with non-const interfaces...
template <typename IFace>
IFace* fixup(const ToolHandle<IFace>& iface) { return &const_cast<IFace&>(*iface); }

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackBestTrackCreator::TrackBestTrackCreator( const std::string& name, ISvcLocator* pSvcLocator ) :
MergingTransformer(name, pSvcLocator,
                   // NOTE that the algorithm behaviour is weakly dependent on the ordering of the input containers,
                   // because if the track 'quality' is the same the ordering comes from the input container ordering.
                   // The quality is related to the track type and number of LHCbIDs.
                   // See LHCBPS-1757 for more details. One should be careful to set this order deterministically.
                   // NOTE that optimising the order may improve algorithm performance -- to be checked
                   { "TracksInContainers",
                       { LHCb::TrackLocation::Forward,
                         LHCb::TrackLocation::Match,
                         LHCb::TrackLocation::VeloTT,
                         LHCb::TrackLocation::Downstream,
                         LHCb::TrackLocation::Tsa,
                         LHCb::TrackLocation::Velo }
                   },
                   {"TracksOutContainer", LHCb::TrackLocation::Default}
                   ) {
  declareProperty( "StateInitTool", m_stateinittool );
  declareProperty( "Fitter", m_fitter );
  declareProperty( "GhostIdTool", m_ghostTool );
}


//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackBestTrackCreator::initialize()
{
  StatusCode sc = MergingTransformer::initialize();
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  m_debugLevel = msgLevel(MSG::DEBUG);
  if( m_debugLevel ) debug() << "==> Initialize" << endmsg;

  // ------------------------------
  if( !m_initTrackStates ) m_stateinittool.disable();

  if( !m_fitTracks ) m_fitter.disable();

  if (!m_addGhostProb ) m_ghostTool.disable();

  // Process in batches in case we are not using MasterFitter
  std::string prefix ("TrackMasterFitter");
  m_processInBatches = m_fitter.typeAndName().compare(0, prefix.size(), prefix);

  // Print out the user-defined settings
  // -----------------------------------
  if( m_debugLevel )
    debug() << endmsg
      << "============ TrackBestTrackCreator Settings ===========" << endmsg
      << "TracksInContainers : " << getProperty( "TracksInContainers" ).toString() << endmsg
      << "TrackOutContainer  : " << getProperty("TracksOutContainer").toString() << endmsg
      << "=======================================================" << endmsg
      << endmsg;

  return sc;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode TrackBestTrackCreator::finalize()
{
  if( m_debugLevel ) debug() << "==> Finalize" << endmsg;

  if( m_fitTracks ) {
    double perf = (1.0 - counter("FitFailed").mean())*100;
    info() << "  Fitting performance   : "
           << format( " %7.2f %%", perf ) << endmsg;
  }

  m_stateinittool.release().ignore();
  m_fitter.release().ignore();

  return MergingTransformer::finalize();
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Tracks
TrackBestTrackCreator::operator()(const Gaudi::Functional::vector_of_const_<LHCb::Tracks> &ranges) const {
  if (m_debugLevel) debug() << "==> Execute" << endmsg;
  if (m_addGhostProb.value()) {
    //@FIXME: make IGhostTool interface stateless
    fixup(m_ghostTool)->beginEvent().ignore();
  }
  m_nGhosts = 0;

  // create pool for TrackData objects for all input tracks
  std::vector<TrackData> trackdatapool;
  // keep a record of which track goes where
  CopyMap copymap;
  // get total number of tracks
  size_t nTracks = std::accumulate(std::begin(ranges), std::end(ranges),
                                   size_t(0), [] (size_t sz, const LHCb::Tracks& range)
                                   { return sz + range.size(); });
  // reserve enough space so we don't have to reallocate
  trackdatapool.reserve(nTracks);
  copymap.reserve(nTracks);

  // generate the TrackData objects for the input tracks, initialising the
  // States for use in the Kalman filter on the way
  nTracks = 0;
  for (auto& range : ranges) {
    for (auto& oldtr : range) {
      // clone track
      std::unique_ptr<LHCb::Track> tr {new LHCb::Track()};
      tr->copy(*oldtr);

      // pre-initialise (if required)
      const bool fitted = m_doNotRefit.value() && (tr->fitStatus() == LHCb::Track::FitStatus::Fitted ||
                                                   tr->fitStatus() == LHCb::Track::FitStatus::FitFailed);
      if (fitted && tr->fitStatus() == LHCb::Track::FitStatus::FitFailed)
        continue;
      StatusCode sc(m_initTrackStates.value() && !fitted ?
                    m_stateinittool->fit(*tr, true) : StatusCode::SUCCESS);
      // if successful, create TrackData object
      if (sc.isSuccess()) {
        if (m_useAncestorInfo.value()) {
          // save mapping between original track and its copy
          copymap.emplace_back(oldtr, nTracks);
        }
        ++nTracks;
        // keep a record where this track came from
        tr->addToAncestors(oldtr);
        trackdatapool.push_back(TrackData(std::move(tr)));
      } else {
        // report error in log file
        Warning("TrackStateInitTool fit failed", sc, 0).ignore();
      }
    }
  }

  // take a vector of "references" which is much easier to sort (because less
  // data is moved around)
  std::vector<std::reference_wrapper<TrackData>> alltracks (trackdatapool.begin(), trackdatapool.end());

  // sort them by quality
  auto qualitySort = [] (const TrackData& t1, const TrackData& t2) { return t1 < t2; };
  std::stable_sort(alltracks.begin(), alltracks.end(), qualitySort);

  // Prepare TrackData reference containers
  std::vector<std::reference_wrapper<TrackData>> successful_tracks;
  // Helper function to verify if t is a clone

  // Conditions for being a clone:
  // * cloneFlag is true
  // * It's a clone of any track in v
  auto isClone = [&] (
    TrackData& t,
    const std::vector<std::reference_wrapper<TrackData>>& v
  ) {
    const auto firstClone = std::find_if(v.begin(), v.end(),
      [&] (const TrackData& t2) { return areClones(t, t2); });
    if (firstClone != v.end()) return true;
    return false;
  };

  if (!m_processInBatches) {
    // Sequential treatment
    std::for_each(alltracks.begin(), alltracks.end(), [&] (TrackData& t) {
      if (
        !t.cloneFlag()
        && !isClone(t, successful_tracks)
      ) {
        std::vector<std::reference_wrapper<TrackData>> to_process {t};
        fitAndSelect(to_process.begin(), to_process.end());
        if (t.isAccepted()) {
          successful_tracks.push_back(t);
        }
      }
    });
  } else {
    // isClone function with added functionality
    auto isCloneExtended = [&] (
      TrackData& t,
      const std::vector<std::reference_wrapper<TrackData>>& v
    ) {
      const auto firstClone = std::find_if(v.begin(), v.end(),
        [&] (const TrackData& t2) { return areClones(t, t2); });
      if (firstClone != v.end()) {
        // Add the clone found to the list of clones of that track
        TrackData& foundT = *firstClone;
        foundT.clones.push_back(t);

        return true;
      }
      return false;
    };

    std::vector<std::reference_wrapper<TrackData>> to_process;
    std::vector<std::reference_wrapper<TrackData>> unsuccessful_tracks;

    RANGES_FOR(auto chunk, ranges::v3::view::chunk(alltracks, m_batchSize.value())) {
      // Fill to_process
      std::for_each(chunk.begin(), chunk.end(), [&] (TrackData& t) {
        if (
          !t.cloneFlag()
          && !isClone(t, successful_tracks)
          && !isCloneExtended(t, to_process)
        ) {
          to_process.push_back(t);
        }
      });

      // Perform fit
      fitAndSelect(to_process.begin(), to_process.end());

      // Move to successful
      std::partition_copy(to_process.begin(), to_process.end(),
        std::back_inserter(successful_tracks), std::back_inserter(unsuccessful_tracks),
        [] (const TrackData& t) { return t.isAccepted(); }
      );

      // Create new to_process batch
      to_process.clear();
      std::for_each(unsuccessful_tracks.begin(), unsuccessful_tracks.end(), [&] (TrackData& t) {
        std::copy_if(t.clones.begin(), t.clones.end(), std::back_inserter(to_process),
          [&] (TrackData& c) {
            return !isClone(c, successful_tracks) && !isCloneExtended(c, to_process);
          }
        );
      });

      unsuccessful_tracks.clear();
    }

    // Remaining tracks
    while (!to_process.empty()) {
      // Perform fit
      fitAndSelect(to_process.begin(), to_process.end());

      // Move to successful
      std::partition_copy(to_process.begin(), to_process.end(),
        std::back_inserter(successful_tracks), std::back_inserter(unsuccessful_tracks),
        [] (const TrackData& t) { return t.isAccepted(); }
      );

      // Create new to_process batch
      to_process.clear();
      std::for_each(unsuccessful_tracks.begin(), unsuccessful_tracks.end(), [&] (TrackData& t) {
        std::copy_if(t.clones.begin(), t.clones.end(), std::back_inserter(to_process),
          [&] (TrackData& c) {
            return !isClone(c, successful_tracks) && !isCloneExtended(c, to_process);
          }
        );
      });

      unsuccessful_tracks.clear();
    }
  }

  // create output container, and put selected tracks there
  LHCb::Tracks tracksOutCont;
  // insert selected tracks
  tracksOutCont.reserve(successful_tracks.size());
  for (TrackData& tr: successful_tracks) {
    // make tr release ownership of track
    tracksOutCont.add(tr.trackptr().release());
  }

  if( m_debugLevel ) {
    debug() << "Selected " << successful_tracks.size() << " out of "
            << nTracks << " tracks. Rejected " << m_nGhosts << endmsg;
  }

  return tracksOutCont;
}

void TrackBestTrackCreator::fitAndSelect (
  const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
  const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd
) const {
  if (m_fitTracks.value()) {
    fitAndUpdateCounters(tracksBegin, tracksEnd);

    unsigned i = 0;
    std::for_each(tracksBegin, tracksEnd, [&] (TrackData& trackData) {
      LHCb::Track& track = trackData.track();
      trackData.setAccepted(track.fitStatus() == LHCb::Track::FitStatus::Fitted);

      // Impose tighter restrictions to composite tracks
      if (trackData.isAccepted() && (
        track.type() == LHCb::Track::Types::Long ||
        track.type() == LHCb::Track::Types::Upstream ||
        track.type() == LHCb::Track::Types::Downstream)) {

        auto* fitResult = track.fitResult();
        const LHCb::ChiSquare& chi2 = fitResult->chi2();
        const LHCb::ChiSquare& chi2T = fitResult->chi2Downstream();
        const LHCb::ChiSquare& chi2Velo = fitResult->chi2Velo();

        // note: this includes TT hit contribution
        const LHCb::ChiSquare chi2MatchAndTT = chi2 - chi2T - chi2Velo;

        if( m_debugLevel ) {
          debug() << "Track #" << i << " "
            << chi2.chi2() << " " << chi2.nDoF() << " (" << (chi2.chi2PerDoF() < m_maxChi2DoF ? "1" : "0") << "), "
            << chi2T.chi2() << " " << chi2T.nDoF() << " (" << (chi2T.chi2PerDoF() < m_maxChi2DoFT ? "1" : "0") << "), "
            << chi2Velo.chi2() << " " << chi2Velo.nDoF() << " (" << (chi2Velo.chi2PerDoF() < m_maxChi2DoFVelo ? "1" : "0") << "), "
            << chi2MatchAndTT.chi2() << " " << chi2MatchAndTT.nDoF() << " (" << (chi2MatchAndTT.chi2PerDoF() < m_maxChi2DoFMatchAndTT ? "1" : "0") << "), "
            << track.ghostProbability() << " (" << (track.ghostProbability() < m_maxGhostProb ? "1" : "0") << ") " << endmsg;
        }

        trackData.setAccepted (
          chi2.chi2PerDoF() < m_maxChi2DoF &&
          chi2T.chi2PerDoF() < m_maxChi2DoFT &&
          chi2Velo.chi2PerDoF() < m_maxChi2DoFVelo &&
          chi2MatchAndTT.chi2PerDoF() < m_maxChi2DoFMatchAndTT &&
          (!m_addGhostProb || track.ghostProbability() < m_maxGhostProb));

        if (!trackData.isAccepted()) {
          ++m_nGhosts;
        }
      }

      if( m_debugLevel ) {
        debug() << "Track #" << i++ << (trackData.isAccepted() ? " accepted" : " not accepted") << endmsg;
      }
    });
  }
}

/**
 * @brief      Invokes the fitter and sets some counters.
 */
void TrackBestTrackCreator::fitAndUpdateCounters (
  const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
  const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd
) const {
  std::vector<std::reference_wrapper<LHCb::Track>> fittingTracks;

  std::for_each(tracksBegin, tracksEnd, [&] (TrackData& trackData) {
    LHCb::Track& track = trackData.track();

    // Conditions for fitting a track
    if ((!m_doNotRefit || (track.fitStatus() != LHCb::Track::FitStatus::Fitted && track.fitStatus() != LHCb::Track::FitStatus::FitFailed))
        && (track.nStates()!=0 && !track.checkFlag(LHCb::Track::Flags::Invalid))) {
      fittingTracks.push_back(track);
    }
  });

  m_fitter->operator()(fittingTracks).ignore();

  // Sequential treatment
  std::for_each(tracksBegin, tracksEnd, [&] (TrackData& trackData) {
    LHCb::Track& track = trackData.track();

    bool badinput  = false;
    bool fitfailed = false;
    /// for counters
    std::string prefix = Gaudi::Utils::toString(track.type());
    if (track.checkFlag(LHCb::Track::Flags::Backward)) prefix += "Backward";
    prefix += '.';

    if (m_doNotRefit && (trackData.previousStatus() == LHCb::Track::FitStatus::Fitted
                         || trackData.previousStatus() == LHCb::Track::FitStatus::FitFailed) ){
      if (trackData.previousStatus() == LHCb::Track::FitStatus::Fitted){
        counter("FittedBefore") += 1;
        if (m_addGhostProb) m_ghostTool->execute(track).ignore();
      }
      else {
        /// fit failed before
        /// This should always be 0 as this type is filtered out when initializing the tracks
        counter("FitFailedBefore") += 1;
        track.setFlag( LHCb::Track::Flags::Invalid, true );
      }
    } else {
      if( track.nStates()==0 ||
          track.checkFlag( LHCb::Track::Flags::Invalid ) )  {
        track.setFlag( LHCb::Track::Flags::Invalid, true );
        badinput = true;
      } else {
        // Note: The track has already been fitted
        if (track.fitStatus() == LHCb::Track::FitStatus::Fitted) {
          if (m_addGhostProb) m_ghostTool->execute(track).ignore();
          // Update counters
          if (track.nDoF() > 0) {
            double chisqprob = track.probChi2();
            counter(prefix + "chisqprobSum") += chisqprob;
            counter(prefix + "badChisq") += bool(chisqprob<0.01);
          }
          counter(prefix + "flipCharge") += bool(trackData.qOverP() * track.firstState().qOverP() < 0);
          counter(prefix + "numOutliers") += track.nMeasurementsRemoved();
        } else {
          track.setFlag(LHCb::Track::Flags::Invalid, true);
          fitfailed = true;
          counter(prefix + "FitFailed") += int(fitfailed);
        }
      }
    }
    // stick as close as possible to whatever EventFitter prints
    counter("BadInput")  += int(badinput);
    counter("FitFailed") += int(fitfailed);
  });
}

bool TrackBestTrackCreator::veloOrClones(const TrackData& lhs,
                                         const TrackData& rhs) const
{
  const double fR   = lhs.overlapFraction(rhs, TrackData::VeloR);
#ifndef DEBUGHISTOGRAMS
  if (fR > m_maxOverlapFracVelo) return true;
#endif
  const double fPhi = lhs.overlapFraction(rhs, TrackData::VeloPhi);
#ifdef DEBUGHISTOGRAMS
  if(fR>0) plot1D(fR,"veloROverlapFractionH1",0,1);
  if(fPhi>0) plot1D(fPhi,"veloPhiOverlapFractionH1",0,1);
  return (fR > m_maxOverlapFracVelo) || (fPhi > m_maxOverlapFracVelo);
#else
  return fPhi > m_maxOverlapFracVelo;
#endif
}

bool TrackBestTrackCreator::TClones(const TrackData& lhs,
                                    const TrackData& rhs) const
{
  const double f = lhs.overlapFraction(rhs,TrackData::T);
#ifdef DEBUGHISTOGRAMS
  if(f>0) plot1D(f,"TOverlapFractionH1",0,1);
#endif
  return f > m_maxOverlapFracT;
}

bool TrackBestTrackCreator::TTClones(const TrackData& lhs,
                                     const TrackData& rhs) const
{
  const double f = lhs.overlapFraction(rhs,TrackData::TT);
#ifdef DEBUGHISTOGRAMS
  if(f>0) plot1D(f,"TTOverlapFractionH1",0,1);
#endif
  return f > m_maxOverlapFracTT;
}

bool TrackBestTrackCreator::areClones(const TrackData& it, const TrackData& jt) const
{
  const LHCb::Track &itrack(it.track()), &jtrack(jt.track());
  const int itype(itrack.type()), jtype(jtrack.type());
  const double dqop = it.qOverP() - jt.qOverP();
  const int offset = 256;
  switch (itype + offset * jtype) {
  case LHCb::Track::Types::Long + offset * LHCb::Track::Types::Long:
#ifdef DEBUGHISTOGRAMS
    if (TClones(it, jt) && veloOrClones(it, jt)) {
      plot(dqop, "LLDqopClones", -1e-5, 1e-5);
    } else if (TClones(it, jt)) {
      plot(dqop, "LLDqopTClones", -1e-5, 1e-5);
    } else if (veloOrClones(it, jt)) {
      plot(dqop, "LLDqopVeloOrClones", -1e-5, 1e-5);
    }
#endif
    return TClones(it, jt) &&
      (std::abs(dqop) < m_minLongLongDeltaQoP || veloOrClones(it, jt));
  case LHCb::Track::Types::Long + offset*LHCb::Track::Types::Downstream:
  case LHCb::Track::Types::Downstream + offset*LHCb::Track::Types::Long:
#ifdef DEBUGHISTOGRAMS
    if (TClones(it, jt)) {
      plot(dqop, "DLDqop", -2e-5, 2e-5);
      if (TTClones(it, jt))
        plot(dqop, "DLDqopTTClones", -2e-5, 2e-5);
    }
#endif
    return TClones(it, jt) &&
      (std::abs(dqop) < m_minLongDownstreamDeltaQoP || TTClones(it, jt));
  case LHCb::Track::Types::Downstream + offset*LHCb::Track::Types::Downstream:
    // it seems that there are no down stream tracks that share T hits ...
#ifdef DEBUGHISTOGRAMS
    if(TClones(it, jt)) {
      plot(dqop, "DDDqop", -1e-4, 1e-4);
    }
#endif
    return TClones(it, jt) && TTClones(it, jt);
  case LHCb::Track::Types::Long + offset*LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Upstream + offset*LHCb::Track::Types::Long:
  case LHCb::Track::Types::Upstream + offset*LHCb::Track::Types::Upstream:
    return veloOrClones(it, jt) && TTClones(it, jt);
  case LHCb::Track::Types::Long + offset*LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*LHCb::Track::Types::Long:
  case LHCb::Track::Types::Upstream + offset*LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Velo + offset*LHCb::Track::Types::Velo:
    return veloOrClones(it, jt);
  case LHCb::Track::Types::Long + offset*LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Ttrack + offset*LHCb::Track::Types::Long:
  case LHCb::Track::Types::Downstream + offset*LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Ttrack + offset*LHCb::Track::Types::Downstream:
  case LHCb::Track::Types::Ttrack + offset*LHCb::Track::Types::Ttrack:
    return TClones(it, jt);
  case LHCb::Track::Types::Ttrack + offset*LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Upstream + offset*LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Ttrack + offset*LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*LHCb::Track::Types::Ttrack:
  case LHCb::Track::Types::Downstream + offset*LHCb::Track::Types::Velo:
  case LHCb::Track::Types::Velo + offset*LHCb::Track::Types::Downstream:
  case LHCb::Track::Types::Downstream + offset*LHCb::Track::Types::Upstream:
  case LHCb::Track::Types::Upstream + offset*LHCb::Track::Types::Downstream:
    break;
  default:
    error() << "Don't know how to handle combi: " << itype << " "
            << jtype << endmsg;
  }
  return false;
}

