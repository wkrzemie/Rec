/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TrackContainerCleaner_H_
#define _TrackContainerCleaner_H_

/** @class TrackContainerCleaner TrackContainerCleaner.h
 *
 *  Clean out tracks with some criteria from the container
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include "GaudiAlg/GaudiAlgorithm.h"

#include <string>
#include <vector>

class TrackContainerCleaner: public GaudiAlgorithm {

public:

  // Constructors and destructor
  TrackContainerCleaner(const std::string& name, ISvcLocator* pSvcLocator);

  StatusCode initialize() override;
  StatusCode execute() override;

private:

  std::string m_inputLocation;

  std::string m_selectorName;
  ITrackSelector* m_selector;

};

#endif
