/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKADDNNGHOSTID_H
#define TRACKADDNNGHOSTID_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"


/** @class TrackAddNNGhostId TrackAddNNGhostId.h
 *
 *
 *  @author Johannes Albrecht
 *  @date   2009-10-06
 */
struct IGhostProbability;

class TrackAddNNGhostId : public GaudiAlgorithm {
public:
  /// Standard constructor
  TrackAddNNGhostId( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

private:
  IGhostProbability* m_ghostTool;

  std::string m_inputLocation;
  std::string m_ghostToolName;

};
#endif // TRACKADDNNGHOSTID_H
