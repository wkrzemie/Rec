/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TrackCompetition_H_
#define _TrackCompetition_H_

/** @class TrackCompetition TrackCompetition.h
 *
 *  Copy a container of tracks. By default do not copy tracks that failed the fit
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

#include "Event/Track.h"

class TrackCompetition: public GaudiAlgorithm {

public:

  // Constructor
  TrackCompetition(const std::string& name,
              ISvcLocator* pSvcLocator);

  StatusCode execute() override;

private:

  std::string m_inputLocation;
  std::string m_outputLocation;
  double m_fracUsed;

};

#endif
