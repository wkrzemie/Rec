/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TrackAddLikelihood_H_
#define _TrackAddLikelihood_H_

/** @class TrackAddLikelihood TrackAddLikelihood.h
 *
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>
#include <vector>
#include <map>

class TrackAddLikelihood final : public GaudiAlgorithm {

public:

  TrackAddLikelihood(const std::string& name, ISvcLocator* pSvcLocator);

  StatusCode initialize() override;
  StatusCode execute() override;

private:

  DataObjectReadHandle<LHCb::Tracks> m_input{ this, "inputLocation", LHCb::TrackLocation::Default };
  Gaudi::Property<std::vector<unsigned int>> m_types { this, "types",
                                    { LHCb::Track::History::PatVelo,       LHCb::Track::History::PatVeloTT,
                                      LHCb::Track::History::PatForward,    LHCb::Track::History::TrackMatching,
                                      LHCb::Track::History::PatMatch,      LHCb::Track::History::PatSeeding,
                                      LHCb::Track::History::PatDownstream, LHCb::Track::History::TsaTrack,
                                      LHCb::Track::History::PatVeloGeneral,LHCb::Track::History::PatFastVelo } };
  Gaudi::Property<std::string>  m_likelihoodToolName { this, "LikelihoodTool", "TrackLikelihood" };
  std::map<unsigned int, const ITrackFunctor*> m_toolMap;

};

#endif
