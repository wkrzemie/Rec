/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKPROJECTORS_TRACKPROJECTORSELECTOR_H
#define TRACKPROJECTORS_TRACKPROJECTORSELECTOR_H 1

// Include files
#include <map>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/VectorMap.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackProjectorSelector.h"


/** @class TrackProjectorSelector TrackProjectorSelector.h TrackProjectors/TrackProjectorSelector.h
 *
 *  TrackProjectorSelector decides which projection to use for
 *  a given (type of) measurement
 *
 *  @author Gerhard Raven
 *  @date   2006-06-22
 */
class TrackProjectorSelector : public extends<GaudiTool, ITrackProjectorSelector>
{

public:

  /// Standard constructor
  TrackProjectorSelector( const std::string& type,
                          const std::string& name,
                          const IInterface* parent );

  StatusCode initialize() override;

  ITrackProjector* projector(const LHCb::Measurement&) const override;

private:

  typedef std::map<LHCb::Measurement::Type,std::string> ProjectorNames;
  ProjectorNames m_projNames;

  GaudiUtils::VectorMap<LHCb::Measurement::Type,ITrackProjector*> m_projectors;

};
#endif // TRACKPROJECTORS_TRACKPROJECTORSELECTOR_H
