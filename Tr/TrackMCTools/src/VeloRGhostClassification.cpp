/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/VeloChannelID.h"

#include "VeloRGhostClassification.h"

#include "Event/Track.h"


DECLARE_COMPONENT( VeloRGhostClassification )

using namespace LHCb;

VeloRGhostClassification::VeloRGhostClassification(const std::string& type,
                     const std::string& name,
                     const IInterface* parent):
  TrackGhostClassificationBase(type, name, parent){

}

VeloRGhostClassification::~VeloRGhostClassification(){
  // destructer
}


StatusCode VeloRGhostClassification::specific(LHCbIDs::const_iterator& start,
                                            LHCbIDs::const_iterator& stop,
                                            LHCb::GhostTrackInfo& tinfo) const{

  if (std::distance(start, stop) == 3u){
    LHCb::GhostTrackInfo::LinkPair bMatch = tinfo.bestLink();
    if (bMatch.second < 0.5) tinfo.setClassification(LHCb::GhostTrackInfo::Classification::Combinatoric);
  }

  return StatusCode::SUCCESS;
}
