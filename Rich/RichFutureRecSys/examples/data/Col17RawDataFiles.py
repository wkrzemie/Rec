###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
import glob
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    "/usera/jonesc/NFS/data/Collision17/LHCb/Raw/",   # Cambridge
    #"/usera/jonesc/NFS/data/Collision17/LHCb/Turcal/",   # Cambridge
    #"/usera/jonesc/NFS/data/Collision17/LHCb/Raw/LOWMULT/",
    "/home/chris/LHCb/Data/Collision17/LHCb/Raw/"     # CRJ's CernVM
    ]

data = [ ]
for path in searchPaths :
    # all
    files = sorted(glob.glob(path+"*/*.raw"))
    # mag down
    #files = sorted(glob.glob(path+"195244/*.raw"))
    # mag up
    #files = sorted(glob.glob(path+"194248/*.raw"))
    data += [ "DATAFILE='"+file+"'" for file in files ]

IOHelper('MDF').inputFiles( data, clear=True )
FileCatalog().Catalogs = [ 'xmlcatalog_file:out.xml' ]

from Configurables import Brunel, LHCbApp

Brunel().DataType  = "2017"
Brunel().Simulation = False
Brunel().WithMC     = False

#Brunel().DDDBtag   = "dddb-20150724"
#Brunel().CondDBtag = "cond-20161011"

#from Configurables import CondDB

#CondDB().Tags['LHCBCOND'] = 'cond-20170510-anita-MDCS'
#CondDB().Tags['LHCBCOND'] = 'cond-20170510-anita2-MDCS'
#CondDB().Tags['LHCBCOND'] = 'cond-20170510-anita-mod-MDCS'
#CondDB().Tags['LHCBCOND'] = 'cond-20170510-chris-MDCS'
