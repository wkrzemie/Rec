###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

importOptions("$APPCONFIGOPTS/Brunel/MC-WithTruth.py")

#importOptions("$BRUNELROOT/tests/options/testBrunel-defaults.py")

from Configurables import TrackBestTrackCreator, TrackVectorFitter
TrackBestTrackCreator().addTool(TrackVectorFitter, 'Fitter')

from Configurables import Brunel
Brunel().DataType = "Upgrade"

from PRConfig import TestFileDB
TestFileDB.test_file_db['upgrade-baseline-FT61-digi'].run()

Brunel().EvtMax      = 1
Brunel().PrintFreq   = 1

Brunel().OutputType = 'NONE'
#Brunel().FilterTrackStates = False

msgSvc = getConfigurable("MessageSvc")
#msgSvc.setVerbose += [ "DeRichGasRadiator" ]
#msgSvc.setVerbose += [ "DeRichSystem" ]
#msgSvc.setVerbose += [ "DeRichHPD" ]
msgSvc.Format = "% F%30W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 30

