/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <numeric>
#include <tuple>

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

namespace Rich::Future::Rec
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace
  {
    /// Output data type
    using OutData = std::tuple< Summary::Track::Vector, Summary::Pixel::Vector >;
  } // namespace

  /** @class RecoSummary RichGlobalPIDRecoSummary.h
   *
   *  Forms a summary of the reconstruction, to be used by the likelihood
   *  minimisation. Builds the various cross links required to make the
   *  calculation efficient.
   *
   *  @author Chris Jones
   *  @date   2016-10-19
   */

  class SIMDRecoSummary final
    : public MultiTransformer< OutData( const LHCb::RichTrackSegment::Vector &,
                                        const Relations::TrackToSegments::Vector &,
                                        const Relations::PhotonToParents::Vector &,
                                        const PhotonYields::Vector &,
                                        const PhotonYields::Vector &,
                                        const SIMDPhotonSignals::Vector &,
                                        const SIMDPixelSummaries & ),
                               Traits::BaseClass_t< AlgBase > >
  {

  public:

    /// Standard constructor
    SIMDRecoSummary( const std::string &name, ISvcLocator *pSvcLocator );

  public:

    /// Functional operator
    OutData operator()( const LHCb::RichTrackSegment::Vector &    segments,
                        const Relations::TrackToSegments::Vector &tkToSegs,
                        const Relations::PhotonToParents::Vector &photToSegPix,
                        const PhotonYields::Vector &              detYields,
                        const PhotonYields::Vector &              sigYields,
                        const SIMDPhotonSignals::Vector &         expPhotSigs,
                        const SIMDPixelSummaries &                pixels ) const override;

  private:

    /// The SIMD float type
    using SIMDFP = SIMD::FP< SIMD::DefaultScalarFP >;
  };

} // namespace Rich::Future::Rec
