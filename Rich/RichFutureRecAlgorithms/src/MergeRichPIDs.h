/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <memory>
#include <numeric>

// from Gaudi
#include "GaudiAlg/MergingTransformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event
#include "Event/RichPID.h"

namespace Rich::Future::Rec
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class MergeRichPIDs MergeRichPIDs.h
   *
   *  Merges RichPID objects from multiple containers into one.
   *
   *  @author Chris Jones
   *  @date   2016-11-05
   */

  class MergeRichPIDs final
    : public MergingTransformer< LHCb::RichPIDs( const vector_of_const_< LHCb::RichPIDs > & ),
                                 Traits::BaseClass_t< AlgBase > >
  {

  public:

    /// Standard constructor
    MergeRichPIDs( const std::string &name, ISvcLocator *pSvcLocator );

  public:

    /// Functional operator
    LHCb::RichPIDs operator()( const vector_of_const_< LHCb::RichPIDs > &inPIDs ) const override;

  private:

    /// Should the original RichPID be preserved in the cloned container ?
    Gaudi::Property< bool > m_keepPIDKey { this, "PreserveOriginalKeys", true };

    /// Data object version for RichPID container
    Gaudi::Property< unsigned short int > m_pidVersion { this,
                                                         "PIDVersion",
                                                         2u,
                                                         "Version of the RichPID container" };
  };

} // namespace Rich::Future::Rec
