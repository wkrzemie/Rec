/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecTupleToolBase.cpp
 *
 *  Implementation file for RICH reconstruction tool base class : RichRecTupleToolBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005/01/13
 */
//-----------------------------------------------------------------------------

// local
#include "RichFutureRecBase/RichRecTupleToolBase.h"

// ============================================================================
// Force creation of templated class
#include "RichRecBase.icpp"
template class Rich::Future::Rec::CommonBase< Rich::Future::TupleToolBase >;
// ============================================================================

// ============================================================================
// Standard constructor
// ============================================================================
Rich::Future::Rec::TupleToolBase::TupleToolBase( const std::string &type,
                                                 const std::string &name,
                                                 const IInterface * parent )
  : Rich::Future::TupleToolBase( type, name, parent )
  , Rich::Future::Rec::CommonBase< Rich::Future::TupleToolBase >( this )
{}
// ============================================================================

// ============================================================================
// Initialise
// ============================================================================
StatusCode
Rich::Future::Rec::TupleToolBase::initialize()
{
  // Initialise base class
  const auto sc = Rich::Future::TupleToolBase::initialize();
  if ( !sc ) return Error( "Failed to initialise Rich::TupleToolBase", sc );

  // Common initialisation
  return initialiseRichReco();
}
// ============================================================================

// ============================================================================
// Finalise
// ============================================================================
StatusCode
Rich::Future::Rec::TupleToolBase::finalize()
{
  // Common finalisation
  const auto sc = finaliseRichReco();
  if ( !sc ) return Error( "Failed to finalise Rich::RecBase", sc );

  // base class finalize
  return Rich::Future::TupleToolBase::finalize();
}
// ============================================================================
