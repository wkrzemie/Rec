###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: RichFutureRecBase
################################################################################
gaudi_subdir(RichFutureRecBase v1r0)

gaudi_depends_on_subdirs(Det/RichDet
                         Event/RecEvent
                         Rich/RichFutureKernel
                         Rich/RichFutureRecEvent
                         Rich/RichRecUtils)
find_package(Boost)
find_package(ROOT)
find_package(Vc)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                           ${Vc_INCLUDE_DIR})

gaudi_add_library(RichFutureRecBase
                  src/*.cpp
                  PUBLIC_HEADERS RichFutureRecBase
                  LINK_LIBRARIES RichDetLib RecEvent RichFutureKernel RichFutureRecEvent RichRecUtils)

#gaudi_add_dictionary(RichFutureRecBase
#                     dict/RichFutureRecBaseDict.h
#                     dict/RichFutureRecBaseDict.xml
#                     LINK_LIBRARIES RichDetLib RecEvent RichFutureKernel RichRecUtils RichRecEvent RichRecBase
#                     OPTIONS "-U__MINGW32__ -DBOOST_DISABLE_ASSERTS")
