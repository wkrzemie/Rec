/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichGeomEffCKMassRings.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

GeomEffCKMassRings::GeomEffCKMassRings( const std::string &name, ISvcLocator *pSvcLocator )
  : MultiTransformer(
      name,
      pSvcLocator,
      { KeyValue { "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
        KeyValue { "CherenkovAnglesLocation", CherenkovAnglesLocation::Emitted },
        KeyValue { "MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted } },
      { KeyValue { "GeomEffsLocation", GeomEffsLocation::Default },
        KeyValue { "GeomEffsPerPDLocation", GeomEffsPerPDLocation::Default },
        KeyValue { "SegmentPhotonFlagsLocation", SegmentPhotonFlagsLocation::Default } } )
{}

//=============================================================================

OutData
GeomEffCKMassRings::operator()( const LHCb::RichTrackSegment::Vector &segments,
                                const CherenkovAngles::Vector &       ckAngles,
                                const MassHypoRingsVector &           massRings ) const
{
  // Scalar type
  using ScType = GeomEffs::Type;

  // make the data to return
  OutData data;
  auto &  geomEffsV      = std::get< GeomEffs::Vector >( data );
  auto &  geomEffsPerPDV = std::get< GeomEffsPerPDVector >( data );
  auto &  segPhotFlags   = std::get< SegmentPhotonFlags::Vector >( data );

  // reserve sizes
  geomEffsV.reserve( ckAngles.size() );
  geomEffsPerPDV.reserve( ckAngles.size() );
  segPhotFlags.reserve( ckAngles.size() );

  // iterate over input data
  for ( const auto &&[segment, ckAngles, massRings] :
        Ranges::ConstZip( segments, ckAngles, massRings ) )
  {

    // make new objects for the geom effs
    geomEffsV.emplace_back();
    auto &geomEffs = geomEffsV.back();
    geomEffsPerPDV.emplace_back();
    auto &geomEffsPerPD = geomEffsPerPDV.back();

    // make entry for segment photon flags
    segPhotFlags.emplace_back();
    auto &photFlags = segPhotFlags.back();

    // Which rich
    const auto rich = segment.rich();

    // Loop over (real) PID types (Below Threshold excluded).
    for ( const auto id : activeParticlesNoBT() )
    {
      // The efficiency
      ScType eff = 0;

      // above threshold ?
      if ( ckAngles[id] > 0 )
      {
        // count the number of detected points
        std::size_t nDetect = 0;

        // number of points
        const auto nPoints = massRings[id].size();

        // PD increment
        const auto pdInc = 1.0f / static_cast< float >( nPoints );

        // data for this ID
        auto &idData = geomEffsPerPD[id];

        // guess at reserve size for # PDs associated to a single CK ring.
        idData.reserve( rich == Rich::Rich1 ? 25 : 30 );

        // Last PD filled
        LHCb::RichSmartID lastPDID;

        // Loop over the points of the mass ring
        for ( const auto &P : massRings[id] )
        {
          if ( RayTracedCKRingPoint::InHPDTube == P.acceptance() )
          {
            // count detected photons
            ++nDetect;

            // The HPD ID
            const auto pdID = P.smartID().pdID();

            // Same PD as last time ?
            if ( lastPDID == pdID )
            {
              // just increment the back() entry
              idData.back().second += pdInc;
            }
            else
            {
              // No, so add a new entry
              idData.emplace_back( pdID, pdInc );
              // update cached last PD ID
              lastPDID = pdID;
            }

            // Segment photon flags
            photFlags.setInAcc( rich, P.globalPosition() );
          }
        }

        // compute the final eff
        eff = static_cast< ScType >( nDetect ) / static_cast< ScType >( nPoints );
      }

      // save the final eff
      geomEffs.setData( id, eff );
    }
  }

  // return the new data
  return data;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GeomEffCKMassRings )

//=============================================================================
