/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichTrackFunctionalCherenkovResolutions.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

TrackFunctionalCherenkovResolutions::TrackFunctionalCherenkovResolutions( const std::string &name,
                                                                          ISvcLocator *pSvcLocator )
  : Transformer(
      name,
      pSvcLocator,
      { KeyValue { "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
        KeyValue { "SignalCherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
        KeyValue { "MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted } },
      { KeyValue { "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default } } )
{
  // debug
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode
TrackFunctionalCherenkovResolutions::initialize()
{
  // Sets up various tools and services
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // services
  m_transSvc = svc< ITransportSvc >( "TransportSvc", true );
  if ( m_useAltGeom ) { m_altGeom = getDet< IDetectorElement >( m_altGeomLoc ); }

  // Compute for each radiator the radiator radiation length / unit path length

  // Create accelerator cache for the transport service
  auto tsCache = m_transSvc->createCache();

  // Fixed vector start/end points for each radiator
  const RadiatorArray< std::pair< Gaudi::XYZPoint, Gaudi::XYZPoint > > radVs { {
    { { 150, 150, 1110 }, { 150, 150, 1170 } },  // Aero (not used in practise)
    { { 150, 150, 1500 }, { 150, 150, 1700 } },  // RICH1 gas
    { { 500, 500, 10000 }, { 500, 500, 11000 } } // RICH2 gas
  } };
  // Use the fixed vectors to compute from the TS the radiation length / mm
  // in each RICH radiator medium
  for ( const auto rad : Rich::radiators() )
  {
    if ( m_cacheRadLenP[rad] && !m_useTSForMS[rad] )
    {
      const auto effL = m_transSvc->distanceInRadUnits_r(
        radVs[rad].first, radVs[rad].second, tsCache, 0, altGeom() );
      const auto length     = std::sqrt( ( radVs[rad].second - radVs[rad].first ).mag2() );
      m_radLenPerUnitL[rad] = ( length > 0 ? effL / length : 0.0 );
      _ri_debug << std::setprecision( 9 ) << rad
                << " radiation length / mm = " << m_radLenPerUnitL[rad] << endmsg;
    }
  }

  // return
  return sc;
}

//=============================================================================

CherenkovResolutions::Vector
TrackFunctionalCherenkovResolutions::operator()( const LHCb::RichTrackSegment::Vector &segments,
                                                 const CherenkovAngles::Vector &       ckAngles,
                                                 const MassHypoRingsVector &massRings ) const
{
  using namespace Gaudi::Units;

  // data to return
  CherenkovResolutions::Vector resV;
  resV.reserve( segments.size() );

  // Create accelerator cache for the transport service
  auto tsCache = m_transSvc->createCache();

  // Loop over input segment data
  for ( const auto &&[segment, angles, rings] : Ranges::ConstZip( segments, ckAngles, massRings ) )
  {
    // Add a resolution entry for this segment
    resV.emplace_back();
    auto &res = resV.back();

    // Is the lowest mass hypo for this track above threshold
    const bool aboveThreshold = ( angles[lightestActiveHypo()] > 0 );

    // momentum for this segment, in GeV units
    const auto ptot = segment.bestMomentumMag() / GeV;

    // check if segment is valid
    if ( aboveThreshold && ptot > 0 )
    {

      // Which radiator
      const auto rad = segment.radiator();

      // cache 1 / momentum
      const auto ptotInv = 1.0 / ptot;

      // the res^2 to fill
      double res2 = 0;

      // Start with the mass hypo independent bits

      //-------------------------------------------------------------------------------
      // multiple scattering
      //-------------------------------------------------------------------------------
      double effL = 0;
      if ( !m_useTSForMS[rad] )
      {
        // compute from the cached parameter
        effL = m_radLenPerUnitL[rad] * segment.pathLength();
      }
      else
      {
        // Full TS treatment ...
        try
        {
          effL = m_transSvc->distanceInRadUnits_r(
            segment.entryPoint(), segment.exitPoint(), tsCache, 0, altGeom() );
        }
        catch ( const TransportSvcException &excpt )
        {
          effL = 0;
          Warning( "Problem computing radiation length : " + excpt.message() ).ignore();
        }
      }
      // compute the scattering coefficient from the radiation length
      if ( effL > 0 )
      {
        const auto multScattCoeff =
          ( m_scatt * std::sqrt( effL ) * ( 1.0 + 0.038 * Rich::Maths::fast_log( effL ) ) );
        const auto scatCOvP = multScattCoeff * ptotInv;
        const auto scattErr = 2.0 * scatCOvP * scatCOvP;
        //_ri_debug << std::setprecision(9) << " Scattering error = " << scattErr << endmsg;
        res2 += scattErr;
      }

      //-------------------------------------------------------------------------------
      // track curvature in the radiator volume
      //-------------------------------------------------------------------------------
      const auto curvErr =
        ( Rich::Aerogel == rad ?
            0 :
            std::pow( Rich::Geom::AngleBetween( segment.entryMomentum(), segment.exitMomentum() ) *
                        0.25,
                      2 ) );
      //_ri_debug << std::setprecision(9) << " Curvature error = " << curvErr << endmsg;
      res2 += curvErr;

      //-------------------------------------------------------------------------------

      //-------------------------------------------------------------------------------
      // tracking direction errors
      //-------------------------------------------------------------------------------
      const auto dirErr = segment.entryErrors().errTX2() + segment.entryErrors().errTY2();
      //_ri_debug << std::setprecision(9) << " Track direction error = " << dirErr << endmsg;
      res2 += dirErr;
      //-------------------------------------------------------------------------------

      // Loop over active hypos ( note including BT here )
      for ( const auto hypo : activeParticles() )
      {
        // start with the hypo independent part
        double hypo_res2 = res2;

        //-------------------------------------------------------------------------------
        // RICH contributions (pixel, PSF errors etc...)
        // Uses the supplied reference contribution for the given pixel area, and scales
        // according to the area for the associated PD
        //-------------------------------------------------------------------------------
        auto pdErr = m_pdErr[rad] * m_pdErr[rad];
        // Are we using the full treatment to deal with PD size difference ?
        if ( UNLIKELY( m_fullPDAreaTreatment[rad] && Rich::BelowThreshold != hypo ) )
        {
          // loop over the ring points for this hypo
          unsigned int totalInPD { 0 };
          double       totalSize { 0.0 };
          if ( !rings[hypo].empty() )
          {
            for ( const auto &P : rings[hypo] )
            {
              if ( RayTracedCKRingPoint::InHPDTube == P.acceptance() )
              {
                // Check PD pointer
                if ( P.photonDetector() )
                {
                  // Count accepted points
                  ++totalInPD;
                  // sum up pixel size
                  totalSize += P.photonDetector()->effectivePixelArea();
                }
                else
                {
                  Warning( "NULL DeRichPD pointer!!" ).ignore();
                }
              }
            }
            // Scale PD errors by average pixel size using reference size
            if ( totalInPD > 0 )
            {
              totalSize /= (double)totalInPD;
              pdErr *= totalSize / m_pdRefArea[rad];
            }
          }
        }
        //_ri_debug << std::setprecision(9) << " HPD error = " << pdErr << endmsg;
        hypo_res2 += pdErr;

        // Expected CK theta
        if ( Rich::BelowThreshold != hypo && angles[hypo] > 1e-6 )
        {

          // cache tan(cktheta)
          const auto tanCkExp = Rich::Maths::fast_tan( angles[hypo] );
          // 1 /  tan(cktheta)
          const auto tanCkExpInv = 1.0 / tanCkExp;
          //_ri_debug << std::setprecision(9) << "  " << hypo
          //          << " ckExp = " << angles[hypo]
          //          << " tanCkExp = " << tanCkExp << endmsg;

          //-------------------------------------------------------------------------------
          // chromatic error
          //-------------------------------------------------------------------------------
          const auto index     = m_refIndex.get()->refractiveIndex( segment.radIntersections(),
                                                                segment.avPhotonEnergy() );
          const auto chromFact = ( index > 0 ? m_detParams.get()->refIndexSD( rad ) / index : 0.0 );
          const auto chromatErr   = chromFact * tanCkExpInv;
          const auto chromatErrSq = chromatErr * chromatErr;
          //_ri_debug << std::setprecision(9) << "         Chromatic err = " << chromatErr <<
          // endmsg;
          hypo_res2 += chromatErrSq;
          //-------------------------------------------------------------------------------

          //-------------------------------------------------------------------------------
          // momentum error
          //-------------------------------------------------------------------------------
          constexpr auto GeVSqInv   = 1.0 / (double)( GeV * GeV );
          const auto     mass2      = richPartProps()->massSq( hypo ) * GeVSqInv;
          const auto     massFactor = mass2 / ( mass2 + ( ptot * ptot ) );
          const auto     A          = massFactor * ptotInv * tanCkExpInv;
          const auto     momErrSq   = ( ( segment.entryErrors().errP2() * GeVSqInv ) * A * A );
          //_ri_debug << std::setprecision(9) << "         momentum err = " << momErrSq << endmsg;
          hypo_res2 += momErrSq;
          //-------------------------------------------------------------------------------

          //-------------------------------------------------------------------------------
          // Global Scale factor
          //-------------------------------------------------------------------------------
          hypo_res2 *= m_scale[rad] * m_scale[rad];
          //-------------------------------------------------------------------------------

        } // theta > 0

        // Compute the final resolution
        const auto ckRes = std::min( (float)std::sqrt( hypo_res2 ), m_maxRes[rad] );
        //_ri_debug << std::setprecision(9) << "           -> Final error^2 " << ckRes << endmsg;
        res.setData( hypo, ckRes );

      } // hypo loop
    }
  }

  return resV;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackFunctionalCherenkovResolutions )

//=============================================================================
