/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <algorithm>
#include <cmath>
#include <iomanip>

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Utils
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Interfaces
#include "RichInterfaces/IRichRefractiveIndex.h"

namespace Rich::Future::Rec
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class TrackCherenkovAnglesBase RichTrackCherenkovAngles.h
   *
   *  Computes the expected Cherenkov angles for the given track
   *  segments and photon spectra data.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackCherenkovAnglesBase : public AlgBase
  {

  public:

    /// Standard constructor
    TrackCherenkovAnglesBase( const std::string &name, ISvcLocator *pSvcLocator )
      : AlgBase( name, pSvcLocator )
    {}

  protected:

    /// Algorithm execution
    CherenkovAngles::Vector run( const LHCb::RichTrackSegment::Vector &segments,
                                 const PhotonSpectra::Vector &         tkSpectra,
                                 const PhotonYields::Vector &          tkYields ) const;

  private:

    /// Pointer to general refractive index tool
    ToolHandle< const IRefractiveIndex > m_refIndex {
      this,
      "RefIndex",
      "Rich::Future::TabulatedRefractiveIndex/RefIndex"
    };
  };

  /** @class TrackEmittedCherenkovAngles RichTrackCherenkovAngles.h
   *
   *  Functional implementation using emitted photon spectra.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackEmittedCherenkovAngles final
    : public Transformer< CherenkovAngles::Vector( const LHCb::RichTrackSegment::Vector &,
                                                   const PhotonSpectra::Vector &,
                                                   const PhotonYields::Vector & ),
                          Traits::BaseClass_t< TrackCherenkovAnglesBase > >
  {
  public:

    /// Constructor
    TrackEmittedCherenkovAngles( const std::string &name, ISvcLocator *pSvcLocator )
      : Transformer(
          name,
          pSvcLocator,
          { KeyValue { "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
            KeyValue { "EmittedPhotonSpectraLocation", PhotonSpectraLocation::Emitted },
            KeyValue { "EmittedPhotonYieldLocation", PhotonYieldsLocation::Emitted } },
          { KeyValue { "EmittedCherenkovAnglesLocation", CherenkovAnglesLocation::Emitted } } )
    {
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }
    /// Algorithm execution via transform
    CherenkovAngles::Vector operator()( const LHCb::RichTrackSegment::Vector &segments,
                                        const PhotonSpectra::Vector &         tkSpectra,
                                        const PhotonYields::Vector &tkYields ) const override
    {
      return run( segments, tkSpectra, tkYields );
    }
  };

  /** @class TrackSignalCherenkovAngles RichTrackCherenkovAngles.h
   *
   *  Functional implementation using emitted photon spectra.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackSignalCherenkovAngles final
    : public Transformer< CherenkovAngles::Vector( const LHCb::RichTrackSegment::Vector &,
                                                   const PhotonSpectra::Vector &,
                                                   const PhotonYields::Vector & ),
                          Traits::BaseClass_t< TrackCherenkovAnglesBase > >
  {
  public:

    /// Constructor
    TrackSignalCherenkovAngles( const std::string &name, ISvcLocator *pSvcLocator )
      : Transformer(
          name,
          pSvcLocator,
          { KeyValue { "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
            KeyValue { "SignalPhotonSpectraLocation", PhotonSpectraLocation::Signal },
            KeyValue { "SignalPhotonYieldLocation", PhotonYieldsLocation::Signal } },
          { KeyValue { "SignalCherenkovAnglesLocation", CherenkovAnglesLocation::Signal } } )
    {
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }
    /// Algorithm execution via transform
    CherenkovAngles::Vector operator()( const LHCb::RichTrackSegment::Vector &segments,
                                        const PhotonSpectra::Vector &         tkSpectra,
                                        const PhotonYields::Vector &tkYields ) const override
    {
      return run( segments, tkSpectra, tkYields );
    }
  };

} // namespace Rich::Future::Rec
