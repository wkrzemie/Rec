/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichTrackFilter.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

TrackFilter::TrackFilter( const std::string &name, ISvcLocator *pSvcLocator )
  : MultiTransformer(
      name,
      pSvcLocator,
      { KeyValue { "InTracksLocation", LHCb::TrackLocation::Default } },
      { KeyValue { "OutLongTracksLocation", LHCb::TrackLocation::Default + "RichLong" },
        KeyValue { "OutDownTracksLocation", LHCb::TrackLocation::Default + "RichDown" },
        KeyValue { "OutUpTracksLocation", LHCb::TrackLocation::Default + "RichUp" } } )
{
  // Debug messages
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

OutData
TrackFilter::operator()( const LHCb::Tracks &tracks ) const
{
  OutData filteredTks;
  auto &[longTks, downTks, upTks] = filteredTks;

  _ri_debug << "Found " << tracks.size() << " tracks" << endmsg;

  for ( const auto *tk : tracks )
  {
    _ri_verbo << " -> type " << tk->type() << endmsg;
    switch ( tk->type() )
    {
      case LHCb::Track::Types::Long: longTks.insert( tk ); break;
      case LHCb::Track::Types::Downstream: downTks.insert( tk ); break;
      case LHCb::Track::Types::Upstream: upTks.insert( tk ); break;
      default: break;
    }
  }

  _ri_debug << "Selected " << longTks.size() << " Long, " << downTks.size() << " Downstream and "
            << upTks.size() << " Upstream tracks" << endmsg;

  return filteredTks;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackFilter )

//=============================================================================
