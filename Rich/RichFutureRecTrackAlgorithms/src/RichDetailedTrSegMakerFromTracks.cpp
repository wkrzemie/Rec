/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Array properties
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// local
#include "RichDetailedTrSegMakerFromTracks.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

// pull in methods from Rich::RayTracingUtils
using namespace Rich::RayTracingUtils;

//-----------------------------------------------------------------------------
// Implementation file for class : DetailedTrSegMakerFromTracks
//
// 2016-09-30 : Chris Jones
//-----------------------------------------------------------------------------

DetailedTrSegMakerFromTracks::DetailedTrSegMakerFromTracks( const std::string &name,
                                                            ISvcLocator *      pSvcLocator )
  : MultiTransformer(
      name,
      pSvcLocator,
      { KeyValue { "TracksLocation", LHCb::TrackLocation::Default } },
      { KeyValue { "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
        KeyValue { "TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Initial },
        KeyValue { "SegmentToTrackLocation", Relations::SegmentToTrackLocation::Default } } )
{
  // init
  m_rich.fill( nullptr );

  // Debug messages
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================
// Initialisation.
//=============================================================================
StatusCode
DetailedTrSegMakerFromTracks::initialize()
{
  // Sets up various tools and services
  auto sc = MultiTransformer::initialize();
  if ( !sc ) return sc;

  // If not using extrapolator, disable it to avoid auto-loading
  if ( m_useStateProvider ) { m_trExt.disable(); }

  // If state provider not required, disable it to avoid auto-loading
  if ( !m_createMissingStates && !m_useStateProvider ) { m_trStateP.disable(); }

  // load MagneticFieldSvc
  m_magFieldSvc = svc< IMagneticFieldSvc >( "MagneticFieldSvc" );

  // get Detector elements for RICH1 and RICH2
  m_rich[Rich::Rich1] = getDet< DeRich >( DeRichLocations::Rich1 );
  m_rich[Rich::Rich2] = getDet< DeRich >( DeRichLocations::Rich2 );

  // Radiators
  if ( usedRads( Rich::Rich1Gas ) )
  { m_radiators.push_back( getDet< DeRichRadiator >( DeRichLocations::Rich1Gas ) ); }
  if ( usedRads( Rich::Rich2Gas ) )
  { m_radiators.push_back( getDet< DeRichRadiator >( DeRichLocations::Rich2Gas ) ); }

  for ( const auto rad : Rich::radiators() )
  { m_minRadLengthSq[rad] = std::pow( m_minRadLength[rad], 2 ); }
  if ( m_useStateProvider )
  { _ri_debug << "Will use StateProvider instead of extrapolator to move states" << endmsg; }

  if ( m_extrapFromRef )
  { _ri_debug << "Will perform all track extrapolations from reference states" << endmsg; }

  _ri_debug << "Min radiator path lengths (aero/R1Gas/R2Gas) : " << m_minRadLength << " mm "
            << endmsg;

  if ( UNLIKELY( m_createMissingStates ) )
  {
    Warning( "Will create missing track states using the StateProvider tool. "
             "If triggered, this will be very slow.",
             StatusCode::SUCCESS )
      .ignore();
  }

  // Preload Geometry ?
  if ( UNLIKELY( m_preload ) )
  {
    auto preloadTool = tool< IGenericTool >( "PreloadGeometryTool" );
    preloadTool->execute();
    release( preloadTool );
  }

  return sc;
}

//=============================================================================

OutData
DetailedTrSegMakerFromTracks::operator()( const InData &tracks ) const
{
  _ri_debug << "Found " << tracks.size() << " tracks" << endmsg;

  // container to return
  OutData data;

  // shortcuts to tuple contents
  auto &segments    = std::get< LHCb::RichTrackSegment::Vector >( data );
  auto &tkToSegsRel = std::get< Relations::TrackToSegments::Vector >( data );
  auto &segToTkRel  = std::get< Relations::SegmentToTrackVector >( data );

  // GEC cut on the number of input tracks
  if ( tracks.size() <= m_maxTracks )
  {

    // reserve sizes for output containers
    const auto numSegs = tracks.size() * 2; // guess at # segments
    segments.reserve( numSegs );
    segToTkRel.reserve( numSegs );
    tkToSegsRel.reserve( tracks.size() );

    // Loop over the input tracks
    LHCb::Tracks::size_type tkIndex( 0 );
    for ( const auto *track : tracks )
    {
      if ( track ) { constructSegments( track, segments, tkIndex, tkToSegsRel, segToTkRel ); }
      ++tkIndex; // must increment for every track in input
    }
  }
  else
  {
    Warning( "Too many tracks (>" + std::to_string( m_maxTracks.value() ) +
               "). Processing aborted.",
             StatusCode::SUCCESS,
             0 )
      .ignore();
  }

  // return the final data
  _ri_debug << "Created " << segments.size() << " track segments" << endmsg;
  return data;
}

//=============================================================================

void
DetailedTrSegMakerFromTracks::constructSegments( const LHCb::Track *                 track,
                                                 LHCb::RichTrackSegment::Vector &    segments,
                                                 const LHCb::Tracks::size_type       tkIndex,
                                                 Relations::TrackToSegments::Vector &tkToSegsRel,
                                                 Relations::SegmentToTrackVector &segToTkRel ) const
{
  if ( msgLevel( MSG::DEBUG ) )
  {
    debug() << "Analysing Track key=" << track->key() << " history=" << track->history() << " : "
            << track->states().size() << " States at z =";
    for ( const auto *S : track->states() ) { debug() << " " << S->z(); }
    debug() << endmsg;
  }

  // Optional 'missing' track states, for start(0) and end(1) points
  boost::optional< LHCb::State > missing_states[2];

  // relations for this track. must be 1 to 1, regardless of how many
  // segments are created for it.
  tkToSegsRel.emplace_back( track->key(), tkIndex );
  auto &tkRels  = tkToSegsRel.back();
  auto &segList = tkRels.segmentIndices;

  // Loop over all radiators
  for ( const auto *radiator : m_radiators )
  {

    // which radiator
    const auto rad = radiator->radiatorID();
    _ri_verbo << " Considering radiator " << rad << endmsg;

    // skip rad ?
    if ( UNLIKELY( skipByType( track, rad ) ) ) { continue; }

    // choose appropriate z start position for initial track states for this radiator
    const auto zStart = ( Rich::Rich2Gas == rad ? m_nomZstates[2] : m_nomZstates[0] );

    // Get the track entry state points
    const auto *entryPStateRaw = &( track->closestState( zStart ) );
    if ( !entryPStateRaw )
    {
      Error( "Problem getting track state" ).ignore();
      continue;
    }

    // check tolerance
    _ri_verbo << " -> Closest Entry State at z=" << entryPStateRaw->z() << "mm" << endmsg;
    const auto entryTol = zStart - entryPStateRaw->z();
    if ( fabs( entryTol ) > m_zTolerance[rad] )
    {
      _ri_verbo << "  -> Entry State : Requested z=" << zStart << " found z=" << entryPStateRaw->z()
                << " failed tolerance check dz=" << m_zTolerance[rad] << endmsg;
      entryPStateRaw = nullptr;
    }

    // Failed to find the state, so try with the state provider....
    if ( UNLIKELY( !entryPStateRaw && m_createMissingStates ) )
    {
      Warning( "Creating missing track state at z=" + std::to_string( zStart ) + "mm",
               StatusCode::SUCCESS,
               3 )
        .ignore();
      missing_states[0].emplace();
      const auto sc = m_trStateP.get()->state( missing_states[0].get(), *track, zStart );
      if ( sc )
      {
        entryPStateRaw = &missing_states[0].get();
        _ri_debug << "   -> Found entry state at z=" << zStart << "mm via StateProvider" << endmsg;
      }
      else
      {
        _ri_verbo << "   -> Failed to get entry State at z=" << zStart << "mm via StateProvider"
                  << endmsg;
      }
    }
    // if still no state, skip this track
    if ( UNLIKELY( !entryPStateRaw ) )
    {
      std::ostringstream mess;
      mess << "Failed to find " << rad << " entry state near z=" << zStart << "mm";
      Warning( mess.str() ).ignore();
      continue;
    }

    // check above electron threshold
    if ( richPartProps()->thresholdMomentum( Rich::Electron, rad ) > entryPStateRaw->p() )
    {
      _ri_verbo << "  -> Below electron cherenkov threshold -> reject" << endmsg;
      continue;
    }

    // choose appropriate z end position for initial track states for this radiator
    const auto zEnd = ( Rich::Rich2Gas == rad ? m_nomZstates[3] : m_nomZstates[1] );

    // Get the track exit state points
    const auto *exitPStateRaw = &( track->closestState( zEnd ) );
    if ( !exitPStateRaw )
    {
      Error( "Problem getting track state" ).ignore();
      continue;
    }

    // check tolerance
    _ri_verbo << " -> Closest Exit State at  z=" << exitPStateRaw->z() << "mm" << endmsg;
    const auto exitTol = zEnd - exitPStateRaw->z();
    if ( fabs( exitTol ) > m_zTolerance[rad] )
    {
      _ri_verbo << "  -> Exit State  : Requested z=" << zEnd << " found z=" << exitPStateRaw->z()
                << " failed tolerance check dz=" << m_zTolerance[rad] << endmsg;
      exitPStateRaw = nullptr;
    }

    // Failed to find the state, so try with the state provider....
    if ( UNLIKELY( !exitPStateRaw && m_createMissingStates ) )
    {
      Warning( "Creating missing track state at z=" + std::to_string( zEnd ) + "mm",
               StatusCode::SUCCESS,
               3 )
        .ignore();
      missing_states[1].emplace();
      const auto sc = m_trStateP.get()->state( missing_states[1].get(), *track, zEnd );
      if ( sc )
      {
        exitPStateRaw = &missing_states[1].get();
        _ri_debug << "    -> Found exit state at z=" << zEnd << "mm via StateProvider" << endmsg;
      }
      else
      {
        _ri_verbo << "    -> Failed to get exit State at z=" << zEnd << "mm via StateProvider"
                  << endmsg;
      }
    }
    // if still no state, skip this track
    if ( UNLIKELY( !exitPStateRaw ) )
    {
      std::ostringstream mess;
      mess << "Failed to find " << rad << " exit state near z=" << zEnd << "mm";
      Warning( mess.str() ).ignore();
      continue;
    }

    // Check for strange states
    if ( UNLIKELY( m_checkStates ) )
    {
      checkState( entryPStateRaw, rad );
      checkState( exitPStateRaw, rad );
    }

    // Clone entry state
    auto entryPState = std::make_unique<LHCb::State>(*entryPStateRaw);
    if ( !entryPState )
    {
      Warning( "Failed to clone entry State" ).ignore();
      continue;
    }

    // Clone exit state (for aero use entrance point)
    auto exitPState = std::make_unique<LHCb::State>(*exitPStateRaw);
    if ( !exitPState )
    {
      Warning( "Failed to clone exit State" ).ignore();
      continue;
    }

    _ri_verbo << "  Found appropriate initial start/end States" << endmsg
              << "   EntryPos : " << entryPState->position() << endmsg
              << "   EntryDir : " << entryPState->slopes() << endmsg
              << "   ExitPos  : " << exitPState->position() << endmsg
              << "   ExitDir  : " << exitPState->slopes() << endmsg;

    // use state closest to the entry point in radiator
    Gaudi::XYZPoint               entryPoint1;
    Rich::RadIntersection::Vector intersects1;
    bool                          entryStateOK = false;
    if ( getNextInterPoint(
           entryPState->position(), entryPState->slopes(), radiator, entryPoint1 ) )
    {
      // extrapolate state to the correct z
      if ( moveState( *entryPState, *track, entryPoint1.z(), entryPStateRaw ) )
      {
        // find radiator entry and exit points
        if ( entryPState && fabs( entryPState->z() - entryPoint1.z() ) < m_zTolerance[rad] )
        {
          if ( getRadIntersections(
                 entryPState->position(), entryPState->slopes(), radiator, intersects1 ) > 0 )
          {
            entryStateOK = true;
            entryPoint1  = intersects1.front().entryPoint();
            _ri_verbo << "      Entry state rad intersection points " << intersects1 << endmsg;
          }
        }
      }
    }
    else
    {
      _ri_verbo << "Failed to intersect entry state" << endmsg;
    }

    // Try and use exit state to get exit point more precisely
    bool                          exitStateOK = false;
    Gaudi::XYZPoint               entryPoint2;
    Rich::RadIntersection::Vector intersects2;
    if ( getNextInterPoint( exitPState->position(), -exitPState->slopes(), radiator, entryPoint2 ) )
    {
      // extrapolate state to the correct z
      if ( moveState( *exitPState, *track, entryPoint2.z(), exitPStateRaw ) )
      {
        // find radiator entry and exit points
        if ( exitPState && fabs( exitPState->z() - entryPoint2.z() ) < m_zTolerance[rad] )
        {
          if ( getRadIntersections(
                 exitPState->position(), exitPState->slopes(), radiator, intersects2 ) > 0 )
          {
            exitStateOK = true;
            entryPoint2 = intersects2.front().entryPoint();
            _ri_verbo << "      Exit state rad intersection points " << intersects2 << endmsg;
          }
        }
      }
    }
    else
    {
      _ri_verbo << "Failed to intersect exit state" << endmsg;
    }

    // transport entry and exit states to best points
    bool sc = false;
    if ( entryStateOK && exitStateOK )
    {
      _ri_verbo << "  Both states OK : Zentry=" << entryPoint1.z()
                << " Zexit=" << intersects2.back().exitPoint().z() << endmsg;

      // make sure at current z positions
      _ri_verbo << "  Checking entry point is at final z=" << entryPoint1.z() << endmsg;
      const bool sc1 = moveState( *entryPState, *track, entryPoint1.z(), entryPStateRaw );
      _ri_verbo << "  Checking exit point is at final z=" << intersects2.back().exitPoint().z()
                << endmsg;
      const bool sc2 =
        moveState( *exitPState, *track, intersects2.back().exitPoint().z(), exitPStateRaw );
      sc = sc1 && sc2;
    }
    else if ( entryStateOK )
    {
      _ri_verbo << "  Entry state OK : Zentry=" << entryPoint1.z()
                << " Zexit=" << intersects1.back().exitPoint().z() << endmsg;

      // delete current exit state and replace with clone of raw entrance state
      exitPState = std::make_unique<LHCb::State>(*entryPStateRaw);
      if ( !exitPState )
      {
        Warning( "Failed to clone State" ).ignore();
        continue;
      }

      // make sure at current z positions
      _ri_verbo << "  Checking entry point is at final z= " << entryPoint1.z() << endmsg;
      const bool sc1 = moveState( *entryPState, *track, entryPoint1.z(), entryPStateRaw );
      _ri_verbo << "  Checking exit point is at final z= " << intersects1.back().exitPoint().z()
                << endmsg;
      const bool sc2 =
        moveState( *exitPState, *track, intersects1.back().exitPoint().z(), exitPStateRaw );
      sc = sc1 && sc2;
    }
    else if ( exitStateOK )
    {
      _ri_verbo << "  Exit state OK  : Zentry=" << entryPoint2.z()
                << " Zexit=" << intersects2.back().exitPoint().z() << endmsg;

      // delete current entry state and replace with clone of raw entrance state
      entryPState = std::make_unique<LHCb::State>(*exitPStateRaw);
      if ( !entryPState )
      {
        Warning( "Failed to clone State" ).ignore();
        continue;
      }

      // make sure at current z positions
      _ri_verbo << "  Checking entry point is at final z= " << entryPoint2.z() << endmsg;
      const bool sc1 = moveState( *entryPState, *track, entryPoint2.z(), entryPStateRaw );
      _ri_verbo << "  Checking exit point is at final z= " << intersects2.back().exitPoint().z()
                << endmsg;
      const bool sc2 =
        moveState( *exitPState, *track, intersects2.back().exitPoint().z(), exitPStateRaw );
      sc = sc1 && sc2;
    }
    else
    {
      // no valid extrapolations, so quit skip this track/radiator
      _ri_verbo << "  Both states failed" << endmsg;
      continue;
    }

    // Test final status code
    if ( !sc )
    {
      _ri_verbo << "    --> Failed to use state information. Quitting." << endmsg;
      continue;
    }

    //---------------------------------------------------------------------------------------------
    // Correction for beam pipe intersections
    if ( checkBeamPipe( rad ) )
    {

      // Get intersections with beam pipe using DeRich object
      Gaudi::XYZPoint inter1, inter2;
      const auto      intType = deBeam( rad )->intersectionPoints(
        entryPState->position(), exitPState->position(), inter1, inter2 );

      _ri_verbo << "  --> Beam Intersects : " << intType << " : " << inter1 << " " << inter2
                << endmsg;

      sc = true;
      if ( intType == DeRichBeamPipe::NoIntersection )
      { _ri_verbo << "   --> No beam intersections -> No corrections needed" << endmsg; }
      else if ( intType == DeRichBeamPipe::FrontAndBackFace )
      {
        _ri_verbo << "   --> Inside beam pipe -> Reject segment" << endmsg;
        continue;
      }
      else if ( intType == DeRichBeamPipe::FrontFaceAndCone )
      {
        // Update entry point to exit point on cone
        _ri_verbo << "   --> Correcting entry point to point on cone" << endmsg;
        sc = moveState( *entryPState, *track, inter2.z(), entryPStateRaw );
      }
      else if ( intType == DeRichBeamPipe::BackFaceAndCone )
      {
        // Update exit point to entry point on cone
        _ri_verbo << "   --> Correcting exit point to point on cone" << endmsg;
        sc = moveState( *exitPState, *track, inter1.z(), exitPStateRaw );
      }
      if ( !sc )
      {
        _ri_verbo << "    --> Error fixing radiator entry/exit points for beam-pipe. Quitting."
                  << endmsg;
        continue;
      }
    }
    //---------------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------------
    // check for intersection with spherical mirror for gas radiators
    // and if need be correct exit point accordingly
    //---------------------------------------------------------------------------------------------
    correctRadExitMirror( radiator, *track, *exitPState, exitPStateRaw );
    //---------------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------------
    // Final check that info is reasonable
    //---------------------------------------------------------------------------------------------
    const bool Zcheck     = entryPState->z() > exitPState->z();
    const bool ZdiffCheck = ( exitPState->z() - entryPState->z() ) < m_minStateDiff[rad];
    if ( Zcheck || ZdiffCheck ) { continue; }
    //---------------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------------
    // Radiator path length cut
    //---------------------------------------------------------------------------------------------
    if ( ( exitPState->position() - entryPState->position() ).Mag2() < m_minRadLengthSq[rad] )
    {
      _ri_verbo << "    --> Path length too short -> rejecting segment" << endmsg;
      continue;
    }
    //---------------------------------------------------------------------------------------------

    // Create final entry and exit state points and momentum vectors
    auto entryPoint( entryPState->position() );
    auto entryStateMomentum( entryPState->slopes() );
    entryStateMomentum *= entryPState->p() / std::sqrt( entryStateMomentum.Mag2() );
    auto exitPoint( exitPState->position() );
    auto exitStateMomentum( exitPState->slopes() );
    exitStateMomentum *= exitPState->p() / std::sqrt( exitStateMomentum.Mag2() );

    // ================================== NOTE ==========================================
    //
    // From now on we might use move semantics, as we no longer care about the
    // various data objects once they have been used to create a RichTrackSegment object.
    // So must be careful not 'move' something before it is finished with....
    //
    // ==================================================================================

    // Update final intersections
    auto &final_intersects = ( entryStateOK ? intersects1 : intersects2 );
    final_intersects.front().setEntryPoint( entryPoint );
    final_intersects.front().setEntryMomentum( entryStateMomentum );
    final_intersects.back().setExitPoint( exitPoint );
    final_intersects.back().setExitMomentum( exitStateMomentum );

    // Errors for entry and exit states
    const LHCb::RichTrackSegment::StateErrors entryErrs( *entryPState );
    const LHCb::RichTrackSegment::StateErrors exitErrs( *exitPState );

    // Check for strange states
    if ( UNLIKELY( m_checkStates ) )
    {
      checkState( entryPState.get(), rad );
      checkState( exitPState.get(), rad );
    }

    // print out final points
    _ri_verbo << "  Found final points :-" << endmsg
              << "   Entry : Pnt=" << final_intersects.front().entryPoint()
              << " Mom=" << final_intersects.front().entryMomentum()
              << " Ptot=" << std::sqrt( final_intersects.front().entryMomentum().Mag2() ) << endmsg;
    _ri_verbo << "   Exit  : Pnt=" << final_intersects.back().exitPoint()
              << " Mom=" << final_intersects.back().exitMomentum()
              << " Ptot=" << std::sqrt( final_intersects.back().exitMomentum().Mag2() ) << endmsg;

    // if get here segment will be saved so save relations
    segList.push_back( segments.size() ); // this gives the index for the next entry ...
    segToTkRel.push_back( tkIndex );

    try
    {

      // For gas radiators transport entry state to mid point to create middle point
      // information for three point RichTrackSegment constructor

      // data for middle state
      LHCb::RichTrackSegment::StateErrors midErrs;
      Gaudi::XYZPoint                     midPoint;
      Gaudi::XYZVector                    midMomentum;
      const auto                          OK = createMiddleInfo( *track,
                                        rad,
                                        *entryPState,
                                        entryPStateRaw,
                                        *exitPState,
                                        exitPStateRaw,
                                        midPoint,
                                        midMomentum,
                                        midErrs );

      if ( OK )
      {
        // Using this information, make radiator segment
        // this version uses 3 states and thus incorporates some concept of track curvature
        segments.emplace_back( std::move( final_intersects ),
                               midPoint,
                               midMomentum,
                               rad,
                               radiator->rich(),
                               entryErrs,
                               midErrs,
                               exitErrs );
      }
      else
      {
        // Using this information, make radiator segment
        // this version uses 2 states and thus forces a straight line approximation
        segments.emplace_back(
          std::move( final_intersects ), rad, radiator->rich(), entryErrs, exitErrs );
      }

      // Set mean photon energy
      segments.back().setAvPhotonEnergy( m_detParameters.get()->meanPhotonEnergy( rad ) );
    }
    catch ( const std::exception &excpt )
    {
      Warning( "Exception whilst creating RichTrackSegment '" + std::string( excpt.what() ) + "'" )
        .ignore();
      throw excpt;
    }

  } // end loop over radiators

  // Final printout of states, to see if anything has changed ...
  if ( msgLevel( MSG::VERBOSE ) )
  {
    verbose() << "Finished with Track key=" << track->key() << " history=" << track->history()
              << " : " << track->states().size() << " States at z =";
    for ( const auto *S : track->states() ) { verbose() << " " << S->z(); }
    verbose() << endmsg;
  }
}

//====================================================================================================
// creates middle point info
bool
DetailedTrSegMakerFromTracks::createMiddleInfo( const LHCb::Track &                  track,
                                                const Rich::RadiatorType             rad,
                                                LHCb::State &                        fState,
                                                const LHCb::State *                  fStateRef,
                                                LHCb::State &                        lState,
                                                const LHCb::State *                  lStateRef,
                                                Gaudi::XYZPoint &                    midPoint,
                                                Gaudi::XYZVector &                   midMomentum,
                                                LHCb::RichTrackSegment::StateErrors &errors ) const
{
  _ri_verbo << "   --> Creating middle point information" << endmsg;

  // middle point z position
  const auto midZ = 0.5 * ( fState.position().z() + lState.position().z() );

  // move start state to this z
  const auto moveFirst = moveState( fState, track, midZ, fStateRef );

  // move end state to this z
  const auto moveLast =
    ( Rich::Rich1Gas == rad ? moveState( lState, track, midZ, lStateRef ) : false );

  if ( moveFirst && moveLast )
  {
    midPoint    = fState.position() + ( lState.position() - fState.position() ) * 0.5;
    midMomentum = ( fState.slopes() + lState.slopes() ) * 0.5;
    midMomentum *= ( fState.p() + lState.p() ) / ( 2.0 * std::sqrt( midMomentum.Mag2() ) );
    errors = LHCb::RichTrackSegment::StateErrors( ( fState.errX2() + lState.errX2() ) * 0.5,
                                                  ( fState.errY2() + lState.errY2() ) * 0.5,
                                                  ( fState.errTx2() + lState.errTx2() ) * 0.5,
                                                  ( fState.errTy2() + lState.errTy2() ) * 0.5,
                                                  ( fState.errP2() + lState.errP2() ) * 0.5 );
  }
  else if ( moveFirst )
  {
    midPoint    = fState.position();
    midMomentum = fState.slopes();
    midMomentum *= fState.p() / std::sqrt( midMomentum.Mag2() );
    errors = LHCb::RichTrackSegment::StateErrors( fState );
  }
  else if ( moveLast )
  {
    midPoint    = lState.position();
    midMomentum = lState.slopes();
    midMomentum *= lState.p() / std::sqrt( midMomentum.Mag2() );
    errors = LHCb::RichTrackSegment::StateErrors( lState );
  }

  return ( moveFirst || moveLast );
}
//====================================================================================================

//====================================================================================================
void
DetailedTrSegMakerFromTracks::correctRadExitMirror( const DeRichRadiator *radiator,
                                                    const LHCb::Track &   track,
                                                    LHCb::State &         state,
                                                    const LHCb::State *   refState ) const
{
  _ri_verbo << "   --> Attempting Correction to exit point for spherical mirror" << endmsg;

  bool sc = true;

  // get rich information
  const auto rich = radiator->rich();

  // initial z position of state
  const auto initialZ = state.z();

  // move state to be on the inside of the mirror
  _ri_verbo << "    --> Moving state first to be inside mirror" << endmsg;
  sc = sc && moveState( state, track, state.z() - m_mirrShift[rich], refState );

  // find mirror intersection using the reflect method
  auto intersection = state.position();
  auto tempDir      = state.slopes();

  // get the RICH side
  const auto side = m_rich[rich]->side( intersection );

  // attempt reflection of mirror
  bool correct = false;
  if ( reflectSpherical( intersection,
                         tempDir,
                         m_rich[rich]->nominalCentreOfCurvature( side ),
                         m_rich[rich]->sphMirrorRadius() ) )
  {
    if ( intersection.z() < initialZ && radiator->geometry()->isInside( intersection ) )
    { correct = true; }
  }

  // finally, update state
  if ( correct )
  {
    _ri_verbo << "    --> Found correction is needed" << endmsg;
    sc = sc && moveState( state, track, intersection.z(), refState );
  }
  else
  {
    _ri_verbo << "    --> Found correction not needed. Moving back to original position" << endmsg;
    sc = sc && moveState( state, track, initialZ, refState );
  }

  if ( !sc ) Warning( "Problem correcting segment exit to mirror intersection" ).ignore();
}
//====================================================================================================

//====================================================================================================
bool
DetailedTrSegMakerFromTracks::moveState( LHCb::State &      stateToMove,
                                         const LHCb::Track &track,
                                         const double       z,
                                         const LHCb::State *refState ) const
{
  bool OK = true;

  // Check if requested move is big enough to bother with
  if ( fabs( stateToMove.z() - z ) > m_minZmove )
  {

    // verbose printout
    _ri_verbo << "    --> Extrapolating state from " << stateToMove.position() << endmsg;

    if ( UNLIKELY( m_extrapFromRef && refState ) )
    {
      // Delete current working state and start fresh from reference state
      stateToMove = *refState;
      _ri_verbo << "      --> Using reference state  " << stateToMove.position() << endmsg;
    }

    // Use State provider to move the state
    if ( UNLIKELY( m_useStateProvider ) )
    {
      // if ( !m_trStateP.get()->state(stateToMove,track,z) )
      if ( !m_trStateP.get()->stateFromTrajectory( stateToMove, track, z ) )
      {
        // Warning( "Failed to move state using StateProvider" ).ignore();
        OK = false;
      }
    }
    // Use original extrapolator
    else
    {
      // try with the extrapolator
      OK = m_trExt.get()->propagate( stateToMove, z ).isSuccess();
    }

    // verbose printout
    _ri_verbo << "                            to   " << stateToMove.position() << endmsg;
  }

  return OK;
}
//====================================================================================================

//====================================================================================================
void
DetailedTrSegMakerFromTracks::checkState( const LHCb::State *      state,
                                          const Rich::RadiatorType rad ) const
{
  if ( state )
  {
    if ( state->errX2() < 0 )
    { Warning( Rich::text( rad ) + " State has negative errX^2", StatusCode::SUCCESS ).ignore(); }
    if ( state->errY2() < 0 )
    { Warning( Rich::text( rad ) + " State has negative errY^2", StatusCode::SUCCESS ).ignore(); }
    if ( state->errTx2() < 0 )
    {
      Warning( Rich::text( rad ) + " State has negative errTx^2", StatusCode::SUCCESS ).ignore();
    }
    if ( state->errTy2() < 0 )
    {
      Warning( Rich::text( rad ) + " State has negative errTy^2", StatusCode::SUCCESS ).ignore();
    }
    if ( state->errP2() < 0 )
    { Warning( Rich::text( rad ) + " State has negative errP^2", StatusCode::SUCCESS ).ignore(); }
  }
}
//====================================================================================================

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DetailedTrSegMakerFromTracks )

//=============================================================================
