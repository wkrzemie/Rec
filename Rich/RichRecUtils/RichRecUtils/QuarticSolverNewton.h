/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//----------------------------------------------------------------------
/** @file QuarticSolverNewton.h
 *
 *  @author Christina Quast, Rainer Schwemmer
 *  @date   2017-02-03
 */
//----------------------------------------------------------------------

#pragma once

// Gaudi
#include "GaudiKernel/Kernel.h"

// VectorClass
#include "VectorClass/vectorclass.h"

// STL
#include <array>
#include <math.h>
#include <type_traits>

namespace Rich::Rec
{

  //-----------------------------------------------------------------------------
  /** @class QuarticSolverNewton
   *
   *  Utility class that implements the solving of the Quartic equation for the RICH
   *  Based on original code by Chris Jones
   *
   *  @author Christina Quast, Rainer Schwemmer
   *  @date   2017-02-03
   */
  //-----------------------------------------------------------------------------
  class QuarticSolverNewton
  {

  public:

    /** Solves the characteristic quartic equation for the RICH optical system.
     *
     *  See note LHCB/98-040 RICH section 3 for more details
     *
     *  @param emissionPoint Assumed photon emission point on track
     *  @param CoC           Spherical mirror centre of curvature
     *  @param virtDetPoint  Virtual detection point
     *  @param radius        Spherical mirror radius of curvature
     *  @param sphReflPoint  The reconstructed reflection pont on the spherical mirror
     *
     *  @return boolean indicating status of the quartic solution
     *  @retval true  Calculation was successful. sphReflPoint is valid.
     *  @retval false Calculation failed. sphReflPoint is not valid.
     */
    template < class TYPE,
               std::size_t BISECTITS = 3,
               std::size_t NEWTONITS = 4,
               class EMISSIONPOINT   = Gaudi::XYZPoint,
               class COC             = Gaudi::XYZPoint,
               class DETECTIONPOINT  = Gaudi::XYZPoint,
               class REFLECTPOINT    = Gaudi::XYZPoint,
               typename std::enable_if< std::is_arithmetic< TYPE >::value >::type * = nullptr >
    inline void solve( const EMISSIONPOINT & emissionPoint,
                       const COC &           CoC,
                       const DETECTIONPOINT &virtDetPoint,
                       const TYPE            radius,
                       REFLECTPOINT &        sphReflPoint ) const
    {
      using namespace std;

      // vector from mirror centre of curvature to assumed emission point
      const auto evec( emissionPoint - CoC );
      const TYPE e2 = evec.Dot( evec );

      // vector from mirror centre of curvature to virtual detection point
      const auto dvec( virtDetPoint - CoC );
      const TYPE d2 = dvec.Dot( dvec );

      // various quantities needed to create quartic equation
      // see LHCB/98-040 section 3, equation 3
      const TYPE ed2 = e2 * d2;
      const TYPE cosgamma2 =
        ( ed2 > TYPE( 0.0 ) ? std::pow( evec.Dot( dvec ), 2 ) / ed2 : TYPE( 1.0 ) );

      // vectorise 4 square roots into 1
      using Vec4x =
        typename std::conditional< std::is_same< TYPE, float >::value, Vec4f, Vec4d >::type;
      const auto tmp_sqrt = sqrt( Vec4x(
        e2, d2, cosgamma2 < TYPE( 1.0 ) ? TYPE( 1.0 ) - cosgamma2 : TYPE( 0.0 ), cosgamma2 ) );
      const TYPE e        = tmp_sqrt[0];
      const TYPE d        = tmp_sqrt[1];
      const TYPE singamma = tmp_sqrt[2];
      const TYPE cosgamma = tmp_sqrt[3];

      // const TYPE e         = std::sqrt(e2);
      // const TYPE d         = std::sqrt(d2);
      // const TYPE singamma  = std::sqrt( cosgamma2 < TYPE(1.0) ? TYPE(1.0)-cosgamma2 : TYPE(0.0)
      // ); const TYPE cosgamma  = std::sqrt( cosgamma2 );

      const TYPE dx  = d * cosgamma;
      const TYPE dy  = d * singamma;
      const TYPE r2  = radius * radius;
      const TYPE dy2 = dy * dy;
      const TYPE edx = e + dx;

      // basic constants
      constexpr TYPE two( 2.0 );
      constexpr TYPE four( 4.0 );

      // Fill array for quartic equation
      // const TYPE a0        = TYPE(4.0) * ed2;
      // Newton solver doesn't care about a0 being not 1.0. Remove costly division and several
      // multiplies. This has some downsides though. The a-values are hovering around a numerical
      // value of 10^15. single precision float max is 10^37. A single square and some multiplies
      // will push it over the limit of what single precision float can handle. It's ok for the
      // newton method, but Halley or higher order Housholder will fail without this normalization.
      // const auto inv_a0  =   ( a0 > 0 ? 1.0 / a0 : std::numeric_limits<TYPE>::max() );
      const TYPE dyrad2 = two * dy * radius;
      const TYPE aa[5]  = { four * ed2,
                           -( two * dyrad2 * e2 ),
                           ( ( dy2 * r2 ) + ( edx * edx * r2 ) - ( four * ed2 ) ),
                           ( dyrad2 * e * ( e - dx ) ),
                           ( ( e2 - r2 ) * dy2 ) };

      // Use optimized newton solver on quartic equation.
      const auto sinbeta = solve_quartic_newton_RICH< TYPE, BISECTITS, NEWTONITS >( aa );

      // TODO: This method should be better but has problems still for some reasons
      // const auto sinbeta = solve_quartic_housholder_RICH<TYPE, 3>(a1, a2, a3, a4);

      // construct rotation transformation
      // Set vector magnitude to radius
      // rotate vector and update reflection point
      // rotation matrix uses sin(beta) and cos(beta) to perform rotation
      // even fast_asinf (which is only single precision and defeats the purpose
      // of this class being templatable to double btw) is still too slow
      // plus there is a cos and sin call inside AngleAxis ...
      // We can do much better by just using the cos(beta) we already have to calculate
      // sin(beta) and do our own rotation. On top of that we rotate non-normalized and save several
      // Divisions by normalizing only once at the very end
      // Again, care has to be taken since we are close to float_max here without immediate
      // normalization. As far as we have tried with extreme values in the rich coordinate systems
      // this is fine.
      const TYPE nx = ( evec.Y() * dvec.Z() ) - ( evec.Z() * dvec.Y() );
      const TYPE ny = ( evec.Z() * dvec.X() ) - ( evec.X() * dvec.Z() );
      const TYPE nz = ( evec.X() * dvec.Y() ) - ( evec.Y() * dvec.X() );

      const TYPE nx2 = nx * nx;
      const TYPE ny2 = ny * ny;
      const TYPE nz2 = nz * nz;

      const TYPE norm      = nx2 + ny2 + nz2;
      const TYPE norm_sqrt = std::sqrt( norm );

      const TYPE a        = sinbeta * norm_sqrt;
      const TYPE sinbeta2 = sinbeta * sinbeta;
      const TYPE b =
        ( sinbeta2 < TYPE( 1.0 ) ? ( TYPE( 1.0 ) - std::sqrt( TYPE( 1.0 ) - sinbeta2 ) ) :
                                   TYPE( 1.0 ) );
      const TYPE enorm = radius / ( e * norm );

      const TYPE bnxny = b * nx * ny;
      const TYPE bnxnz = b * nx * nz;
      const TYPE bnynz = b * ny * nz;

      // Perform non-normalized rotation
      const std::array< TYPE, 9 > M = { norm - b * ( nz2 + ny2 ), a * nz + bnxny,
                                        -a * ny + bnxnz,          -a * nz + bnxny,
                                        norm - b * ( nx2 + nz2 ), a * nx + bnynz,
                                        a * ny + bnxnz,           -a * nx + bnynz,
                                        norm - b * ( ny2 + nx2 ) };

      // re-normalize rotation and scale to radius in one step
      const TYPE ex = enorm * ( evec.X() * M[0] + evec.Y() * M[3] + evec.Z() * M[6] );
      const TYPE ey = enorm * ( evec.X() * M[1] + evec.Y() * M[4] + evec.Z() * M[7] );
      const TYPE ez = enorm * ( evec.X() * M[2] + evec.Y() * M[5] + evec.Z() * M[8] );
      sphReflPoint  = { CoC.X() + ex, CoC.Y() + ey, CoC.Z() + ez };
    }

    /** Solves the characteristic quartic equation for the RICH optical system.
     *
     *  SIMD vectorised version
     *
     *  See note LHCB/98-040 RICH section 3 for more details
     *
     *  @param emissionPoint Assumed photon emission point on track
     *  @param CoC           Spherical mirror centre of curvature
     *  @param virtDetPoint  Virtual detection point
     *  @param radius        Spherical mirror radius of curvature
     *  @param sphReflPoint  The reconstructed reflection pont on the spherical mirror
     *
     *  @return boolean indicating status of the quartic solution
     *  @retval true  Calculation was successful. sphReflPoint is valid.
     *  @retval false Calculation failed. sphReflPoint is not valid.
     */
    template < class TYPE,
               std::size_t BISECTITS = 3,
               std::size_t NEWTONITS = 4,
               class EMISSIONPOINT   = Gaudi::XYZPoint,
               class COC             = Gaudi::XYZPoint,
               class DETECTIONPOINT  = Gaudi::XYZPoint,
               class REFLECTPOINT    = Gaudi::XYZPoint,
               typename std::enable_if< !std::is_arithmetic< TYPE >::value >::type * = nullptr >
    inline void solve( const EMISSIONPOINT & emissionPoint,
                       const COC &           CoC,
                       const DETECTIONPOINT &virtDetPoint,
                       const TYPE &          radius,
                       REFLECTPOINT &        sphReflPoint ) const
    {
      using namespace std;

      // vector from mirror centre of curvature to assumed emission point
      const auto evec( emissionPoint - CoC );
      const TYPE e2 = evec.Dot( evec );

      // vector from mirror centre of curvature to virtual detection point
      const auto dvec( virtDetPoint - CoC );
      const TYPE d2 = dvec.Dot( dvec );

      // various quantities needed to create quartic equation
      // see LHCB/98-040 section 3, equation 3
      const TYPE ed2     = e2 * d2;
      const auto ed2mask = ed2 > TYPE::Zero();
      TYPE       eDotd   = evec.Dot( dvec );
      eDotd( !ed2mask )  = TYPE::One();
      TYPE cosgamma2     = ( eDotd * eDotd ) / ed2;

      const TYPE e               = std::sqrt( e2 );
      const TYPE d               = std::sqrt( d2 );
      const auto cosgamma2mask   = cosgamma2 > TYPE::One();
      cosgamma2( cosgamma2mask ) = TYPE::One();
      const TYPE singamma2       = TYPE::One() - cosgamma2;
      // singamma2.setZero(cosgamma2mask); // might need this for FPEs... but avoid if not.
      const TYPE singamma = std::sqrt( singamma2 );
      const TYPE cosgamma = std::sqrt( cosgamma2 );

      const TYPE dx  = d * cosgamma;
      const TYPE dy  = d * singamma;
      const TYPE r2  = radius * radius;
      const TYPE dy2 = dy * dy;
      const TYPE edx = e + dx;

      // basic constants
      const TYPE two( 2.0 );
      const TYPE four( 4.0 );

      // Fill array for quartic equation
      // const TYPE a0        = TYPE(4.0) * ed2;
      // Newton solver doesn't care about a0 being not 1.0. Remove costly division and several
      // multiplies. This has some downsides though. The a-values are hovering around a numerical
      // value of 10^15. single precision float max is 10^37. A single square and some multiplies
      // will push it over the limit of what single precision float can handle. It's ok for the
      // newton method, but Halley or higher order Housholder will fail without this normalization.
      // const auto inv_a0  =   ( a0 > 0 ? 1.0 / a0 : std::numeric_limits<TYPE>::max() );
      const TYPE dyrad2 = two * dy * radius;
      const TYPE aa[5]  = { four * ed2,
                           -( two * dyrad2 * e2 ),
                           ( ( dy2 * r2 ) + ( edx * edx * r2 ) - ( four * ed2 ) ),
                           ( dyrad2 * e * ( e - dx ) ),
                           ( ( e2 - r2 ) * dy2 ) };

      // Use optimized newton solver on quartic equation.
      const auto sinbeta = solve_quartic_newton_RICH< TYPE, BISECTITS, NEWTONITS >( aa );

      // TODO: This method should be better but has problems still for some reasons
      // const auto sinbeta = solve_quartic_housholder_RICH<TYPE, 3>(a1, a2, a3, a4);

      // construct rotation transformation
      // Set vector magnitude to radius
      // rotate vector and update reflection point
      // rotation matrix uses sin(beta) and cos(beta) to perform rotation
      // even fast_asinf (which is only single precision and defeats the purpose
      // of this class being templatable to double btw) is still too slow
      // plus there is a cos and sin call inside AngleAxis ...
      // We can do much better by just using the cos(beta) we already have to calculate
      // sin(beta) and do our own rotation. On top of that we rotate non-normalized and save several
      // Divisions by normalizing only once at the very end
      // Again, care has to be taken since we are close to float_max here without immediate
      // normalization. As far as we have tried with extreme values in the rich coordinate systems
      // this is fine.
      const TYPE nx = ( evec.Y() * dvec.Z() ) - ( evec.Z() * dvec.Y() );
      const TYPE ny = ( evec.Z() * dvec.X() ) - ( evec.X() * dvec.Z() );
      const TYPE nz = ( evec.X() * dvec.Y() ) - ( evec.Y() * dvec.X() );

      const TYPE nx2 = nx * nx;
      const TYPE ny2 = ny * ny;
      const TYPE nz2 = nz * nz;

      const TYPE norm      = nx2 + ny2 + nz2;
      const TYPE norm_sqrt = std::sqrt( norm );

      const TYPE a              = sinbeta * norm_sqrt;
      TYPE       sinbeta2       = sinbeta * sinbeta;
      const auto sinbeta2mask   = sinbeta2 < TYPE::One();
      sinbeta2( !sinbeta2mask ) = TYPE::One();
      const TYPE b              = ( TYPE::One() - std::sqrt( TYPE::One() - sinbeta2 ) );
      const TYPE enorm          = radius / ( e * norm );

      const TYPE bnxny = b * nx * ny;
      const TYPE bnxnz = b * nx * nz;
      const TYPE bnynz = b * ny * nz;

      // Perform non-normalized rotation
      const std::array< TYPE, 9 > M = { norm - b * ( nz2 + ny2 ), a * nz + bnxny,
                                        -a * ny + bnxnz,          -a * nz + bnxny,
                                        norm - b * ( nx2 + nz2 ), a * nx + bnynz,
                                        a * ny + bnxnz,           -a * nx + bnynz,
                                        norm - b * ( ny2 + nx2 ) };

      // re-normalize rotation and scale to radius in one step
      const TYPE ex = enorm * ( evec.X() * M[0] + evec.Y() * M[3] + evec.Z() * M[6] );
      const TYPE ey = enorm * ( evec.X() * M[1] + evec.Y() * M[4] + evec.Z() * M[7] );
      const TYPE ez = enorm * ( evec.X() * M[2] + evec.Y() * M[5] + evec.Z() * M[8] );
      sphReflPoint  = { CoC.X() + ex, CoC.Y() + ey, CoC.Z() + ez };
    }

  private:

    // A newton iteration solver for the Rich quartic equation
    // Since the polynomial that is evaluated here is extremely constrained
    // (root is in small interval, one root guaranteed), we can use a much more
    // efficient approximation (which still has the same precision) instead of the
    // full blown mathematically absolute correct method and still end up with
    // usable results

    template < class TYPE >
    inline TYPE f4( const TYPE ( &a )[5], const TYPE &x ) const
    {
      return ( ( ( ( ( ( ( a[0] * x ) + a[1] ) * x ) + a[2] ) * x ) + a[3] ) * x ) + a[4];
    }

    /** Horner's method to evaluate the polynomial and its derivatives with as little math
     * operations as possible. We use a template here to allow the compiler to unroll the for loops
     * and produce code that is free from branches and optimized for the grade of polynomial and
     * derivatives as necessary.
     */
    template < class TYPE, std::size_t ORDER = 4, std::size_t DIFFGRADE = 3 >
    inline void evalPolyHorner( const TYPE ( &a )[ORDER + 1],
                                TYPE ( &res )[DIFFGRADE + 1],
                                const TYPE &x ) const
    {
      for ( std::size_t i = 0; i <= DIFFGRADE; ++i ) { res[i] = a[0]; }
      for ( std::size_t j = 1; j <= ORDER; ++j )
      {
        res[0]       = ( res[0] * x ) + a[j];
        const auto l = std::min( ORDER - j, DIFFGRADE );
        for ( std::size_t i = 1; i <= l; ++i ) { res[i] = ( res[i] * x ) + res[i - 1]; }
      }
      TYPE l = 1.0;
      for ( std::size_t i = 2; i <= DIFFGRADE; ++i )
      {
        l *= i;
        res[i] *= l;
      }
    }

    /** Newton-Rhapson method for calculating the root of the rich polynomial. It uses the bisection
     *  method in the beginning to get close enough to the root to allow the second stage newton
     * method to converge faster. After 4 iterations of newton precision is as good as single
     * precision floating point will get you. We have introduced a few tuning parameters like the
     * newton gain factor and a slightly skewed bisection division, which in this particular case
     * help to speed things up.
     *  TODO: Once we are happy with the number of newton and bisection iterations, this function
     * should be templated to the number of these iterations to allow loop unrolling and elimination
     * of unnecessary branching.
     *  TODO: These tuning parameters have been found by low effort experimentation on random input
     * data. A more detailed study should be done with real data to find the best values.
     */
    template < class TYPE,
               std::size_t BISECTITS                                                = 3,
               std::size_t NEWTONITS                                                = 4,
               typename std::enable_if< std::is_arithmetic< TYPE >::value >::type * = nullptr >
    inline TYPE solve_quartic_newton_RICH( const TYPE ( &a )[5] ) const
    {

      // Use N steps of bisection method to find starting point for newton
      TYPE l( 0.0 );
      TYPE u( 0.5 );
      TYPE m( 0.2 ); // We start a bit off center since the distribution of roots tends to be more
                     // to the left side
      for ( std::size_t i = 0; i < BISECTITS; ++i )
      {
        const auto oppositeSign = std::signbit( f4( a, m ) * f4( a, l ) );
        l                       = oppositeSign ? l : m;
        u                       = oppositeSign ? m : u;
        // 0.4 instead of 0.5 to speed up convergence. Most roots seem to be closer to 0 than to the
        // extreme end
        m = ( u + l ) * TYPE( 0.4 );
      }

      // Most of the times we are approaching the root of the polynomial from one side
      // and fall short by a certain fraction. This fraction seems to be around 1.04 of the
      // quotient which is subtracted from x. By scaling it up, we take bigger steps towards
      // the root and thus converge faster.
      // TODO: study this factor more closely it's pure guesswork right now. We might get
      // away with 3 iterations if we can find an exact value
      const TYPE gain   = 1.04;
      TYPE       res[2] = { TYPE( 0 ), TYPE( 0 ) };
      for ( std::size_t i = 0; i < NEWTONITS; ++i )
      {
        evalPolyHorner< TYPE, 4, 1 >( a, res, m );
        m -= gain * ( res[0] / res[1] );
      }

      return m;
    }

    /// SIMD version of solve_quartic_newton_RICH
    template < class TYPE,
               std::size_t BISECTITS                                                 = 3,
               std::size_t NEWTONITS                                                 = 4,
               typename std::enable_if< !std::is_arithmetic< TYPE >::value >::type * = nullptr >
    inline TYPE solve_quartic_newton_RICH( const TYPE ( &a )[5] ) const
    {

      // Use N steps of bisection method to find starting point for newton
      TYPE l( 0.0 );
      TYPE u( 0.5 );
      TYPE m( 0.2 ); // We start a bit off center since the distribution of roots tends to be more
                     // to the left side
      for ( std::size_t i = 0; i < BISECTITS; ++i )
      {
        const auto f4am         = f4( a, m );
        const auto f4al         = f4( a, l );
        const auto oppositeSign = ( Vc::isnegative( f4am ) ^ Vc::isnegative( f4al ) );
        l( !oppositeSign )      = m;
        u( oppositeSign )       = m;
        // 0.4 instead of 0.5 to speed up convergence. Most roots seem to be closer to 0 than to the
        // extreme end
        m = ( u + l ) * TYPE( 0.4 );
      }

      // Most of the times we are approaching the root of the polynomial from one side
      // and fall short by a certain fraction. This fraction seems to be around 1.04 of the
      // quotient which is subtracted from x. By scaling it up, we take bigger steps towards
      // the root and thus converge faster.
      // TODO: study this factor more closely it's pure guesswork right now. We might get
      // away with 3 iterations if we can find an exact value
      const TYPE gain   = 1.04;
      TYPE       res[2] = { TYPE::Zero(), TYPE::Zero() };
      for ( std::size_t i = 0; i < NEWTONITS; ++i )
      {
        evalPolyHorner< TYPE, 4, 1 >( a, res, m );
        m -= gain * ( res[0] / res[1] );
      }

      return m;
    }
  };

} // namespace Rich::Rec
