/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <algorithm>

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// RichDet
#include "RichDet/DeRichPD.h"

// Utils
#include "RichUtils/ZipRange.h"

// interfaces
#include "RichFutureInterfaces/IRichSmartIDTool.h"

namespace Rich::Future::Rec
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class SIMDSummaryPixels RichSIMDSummaryPixels.h
   *
   *  Forms SIMD summary objects for the pixel information
   *
   *  @author Chris Jones
   *  @date   2017-10-16
   */
  class SIMDSummaryPixels final
    : public Transformer< SIMDPixelSummaries( const Rich::PDPixelCluster::Vector & ),
                          Traits::BaseClass_t< AlgBase > >
  {

  public:

    /// Standard constructor
    SIMDSummaryPixels( const std::string &name, ISvcLocator *pSvcLocator );

  public:

    /// Operator for each space point
    SIMDPixelSummaries operator()( const Rich::PDPixelCluster::Vector &clusters ) const override;

  private:

    /// RichSmartID Tool
    ToolHandle< const ISmartIDTool > m_idTool { this,
                                                "SmartIDTool",
                                                "Rich::Future::SmartIDTool/SmartIDTool:PUBLIC" };

    /// Shortcut incase clustering is disabled
    Gaudi::Property< bool > m_noClustering { this, "NoClustering", false };
  };

} // namespace Rich::Future::Rec
