/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichPixelClusters.h"

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : PixelClusters
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

PixelClusters::PixelClusters( const std::string &name, ISvcLocator *pSvcLocator )
  : Consumer( name,
              pSvcLocator,
              KeyValue { "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default } )
{}

//-----------------------------------------------------------------------------

StatusCode
PixelClusters::prebookHistograms()
{
  bool ok = true;

  for ( const auto rich : Rich::detectors() )
  {
    ok &= richHisto1D(
            Rich::HistogramID( "clusterSize", rich ), "Pixel Cluster Sizes", -0.5, 100.5, 101 ) !=
          nullptr;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

//-----------------------------------------------------------------------------

void
PixelClusters::operator()( const Rich::PDPixelCluster::Vector &clusters ) const
{
  // the lock
  std::lock_guard lock( m_updateLock );

  for ( const auto &cluster : clusters )
  {
    if ( UNLIKELY( cluster.empty() ) )
    { Warning( "Empty cluster !", StatusCode::SUCCESS ).ignore(); }
    else
    {
      richHisto1D( HID( "clusterSize", cluster.rich() ) )->fill( cluster.size() );
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PixelClusters )

//-----------------------------------------------------------------------------
