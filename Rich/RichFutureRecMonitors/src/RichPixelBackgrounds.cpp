/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichPixelBackgrounds.h"

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : PixelBackgrounds
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

PixelBackgrounds::PixelBackgrounds( const std::string &name, ISvcLocator *pSvcLocator )
  : Consumer( name,
              pSvcLocator,
              { KeyValue { "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                KeyValue { "PixelBackgroundsLocation", SIMDPixelBackgroundsLocation::Default } } )
{
  // Default number of bins
  setProperty( "NBins1DHistos", 25 );
  setProperty( "NBins2DHistos", 20 );
  // debug
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//-----------------------------------------------------------------------------

StatusCode
PixelBackgrounds::prebookHistograms()
{
  bool ok = true;

  // The max background value for each RICH for plots
  DetectorArray< float > maxBkg = { 0.6, 0.3 };

  for ( const auto rich : Rich::detectors() )
  {
    ok &=
      richHisto1D(
        HID( "pixBkg", rich ), "Pixel Likelihood Background", -0.001, maxBkg[rich], nBins1D() ) !=
      nullptr;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

//-----------------------------------------------------------------------------

void
PixelBackgrounds::operator()( const SIMDPixelSummaries &  pixels,
                              const SIMDPixelBackgrounds &backgrounds ) const
{
  // the lock
  std::lock_guard lock( m_updateLock );

  for ( const auto &&[pix, bkg] : Ranges::ConstZip( pixels, backgrounds ) )
  {
    // which rich
    const auto rich = pix.rich();

    // Loop over the scalar entries for this SIMD pixel
    for ( std::size_t i = 0; i < SIMDFP::Size; ++i )
    {
      // pixel background
      richHisto1D( HID( "pixBkg", rich ) )->fill( bkg[i] );
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PixelBackgrounds )

//-----------------------------------------------------------------------------
