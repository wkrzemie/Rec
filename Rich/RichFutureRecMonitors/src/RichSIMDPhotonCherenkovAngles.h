/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STD
#include <algorithm>
#include <sstream>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPDIdentifier.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

// Track selector
#include "TrackInterfaces/ITrackSelector.h"

// RichDet
#include "RichDet/DeRichSystem.h"

namespace Rich::Future::Rec::Moni
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class SIMDPhotonCherenkovAngles RichSIMDPhotonCherenkovAngles.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class SIMDPhotonCherenkovAngles final
    : public Consumer< void( const LHCb::Track::Selection &,
                             const Summary::Track::Vector &,
                             const Relations::PhotonToParents::Vector &,
                             const LHCb::RichTrackSegment::Vector &,
                             const CherenkovAngles::Vector &,
                             const SIMDCherenkovPhoton::Vector & ),
                       Traits::BaseClass_t< HistoAlgBase > >
  {

  public:

    /// Standard constructor
    SIMDPhotonCherenkovAngles( const std::string &name, ISvcLocator *pSvcLocator );

    /// Initialize
    StatusCode initialize() override;

  public:

    /// Functional operator
    void operator()( const LHCb::Track::Selection &            tracks,
                     const Summary::Track::Vector &            sumTracks,
                     const Relations::PhotonToParents::Vector &photToSegPix,
                     const LHCb::RichTrackSegment::Vector &    segments,
                     const CherenkovAngles::Vector &           expTkCKThetas,
                     const SIMDCherenkovPhoton::Vector &       photons ) const override;

  protected:

    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:

    /// Get the per PD resolution histogram ID
    inline std::string pdResPlotID( const LHCb::RichSmartID hpd ) const
    {
      const Rich::DAQ::PDIdentifier hid( hpd );
      std::ostringstream            id;
      id << "PDs/pd-" << hid.number();
      return id.str();
    }

  private:

    /// Pointer to RICH system detector element
    const DeRichSystem *m_RichSys = nullptr;

    /// Which radiators to monitor
    Gaudi::Property< RadiatorArray< bool > > m_rads { this, "Radiators", { false, true, true } };

    /// minimum beta value for tracks
    Gaudi::Property< RadiatorArray< float > > m_minBeta { this,
                                                          "MinBeta",
                                                          { 0.9999f, 0.9999f, 0.9999f } };

    /// maximum beta value for tracks
    Gaudi::Property< RadiatorArray< float > > m_maxBeta { this,
                                                          "MaxBeta",
                                                          { 999.99f, 999.99f, 999.99f } };

    /// Min theta limit for histos for each rad
    Gaudi::Property< RadiatorArray< float > > m_ckThetaMin { this,
                                                             "ChThetaRecHistoLimitMin",
                                                             { 0.150f, 0.030f, 0.010f } };

    /// Max theta limit for histos for each rad
    Gaudi::Property< RadiatorArray< float > > m_ckThetaMax { this,
                                                             "ChThetaRecHistoLimitMax",
                                                             { 0.325f, 0.060f, 0.036f } };

    /// Histogram ranges for CK resolution plots
    Gaudi::Property< RadiatorArray< float > > m_ckResRange { this,
                                                             "CKResHistoRange",
                                                             { 0.025f, 0.005f, 0.0025f } };

    /// Enable the per PD resolution plots, for the given radiators
    Gaudi::Property< RadiatorArray< bool > > m_pdResPlots { this,
                                                            "EnablePerPDPlots",
                                                            { false, false, false } };

    /** Track selector.
     *  Longer term should get rid of this and pre-filter the input data instead.
     */
    ToolHandle< const ITrackSelector > m_tkSel { this, "TrackSelector", "TrackSelector" };
  };

} // namespace Rich::Future::Rec::Moni
