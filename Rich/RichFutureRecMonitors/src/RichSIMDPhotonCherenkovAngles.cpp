/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichSIMDPhotonCherenkovAngles.h"

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : SIMDPhotonCherenkovAngles
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

SIMDPhotonCherenkovAngles::SIMDPhotonCherenkovAngles( const std::string &name,
                                                      ISvcLocator *      pSvcLocator )
  : Consumer( name,
              pSvcLocator,
              { KeyValue { "TracksLocation", LHCb::TrackLocation::Default },
                KeyValue { "SummaryTracksLocation", Summary::TESLocations::Tracks },
                KeyValue { "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                KeyValue { "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                KeyValue { "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                KeyValue { "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default } } )
{
  // print some stats on the final plots
  setProperty( "HistoPrint", true );
}

//-----------------------------------------------------------------------------

StatusCode
SIMDPhotonCherenkovAngles::initialize()
{
  auto sc = Consumer::initialize();
  if ( !sc ) return sc;

  // RichDet
  m_RichSys = getDet< DeRichSystem >( DeRichLocations::RichSystem );

  return sc;
}

//-----------------------------------------------------------------------------

StatusCode
SIMDPhotonCherenkovAngles::prebookHistograms()
{

  // List of HPDs
  const auto &hpds = m_RichSys->allPDRichSmartIDs();

  // Loop over radiators
  for ( const auto rad : Rich::radiators() )
  {
    if ( m_rads[rad] )
    {
      // Which RICH ?
      const auto rich = ( rad == Rich::Rich2Gas ? Rich::Rich2 : Rich::Rich1 );

      // inclusive plots
      richHisto1D( HID( "thetaRec", rad ),
                   "Reconstructed Ch Theta | All photons",
                   m_ckThetaMin[rad],
                   m_ckThetaMax[rad],
                   nBins1D(),
                   "Cherenkov Theta / rad" );
      richHisto1D( HID( "phiRec", rad ),
                   "Reconstructed Ch Phi | All photons",
                   0.0,
                   2.0 * Gaudi::Units::pi,
                   nBins1D(),
                   "Cherenkov Phi / rad" );
      richHisto1D( HID( "ckResAll", rad ),
                   "Rec-Exp Cktheta | All photons",
                   -m_ckResRange[rad],
                   m_ckResRange[rad],
                   nBins1D(),
                   "delta(Cherenkov theta) / rad" );

      // loop over detector sides
      for ( const auto side : Rich::sides() )
      {
        richHisto1D( HID( "ckResAllPerPanel", side, rad ),
                     "Rec-Exp Cktheta | All photons",
                     -m_ckResRange[rad],
                     m_ckResRange[rad],
                     nBins1D(),
                     "delta(Cherenkov theta) / rad" );
      }

      // Enable per PD resolution plots ?
      if ( UNLIKELY( m_pdResPlots[rad] ) )
      {
        // Loop over PDs
        for ( const auto PD : hpds )
        {
          if ( PD.rich() != rich ) continue;
          // construct title
          const auto         copyN = m_RichSys->copyNumber( PD );
          std::ostringstream title;
          title << "Rec-Exp Cktheta | All photons | PD#" << copyN << " " << PD;
          // construct ID
          const HID hID( pdResPlotID( PD ), rad );
          // book the plot
          richHisto1D( hID,
                       title.str(),
                       -m_ckResRange[rad],
                       m_ckResRange[rad],
                       nBins1D(),
                       "delta(Cherenkov theta) / rad" );
        }
      }

    } // rad is active
  }   // rad loop

  return StatusCode::SUCCESS;
}

//-----------------------------------------------------------------------------

void
SIMDPhotonCherenkovAngles::operator()( const LHCb::Track::Selection &            tracks,
                                       const Summary::Track::Vector &            sumTracks,
                                       const Relations::PhotonToParents::Vector &photToSegPix,
                                       const LHCb::RichTrackSegment::Vector &    segments,
                                       const CherenkovAngles::Vector &           expTkCKThetas,
                                       const SIMDCherenkovPhoton::Vector &       photons ) const
{
  // loop over the track containers
  for ( const auto &&[tk, sumTk] : Ranges::ConstZip( tracks, sumTracks ) )
  {
    // Is this track selected ?
    if ( !m_tkSel.get()->accept( *tk ) ) continue;

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() )
    {
      // photon data
      const auto &phot = photons[photIn];
      const auto &rels = photToSegPix[photIn];
      // the segment for this photon
      const auto &seg = segments[rels.segmentIndex()];

      // get the expected CK theta values for this segment
      const auto &expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !m_rads[rad] ) continue;

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // The PID type to assume. Just use Pion here.
      const auto pid = Rich::Pion;

      // beta
      const auto beta = richPartProps()->beta( pTot, pid );
      // selection cuts
      if ( beta < m_minBeta[rad] || beta > m_maxBeta[rad] ) continue;

      // expected CK theta
      const auto thetaExp = expCKangles[pid];

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i )
      {
        // Select valid entries
        if ( phot.validityMask()[i] )
        {

          // reconstructed theta
          const auto thetaRec = phot.CherenkovTheta()[i];
          // reconstructed phi
          const auto phiRec = phot.CherenkovPhi()[i];
          // delta theta
          const auto deltaTheta = thetaRec - thetaExp;

          // SmartID
          const auto id = phot.smartID()[i];

          // Detctor side
          const auto side = id.panel();

          // fill some plots
          richHisto1D( HID( "thetaRec", rad ) )->fill( thetaRec );
          richHisto1D( HID( "phiRec", rad ) )->fill( phiRec );
          richHisto1D( HID( "ckResAll", rad ) )->fill( deltaTheta );
          richHisto1D( HID( "ckResAllPerPanel", side, rad ) )->fill( deltaTheta );

          // Per PD plots
          if ( UNLIKELY( m_pdResPlots[rad] ) )
          {
            const auto pdID = id.pdID();
            auto       h    = richHisto1D( HID( pdResPlotID( pdID ), rad ) );
            if ( h ) { h->fill( deltaTheta ); }
          }

        } // valid scalars
      }   // SIMD loop
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPhotonCherenkovAngles )

//-----------------------------------------------------------------------------
