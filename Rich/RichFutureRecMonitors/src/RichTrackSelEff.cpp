/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichTrackSelEff.h"

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : TrackSelEff
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

TrackSelEff::TrackSelEff( const std::string &name, ISvcLocator *pSvcLocator )
  : Consumer( name,
              pSvcLocator,
              { KeyValue { "TracksLocation", LHCb::TrackLocation::Default },
                KeyValue { "RichPIDsLocation", LHCb::RichPIDLocation::Default } } )
{
  // reset defaults in base class
  setProperty( "NBins1DHistos", 50 );
}

//-----------------------------------------------------------------------------

StatusCode
TrackSelEff::prebookHistograms()
{
  // global quantities
  richHisto1D( HID( "nTracks" ), "# Tracks / Event", -0.5, 200.5, 201 );
  richHisto1D( HID( "nRichTracks" ), "# Rich Tracks / Event", -0.5, 200.5, 201 );
  // track variable plots
  prebookHistograms( "All/" );
  // return
  return StatusCode::SUCCESS;
}

//-----------------------------------------------------------------------------

void
TrackSelEff::prebookHistograms( const std::string &tkClass )
{
  using namespace Gaudi::Units;

  richProfile1D(
    tkClass + "effVP", "RICH Track Sel. Eff. V P", 1.00 * GeV, 100.0 * GeV, nBins1D() );
  richProfile1D(
    tkClass + "effVPt", "RICH Track Sel. Eff. V Pt", 0.10 * GeV, 8.0 * GeV, nBins1D() );
  richProfile1D(
    tkClass + "effVChi2PDOF", "RICH Track Sel. Eff. V Chi^2 / D.O.F.", 0, 3, nBins1D() );
  richProfile1D(
    tkClass + "effVGhostProb", "RICH Track Sel. Eff. V Ghost Probability", 0.0, 0.4, nBins1D() );
  richProfile1D(
    tkClass + "effVCloneDist", "RICH Track Sel. Eff. V Clone Distance", 0.0, 6e3, nBins1D() );

  trackPlots( tkClass + "Selected/" );
  trackPlots( tkClass + "Rejected/" );
}

//-----------------------------------------------------------------------------

void
TrackSelEff::trackPlots( const std::string &tag )
{
  using namespace Gaudi::Units;

  richHisto1D( tag + "P", "Track Momentum", 0 * GeV, 100 * GeV, nBins1D() );
  richHisto1D( tag + "Pt", "Track Transverse Momentum", 0 * GeV, 8 * GeV, nBins1D() );
  richHisto1D( tag + "Chi2PDOF", "Track Chi^2 / D.O.F.", 0, 3, nBins1D() );
  richHisto1D( tag + "GhostProb", "Track Ghost Probability", 0.0, 0.4, nBins1D() );
  richHisto1D( tag + "CloneDist", "Track Clone Distance", 0.0, 6e3, nBins1D() );
}

//-----------------------------------------------------------------------------

void
TrackSelEff::operator()( const LHCb::Track::Selection &tracks, const LHCb::RichPIDs &pids ) const
{

  // Count selected tracks
  unsigned int nSelTracks( 0 );

  // loop over input tracks
  for ( const auto *tk : tracks )
  {
    // Does this track have a PID result associated to it ?
    const bool sel = std::any_of(
      pids.begin(), pids.end(), [&tk]( const auto pid ) { return pid->track() == tk; } );

    // count selected tracks
    if ( sel ) { ++nSelTracks; }

    // Fill plots
    fillTrackPlots( tk, sel, "All/" );
  }

  // fill event plots
  richHisto1D( HID( "nTracks" ) )->fill( tracks.size() );
  richHisto1D( HID( "nRichTracks" ) )->fill( nSelTracks );
}

//-----------------------------------------------------------------------------

void
TrackSelEff::fillTrackPlots( const LHCb::Track *track,
                             const bool         sel,
                             const std::string &tkClass ) const
{
  // cache clone dist
  const double cloneDist = track->info( LHCb::Track::AdditionalInfo::CloneDist, 5.5e3 );

  // Efficiencies plots
  const double richEff = ( sel ? 100.0 : 0.0 );
  richProfile1D( tkClass + "effVP" )->fill( track->p(), richEff );
  richProfile1D( tkClass + "effVPt" )->fill( track->pt(), richEff );
  richProfile1D( tkClass + "effVChi2PDOF" )->fill( track->chi2PerDoF(), richEff );
  richProfile1D( tkClass + "effVGhostProb" )->fill( track->ghostProbability(), richEff );
  richProfile1D( tkClass + "effVCloneDist" )->fill( cloneDist, richEff );

  // plot selection variables
  const std::string tag = ( sel ? tkClass + "Selected/" : tkClass + "Rejected/" );
  richHisto1D( tag + "P" )->fill( track->p() );
  richHisto1D( tag + "Pt" )->fill( track->pt() );
  richHisto1D( tag + "Chi2PDOF" )->fill( track->chi2PerDoF() );
  richHisto1D( tag + "GhostProb" )->fill( track->ghostProbability() );
  richHisto1D( tag + "CloneDist" )->fill( cloneDist );
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackSelEff )

//-----------------------------------------------------------------------------
