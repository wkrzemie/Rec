/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RichFutureRecEvent/RichRecMassHypoRings.h"

#include "GaudiKernel/PhysicalConstants.h"

Rich::Future::Rec::ClosestPoints
Rich::Future::Rec::getPointsClosestInAzimuth( const RayTracedCKRingPoint::Vector &ring,
                                              const float                         angle )
{
  // return object
  ClosestPoints points( nullptr, nullptr );

  // Do we have enough rings to work with ?
  const auto nPoints = ring.size();
  if ( nPoints > 1 )
  {
    // cache 1 / 2pi
    constexpr auto oneOverTwoPi = 1.0 / Gaudi::Units::twopi;

    // compute index in array based on angle (assumes constant seperation).
    const auto iPoint = (unsigned int)( ( angle * (float)(nPoints)*oneOverTwoPi ) - 0.5 );

    // Find the lower edge point
    points.first = &ring[iPoint < nPoints ? iPoint : nPoints - 1];

    // ... and the next point on
    points.second = &ring[iPoint < nPoints - 1 ? iPoint + 1 : 0];
  }

  // return found points
  return points;
}
