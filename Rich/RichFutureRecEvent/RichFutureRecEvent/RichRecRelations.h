/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <ostream>
#include <vector>

// Utils
#include "RichUtils/RichGeomPhoton.h"
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"

// Kernel
#include "Kernel/FastAllocVector.h"

// Event Model
#include "Event/Track.h"

namespace Rich::Future::Rec::Relations
{

  //=============================================================================

  /// Type for storing a list of track segment indices
  using SegmentIndices = LHCb::STL::Vector< LHCb::RichTrackSegment::Vector::size_type >;

  /** @class TrackToSegments RichFutureRecEvent/RichRecRelations.h
   *
   *  Store the relationship between tracks and RICH segments.
   *
   *  The key of the track and the container index for the segments.
   *  This is likely to evolve...
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackToSegments final
  {
  public:

    /// Constructor from track key
    TrackToSegments( const LHCb::Tracks::key_type key, const LHCb::Tracks::size_type index )
      : tkKey( key ), tkIndex( index )
    {
      // reserve space for 2 raditors (RICH1+RICH2 gas).
      segmentIndices.reserve( 2 );
    }

  public:

    /// The track key
    LHCb::Tracks::key_type tkKey {};
    /// Track index
    LHCb::Tracks::size_type tkIndex { 0 };
    /// The list of segment indices
    SegmentIndices segmentIndices {};

  public:

    /// Container type
    using Vector = LHCb::STL::Vector< TrackToSegments >;

  public:

    /// overload printout to ostream operator <<
    friend inline std::ostream &operator<<( std::ostream &s, const TrackToSegments &rels )
    {
      return s << "Track Key = " << rels.tkKey << " Segments = " << rels.segmentIndices;
    }
  };

  /// TES locations for Track to Segment relations
  namespace TrackToSegmentsLocation
  {
    /// Default Location in TES for the track to segment 'relations, before selection.
    inline const std::string Initial = "Rec/RichFuture/Relations/TrackToSegments/Initial";
    /// Default Location in TES for the track to segment 'relations, after segment selection.
    inline const std::string Selected = "Rec/RichFuture/Relations/TrackToSegments/Selected";
  } // namespace TrackToSegmentsLocation

  //=============================================================================

  /// List of segment to track indices
  using SegmentToTrackVector = LHCb::STL::Vector< LHCb::Tracks::size_type >;

  /// TES locations for lists of track indices
  namespace SegmentToTrackLocation
  {
    /// Default Location in TES for the track to photon indices relations
    inline const std::string Default = "Rec/RichFuture/Relations/SegmentToTrack/Default";
  } // namespace SegmentToTrackLocation

  //=============================================================================

  /** @class PhotonToParents RichFutureRecEvent/RichRecRelations.h
   *
   *  Class storing the relationship between photons and the track, segment and
   *  pixel objects they are built from.
   *
   *  Container indices are used through. Again, liable to change....
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class PhotonToParents final
  {
  public:

    /// Container type
    using Vector = LHCb::STL::Vector< PhotonToParents >;

  public:

    /// Default constructor
    PhotonToParents() = default;
    /// Constructor from a pixel and segment index pair
    PhotonToParents( const LHCb::RichGeomPhoton::Vector::size_type   photIn,
                     const Rich::PDPixelCluster::Vector::size_type   pixIn,
                     const LHCb::RichTrackSegment::Vector::size_type segIn,
                     const LHCb::Tracks::size_type                   tkIn )
      : m_photonIndex( photIn )
      , m_pixelIndex( pixIn )
      , m_segmentIndex( segIn )
      , m_trackIndex( tkIn )
    {}

  public:

    /// access the photon index
    inline LHCb::RichGeomPhoton::Vector::size_type photonIndex() const noexcept
    {
      return m_photonIndex;
    }
    /// access the pixel index
    inline Rich::PDPixelCluster::Vector::size_type pixelIndex() const noexcept
    {
      return m_pixelIndex;
    }
    /// access the segment index
    inline LHCb::RichTrackSegment::Vector::size_type segmentIndex() const noexcept
    {
      return m_segmentIndex;
    }
    /// access the track index
    inline LHCb::Tracks::size_type trackIndex() const noexcept { return m_trackIndex; }

  public:

    /// overload printout to ostream operator <<
    friend inline std::ostream &operator<<( std::ostream &s, const PhotonToParents &rels )
    {
      return s << "Photon=" << rels.photonIndex() << " Pixel=" << rels.pixelIndex()
               << " Segment=" << rels.segmentIndex() << " Track=" << rels.trackIndex();
    }

  private:

    /// The photon index
    LHCb::RichGeomPhoton::Vector::size_type m_photonIndex { 0 };
    /// The pixel index
    Rich::PDPixelCluster::Vector::size_type m_pixelIndex { 0 };
    /// The segment index
    LHCb::RichTrackSegment::Vector::size_type m_segmentIndex { 0 };
    /// The track index
    LHCb::Tracks::size_type m_trackIndex { 0 };
  };

  /// TES locations for Photon to Pixel+Segment relations
  namespace PhotonToParentsLocation
  {
    /// Default Location in TES for the photon to pixel+segment relations
    inline const std::string Default = "Rec/RichFuture/Relations/PhotonToParents/Default";
  } // namespace PhotonToParentsLocation

  //=============================================================================

  /// Type for storing a list of photon indices
  using PhotonIndices = LHCb::STL::Vector< LHCb::RichGeomPhoton::Vector::size_type >;
  /// List of photon indices
  using PhotonIndicesVector = LHCb::STL::Vector< PhotonIndices >;

  /// TES locations for lists of Photon indices
  namespace PhotonIndicesLocation
  {
    /// Default Location in TES for the track to photon indices relations
    inline const std::string Tracks = "Rec/RichFuture/Relations/PhotonIndices/Tracks";
  } // namespace PhotonIndicesLocation

  //=============================================================================

} // namespace Rich::Future::Rec::Relations
