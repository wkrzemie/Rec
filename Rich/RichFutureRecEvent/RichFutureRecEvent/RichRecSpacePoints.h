/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <ostream>
#include <string>

// Kernel
#include "Kernel/FastAllocVector.h"
#include "Kernel/RichSmartID.h"

// geometry
#include "GaudiKernel/Point3DTypes.h"

namespace Rich::Future::Rec
{

  /** @class SpacePoint RichFutureRecEvent/RichRecSpacePoints.h
   *
   *  Simple class to store space point information
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  using SpacePoint = Gaudi::XYZPoint;

  /// Container of space points
  using SpacePointVector = LHCb::STL::Vector< SpacePoint >;

  /// TES locations
  namespace SpacePointLocation
  {
    /// Default Location in TES for the global pixel space points
    inline const std::string PixelsGlobal = "Rec/RichFuture/PixelPositions/Global";
    /// Default Location in TES for the local pixel space points
    inline const std::string PixelsLocal = "Rec/RichFuture/PixelPositions/Local";
    /// Default Location in TES for the global ray traced segment space points
    inline const std::string SegmentsGlobal = "Rec/RichFuture/SegmentPositions/Global";
    /// Default Location in TES for the local ray traced segment space points
    inline const std::string SegmentsLocal = "Rec/RichFuture/SegmentPositions/Local";
  } // namespace SpacePointLocation

  /** @class SegmentPanelSpacePoints RichFutureRecEvent/RichRecSpacePoints.h
   *
   *  Simple class to store ray trace track space point information for each RICH panel
   *
   *  @author Chris Jones
   *  @date   20160930
   */
  class SegmentPanelSpacePoints final
  {

  public:

    /// Vector of space points
    using Vector = LHCb::STL::Vector< SegmentPanelSpacePoints >;

  public:

    /// Standard constructor
    SegmentPanelSpacePoints() = default;

    /// Constructor from full information
    SegmentPanelSpacePoints( const SpacePoint &      p,
                             const SpacePoint &      p0,
                             const SpacePoint &      p1,
                             const Rich::Side        bestSide,
                             const LHCb::RichSmartID pdID,
                             const bool              OK = true )
      : m_OK( OK )
      , m_bestPoint( p )
      , m_pos( { p0, p1 } )
      , m_bestSide( bestSide )
      , m_closestPD( pdID )
    {}

  public:

    /// Access the point for the given panel
    inline const SpacePoint &point( const Rich::Side side ) const noexcept { return m_pos[side]; }

    /// Access the point for the best side
    inline const SpacePoint &point() const noexcept { return m_bestPoint; }

    /// Access the best side
    inline Rich::Side bestSide() const noexcept { return m_bestSide; }

    /// Access the closest PD
    inline const LHCb::RichSmartID &closestPD() const noexcept { return m_closestPD; }

    /// Access the status code
    inline bool ok() const noexcept { return m_OK; }

  public:

    /// overload printout to ostream operator <<
    friend inline std::ostream &operator<<( std::ostream &s, const SegmentPanelSpacePoints &pts )
    {
      return s << "[ Best Ptn = " << pts.point() << " | Side Ptns = " << pts.point( Rich::left )
               << " " << pts.point( Rich::right ) << " ]";
    }

  private:

    /// Status code
    bool m_OK { false };

    /// The best point
    SpacePoint m_bestPoint;

    /// Space points for each panel
    std::array< SpacePoint, Rich::NPDPanelsPerRICH > m_pos = { {} };

    /// Which is the best side for this track segment
    Rich::Side m_bestSide { Rich::top };

    /// Closest PD to the segment extrapolation
    LHCb::RichSmartID m_closestPD;
  };

} // namespace Rich::Future::Rec
