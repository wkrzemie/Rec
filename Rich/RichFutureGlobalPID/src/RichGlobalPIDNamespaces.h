/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @namespace Rich::Future::Rec::GlobalPID
 *
 *  Namespace for RICH Global PID software
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   04/12/2006
 */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/** @namespace Rich::Future::Rec::GlobalPID::MC
 *
 *  Namespace for RICH Global PID MC related software
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   05/12/2006
 */
//-----------------------------------------------------------------------------
