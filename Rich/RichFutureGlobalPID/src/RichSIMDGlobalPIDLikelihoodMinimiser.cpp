/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichSIMDGlobalPIDLikelihoodMinimiser.h"

using namespace Rich::Future::Rec;
using namespace Rich::Future::Rec::GlobalPID;

//-----------------------------------------------------------------------------
// Implementation file for class : RichGlobalPIDLikelihoodMinimiser
//
// 2016-10-25 : Chris Jones
//-----------------------------------------------------------------------------

SIMDLikelihoodMinimiser::SIMDLikelihoodMinimiser( const std::string &name,
                                                  ISvcLocator *      pSvcLocator )
  : MultiTransformer(
      name,
      pSvcLocator,
      { KeyValue { "SummaryTracksLocation", Summary::TESLocations::Tracks },
        KeyValue { "SummaryPixelsLocation", Summary::TESLocations::Pixels },
        KeyValue { "TrackPIDHyposInputLocation", TrackPIDHyposLocation::Default },
        KeyValue { "TrackDLLsInputLocation", TrackDLLsLocation::Default },
        KeyValue { "PixelBackgroundsLocation", SIMDPixelBackgroundsLocation::Default },
        KeyValue { "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
        KeyValue { "PhotonSignalsLocation", SIMDPhotonSignalsLocation::Default } },
      { KeyValue { "TrackPIDHyposOutputLocation", TrackPIDHyposLocation::Global },
        KeyValue { "TrackDLLsOutputLocation", TrackDLLsLocation::Global } } )
{
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//-----------------------------------------------------------------------------

StatusCode
SIMDLikelihoodMinimiser::initialize()
{
  auto sc = MultiTransformer::initialize();
  if ( !sc ) return sc;

  // Initialise the interpolator for f = log ( exp(x) - 1 )
  // m_logExpLookUp.init( FloatType(5),
  //                     []( const FloatType x )
  //                     { const FloatType a = vdt::fast_exp(x) - 1.0;
  //                       const FloatType b = std::max(a,std::numeric_limits<FloatType>::min());
  //                       return vdt::fast_log(b); } );

  // Initialise parameters
  m_minSigSIMD = SIMDFP( m_minSig );
  m_logMinSig  = SIMDFP( full_logExp( m_minSig.value() ) );

  // return
  return sc;
}

//-----------------------------------------------------------------------------

OutData
SIMDLikelihoodMinimiser::operator()( const Summary::Track::Vector &            gTracks,
                                     const Summary::Pixel::Vector &            gPixels,
                                     const TrackPIDHypos &                     inTkHypos,
                                     const TrackDLLs::Vector &                 inTkDLLs,
                                     const SIMDPixelBackgrounds &              pixelBkgs,
                                     const Relations::PhotonToParents::Vector &photRel,
                                     const SIMDPhotonSignals::Vector &         photSignals ) const
{
  // Form the output data, cloning from the inputs
  OutData outData( inTkHypos, inTkDLLs );

  // form the track containers (includes output data)
  TrackConts tkC( outData, gTracks );

  // local object for photon containers
  PhotConts photC( photRel, photSignals );

  // Local working containers for pixel data.
  PixelConts pixC( gPixels, pixelBkgs );

  // --------------------------------------------------------------------
  // Pre-compute the starting signal contributions for each pixel, using
  // the current set of track hypothesis values
  // --------------------------------------------------------------------

  // Loop over all pixels
  for ( auto &&[bkg, sig, logExp, gPix] :
        Ranges::Zip( pixC.pixelBkgs, pixC.pixSignals, pixC.pixCurrlogExp, pixC.gPixels ) )
  {

    // reset the signal for this pixel
    sig.setZero();

    // loop over the photons for this pixel
    for ( const auto iPhot : gPix.photonIndices() )
    {
      // get the track index for this photon
      const auto &tkIndex = photRel[iPhot].trackIndex();
      // Get the current ID for this track
      const auto &tkId = tkC.tkHypos()[tkIndex];
      // The signal for this photon and ID
      const auto &photSig = ( photSignals[iPhot] )[tkId];
      // add to the signal for this pixel
      sig += photSig;
    }

    // recompute the cached log(exp(x)-1) term
    // only need it if pixel has photons
    // Allows for a more efficient loop in likelihood()
    // and avoids unneccessary calculations here.
    logExp = ( !gPix.photonIndices().empty() ? sigFunc( bkg + sig ) : SIMDFP::Zero() );
    // logExp = sigFunc( bkg + sig );
  }

  // print pixel data
  if ( msgLevel( MSG::VERBOSE ) )
  {
    verbose() << "Starting Pixels" << endmsg;
    print( pixC, MSG::VERBOSE );
  }

  // --------------------------------------------------------------------
  // Compute complete likelihood for event with starting hypotheses
  // --------------------------------------------------------------------

  auto currentBestLL = logLikelihood( tkC, pixC );

  // --------------------------------------------------------------------

  // Make the Track+DLL list container for DLL sorting
  TrackList trackList;
  trackList.reserve( gTracks.size() );

  // Initial likelihood iteration. Fills the track list.
  const auto nChangeFirstIt = initBestLogLikelihood( trackList, tkC, pixC, photC );

  // print pixel data
  if ( msgLevel( MSG::VERBOSE ) )
  {
    verbose() << "Pixels Before Minimisation" << endmsg;
    print( pixC, MSG::VERBOSE );
  }

  // recompute the complete event LL
  const auto finalLL = logLikelihood( tkC, pixC );
  _ri_debug << "LogLL before/after initial minimisation = " << currentBestLL << "/" << finalLL
            << " : " << nChangeFirstIt << " tracks changed hypothesis" << endmsg;
  currentBestLL = finalLL;

  // --------------------------------------------------------------------

  // did any tracks change in the first initialisation
  unsigned int eventIteration = 0;
  if ( gTracks.size() > 1 && nChangeFirstIt > 0 )
  {

    // Do the event iterations
    eventIteration = doIterations( currentBestLL, trackList, tkC, pixC, photC );

    // Final check, to see if we are at the global best set of hypos
    if ( m_doFinalDllCheck )
    {

      unsigned int nChange( 1 ), nRetries( 0 );
      while ( nChange > 0 && nRetries < m_maxItRetries )
      {
        ++nRetries;

        // First pass at minimising
        nChange = initBestLogLikelihood( trackList, tkC, pixC, photC );
        if ( nChange > 0 )
        {
          // Not quite at best hypos yet ...
          std::ostringstream mess;
          mess << nChange << " track(s) changed hypo after iteration " << nRetries;
          // debug printout
          _ri_debug << mess.str() << endmsg;

          // Rerun iterations again
          eventIteration += doIterations( currentBestLL, trackList, tkC, pixC, photC );
        }

        // Compute the event likelihood from scratch
        currentBestLL = logLikelihood( tkC, pixC );

        if ( nRetries == m_maxItRetries && nChange > 0 )
        {
          Warning( "Maximum number of iteration re-tries reached", StatusCode::SUCCESS ).ignore();
        }

      } // retry while loop

      _ri_debug << "Ran " << nRetries << " iteration re-tries" << endmsg;

    } // final check

  } // changes on first try

  // --------------------------------------------------------------------

  _ri_debug << "Performed " << eventIteration
            << " event minimisation iteration(s). Final LogL = " << currentBestLL << endmsg;

  // --------------------------------------------------------------------

  return outData;
}

//=============================================================================

unsigned int
SIMDLikelihoodMinimiser::initBestLogLikelihood( TrackList & trackList,
                                                TrackConts &tkC,
                                                PixelConts &pixC,
                                                PhotConts & photC ) const
{
  _ri_debug << "Running initial log likelihood maximisation" << endmsg;

  // local data for the track changes
  InitTrackInfo::Vector minTrackData;
  // guess the average number of changing tracks..
  minTrackData.reserve( 30 );

  // Loop over the tracks
  for ( auto &&[gTk, gTkID, gTkDLLs] :
        Ranges::Zip( std::as_const( tkC.gTracks ), tkC.tkHypos(), tkC.tkDLLs() ) )
  {
    //_ri_verbo << "Track " << gTk.key() << endmsg;

    // Skip inactive tracks
    if ( !gTk.active() )
    {
      //_ri_debug << "  -> Skipping inactive track " << gTk.key() << endmsg;
      continue;
    }

    // Initialise starting values
    auto mindeltaLL = boost::numeric::bounds< FloatType >::highest();
    auto minHypo    = gTkID;

    // Loop over all particle codes
    for ( auto hypo = activeParticles().begin(); hypo != activeParticles().end(); ++hypo )
    {
      // Skip analysing starting hypothesis
      if ( *hypo != gTkID )
      {

        // calculate delta logLikelihood for event with new track hypothesis
        const auto deltaLogL = deltaLogLikelihood( gTk, gTkID, *hypo, pixC, photC );
        //_ri_verbo << "  -> " << *hypo << " dLL = " << deltaLogL << endmsg;

        // Set the value for deltaLL for this hypothesis
        gTkDLLs.setData( *hypo, deltaLogL );

        // Set new minimum if lower logLikelihood is achieved
        if ( deltaLogL < mindeltaLL )
        {
          if ( 0 != deltaLogL ) { mindeltaLL = deltaLogL; }
          if ( deltaLogL < m_epsilon ) { minHypo = *hypo; }
        }

        // In case the threshold is reached, skip other hypotheses
        if ( std::none_of( hypo, activeParticles().end(), [&gTk]( const auto id ) {
               return gTk.thresholds()[id];
             } ) )
        {
          // Pick the DLL value to use for all below threshold typs
          // Either the current value, or if the current best is below, use that value
          const auto thresDLL = !gTk.thresholds()[gTkID] ? gTkDLLs[gTkID] : deltaLogL;
          //_ri_verbo << "   -> " << " below threshold" << endmsg;
          std::for_each( hypo, activeParticles().end(), [&gTkDLLs, &thresDLL]( const auto id ) {
            gTkDLLs.setData( id, thresDLL );
          } );
          break;
        }

      } // not same PID

    } // pid loop

    // Save info on tracks that have a better minimum hypothesis
    if ( minHypo != gTkID ) { minTrackData.emplace_back( &gTk, minHypo, mindeltaLL ); }

    // Add this track / deltaLL to the track list
    trackList.emplace_back( mindeltaLL, &gTk );

  } // track loop

  // Finally, set all track hypotheses to their minimum
  _ri_debug << "Setting initial minimum track hypotheses" << endmsg;
  for ( const auto &T : minTrackData )
  {
    _ri_debug << " -> Track " << boost::format( "%4i" ) % T.pidTrack->key()
              << " prefers hypothesis " << T.hypo << ". DLL = " << T.minDLL << endmsg;
    // The gTrack
    const auto &gtk = *T.pidTrack;
    // set best hypothesis
    setBestHypo( gtk, tkC.tkHypos(), T.hypo, pixC, photC );
    // set delta LL to zero for best hypothesis
    tkC.tkDLLs()[gtk.index()].setData( T.hypo, 0 );
  }

  if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) )
  {
    verbose() << "Track data after initial likelihood minimisation :-" << endmsg;
    print( tkC, MSG::VERBOSE );
  }

  return minTrackData.size();
}

//=============================================================================

SIMDLikelihoodMinimiser::FloatType
SIMDLikelihoodMinimiser::findBestLogLikelihood( const DetectorArray< bool > &inR,
                                                MinTrList &                  minTracks,
                                                TrackList &                  trackList,
                                                TrackConts &                 tkC,
                                                PixelConts &                 pixC,
                                                PhotConts &                  photC ) const
{
  // Initialise
  minTracks.clear();
  const Summary::Track *overallMinTrack = nullptr;
  Rich::ParticleIDType  overallMinHypo  = Rich::Unknown;
  FloatType             minDLLs         = 0;
  FloatType             minEventDll     = boost::numeric::bounds< FloatType >::highest();

  // sort Track list according to delta LL
  std::stable_sort( trackList.begin(), trackList.end(), []( const auto &p1, const auto &p2 ) {
    return p1.first < p2.first;
  } );
  if ( msgLevel( MSG::DEBUG ) ) { printTrackList( trackList, tkC, MSG::DEBUG ); }

  // Loop on all tracks
  _ri_debug << "Finding best overall log likelihood" << endmsg;
  for ( auto &P : trackList )
  {
    // Get the global PID track for this entry
    const auto *gTk = P.second;

    //_ri_debug << " -> Track " << gTk->key() << " DLL = " << P.first << endmsg;

    // Skip inactive tracks
    if ( !gTk->active() )
    {
      //_ri_debug << "   -> Inactive. Skipping..." << endmsg;
      continue;
    }

    // Get the current track hypo
    const auto &gTkID = tkC.tkHypos()[gTk->index()];

    // get the DLLs
    auto &gTkDLLs = tkC.tkDLLs()[gTk->index()];

    // skip tracks frozen for this iteration
    if ( P.first > freezeOutDll() )
    {
      _ri_debug << "  -> Freeze-out value = " << freezeOutDll() << " -> Aborting remaining tracks"
                << endmsg;
      break;
    }

    // skip tracks that do not share a RICH with the last changed tracks
    // for these tracks it is not possible that the hypos could change
    if ( m_richCheck && !( ( inR[Rich::Rich1] && gTk->richActive()[Rich::Rich1] ) ||
                           ( inR[Rich::Rich2] && gTk->richActive()[Rich::Rich2] ) ) )
    {
      _ri_debug << "  -> Skipping track in unaltered RICH" << endmsg;
      continue;
    }

    bool                 addto( false ), minFound( false );
    FloatType            minTrackDll = boost::numeric::bounds< FloatType >::highest();
    Rich::ParticleIDType minHypo     = Rich::Unknown;
    for ( auto hypo = activeParticles().begin(); hypo != activeParticles().end(); ++hypo )
    {
      // Skip analysing starting hpyothesis
      if ( *hypo != gTkID )
      {

        // calculate delta logLikelihood for event with new track hypothesis
        const auto deltaLogL = deltaLogLikelihood( *gTk, gTkID, *hypo, pixC, photC );

        // Set the value for deltaLL for this hypothesis
        gTkDLLs.setData( *hypo, deltaLogL );

        // is DLL change significant ?
        if ( deltaLogL < m_epsilon )
        {

          // Is this the best hypo for this track
          if ( deltaLogL < minTrackDll )
          {
            _ri_debug << "    -> Track " << boost::format( "%4i" ) % gTk->key()
                      << " prefers hypothesis " << *hypo << " to " << gTkID
                      << ". DLL = " << deltaLogL << endmsg;

            // set that a new best is found and update best dll and type
            minFound    = true;
            P.first     = deltaLogL;
            minHypo     = *hypo;
            minTrackDll = deltaLogL;

            // Is dll change enough to add to force change list
            if ( !addto && ( deltaLogL < forceChangeDll() ) )
            {
              _ri_debug << "     -> Adding to force change list" << endmsg;
              addto = true;
            }

            // Overall best track change for event ?
            if ( deltaLogL < minEventDll )
            {
              overallMinTrack = gTk;
              overallMinHypo  = *hypo;
              minEventDll     = deltaLogL;
            }

          } // best hypo for this track
        }
        else
        {
          // store best DLL for this track to enable it to be set at end of hypo loop
          // do here so set even if deltaLogL > m_epsilon
          if ( deltaLogL != 0 && deltaLogL < minTrackDll ) { minTrackDll = deltaLogL; }
        }

        // If threshold is reached, set the deltaLL for all other hypotheses
        if ( std::none_of( hypo, activeParticles().end(), [&gTk]( const auto id ) {
               return gTk->thresholds()[id];
             } ) )
        {
          // Pick the DLL value to use for all below threshold typs
          // Either the current value, or if the current best is below, use that value
          const auto thresDLL = !gTk->thresholds()[gTkID] ? gTkDLLs[gTkID] : deltaLogL;
          std::for_each( hypo, activeParticles().end(), [&gTkDLLs, &thresDLL]( const auto id ) {
            gTkDLLs.setData( id, thresDLL );
          } );
          break;
        }

      } // not same ID

    } // end hypothesis loop

    // if a good new hypo is not found, just update track dll in container to best
    if ( !minFound ) { P.first = minTrackDll; }

    // if found tracks with good enough deltaLL, add to force-change map
    if ( addto )
    {
      minTracks[gTk] = minHypo;
      minDLLs += minTrackDll;
      P.first = 0;
      break;
    }

    // do we have enough tracks to change to break out the loop early ?
    if ( minTracks.size() >= m_maxTkChanges )
    {
      _ri_debug << "    -> Found " << minTracks.size() << " tracks to change. Aborting track loop"
                << endmsg;
      break;
    }

  } // end track loop

  // update final information
  if ( overallMinTrack )
  {
    if ( minTracks.empty() )
    {
      minTracks[overallMinTrack] = overallMinHypo;
      minDLLs                    = minEventDll;
    }
  }

  _ri_debug << "Found " << minTracks.size() << " track(s) to change" << endmsg;

  // return the change in the event LL
  return ( overallMinTrack ? minDLLs : 0 );
}

//=============================================================================

unsigned int
SIMDLikelihoodMinimiser::doIterations( FloatType & currentBestLL,
                                       TrackList & trackList,
                                       TrackConts &tkC,
                                       PixelConts &pixC,
                                       PhotConts & photC ) const
{

  // iterate to minimum solution
  unsigned int eventIteration = 0;
  MinTrList    minTracks;
  bool         tryAgain = true;
  while ( tryAgain || !minTracks.empty() )
  {
    _ri_debug << "Event Iteration " << eventIteration << endmsg;

    // Get the active RICH flags
    const auto inR = getRICHFlags( minTracks );

    // Iterate finding the best likelihood
    currentBestLL += findBestLogLikelihood( inR, minTracks, trackList, tkC, pixC, photC );

    // set track hypotheses to the current best
    if ( !minTracks.empty() )
    {
      for ( auto &tk : minTracks )
      {
        if ( Rich::Unknown == tk.second )
        {
          err() << " Track " << ( tk.first )->key() << " has been Id'ed as Unknown !!" << endmsg;
        }
        else
        {
          // The gTrack
          const auto &gtk = *tk.first;
          _ri_debug << " -> Changing Track " << gtk.key() << " hypothesis to from "
                    << tkC.tkHypos()[gtk.index()] << " to " << tk.second
                    << ". LogL = " << currentBestLL << endmsg;
          // Update hypothesis to best
          setBestHypo( gtk, tkC.tkHypos(), tk.second, pixC, photC );
          // set deltaLL to zero
          tkC.tkDLLs()[gtk.index()].setData( tk.second, 0 );
        }
      }
    }
    // only quit if last iteration was with both riches
    else if ( inR[Rich::Rich1] && inR[Rich::Rich2] )
    {
      _ri_debug << " -> ALL DONE. Quitting event iterations" << endmsg;
      tryAgain = false;
    }

    if ( ++eventIteration > m_maxEventIterations )
    {
      Warning( "Taken more than maximum number of iterations -> quitting", StatusCode::SUCCESS )
        .ignore();
      break;
    }

  } // end iteration while loop

  return eventIteration;
}

//=============================================================================

void
SIMDLikelihoodMinimiser::setBestHypo( const Summary::Track &     track,
                                      TrackPIDHypos &            tkHypos,
                                      const Rich::ParticleIDType newHypo,
                                      PixelConts &               pixC,
                                      PhotConts &                photC ) const
{
  // the hypo for this track
  auto &hypo = tkHypos[track.index()];

  // update the pixel signal terms for each photon
  for ( const auto iPhot : track.photonIndices() )
  {
    // index for the pixel associated to this photon
    const auto iPix = photC.photRel[iPhot].pixelIndex();

    // signals for this photon
    const auto &sigs = photC.photSignals[iPhot];

    // ref to the signal
    auto &pixSig = pixC.pixSignals[iPix];

    // Update the signal term
    pixSig += ( sigs[newHypo] - sigs[hypo] );

    // recompute the cached log(exp(x)-1) term
    pixC.pixCurrlogExp[iPix] = sigFunc( pixC.pixelBkgs[iPix] + pixSig );
  }

  // Finally set the track hypo to the new value
  hypo = newHypo;
}

//=============================================================================

SIMDLikelihoodMinimiser::FloatType
SIMDLikelihoodMinimiser::logLikelihood( const TrackConts &tkC, const PixelConts &pixC ) const
{
  // return trackLL - pixelLL

  // track part
  // const auto tkrange = Ranges::ConstZip(tkC.gTracks,tkC.tkHypos());
  // need to look into why ranges v3 accumulate did not seem to work
  // const auto trackLL =
  //  std::accumulate( tkrange.begin(), tkrange.end(), 0.0,
  //                   []( const auto sum, const auto && tkd )
  //                   { // The track
  //                     const auto & tk = std::get<0>(tkd);
  //                     // its current hypo
  //                     const auto & id = std::get<1>(tkd);
  //                     // sum up the LL
  //                     return sum + tk.totalSignals()[id]; } );
  FloatType trackLL = 0;
  for ( const auto &&[tk, id] : Ranges::ConstZip( tkC.gTracks, tkC.tkHypos() ) )
  {
    // sum up the LL
    trackLL += tk.totalSignals()[id];
  }
  _ri_debug << "  -> Track contribution    = " << trackLL << endmsg;

  // pixel term
  // can only use this call if pixC.pixCurrlogExp is filled with
  // 0 for pixels with no associated photons
  // Note the sum() call is because pixCurrlogExp is a vector type
  const auto pixelLL =
    std::accumulate( pixC.pixCurrlogExp.begin(), pixC.pixCurrlogExp.end(), SIMDFP::Zero() ).sum();
  // const auto pixrange = Ranges::ConstZip(pixC.pixCurrlogExp,pixC.gPixels);
  // const auto pixelLL
  //  = std::accumulate( pixrange.begin(), pixrange.end(), 0.0,
  //                     []( const auto& sum, const auto && pix )
  //                     { const auto & logExp = std::get<0>(pix);
  //                       const auto & gpix   = std::get<1>(pix);
  //                       return ( !gpix.photonIndices().empty() ? sum+logExp : sum ); } );
  _ri_debug << "  -> Pixel contribution    = " << pixelLL << endmsg;

  return ( trackLL - pixelLL );
}

//=============================================================================

void
SIMDLikelihoodMinimiser::print( const TrackConts &tkC, const MSG::Level level ) const
{
  msgStream( level ) << "Printing Track Data" << endmsg;
  for ( const auto &[gTk, gTkID, gTkDLLs] :
        Ranges::ConstZip( tkC.gTracks, tkC.tkHypos(), tkC.tkDLLs() ) )
  {
    msgStream( level ) << " Tk " << gTk.key() << " " << gTkID << " : DLLs " << gTkDLLs << endmsg;
  }
}

//=============================================================================

void
SIMDLikelihoodMinimiser::print( const PixelConts &pixC, const MSG::Level level ) const
{
  msgStream( level ) << "Printing Pixel Data" << endmsg;
  for ( const auto &&[pixBkg, pixSig, pixCurrLogE] :
        Ranges::ConstZip( pixC.pixelBkgs, pixC.pixSignals, pixC.pixCurrlogExp ) )
  {
    msgStream( level ) << std::setprecision( 9 ) << " Pix Bkg " << pixBkg << " Sig " << pixSig
                       << " curLogExp " << pixCurrLogE << endmsg;
  }
}

//=============================================================================

void
SIMDLikelihoodMinimiser::printTrackList( const TrackList & trackList,
                                         const TrackConts &tkC,
                                         const MSG::Level  level ) const
{
  if ( msgLevel( level ) )
  {
    msgStream( level ) << trackList.size() << " Tracks in DLL list" << endmsg;
    for ( const auto &P : trackList )
    {
      const auto *gTrack = P.second;
      if ( gTrack->index() >= tkC.tkHypos().size() )
      { Error( "GPID Track index out out range !" ).ignore(); }
      msgStream( level ) << " -> Track Key = " << gTrack->key() << " DLL = " << P.first
                         << " BestID = " << tkC.tkHypos()[gTrack->index()] << endmsg;
    }
    msgStream( level ) << "   -> End of track list" << endmsg;
  }
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDLikelihoodMinimiser )

//=============================================================================
