/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STl
#include <algorithm>
#include <assert.h>

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Event Model
#include "Event/RichPID.h"
#include "Event/Track.h"

// Rec Event
#include "RichFutureRecEvent/RichRecTrackPIDInfo.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

namespace Rich::Future::Rec::GlobalPID
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace
  {
    /// The input track container type
    using InTracks = LHCb::Track::Selection;
    // using InTracks = LHCb::Track::Range;
  } // namespace

  /** @class WriteRichPIDs RichGlobalPIDWriteRichPIDs.h
   *
   *  Writes the final RichPID data objects.
   *
   *  @author Chris Jones
   *  @date   2016-10-28
   */

  class WriteRichPIDs final : public Transformer< LHCb::RichPIDs( const InTracks &,
                                                                  const Summary::Track::Vector &,
                                                                  const TrackPIDHypos &,
                                                                  const TrackDLLs::Vector & ),
                                                  Traits::BaseClass_t< AlgBase > >
  {

  public:

    /// Standard constructor
    WriteRichPIDs( const std::string &name, ISvcLocator *pSvcLocator );

  public:

    /// Algorithm execution via transform
    LHCb::RichPIDs operator()( const InTracks &              tracks,
                               const Summary::Track::Vector &gtracks,
                               const TrackPIDHypos &         hypos,
                               const TrackDLLs::Vector &     dlls ) const override;

  private:

    /// Data object version for RichPID container
    Gaudi::Property< unsigned short int > m_pidVersion { this,
                                                         "PIDVersion",
                                                         2u,
                                                         "Version of the RichPID container" };
  };

} // namespace Rich::Future::Rec::GlobalPID
