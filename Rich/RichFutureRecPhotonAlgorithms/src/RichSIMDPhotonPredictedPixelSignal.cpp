/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "RichSIMDPhotonPredictedPixelSignal.h"

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------
// Implementation file for class : RichPhotonPredictedPixelSignal
//
// 2016-09-30 : Chris Jones
//-----------------------------------------------------------------------------

//=============================================================================

SIMDPhotonPredictedPixelSignal::SIMDPhotonPredictedPixelSignal( const std::string &name,
                                                                ISvcLocator *      pSvcLocator )
  : Transformer(
      name,
      pSvcLocator,
      { KeyValue { "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
        KeyValue { "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },
        KeyValue { "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
        KeyValue { "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
        KeyValue { "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
        KeyValue { "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default },
        KeyValue { "PhotonYieldLocation", PhotonYieldsLocation::Detectable } },
      { KeyValue { "PhotonSignalsLocation", SIMDPhotonSignalsLocation::Default } } )
{
  // init
  m_factor.fill( 0 );
  // debug
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode
SIMDPhotonPredictedPixelSignal::initialize()
{
  const auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // Get Rich DetElems
  const auto *Rich1DE = getDet< DeRich1 >( DeRichLocations::Rich1 );
  const auto *Rich2DE = getDet< DeRich2 >( DeRichLocations::Rich2 );

  // Mirror radii of curvature in mm
  const DetectorArray< double > radiusCurv = { Rich1DE->sphMirrorRadius(),
                                               Rich2DE->sphMirrorRadius() };

  // cache the factor for each RICH
  for ( const auto det : { Rich::Rich1, Rich::Rich2 } )
  {
    m_factor[det] =
      ( m_scaleFactor[det] / ( std::pow( radiusCurv[det], 2 ) * std::pow( 2.0 * M_PI, 1.5 ) ) );
  }
  // loop over radiators
  for ( const auto rad : Rich::radiators() )
  {
    // cache SIMD versions of min prob values
    m_minPhotonProbSIMD[rad] = SIMDFP( m_minPhotonProb[rad] );

    // Compute an estimate of the minimum argument to the exp function below
    // with which we can be sure the pixel would anyway always fail the min
    // probability cut. The factor of 100 is to take into account the maximum
    // possible factor exp(arg) is scaled by. Max normally never goes above 2
    // so 100 is very safe...
    m_minExpArgF[rad] =
      SIMDFP( std::max( m_minArg.value(), std::log( m_minPhotonProb[rad] / 100.0f ) ) );
  }

  // cache min exp argument
  m_minArgSIMD = SIMDFP( m_minArg );
  m_expMinArg  = std::exp( m_minArgSIMD );

  // return
  return sc;
}

//=============================================================================

OutData
SIMDPhotonPredictedPixelSignal::operator()( const SIMDPixelSummaries &                pixels,
                                            const SIMDCherenkovPhoton::Vector &       photons,
                                            const Relations::PhotonToParents::Vector &photRels,
                                            const LHCb::RichTrackSegment::Vector &    segments,
                                            const CherenkovAngles::Vector &           ckAngles,
                                            const CherenkovResolutions::Vector &      ckRes,
                                            const PhotonYields::Vector &photYields ) const
{
  // output data
  OutData signals;
  signals.reserve( photons.size() );

  // Min CK theta angle for signal
  const SIMDFP minCKTheta( 1e-10 );

  // Form the zipped track data range
  const auto tkRange = Ranges::ConstZip( ckAngles, ckRes, photYields, segments );

  // Loop over photon data
  for ( const auto &&[phot, rels] : Ranges::ConstZip( photons, photRels ) )
  {
    // Save a new entry in the output for this photon
    signals.emplace_back();
    auto &sigs = signals.back();

    // Which detector ?
    const auto det = phot.rich();

    // Reconstructed Cherenkov theta angle
    auto thetaReco                      = phot.CherenkovTheta();
    thetaReco( thetaReco < minCKTheta ) = minCKTheta;

    // get the pixel summary via index
    const auto &pix = pixels[rels.pixelIndex()];

    // Compute the ID independent term
    const auto AInd = pix.effArea() * phot.activeSegmentFraction() * m_factor[det] / thetaReco;

    // Access the segment data via index
    const auto &[tkCkAngles, tkCkRes, tkYields, segment] = tkRange[rels.segmentIndex()];

    // radiator
    const auto rad = segment.radiator();

    // Loop over the mass hypos and compute and fill each value
    for ( const auto id : activeParticlesNoBT() )
    {
      // get the expected CK theta angle for this hypo
      const auto tkA = tkCkAngles[id];

      // mass types are strictly ordered by increasing mass, so once we
      // are below threshold can abort the mass hypothesis loop.
      if ( UNLIKELY( tkA <= m_minExpCKT[rad] ) ) { break; }

      // 1 / Expected Cherenkov theta angle resolution
      const auto thetaExpResInv = ( SIMDFP::One() / SIMDFP( tkCkRes[id] ) );

      // Theta diff / resolution
      const auto thetaDiffOvRes = ( thetaReco - SIMDFP( tkA ) ) * thetaExpResInv;

      // compute the signal probability for this hypo

      // First the argument to exp() function
      auto arg = SIMDFP( -0.5f ) * thetaDiffOvRes * thetaDiffOvRes;

      // selection mask
      auto mask = phot.validityMask() && ( arg > m_minExpArgF[rad] );

      // If any are OK continue
      if ( any_of( mask ) )
      {

        // Set any arg values below the minimum to the min
        arg( !mask ) = m_minArgSIMD;

        // compute exp(arg)
        const auto expArg = myexp( arg );

        // Expected yield
        const SIMDFP tkY( tkYields[id] );

        // The signal to set
        auto &sig = sigs[id];

        // Compute the signal
        sig = AInd * expArg * tkY * thetaExpResInv;

        // Check min prob value
        mask &= sig > m_minPhotonProbSIMD[rad];
        sig.setZeroInverted( mask );

      } // min arg

    } // hypo loop
  }

  // return the final data
  return signals;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPhotonPredictedPixelSignal )

//=============================================================================
