/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichSIMDQuarticPhotonReco.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future;
using namespace Rich::Future::Rec;

//=============================================================================

SIMDQuarticPhotonReco::SIMDQuarticPhotonReco( const std::string &name, ISvcLocator *pSvcLocator )
  : MultiTransformer(
      name,
      pSvcLocator,
      { KeyValue { "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
        KeyValue { "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
        KeyValue { "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default },
        KeyValue { "TrackLocalPointsLocation", SpacePointLocation::SegmentsLocal },
        KeyValue { "SegmentPhotonFlagsLocation", SegmentPhotonFlagsLocation::Default },
        KeyValue { "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
        KeyValue { "TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected } },
      { KeyValue { "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },
        KeyValue { "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default } } )
{
  // debugging
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode
SIMDQuarticPhotonReco::initialize()
{
  // Sets up various tools and services
  auto sc = MultiTransformer::initialize();
  if ( !sc ) return sc;

  // loop over radiators
  for ( const auto rad : Rich::radiators() )
  {
    // Local SIMD copies of various base class properties
    m_minActiveFracSIMD[rad]   = SIMDFP( m_minActiveFrac[rad] );
    m_minSphMirrTolItSIMD[rad] = SIMDFP( m_minSphMirrTolIt[rad] );

    // Do not support 0 iterations here...
    if ( m_nMaxQits[rad] <= 0 ) { return Error( "Iterations must be >0" ); }
  }

  // return
  return sc;
}

//=============================================================================

OutData
SIMDQuarticPhotonReco::operator()( const LHCb::RichTrackSegment::Vector &    segments,
                                   const CherenkovAngles::Vector &           ckAngles,
                                   const CherenkovResolutions::Vector &      ckResolutions,
                                   const SegmentPanelSpacePoints::Vector &   trHitPntsLoc,
                                   const SegmentPhotonFlags::Vector &        segPhotFlags,
                                   const SIMDPixelSummaries &                pixels,
                                   const Relations::TrackToSegments::Vector &tkToSegRels ) const
{

  // make the output data
  OutData outData;

  // Shortcut to the photons and relations
  auto &[photons, relations] = outData;

  // guess at reserve size
  const auto resSize = segments.size() * pixels.size() / 8;
  photons.reserve( resSize );
  relations.reserve( resSize );

  // local position corrector
  // longer term need to remove this
  const Rich::Rec::RadPositionCorrector< SIMDFP > corrector;

  // best secondary and spherical mirror data caches
  Rich::SIMD::MirrorData sphMirrData, secMirrData;
  Mirrors                sphMirrors { {} }, secMirrors { {} };

  // global photon index
  int photonIndex( -1 );

  // Loop over the track->segment relations
  for ( const auto &inTkRel : tkToSegRels )
  {
    // loop over segments for this track
    for ( const auto &segIndex : inTkRel.segmentIndices )
    {
      // Get the data from the segment containers
      const auto &segment    = segments[segIndex];
      const auto &tkCkAngles = ckAngles[segIndex];
      const auto &tkCkRes    = ckResolutions[segIndex];
      const auto &tkLocPtn   = trHitPntsLoc[segIndex];
      const auto &segFlags   = segPhotFlags[segIndex];

      // which RICH and radiator
      const auto rich = segment.rich();
      const auto rad  = segment.radiator();

      // Cache the ambiguous photon check start points outside pixel loop
      // First emission point, at start of track segment
      const SIMDPixel::Point emissionPoint1(
        m_testForUnambigPhots[rad] ? segment.bestPoint( 0.01 ) : segment.bestPoint() );
      // now do it again for emission point #2, at end of segment
      const SIMDPixel::Point emissionPoint2(
        m_testForUnambigPhots[rad] ? segment.bestPoint( 0.99 ) : segment.bestPoint() );

      // cache SIMD version of segment point for each side
      const PanelArray< SIMDPixel::Point > segSidePtns {
        { SIMDPixel::Point( tkLocPtn.point( Rich::top ) ), // also R2 left
          SIMDPixel::Point( tkLocPtn.point( Rich::bottom ) ) }
      }; // also R2 right

      //_ri_verbo << endmsg;
      //_ri_verbo << "Segment P=" << segment.bestMomentum() << " " << rich << " " << rad << endmsg;

      // get the best pixel range for this segment, based on where hits are expected
      const auto pixR =
        ( segFlags.inBothPanels() ?
            pixels.range( rich ) :
            Rich::Rich1 == rich ?
            ( segFlags.inPanel( Rich::top ) ? pixels.range( Rich::Rich1, Rich::top ) :
                                              pixels.range( Rich::Rich1, Rich::bottom ) ) :
            ( segFlags.inPanel( Rich::left ) ? pixels.range( Rich::Rich2, Rich::left ) :
                                               pixels.range( Rich::Rich2, Rich::right ) ) );

      // SIMD Pixel index in container (start index - 1) for this range
      int pixIndex = pixR.begin() - pixels.begin() - 1;

      // loop over selected pixels
      for ( const auto &pix : pixR )
      {

        // increment the pixel index
        ++pixIndex;

        //_ri_verbo << endmsg;
        //_ri_verbo << " -> Pixel IDs    " << pix.smartID() << endmsg;
        //_ri_verbo << " ->       GloPos " << pix.gloPos() << endmsg;
        //_ri_verbo << "          LocPos " << pix.locPos() << endmsg;
        //_ri_verbo << "          Mask   " << pix.validMask() << endmsg;

        // side for this pack of pixels
        const auto side = pix.side();

        // Pixel position, in local HPD coords corrected for average radiator distortion
        // Might need to pre-compute and cache this as we repeat it here each segment...
        const auto pixP = corrector.correct( pix.locPos(), rad );

        //  Track local hit point on the same panel as the hit
        const auto &segPanelPnt = segSidePtns[side];

        // compute the seperation squared
        const auto dx   = pixP.x() - segPanelPnt.x();
        const auto dy   = pixP.y() - segPanelPnt.y();
        const auto sep2 = ( ( dx * dx ) + ( dy * dy ) );

        // Start mask off as the pixel mask
        auto pixmask = pix.validMask();

        // Check overall boundaries
        pixmask &= ( sep2 < m_maxROI2PreSelSIMD[rad] && sep2 > m_minROI2PreSelSIMD[rad] );
        //_ri_verbo << "  -> sep2 = " << sep2 << " OK=" << pixmask << endmsg;
        if ( any_of( pixmask ) )
        {
          // estimated CK theta
          const auto ckThetaEsti = std::sqrt( sep2 ) * m_scalePreSelSIMD[rad];
          //_ri_verbo << "   -> CK theta Esti " << ckThetaEsti << endmsg;

          // Is any hit close to any mass hypo in local coordinate space ?
          // only need confirmation for those already selected so start with !pixmask
          auto hypoMask = !pixmask;
          for ( const auto hypo : activeParticlesNoBT() )
          {
            const SIMDPixel::SIMDFP angs( tkCkAngles[hypo] ); // maybe should cache this
            const SIMDPixel::SIMDFP reso( tkCkRes[hypo] );    // maybe should cache this
            hypoMask |= ( abs( angs - ckThetaEsti ) < ( m_nSigmaPreSelSIMD[rad] * reso ) );
            if ( all_of( hypoMask ) ) break;
          }
          pixmask &= hypoMask;
        }

        // Did any hit pass the pre-sel
        //_ri_verbo << "  -> PreSel=" << pixmask << endmsg;
        if ( UNLIKELY( none_of( pixmask ) ) ) { continue; }

        // make a photon object to work on
        photons.emplace_back( rich, pix.smartID() );
        auto &gPhoton = photons.back();
        // Use a guard to remove the last added photon unless explicitly saved
        DataGuard< decltype( photons ) > photGuard( photons );

        // Emission point to use for photon reconstruction
        SIMD::Point< FP > emissionPoint { segment.bestPoint() };

        // Final reflection points on sec and spherical mirrors
        SIMD::Point< FP > sphReflPoint, secReflPoint;

        // fraction of segment path length accessible to the photon
        auto fraction = SIMDFP::One();

        // flag to say if these photon candidates are un-ambiguous - default to false
        auto unambigPhoton = SIMDFP::MaskType::Zero();

        // find the reflection of the detection point in the sec mirror
        // (virtual detection point) starting with nominal values
        // At this point we are assuming a flat nominal mirror common to all segments
        // Note rich and side are the same for all SIMD pixels by design
        const auto nom_dist     = m_rich[rich]->nominalPlaneSIMD( side ).Distance( pix.gloPos() );
        auto       virtDetPoint = pix.gloPos();
        virtDetPoint -= ( SIMDFP( 2.0 ) * nom_dist * m_rich[rich]->nominalNormalSIMD( side ) );
        //_ri_verbo << "  -> virtDetPoint = " << virtDetPoint << endmsg;

        // --------------------------------------------------------------------------------------
        // For gas radiators, try start and end points to see if photon is unambiguous
        // NOTE : For this we are using the virtual detection point determined using
        // the noimnal flat secondary mirror plane. Now the secondary mirrors are actually
        // spherical this may be introducing some additional uncertainties.
        // --------------------------------------------------------------------------------------
        if ( UNLIKELY( m_testForUnambigPhots[rad] ) )
        {

          // -------------------------------------------------------------------------------
          // Test reconstruction from segment start point
          // -------------------------------------------------------------------------------
          // Find mirror segments for this emission point
          SIMD::Point< FP > sphReflPoint1, secReflPoint1;
          findMirrorData( rich,
                          side,
                          virtDetPoint,
                          emissionPoint1,
                          sphMirrors,
                          secMirrors,
                          sphReflPoint1,
                          secReflPoint1,
                          pixmask );
          //_ri_verbo << "  -> unambig1 " << pixmask << endmsg;
          if ( UNLIKELY( none_of( pixmask ) ) )
          {
            //_ri_debug << rad << " : Failed to reconstruct photon for start of segment" << endmsg;
            continue;
          }
          const auto intersectBeamPipe1 =
            ( m_checkBeamPipe[rad] ?
                deBeam( rich )->testForIntersection( emissionPoint1, sphReflPoint1, pixmask ) :
                SIMDFP::MaskType::Zero() );
          // -------------------------------------------------------------------------------

          // -------------------------------------------------------------------------------
          // Test reconstruction from segment end point
          // -------------------------------------------------------------------------------
          // Temporary mirror data objects
          Mirrors           sphMirrors2, secMirrors2;
          SIMD::Point< FP > sphReflPoint2, secReflPoint2;
          findMirrorData( rich,
                          side,
                          virtDetPoint,
                          emissionPoint2,
                          sphMirrors2,
                          secMirrors2,
                          sphReflPoint2,
                          secReflPoint2,
                          pixmask );
          //_ri_verbo << "  -> unambig2 " << pixmask << endmsg;
          if ( UNLIKELY( none_of( pixmask ) ) )
          {
            //_ri_debug << rad << " : Failed to reconstruct photon for end of segment" << endmsg;
            continue;
          }
          // only run this if entry failed all photons
          const auto intersectBeamPipe2 =
            ( any_of( intersectBeamPipe1 ) && m_checkBeamPipe[rad] ?
                deBeam( rich )->testForIntersection( emissionPoint2, sphReflPoint2, pixmask ) :
                SIMDFP::MaskType::Zero() );
          // -------------------------------------------------------------------------------

          // -------------------------------------------------------------------------------
          // Beampipe check
          // -------------------------------------------------------------------------------
          const auto beamPipeOK = !( intersectBeamPipe1 && intersectBeamPipe2 );
          //_ri_verbo << "  -> beamPipeOK " << beamPipeOK << endmsg;
          pixmask &= beamPipeOK;
          //_ri_verbo << "  -> unambig beampipe   " << pixmask << endmsg;
          if ( UNLIKELY( none_of( pixmask ) ) )
          {
            // both start and end points failed beam pipe test -> reject
            //_ri_debug << rad << " : Failed ambiguous photon beampipe intersection checks" <<
            // endmsg;
            continue;
          }
          // -------------------------------------------------------------------------------

          // -------------------------------------------------------------------------------
          // Get the best gas emission point
          // -------------------------------------------------------------------------------
          pixmask &= getBestGasEmissionPoint(
            rad, sphReflPoint1, sphReflPoint2, pix.gloPos(), segment, emissionPoint, fraction );
          //_ri_verbo << "  -> getBestGasEmissionPoint " << pixmask << endmsg;
          if ( UNLIKELY( none_of( pixmask ) ) )
          {
            //_ri_debug << rad << " : Failed to compute best gas emission point" << endmsg;
            continue;
          }
          // -------------------------------------------------------------------------------

          // -------------------------------------------------------------------------------
          // Is this an unambiguous photon - I.e. only one possible mirror combination
          // -------------------------------------------------------------------------------
          for ( std::size_t i = 0; i < SIMDFP::Size; ++i )
          {
            unambigPhoton[i] =
              ( ( sphMirrors[i] == sphMirrors2[i] ) && ( secMirrors[i] == secMirrors2[i] ) );
          }
          if ( any_of( unambigPhoton ) )
          {
            // rough guesses at reflection points (improved later on)
            sphReflPoint = sphReflPoint1 + SIMDFP( 0.5 ) * ( sphReflPoint2 - sphReflPoint1 );
            secReflPoint = secReflPoint1 + SIMDFP( 0.5 ) * ( secReflPoint2 - secReflPoint1 );
          }
          // -------------------------------------------------------------------------------

          // -------------------------------------------------------------------------------
          // if configured to do so reject ambiguous photons
          // -------------------------------------------------------------------------------
          if ( UNLIKELY( m_rejectAmbigPhots[rad] ) )
          {
            pixmask &= unambigPhoton;
            //_ri_debug << rad << " : Failed ambiguous photon test" << endmsg;
            if ( UNLIKELY( none_of( pixmask ) ) ) { continue; }
          }
          // -------------------------------------------------------------------------------

          //_ri_verbo << "  -> end unambiguous photon check " << pixmask << endmsg;

        } // end unambiguous photon check
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Active segment fraction cut
        // --------------------------------------------------------------------------------------
        pixmask &= ( fraction >= m_minActiveFracSIMD[rad] );
        //_ri_verbo << "  -> active fraction " << pixmask << endmsg;
        if ( UNLIKELY( none_of( pixmask ) ) )
        {
          //_ri_debug << rad << " : Failed active segment fraction cut" << endmsg;
          continue;
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // if ambiguous gas photon or if ambiguous photon check above has been skipped, try again
        // using best emission point and nominal mirror geometries to get the spherical and sec
        // mirrors.
        // --------------------------------------------------------------------------------------
        if ( LIKELY( !m_testForUnambigPhots[rad] || !all_of( unambigPhoton ) ) )
        {
          findMirrorData( rich,
                          side,
                          virtDetPoint,
                          emissionPoint,
                          sphMirrors,
                          secMirrors,
                          sphReflPoint,
                          secReflPoint,
                          pixmask );
          //_ri_verbo << "  -> retest ambig photon " << pixmask << endmsg;
          if ( UNLIKELY( none_of( pixmask ) ) )
          {
            //_ri_debug << rad << " : Failed to compute best gas emission point" << endmsg;
            continue;
          }
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // make sure the mirror caches are up to date at this point
        // --------------------------------------------------------------------------------------
        secMirrData.update( secMirrors );
        sphMirrData.update( sphMirrors );
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Finally reconstruct the photon using best emission point and the best mirror segments
        // --------------------------------------------------------------------------------------
        if ( LIKELY( m_useAlignedMirrSegs[rad] ) )
        {

          // Iterate to final solution, improving the secondary mirror info
          int               iIt( 0 );
          SIMD::Point< FP > last_mirror_point( 0, 0, 0 );
          while ( iIt < m_nMaxQits[rad] )
          {

            // Get secondary mirror reflection point,
            // using the best actual secondary mirror segment at this point
            const auto dir = virtDetPoint - sphReflPoint;
            pixmask &=
              ( m_treatSecMirrsFlat[rich] ?
                  intersectPlane( sphReflPoint, dir, secMirrData.getNormalPlane(), secReflPoint ) :
                  intersectSpherical( sphReflPoint,
                                      dir,
                                      secMirrData.getCoCs(),
                                      secMirrData.getRoCs(),
                                      secReflPoint ) );
            //_ri_verbo << "  -> It" << iIt << " secMirrIntersect " << pixmask << endmsg;
            if ( UNLIKELY( none_of( pixmask ) ) ) { break; }

            // (re)find the secondary mirror
            secMirrors = m_mirrorSegFinder.get()->findSecMirror( rich, side, secReflPoint );
            // update the cache
            secMirrData.update( secMirrors );

            // Compute the virtual reflection point

            // Construct plane tangential to secondary mirror passing through reflection point
            // const Gaudi::Plane3D plane( secSegment->centreOfCurvature()-secReflPoint,
            // secReflPoint );
            // re-find the reflection of the detection point in the sec mirror
            // (virtual detection point) with this mirror plane
            // const auto distance     = plane.Distance(gloPos);
            // virtDetPoint = gloPos - 2.0 * distance * plane.Normal();

            // Same as above, just written out by hand and simplified a bit
            const auto normV    = secMirrData.getCoCs() - secReflPoint;
            const auto distance = ( ( normV.X() * ( pix.gloPos().X() - secReflPoint.X() ) ) +
                                    ( normV.Y() * ( pix.gloPos().Y() - secReflPoint.Y() ) ) +
                                    ( normV.Z() * ( pix.gloPos().Z() - secReflPoint.Z() ) ) );
            virtDetPoint = pix.gloPos() - ( normV * ( SIMDFP( 2.0 ) * distance / normV.Mag2() ) );

            // solve the quartic using the new data
            m_quarticSolver.solve< SIMDFP, 2, 3 >( emissionPoint,
                                                   sphMirrData.getCoCs(),
                                                   virtDetPoint,
                                                   sphMirrData.getRoCs(),
                                                   sphReflPoint );

            // Iteration flags
            // First iteration ?
            const bool firstIt = ( 0 == iIt );
            // increment iteration counter
            ++iIt;
            // Last it ?
            const bool lastIt = ( m_nMaxQits[rad] == iIt );

            // for iterations after the first, and not the last, see if we are still moving.
            // If not, abort iterations early.
            if ( !firstIt && !lastIt )
            {
              const auto diffSq = ( last_mirror_point - secReflPoint ).Mag2();
              if ( UNLIKELY( all_of( diffSq < m_minSphMirrTolItSIMD[rad] ) ) )
              {
                //_ri_verbo << "   -> Not Moving -> Abort iterations."<< endmsg;
                break;
              }
            }

            // Are we going to iterate again ?
            if ( !lastIt )
            {
              //  (re)find the spherical mirror segments
              //_ri_verbo << "   -> Update for sphMirrors using " << sphReflPoint << endmsg;
              sphMirrors = m_mirrorSegFinder.get()->findSphMirror( rich, side, sphReflPoint );
              // update the caches
              sphMirrData.update( sphMirrors );
              // store last sec mirror point
              last_mirror_point = secReflPoint;
            }
          }

          // if above while loop failed, abort this photon
          //_ri_verbo << "  -> End It " << pixmask << endmsg;
          if ( UNLIKELY( none_of( pixmask ) ) ) { continue; }
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // check that spherical mirror reflection point is on the same side as detection point
        // and (if configured to do so) photon does not cross between detector sides
        // --------------------------------------------------------------------------------------
        pixmask &= sameSide( rad, sphReflPoint, virtDetPoint );
        //_ri_verbo << "  -> sameSide " << pixmask << endmsg;
        if ( UNLIKELY( none_of( pixmask ) ) )
        {
          //_ri_debug << rad << " : Reflection point on wrong side" << endmsg;
          continue;
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // For gas radiators if ambiguous photon checks are disabled (since this is
        // already done for these photons during those checks), check if the photon would have
        // intersected with the beampipe
        // --------------------------------------------------------------------------------------
        if ( UNLIKELY( !m_testForUnambigPhots[rad] && m_checkBeamPipe[rad] ) )
        {
          const auto beamInt =
            deBeam( rich )->testForIntersection( emissionPoint, sphReflPoint, pixmask );
          pixmask &= !beamInt;
          //_ri_verbo << "  -> final beamtest " << pixmask << endmsg;
          if ( UNLIKELY( none_of( pixmask ) ) )
          {
            //_ri_debug << rad << " : Failed final beampipe intersection checks" << endmsg;
            continue;
          }
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // If using aligned mirror segments, get the final sec mirror reflection
        // point using the best mirror segments available at this point
        // For RICH2, use the spherical nature of the scondary mirrors
        // For RICH1, where they are much flatter, assume complete flatness
        // --------------------------------------------------------------------------------------
        if ( LIKELY( m_useAlignedMirrSegs[rad] ) )
        {
          const auto dir = virtDetPoint - sphReflPoint;
          pixmask &=
            ( m_treatSecMirrsFlat[rich] ?
                intersectPlane( sphReflPoint, dir, secMirrData.getNormalPlane(), secReflPoint ) :
                intersectSpherical(
                  sphReflPoint, dir, secMirrData.getCoCs(), secMirrData.getRoCs(), secReflPoint ) );
          //_ri_verbo << "  -> alignedMirrors " << pixmask << endmsg;
          if ( UNLIKELY( none_of( pixmask ) ) )
          {
            //_ri_debug << rad << " : Failed final secondary mirror plane intersection" << endmsg;
            continue;
          }
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Calculate the cherenkov angles using the photon and track vectors
        // --------------------------------------------------------------------------------------
        // direction at emission. No need to be normalised.
        const auto photonDirection = ( sphReflPoint - emissionPoint );
        SIMDFP     thetaCerenkov( SIMDFP::Zero() ), phiCerenkov( SIMDFP::Zero() );
        segment.angleToDirection( photonDirection, thetaCerenkov, phiCerenkov );
        //_ri_verbo << std::setprecision(9) << " -> Phot Dir " << photonDirection << endmsg;
        //_ri_verbo << std::setprecision(9) << "    theta    " << thetaCerenkov << endmsg;
        //_ri_verbo << std::setprecision(9) << "    phi      " << phiCerenkov << endmsg;
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Truncate the theta and phi values to a fixed precision
        // @todo Implement this for SIMD types
        // --------------------------------------------------------------------------------------
        // thetaCerenkov = truncate(thetaCerenkov);
        // phiCerenkov   = truncate(phiCerenkov);
        // --------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------
        // Apply fudge factor correction for small biases in CK theta
        //---------------------------------------------------------------------------------------
        thetaCerenkov += m_ckThetaCorrSIMD[rad];
        //---------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Final checks on the Cherenkov angle
        // --------------------------------------------------------------------------------------
        checkAngle( rad, tkCkAngles, tkCkRes, thetaCerenkov, pixmask );
        //_ri_verbo << "  -> checkAngles " << pixmask << endmsg;
        if ( UNLIKELY( none_of( pixmask ) ) )
        {
          //_ri_verbo << "    -> photon FAILED checkAngleInRange test" << endmsg;
          continue;
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Set (remaining) photon parameters
        // --------------------------------------------------------------------------------------
        gPhoton.setCherenkovTheta( thetaCerenkov );
        gPhoton.setCherenkovPhi( phiCerenkov );
        gPhoton.setActiveSegmentFraction( fraction );
        gPhoton.setUnambiguousPhoton( unambigPhoton );
        gPhoton.setValidityMask( pixmask );
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // If we get here, keep the just made photon
        // --------------------------------------------------------------------------------------
        photGuard.setOK();
        // Save relations
        relations.emplace_back( ++photonIndex, pixIndex, segIndex, inTkRel.tkIndex );
        // info() << relations.back() << endmsg;
        // info() << gPhoton << endmsg;
        // for ( const auto & sphot : gPhoton.scalarPhotons() ) { info() << sphot << endmsg; }
        // --------------------------------------------------------------------------------------

      } // pixel loop

    } // segment loop
  }   // track loop

  // if ( photons.size() > resSize )
  // {
  //   warning() << "Photons reserve size too small. Scale = "
  //             << (float)( segments.size() * pixels.size() ) / photons.size()
  //             << endmsg;
  // }

  // return the final data
  return outData;
}

//=============================================================================
// Find mirror segments and reflection points for given data
//=============================================================================
void
SIMDQuarticPhotonReco::findMirrorData( const Rich::DetectorType rich,
                                       const Rich::Side         side,
                                       const SIMD::Point< FP > &virtDetPoint,
                                       const SIMD::Point< FP > &emissionPoint,
                                       Mirrors &                primMirr,
                                       Mirrors &                secMirr,
                                       SIMD::Point< FP > &      sphReflPoint,
                                       SIMD::Point< FP > &      secReflPoint,
                                       SIMDFP::mask_type &      OK ) const
{
  // solve quartic equation with nominal values and find spherical mirror reflection point
  m_quarticSolver.solve< SIMDFP, 2, 2 >( emissionPoint,
                                         m_rich[rich]->nominalCentreOfCurvatureSIMD( side ),
                                         virtDetPoint,
                                         m_rich[rich]->sphMirrorRadiusSIMD(),
                                         sphReflPoint );
  // find the spherical mirror segments
  //_ri_verbo << "   -> sphReflPoint " << sphReflPoint << endmsg;
  primMirr = m_mirrorSegFinder.get()->findSphMirror( rich, side, sphReflPoint );
  // Search for the secondary segment
  // Direction vector from primary mirror point to virtual detection point
  const auto dir( virtDetPoint - sphReflPoint );
  // find the sec mirror intersction point and secondary mirror segment
  OK &= intersectPlane( sphReflPoint, dir, m_rich[rich]->nominalPlaneSIMD( side ), secReflPoint );
  if ( LIKELY( any_of( OK ) ) )
  {
    // find the secondary mirrors
    secMirr = m_mirrorSegFinder.get()->findSecMirror( rich, side, secReflPoint );
  }
}

//=============================================================================
// Compute the best emission point for the gas radiators using
// the given spherical mirror reflection points
//=============================================================================
SIMDQuarticPhotonReco::SIMDFP::mask_type
SIMDQuarticPhotonReco::getBestGasEmissionPoint( const Rich::RadiatorType      radiator,
                                                const SIMD::Point< FP > &     sphReflPoint1,
                                                const SIMD::Point< FP > &     sphReflPoint2,
                                                const SIMD::Point< FP > &     detectionPoint,
                                                const LHCb::RichTrackSegment &segment,
                                                SIMD::Point< FP > &           emissionPoint,
                                                SIMDFP &                      fraction ) const
{
  // Constants
  const SIMDFP half( 0.5 );

  // Default point for emission point is the middle
  SIMDFP alongTkFrac = half;

  // Default to all OK
  auto ok = SIMDFP::MaskType::One();

  // Default emission point is in the middle
  emissionPoint = segment.bestPoint();

  // which radiator ?
  if ( radiator == Rich::Rich1Gas )
  {
    // First reflection and hit point on same y side ?
    const auto sameSide1 = ( sphReflPoint1.Y() * detectionPoint.Y() > SIMDFP::Zero() );
    const auto sameSide2 = ( sphReflPoint2.Y() * detectionPoint.Y() > SIMDFP::Zero() );
    // set return flag
    ok = sameSide1 || sameSide2;
    // Update those that pass just one test
    if ( UNLIKELY( any_of( sameSide1 ^ sameSide2 ) ) )
    {
      const auto m1         = sameSide1 && !sameSide2;
      const auto m2         = sameSide2 && !sameSide1;
      auto       diff       = sphReflPoint1.Y() - sphReflPoint2.Y();
      diff( !( m1 || m2 ) ) = SIMDFP::One(); // prevent div by zero
      const auto f          = SIMDFP::One() / diff;
      fraction( m1 )        = abs( sphReflPoint1.Y() * f );
      fraction( m2 )        = abs( sphReflPoint2.Y() * f );
      alongTkFrac( m1 )     = half * fraction;
      alongTkFrac( m2 )     = SIMDFP::One() - ( half * fraction );
      emissionPoint         = segment.bestPoint( alongTkFrac );
    }
  }
  else // RICH2 gas
  {
    // First reflection and hit point on same y side ?
    const auto sameSide1 = ( sphReflPoint1.X() * detectionPoint.X() > SIMDFP::Zero() );
    const auto sameSide2 = ( sphReflPoint2.X() * detectionPoint.X() > SIMDFP::Zero() );
    // set return flag
    ok = sameSide1 || sameSide2;
    // Update those that pass just one test
    if ( UNLIKELY( any_of( sameSide1 ^ sameSide2 ) ) )
    {
      const auto m1         = sameSide1 && !sameSide2;
      const auto m2         = sameSide2 && !sameSide1;
      auto       diff       = sphReflPoint1.X() - sphReflPoint2.X();
      diff( !( m1 || m2 ) ) = SIMDFP::One(); // prevent div by zero
      const auto f          = SIMDFP::One() / diff;
      fraction( m1 )        = abs( sphReflPoint1.X() * f );
      fraction( m2 )        = abs( sphReflPoint2.X() * f );
      alongTkFrac( m1 )     = half * fraction;
      alongTkFrac( m2 )     = SIMDFP::One() - ( half * fraction );
      emissionPoint         = segment.bestPoint( alongTkFrac );
    }
  }

  // return the mask
  return ok;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDQuarticPhotonReco )

//=============================================================================
